function setLocationAjax_productgrid(el, url, id, type) {
    var qty = 1;
    if (jQuery("#qty_" + id).val() > 0)
        qty = jQuery("#qty_" + id).val();
    if (url.indexOf("?")) {
        url = url.split("?")[0];
    }
    url += 'isAjax/1';
    url = url.replace("checkout/cart", "ajaxcart/index");
    if (window.location.href.match("https://") && !url.match("https://")) {
        url = url.replace("http://", "https://");
    }
    if (window.location.href.match("http://") && !url.match("http://")) {
        url = url.replace("https://", "http://");
    }
    jQuery(el).parent().parent().children(".loader-container").show();
    try {
        jQuery.ajax({
            url: url,
            dataType: 'json',
            data: {qty: qty},
            success: function (data) {
                jQuery('#loading-mask').hide();
                jQuery(".loader-container").hide();
                setAjaxData(data, false, type);
            }
        });
    } catch (e) {
    }
}
function showOptions_productgrid(id) {
    initFancybox_productgrid();
    jQuery('#fancybox' + id).trigger('click');
}

function initFancybox_productgrid() {
    jQuery.noConflict();
    jQuery(document).ready(function () {
        jQuery('.fancybox').fancybox({
            hideOnContentClick: true,
            width: 382,
            autoDimensions: true,
            type: 'iframe',
            showTitle: false,
            scrolling: 'no',
            onComplete: function () {
                jQuery('#fancybox-frame').load(function () { // wait for frame to load and then gets it's height
                    jQuery('#fancybox-content').height(jQuery(this).contents().find('body').height() + 100);
                    jQuery.fancybox.resize();
                });

            },
            'beforeLoad': function () {
                jQuery("head").append('<style type="text/css" id="fancybox_hide_loading_css">#fancybox-loading{display:none}.fancybox-overlay{background:transparent}</style>');
                jQuery(".loader-container").hide();
                jQuery(this.element).parent().parent().children(".loader-container").show();
                jQuery(this.element).parent().children(".loader-container").show();
            },
            'afterLoad': function () {
                jQuery("#fancybox_hide_loading_css").remove();
                jQuery(".loader-container").hide();
            },
            'afterClose': function () {
                setTimeout(function () {
                    jQuery("#fancybox_hide_loading_css").remove();
                }, 500);
                jQuery(".loader-container").hide();
            }
        }
        );
    });
}