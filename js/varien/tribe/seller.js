jQuery(document).ready(function($){
$(".anc").fancybox({
        // API options 
        width: 'auto',
        height: 'auto',
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        //'scrolling': 'no',
        type: 'ajax',
        wrapCSS: 'custom-pin-fancybox'	
    });

});

function showDirection(seller_id, url) {
    jQuery.fancybox({
        'padding': '0px',
        'closeClick': false,
        'autoScale': true,
        autoSize: false,
        'width':'90%',
        'height':'450px',
        'transitionIn': 'fade',
        'transitionOut': 'fade',
        'type': 'ajax',
        'href': url,
        'onComplete': function () {
google.maps.event.trigger(map, "resize");
map.setZoom( map.getZoom() );
        },
        'tpl': {
            closeBtn: '<a title="Close" class="fancybox-item fancybox-close fancybox-newsletter-close" href="javascript:;"></a>'
        },
        'ajax': {
            data: {'seller_id': seller_id},
            dataType: 'html',
            headers: {'X-fancyBox': true}
        },
        'helpers': {
            overlay: {
                locked: true,
                closeClick: false
            }
        }, keys: {
            close: null
        }
    });
}