<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Contacts
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Contacts index controller
 *
 * @category   Mage
 * @package    Mage_Contacts
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Contacts_IndexController extends Mage_Core_Controller_Front_Action {

    const XML_PATH_EMAIL_RECIPIENT = 'contacts/email/recipient_email';
    const XML_PATH_EMAIL_SENDER = 'contacts/email/sender_email_identity';
    const XML_PATH_EMAIL_TEMPLATE = 'contacts/email/email_template';
    const XML_PATH_ENABLED = 'contacts/contacts/enabled';

    public function preDispatch() {
        //parent::preDispatch();
        $bypassaction = array('post');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
//        if (!Mage::getStoreConfigFlag(self::XML_PATH_ENABLED)) {
//            $this->norouteAction();
//        }
    }

    public function indexAction() {
        $this->loadLayout();
        $this->getLayout()->getBlock('contactForm')
                ->setFormAction(Mage::getUrl('*/*/post'));

        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }

    public function postAction() {
        $datas = $this->getRequest()->getPost();
        if ($datas['name'] && $datas['email'] && $datas['comment']) {
            $phone_present = $datas["telephone"] ? $datas["telephone"] : "";
            $comment = $datas["comment"] ? '<br/> Enquire Comment : ' . $datas["comment"] : "";
            $content = 'Dear admin, <br/><br/>'
                    . 'Following are the enquire details:<br /><br/>'
                    . ' Contact Person Name : ' . $datas["name"] .
                    '<br/> Contact Person Phone : ' . $phone_present .
                    '<br/>' . $comment . '<br/><br/>'
                    . 'Kind Regards,<br/>EarthynGreen Team (<a href="mailto:info@earthyngreen.com">info@earthyngreen.com</a>)<br/><br/><a href="http://www.earthyngreen.com/">www.earthyngreen.com</a>';

            $contentUser = 'Dear ' . $datas["name"] . ', <br/><br/>'
                    . 'Thank you for contacting us. We will get back to you as soon as we can.<br /><br/>' .
                    ' Kind Regards,<br/>EarthynGreen Team (<a href="mailto:info@earthyngreen.com">info@earthyngreen.com</a>)<br/><br/><a href="http://www.earthyngreen.com/">www.earthyngreen.com</a>';
            /* core email */
            require('email/class.phpmailer.php');
            $mail = new PHPMailer();
            $subject = "Contact Us";
            $mail->IsSMTP();
            $mail->SMTPDebug = 0;
            $mail->SMTPAuth = TRUE;
            $mail->SMTPSecure = "tls";
            $mail->Port = SMTPPORT;
            $mail->Username = SMTPUSERNAME;
            $mail->Password = SMTPPASSWORD;
            $mail->Host = SMTPHOST;
            $mail->Mailer = "smtp";
            $mail->From = SMTPFROMEMAIL;
            $mail->FromName = SMTPFROMNAME;
            $mail->AddAddress(SMTPTOEMAIL);
            $mail->Subject = $subject;
            $mail->WordWrap = 80;
            $mail->MsgHTML($content);
            $mail->IsHTML(true);
            if (!$mail->Send()) {
                $res['haserror'] = true;
                $res['resultmail'] = $mail->ErrorInfo;
            } else {
                $res['haserror'] = false;
            }
            $usermail = new PHPMailer();
            $subject = "Contact Us";
            $usermail->IsSMTP();
            $usermail->SMTPDebug = 0;
            $usermail->SMTPAuth = TRUE;
            $usermail->SMTPSecure = "tls";
            $usermail->Port = SMTPPORT;
            $usermail->Username = SMTPUSERNAME;
            $usermail->Password = SMTPPASSWORD;
            $usermail->Host = SMTPHOST;
            $usermail->Mailer = "smtp";
            $usermail->From = SMTPFROMEMAIL;
            $usermail->FromName = SMTPFROMNAME;
            $usermail->AddAddress($datas['email']);
            $usermail->Subject = $subject;
            $usermail->WordWrap = 80;
            $usermail->MsgHTML($contentUser);
            $usermail->IsHTML(true);
            $usermail->Send();
            /* core email */
        } else {
            $res['haserror'] = true;
        }
        echo json_encode($res);
        exit();
    }

//    public function postAction() {
//        $post = $this->getRequest()->getPost();
//        if ($post) {
//            $translate = Mage::getSingleton('core/translate');
//            /* @var $translate Mage_Core_Model_Translate */
//            $translate->setTranslateInline(false);
//            try {
//                $postObject = new Varien_Object();
//                $postObject->setData($post);
//
//                $error = false;
//
//                if (!Zend_Validate::is(trim($post['name']), 'NotEmpty')) {
//                    $error = true;
//                }
//
//                if (!Zend_Validate::is(trim($post['comment']), 'NotEmpty')) {
//                    $error = true;
//                }
//
//                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
//                    $error = true;
//                }
//
//                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
//                    $error = true;
//                }
//
//                if ($error) {
//                    throw new Exception();
//                }
//                $mailTemplate = Mage::getModel('core/email_template');
//                /* @var $mailTemplate Mage_Core_Model_Email_Template */
//                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
//                        ->setReplyTo($post['email'])
//                        ->sendTransactional(
//                                Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE), Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER), Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT), "info@earthyngreen.com", null, array('data' => $postObject)
//                );
//
//                if (!$mailTemplate->getSentSuccess()) {
//                    throw new Exception();
//                }
//
//                $translate->setTranslateInline(true);
//
//                //easy submit
//                $res['status'] = "ERROR";
//                $res['message'] = "Successfully Inserted";
//                $res['redirect'] = "";
//                $res['divid'] = "contactusmessage";
//                print_r(json_encode($res));
//                exit;
//                //easy submit
////                Mage::getSingleton('customer/session')->addSuccess(Mage::helper('contacts')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
////                $this->_redirect("");
//                // $this->_redirect('*/*/');
//                //return;
//            } catch (Exception $e) {
//                $translate->setTranslateInline(true);
//
//                //easy submit
//                $res['status'] = "ERROR";
//                $res['message'] = "Error in submit";
//                $res['redirect'] = "";
//                $res['divid'] = "contactusmessage";
//                print_r(json_encode($res));
//                exit;
//                //easy submit
////                Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
////                $this->_redirect("");
////                return;
//                // $this->_redirect('*/*/');
//            }
//        } else {
//            // $this->_redirect('*/*/');
//            $this->_redirect('');
//        }
//    }
}
