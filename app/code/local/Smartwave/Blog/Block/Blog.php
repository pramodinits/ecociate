<?php

class Smartwave_Blog_Block_Blog extends Smartwave_Blog_Block_Abstract
{
    public function getPosts()
    {
        $collection = $this->_prepareCollection();
        $tag = $this->getRequest()->getParam('tag');
        if ($tag) {
            $collection->addTagFilter(urldecode($tag));
        }
        parent::_processCollection($collection);
        return $collection;
    }

    protected function _prepareLayout()
    {
        if ($this->isBlogPage() && ($breadcrumbs = $this->getCrumbs())) {
            parent::_prepareMetaData(self::$_helper);
            $tag = $this->getRequest()->getParam('tag', false);
            if ($tag) {
                $tag = urldecode($tag);
                $breadcrumbs->addCrumb(
                    'blog',
                    array(
                        'label' => self::$_helper->getTitle(),
                        'title' => $this->__('Return to ' . self::$_helper->getTitle()),
                        'link'  => $this->getBlogUrl(),
                    )
                );
                $breadcrumbs->addCrumb(
                    'blog_tag',
                    array(
                        'label' => $this->__('Tagged with "%s"', self::$_helper->convertSlashes($tag)),
                        'title' => $this->__('Tagged with "%s"', $tag),
                    )
                );
            } else {
                $breadcrumbs->addCrumb('blog', array('label' => self::$_helper->getTitle()));
            }
        }
    }

    protected function _prepareCollection()
    {
        if (!$this->getData('cached_collection')) {
            $sortOrder = $this->getRequest()->getParam('order', self::DEFAULT_SORT_ORDER);
            $sortDirection = $this->getCurrentDirection();
            $collection = Mage::getModel('blog/blog')->getCollection()
                ->addPresentFilter()
                ->addEnableFilter(Smartwave_Blog_Model_Status::STATUS_ENABLED)
                ->addStoreFilter()
                ->joinComments()
            ;
            $collection->setOrder($collection->getConnection()->quote($sortOrder), $sortDirection);
            $collection->setPageSize((int)self::$_helper->postsPerPage());

            $this->setData('cached_collection', $collection);
        }
        return $this->getData('cached_collection');
    }


    public function getCurrentDirection()
    {
        $dir = $this->getRequest()->getParam('dir');

        if (in_array($dir, array('asc', 'desc'))) {
            return $dir;
        }

        return Mage::helper('blog')->defaultPostSort(Mage::app()->getStore()->getId());
    }
    
    
    //added by  bhagyashree
     public function resizeImges($fileName, $width, $height = '',$aspect_ratio = FALSE ) {
        $dimension_slug = ($width && $height) ? $width . "_" . $height : $width;
//        $folderURL = Mage::getBaseUrl('media') . 'arccore/arcs/images';
//        $imageURL = $folderURL . $fileName;
        $basePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . $fileName;
        $newPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . "wysiwyg/smartwave/blog/resized/$dimension_slug" . DS . $fileName;
//        //if width empty then return original size image's URL
//        if ($width != '') {
//            //if image has already resized then just return URL
//            if (file_exists($basePath) && is_file($basePath) && !file_exists($newPath)) {
                $imageObj = new Varien_Image($basePath);
                $imageObj->constrainOnly(TRUE);
                $imageObj->keepAspectRatio($aspect_ratio);
                $imageObj->keepFrame(FALSE);
                $imageObj->resize($width, $height);
                $imageObj->save($newPath);
//            }
            $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "wysiwyg/smartwave/blog/resized/$dimension_slug" . $fileName;
//        } else {
//            $resizedURL = $imageURL;
//        }
        return $resizedURL;
    }
  
}