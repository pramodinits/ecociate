<?php
/**
 * Adaptive image resize extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Bolevar AdaptiveResize module to newer versions in the future.
 * If you wish to customize the Bolevar AdaptiveResize module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Bolevar
 * @package    Bolevar_AdaptiveResize
 * @copyright  Copyright (C) 2012 Roomine Bolevar ltd (http://bolevar.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * AdaptiveResize Helper
 *
 * @category   Bolevar
 * @package    Bolevar_AdaptiveResize
 * @subpackage Helper
 * @author     Denis Obukhov <roomine@bolevar.com>
 */
class Smartwave_Blog_Helper_Image extends Mage_Catalog_Helper_Image
{
    /**
     * Crop position
     *
     * @var string
     */
    protected $_cropPosition;
    /**
     * Adaptive resize flag
     *
     * @var bool
     */
    protected $_scheduleAdaptiveResize = false;
    /**
     * Reset all previous data
     *
     * @return Bolevar_AdaptiveResize_Helper_Image
     */
    protected function _reset()
    {
        $this->_scheduleAdaptiveResize = false;
        $this->_cropPosition = 0;
        parent::_reset();
    }
    /**
     * Adaptive resize method
     *
     * @param int      $width  image width
     * @param int|null $height image height
     *
     * @return \Bolevar_AdaptiveResize_Helper_Image
     */
    
    
     /**
     * @return $this
     */
   public function adaptiveResize($filename,$width, $height = null) {
        $_topRate = 0.5;
        $_bottomRate = 0.5;
        $_leftRate = 1;
        $_rightRate = 1;
        if (is_null($width)) {
            $width = "980";
        }
        if (is_null($height)) {
            $height = $width;
        }
        if ($filename) {
            $orig_img_path = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . $filename;
            $image_name = str_replace('wysiwyg/smartwave/blog/','',$filename); //echo $image_name;exit;
            $newFile =  Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . $width . 'x' . $height. DS . $image_name;
          
            if(!file_exists($newFile)){
            $imageObj = new Varien_Image($orig_img_path);
            $origheight = $imageObj->getOriginalHeight();
            $origwidth = $imageObj->getOriginalWidth();
            $currentRatio = $origwidth / $origheight;
            $targetRatio = $width / $height;
            //echo $targetRatio.'----'.$currentRatio,"<br>";
            if ($targetRatio > $currentRatio) {//echo 7;exit;
                if (true || !$this->fileExists($newFile)) {
                    $imageObj->resize($width, null);
                }
            } else { //echo 97;exit;
                if (true || !$this->fileExists($newFile)) {
                    $imageObj->resize(null, $height);
                }
            }
            $diffWidth = $origwidth - $width;
            $diffHeight = $origheight - $height;
            //echo floor($diffHeight * $_topRate), floor(($diffWidth / 2) * $_leftRate), ceil(($diffWidth / 2) * $_rightRate), ceil($diffHeight * $_bottomRate);
            $imageObj->crop(
                    floor($diffHeight * $_topRate), floor(($diffWidth / 2) * $_leftRate), ceil(($diffWidth / 2) * $_rightRate), ceil($diffHeight * $_bottomRate)
            );
           // $newFile = $this->_mediaDirectory->getAbsolutePath($newFile);
           // $base_url = Mage::getBaseUrl();
            //$newFile = str_replace('/var/www/html/',$base_url,$newFile);
            $imageObj->save($newFile);
           
            unset($imageObj);
        }
        }
        $newFilenm = Mage::getBaseUrl('media').DS.$width . 'x' . $height.DS.$image_name;
        return $newFilenm;
    }

  
    
}