<?php

require_once Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'CartController.php';

class VF_AjaxCart_CartController extends Mage_Checkout_CartController {

    public function updatePostAction() {
        $updateAction = (string) $this->getRequest()->getParam('update_cart_action');
        $easy_ajax = (string) $this->getRequest()->getParam('easy_ajax');
        if (!$easy_ajax) {
            if (!$this->_validateFormKey()) {
                $this->_redirect('*/*/');
                return;
            }
        }
        switch ($updateAction) {
            case 'empty_cart':
                $this->_emptyShoppingCart();
                break;
            case 'update_qty':
                $this->_updateShoppingCart();
                break;
            default:
                $this->_updateShoppingCart();
        }
        $this->_getSession()->setNoautored(true);
        $this->_goBack();
    }

    /**
     * Delete shoping cart item action
     */
    public function deleteAction() {
        $this->_getSession()->setNoautored(true);
        parent::deleteAction();
    }

}