<?php
 
class VES_VAdmin_Model_Observer
{
    /**
     * Hook that allows us to edit the form that is used to create and/or edit attributes.
     * @param Varien_Event_Observer $observer
     */
    public function addFieldToAttributeEditForm($observer)
    {
        // Add an extra field to the base fieldset:
        $fieldset = $observer->getForm()->getElement('base_fieldset');
        $fieldset->addField('background_color', 'text', array(
            'name' => 'background_color',
            'label' => Mage::helper('core')->__('Background Color'),
            'title' => Mage::helper('core')->__('Background Color'),
            'note' =>Mage::helper('core')->__('Ignore these values if not a nutritional attribute.')
        ));
        
        $fieldset->addField('unit', 'text', array(
            'name' => 'unit',
            'label' => Mage::helper('core')->__('Nutritional Unit'),
            'title' => Mage::helper('core')->__('Nutritional Unit'),
            'note' =>Mage::helper('core')->__('Ignore these values if not a nutritional attribute.')
        ));

    }
}