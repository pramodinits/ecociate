<?php

class Fedobe_Arccore_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * XML path to input types validator data in config
     */
    const XML_PATH_VALIDATOR_DATA_INPUT_TYPES = 'general/validator_data/input_types';

    protected $_attributesLockedFields = array();
    protected $_entityTypeFrontendClasses = array();

    /**
     * Return default frontend classes value labal array
     *
     * @return array
     */
    protected function _getDefaultFrontendClasses() {
        return array(
            array(
                'value' => '',
                'label' => Mage::helper('eav')->__('None')
            ),
            array(
                'value' => 'validate-number',
                'label' => Mage::helper('eav')->__('Decimal Number')
            ),
            array(
                'value' => 'validate-digits',
                'label' => Mage::helper('eav')->__('Integer Number')
            ),
            array(
                'value' => 'validate-email',
                'label' => Mage::helper('eav')->__('Email')
            ),
            array(
                'value' => 'validate-url',
                'label' => Mage::helper('eav')->__('URL')
            ),
            array(
                'value' => 'validate-alpha',
                'label' => Mage::helper('eav')->__('Letters')
            ),
            array(
                'value' => 'validate-alphanum',
                'label' => Mage::helper('eav')->__('Letters (a-z, A-Z) or Numbers (0-9)')
            )
        );
    }

    /**
     * Return merged default and entity type frontend classes value label array
     *
     * @param string $entityTypeCode
     * @return array
     */
    public function getFrontendClasses($entityTypeCode) {
        $_defaultClasses = $this->_getDefaultFrontendClasses();
        if (isset($this->_entityTypeFrontendClasses[$entityTypeCode])) {
            return array_merge(
                    $_defaultClasses, $this->_entityTypeFrontendClasses[$entityTypeCode]
            );
        }
        $_entityTypeClasses = Mage::app()->getConfig()
                ->getNode('global/eav_frontendclasses/' . $entityTypeCode);
        if ($_entityTypeClasses) {
            foreach ($_entityTypeClasses->children() as $item) {
                $this->_entityTypeFrontendClasses[$entityTypeCode][] = array(
                    'value' => (string) $item->value,
                    'label' => (string) $item->label
                );
            }
            return array_merge(
                    $_defaultClasses, $this->_entityTypeFrontendClasses[$entityTypeCode]
            );
        }
        return $_defaultClasses;
    }

    /**
     * Retrieve attributes locked fields to edit
     *
     * @param string $entityTypeCode
     * @return array
     */
    public function getAttributeLockedFields($entityTypeCode) {
        if (!$entityTypeCode) {
            return array();
        }
        if (isset($this->_attributesLockedFields[$entityTypeCode])) {
            return $this->_attributesLockedFields[$entityTypeCode];
        }
        $_data = Mage::app()->getConfig()->getNode('global/eav_attributes/' . $entityTypeCode);
        if ($_data) {
            foreach ($_data->children() as $attribute) {
                $this->_attributesLockedFields[$entityTypeCode][(string) $attribute->code] = array_keys($attribute->locked_fields->asArray());
            }
            return $this->_attributesLockedFields[$entityTypeCode];
        }
        return array();
    }

    /**
     * Get input types validator data
     *
     * @return array
     */
    public function getInputTypesValidatorData() {
        return Mage::getStoreConfig(self::XML_PATH_VALIDATOR_DATA_INPUT_TYPES);
    }

    /**
     * Retrieve attribute hidden fields
     *
     * @return array
     */
    public function getAttributeHiddenFields() {
        if (Mage::registry('attribute_type_hidden_fields')) {
            return Mage::registry('attribute_type_hidden_fields');
        } else {
            return array();
        }
    }

    /**
     * Retrieve attribute disabled types
     *
     * @return array
     */
    public function getAttributeDisabledTypes() {
        if (Mage::registry('attribute_type_disabled_types')) {
            return Mage::registry('attribute_type_disabled_types');
        } else {
            return array();
        }
    }

    public function getEntityTypeId() {
        $entitytypeid = Mage::getModel('eav/entity')->setType(Mage::getModel('eav/config')->getEntityType($this->getEntityType()))->getTypeId();
        return $entitytypeid;
    }

    public function getEntityType() {
        $entitytype = 'fedobe_arccore_arc';
        return $entitytype;
    }

    public function getEntityTypeIda($module = 'customerattributeset') {
        switch ($module) {
            case 'customerattributeset':
            case 'customer':
                $entitytypeid = Mage::getModel('eav/entity')->setType(Mage::getModel('eav/config')->getEntityType('customer'))->getTypeId();
                break;
            case 'arccore':
                $entitytypeid = Mage::getModel('eav/entity')->setType(Mage::getModel('eav/config')->getEntityType($this->getEntityType())->getTypeId());
                break;
            default :
                $entitytypeid = Mage::getModel('eav/entity')->setType(Mage::getModel('eav/config')->getEntityType('catalog_product'))->getTypeId();
        }
        return $entitytypeid;
    }

    public function getAttributeObject($module) {
        switch ($module) {
            case 'arccore':
                $attrobject = Mage::registry('arcattribute_data');
                break;
            default :
                $attrobject = Mage::registry('entity_attribute');
        }
        return $attrobject;
    }

    public function getAttributeSourceModelByInputType($inputType) {
        $inputTypes = $this->getAttributeInputTypes();
        if (!empty($inputTypes[$inputType]['source_model'])) {
            return $inputTypes[$inputType]['source_model'];
        }
        return null;
    }

    public function getBackendTypes($frontend_input) {
        $bcktype = '';
        switch ($frontend_input) {
            case 'password':
                $bcktype = 'varchar';
                break;
            case 'image':
                $bcktype = 'varchar';
                break;
            case 'content':
                $bcktype = 'text';
                break;
            default:
                $bcktype = '';
                break;
        }
        return $bcktype;
    }

    public function getAttributeBackendModelByInputType($inputType) {
        $inputTypes = $this->getAttributeInputTypes();
        if (!empty($inputTypes[$inputType]['backend_model'])) {
            return $inputTypes[$inputType]['backend_model'];
        }
        return null;
    }

    public function getAttributeInputTypes($inputType = null) {
        /**
         * @todo specify there all relations for properties depending on input type
         */
        $inputTypes = array(
            'multiselect' => array(
                'backend_model' => 'eav/entity_attribute_backend_array',
                'source_model' => 'eav/entity_attribute_source_table',
            ),
            'boolean' => array(
                'source_model' => 'eav/entity_attribute_source_boolean'
            ),
            'image' => array(
                'backend_model' => 'arccore/mysql4_arc_attribute_backend_image'
            )
        );

        if (is_null($inputType)) {
            return $inputTypes;
        } else if (isset($inputTypes[$inputType])) {
            return $inputTypes[$inputType];
        }
        return array();
    }

    public function addAttributeOption($attribute_code, $option_name) {
        $attr_id = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', $attribute_code);
        $option['attribute_id'] = $attr_id;
        $option['value']['any_option_name'][0] = $option_name;

        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttributeOption($option);
    }

    public function clearLayerRewrites() {
        Mage::getConfig()->setNode('modules/MageWorx_SeoSuite/active', 'false', true);
        Mage::getConfig()->setNode('global/models/catalog/rewrite/layer_filter_item', null, true);
        Mage::getConfig()->setNode('global/models/catalog/rewrite/layer_filter_attribute', null, true);
        Mage::getConfig()->setNode('global/models/catalog/rewrite/layer_filter_category', null, true);
        Mage::getConfig()->setNode('global/models/catalog_resource/rewrite/layer_filter_item', null, true);
        Mage::getConfig()->setNode('global/models/catalog_resource_eav_mysql4/rewrite/layer_filter_item', null, true);
        Mage::getConfig()->setNode('global/blocks/catalog/rewrite/product_list_toolbar', null, true);
        Mage::getConfig()->setNode('global/blocks/catalog/rewrite/layer_filter_attribute', null, true);

//        if ($this->isFishPigSeoInstalledAndActive()) {
//            Mage::helper('fseo/layer')->applyLayerRewrites();
//        }

        return $this;
    }

    function getLnt($zip) {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=
" . urlencode($zip) . "&sensor=false";
        $result_string = file_get_contents($url);
        $result = json_decode($result_string, true);
        $result1[] = $result['results'][0];
        $result2[] = $result1[0]['geometry'];
        $result3[] = $result2[0]['location'];
        return $result3[0];
    }

    function getDistance($zip1, $zip2) {
        $first_lat = $this->getLnt($zip1);
        $next_lat = $this->getLnt($zip2);
        $lat1 = $first_lat['lat'];
        $lon1 = $first_lat['lng'];
        $lat2 = $next_lat['lat'];
        $lon2 = $next_lat['lng'];
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +
                cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
                cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return ($miles * 1.609344); //return in km
    }

    public function haversine_sql($table, $latcol, $lngcol, $lat, $lng, $radius, $cols) {
        $output = "SELECT $cols , 
                    ( 3959 * acos( cos( radians( $lat ) ) * cos( radians( `$latcol` ) ) * cos( radians( `$lngcol` ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( `$latcol` ) ) ) ) AS distance
                    FROM `$table` HAVING distance <= $radius 
                    ORDER BY distance;";
        return $output;
    }

    public function haversine_sql_new($table, $lat, $lng, $radius, $cols, $attribute_set = NULL) {
        $output = "SELECT txt.attribute_id, ent.attribute_set_id,txt.entity_id ,( 3959 * acos( cos( radians($lat) ) * cos( radians( SUBSTRING(txt.`value`,1,POSITION(',' IN txt.`value`)-1) ) ) * cos( radians( SUBSTRING(txt.`value`,POSITION(',' IN txt.`value`)+1) ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( SUBSTRING(txt.`value`,1,POSITION(',' IN txt.`value`)-1) ) ) ) ) AS distance ,value FROM arc_entity_text as txt JOIN arc_entity as ent ON ent.entity_id=txt.entity_id HAVING distance <= $radius AND txt.attribute_id=203 AND ent.attribute_set_id=$attribute_set;";
        //echo $output;exit;
        return $output;
    }

    public function getGlobalSearchurl() {
        return Mage::getUrl('arccore/tribesearch/search');
    }

    public function getBaseCustomerLabel() {
        return $this->__('Early Customer');
    }

    public function getProfileUrl($urlkey) {
        $urlkey = "@$urlkey";
        $websites = Mage::app()->getWebsites();
        $stores = array();
        foreach ($websites as $wk => $wv) {
            $website_id = $wv->getId();
            break;
        }
        $website_id = (Mage::app()->getRequest()->getParam('website')) ? Mage::app()->getRequest()->getParam('website') : $website_id;
        $websites = Mage::getModel("core/website")->load($website_id);
        foreach ($websites->getStoreIds() as $sk => $sv) {
            $store_id = $sv;
            break;
        }
        $storeid = (Mage::app()->getRequest()->getParam('store')) ? Mage::app()->getRequest()->getParam('store') : $store_id;
        $use_store_code_in_url = Mage::getStoreConfig('web/url/use_store', $storeid);
        if ($use_store_code_in_url) {
            $store_code = Mage::getModel('core/store')->load($storeid)->getCode();
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $store_code . '/' . $urlkey;
        } else {
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $urlkey;
        }
    }

    public function getEntityAttributeSetId() {
        $attrsetId = (int) Mage::app()->getRequest()->getParam('set');
        if ($attrsetId) {
            $setId = $attrsetId;
        } else {
            $setId = Mage::getModel('eav/entity_attribute_set')->getCollection()
                            ->setEntityTypeFilter($this->getEntityTypeId())->getFirstItem()->getAttributeSetId();
        }
//        }
        return $setId;
    }

    public function getvalidateurlkeyaction() {
        $controllername = Mage::app()->getRequest()->getControllerName();
        return Mage::helper("adminhtml")->getUrl("*/$controllername/checkurlkey");
    }

    public function getCustomerInfourlkeyaction() {
        $controllername = Mage::app()->getRequest()->getControllerName();
        return Mage::helper("adminhtml")->getUrl("*/$controllername/getCustomerInfo");
    }

    public function resizeImg($fileName, $width, $height = '',$aspect_ratio = FALSE ) {
        $dimension_slug = ($width && $height) ? $width . "_" . $height : $width;
        $folderURL = Mage::getBaseUrl('media') . 'arccore/arcs/images';
        $imageURL = $folderURL . $fileName;
        $basePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'arccore/arcs/images' . $fileName;
        $newPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . "arccore/arcs/images/resized/$dimension_slug" . DS . $fileName;
        //if width empty then return original size image's URL
        if ($width != '') {
            //if image has already resized then just return URL
            if (file_exists($basePath) && is_file($basePath) && !file_exists($newPath)) {
                $imageObj = new Varien_Image($basePath);
                $imageObj->constrainOnly(TRUE);
                $imageObj->keepAspectRatio($aspect_ratio);
                $imageObj->keepFrame(FALSE);
                $imageObj->resize($width, $height);
                $imageObj->save($newPath);
            }
            $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "arccore/arcs/images/resized/$dimension_slug" . $fileName;
        } else {
            $resizedURL = $imageURL;
        }
        return $resizedURL;
    }

    public function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function all_attributeset() {
        $attribute_set = array();
        $attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection')->load();
        foreach ($attributeSetCollection as $id => $attributeSet) { //print_r($attributeSet);exit;
            $entityTypeId = $attributeSet->getAttributeSetId();
            $name = $attributeSet->getAttributeSetName();
            $attribute_set[$name] = $entityTypeId;
        }
        return $attribute_set;
    }

    public function attribute_options($attribute_code) {
        $attribute = Mage::getModel('eav/config')->getAttribute('fedobe_arccore_arc', $attribute_code);
        $allOptions = $attribute->getSource()->getAllOptions(true, true);
        return $allOptions;
    }

    public function allattributes_ofset($attributesetid) {
        $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
                ->setAttributeSetFilter($attributesetid)
                ->addFieldToFilter('attribute_group_name', array('neq' => 'General'))
                ->setSortOrder()
                ->load();
        $attributes = array();
        foreach ($groupCollection as $group) {
            $attributeCollection = Mage::getResourceModel('eav/entity_attribute_collection')
                    ->setAttributeGroupFilter($group->getId());
            foreach ($attributeCollection as $attribute) {
                $attributes[] = $attribute->getAttributeCode();
            }
        }
        return $attributes;
    }

    public function getProfileInfourlkeyaction() {
        $controllername = Mage::app()->getRequest()->getControllerName();
        return Mage::helper("adminhtml")->getUrl("*/$controllername/getProfiledata");
    }

    public function getFormAttributes($attribute_set_id, $exclude = array()) {
        $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
                ->setAttributeSetFilter($attribute_set_id)
                ->addFieldToFilter('attribute_group_name', array('neq' => 'General'))
                ->setSortOrder()
                ->load();
        $attributes = array();
        foreach ($groupCollection as $group) {
            $attributeCollection = Mage::getResourceModel('eav/entity_attribute_collection')
                    ->setAttributeGroupFilter($group->getId());
            foreach ($attributeCollection as $attribute) {
                if (!in_array($attribute->getAttributeCode(), $exclude)) {
                    if ($attribute->getFrontendInput() == 'textarea') {
                        $attr_id = $attribute->getAttributeId();
                        $addtionaldata = Mage::getModel('arccore/attributedata')->load($attr_id);
                        $attributes[$group->getAttributeGroupName()][] = array_merge($attribute->getData(), $addtionaldata->getData());
                    } else {
                        $attributes[$group->getAttributeGroupName()][] = $attribute->getData();
                    }
                }
            }
        }
        return $attributes;
    }
    
    public function getImageAttributeCodes($attribute_set_id) {
        $attrcodes = array();
        $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
                ->setAttributeSetFilter($attribute_set_id)
                ->addFieldToFilter('attribute_group_name', array('neq' => 'General'))
                ->setSortOrder()
                ->load();
        foreach ($groupCollection as $group) {
            $attributeCollection = Mage::getResourceModel('eav/entity_attribute_collection')
                    ->setAttributeGroupFilter($group->getId());
            foreach ($attributeCollection as $attribute) {
                    if ($attribute->getFrontendInput() == 'image') {
                        $attrcodes[] = $attribute->getAttributeCode();
                }
            }
        }
        return $attrcodes;
    }
    
    

    public function getattributeoptions($attribute_code) {
        $data_option = array();
        $attr_obj = Mage::getModel('eav/config')->getAttribute('fedobe_arccore_arc', $attribute_code);
        $data_option = $attr_obj->getSource()->getAllOptions(false); //if(Mage::app()->getRequest()->getModuleName()=='tribecustomer'){echo $attribute_code;}
        return $data_option;
    }

    public function getattributeoptionsid($attribute_code, $option) {
        $options = $this->getattributeoptions($attribute_code);
        $options_val_id = array_combine(array_column($options, 'label'), array_column($options, 'value'));
        $id = $options_val_id[$option];
        return $id;
    }

    public function getattributeid($entity, $attribute_code) {
        $attribute = Mage::getModel('eav/config')->getAttribute($entity, $attribute_code);
        $id = $attribute->getId();
        return $id;
    }
     public function getAllposttype(){
      $post_type_attributesets=array();
      $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
      $trcr_post_type = $this->getattributeid('fedobe_arccore_arc','trcr_post_type');
      $data = $readConnection->fetchAll("SELECT * FROM `eav_entity_attribute` WHERE attribute_id = $trcr_post_type");
      $post_type_attributesets = array_column($data,'attribute_set_id');
      return $post_type_attributesets;
  } 
    
public function checkemailidlinked($email) {
        $customers = array();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $tableName = $resource->getTableName('customer_entity');
        $profiles = array();
        $allposttypes = $this->getAllposttype();
        $id = Mage::app()->getRequest()->getParam('id');
        $query = "SELECT `entity_id`,`email` FROM $tableName WHERE email='".$email."'";
        $customerdata = $readConnection->fetchAll($query);
        
        $adminuserid = Mage::getResourceModel('arccore/arc')->getAdminUserId($email, $email);
        //$adminusercheck = $this->checkAdminUser($email, $email, $adminuserid); return $adminusercheck;exit;
        if ($adminuserid || !empty($customerdata)) {
           $ret = 0;
        } else {
            $ret = 1;
        }
        return $ret;
    }
    public function checkAdminUser($username, $email, $userid = null) {
        //Here let's check user exeists or not
        $user = Mage::getModel('admin/user')->getCollection()
                ->addFieldToSelect('user_id')
                ->addFieldToFilter(
                array('username', 'email'), array(
            array('eq' => $username),
            array('eq' => $email)
                )
        );
        if ($userid) {
            $user->addFieldToFilter(array('user_id'), array(array('neq' => $userid)));
        }
        if (!$user->getData()) {
            return false;
        } else {
            return true;
        }
    }
}