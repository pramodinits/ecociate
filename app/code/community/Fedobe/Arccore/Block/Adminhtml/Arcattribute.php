<?php

/**
 * Grid container file
 * 
 * @category    Fedobe
 * @package     Fedobe_Arccore
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Arccore_Block_Adminhtml_Arcattribute extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_arcattribute';
        $this->_blockGroup = 'arccore';
        $this->_headerText = Mage::helper('arccore')->__('Manage Arc Attributes');
        $this->_addButtonLabel = Mage::helper('arccore')->__('Add New Arc Attribute');
         $this->setSaveParametersInSession(true);
        parent::__construct();
    }

}