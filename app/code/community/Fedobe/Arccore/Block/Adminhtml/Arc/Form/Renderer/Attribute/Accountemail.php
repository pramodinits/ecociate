<?php

class Fedobe_Arccore_Block_Adminhtml_Arc_Form_Renderer_Attribute_Accountemail extends Fedobe_Arccore_Block_Adminhtml_Arc_Form_Renderer_Fieldset_Element {

    public function getElementHtml() {
        $element = $this->getElement();
        $id = $element->getId();
        $entity_id = (int) Mage::app()->getRequest()->getParam('id');
        $html = parent::getElementHtml();
        $controllername = 'arc';
        $url = Mage::helper("adminhtml")->getUrl("*/$controllername/isemailexist");
        $html .= "<div id='email_err'></div><script type='text/javascript'>
                   $('trcr_email').on('blur',function(){
                   var custurl = '".$url."';
                  var email = jQuery(this).val();
                  if(email)
                  matchcustomeremailid(custurl,email);
                    });
                   </script>";
        return $html;
    }

}