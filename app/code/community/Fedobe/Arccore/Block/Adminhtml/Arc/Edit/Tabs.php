<?php

/**
 * Manage Seller Attribute controller file
 * 
 * @category    Fedobe
 * @package     Fedobe_Arcrcore
 * @author      Fedobe Magento Team
 */
class Fedobe_Arccore_Block_Adminhtml_Arc_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    protected $_attributeTabBlock = 'arccore/adminhtml_arc_edit_tab_attributes';

    public function __construct() {
        parent::__construct();
        $this->setId('profile_info_tabs');
        $this->setDestElementId('arc_edit_form');
        $this->setTitle(Mage::helper('arccore')->__('Information'));
    }

    protected function _prepareLayout() {
        $entityTypeId = Mage::helper('arccore')->getEntityTypeId();
        $setId = Mage::helper('arccore')->getEntityAttributeSetId();//echo $setId;exit;
        $storeId = 0;
        if ($this->getRequest()->getParam('store')) {
            $storeId = Mage::app()->getStore($this->getRequest()->getParam('store'))->getId();
        }
        if ($setId) {
            $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
                    ->setAttributeSetFilter($setId)
                    ->addFieldToFilter('attribute_group_name',array('neq'=>'General'))
                    ->setSortOrder()
                    ->load(); //print_r($groupCollection);exit;
            foreach ($groupCollection as $group) {
                $attributeCollection = Mage::getResourceModel('eav/entity_attribute_collection')
                        ->setAttributeGroupFilter($group->getId());
                $attributes = array();
                foreach ($attributeCollection as $attribute) {
                    $attributes[] = $attribute;
                }//echo "<pre>";print_r($attributes);exit;
                if ($group->getAttributeGroupName() == 'Meta Information') {
                    $this->addTabAfter('group_' . $group->getId(), array(
                        'label' => Mage::helper('arccore')->__($group->getAttributeGroupName()),
                        'class' => Mage::helper('arccore')->__($group->getAttributeGroupName()),
                        'content' => $this->_translateHtml($this->getLayout()->createBlock($this->getAttributeTabBlock(), 'arccore.adminhtml.arc.edit.tab.attributes')->setGroup($group)
                                        ->setGroupAttributes($attributes)
                                        ->setStoreId($storeId)
                                        ->setAttributeSetId($setId)
                                        ->setTypeId($entityTypeId)
                                        ->toHtml()),
                            ), 'products');
                } else { //echo 567;exit;
                    $this->addTab('group_' . $group->getId(), array(
                        'label' => Mage::helper('arccore')->__($group->getAttributeGroupName()),
                        'class' => Mage::helper('arccore')->__($group->getAttributeGroupName()),
                        'content' => $this->_translateHtml($this->getLayout()->createBlock($this->getAttributeTabBlock(), 'arccore.adminhtml.arc.edit.tab.attributes')
                                        ->setGroup($group)
                                        ->setGroupAttributes($attributes)
                                        ->setStoreId($storeId)
                                        ->setAttributeSetId($setId)
                                        ->setTypeId($entityTypeId)
                                        ->toHtml()),
                    ));
                }
            }
        }

        return parent::_prepareLayout();
    }

    /**
     * Getting attribute block name for tabs
     *
     * @return string
     */
    public function getAttributeTabBlock() {
        return $this->_attributeTabBlock;
    }

    public function setAttributeTabBlock($attributeTabBlock) {
        $this->_attributeTabBlock = $attributeTabBlock;
        return $this;
    }

    /**
     * Translate html content
     *
     * @param string $html
     * @return string
     */
    protected function _translateHtml($html) {
        Mage::getSingleton('core/translate_inline')->processResponseBody($html);
        return $html;
    }

}
