<?php

/**
 * Customer attribute edit block
 * 
 * @category    Fedobe
 * @package     Fedobe_Arccore
 * @author      Fedobe Magento Team
 */
class Fedobe_Arccore_Block_Adminhtml_Arc_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        $this->_controller = 'profiles';
        $this->_blockGroup = 'arccore';
        parent::__construct();
        $this->setTemplate('fedobe/arccore/edit.phtml');
        $this->setId('arc_edit_form');
        $store_id = ($this->getRequest()->getParam('store')) ? $this->getRequest()->getParam('store') : Mage_Core_Model_App::ADMIN_STORE_ID;
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                var tab = $$('.tabs')[0].down('a.active').readAttribute('id');
                arc_edit_form.submit($('arc_edit_form').action+'back/edit/store/{$store_id}/tab/'+tab);
            }
        ";
    }

    protected function _prepareLayout(){
        $this->_updateButton('save', 'label', Mage::helper('arccore')->__('Save'));
        $this->_updateButton('save', 'onclick', 'arc_edit_form.submit()');
      //  $data = Mage::registry('arcprofile_data')->getData();
        if (isset($data['id']) && !$data['id']) {
            $this->_removeButton('delete');
        } else {
            $this->_updateButton('delete', 'label', Mage::helper('arccore')->__('Delete'));
        }
        $this->_addButton(
                'saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
                ), 100
        );
    }
    /**
     * This function return’s the Text to display as the form header.
     */
    public function getHeaderText() {
        $data = Mage::registry('arcprofile_data')->getData();
        if (isset($data['entity_id']) && $data['entity_id']) {
            $frontendLabel = Mage::registry('arcprofile_data')->getTrcrName();
            if (is_array($frontendLabel)) {
                $frontendLabel = $frontendLabel[0];
            }
            return "<div class='icon-head head-products'>".Mage::helper('arccore')->__($frontendLabel, $this->escapeHtml($frontendLabel))." ( ".$this->getAttributeSetName($data['attribute_set_id'])." )"."</div>";
        } else {
           // return Mage::helper('arccore')->__('New ')." ( ".$this->getAttributeSetName($data['attribute_set_id'])." )";


$new_var = $this->getAttributeSetName($data['attribute_set_id']);
 
$nw = str_replace("Tribe","",$new_var);
return Mage::helper('arccore')->__('New ').$nw;
        }
    }

    public function getAttributeSetName($setid) {
        $attrsetid = ($setid) ? $setid : Mage::registry('arcprofile_data')->getAttributeSetId();
       $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
        $attributeSetModel->load($attrsetid);
        return $this->escapeHtml($attributeSetModel->getAttributeSetName());
    }
    
    public function getValidationUrl() {
        return $this->getUrl('*/*/validate', array('_current' => true));
    }

    public function getSaveUrl() {
        return $this->getUrl('*/' . $this->_controller . '/save', array('_current' => true, 'back' => null));
    }

    public function getSelectedTabId()
    {
        return addslashes(htmlspecialchars($this->getRequest()->getParam('tab')));
    }

}