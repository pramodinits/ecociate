<?php

/**
 * Manage Arc Attribute controller file
 * 
 * @category    Fedobe
 * @package     Fedobe_Arccore
 * @author      Fedobe Magento Team
 */
class Fedobe_Arccore_Block_Adminhtml_Arc_Edit_Tab_Attributes extends Fedobe_Arccore_Block_Adminhtml_Arc_Form {

    /**
     * Load Wysiwyg on demand and prepare layout
     */
    protected function _prepareLayout() {
        parent::_prepareLayout();
        $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
    }

    /**
     * Prepare attributes form
     *
     * @return null
     */
    protected function _prepareForm() {
        $group = $this->getGroup();
        if ($group) {
            $form = new Varien_Data_Form();
            $fieldset = $form->addFieldset('group_fields' . $group->getId(), array(
                'legend' => Mage::helper('arccore')->__($group->getAttributeGroupName()),
                'class' => 'fieldset-wide'
            ));
            $attributes = $this->getGroupAttributes();
            $this->_setFieldset($attributes, $fieldset, array('gallery'));


            //Here adding customconfiguration for account tab settings
            //Please do not edit any attribute code used here
            $trcr_first_name = $form->getElement('trcr_first_name');
            if ($trcr_first_name)
                $trcr_first_name->setClass("new-customers");
            $trcr_last_name = $form->getElement('trcr_last_name');
            if ($trcr_last_name)
                $trcr_last_name->setClass("new-customers");
            $trcr_email = $form->getElement('trcr_email');
            if ($trcr_email)
                $trcr_email->setClass("new-customers");
            $trcr_password = $form->getElement('trcr_password');
            if ($trcr_password)
                $trcr_password->setClass("new-customers");

            $tribe_customer_type = $form->getElement('trcr_account_type');
            if ($tribe_customer_type) {
                $tribe_customer_type->setOnchange("setCustomertype(this)");
            }
            $all_customers = $form->getElement('trcr_customer_list');
            if ($all_customers) {
                $all_customers->setOnchange("changeCustomer(this)");
            }
            //End


            $values = Mage::registry('arcprofile_data')->getData(); //print_r($values);exit;
            // Set default attribute values for new product
            if (!isset($values['id'])) {
                foreach ($attributes as $attribute) {
                    $attr_id = $attribute->getAttributeId();
                    $addtionaldata = Mage::getModel('arccore/attributedata')->load($attr_id);
                    $inputrenderer = $addtionaldata->getData('frontend_input_renderer');
                    if (strlen($inputrenderer) > 15) {
                        $form->getElement($attribute->getAttributeCode())->setRenderer($this->getLayout()->createBlock($inputrenderer));
                    }
                    if (!isset($values[$attribute->getAttributeCode()])) {
                        $values[$attribute->getAttributeCode()] = $attribute->getDefaultValue();
                    }
                }
            } //echo "<pre>";print_r($attributes);exit;
//exit;
            if (isset($values['locked_attributes'])) {
                foreach ($values['locked_attributes'] as $attribute) {
                    $element = $form->getElement($attribute);
                    if ($element) {
                        $element->setReadonly(true, true);
                    }
                }
            }

            $form->addValues($values);
            $form->setFieldNameSuffix('arc');
            if (strcasecmp($group->getAttributeGroupName(), 'Arc') === 0 && $values['id']) {
                $form->addType('script', 'Fedobe_Arccore_Block_Adminhtml_Script');
                $form->addField("", 'script', array());
            }
            Mage::dispatchEvent('adminhtml_arc_product_edit_prepare_form', array('form' => $form));
            $this->setForm($form);
        }
    }

    /**
     * Retrieve additional element types
     *
     * @return array
     */
    protected function _getAdditionalElementTypes() {
        $result = array(
            'map' => 'Fedobe_Arccore_Block_Adminhtml_Arc_Form_Element_Map',
            'content' => 'Fedobe_Arccore_Block_Adminhtml_Arc_Form_Element_Content',
            'gallery' => 'Fedobe_Arccore_Block_Adminhtml_Arc_Form_Element_Gallery',
            'image' => 'Fedobe_Arccore_Block_Adminhtml_Arc_Form_Element_Image',
            'boolean' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_boolean'),
            'textarea' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_helper_form_wysiwyg'),
            'price' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_price'),
            'weight' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_weight'),
        );

        $response = new Varien_Object();
        $response->setTypes(array());
        Mage::dispatchEvent('adminhtml_tribe_arccore_edit_element_types', array('response' => $response));

        foreach ($response->getTypes() as $typeName => $typeClass) {
            $result[$typeName] = $typeClass;
        }
        return $result;
    }

}