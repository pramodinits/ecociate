<?php

class Fedobe_Arccore_Block_Adminhtml_Arc_Form_Element_Map extends Varien_Data_Form_Element_Abstract {

    public function __construct($attributes = array()) {
        parent::__construct($attributes);
        $this->setType('tribemap');
    }

    public function getElementHtml() {
        $this->addClass('arccore tribemap');
        $html = '';
        $name = parent::getName();
        $value = $this->getValue();
        $data = array(
            'id'=>$this->getHtmlId(),
            'name'=>$name,
            'value'=>$value,
        );
        $html .= $this->getAfterElementHtml();
        $html .= $this->_googlemapjavascriptcode($data);
        return $html;
    }

    public function getDefaultHtml() {
        $this->addClass('arccore tribemap');
        $html = '';
        $data = array(
            'id'=>$this->getHtmlId(),
            'name'=>$name,
            'value'=>$value,
        );
        $html .= $this->getAfterElementHtml();
        $html .= $this->_googlemapjavascriptcode($data);
        return $html;
    }

    protected function _googlemapjavascriptcode($data){
       return Mage::app()->getLayout()->createBlock('core/template')->setElementdata($data)->setTemplate('fedobe/arccore/form/element/map.phtml')->toHtml();
    }

}