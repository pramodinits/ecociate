<?php

class Fedobe_Arccore_Block_Adminhtml_Arc_Form_Renderer_Attribute_Profiletype extends Fedobe_Arccore_Block_Adminhtml_Arc_Form_Renderer_Fieldset_Element {

    public function getElementHtml() {
        $element = $this->getElement();
        $id = $element->getId();
        $html = parent::getElementHtml();
        $helper = str_replace("_", '', Mage::app()->getRequest()->getControllerName());
        $type = Mage::helper($helper)->getProfileType();
        $html .= "<script type='text/javascript'>document.observe('dom:loaded', function() {
                            $$('#$id option').each(function(element) {
                                if(element.innerHTML == '$type'){
                                    element.selected = true;
                                }else{
                                    element.disabled = true;
                                }
                            })                    
                    });</script>";
        return $html;
    }

}