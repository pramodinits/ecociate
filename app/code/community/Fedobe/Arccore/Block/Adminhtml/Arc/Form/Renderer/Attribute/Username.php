<?php

class Fedobe_Arccore_Block_Adminhtml_Arc_Form_Renderer_Attribute_Username extends Fedobe_Arccore_Block_Adminhtml_Arc_Form_Renderer_Fieldset_Element {

    public function getElementHtml() {
        $element = $this->getElement();
        $actionurl = Mage::helper('arccore')->getvalidateurlkeyaction();
        $cusurl = Mage::helper('arccore')->getCustomerInfourlkeyaction();
        $id = Mage::app()->getRequest()->getParam('id')?Mage::app()->getRequest()->getParam('id'):0;
        $element->setOnkeyup("onStoreNameChanged('".$id."','" . $element->getHtmlId() .  "','".$actionurl."')");
        $element->setOnchange("onStoreNameChanged('".$id."','" . $element->getHtmlId() .  "','".$actionurl."')");
        $username = Mage::registry('arcprofile_data')->getTrcrUsername();
        $html  = parent::getElementHtml()."<div id='trcr_username_msg'></div><div id='trcr_username_url'></div>";
        $html .="<script type='text/javascript'>var customerurl = '$cusurl'; var sellerid = $id;</script>";
        if($username){
            $helper = str_replace("_",'',Mage::app()->getRequest()->getControllerName());
            $sellerurl = Mage::helper($helper)->getFrontendProductUrl($username);
            $urlkey = '<div class="sellermsg" style="color:#3d6611 !important"><a href="'.$sellerurl.'">'.$sellerurl.'</a></div>';
            $html .= "<script type='text/javascript'>document.observe('dom:loaded', function() {
                            $('trcr_username_url').update('$urlkey');
                    });</script>";
        }
        return $html;
    }

}