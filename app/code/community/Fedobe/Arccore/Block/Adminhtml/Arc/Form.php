<?php

class Fedobe_Arccore_Block_Adminhtml_Arc_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareLayout() {
        Varien_Data_Form::setElementRenderer(
            $this->getLayout()->createBlock('adminhtml/widget_form_renderer_element')
        );
        Varien_Data_Form::setFieldsetRenderer(
            $this->getLayout()->createBlock('adminhtml/widget_form_renderer_fieldset')
        );
        Varien_Data_Form::setFieldsetElementRenderer(
            $this->getLayout()->createBlock('arccore/adminhtml_arc_form_renderer_fieldset_element')
        );
    }

}
