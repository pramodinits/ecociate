<?php

class Fedobe_Arccore_Block_Adminhtml_Arcattribute_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
 
  public function __construct()
  {
      parent::__construct();
      $this->setId('arcattribute_tabs');
      $this->setDestElementId('edit_form'); // this should be same as the form id define above
      $this->setTitle(Mage::helper('arccore')->__('Attribute Information'));
  }
  
 /**
  * Specified customer attribute edit page tabs
  */
  protected function _beforeToHtml()
  {
      $this->addTab('main', array(
            'label'     => Mage::helper('arccore')->__('Properties'),
            'title'     => Mage::helper('arccore')->__('Properties'),
            'content'   => $this->getLayout()->createBlock('arccore/adminhtml_arcattribute_edit_tab_main')->toHtml(),
            'active'    => true
        ));
      
      $model = Mage::registry('arcattribute_data');

        $this->addTab('labels', array(
            'label'     => Mage::helper('arccore')->__('Manage Label / Options'),
            'title'     => Mage::helper('arccore')->__('Manage Label / Options'),
            'content'   => $this->getLayout()->createBlock('arccore/adminhtml_arcattribute_edit_tab_options')->toHtml(),
        ));
      
      return parent::_beforeToHtml();
  }
}