<?php

/**
 * Customer attribute add/edit form main tab
 * 
 * @category    Fedobe
 * @package     Fedobe_Arccore
 * @author      Fedobe Magento Team
 */
class Fedobe_Arccore_Block_Adminhtml_Arcattribute_Edit_Tab_Main extends Mage_Adminhtml_Block_Catalog_Product_Attribute_Edit_Tab_Main {

    public function getAttributeObject() {
        return Mage::helper('arccore')->getAttributeObject('arccore');
    }

    protected function _prepareForm() {
        parent::_prepareForm();
        $form = $this->getForm();
        $data = Mage::registry('arceavattribute_data');
        $fieldset = $form->getElement('base_fieldset');
        $fieldset->removeField('apply_to');
        $frontendproperties = $form->getElement('front_fieldset');
        if ($data[0]['frontend_input_renderer'] == 'password') {
            $fieldset->removeField('is_unique');
        }
        $frontendproperties->removeField('is_comparable');
        $frontendproperties->removeField('is_visible_on_front');
        $frontendproperties->removeField('used_in_product_listing');
        $frontendproperties->removeField('used_for_sort_by');
        $additionalTypes = array(array(
                'value' => 'password',
                'label' => Mage::helper('catalog')->__('Password')
            ), array(
                'value' => 'image',
                'label' => Mage::helper('catalog')->__('Image')
            ), array(
                'value' => 'content',
                'label' => Mage::helper('catalog')->__('Content')
            )
        );

        $frontendInputElm = $form->getElement('frontend_input');
        $frontendInputValues = array_merge($frontendInputElm->getValues(), $additionalTypes);
        $frontendInputElm->setValues($frontendInputValues);
        $form->setValues($data[0]);
        return $this;
    }

    protected function _afterToHtml($html) {
        $jsScripts = $this->getLayout()
                        ->createBlock('eav/adminhtml_attribute_edit_js')->toHtml();
        $data = Mage::registry('arceavattribute_data');
        if ($data[0]['frontend_input_renderer'] == 'password') {
            $jsScripts.= '<script type="text/javascript">window.onload = function(){
                            $("front_fieldset").previous().hide();
                            $("front_fieldset").hide();
                            }</script>';
        }
        $jsScripts.= '<script type="text/javascript">Event.observe("frontend_input", "change", function(){'
                . 'if($(this).value == "password"){ setTimeout(function(){$("front_fieldset").previous().hide();$("front_fieldset").hide();},50);'
                . '}else{'
                . '$("front_fieldset").previous().show();'
                . '$("front_fieldset").show();'
                . '}'
                . '});</script>';
        return $html . $jsScripts;
    }

}