<?php

class Fedobe_Arccore_Block_Adminhtml_Arccore_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    protected $_entitytypeid = null;
    protected $_attributesetid = null;

    public function __construct() {
        parent::__construct();
    }

    public function setEntityId($entityid) {
        $this->_entitytypeid = $entityid;
    }

    public function getEntityId() {
        if($this->_entitytypeid){
            $entitytypeid = $this->_entitytypeid;
        }else{
            $entitytypeid = Mage::helper('arccore')->getEntityTypeId('arccore');
        }
        return $entitytypeid;
    }

    public function setAttributeSetId($attributesetid) {
        $this->_attributesetid = $attributesetid;
    }

    public function getAttributeSetId() {
        return $this->_attributesetid;
    }

    /**
     * Prepare customer attributes grid collection object
     *
     * @return Fedobe_Arccore_Block_Adminhtml_Arccore_Grid
     */
    protected function _prepareCollection() {
        $collection = Mage::getModel('arccore/arc')->getCollection()
                ->addAttributeToSelect('*');
        if ($this->getEntityId()) {
            $collection->addAttributeToFilter('entity_type_id', $this->getEntityId());
        }
        if ($this->getAttributeSetId()) {
            $collection->addAttributeToFilter('attribute_set_id', $this->getAttributeSetId());
        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getStatus() {
        return Mage::getModel('arccore/arc_attribute_status')->getAllOptions();
    }

    protected function _prepareColumns() {
        if (!$this->getColumnCount()) {
            $this->addColumn('entity_id', array(
                'header' => Mage::helper('arccore')->__('ID'),
                'width' => '50px',
                'type' => 'number',
                'index' => 'entity_id',
            ));
            $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
                    ->setEntityTypeFilter($this->getEntityId())
                    ->load()
                    ->toOptionHash();
            $this->addColumn('attribute_set_id', array(
                'header' => Mage::helper('arccore')->__('Type'),
                'width' => '50px',
                'type' => 'options',
                'options' => $sets,
                'index' => 'attribute_set_id',
            ));
            $this->addColumn('is_active', array(
                'header' => Mage::helper('arccore')->__('Status'),
                'width' => '80px',
                'index' => 'is_active',
                'type' => 'options',
                'options' => $this->getStatus(),
            ));

            $this->addColumn('created_at', array(
                'header' => Mage::helper('arccore')->__('Created At'),
                'index' => 'created_at',
            ));
            $this->addColumn('action', array(
                'header' => Mage::helper('arccore')->__('Action'),
                'width' => '50px',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('arccore')->__('Edit'),
                        'url' => array(
                            'base' => '*/*/edit',
                            'params' => array('store' => $this->getRequest()->getParam('store'))
                        ),
                        'field' => 'id'
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
            ));
        }
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('arc');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('arccore')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('arccore')->__('Are you sure?')
        ));
        $statuses = $this->getStatus();
        array_unshift($statuses, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('arccore')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('arccore')->__('Status'),
                    'values' => $statuses
                )
            )
        ));
        Mage::dispatchEvent('arccore_adminhtml_arccore_grid_prepare_massaction', array('block' => $this));
        return $this;
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/index', array('_current' => true));
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array(
                    'store' => $this->getRequest()->getParam('store'),
                    'set' => $this->getAttributeSetId(),
                    'id' => $row->getId())
        );
    }

}