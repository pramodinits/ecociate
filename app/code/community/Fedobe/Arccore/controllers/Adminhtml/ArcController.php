<?php

/**
 * Manage Arc Attribute controller file
 * 
 * @category    Fedobe
 * @package     Fedobe_Arccore
 * @author      Fedobe Magento Team
 */
class Fedobe_Arccore_Adminhtml_ArcController extends Mage_Adminhtml_Controller_Action {

    protected $_entityTypeId;

    public function preDispatch() {
        parent::preDispatch();
        $this->_entityTypeId = Mage::helper('arccore')->getEntityTypeId();
    }

    /**
     * Init actions
     *
     */
    protected function _initAction() {
        // load layout, set active menu and breadcrumbs
        $this->_title($this->__('Tribe'))
                ->_title($this->__('Manage Profiles'));

        if ($this->getRequest()->getParam('popup')) {
            $this->loadLayout('popup');
        } else {
            $this->loadLayout()
                    ->_setActiveMenu('fedobe/arccore_manage_arc');
        }
        return $this;
    }

    /**
     * Index action method
     */
    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }
     public function newAction() {
        $attrsetid = $this->getRequest()->getParam('set');
        if ($attrsetid) {
            $this->_forward('edit');
        } else {
            $this->loadLayout();
            $this->renderLayout();
        }
    }

    public function checkaddonAction() {
        $attributeset = $this->getRequest()->getParam('set');
        $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
        $attributeSetModel->load($attributeset);
        $attributeSetName  = $attributeSetModel->getAttributeSetName();
       // print_r($attributeSetName);exit;
        /*if ($attributeSetName == 'Tribe Seller') {
            $module = 'Fedobe_Tribeseller';
            $addon_enabled = Mage::helper('core')->isModuleEnabled($module); //echo $addon_enabled;exit;
            if ($addon_enabled) {
                $this->_redirect('adminhtml/sellerprofiles/new/set/'.$attributeset);
            } 
        }*/
    }
    
     public function isemailexistAction(){
        $data=array();
        $email_id = $this->getRequest()->getPost('email');
        $ret = Mage::helper('arccore')->checkemailidlinked($email_id);
        $data['ret'] = $ret;
        echo Mage::helper('core')->jsonEncode($data);
    }

}
