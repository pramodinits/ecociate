<?php

/**
 * Manage Arc Attribute controller file
 * 
 * @category    Fedobe
 * @package     Fedobe_Arccore
 * @author      Fedobe Magento Team
 */
class Fedobe_Arccore_Adminhtml_ArcattributeController extends Mage_Adminhtml_Controller_Action {

    protected $_entityTypeId;

    public function preDispatch() {
        parent::preDispatch();
        $this->_entityTypeId = Mage::helper('arccore')->getEntityTypeId('arccore');
    }

    /**
     * Init actions
     *
     */
    protected function _initAction() {
        // load layout, set active menu and breadcrumbs
        $this->_title($this->__('Arc'))
                ->_title($this->__('Attributes'))
                ->_title($this->__('Manage Arc Attributes'));

        if ($this->getRequest()->getParam('popup')) {
            $this->loadLayout('popup');
        } else {
            $this->loadLayout()
                    ->_setActiveMenu('fedobe/arccore_arc_attributes_manage_attr');
        }
        return $this;
    }

    /**
     * Index action method
     */
    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }

    /**
     * Code to display the form
     * 
     * The main form container gets added to the content and the tabs block gets added to left.
     */
    public function newAction() {
        // the same form is used to create and edit
        $this->_forward('edit');
    }

    /**
     * Edit Arc Attribute
     */
    public function editAction() {
        $id = $this->getRequest()->getParam('attribute_id');
        $model = Mage::getModel('eav/entity_attribute')
                ->setEntityTypeId($this->_entityTypeId);
        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('arccore')->__('This employee no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }
        // entity type check
        if ($model->getEntityTypeId() != $this->_entityTypeId) {
            Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('arccore')->__('This attribute cannot be edited.'));
            $this->_redirect('*/*/');
            return;
        }

        // Sets the window title
        $this->_title($id ? $model->getFrontendLabel() : $this->__('New Attribute'));

        // 3. Set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getCustomerattributeData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        Mage::register('arcattribute_data', $model);
        $model_arc_eav = Mage::getModel('arccore/attributedata')->getCollection()->addFieldToFilter('attribute_id', array('eq' => $id));
        Mage::register('arceavattribute_data', $model_arc_eav->getData());

        // 5. Build edit form
        $this->_initAction()
                ->_addBreadcrumb(
                        $id ? Mage::helper('arccore')->__('Edit Arc Attribute') : Mage::helper('arccore')->__('New Arc Attribute'), $id ? Mage::helper('arccore')->__('Edit Arc Attribute') : Mage::helper('arccore')->__('New Arc Attribute'));
        $this->_addContent($this->getLayout()->createBlock('arccore/adminhtml_arcattribute_edit'))
                ->_addLeft($this->getLayout()->createBlock('arccore/adminhtml_arcattribute_edit_tabs'));
        $this->getLayout()->getBlock('attribute_edit_js')
                ->setIsPopup((bool) $this->getRequest()->getParam('popup'));
        $this->renderLayout();
    }

    public function validateAction() {
        $response = new Varien_Object();
        $response->setError(false);

        $attributeCode = $this->getRequest()->getParam('attribute_code');
        $attributeId = $this->getRequest()->getParam('attribute_id');
        $entityType = Mage::getModel('eav/entity_type')->load($this->_entityTypeId)->getentityTypeCode();
        $attribute = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode($entityType, $attributeCode);
        if ($attribute && !$attributeId) {
            Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('arccore')->__('Attribute with the same code already exists'));
            $this->_initLayoutMessages('adminhtml/session');
            $response->setError(true);
            $response->setMessage($this->getLayout()->getMessagesBlock()->getGroupedHtml());
        }

        $this->getResponse()->setBody($response->toJson());
    }

    /**
     * Filter post data
     *
     * @param array $data
     * @return array
     */
    protected function _filterPostData($data) {
        if ($data) {
            /** @var $helperCatalog Mage_Catalog_Helper_Data */
            $helperCatalog = Mage::helper('arccore');
            //labels
            foreach ($data['frontend_label'] as & $value) {
                if ($value) {
                    $value = $helperCatalog->stripTags($value);
                }
            }

            if (!empty($data['option']) && !empty($data['option']['value']) && is_array($data['option']['value'])) {
                foreach ($data['option']['value'] as $key => $values) {
                    $data['option']['value'][$key] = array_map(array($helperCatalog, 'stripTags'), $values);
                }
            }
        }
        return $data;
    }

    /**
     * 
     * Save Arc attributes
     */
    public function saveAction() {
        $data = $this->getRequest()->getPost();

        if ($data) {
            $session = Mage::getSingleton('adminhtml/session');
            $redirectBack = $this->getRequest()->getParam('back', false);
            $model = Mage::getModel('arccore/mysql4_eav_attribute');
            $helper = Mage::helper('arccore');
            $id = $this->getRequest()->getParam('attribute_id');
            //validate attribute_code
            //echo $data['attribute_code'];exit;
            if (isset($data['attribute_code'])) {
                $validatorAttrCode = new Zend_Validate_Regex(array('pattern' => '/^[a-z][a-z_0-9]{1,254}$/'));
                if (!$validatorAttrCode->isValid($data['attribute_code'])) {
                    $session->addError(
                            Mage::helper('arccore')->__('Attribute code is invalid. Please use only letters (a-z), numbers (0-9) or underscore(_) in this field, first character should be a letter.')
                    );
                    $this->_redirect('*/*/edit', array('attribute_id' => $id, '_current' => true));
                    return;
                }
            }

            //validate frontend_input
            if (isset($data['frontend_input'])) {
                $validatorInputType = Mage::getModel('arccore/adminhtml_system_config_source_inputtype_validator');
                if (!$validatorInputType->isValid($data['frontend_input'])) {
                    foreach ($validatorInputType->getMessages() as $message) {
                        $session->addError($message);
                    }
                    $this->_redirect('*/*/edit', array('attribute_id' => $id, '_current' => true));
                    return;
                }
            }

            if ($id) {
                $model->load($id);
                if (!$model->getId()) {
                    $session->addError(
                            Mage::helper('arccore')->__('This Attribute no longer exists'));
                    $this->_redirect('*/*/');
                    return;
                }
                // entity type check
                if ($model->getEntityTypeId() != $this->_entityTypeId) {
                    $session->addError(
                            Mage::helper('arccore')->__('This attribute cannot be updated.'));
                    $session->setAttributeData($data);
                    $this->_redirect('*/*/');
                    return;
                }
                $data['attribute_code'] = $model->getAttributeCode();
                $data['is_user_defined'] = $model->getIsUserDefined();
                $data['frontend_input'] = $model->getFrontendInput();
            } else {
                /**
                 * @todo add to helper and specify all relations for properties
                 */
                $data['source_model'] = $helper->getAttributeSourceModelByInputType($data['frontend_input']);
                $data['backend_model'] = $helper->getAttributeBackendModelByInputType($data['frontend_input']);
            }
            if (!isset($data['is_filterable'])) {
                $data['is_filterable'] = 0;
            }
            if (!isset($data['is_filterable_in_search'])) {
                $data['is_filterable_in_search'] = 0;
            }

            if (is_null($model->getIsUserDefined()) || $model->getIsUserDefined() != 0) {
                $data['backend_type'] = $model->getBackendTypeByInput($data['frontend_input']);
            }

            $defaultValueField = $model->getDefaultValueByInput($data['frontend_input']);
            if ($defaultValueField) {
                $data['default_value'] = $this->getRequest()->getParam($defaultValueField);
            }
            //filter
            $data = $this->_filterPostData($data);

            
            //Here adding custom backend types
            if(!$data['backend_type']){
                $bcktype = $helper->getBackendTypes($data['frontend_input']);
                if($bcktype){
                    $data['backend_type'] = $bcktype;
                }
            }

            $model->addData($data);

            if (!$id) {
                $model->setEntityTypeId($this->_entityTypeId);
                $model->setIsUserDefined(1);
            }


            if ($this->getRequest()->getParam('set') && $this->getRequest()->getParam('group')) {
                // For creating product attribute on product page we need specify attribute set and group
                $model->setAttributeSetId($this->getRequest()->getParam('set'));
                $model->setAttributeGroupId($this->getRequest()->getParam('group'));
            }
            try { //echo "<pre>";echo get_class($model);print_r($model->getData());exit;
                $model->save();
                $session->addSuccess(
                        Mage::helper('arccore')->__('The arc attribute has been saved.'));

                /**
                 * Clear translation cache because attribute labels are stored in translation
                 */
                Mage::app()->cleanCache(array(Mage_Core_Model_Translate::CACHE_TAG));
                $session->setAttributeData(false);
                if ($this->getRequest()->getParam('popup')) {
                    $this->_redirect('adminhtml/catalog_product/addAttribute', array(
                        'id' => $this->getRequest()->getParam('product'),
                        'attribute' => $model->getId(),
                        '_current' => true
                    ));
                } elseif ($redirectBack) {
                    $this->_redirect('*/*/edit', array('attribute_id' => $model->getId(), '_current' => true));
                } else {
                    $this->_redirect('*/*/', array());
                }
                return;
            } catch (Exception $e) {
                $session->addError($e->getMessage());
                $session->setAttributeData($data);
                $this->_redirect('*/*/edit', array('attribute_id' => $id, '_current' => true));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Delete Arc attribute
     *
     * @return null
     */
    public function deleteAction() {
        if ($id = $this->getRequest()->getParam('attribute_id')) {
            $model = Mage::getModel('eav/entity_attribute');
            // entity type check
            $model->load($id);
            if ($model->getEntityTypeId() != $this->_entityTypeId) {
                Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('arccore')->__('This attribute cannot be deleted.'));
                $this->_redirect('*/*/');
                return;
            }

            try {
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('arccore')->__('The Arc attribute has been deleted.'));
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('attribute_id' => $this->getRequest()->getParam('attribute_id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('arccore')->__('Unable to find an attribute to delete.'));
        $this->_redirect('*/*/');
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('arccore/arc_attributes/manage_attr');
    }

}