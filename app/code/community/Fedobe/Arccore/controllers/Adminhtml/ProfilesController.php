<?php

/**
 * Manage Arc Attribute controller file
 * 
 * @category    Fedobe
 * @package     Fedobe_Arccore
 * @author      Fedobe Magento Team
 */
class Fedobe_Arccore_Adminhtml_ProfilesController extends Mage_Adminhtml_Controller_Action {

    protected $_entityTypeId;

    public function preDispatch() {
        parent::preDispatch();
        $this->_entityTypeId = Mage::helper('arccore')->getEntityTypeId('arccore');
    }

    /**
     * Init actions
     *
     */
    protected function _initAction() {
        // load layout, set active menu and breadcrumbs
        $this->_title($this->__('Tribe'))
                ->_title($this->__('Manage Profiles'));

        if ($this->getRequest()->getParam('popup')) {
            $this->loadLayout('popup');
        } else {
            $this->loadLayout()
                    ->_setActiveMenu('fedobe/arccore_manage_profiles');
        }
        return $this;
    }

    /**
     * Index action method
     */
    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }

    /**
     * Code to display the form
     * 
     * The main form container gets added to the content and the tabs block gets added to left.
     */
    public function newAction() {
        $attrsetid = $this->getRequest()->getParam('set');
        if ($attrsetid) {
            $this->_forward('edit');
        } else {
            $this->loadLayout();
            $this->renderLayout();
        }
    }

    public function checkurlkeyAction() {
        $data = array();
        $urlkey = $this->getRequest()->getPost('urlkey');
        $id = $this->getRequest()->getPost('id');
        $model = Mage::getModel('arccore/arc')->getCollection()
                ->addFieldToSelect('entity_id')
                ->addFieldToFilter('trco_user_name', array('eq' => $urlkey))
                ->addFieldToFilter('entity_id', array('neq' => $id));
        $model->getSelect()->limit(1);

        if ($id) {
            $savedusername = Mage::getModel('arccore/arc')->load($id)->getTrcoUserName();
            $user = Mage::getModel('admin/user')->getCollection()
                    ->addFieldToSelect('user_id')
                    ->addFieldToFilter('username', array('eq' => $savedusername));
            $savedid = $user->getUserId();
        }
        $user = Mage::getModel('admin/user')->getCollection()
                ->addFieldToSelect('user_id')
                ->addFieldToFilter('username', array('eq' => $urlkey));
        if ($savedid) {
            $user->addFieldToFilter('user_id', array('neq' => $id));
        }
        $err = 1;
        if (!$model->getData() && !$user->getUserId()) {
            $err = 0;
        }
        $data['err'] = $err;
        $data['msg'] = ($err) ? Mage::helper('arccore')->__("A Store with this username already exists!") : Mage::helper('arccore')->__("Username available");
        $res = Mage::helper('core')->jsonEncode($data);
        return $res;
    }

    public function getCustomerInfoAction() {
        $data = array();
        $customer_id = $this->getRequest()->getPost('customer_id');
        $user = mage::getModel('customer/customer')->load($customer_id);
        $custdata = $user->getData();
        if ($custdata) {
            //Here let's check username exists or not if not there
            //then save it by auto creating from email
            if (!$custdata['username']) {
                $username = Mage::helper('arccore')->clean($custdata['email']);
                $user->setUsername($username);
                $user->save();
            }
            $data['err'] = 0;
            $data = $user->getData();
            $data['url'] = Mage::helper('arccore')->getFrontendProductUrl($data['username']);
            $data['data'] = $data;
        } else {
            $data['err'] = 1;
            $data['data'] = NULL;
        }
        echo Mage::helper('core')->jsonEncode($data);
    }

    /**
     * Edit Arc Attribute
     */
    public function editAction() {
        $arcId = $this->getRequest()->getParam('id');
        $arc = $this->_initProfile();
        if ($arcId && !$arc->getId()) {
            $this->_getSession()->addError(Mage::helper('arccore')->__('This profile no longer exists.'));
            $this->_redirect('*/*/');
            return;
        }
        Mage::dispatchEvent('arccore_arc_edit_action', array('arc' => $arc));
        return $this;
    }

    public function validateAction() {
        $response = new Varien_Object();
        $response->setError(false);

        $attributeCode = $this->getRequest()->getParam('attribute_code');
        $attributeId = $this->getRequest()->getParam('attribute_id');
        $entityType = Mage::getModel('eav/entity_type')->load($this->_entityTypeId)->getentityTypeCode();
        $attribute = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode($entityType, $attributeCode);
        if ($attribute && !$attributeId) {
            Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('arccore')->__('Attribute with the same code already exists'));
            $this->_initLayoutMessages('adminhtml/session');
            $response->setError(true);
            $response->setMessage($this->getLayout()->getMessagesBlock()->getGroupedHtml());
        }

        $this->getResponse()->setBody($response->toJson());
    }

    /**
     * Filter post data
     *
     * @param array $data
     * @return array
     */
    protected function _filterPostData($data) {
        if ($data) {
            /** @var $helperCatalog Mage_Catalog_Helper_Data */
            $helperCatalog = Mage::helper('arccore');
            //labels
            foreach ($data['frontend_label'] as & $value) {
                if ($value) {
                    $value = $helperCatalog->stripTags($value);
                }
            }

            if (!empty($data['option']) && !empty($data['option']['value']) && is_array($data['option']['value'])) {
                foreach ($data['option']['value'] as $key => $values) {
                    $data['option']['value'][$key] = array_map(array($helperCatalog, 'stripTags'), $values);
                }
            }
        }
        return $data;
    }

    public function wysiwygAction() {
        $elementId = $this->getRequest()->getParam('element_id', md5(microtime()));
        $storeId = $this->getRequest()->getParam('store_id', 0);
        $storeMediaUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);

        $content = $this->getLayout()->createBlock('adminhtml/catalog_helper_form_wysiwyg_content', '', array(
            'editor_element_id' => $elementId,
            'store_id' => $storeId,
            'store_media_url' => $storeMediaUrl,
        ));
        $this->getResponse()->setBody($content->toHtml());
    }

    public function productsAction() {
        $this->_initProfile();
        $this->loadLayout();
        $this->getLayout()->getBlock('tribe.arc.products.tab');
        $this->renderLayout();
    }

    protected function _initProfile() {
        $arcId = (int) $this->getRequest()->getParam('id');
        $arc = Mage::getModel('arccore/arc')
                ->setStoreId($this->getRequest()->getParam('store', 0))
                ->setEntityTypeId($this->_entityTypeId); //echo $arcId;exit;
        if ($arcId) {
            try {
                $arc->load($arcId);
                $arc->setStoreId($this->getRequest()->getParam('store', 0));
                $arc->setData('edit_mode', 1);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        } else {
            if ($setId = (int) $this->getRequest()->getParam('set')) {
                $arc->setAttributeSetId($setId);
            } else if ($this->_attributesetId) {
                $arc->setAttributeSetId($this->_attributesetId);
            }
            $arc->setData('edit_mode', 0);
        }

//echo "<pre>";
//print_r($arc->getData());
//echo "</pre>";

        Mage::register('arcprofile_data', $arc);
        Mage::register('current_arc_profile', $arc);
        return $arc;
    }

    /**
     * Initialize product before saving
     */
    protected function _initProfileSave() {
        $arc = $this->_initProfile();
        $arcData = $this->getRequest()->getPost('arc');
        $arc->addData($arcData);
        /**
         * Check "Use Default Value" checkboxes values
         */
        if ($useDefaults = $this->getRequest()->getPost('use_default')) {
            foreach ($useDefaults as $attributeCode) {
                $arc->setData($attributeCode, false);
            }
        }

        /**
         * Init product links data (related, upsell, crosssel)
         */
        $arc_products = array();
        $links = $this->getRequest()->getPost('arclinks');
        if (isset($links['products'])) {
            $arc_products = array_keys(Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['products']));
            $arc_product_ids = implode(",", $arc_products);
            $arc->setProductsData($arc_product_ids);
        }
        /**
         * Initialize product categories
         */
        if ($arc_products) {
            $categoryIds = Mage::helper('arccore')->getProductCategories($arc_products);
            if (!$categoryIds) {
                $categoryIds = '';
            }
            $arc->setCategoryIds($categoryIds);
        }
        Mage::dispatchEvent(
                'arccore_prepare_save', array('product' => $arc, 'request' => $this->getRequest())
        );
        return $arc;
    }

    /**
     * Get upsell products grid
     */
    public function productsgridAction() {
        $this->_initProfile();
        $this->loadLayout();
        $this->getLayout()->getBlock('tribe.arc.products.tab')
                ->setProductsData($this->getRequest()->getPost('products_data', null));
        $this->renderLayout();
    }

    public function reviewsAction() {
        $this->_initProfile();
        $this->loadLayout();
        $this->getLayout()->getBlock('tribe.arc.reviews')
                ->setArcId(Mage::registry('arcprofile_data')->getId())
                ->setUseAjax(true);
        $this->renderLayout();
    }

    /**
     * 
     * Save arc attributes
     */
    public function saveAction() {
        $storeId = $this->getRequest()->getParam('store');
        $redirectBack = $this->getRequest()->getParam('back', false);
        $arcId = $this->getRequest()->getParam('id');
        $data = $this->getRequest()->getPost();//echo "<pre>";print_r($data);exit;
        if ($data) {
            $arc = $this->_initProfileSave();
            try { //echo "<pre>";print_r($arc);exit;
                $arc->save();
                $arcId = $arc->getId();
                $this->_getSession()->addSuccess(Mage::helper('arccore')->__('Successfully saved.'));
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $result['message'] = $redirectBack = true;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $redirectBack = true;
            }
        }
        if($this->getRequest()->isXmlHttpRequest()){
            echo 1;exit;
        }
        if ($this->getRequest()->getParam('set')) {
            if ($redirectBack) {
                $this->_redirect('*/*/edit', array(
                    'id' => $arcId,
                    '_current' => true
                ));
            } else {
                $this->_redirect('*/*/', array('store' => $storeId));
            }
        } else {
            $this->_redirectReferer();
        }
    }

    /**
     * Delete arc attribute
     *
     * @return null
     */
    public function deleteAction($chl_id) {
        $error = 1;
        if ($id = $this->getRequest()->getParam('id')) {
            $model = Mage::getModel('arccore/arc');
            // entity type check
            $model->load($id);
            if ($model->getEntityTypeId() != $this->_entityTypeId) {
                $this->_getSession()->addError(Mage::helper('arccore')->__('cannot be deleted.'));
            }
            try {
                $model->delete();
                $this->_getSession()->addSuccess(Mage::helper('arccore')->__('Successfully deleted.'));
                $error = 0;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        if($chl_id){
            return $error;
        }else{
            $url = $this->getUrl('*/*/', array('store' => $storeId));
            header("Location: $url");exit;
        }
        
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('arccore/manage_arc');
    }

    public function massDeleteAction() {
        $arcIds = $this->getRequest()->getParam('arc');
        if (!is_array($arcIds)) {
            $this->_getSession()->addError($e->getMessage());
        } else {
            if (!empty($arcIds)) {
                try {
                    foreach ($arcIds as $arcId) {
                        $arc = Mage::getSingleton('arccore/arc')->load($arcId);
                        Mage::dispatchEvent('profiles_controller_arc_delete', array('arc' => $arc));
                        $arc->delete();
                    }
                    $this->_getSession()->addSuccess(Mage::helper('arccore')->__('Total of %d record(s) have been deleted.', count($arcIds)));
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }
            }
        }
        $this->_redirect('*/*/', array('store' => $storeId));
    }

    /**
     * Update product(s) status action
     *
     */
    public function massStatusAction() {
        $arcIds = (array) $this->getRequest()->getParam('arc');
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        $status = (int) $this->getRequest()->getParam('status');
        try {
            foreach ($arcIds as $arcId) {
                $arc = Mage::getSingleton('arccore/arc')->load($arcId);
                $arc->setData('trco_status', $status);
                $arc->setData('trcr_status', $status);
                $arc->setData('is_active', $status);
                $arc->save();
            }
            $this->_getSession()->addSuccess(Mage::helper('arccore')->__('Total of %d record(s) have been updated.', count($arcIds)));
        } catch (Mage_Core_Model_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError(Mage::helper('arccore')->__('An error occurred while updating the status.'));
        }
         $this->_redirect('*/*/', array('store' => $storeId));
    }

    public function loadprofile() {
        $arcId = (int) $this->getRequest()->getParam('id');
        $arc = Mage::getModel('arccore/arc')
                ->setStoreId($this->getRequest()->getParam('store', 0))
                ->setEntityTypeId($this->_entityTypeId);
        $arc->load($arcId);
        return $arc;
    }

    public function getProfiledataAction() {
        $data = array();
        $profile_id = $this->getRequest()->getPost('profile_id');
        $collection_data = Mage::getModel('arccore/arc')->getCollection()
                        ->addAttributeToSelect('*')
                        ->addFieldToFilter('trcr_profile_type', $profile_id)->load();
        $str = '';
        $reviewId = $this->getRequest()->getPost('id');
        $username = 0;
        if ($reviewId) {
            $review_data = Mage::getModel('arccore/arc')->getCollection()->addAttributeToSelect('*')
                            ->addFieldToFilter('entity_id', $reviewId)->getFirstItem()->getData();
            $username = $review_data['trcr_username_list'];
        }
        foreach ($collection_data as $val) {
            if ($username == $val->getEntityId())
                $str.="<option value='" . $val->getEntityId() . "' selected='selected'>" . $val->getTrcrName() . "</option>";
            else
                $str.="<option value='" . $val->getEntityId() . "'>" . $val->getTrcrName() . "</option>";
        }
        $data['data'] = $str;
        echo Mage::helper('core')->jsonEncode($data);
    }

}