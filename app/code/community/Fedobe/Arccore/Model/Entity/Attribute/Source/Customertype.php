<?php

class Fedobe_Arccore_Model_Entity_Attribute_Source_Customertype extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    public function getAllOptions($withEmpty = false) {
        $profiles = array();
        $allOptions = Mage::helper('arccore')->attribute_options('trcr_profile_type');
      //  $profiles = array_combine(array_column($allOptions,'value'),array_column($allOptions,'label'));
        return $allOptions;
    }

    public function getOptionText($value) {
        $options = $this->getAllOptions(false);
        foreach ($options as $item) {
            if ($item['value'] == $value) {
                return $item['label'];
            }
        }
        return false;
    }

}