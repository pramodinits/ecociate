<?php

class Fedobe_Arccore_Model_Entity_Attribute_Source_Data extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    protected function getAttrConfig() {
        $actionaname = Mage::app()->getRequest()->getActionName();
        $controllername = Mage::app()->getRequest()->getControllerName();
        $attrcode = "attributecode";
        $attributecode = Mage::getStoreConfig("$controllername/$actionaname/$attrcode");
        return $attributecode;
    }

    private function checkValidAttribute($attrcode) {
        $attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', $attrcode);
        return $attributeId;
    }

    public function __construct() {
        $actionaname = Mage::app()->getRequest()->getActionName();
        $controllername = Mage::app()->getRequest()->getControllerName();
        $attrcode = "attributecode";
        $attributecode = $this->getAttrConfig();
        if ($attributecode) {
            if ($this->checkValidAttribute($attributecode)) {
                
            } else {
                die(Mage::helper('adminhtml')->__("Attribute Code used does not exists in the System !"));
            }
        } else {
            $message = "You need to add the configuration in your system.xml & adminhtml.xml with correct ACL and save the attribute code and then continue or unassign `trcr_attribute_option` attribute from this Attribute Set ";
            $desc = <<<DESC
                    section name to be : $controllername
                    group name to be : $actionaname
                    field name to be :$attrcode
DESC;
            echo "<pre>";
            die(Mage::helper('adminhtml')->__($message . "<br/>") . $desc);
            echo "</pre>";
        }
    }

    /**
     * Default values for option cache
     *
     * @var array
     */
    protected $_optionsDefault = array();

    /**
     * Retrieve Full Option values array
     *
     * @param bool $withEmpty       Add empty option to array
     * @param bool $defaultValues
     * @return array
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false) {
        $storeId = $this->getAttribute()->getStoreId();
        if (!is_array($this->_options)) {
            $this->_options = array();
        }
        if (!is_array($this->_optionsDefault)) {
            $this->_optionsDefault = array();
        }
        if (!isset($this->_options[$storeId])) {
            $collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
                    ->setPositionOrder('asc')
                    ->setAttributeFilter($this->checkValidAttribute($this->getAttrConfig()))
                    ->setStoreFilter($this->getAttribute()->getStoreId())
                    ->load();
            $this->_options[$storeId] = $collection->toOptionArray();
            $this->_optionsDefault[$storeId] = $collection->toOptionArray('default_value');
        }
        $options = ($defaultValues ? $this->_optionsDefault[$storeId] : $this->_options[$storeId]);
        if ($withEmpty) {
            array_unshift($options, array('label' => '', 'value' => ''));
        }

        return $options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string
     */
    public function getOptionText($value) {
        $isMultiple = false;
        if (strpos($value, ',')) {
            $isMultiple = true;
            $value = explode(',', $value);
        }

        $options = $this->getAllOptions(false);

        if ($isMultiple) {
            $values = array();
            foreach ($options as $item) {
                if (in_array($item['value'], $value)) {
                    $values[] = $item['label'];
                }
            }
            return $values;
        }

        foreach ($options as $item) {
            if ($item['value'] == $value) {
                return $item['label'];
            }
        }
        return false;
    }

    /**
     * Add Value Sort To Collection Select
     *
     * @param Mage_Eav_Model_Entity_Collection_Abstract $collection
     * @param string $dir
     *
     * @return Mage_Eav_Model_Entity_Attribute_Source_Table
     */
    public function addValueSortToCollection($collection, $dir = Varien_Db_Select::SQL_ASC) {
        $valueTable1 = $this->getAttribute()->getAttributeCode() . '_t1';
        $valueTable2 = $this->getAttribute()->getAttributeCode() . '_t2';
        $collection->getSelect()
                ->joinLeft(
                        array($valueTable1 => $this->getAttribute()->getBackend()->getTable()), "e.entity_id={$valueTable1}.entity_id"
                        . " AND {$valueTable1}.attribute_id='{$this->getAttribute()->getId()}'"
                        . " AND {$valueTable1}.store_id=0", array())
                ->joinLeft(
                        array($valueTable2 => $this->getAttribute()->getBackend()->getTable()), "e.entity_id={$valueTable2}.entity_id"
                        . " AND {$valueTable2}.attribute_id='{$this->getAttribute()->getId()}'"
                        . " AND {$valueTable2}.store_id='{$collection->getStoreId()}'", array()
        );
        $valueExpr = $collection->getSelect()->getAdapter()
                ->getCheckSql("{$valueTable2}.value_id > 0", "{$valueTable2}.value", "{$valueTable1}.value");

        Mage::getResourceModel('eav/entity_attribute_option')
                ->addOptionValueToCollection($collection, $this->getAttribute(), $valueExpr);

        $collection->getSelect()
                ->order("{$this->getAttribute()->getAttributeCode()} {$dir}");

        return $this;
    }

    /**
     * Retrieve Column(s) for Flat
     *
     * @return array
     */
    public function getFlatColums() {
        $columns = array();
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $isMulti = $this->getAttribute()->getFrontend()->getInputType() == 'multiselect';

        if (Mage::helper('core')->useDbCompatibleMode()) {
            $columns[$attributeCode] = array(
                'type' => $isMulti ? 'varchar(255)' : 'int',
                'unsigned' => false,
                'is_null' => true,
                'default' => null,
                'extra' => null
            );
            if (!$isMulti) {
                $columns[$attributeCode . '_value'] = array(
                    'type' => 'varchar(255)',
                    'unsigned' => false,
                    'is_null' => true,
                    'default' => null,
                    'extra' => null
                );
            }
        } else {
            $type = ($isMulti) ? Varien_Db_Ddl_Table::TYPE_TEXT : Varien_Db_Ddl_Table::TYPE_INTEGER;
            $columns[$attributeCode] = array(
                'type' => $type,
                'length' => $isMulti ? '255' : null,
                'unsigned' => false,
                'nullable' => true,
                'default' => null,
                'extra' => null,
                'comment' => $attributeCode . ' column'
            );
            if (!$isMulti) {
                $columns[$attributeCode . '_value'] = array(
                    'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
                    'length' => 255,
                    'unsigned' => false,
                    'nullable' => true,
                    'default' => null,
                    'extra' => null,
                    'comment' => $attributeCode . ' column'
                );
            }
        }

        return $columns;
    }

    /**
     * Retrieve Indexes for Flat
     *
     * @return array
     */
    public function getFlatIndexes() {
        $indexes = array();

        $index = sprintf('IDX_%s', strtoupper($this->getAttribute()->getAttributeCode()));
        $indexes[$index] = array(
            'type' => 'index',
            'fields' => array($this->getAttribute()->getAttributeCode())
        );

        $sortable = $this->getAttribute()->getUsedForSortBy();
        if ($sortable && $this->getAttribute()->getFrontend()->getInputType() != 'multiselect') {
            $index = sprintf('IDX_%s_VALUE', strtoupper($this->getAttribute()->getAttributeCode()));

            $indexes[$index] = array(
                'type' => 'index',
                'fields' => array($this->getAttribute()->getAttributeCode() . '_value')
            );
        }

        return $indexes;
    }

    /**
     * Retrieve Select For Flat Attribute update
     *
     * @param int $store
     * @return Varien_Db_Select|null
     */
    public function getFlatUpdateSelect($store) {
        return Mage::getResourceModel('eav/entity_attribute_option')
                        ->getFlatUpdateSelect($this->getAttribute(), $store);
    }

}