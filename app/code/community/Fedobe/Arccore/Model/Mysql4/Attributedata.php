<?php

class Fedobe_Arccore_Model_Mysql4_Attributedata extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('arccore/eav_attribute', 'attribute_id');
    }

}
