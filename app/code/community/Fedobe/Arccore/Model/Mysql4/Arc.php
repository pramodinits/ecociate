<?php
class Fedobe_Arccore_Model_Mysql4_Arc extends Fedobe_Arccore_Model_Mysql4_Arc_Abstract {
    public function _construct() {
        $resource = Mage::getSingleton('core/resource');
        $this->setType('fedobe_arccore_arc');
        $this->setConnection(
                $resource->getConnection('arccore_read'), $resource->getConnection('arccore_write')
        );
    }
    public function getCategoryIds($arc) {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()
                ->from($this->getEntityTable(), 'category_ids')
                ->where('entity_id = ?', (int) $arc->getId());
        return $adapter->fetchCol($select);
    }
    public function getIdByArcUsername($username) {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()
                ->from($this->getEntityTable(), 'entity_id')
                ->where('trcr_username = :username');
        $bind = array(':trcr_username' => (string) $username);
        return $adapter->fetchOne($select, $bind);
    }
    public function getIdByArcStorename($arc_store_name) {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()
                ->from($this->getEntityTable(), 'entity_id')
                ->where('trcr_name = :arc_store_name');
        $bind = array(':trcr_name' => (string) $arc_store_name);
        return $adapter->fetchOne($select, $bind);
    }
    public function getIdByArcEmail($arc_email) {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()
                ->from($this->getEntityTable(), 'entity_id')
                ->where('trcr_email = :arc_email');
        $bind = array(':trcr_email' => (string) $arc_email);
        return $adapter->fetchOne($select, $bind);
    }
    public function getAdminUserId($username, $email) {
        $user = Mage::getModel('admin/user')->getCollection()
                ->addFieldToSelect('user_id')
                ->addFieldToFilter(
                array('username', 'email'), array(
            array('eq' => $username),
            array('eq' => $email)
                )
        );
        $user_id = $user->getData()[0]['user_id'];
        return $user_id;
    }
    public function getArcsStoreName(array $Ids) {
        $select = $this->_getReadAdapter()->select()
                ->from($this->getEntityTable(), array('entity_id', 'trcr_name'))
                ->where('entity_id IN (?)', $Ids);
        return $this->_getReadAdapter()->fetchAll($select);
    }
    public function getProductEntitiesInfo($columns = null) {
        if (!empty($columns) && is_string($columns)) {
            $columns = array($columns);
        }
        if (empty($columns) || !is_array($columns)) {
            $columns = $this->_getDefaultAttributes();
        }
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()
                ->from($this->getEntityTable(), $columns);
        return $adapter->fetchAll($select);
    }
    public function getAssignedImages($arc, $storeIds) {
        if (!is_array($storeIds)) {
            $storeIds = array($storeIds);
        }
        $mainTable = $arc->getResource()->getAttribute('arc_entity_media_gallery')
                ->getBackend()
                ->getTable();
        $read = $this->_getReadAdapter();
        $select = $read->select()
                ->from(
                        array('images' => $mainTable), array('value as filepath', 'store_id')
                )
                ->joinLeft(
                        array('attr' => $this->getTable('eav/attribute')), 'images.attribute_id = attr.attribute_id', array('attribute_code')
                )
                ->where('entity_id = ?', $arc->getId())
                ->where('store_id IN (?)', $storeIds)
                ->where('attribute_code IN (?)', array('small_image', 'thumbnail', 'image'));
        $images = $read->fetchAll($select);
        return $images;
    }
    /**
     * Retrieve a collection of products associated with the landingpage page
     *
     * @return Mage_Catalog_Model_Resource_Eav_Resource_Product_Collection
     */
    public function getProductCollection(Fedobe_Arccore_Model_Arc $arc) {
        $storeId = ((int) $arc->getStoreId() === 0) ? Mage::app()->getStore()->getId() : $page->getStoreId();
        $collection = Mage::getResourceModel('catalog/product_collection')
                ->setStoreId($storeId)
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter('visibility', array('in' => array(
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH
        )));
        $ids = $arc->getProductsData() ? explode(',', $arc->getProductsData()) : array(0);
        $collection->addFieldToFilter('entity_id', array('in' => $ids));
        return $collection;
    }
}