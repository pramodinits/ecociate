<?php

class Fedobe_Arccore_Model_Mysql4_Arc_Collection extends Mage_Eav_Model_Entity_Collection_Abstract {

    /**
     * Store id of application
     *
     * @var integer
     */
    protected $_storeId = null;

    public function _construct() {
        $this->_init('arccore/arc');
    }

    /**
     * Set store id
     *
     * @param integer $storeId
     * @return Fedobe_Arccore_Model_Mysql4_Arc_Collection
     */
    public function setStoreId($storeId) {
        $this->_storeId = $storeId;
        return $this;
    }

}
