<?php

class Fedobe_Arccore_Model_Mysql4_Arc_Attribute_Backend_Image extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract {

    /**
     * After save
     *
     * @param Varien_Object $object
     * @return Mage_Catalog_Model_Resource_Product_Attribute_Backend_Image
     */
    public function afterSave($object) {
        $value = $object->getData($this->getAttribute()->getName());
        if (is_array($value) && !empty($value['delete'])) {
            $object->setData($this->getAttribute()->getName(), ''); //echo "<pre>";print_r($object);exit;
            $this->getAttribute()->getEntity()
                    ->saveAttribute($object, $this->getAttribute()->getName());
            return;
        }

        try {
            $uploader = new Mage_Core_Model_File_Uploader($this->getAttribute()->getName());
            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);//if($_SERVER['REMOTE_ADDR']=='45.251.36.100'){echo $uploader->getUploadedFileName();exit;}
        } catch (Exception $e) {
            return $this;
        }
        if (!file_exists(Mage::getBaseDir('media') . '/arccore/arcs/images')) {
            mkdir(Mage::getBaseDir('media') . '/arccore/arcs/images');
            chmod(Mage::getBaseDir('media') . '/arccore/arcs/images', 0777);
        }//echo "<pre>";var_dump($uploader);exit;
        $uploader->save(Mage::getBaseDir('media') . '/arccore/arcs/images');

        $fileName = $uploader->getUploadedFileName();
        if ($fileName) {
            $object->setData($this->getAttribute()->getName(), $fileName);
            $this->getAttribute()->getEntity()
                    ->saveAttribute($object, $this->getAttribute()->getName());
        }
        return $this;
    }

}