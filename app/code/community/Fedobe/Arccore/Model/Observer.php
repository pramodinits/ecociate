<?php

class Fedobe_Arccore_Model_Observer {
    public function addButtonprofile($observer){
    $container = $observer->getBlock();
    $controller_name_org = Mage::app()->getRequest()->getControllerName();
    $tribemodule = str_replace('_','',$controller_name_org);
    $modulename = str_replace('tribe','',$tribemodule);
    $controller_name = ucwords(str_replace('_',' ',$controller_name));
    $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                        ->load($controller_name, 'attribute_set_name')
                        ->getAttributeSetId();
   // echo $attributeSetId;exit;
    if(null !== $container && $container->getType() == $tribemodule.'/'.'adminhtml_'.$modulename) {
        $data_2 = array(
            'label'     => 'Add '.ucfirst($modulename),
            'class'=>'add',
            'onclick'   => 'setLocation(\' '  . Mage::helper('adminhtml')->getUrl('adminhtml/'.$controller_name_org.'/new/set/'.$attributeSetId, array('_secure' => true)) . '\')',
        );
        $container->addButton('addcollection', $data_2);
    }
    return $this;
    
}
}
