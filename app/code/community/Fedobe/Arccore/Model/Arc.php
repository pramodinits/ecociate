<?php
class Fedobe_Arccore_Model_Arc extends Fedobe_Arccore_Model_Arc_Abstract {
    protected function _construct() {
        $this->_init('arccore/arc');
    }
public function loadByAttribute($attribute, $value, $additionalAttributes = '*') {
        $collection = $this->getResourceCollection()
                ->addAttributeToSelect($additionalAttributes)
                ->addAttributeToFilter($attribute, $value)
                ->setPage(1, 1);

        foreach ($collection as $object) {
            return $object;
        }
        return false;
    }
    
    public function getEntityTypeId(){
         return Mage::helper('arccore')->getEntityTypeId();
    }

}