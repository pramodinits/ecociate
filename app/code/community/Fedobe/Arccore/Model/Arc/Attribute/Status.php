<?php

class Fedobe_Arccore_Model_Arc_Attribute_Status extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    public function getAllOptions($withEmpty = false) {
        $status = array(array('value'=>1,'label' => 'Inactive'),array('value'=>2,'label'=> 'Active'));
        return $status;
    }

    public function getOptionText($value) {
        $options = $this->getAllOptions(false);
        foreach ($options as $item) {
            if ($item['value'] == $value) {
                return $item['label'];
            }
        }
        return false;
    }

}