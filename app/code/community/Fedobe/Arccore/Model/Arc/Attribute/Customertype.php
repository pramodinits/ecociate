<?php

class Fedobe_Arccore_Model_Arc_Attribute_Customertype extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    public function getAllOptions($withEmpty = false) {
        $status = array(1 => 'New',2 => 'Existing');
        return $status;
    }

    public function getOptionText($value) {
        $options = $this->getAllOptions(false);
        foreach ($options as $item) {
            if ($item['value'] == $value) {
                return $item['label'];
            }
        }
        return false;
    }

}