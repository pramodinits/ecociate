<?php

class Fedobe_Arccore_Model_Arc_Attribute_Customers extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

     public function getAllOptions($withEmpty = false) {
        $customers = array();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $tableName = $resource->getTableName('customer_entity');
        $profiles = array();
        $allposttypes = Mage::helper('arccore')->getAllposttype();
        
        $id = Mage::app()->getRequest()->getParam('id');
        $set = Mage::app()->getRequest()->getParam('set');
        if ($set && !in_array($set, $allposttypes)) {
            $allprofiles = Mage::helper('arccore')->getattributeoptions('trcr_profile_type');
            $keys = array_column($allprofiles, 'value');
            $collection = Mage::getModel('arccore/arc')->getCollection()->addAttributeToSelect('*')
                    ->addFieldTofilter('trcr_profile_type', array('in' => $keys))
                    ->addFieldTofilter('trcr_customer_list', array('notnull' => true));
            if ($id)
                $collection->addFieldTofilter('entity_id', array('neq' => $id));
            $collection = $collection->getData();

            $profiles = array_unique(array_column($collection, 'trcr_customer_list'));
            
            $seller_module_enabled = Mage::helper('core')->isModuleEnabled('Fedobe_Tribeseller');
            if ($seller_module_enabled):
                 $sellercollection = Mage::getModel('tribeseller/seller')->getCollection()
                                ->addAttributeToSelect('entity_id')
                                ->addFieldTofilter('all_customers', array('notnull' => true))
                                ->addFieldToFilter('seller_customer_type',2);
         if ($id)
            $sellercollection->addFieldTofilter('entity_id', array('neq' => $id));
            $sellercollection = $sellercollection->getData();
            $seller_profiles_allcustomer = array_unique(array_column($sellercollection, 'all_customers'));
            $profiles = array_merge($profiles, $seller_profiles_allcustomer);
            
            //seller_email remove for seller customer type new
        $sellercollection_allcustomers = Mage::getModel('tribeseller/seller')->getCollection()
                                ->addAttributeToSelect('entity_id')
                                ->addFieldTofilter('seller_email', array('notnull' => true))
                                ->addFieldToFilter('seller_customer_type',1);
                                
        if ($id)
            $sellercollection_allcustomers->addFieldTofilter('entity_id', array('neq' => $id));
             $sellercollection_allcustomers = $sellercollection_allcustomers->getData();
             $seller_profiles = array_unique(array_column($sellercollection_allcustomers, 'seller_email'));
             
             //admin user email
             $admin_user_all = Mage::getModel('admin/user')->getCollection()
                ->addFieldToSelect(array('email','username'));
            if ($id){
                $profile = Mage::getModel('arccore/arc')->load($id);
                $adminuserid = Mage::getResourceModel('tribeseller/seller')->getAdminUserId($profile->getTrcrUsername(), $profile->getTrcrEmail());
                $admin_user_all->addFieldToFilter(array('user_id'), array(array('neq' => $adminuserid)));    
            } 
            $admin_user_all=$admin_user_all ->getData();
            $admin_user_email =  array_unique(array_column($admin_user_all, 'email'));
            $seller_profiles = array_merge($admin_user_email, $seller_profiles);
            $seller_profiles_emails = implode("','",$seller_profiles);
            
            //echo "<pre>";print_r($seller_profiles);exit;
            $admin_username =  array_unique(array_column($admin_user_all, 'username'));
            $admin_username_str = implode(',',$admin_username);
            
            
            endif;
            $query = "SELECT `entity_id`,`email` FROM $tableName WHERE is_active = 1 AND group_id = 1 AND `entity_id` NOT IN (" . implode(',', $profiles) . ") AND `email` NOT IN ('".$seller_profiles_emails."') ORDER BY `email` ASC";
        }else {
            $query = "SELECT `entity_id`,`email` FROM $tableName WHERE is_active = 1 AND group_id = 1 ORDER BY `email` ASC";
        }
        $res = $readConnection->query($query);
        while ($row = $res->fetch()) {
            //$customers[$row['entity_id']] = $row['email'];
            $customers[$row['entity_id']]['label'] = $row['email'];
            $customers[$row['entity_id']]['value'] = $row['entity_id'];
        }
        return $customers;
    }

    public function getOptionText($value) {
        $options = $this->getAllOptions(false);
        foreach ($options as $item) {
            if ($item['value'] == $value) {
                return $item['label'];
            }
        }
        return false;
    }

}