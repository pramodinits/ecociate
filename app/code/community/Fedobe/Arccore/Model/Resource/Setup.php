<?php

class Fedobe_Arccore_Model_Resource_Setup extends Mage_Eav_Model_Entity_Setup {

    /**
     * Prepare catalog attribute values to save
     *
     * @param array $attr
     * @return array
     */
    protected function _prepareValues($attr) {
        $data = parent::_prepareValues($attr);
        $data = array_merge($data, array(
            'frontend_input_renderer' => $this->_getValue($attr, 'input_renderer'),
            'is_global' => $this->_getValue(
                    $attr, 'global', Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
            ),
            'is_visible' => $this->_getValue($attr, 'visible', 1),
            'is_searchable' => $this->_getValue($attr, 'searchable', 0),
            'is_filterable' => $this->_getValue($attr, 'filterable', 0),
            'is_wysiwyg_enabled' => $this->_getValue($attr, 'wysiwyg_enabled', 0),
            'is_html_allowed_on_front' => $this->_getValue($attr, 'is_html_allowed_on_front', 0),
            'is_visible_in_advanced_search' => $this->_getValue($attr, 'visible_in_advanced_search', 0),
            'is_filterable_in_search' => $this->_getValue($attr, 'filterable_in_search', 0),
            'position' => $this->_getValue($attr, 'position', 0),
            'is_used_for_promo_rules' => $this->_getValue($attr, 'used_for_promo_rules', 0)
        ));
        return $data;
    }

    /*
     * Setup attributes for inchoo_blog_post entity type
     * -this attributes will be saved in db if you set them
     */

    public function getDefaultEntities() {
          $entities = array(
            'fedobe_arccore_arc' => array(
                'entity_model' => 'arccore/arc',
                'additional_attribute_table' => 'arccore/eav_attribute',
                'entity_attribute_collection' => '',
                'table' => 'arccore/arc_entity',
                'attributes' => array(
                    'arc_customer_type' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Seller Customer Type',
                        'input' => 'select',
                        'option' => $this->getAllSellerCustomerType(),
                        'class' => '',
                        'source' => 'arccore/arc_attribute_customertype',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Arc',
                    )
               )
            )
        );
        return $entities;
    }

    
    public function getAllCountries() {
        $formattedcountries = array();
        $countries = Mage::getResourceModel('directory/country_collection')->loadData()->toOptionArray(false);
        foreach ($countries as $k => $v) {
            $formattedcountries['values'][] = $v['label'];
        }
        return $formattedcountries;
    }

    public function getAllSellerStatus() {
        $formatedstatus = array();
        $status = Mage::getModel('arccore/arc_attribute_status')->getAllOptions();
        foreach ($status as $k => $v) {
            $formatedstatus['values'][] = $v;
        }
        return $formatedstatus;
    }
    public function getAllCustomers() {
        $formatedstatus = array();
        $status = Mage::getModel('arccore/arc_attribute_customers')->getAllOptions();
        foreach ($status as $k => $v) {
            $formatedstatus['values'][] = $v;
        }
        return $formatedstatus;
    }

    public function getAllSellerCustomerType() {
        $formatedstatus = array();
        $status = Mage::getModel('arccore/arc_attribute_customertype')->getAllOptions();
        foreach ($status as $k => $v) {
            $formatedstatus['values'][] = $v;
        }
        return $formatedstatus;
    }
    
    
    public function createEntityTables($baseTableName, array $options = array()) {
        $isNoCreateMainTable = $this->_getValue($options, 'no-main', false);
        $isNoDefaultTypes = $this->_getValue($options, 'no-default-types', false);
        $customTypes = $this->_getValue($options, 'types', array());
        $tables = array();

        if (!$isNoCreateMainTable) {
            /**
             * Create table main eav table
             */
            $connection = $this->getConnection();
            $mainTable = $connection
                    ->newTable($this->getTable($baseTableName))
                    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                            ), 'Entity Id')
                    ->addColumn('entity_type_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                        'unsigned' => true,
                        'nullable' => false,
                        'default' => '0',
                            ), 'Entity Type Id')
                    ->addColumn('attribute_set_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                        'unsigned' => true,
                        'nullable' => false,
                        'default' => '0',
                            ), 'Attribute Set Id')
                    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
                        'nullable' => false,
                            ), 'Created At')
                    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
                        'nullable' => false,
                            ), 'Updated At')
                    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                        'unsigned' => true,
                        'nullable' => false,
                        'default' => '1',
                            ), 'Defines Is Entity Active')
                    ->addIndex($this->getIdxName($baseTableName, array('entity_type_id')), array('entity_type_id'))
                    ->addForeignKey($this->getFkName($baseTableName, 'entity_type_id', 'eav/entity_type', 'entity_type_id'), 'entity_type_id', $this->getTable('eav/entity_type'), 'entity_type_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
                    ->setComment('Seller Entity Main Table');

            $tables[$this->getTable($baseTableName)] = $mainTable;
        }

        $types = array();
        if (!$isNoDefaultTypes) {
            $types = array(
                'datetime' => array(Varien_Db_Ddl_Table::TYPE_DATETIME, null),
                'decimal' => array(Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4'),
                'int' => array(Varien_Db_Ddl_Table::TYPE_INTEGER, null),
                'text' => array(Varien_Db_Ddl_Table::TYPE_TEXT, '64k'),
                'varchar' => array(Varien_Db_Ddl_Table::TYPE_TEXT, '255'),
                'char' => array(Varien_Db_Ddl_Table::TYPE_TEXT, '255')
            );
        }

        if (!empty($customTypes)) {
            foreach ($customTypes as $type => $fieldType) {
                if (count($fieldType) != 2) {
                    throw Mage::exception('Mage_Eav', Mage::helper('eav')->__('Wrong type definition for %s', $type));
                }
                $types[$type] = $fieldType;
            }
        }

        /**
         * Create table array($baseTableName, $type)
         */
        foreach ($types as $type => $fieldType) {
            $eavTableName = array($baseTableName, $type);

            $eavTable = $connection->newTable($this->getTable($eavTableName));
            $eavTable
                    ->addColumn('value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                            ), 'Value Id')
                    ->addColumn('entity_type_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                        'unsigned' => true,
                        'nullable' => false,
                        'default' => '0',
                            ), 'Entity Type Id')
                    ->addColumn('attribute_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                        'unsigned' => true,
                        'nullable' => false,
                        'default' => '0',
                            ), 'Attribute Id')
                    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                        'unsigned' => true,
                        'nullable' => false,
                        'default' => '0',
                            ), 'Store Id')
                    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
                        'unsigned' => true,
                        'nullable' => false,
                        'default' => '0',
                            ), 'Entity Id')
                    ->addColumn('value', $fieldType[0], $fieldType[1], array(
                        'nullable' => false,
                            ), 'Attribute Value')
                    ->addIndex($this->getIdxName($eavTableName, array('entity_type_id')), array('entity_type_id'))
                    ->addIndex($this->getIdxName($eavTableName, array('attribute_id')), array('attribute_id'))
                    ->addIndex($this->getIdxName($eavTableName, array('store_id')), array('store_id'))
                    ->addIndex($this->getIdxName($eavTableName, array('entity_id')), array('entity_id'));
            
            $eavTable
                    ->addForeignKey($this->getFkName($eavTableName, 'entity_id', $baseTableName, 'entity_id'), 'entity_id', $this->getTable($baseTableName), 'entity_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
                    ->addForeignKey($this->getFkName($eavTableName, 'entity_type_id', 'eav/entity_type', 'entity_type_id'), 'entity_type_id', $this->getTable('eav/entity_type'), 'entity_type_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
                    ->addForeignKey($this->getFkName($eavTableName, 'store_id', 'core/store', 'store_id'), 'store_id', $this->getTable('core/store'), 'store_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
                    ->setComment('Eav Entity Value Table');

            $tables[$this->getTable($eavTableName)] = $eavTable;
        }

        // DDL operations are forbidden within transactions
        // See Varien_Db_Adapter_Pdo_Mysql::_checkDdlTransaction()
        try {
            foreach ($tables as $tableName => $table) {
                $connection->createTable($table);
                if($tableName != $baseTableName){
                    $indexName = $this->getIdxName($tableName,array('entity_id','attribute_id','store_id'),Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);
                    $connection->addIndex($tableName, $indexName, array('entity_id','attribute_id','store_id'),Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);
                }
            }
        } catch (Exception $e) {
            throw Mage::exception('Mage_Eav', Mage::helper('eav')->__('Can\'t create table: %s', $tableName));
        }

        return $this;
    }

}
