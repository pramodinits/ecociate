<?php

$entityTypeId = Mage::getModel('eav/entity')
        ->setType('catalog_product')
        ->getTypeId();
$attributeSetIds = Mage::getModel('eav/entity_attribute_set')
                ->getCollection()
                ->addFieldToSelect('attribute_set_id')
                ->setEntityTypeFilter($entityTypeId)->getData();
$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$installer->startSetup();


/* Username field to Customer*/

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId = $setup->getEntityTypeId('customer');
$attributeSetId = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$installer->addAttribute("customer", "username", array(
    "type" => "varchar",
    "backend" => "",
    "label" => "Username",
    "input" => "text",
    "source" => "",
    'global' => true,
    "visible" => true,
    "required" => true,
    'visible_on_front' => true,
    "default" => "",
    "frontend" => "",
    "unique" => false,
    'user_defined' => 0,
));
$attribute = Mage::getSingleton("eav/config")->getAttribute("customer", "username");
$setup->addAttributeToGroup(
        $entityTypeId, $attributeSetId, $attributeGroupId, 'username', '999'  //sort_order
);

$used_in_forms = array();

$used_in_forms[] = "adminhtml_customer";    
$used_in_forms[]="checkout_register";
$used_in_forms[]="customer_account_create";
$used_in_forms[]="customer_account_edit";
//$used_in_forms[]="adminhtml_checkout";
$attribute->setData("used_in_forms", $used_in_forms)
        ->setData("is_used_for_customer_segment", true)
        ->setData("is_system", 0)
        ->setData("is_user_defined", 1)
        ->setData("is_visible", 1)
        ->setData("sort_order", 100)
;
$attribute->save();
/* End Username field to Customer*/

$installer->endSetup();

$installer_core = $this;
$installer_core->startSetup();
$installer_core->createEntityTables(
    $this->getTable('arccore/arc_entity')
);
$installer_core->addEntityType('fedobe_arccore_arc',Array(
    'entity_model'          =>'arccore/arc',
    'attribute_model'       =>'',
    'table'                 =>'arccore/arc_entity',
    'increment_model'       =>'',
    'increment_per_store'   =>'0'
));
$installer_core->run("
    DROP TABLE IF EXISTS {$this->getTable('arc_eav_attribute')};
CREATE TABLE IF NOT EXISTS {$this->getTable('arc_eav_attribute')} (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `frontend_input_renderer` varchar(255) DEFAULT NULL COMMENT 'Frontend Input Renderer',
  `is_global` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Global',
  `is_visible` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Visible',
  `is_searchable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Searchable',
  `is_filterable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable',
  `is_html_allowed_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is HTML Allowed On Front',
  `is_filterable_in_search` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable In Search',
  `is_visible_in_advanced_search` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible In Advanced Search',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_wysiwyg_enabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is WYSIWYG Enabled',
  `is_used_for_promo_rules` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Promo Rules',
  PRIMARY KEY (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Arc EAV Attribute Table';

DROP TABLE IF EXISTS {$this->getTable('arc_entity_media_gallery')};
CREATE TABLE IF NOT EXISTS {$this->getTable('arc_entity_media_gallery')}(
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  KEY `IDX_TRIBE_ARC_ENTITY_MEDIA_GALLERY_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_TRIBE_ARC_ENTITY_MEDIA_GALLERY_ENTITY_ID` (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Arc Media Gallery Attribute Backend Table';

ALTER TABLE {$this->getTable('arc_entity_media_gallery')}
  ADD CONSTRAINT `FK_ARC_ENTT_MDA_GLR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ARC_ENTT_MDA_GLR_ENTT_ID_ARC_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `arc_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;


DROP TABLE IF EXISTS {$this->getTable('arc_entity_media_gallery_value')};
CREATE TABLE IF NOT EXISTS {$this->getTable('arc_entity_media_gallery_value')} (
  `value_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Value ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `label` varchar(255) DEFAULT NULL COMMENT 'Label',
  `position` int(10) unsigned DEFAULT NULL COMMENT 'Position',
  `disabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Disabled',
  PRIMARY KEY (`value_id`,`store_id`),
  KEY `IDX_TRIBE_ARC_ENTITY_MEDIA_GALLERY_VALUE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Arc Media Gallery Attribute Value Table';

ALTER TABLE {$this->getTable('arc_entity_media_gallery_value')}
  ADD CONSTRAINT `FK_ARC_ENTT_MDA_GLR_VAL_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_ARC_ENTT_MDA_GLR_VAL_VAL_ID_ARC_ENTT_MDA_GLR_VAL_ID` FOREIGN KEY (`value_id`) REFERENCES `arc_entity_media_gallery` (`value_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");
  
//$installer_core->installEntities();
$installer_core->endSetup();
?>