<?php

class Fedobe_Tribecustomer_Block_Favorited extends Mage_Core_Block_Template {
    
     protected function _construct(){
        parent::_construct();
        $this->setTemplate('fedobe/tribe/customer/favorited.phtml');
    }
    
    public function getfavstore(){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $tableName = $resource->getTableName('customer_entity_text');
        $profiles = array();
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {
       $customerData = Mage::getSingleton('customer/session')->getCustomer();
       $customer_id = $customerData->getId();
        }
        $attrid = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('customer','favourite_store');//'363';//temporary hardcoded
        $query = "SELECT `entity_id`,`value` FROM $tableName WHERE entity_id='".$customer_id."' AND attribute_id ='".$attrid."'";
        $customerdata = $readConnection->fetchAll($query);
        
        $fav_stores = $customerdata['0']['value'];
        if($fav_stores){
        $seller_query = "SELECT `seller_store_name`,`entity_id` FROM `tribe_seller_entity` WHERE `entity_id` IN($fav_stores)";
        $seller_querydata = $readConnection->fetchAll($seller_query);
        }else{
           $seller_querydata = 1; 
        }
        return $seller_querydata;
        
    }
}
?>