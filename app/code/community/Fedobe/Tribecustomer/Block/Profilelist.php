<?php

class Fedobe_Tribecustomer_Block_Profilelist extends Mage_Core_Block_Template { 

public function getlist(){
    $customerprofile_list = array(64,318,319,335,79);
    $data = Mage::getModel('arccore/arc')->getCollection()
                      ->addAttributeToSelect('*')
                     ->addAttributeTofilter('entity_id',$customerprofile_list)->load();
                    
    $customerdata = array();
    foreach($data as $csdata){
        $id = $csdata->getEntityId();
        $customerdata[$id]['name'] = $csdata->getTrcrUsername();
        $customerdata[$id]['image'] = Mage::getBaseUrl('media').'arccore/arcs/images'.$csdata->getTrcrProfilePic();
        $customerdata[$id]['profile'] = Mage::getBaseUrl().$csdata->getTrcrUsername();
    }
   return $customerdata;
}
}
?>