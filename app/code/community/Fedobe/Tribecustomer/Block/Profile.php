<?php

class Fedobe_Tribecustomer_Block_Profile extends Mage_Core_Block_Template {
    
     protected function _construct(){
        parent::_construct();
        $this->setTemplate('fedobe/tribe/customer/profile.phtml');
    }
    public function getattributes(){
        $attributes = array();
        if(!Mage::helper('tribecore')->is_seller()){
        $allowedattributes = $this->allowedattributes();
        $attribute_set_id = Mage::helper('tribecustomer')->getAttributeSetId();
        $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
                ->setAttributeSetFilter($attribute_set_id)
                ->addFieldToFilter('attribute_group_name', array('in' => array('Profile','Content')))
                ->setSortOrder()
                ->load();
        $attributes = array();
        foreach ($groupCollection as $group) {
            $attributeCollection = Mage::getResourceModel('eav/entity_attribute_collection')
                    ->setAttributeGroupFilter($group->getId());
            foreach ($attributeCollection as $attribute) {
                if(in_array($attribute->getAttributeCode(),$allowedattributes)){
                    if ($attribute->getFrontendInput() == 'textarea') {
                        $attr_id = $attribute->getAttributeId();
                        $addtionaldata = Mage::getModel('arccore/attributedata')->load($attr_id);
                        $attributes[$group->getAttributeGroupName()][] = array_merge($attribute->getData(), $addtionaldata->getData());
                    } else {
                        $attributes[$group->getAttributeGroupName()][] = $attribute->getData();
                    }
                }
            }
        }
        }
        return $attributes;
    }
    public function allowedattributes(){
        return array('trcr_profile_pic','trcr_cover_pic','trcr_bio','trcr_name','trcr_username');
    }
    public function getcustomerdata(){
        $customer_profile = $customer = $arc = array();
        $customer = Mage::getSingleton('customer/session')->getCustomer()->getData();
        
        $seller_profile = Mage::helper('tribecore')->is_seller();
        $entityTypeId =Mage::helper('arccore')->getEntityTypeId('arccore');
        $customer_profile = Mage::helper('tribecore')->getprofile_by_emailoption($customer['entity_id']);
        if(!empty($customer_profile) && !$seller_profile){
        $arc = Mage::getModel('arccore/arc')
                ->setStoreId($this->getRequest()->getParam('store', 0))
                ->setEntityTypeId($entityTypeId);
       $arc->load($customer_profile['entity_id']);
       $arcdata = $arc->getData();
        }
        return $arcdata;
    }
    
}
?>