<?php

class Fedobe_Tribecustomer_Block_Adminhtml_Customerattribute extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_customerattribute';
        $this->_blockGroup = 'tribecustomer';
        $this->_headerText = Mage::helper('tribecustomer')->__('Manage Attributes');
        $this->_addButtonLabel = Mage::helper('tribecustomer')->__('Add New Attribute');
        parent::__construct();
    }

}
