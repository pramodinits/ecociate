<?php

class Fedobe_Tribecustomer_Block_Adminhtml_Customer_Profile_Grid extends Fedobe_Arccore_Block_Adminhtml_Arccore_Grid {
    
     public function __construct() {
        parent::__construct();
        $this->setAttributeSetId(Mage::helper('tribecustomer')->getAttributeSetId());// echo Mage::helper('tribecustomer')->getAttributeSetId();exit;
    }

    /**
     * Prepare customer attributes grid collection object
     *
     * @return Fedobe_Tribephotos_Block_Adminhtml_Photo_Grid
     */
    protected function _prepareCollection() {
        $collection = Mage::getModel('arccore/arc')->getCollection()->addAttributeToSelect('*');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
   protected function _prepareColumns() {
        $this->addColumn('entity_id', array(
            'header' => Mage::helper('tribecustomer')->__('Profile ID'),
            'width' => '50px',
            'type' => 'number',
            'index' => 'entity_id',
        ));

         $this->addColumn('trcr_username', array(
            'header'    => Mage::helper('tribecustomer')->__('Name'),
            'index'     => 'trcr_username'
        ));


        $attributeSetId = Mage::helper('tribecustomer')->getAttributeSetId();
        $this->addColumn('action', array(
            'header' => Mage::helper('tribecustomer')->__('Action'),
            'width' => '50px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('tribecustomer')->__('Edit'),
                    'url' => array(
                        'base' => '*/*/edit/set/' . $attributeSetId,
                        'params' => array('store' => $this->getRequest()->getParam('store'))
                    ),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
        ));


        return parent::_prepareColumns();
    }
}
