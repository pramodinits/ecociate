<?php

class Fedobe_Tribecustomer_Block_Adminhtml_Customer_Edit extends Mage_Adminhtml_Block_Customer_Edit {
   
    public function getHeaderText() {
        if (Mage::registry('current_customer')->getId()) {
            return $this->escapeHtml(Mage::registry('current_customer')->getName()) . " (" . $this->getAttributeSetName() . " )";
        } else {
            return Mage::helper('tribecustomer')->__('New Customer') . " (" . $this->getAttributeSetName() . " )";
        }
    }

    public function getAttributeSetName($setid) {
        $attrsetid = ($setid) ? $setid : Mage::registry('current_customer')->getAttributeSetId();
        $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
        $attributeSetModel->load($attrsetid);
        return $this->escapeHtml($attributeSetModel->getAttributeSetName());
    }

}
