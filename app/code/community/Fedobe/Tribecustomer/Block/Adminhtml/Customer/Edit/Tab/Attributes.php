<?php

class Fedobe_Tribecustomer_Block_Adminhtml_Customer_Edit_Tab_Attributes extends Mage_Adminhtml_Block_Widget_Form {

    /**
     * Prepare attributes form
     *
     * @return null
     */
    protected function _prepareForm() {
        $group = $this->getGroup();
        if ($group) {
            $form = new Varien_Data_Form();
            $form->setHtmlIdPrefix('_'.  strtolower($group->getAttributeGroupName()));
            $form->setFieldNameSuffix('account');
            // Initialize product object as form property to use it during elements generation
            $form->setDataObject(Mage::registry('current_customer'));

            $fieldset = $form->addFieldset('group_fields' . $group->getId(), array(
                'legend' => Mage::helper('tribecustomer')->__($group->getAttributeGroupName()),
                'class' => 'fieldset-wide'
            ));

            $attributes = $this->getGroupAttributes();
            $this->_setFieldset($attributes, $fieldset, array('gallery'));
           
            $values = Mage::registry('current_customer')->getData();
            // Set default attribute values for new product
            if (!Mage::registry('current_customer')->getId()) {
                foreach ($attributes as $attribute) {
                    if (!isset($values[$attribute->getAttributeCode()])) {
                        $values[$attribute->getAttributeCode()] = $attribute->getDefaultValue();
                    }
                }
            }

            if (Mage::registry('current_customer')->hasLockedAttributes()) {
                foreach (Mage::registry('product')->getLockedAttributes() as $attribute) {
                    $element = $form->getElement($attribute);
                    if ($element) {
                        $element->setReadonly(true, true);
                    }
                }
            }
            $form->addValues($values);
            Mage::dispatchEvent('adminhtml_tribecustomer_edit_prepare_form', array('form' => $form));
            $this->setForm($form);
        }
    }

}