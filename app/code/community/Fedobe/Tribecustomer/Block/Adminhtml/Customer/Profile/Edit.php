<?php

class Fedobe_Tribecustomer_Block_Adminhtml_Customer_Profile_Edit extends Fedobe_Arccore_Block_Adminhtml_Arc_Edit {

    public function __construct() {
        parent::__construct();
        $this->_controller = 'tribe_customer';
        $this->_blockGroup = 'tribecustomer';
    }

}
