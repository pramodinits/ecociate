<?php

class Fedobe_Tribecustomer_Block_Adminhtml_Customer_Edit_Tabs extends Mage_Adminhtml_Block_Customer_Edit_Tabs {

    protected $_attributeTabBlock = 'arccore/adminhtml_customer_edit_tab_attributes';

    protected function _prepareLayout() {
        $entityTypeId = Mage::registry('current_customer')->getEntityTypeId();
        $setId = Mage::registry('current_customer')->getAttributeSetId();
        $storeId = 0;
        if ($setId) {
            $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
                    ->setAttributeSetFilter($setId)
                    ->setSortOrder()
                    ->load();
            foreach ($groupCollection as $group) {
                if (strtolower($group->getAttributeGroupName()) != 'general') {
                    $attributeCollection = Mage::getResourceModel('eav/entity_attribute_collection')
                            ->setAttributeGroupFilter($group->getId());
                    $attributes = array();
                    foreach ($attributeCollection as $attribute) {
                        $attributes[] = $attribute;
                    }
                    $this->addTabAfter('group_' . $group->getId(), array(
                        'label' => Mage::helper('tribecustomer')->__($group->getAttributeGroupName()),
                        'class' => Mage::helper('tribecustomer')->__($group->getAttributeGroupName()),
                        'content' => $this->_translateHtml($this->getLayout()->createBlock($this->getAttributeTabBlock(), 'tribeseller.adminhtml.sellers.edit.tab.attributes')->setGroup($group)
                                        ->setGroupAttributes($attributes)
                                        ->setAttributeSetId($setId)
                                        ->setTypeId($entityTypeId)
                                        ->toHtml()),
                            ), 'addresses');
                }
            }
        }

        return parent::_prepareLayout();
    }

    /**
     * Getting attribute block name for tabs
     *
     * @return string
     */
    public function getAttributeTabBlock() {
        return $this->_attributeTabBlock;
    }

    public function setAttributeTabBlock($attributeTabBlock) {
        $this->_attributeTabBlock = $attributeTabBlock;
        return $this;
    }

    /**
     * Translate html content
     *
     * @param string $html
     * @return string
     */
    protected function _translateHtml($html) {
        Mage::getSingleton('core/translate_inline')->processResponseBody($html);
        return $html;
    }

}
