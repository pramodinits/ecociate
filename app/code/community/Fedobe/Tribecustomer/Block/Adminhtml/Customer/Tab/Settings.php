<?php

class Fedobe_Tribecustomer_Block_Adminhtml_Customer_Tab_Settings extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareLayout() {
        $this->setChild('continue_button', $this->getLayout()->createBlock('adminhtml/widget_button')
                        ->setData(array(
                            'label' => Mage::helper('tribecustomer')->__('Continue'),
                            'onclick' => "setSettings('" . $this->getContinueUrl() . "','attribute_set_id','product_type')",
                            'class' => 'save'
                        ))
        );
        
        return parent::_prepareLayout();
    }

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('settings', array('legend' => Mage::helper('tribecustomer')->__('Customer Attribute set Settings')));
        
        $entityTypeid = Mage::helper('tribecustomer')->getEntityTypeId('customer');
        $attrsetcolletion = Mage::getResourceModel('eav/entity_attribute_set_collection')
                    ->setEntityTypeFilter($entityTypeid)->load();
        
        
        $customertattrsets = $attrsetcolletion->toOptionArray();
        
        $fieldset->addField('attribute_set_id', 'select', array(
            'label' => Mage::helper('tribecustomer')->__('Attribute Set'),
            'title' => Mage::helper('tribecustomer')->__('Attribute Set'),
            'name' => 'set',
            'values' => $customertattrsets
        ));

        $button = $fieldset->addField('continue_button', 'note', array(
            'text' => $this->getChildHtml('continue_button'),
        ));

        $button->setAfterElementHtml($this->getLayout()->createBlock('core/template')->setTemplate("fedobe/customer/customer/adminhtml/cusomerjs.phtml")->toHtml());
        $this->setForm($form);
    }

    public function getContinueUrl() {
        return $this->getUrl('*/*/new', array(
                    '_current' => true,
                    'set' => '{{attribute_set}}'
        ));
    }

}
