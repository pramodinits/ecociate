<?php

class Fedobe_Tribecustomer_Block_Adminhtml_Customerattribute_Edit_Tab_Main extends Fedobe_Tribecustomer_Block_Adminhtml_Customerattribute_Edit_Main_Main {

    protected function _prepareForm() {
        parent::_prepareForm();
        $attributeObject = $this->getAttributeObject();
        /* @var $form Varien_Data_Form */
        $form = $this->getForm();
        /* @var $fieldset Varien_Data_Form_Element_Fieldset */
        $fieldset = $form->getElement('base_fieldset');

        // frontend properties fieldset
        $fieldset = $form->addFieldset('front_fieldset', array('legend' => Mage::helper('tribecustomer')->__('Frontend Properties')));
        $fieldset->addField('sort_order', 'text', array(
            'name' => 'sort_order',
            'label' => Mage::helper('tribecustomer')->__('Sort Order'),
            'title' => Mage::helper('tribecustomer')->__('Sort Order'),
            'note' => Mage::helper('tribecustomer')->__('The order to display attribute on the frontend'),
            'class' => 'validate-digits',
        ));

        $usedInForms = $attributeObject->getUsedInForms();

        $fieldset->addField('customer_account_create', 'checkbox', array(
            'name' => 'customer_account_create',
            'checked' => in_array('customer_account_create', $usedInForms) ? true : false,
            'value' => '1',
            'label' => Mage::helper('tribecustomer')->__('Show on the Customer Account Create Page'),
            'title' => Mage::helper('tribecustomer')->__('Show on the Customer Account Create Page'),
        ));

        $fieldset->addField('customer_account_edit', 'checkbox', array(
            'name' => 'customer_account_edit',
            'checked' => in_array('customer_account_edit', $usedInForms) ? true : false,
            'value' => '1',
            'label' => Mage::helper('tribecustomer')->__('Show on the Customer Account Edit Page'),
            'title' => Mage::helper('tribecustomer')->__('Show on the Customer Account Edit Page'),
        ));

        $fieldset->addField('adminhtml_customer', 'checkbox', array(
            'name' => 'adminhtml_customer',
            'checked' => in_array('adminhtml_customer', $usedInForms) ? true : false,
            'value' => '1',
            'label' => Mage::helper('tribecustomer')->__('Show on the Admin Manage Customers'),
            'title' => Mage::helper('tribecustomer')->__('Show on the Admin Manage Customers'),
            'note' => Mage::helper('tribecustomer')->__('Show on the Admin Manage Customers Add and Edit customer Page'),
        ));
        return $this;
    }

}
