<?php

class Fedobe_Tribecustomer_Block_Adminhtml_Customerattribute_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        /**
         * This variable is used in the form URL’s. 
         * This variable has the forms entity primary key, e.g the delete button URL would be 
         * module/controller/action/$this->_objectid/3
         */
        $this->_objectId = 'attribute_id';

        
        $this->_blockGroup = 'tribecustomer';
        $this->_controller = 'adminhtml_customerattribute';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('tribecustomer')->__('Save Attribute'));
        $this->_updateButton('save', 'onclick', 'saveAttribute()');

        if (!Mage::registry('customerattribute_data')->getIsUserDefined()) {
            $this->_removeButton('delete');
        } else {
            $this->_updateButton('delete', 'label', Mage::helper('tribecustomer')->__('Delete Attribute'));
        }

        $this->_addButton(
                'saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
                ), 100
        );
        /*
          $this->_formScripts[] = "
          function saveAndContinueEdit(){
          editForm.submit($('edit_form').action+'back/edit/');
          }
          ";
         */
    }

    /**
     * This function return’s the Text to display as the form header.
     */
    public function getHeaderText() {
        if (Mage::registry('customerattribute_data')->getId()) {
            $frontendLabel = Mage::registry('customerattribute_data')->getFrontendLabel();
            if (is_array($frontendLabel)) {
                $frontendLabel = $frontendLabel[0];
            }
            return Mage::helper('tribecustomer')->__('Edit Customer Attribute "%s"', $this->escapeHtml($frontendLabel));
        } else {
            return Mage::helper('tribecustomer')->__('New Customer Attribute');
        }
    }

    public function getValidationUrl() {
        return $this->getUrl('*/*/validate', array('_current' => true));
    }

    public function getSaveUrl() {
        return $this->getUrl('*/' . $this->_controller . '/save', array('_current' => true, 'back' => null));
    }

}