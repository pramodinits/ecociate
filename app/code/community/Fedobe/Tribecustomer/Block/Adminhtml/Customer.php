<?php
/**
 * Grid container file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribecustomer
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Tribecustomer_Block_Adminhtml_Customer extends Mage_Adminhtml_Block_Widget_Grid_Container
{       
  public function __construct()
  {
    /*both these variables tell magento the location of our Grid.php(grid block) file.
     * $this->_blockGroup.'/' . $this->_controller . '_grid'
     * i.e clarion_Customerattribute/adminhtml_customerattribute_grid
     * $_blockGroup - is your module's name.
     * $_controller - is the path to your grid block. 
     */
    
    $this->_controller = 'adminhtml_customer_profile';
    $this->_blockGroup = 'tribecustomer';
    
    $this->_headerText = Mage::helper('tribecustomer')->__('Manage Customer');
    $this->_addButtonLabel = Mage::helper('tribecustomer')->__('Add New Customer');
    parent::__construct();
    $this->_removeButton('add');
  }
}