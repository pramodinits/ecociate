<?php
class Fedobe_Tribecustomer_Adminhtml_CustomerprofileController extends Mage_Adminhtml_Controller_Action {
public function preDispatch() {
        $bypassaction = array('addattribute');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }
function addattributeAction(){
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId = $setup->getEntityTypeId('customer');
$attributeSetId = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$installer->addAttribute("customer", "username", array(
    "type" => "varchar",
    "backend" => "",
    "label" => "Username",
    "input" => "text",
    "source" => "",
    'global' => true,
    "visible" => true,
    "required" => true,
    'visible_on_front' => true,
    "default" => "",
    "frontend" => "",
    "unique" => false,
    'user_defined' => 0,
));
$attribute = Mage::getSingleton("eav/config")->getAttribute("customer", "username");
$setup->addAttributeToGroup(
        $entityTypeId, $attributeSetId, $attributeGroupId, 'username', '999'  //sort_order
);

$used_in_forms = array();

$used_in_forms[] = "adminhtml_customer";    
$used_in_forms[]="checkout_register";
$used_in_forms[]="customer_account_create";
$used_in_forms[]="customer_account_edit";
//$used_in_forms[]="adminhtml_checkout";
$attribute->setData("used_in_forms", $used_in_forms)
        ->setData("is_used_for_customer_segment", true)
        ->setData("is_system", 0)
        ->setData("is_user_defined", 1)
        ->setData("is_visible", 1)
        ->setData("sort_order", 100)
;
$attribute->save();
}
    
}
