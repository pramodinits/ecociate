<?php

/**
 * Manage customer Attribute controller file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribecustomer
 * @author      Fedobe Magento Team
 */
require_once Mage::getBaseDir('code').DS.'community/Fedobe/Arccore/controllers/Adminhtml/ProfilesController.php';
class Fedobe_Tribecustomer_Adminhtml_Tribe_CustomerController extends Fedobe_Arccore_Adminhtml_ProfilesController {

    protected $_entityTypeId;

    public function preDispatch() {
        parent::preDispatch();
        $this->_entityTypeId = Mage::helper('arccore')->getEntityTypeId('arccore');
    }

    /**
     * Init actions
     *
     */
    protected function _initAction() {
        // load layout, set active menu and breadcrumbs

        $this->_title($this->__('Tribe'))
                ->_title($this->__('Manage Customers'));
 
        if ($this->getRequest()->getParam('popup')) {
            $this->loadLayout('popup');
        } else {
            $this->loadLayout()
                    ->_setActiveMenu('fedobe/tribecustomer_manage_customer');
        }
        return $this;
    }

    /**
     * Index action method
     */
    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }

    /**
     * Code to display the form
     * 
     * The main form container gets added to the content and the tabs block gets added to left.
     */
    public function newAction() {
        $attrsetid = $this->getRequest()->getParam('set');
        if ($attrsetid) {
            $this->_forward('edit');
        } else {
            $this->loadLayout();
            $this->renderLayout();
        }
    }

    /**
     * Edit Customer Attribute
     */
    public function editAction() {
         parent::editAction();
         $id = $this->getRequest()->getParam('id');
         $this->_initAction()
               ->_addBreadcrumb(
                        $id ? Mage::helper('tribecustomer')->__('Edit') : Mage::helper('tribecustomer')->__('New'), $id ? Mage::helper('tribecustomer')->__('Edit') : Mage::helper('tribecustomer')->__('New'));
        $arc = parent::loadProfile();
        if($id)
         $this->_title($this->__($arc->getTrrvName()));
        else
         $this->_title($this->__('New Customer'));
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->renderLayout();
    }

    public function validateAction() {
        parent::validateAction();
    }

    /**
     * Delete tribecustomer attribute
     *
     * @return null
     */
    public function deleteAction() {
        if ($id = $this->getRequest()->getParam('id')) {
            $res = parent::deleteAction();
            
             if($res['success']){
                 Mage::getSingleton('adminhtml/session')->addSuccess($res['message']);
                 $this->_redirect('*/*/');
                 return;
             }else{
                  Mage::getSingleton('adminhtml/session')->addError($res['message']);
                  $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('attribute_id')));
                  return;
             }
        }
        Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('tribecustomer')->__('Unable to find a customer to delete.'));
        $this->_redirect('*/*/');
    }
    public function saveAction() {
        $error = 0;
        $data = $this->getRequest()->getPost('arc');
        $Arcdata=$data;
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $storeId = Mage::app()->getRequest()->getParam('store');
        $customermodel = Mage::getModel('customer/customer');
        $email = $data['trcr_email'];
        $customercreate = 0;
        $customer_entity_id = $this->getRequest()->getParam('id');
        $redirectBack = $this->getRequest()->getParam('back', false);
        if ($customer_entity_id) {
            $branddata = Mage::getModel('arccore/arc')->load($customer_entity_id);
            //Here let's check if any User or Customer information changed
            $ext_fname = $branddata->getData('trcr_first_name');
            $ext_lname = $branddata->getData('trcr_last_name');
            $ext_email = $branddata->getData('trcr_email');
            $ext_status = $branddata->getData('trcr_status');
            $ext_password = $branddata->getData('trcr_password');
        }
        $new_fname = $data['trcr_first_name'];
        $new_lname = $data['trcr_last_name'];
        $new_email = $data['trcr_email'];
        $new_password = $data['trcr_password'];
        $new_status = $data['trcr_status'];
        
       // if ($data['trcr_account_type'] == 1) {
        //Here for new, let's check whether exists with the same email
            $tableName = $resource->getTableName('customer_entity');
            if ($customer_entity_id) {
                if ($ext_email != $new_email) {
                    $query = "SELECT `entity_id` FROM $tableName WHERE `email` = '$new_email' LIMIT 1";
                } else {
                    $query = "SELECT `entity_id` FROM $tableName WHERE `email` = '$email' AND `email` <> '$email' LIMIT 1";
                }
            } else {
                $query = "SELECT `entity_id` FROM $tableName WHERE `email` = '$email' LIMIT 1";
            }
            $res = $readConnection->query($query);
            $cust = $res->fetch();//echo "<pre>";print_r($cust);exit;
            $customerdat = array();
            $customerdat['website_id'] = 1;
            $customerdat['firstname'] = $new_fname;
            $customerdat['lastname'] = $new_lname;
            $customerdat['email'] = $email;
            $customerdat['password'] = $new_password;
            if (empty($cust)) {
                //Here let's proceed and create the customer
                $customercreate = 1;
            } else {
                $error = 1;
                Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('tribebrand')->__("A Customer with email '$email' already exists!.")
                );
            }
            if ($customercreate) {
                try {
                    //Here let's create the User
                        if ($customer_entity_id) {//adding new email id in edit case of customer.
                            if (($new_status != $ext_status ) || (($ext_fname != $new_fname) || ($ext_lname != $new_lname) || ($ext_email != $new_email) || ($ext_password != $new_password))) {
                                $query = "SELECT `entity_id` FROM $tableName WHERE `email` = '$ext_email' LIMIT 1";
                                $res = $readConnection->query($query);
                                $cust = $res->fetch();
                                $customer_id = $cust['entity_id']; 
                                //Here let's create the Customer
                                $customer = $customermodel->load($customer_id);
                                $customer->setFirstname($new_fname);
                                $customer->setLastname($new_lname);
                                $customer->setEmail($email);
                                $customer->setPassword($new_password);
                                $customer->save();
                            }
                        } else {
                            //Here let's create the Customer
                            $customermodel->setData($customerdat);
                            $customermodel->save();
                        }
                        $customer_entity_id = $customermodel->getEntityId();
                        $Arcdata['trcr_customer_list'] = $customer_entity_id;
                        $this->getRequest()->setPost('arc',$Arcdata);
                } catch (Exception $e) {
                    $error = 1;
                    Mage::getSingleton('adminhtml/session')->addError(
                            Mage::helper('tribebrand')->__($e->getMessage())
                    );
                }
            }
       // }
         if ($error) {
            if ($customer_entity_id && $redirectBack) {
                $this->_redirect('*/*/edit', array(
                    'id' => $customer_entity_id,
                    '_current' => true
                ));
            } else {
                $this->_redirect('*/*/', array('store' => $storeId));
            }
        } else {
            parent::saveAction();
        }
    }

}
