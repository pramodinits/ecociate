<?php
require_once Mage::getBaseDir('code').DS.'community/Fedobe/Arccore/controllers/Adminhtml/ProfilesController.php';
class Fedobe_Tribecustomer_CustomerprofileController extends Fedobe_Arccore_Adminhtml_ProfilesController {
    protected $_entityTypeId;
    
    public function preDispatch() {
        //parent::preDispatch();
        $this->_entityTypeId = Mage::helper('arccore')->getEntityTypeId('arccore');
        $this->_attributesetId = Mage::helper('tribecustomer')->getAttributeSetId();
        $bypassaction = array('save','checkunique');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }
    
    public function saveAction(){
         $data = $this->getRequest()->getPost();
         $Arcdata=$data['arc'];
          if(isset($data['arc']['trcr_username'] )&& $data['arc']['trcr_username']){
              $Arcdata['trcr_username_default'] = 0;
              $this->getRequest()->setPost('arc',$Arcdata);
          }
          parent::saveAction();
    }
    public function checkuniqueAction(){
         $result = array('msg'=>'notexist');
        $data = $this->getRequest()->getParams();//echo "<pre>";print_r($data);exit;
        $user_name = $data['username'];
        $profile_id = $data['id'];
         $collection_data = Mage::getModel('arccore/arc')->getCollection()
                        ->addAttributeToSelect('*')
                        ->addFieldToFilter('trcr_username',$user_name)
                        ->addFieldToFilter('entity_id',array('neq'=>$profile_id))
                        ->load();
        if($collection_data->getSize()){
          $result = array('msg'=>'exist');
        }
        echo Mage::helper('core')->jsonEncode($result);
        exit;
      
    }
}
?>