<?php

class Fedobe_Tribecustomer_Model_Mysql4_Tribecustomer_Attribute_Collection extends Mage_Customer_Model_Resource_Attribute_Collection {

    public function setAttributeSetFilter($setId) {
        if (is_array($setId)) {
            if (!empty($setId)) {
                $this->join(
                        'entity_attribute', 'entity_attribute.attribute_id = main_table.attribute_id', 'attribute_id'
                );
                $this->addFieldToFilter('entity_attribute.attribute_set_id', array('in' => $setId));
                $this->addAttributeGrouping();
                $this->_useAnalyticFunction = true;
            }
        } elseif ($setId) {
            $this->join(
                    'entity_attribute', 'entity_attribute.attribute_id = main_table.attribute_id'
            );
            $this->addFieldToFilter('entity_attribute.attribute_set_id', $setId);
            $this->setOrder('additional_table.sort_order', self::SORT_ORDER_ASC);
        }
        return $this;
    }

}