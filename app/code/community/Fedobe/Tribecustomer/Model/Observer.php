<?php

class Fedobe_Tribecustomer_Model_Observer {
    public function addButtonCustomer($observer){
    $container = $observer->getBlock();
    $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                        ->load('Tribe Customer', 'attribute_set_name')
                        ->getAttributeSetId();
   // echo $attributeSetId;exit;
    if(null !== $container && $container->getType() == 'tribecustomer/adminhtml_customer') {
        $data_2 = array(
            'label'     => 'Add Customer',
            'class'=>'add',
            'onclick'   => 'setLocation(\' '  . Mage::helper('adminhtml')->getUrl('adminhtml/tribe_customer/new/set/'.$attributeSetId, array('_secure' => true)) . '\')',
        );
        $container->addButton('addcustomer', $data_2);
    }
    return $this;
}
public function createprofile($observer){
    if(Mage::registry('customer_save_observer_executed')){
        return $this; //this method has already been executed once in this request (see comment below)
    }
    $controller_name = Mage::app()->getRequest()->getControllerName();
    if(in_array($controller_name,array('customerprofile','customer','account','facebook','google'))){
        $customer_post_data = Mage::app()->getRequest()->getPost();
   $customerobj = $observer->getCustomer();
   $customer=$customerobj->getData();//echo "<pre>";print_r($customer);exit;
   $profilecollection = Mage::helper('tribecustomer')->getprofile_by_customerlist($customer['entity_id']);
   $profile = $profilecollection->getData();//echo "<pre>";print_r($profile);exit;
   $sellercollection = Mage::helper('tribecore')->getseller_by_customer($customer['entity_id']);
   $seller_profile = array();
   $seller_profile = $sellercollection->getData(); //echo "<pre>";print_r($seller_profile);exit;
   if(empty($profile) && empty($seller_profile)){
  // echo "<pre>";print_r($profile);exit;
   $attribute_set_id = Mage::helper('tribecustomer')->getAttributeSetId(); //echo $attribute_set_id;exit;
   $entityTypeId =Mage::helper('arccore')->getEntityTypeId('arccore');
   $attr_options_id = Mage::helper('arccore')->getattributeoptionsid('trcr_profile_type','Customer');
   $cat_options_id = Mage::helper('arccore')->getattributeoptionsid('trcr_category','Default');
   $status_id = Mage::helper('arccore')->getattributeoptionsid('trcr_status','Active');
   $accnt_options = Mage::helper('arccore')->getattributeoptions('trcr_account_type');
   $default_username_opt = Mage::helper('arccore')->getattributeoptionsid('trcr_username_default','Yes');
   $layout_opt = Mage::helper('arccore')->getattributeoptionsid('trcr_page_layout','1 column'); //echo $layout_opt;exit;
   //echo "<pre>";print_r($default_username_opt);exit;
   $accnt_options = array_flip($accnt_options);
   $accnt_type_id = $accnt_options['Existing'];
   $email_id = Mage::helper('arccore')->getattributeoptionsid('trcr_customer_list',$customer['email']);
   $data = array();
   $data['trcr_profile_type']=$attr_options_id;
   $data['trcr_username']=uniqid();//$customer['username'];
   $data['trcr_username_default']=$default_username_opt;
   $data['trcr_name']=$customer['firstname'].' '.$customer['lastname'];
   $data['trcr_category']=$cat_options_id;
   $data['trcr_status']=$status_id;
   $data['trcr_account_type']=$accnt_type_id;
   $data['trcr_customer_list']=$email_id;
   $data['trcr_first_name']=$customer['firstname'];
   $data['trcr_last_name']=$customer['lastname'];
   $data['trcr_email']=$customer['email'];
   $data['trcr_page_layout']= $layout_opt;
   if(!empty($customer_post_data) && isset($customer_post_data['password']))
   $data['trcr_password']=$customer_post_data['password'];
   if(isset($customer['picture'])){
       
        $img_name = strtolower($customer['username'].$customer['entity_id'].'.jpeg');
        $charc_1 = substr($img_name,0,1);
        $charc_2 =substr($img_name,1,1);
        $folder = Mage::getBaseDir('media') . '/arccore/arcs/images/'.$charc_1.'/'.$charc_2.'/'; //echo $folder;exit;
         if (!file_exists($folder)) {
            mkdir($folder);
            chmod($folder, 0777);
        }
        //echo $charc_1.'--'.$charc_2;exit;
        copy($customer['picture'], $folder.$img_name);//echo 888;exit;
        $data['trcr_profile_pic']='/'.$charc_1.'/'.$charc_2.'/'.$img_name;
   }
    if(isset($customer['cover'])){
       
        $cvrimg_name = strtolower($customer['username'].'cover'.$customer['entity_id'].'.jpeg');
        $cvr_1 = substr($cvrimg_name,0,1);
        $cvr_2 =substr($cvrimg_name,1,1);
        $cvrfolder = Mage::getBaseDir('media') . '/arccore/arcs/images/'.$cvr_1.'/'.$cvr_2.'/';
         if (!file_exists($cvrfolder)) {
            mkdir($cvrfolder);
            chmod($cvrfolder, 0777);
        }
        copy($customer['cover'], $cvrfolder.$cvrimg_name);
        $data['trcr_cover_pic']='/'.$cvr_1.'/'.$cvr_2.'/'.$cvrimg_name;
   }
    $arc = Mage::getModel('arccore/arc')
                ->setStoreId(0)
                ->setEntityTypeId($entityTypeId);
    $arc->setAttributeSetId($attribute_set_id);
     $arc->addData($data); //echo "<pre>";print_r($arc);exit;
     $arc->save();
      Mage::register('customer_save_observer_executed',true); 
   }
  return $this;
   }
}
}
