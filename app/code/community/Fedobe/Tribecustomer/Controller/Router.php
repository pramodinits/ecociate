<?php

class Fedobe_Tribecustomer_Controller_Router extends Mage_Core_Controller_Varien_Router_Standard {

    public function match(Zend_Controller_Request_Http $request) {
        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                    ->setRedirect(Mage::getUrl('install'))
                    ->sendResponse();
            exit;
        }

        $identifier = trim($request->getPathInfo(), '/'); 

        $condition = new Varien_Object(array(
            'identifier' => $identifier,
            'continue' => true
        ));
        Mage::dispatchEvent('cms_controller_router_match_before', array(
            'router' => $this,
            'condition' => $condition
        ));

        $identifier = $condition->getIdentifier();
        $defaultaction = '';
        $customkey = '';
        $allowed_posttypes = Mage::helper('tribecustomer')->getAllowedPosttypes();


   //bhagyashree
if($identifier == "favorited"){
            $request->setModuleName('tribecustomer')
                    ->setControllerName('page')
                    ->setActionName('favorited');
            return true;   
        }
//

        if (!empty($allowed_posttypes)) {
            foreach ($allowed_posttypes as $pk => $pv) {
                $configpath_id = "$pv/general/identifier";
                $url_identifier = Mage::getStoreConfig($configpath_id);// echo $configpath_id.'--'.$url_identifier."<br>";
                if ($url_identifier) {
                    $pos = strpos($identifier, $url_identifier);
                    if ($pos !== FALSE) {
                        $post_type_str = substr($identifier, $pos, strlen(trim($identifier, '/')));
                        //Here to check for individual Notes or any individual post type
                        if (strlen($post_type_str) > strlen($url_identifier)) {
                            $single_configpath_id = "$pv/general/single_url_suffix";
                            $single_url_suffix = Mage::getStoreConfig($single_configpath_id);
                            $indurl_key = str_replace("$url_identifier/", "", str_replace($single_url_suffix, '', $post_type_str));
                            $defaultaction = 'single' . $url_identifier;
                            $customkey = $indurl_key;
                            $identifier = trim(substr($identifier, 0, $pos), '/');
                        } else {
                            $defaultaction = $url_identifier;
                        }
                        $identifier = trim(substr($identifier, 0, $pos), '/');
                    }
                }
            }
        }//exit;
        if ($condition->getRedirectUrl()) {
            Mage::app()->getFrontController()->getResponse()
                    ->setRedirect($condition->getRedirectUrl())
                    ->sendResponse();
            $request->setDispatched(true);
            return true;
        }
        if (!$condition->getContinue()) {
            return false;
        }
        $customer = Mage::getModel('tribecustomer/customer');
        $customerId = $customer->checkIdentifier($identifier, Mage::app()->getStore()->getId());
        if (!$customerId) {
            if($identifier!='profile')
            return false;
        }
        if($identifier=='profile'){
        $defaultaction='myprofile';
        }
        if (!$defaultaction) {
            $allowed_tabs = Mage::helper('tribecustomer')->getAllowedPosttypes();
            if (!empty($allowed_tabs)) {
                $defaultaction = $allowed_tabs[0];
            } else {
                $defaultaction = 'notes';
            }
        } 
        $attributesetid = Mage::helper('tribecustomer')->getAttributeSetId();
        $request->setModuleName('tribecustomer')
                ->setControllerName('page')
                ->setActionName($defaultaction)
                ->setParam('attribute_set_id', $attributesetid)
                ->setParam('brand_id', $customerId);
        if ($customkey) { //echo $customkey;exit;
            $request->setParam('singlepostkey', $customkey);
        }
        $request->setAlias(
                Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS, $identifier
        );
         //echo $identifier.'--'.$defaultaction;exit;
        return true;
    }

}