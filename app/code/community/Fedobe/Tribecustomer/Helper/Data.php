<?php
class Fedobe_Tribecustomer_Helper_Data extends Mage_Core_Helper_Abstract {
      const XML_PATH_VALIDATOR_DATA_INPUT_TYPES = 'general/validator_data/input_types';
      const ATTRIBUTESET_NAME = 'Tribe Customer';
    protected $_attributesLockedFields = array();
    protected $_entityTypeFrontendClasses = array();
    public function getEntityTypeId($module = 'customerattributeset') {
        switch ($module) {
            case 'customerattributeset':
            case 'customer':
                $entitytypeid = Mage::getModel('eav/entity')->setType(Mage::getModel('eav/config')->getEntityType('customer'))->getTypeId();
                break;
            default :
                $entitytypeid = Mage::getModel('eav/entity')->setType(Mage::getModel('eav/config')->getEntityType('catalog_product'))->getTypeId();
        }
        return $entitytypeid;
    }
     protected function _getDefaultFrontendClasses() {
        return array(
            array(
                'value' => '',
                'label' => Mage::helper('eav')->__('None')
            ),
            array(
                'value' => 'validate-number',
                'label' => Mage::helper('eav')->__('Decimal Number')
            ),
            array(
                'value' => 'validate-digits',
                'label' => Mage::helper('eav')->__('Integer Number')
            ),
            array(
                'value' => 'validate-email',
                'label' => Mage::helper('eav')->__('Email')
            ),
            array(
                'value' => 'validate-url',
                'label' => Mage::helper('eav')->__('URL')
            ),
            array(
                'value' => 'validate-alpha',
                'label' => Mage::helper('eav')->__('Letters')
            ),
            array(
                'value' => 'validate-alphanum',
                'label' => Mage::helper('eav')->__('Letters (a-z, A-Z) or Numbers (0-9)')
            )
        );
    }
     public function getFrontendClasses($entityTypeCode) {
        $_defaultClasses = $this->_getDefaultFrontendClasses();
        if (isset($this->_entityTypeFrontendClasses[$entityTypeCode])) {
            return array_merge(
                    $_defaultClasses, $this->_entityTypeFrontendClasses[$entityTypeCode]
            );
        }
        $_entityTypeClasses = Mage::app()->getConfig()
                ->getNode('global/eav_frontendclasses/' . $entityTypeCode);
        if ($_entityTypeClasses) {
            foreach ($_entityTypeClasses->children() as $item) {
                $this->_entityTypeFrontendClasses[$entityTypeCode][] = array(
                    'value' => (string) $item->value,
                    'label' => (string) $item->label
                );
            }
            return array_merge(
                    $_defaultClasses, $this->_entityTypeFrontendClasses[$entityTypeCode]
            );
        }
        return $_defaultClasses;
    }
    public function getAttributeLockedFields($entityTypeCode) {
        if (!$entityTypeCode) {
            return array();
        }
        if (isset($this->_attributesLockedFields[$entityTypeCode])) {
            return $this->_attributesLockedFields[$entityTypeCode];
        }
        $_data = Mage::app()->getConfig()->getNode('global/eav_attributes/' . $entityTypeCode);
        if ($_data) {
            foreach ($_data->children() as $attribute) {
                $this->_attributesLockedFields[$entityTypeCode][(string) $attribute->code] = array_keys($attribute->locked_fields->asArray());
            }
            return $this->_attributesLockedFields[$entityTypeCode];
        }
        return array();
    }
    public function getAttributeSetId() {
        $entityTypeId = Mage::helper('arccore')->getEntityTypeId();
        $attributeSetName = SELF::ATTRIBUTESET_NAME;
        $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                ->getCollection()
                ->setEntityTypeFilter($entityTypeId)
                ->addFieldToFilter('attribute_set_name', $attributeSetName)
                ->getFirstItem()
                ->getAttributeSetId();
        return $attributeSetId;
    }

    public function getProfileType() {
        return 'Customer';
    }

    public function getUrlkeyAttributeCode() {
        return 'trcr_username';
    }

    public function getFrontendProductUrl($key) {
        $urlkey = $key;
        $preffix = Mage::getStoreConfig('tribe_customer/edit/url_prefix');
        $suffix = Mage::getStoreConfig('tribe_customer/edit/url_suffix');
        if ($preffix) {
            $urlkey = "$preffix/$urlkey";
        }
        if ($suffix) {
            $urlkey .=$suffix;
        }
        return trim(Mage::getUrl("$urlkey"), "/");
    }

    public function extractUrlKey($identifier) {
        $key = $identifier;
        $preffix = Mage::getStoreConfig('tribe_customer/edit/url_prefix');
        if ($preffix) {
            $key = str_replace("$preffix/", "", $key);
        }
        $suffix = Mage::getStoreConfig('tribe_customer/edit/url_suffix');
        if ($suffix) {
            $key = str_replace($suffix, "", $key);
        }
        return $key;
    }
     public function getAllowedPosttypes(){
        $final_posttypes = array();
        /*if ($tabs = Mage::getStoreConfig('tribe_brand/tab/posttypes')) {
            $posttypes = Mage::helper('arccore')->attribute_options('trcr_post_type');
            $_options = array();
            foreach ($posttypes as $option) {
                if ($option["value"])
                    $_options[$option["value"]] = $option["label"];
            }
            $tabsarr = explode(',', $tabs);
            if (!empty($tabsarr)) {
                foreach ($tabsarr as $k => $v) {
                    $final_posttypes[$v] = strtolower($_options[$v]);
                }
            }
        }*/
        if ($tabs = Mage::getStoreConfig('tribe_customer/tab/posttypes_taborder')) {
            $final_posttypes = explode(',', $tabs);
            $final_posttypes = array_map('strtolower', $final_posttypes);
        }
        return $final_posttypes;
    }
     public function getBaseCustomerLabel() {
        return $this->__('Early Customer');
    }
    public function getInfo1($data) {
        if (Mage::getStoreConfig('tribe_customer/tab/followers')) {
            $data['followinfo'] = array('count' => 0);
        }
        $tabsarr = $this->getAllowedPosttypes();
        if (!empty($tabsarr)) {
            foreach ($tabsarr as $k => $v) {
                if($v == 'notes'){
                    $status_data = Mage::helper('tribestatus')->getInfo($v,$data);
                }else if($v == 'review'){
                    $status_data = Mage::helper('tribereview')->getInfo($v,$data);
                }else{
                    $status_data = array('label' => $v, 'count' => 0);
                }
                $data['tabs'][$k] = $status_data;
            }
        }
        return $data;
    }
     public function getInfo($data) {
        $data['url'] = $this->getFrontendProductUrl($data[$this->getUrlkeyAttributeCode()]);
        if (Mage::helper('tribecore')->isActionallowed($data)) {
                $data['canbefollowed'] = 1;
        }
        $tabsarr = $this->getAllowedPosttypes();
        if (!empty($tabsarr)) {
            foreach ($tabsarr as $k => $v) {
                $helper = "tribe$v";
                if($v == 'notes')
                    $helper = "tribestatus";
                $status_data = Mage::helper($helper)->getInfo($v,$data);
                $data['tabs'][$k] = $status_data;
            }
        }
        return $data;
    }
    public function getprofile_by_customerlist($option_id){
       $collection = Mage::getModel('arccore/arc')->getCollection()
                      ->addAttributeToSelect('*')
                     ->addAttributeTofilter('trcr_customer_list',$option_id)
                      ->addAttributeTofilter('trcr_profile_type', array('notnull' => true))
                     ->getFirstItem();
         return $collection;
   }
   
   
   
   //added by bhagyashree
   //function is written for checking customer is already have store or not.
    public function customerfavstore($id,$attrid) {
        $customers = array();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $tableName = $resource->getTableName('customer_entity_text');
        $profiles = array();
        $query = "SELECT `entity_id`,`value` FROM $tableName WHERE entity_id='".$id."' AND attribute_id ='".$attrid."'";
        $customerdata = $readConnection->fetchAll($query);
        if (!empty($customerdata)) {
           $ret = $customerdata[0];
        } else {
            $ret = 0;
        }
        return $ret;
}   
//function is written for update customer store info.
    public function updatefavstore($data) {
        //print_r($data);//exit;
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection("core_write");
        $qur_up="UPDATE customer_entity_text SET value='{$data['value']}' WHERE entity_id={$data['entity_id']} AND attribute_id = {$data['attribute_id']};";//print_r($qur_up);exit;
        $inserted =$writeConnection->query($qur_up);
        echo 1;
        
    }

//function is written for insert customer store info.
    public function addfavstore($data) {
        //print_r($data);
        $keys_data = array_keys($data);
        $keys_data_str = implode(',',$keys_data);
        $val_data_str = implode('","',array_values($data));
        
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection("core_write");
        $qur = 'INSERT INTO customer_entity_text('.$keys_data_str.') VALUES ("'.$val_data_str.'")';
        $inserted =$writeConnection->query($qur);
        echo 1; 
    }
    
    //function is written for delete customer store info.
    public function deletefavstore($data) {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection("core_write");
        $delsql = "DELETE FROM customer_entity_text WHERE entity_id={$data['entity_id']} AND attribute_id = {$data['attribute_id']};";
        $writeConnection->query($delsql);
        echo 1; 
    }
    
}
    

?>