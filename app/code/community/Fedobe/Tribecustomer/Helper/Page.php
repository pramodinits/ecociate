<?php

class Fedobe_Tribecustomer_Helper_Page extends Mage_Core_Helper_Abstract{
 public function getProfileType() {
        return 'Customer';
    }
    public function renderPage(Mage_Core_Controller_Front_Action $action, $pageId = null,$attrset_id=null) {
        $this->setHandle('customer_page');
        return $this->_renderPage($action, $pageId,$attrset_id);
    }

}