<?php

class Fedobe_Tribeseller_Model_Tribeseller extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('tribeseller/tribeseller');
    }
    
     public function subscribe($email,$fname,$lname)
    {
         $newsletter = Mage::getModel('newsletter/subscriber');
        $newsletter->loadByEmail($email);
        $customerSession = Mage::getSingleton('customer/session');
        if(!$newsletter->getId()) {
            $newsletter->setSubscriberConfirmCode($newsletter->randomSequence());
        }
//        $isConfirmNeed   = (Mage::getStoreConfig(self::XML_PATH_CONFIRMATION_FLAG) == 1) ? true : false;
        $isConfirmNeed   = false;
//        
        $isOwnSubscribes = false;

        $ownerId = Mage::getModel('customer/customer')
            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
            ->loadByEmail($email)
            ->getId();
        $isSubscribeOwnEmail = $customerSession->isLoggedIn() && $ownerId == $customerSession->getId();

        if (!$newsletter->getId() || $newsletter->getStatus() == self::STATUS_UNSUBSCRIBED
            || $newsletter->getStatus() == self::STATUS_NOT_ACTIVE
        ) {
//            if ($isConfirmNeed === true) {
//                // if user subscribes own login email - confirmation is not needed
//                $isOwnSubscribes = $isSubscribeOwnEmail;
//                if ($isOwnSubscribes == true){
//                    $newsletter->setStatus(self::STATUS_SUBSCRIBED);
//                } else {
//                    $newsletter->setStatus(self::STATUS_NOT_ACTIVE);
//                }
//            } else {
//                $newsletter->setStatus(self::STATUS_SUBSCRIBED);
//            }
            
            $newsletter->setSubscriberEmail($email);
            $newsletter->setFirstName($fname);
            $newsletter->setLastName($lname);
            $newsletter->setMyInserted(1);
        }
        if ($isSubscribeOwnEmail) {
            $newsletter->setStoreId($customerSession->getCustomer()->getStoreId());
            $newsletter->setCustomerId($customerSession->getCustomerId());
        } else {
            $newsletter->setStoreId(Mage::app()->getStore()->getId());
            $newsletter->setCustomerId(0);
        }

        $newsletter->setIsStatusChanged(true);

        try {
            $newsletter->save();
            echo 1;exit;//temporary
            if ($isConfirmNeed === true
                && $isOwnSubscribes === false
            ) {
                $newsletter->sendConfirmationRequestEmail();
            } else {
                $newsletter->sendConfirmationSuccessEmail();echo 1;exit;
            }
echo 1;exit;
            return $newsletter->getStatus();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

}
