<?php

class Fedobe_Tribeseller_Model_Sellers extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('tribeseller/sellers');
    }
    public function loadByUrlKey($urlKey) {
        return $this->load($urlKey, 'seller_url');
    }
}