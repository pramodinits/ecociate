<?php

class Fedobe_Tribeseller_Model_Countries_Source_Details extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    public function getAllOptions($withEmpty = false) {
        $countries = Mage::getResourceModel('directory/country_collection')->loadData()->toOptionArray(false);
        return $countries;
    }

    public function getOptionText($value) {
        $options = $this->getAllOptions(false);

        foreach ($options as $item) {
            if ($item['value'] == $value) {
                return $item['label'];
            }
        }
        return false;
    }

}