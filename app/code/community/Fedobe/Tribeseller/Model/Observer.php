<?php

class Fedobe_Tribeseller_Model_Observer {

    public function getFinalPrice(Varien_Event_Observer $observer) { //echo 234;exit;
        $event = $observer->getEvent();
        $_product = $event->getProduct();
        $qty = $event->getQty();
        if (Mage::registry('current_seller')) {
            if ($sellerproductinfo = Mage::getSingleton('core/session')->getSellerProductInfo()) {
                if (isset($sellerproductinfo[$_product->getId()])) {
                    $selectedAttributes = array();
                    if ($_product->getCustomOption('attributes')) {
                        $selectedAttributes = unserialize($_product->getCustomOption('attributes')->getValue());
                        if (sizeof($selectedAttributes))
                            return $this->getSimpleProductPrice($qty, $_product);
                    }else {
                        $_product->setPrice($sellerproductinfo[$_product->getId()]['converted_price']);
                        $_product->setFinalPrice($sellerproductinfo[$_product->getId()]['converted_price']);
                    }
                }
            }
        } else {
            $sellerid = Mage::app()->getRequest()->getParam('s');
            $assfound = false;
            $sellerinf = Mage::helper('tribeseller')->getCurrentProductSeller($_product, $sellerid);
            $seller = $sellerinf[1];
            $assfound = $sellerinf[0]; //echo $assfound;
            $i = 0;
            if ($assfound) {
                if ($sellerproductinfo = Mage::getSingleton('core/session')->getSellerProductInfo()) {
                    if (isset($sellerproductinfo[$_product->getId()])) {
                        if ($_product->getCustomOption('attributes')) {
                            $selectedAttributes = unserialize($_product->getCustomOption('attributes')->getValue());
                            if (sizeof($selectedAttributes))
                                return $this->getSimpleProductPrice($qty, $_product);
                        }else {
                            $_product->setPrice($sellerproductinfo[$_product->getId()]['converted_price']);
                            $_product->setFinalPrice($sellerproductinfo[$_product->getId()]['converted_price']);
                        }
                    }
                }
            } else {
                //Default seller
                $selectedAttributes = array();
                if ($_product->getCustomOption('attributes')) {
                    $selectedAttributes = unserialize($_product->getCustomOption('attributes')->getValue());

                    if (sizeof($selectedAttributes))
                        return $this->getSimpleProductPrice($qty, $_product);
                }
            }
        }
    }

    public function getSimpleProductPrice($qty = null, $product) {

        $cfgId = $product->getId();
        $product->getTypeInstance(true)
                ->setStoreFilter($product->getStore(), $product);
        $attributes = $product->getTypeInstance(true)
                ->getConfigurableAttributes($product);
        $selectedAttributes = array();
        if ($product->getCustomOption('attributes')) {
            $selectedAttributes = unserialize($product->getCustomOption('attributes')->getValue());
        }
        $db = Mage::getSingleton('core/resource')->getConnection('core_read');
        $dbMeta = Mage::getSingleton('core/resource');
        $sql = <<<SQL
SELECT main_table.entity_id FROM {$dbMeta->getTableName('catalog/product')} `main_table` INNER JOIN
{$dbMeta->getTableName('catalog/product_super_link')} `sl` ON sl.parent_id = {$cfgId}
SQL;
        foreach ($selectedAttributes as $attributeId => $optionId) {
            $alias = "a{$attributeId
                    }";
            $sql .= ' INNER JOIN ' . $dbMeta->getTableName('catalog/product') . "_int" . " $alias ON $alias.entity_id = main_table.entity_id AND $alias.attribute_id = $attributeId AND $alias.value = $optionId AND $alias.entity_id = sl.product_id";
        }
        $id = $db->fetchOne($sql);
        //Mage::log(Mage::getModel("catalog/product")->load($id)->getFinalPrice($qty), null, 'confPricing.log');
        //return
        $fp = Mage::getModel("catalog/product")->load($id)->getFinalPrice($qty);
        return $product->setFinalPrice($fp);
    }

    public function setProductSeller(Varien_Event_Observer $observer) {
        $action = Mage::app()->getFrontController()->getAction();
/// avoid cron jobs interruption
        if (!$action) {
            return;
        }
        $actionsarr = array('checkout_cart_add', 'ajaxcart_index_add', 'sales_order_reorder');
        if (in_array($action->getFullActionName(), $actionsarr)) { // add your add to cart action name.
            $params = Mage::app()->getRequest()->getParams();
            $product_seller = (isset($params['product_seller']) && $params['product_seller']) ? $params['product_seller'] : Mage::helper('tribeseller')->getDefaultSeller();
            
            $item = $observer->getProduct();
            $additionalOptions = array();
/// Add here your additional data
            if (isset($params['product_seller']) && $params['product_seller']) {
                $seller = Mage::getSingleton("tribeseller/seller")->load($params['product_seller']);
            } else {
              //  $_product = Mage::getModel('catalog/product')->load($item->getId());
              // $seller = Mage::helper('tribeseller')->getCurrentProductSeller($_product);
              $_product['id']=$item->getId(); 
              $resource = Mage::getSingleton('core/resource');
              $readConnection = $resource->getConnection('core_read');
              
              $sellerattr_code =Mage::helper('tribeseller')->getSupplierAttributeCode();
              $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $sellerattr_code);
              $attr_id =  $attribute->getId();
              
              $pdata = $readConnection->fetchAll('SELECT value FROM catalog_product_entity_int WHERE attribute_id='.$attr_id.' AND entity_id='.$item->getId());
              $_product['suppliers']=$pdata[0]['value'];
                $seller_arr = Mage::helper('tribeseller')->getCurrentProductSeller($_product);
                $seller = $seller_arr[1];
            }
           $name = $seller->getSellerStoreName();
            $id = $seller->getId();
            $additionalOptions[] = array(
                'label' => Mage::helper('tribeseller')->__('Seller'),
                'value' => $name,
                'tribe_seller_id' => $id,
            );
            $item->addCustomOption('additional_options', serialize($additionalOptions));
        }
    }

    public function setCustomDataOnQuoteItem(Varien_Event_Observer $observer) {
        $quoteItem = $observer->getItem();
        $orderItem = $observer->getOrderItem();
        if ($additionalOptions = $quoteItem->getOptionByCode('additional_options')) {
            $options = $orderItem->getProductOptions();
            $options['additional_options'] = unserialize($additionalOptions->getValue());
            $tribe_seller_id = 0;
            foreach ($options['additional_options'] as $key => $value) {
                if (isset($value['tribe_seller_id'])) {
                    $tribe_seller_id = $value['tribe_seller_id'];
                    break;
                }
            }
            if (!$tribe_seller_id) {
                $_product = Mage::getModel('catalog/product')->load($orderItem->getProduct()->getId());
                $sellerinf = Mage::helper('tribeseller')->getCurrentProductSeller($_product);
                $tribe_seller_id = $sellerinf[1]->getId();
            }
            if ($tribe_seller_id)
                $orderItem->setData('tribe_seller_id', $tribe_seller_id);
            $orderItem->setProductOptions($options);
        }else {
            $_product = Mage::getModel('catalog/product')->load($quoteItem->getProduct()->getId());
            $sellerinfo = Mage::helper('tribeseller')->getCurrentProductSeller($_product);
            $seller = $sellerinfo[1];
            $name = $seller->getSellerStoreName();
            $id = $seller->getId();
            $options = $orderItem->getProductOptions();
            $options['additional_options'][] = array(
                'label' => Mage::helper('tribeseller')->__('Seller'),
                'value' => $name,
                'tribe_seller_id' => $id,
            );
            if ($id)
                $orderItem->setData('tribe_seller_id', $id);
            $orderItem->setProductOptions($options);
        }
    }

    public function setProductSellerPrice(Varien_Event_Observer $observer) {
        /* @var $item Mage_Sales_Model_Quote_Item */
        $params = Mage::app()->getRequest()->getParams();
        $item = $observer->getQuoteItem();
        if ($item->getParentItem()) {
            $item = $item->getParentItem();
        }
// This makes sure the discount isn't applied over and over when refreshing
        $specialPrice = ($params['product_seller_price']) ? $params['product_seller_price'] : $item->getOriginalPrice();
// Make sure we don't have a negative
        if ($specialPrice > 0) {
            $item->setCustomPrice($specialPrice);
            $item->setOriginalCustomPrice($specialPrice);
            $item->getProduct()->setIsSuperMode(true);
        }
    }

    public function addButtonTest($observer) {
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
            
        } else {
            $container = $observer->getBlock();
            if (null !== $container && $container->getType() == 'tribeseller/adminhtml_sellers') {
                $data = array(
                    'label' => 'Manage Attributes',
                    'onclick' => 'setLocation(\' ' . Mage::helper('adminhtml')->getUrl('adminhtml/sellerattribute/', array('_secure' => true)) . '\')',
                );
                $data_1 = array(
                    'label' => 'Manage Attributes Sets',
                    'onclick' => 'setLocation(\' ' . Mage::helper('adminhtml')->getUrl('adminhtml/sellerattributeset/', array('_secure' => true)) . '\')',
                );
                $data_2 = array(
                    'label' => 'Add Seller',
                    'class' => 'add',
                    'onclick' => 'setLocation(\' ' . Mage::helper('adminhtml')->getUrl('adminhtml/sellerprofiles/new', array('_secure' => true)) . '\')',
                );
                $container->addButton('sellerattribute', $data);
                $container->addButton('sellerattributeset', $data_1);
                $container->addButton('addseller', $data_2);
            }
        }

        return $this;
    }

    public function add_customer_username($observer) {
        $customer = $observer->getCustomer();
        $data = $customer->getData();
        if (!isset($data['username'])) {
            $username = str_replace('.', '', str_replace('@', '', $data['email']));
            $customer->setData('username', $username);
        }
        if (!isset($data['tribe_seller_id'])) {
            $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
            $sellerid = ($is_seller) ? Mage::helper('tribeseller')->getSellerIdFromadminUser() : 0;
            $customer->setData('tribe_seller_id', $sellerid);
        }
    }

    public function loadconfig($observer) {
        $product = $observer->getData('product');
    }
    
    public function add_ipzipcode($observer) {
        Mage::helper('tribeseller')->getzipcodefromip();
    }

}