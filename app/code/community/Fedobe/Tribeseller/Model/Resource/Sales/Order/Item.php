<?php

class Fedobe_Tribeseller_Model_Resource_Sales_Order_Item extends Mage_Sales_Model_Order_Item {

    protected $seller_id = null;
    protected $is_seller = null;

    protected function _construct() {
        parent::_construct();
        if (is_null($this->is_seller)) {
            $this->is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        }
        if ($this->is_seller) {
            if (is_null($this->seller_id)) {
                $this->seller_id = Mage::helper('tribeseller')->getSellerIdFromadminUser();
            }
        }
    }

    public function getParentItem() {
         if ($this->is_seller) {
            if ($this->seller_id != $this->getTribeSellerId())
                return $this;
            else
                return $this->_parentItem;
        } else {
            $email_seller_id = Mage::getSingleton('customer/session')->getEmailtribesellerid();
            if($email_seller_id){
            if ($email_seller_id != $this->getTribeSellerId())
                return $this;
            }else{
                return $this->_parentItem;
            }
        }
    }

    protected function _beforeSave() {
        parent::_beforeSave();
        if ($this->getParentItem()) {
            if ($this->getParentItem()->getId() == $this->getItemId()) {
                $this->setParentItemId(null);
            }
        }
        return $this;
    }

    public function isChildrenCalculated() {
        if ($this->is_seller) {
            return true;
        } else {
            parent::isChildrenCalculated();
        }
    }

}