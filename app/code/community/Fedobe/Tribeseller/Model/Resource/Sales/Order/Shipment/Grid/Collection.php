<?php

class Fedobe_Tribeseller_Model_Resource_Sales_Order_Shipment_Grid_Collection extends Mage_Sales_Model_Resource_Order_Shipment_Grid_Collection {

    public function setOrderFilter($order) {
        if ($order instanceof Mage_Sales_Model_Order) {
            $this->setSalesOrder($order);
            $orderId = $order->getId();
            if ($orderId) {
                $this->addFieldToFilter($this->_orderField, $orderId);
            } else {
                $this->_totalRecords = 0;
                $this->_setIsLoaded(true);
            }
        } else {
            $this->addFieldToFilter($this->_orderField, $order);
        }

        //Here added extra filter
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
            $itemids = Mage::helper('tribeseller')->getSellerItemIdsshiped($orderId);
            $invoice_entity_ids = Mage::helper('tribeseller')->getShipmenteEntityIdsFromOrderItemsIds($itemids);
            $this->addFieldToFilter('entity_id', array('in' => $invoice_entity_ids));
        }
        return $this;
    }

    public function setSellerFilter() {
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
            $itemids = Mage::helper('tribeseller')->getSellerItemIdsshiped($orderId);
            $invoice_entity_ids = Mage::helper('tribeseller')->getShipmenteEntityIdsFromOrderItemsIds($itemids);
                $this->addFieldToFilter('entity_id', array('in' => $invoice_entity_ids));
        }
        return $this;
    }

}