<?php

class Fedobe_Tribeseller_Model_Resource_Setup extends Mage_Eav_Model_Entity_Setup {

    /**
     * Prepare catalog attribute values to save
     *
     * @param array $attr
     * @return array
     */
    protected function _prepareValues($attr) {
        $data = parent::_prepareValues($attr);
        $data = array_merge($data, array(
            'frontend_input_renderer' => $this->_getValue($attr, 'input_renderer'),
            'is_global' => $this->_getValue(
                    $attr, 'global', Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
            ),
            'is_visible' => $this->_getValue($attr, 'visible', 1),
            'is_searchable' => $this->_getValue($attr, 'searchable', 0),
            'is_filterable' => $this->_getValue($attr, 'filterable', 0),
            'is_wysiwyg_enabled' => $this->_getValue($attr, 'wysiwyg_enabled', 0),
            'is_html_allowed_on_front' => $this->_getValue($attr, 'is_html_allowed_on_front', 0),
            'is_visible_in_advanced_search' => $this->_getValue($attr, 'visible_in_advanced_search', 0),
            'is_filterable_in_search' => $this->_getValue($attr, 'filterable_in_search', 0),
            'position' => $this->_getValue($attr, 'position', 0),
            'is_used_for_promo_rules' => $this->_getValue($attr, 'used_for_promo_rules', 0)
        ));
        return $data;
    }

    /*
     * Setup attributes for inchoo_blog_post entity type
     * -this attributes will be saved in db if you set them
     */

    public function getDefaultEntities() {
        $entities = array(
            'fedobe_tribe_seller' => array(
                'entity_model' => 'tribeseller/seller',
                'additional_attribute_table' => 'tribeseller/eav_attribute',
                'entity_attribute_collection' => '',
                'table' => 'tribeseller/seller_entity',
                'attributes' => array(
                    'seller_customer_type' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Seller Customer Type',
                        'input' => 'select',
                        'option' => $this->getAllSellerCustomerType(),
                        'class' => '',
                        'source' => 'tribeseller/seller_attribute_customertype',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Profile',
                    ),
                    'all_customers' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Customers',
                        'input' => 'select',
                        'option' => $this->getAllCustomers(),
                        'class' => 'all-customers',
                        'source' => 'tribeseller/seller_attribute_customers',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Profile',
                    ),
                    'seller_first_name' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'First Name',
                        'input' => 'text',
                        'class' => 'new-customers',
                        'source' => '',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Profile',
                    ),
                    'seller_last_name' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Last Name',
                        'input' => 'text',
                        'class' => 'new-customers',
                        'source' => '',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Profile',
                    ),
                    'seller_email' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Email',
                        'input' => 'text',
                        'class' => 'new-customers',
                        'source' => '',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Profile',
                    ),
                    'seller_user_name' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Username',
                        'input' => 'text',
                        'class' => 'new-customers',
                        'source' => '',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => true,
                        'group' => 'Profile',
                        'note' => 'Allowed values[A-za-z0-9]'
                    ),
                    'seller_password' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Password',
                        'input' => 'password',
                        'class' => 'new-customers',
                        'source' => '',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Profile',
                    ),
                    'seller_store_name' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Store Name',
                        'input' => 'text',
                        'class' => '',
                        'source' => '',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Profile',
                    ),
                    'seller_partner' => array(
                        'type' => 'int',
                        'frontend' => '',
                        'label' => 'Seller Partner',
                        'input' => 'select',
//                        'option' => $this->getAllSellerStatus(),
                        'class' => '',
                        'source' => 'tribeseller/seller_attribute_unassignedsellers',
                        'backend' => 'eav/entity_attribute_backend_array',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Profile',
                    ),
                    'seller_status' => array(
                        'type' => 'int',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Status',
                        'input' => 'select',
                        'option' => $this->getAllSellerStatus(),
                        'class' => '',
                        'source' => 'tribeseller/seller_attribute_status',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Profile',
                    ),
                    'seller_profile_pic' => array(
                        'type' => 'varchar',
                        'backend' => 'tribeseller/mysql4_seller_attribute_backend_image',
                        'frontend' => '',
                        'label' => 'Profile Picture',
                        'input' => 'image',
                        'class' => '',
                        'source' => '',
                        'global' => 1,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Profile',
                    ),
                    'featured_seller' => array(
                        'type' => 'int',
                        'label' => 'Is Featured',
                        'input' => 'boolean',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Profile',
                    ),
                    'seller_country' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Country',
                        'input' => 'select',
                        'option' => $this->getAllCountries(),
                        'class' => '',
                        'source' => 'tribeseller/countries_source_details',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Address',
                    ),
                    'seller_state' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'State',
                        'input' => 'text',
                        'class' => '',
                        'source' => '',
                        'global' => 1,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Address',
                    ),
                    'seller_city' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'City',
                        'input' => 'text',
                        'class' => '',
                        'source' => '',
                        'global' => 1,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Address',
                    ),
                    'seller_pin' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Pin',
                        'input' => 'text',
                        'class' => '',
                        'source' => '',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Address',
                    ),
                    'seller_street_address_one' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Street Address 1',
                        'input' => 'textarea',
                        'class' => '',
                        'source' => '',
                        'global' => 0,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Address',
                    ),
                    'seller_street_address_two' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Street Address 2',
                        'input' => 'textarea',
                        'class' => '',
                        'source' => '',
                        'global' => 0,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Address',
                    ),
                     'sellerhome' => array(
                        'type' => 'text',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'HomePage Content',
                        'is_wysiwyg_enabled' => 0,
                        'input' => 'tribecontent',
                        'class' => '',
                        'source' => '',
                        'global' => 0,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default_value_textarea' => '{{block type="cms/block" block_id="slider_page1"}}
{{block type="cms/block" block_id="slider_page"}}',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'group' => 'Home',
                    ),
                     'sellerhome_layoutupdate' => array(
                        'type' => 'text',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'HomePage Layout Update',
                        'is_wysiwyg_enabled' => 0,
                        'input' => 'tribecontent',
                        'class' => '',
                        'source' => '',
                        'global' => 0,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default_value_textarea' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'group' => 'Home',
                    ),
                    'seller_about' => array(
                        'type' => 'text',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Brief Description about seller',
                        'is_wysiwyg_enabled' => true,
                        'input' => 'tribecontent',
                        'class' => '',
                        'source' => '',
                        'global' => 0,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'group' => 'About',
                    ),
                    'seller_policies' => array(
                        'type' => 'text',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Seller Policy Description',
                        'is_wysiwyg_enabled' => true,
                        'input' => 'tribecontent',
                        'class' => '',
                        'source' => '',
                        'global' => 0,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'group' => 'Policy',
                    ),
                    'seller_direction_map' => array(
                        'type' => 'text',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Seller Map Location',
                        'input' => 'tribemap',
                        'class' => '',
                        'source' => '',
                        'global' => 1,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'group' => 'Direction',
                    ),
                    'seller_media_gallery' => array(
                        'type' => 'varchar',
                        'label' => 'Media Gallery',
                        'input' => 'gallery',
                        'backend' => 'tribeseller/seller_attribute_backend_media',
                        'required' => false,
                        'group' => 'Images',
                    ),
                    'seller_meta_title' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Meta Title',
                        'input' => 'text',
                        'class' => '',
                        'source' => '',
                        'global' => 0,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Meta Information',
                    ),
                    'seller_meta_keywords' => array(
                        'type' => 'text',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Meta Keywords',
                        'input' => 'textarea',
                        'class' => '',
                        'source' => '',
                        'global' => 0,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Meta Information',
                    ),
                    'seller_meta_description' => array(
                        'type' => 'text',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Meta Description',
                        'input' => 'textarea',
                        'class' => '',
                        'source' => '',
                        'global' => 0,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Meta Information',
                    ),
                    'seller_order_mail' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Receive New Order Email At',
                        'input' => 'text',
                        'class' => '',
                        'source' => '',
                        'global' => 1,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Sales Email',
                    ),
                    'seller_order_mail_copy_method' => array(
                        'type' => 'varchar',
                        'backend_type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Order Email Copy Method',
                        'input' => 'select',
                        'option' => '',
                        'class' => '',
                        'source' => 'tribeseller/adminhtml_system_config_source_email_method',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Sales Email',
                    ),
                    'seller_order_invoice_mail' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Receive Order Invoice Email At',
                        'input' => 'text',
                        'class' => '',
                        'source' => '',
                        'global' => 1,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Sales Email',
                    ),
                    'seller_invoice_copy_method' => array(
                        'type' => 'varchar',
                        'backend_type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Order Invoice Email Copy Method',
                        'input' => 'select',
                        'option' => '',
                        'class' => '',
                        'source' => 'tribeseller/adminhtml_system_config_source_email_method',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Sales Email',
                    ),
                    'seller_order_shippment_mail' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Receive Shippment Email At',
                        'input' => 'text',
                        'class' => '',
                        'source' => '',
                        'global' => 1,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Sales Email',
                    ),
                    'seller_shippment_copy_method' => array(
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => 'Shippment Email Copy Method',
                        'input' => 'select',
                        'backend_type' => 'varchar',
                        'option' => '',
                        'class' => '',
                        'source' => 'tribeseller/adminhtml_system_config_source_email_method',
                        'global' => 1,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                        'group' => 'Sales Email',
                    ),
                ),
            )
        );
        return $entities;
    }

    
    public function getAllCountries() {
        $formattedcountries = array();
        $countries = Mage::getResourceModel('directory/country_collection')->loadData()->toOptionArray(false);
        foreach ($countries as $k => $v) {
            $formattedcountries['values'][] = $v['label'];
        }
        return $formattedcountries;
    }

    public function getAllSellerStatus() {
        $formatedstatus = array();
        $status = Mage::getModel('tribeseller/seller_attribute_status')->getAllOptions();
        foreach ($status as $k => $v) {
            $formatedstatus['values'][] = $v;
        }
        return $formatedstatus;
    }
    public function getAllCustomers() {
        $formatedstatus = array();
        $status = Mage::getModel('tribeseller/seller_attribute_customers')->getAllOptions();
        foreach ($status as $k => $v) {
            $formatedstatus['values'][] = $v;
        }
        return $formatedstatus;
    }

    public function getAllSellerCustomerType() {
        $formatedstatus = array();
        $status = Mage::getModel('tribeseller/seller_attribute_customertype')->getAllOptions();
        foreach ($status as $k => $v) {
            $formatedstatus['values'][] = $v;
        }
        return $formatedstatus;
    }
    
    
    public function createEntityTables($baseTableName, array $options = array()) {
        $isNoCreateMainTable = $this->_getValue($options, 'no-main', false);
        $isNoDefaultTypes = $this->_getValue($options, 'no-default-types', false);
        $customTypes = $this->_getValue($options, 'types', array());
        $tables = array();

        if (!$isNoCreateMainTable) {
            /**
             * Create table main eav table
             */
            $connection = $this->getConnection();
            $mainTable = $connection
                    ->newTable($this->getTable($baseTableName))
                    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                            ), 'Entity Id')
                    ->addColumn('entity_type_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                        'unsigned' => true,
                        'nullable' => false,
                        'default' => '0',
                            ), 'Entity Type Id')
                    ->addColumn('attribute_set_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                        'unsigned' => true,
                        'nullable' => false,
                        'default' => '0',
                            ), 'Attribute Set Id')
                    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
                        'nullable' => false,
                            ), 'Created At')
                    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
                        'nullable' => false,
                            ), 'Updated At')
                    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                        'unsigned' => true,
                        'nullable' => false,
                        'default' => '1',
                            ), 'Defines Is Entity Active')
                    ->addIndex($this->getIdxName($baseTableName, array('entity_type_id')), array('entity_type_id'))
                    ->addForeignKey($this->getFkName($baseTableName, 'entity_type_id', 'eav/entity_type', 'entity_type_id'), 'entity_type_id', $this->getTable('eav/entity_type'), 'entity_type_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
                    ->setComment('Seller Entity Main Table');

            $tables[$this->getTable($baseTableName)] = $mainTable;
        }

        $types = array();
        if (!$isNoDefaultTypes) {
            $types = array(
                'datetime' => array(Varien_Db_Ddl_Table::TYPE_DATETIME, null),
                'decimal' => array(Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4'),
                'int' => array(Varien_Db_Ddl_Table::TYPE_INTEGER, null),
                'text' => array(Varien_Db_Ddl_Table::TYPE_TEXT, '64k'),
                'varchar' => array(Varien_Db_Ddl_Table::TYPE_TEXT, '255'),
                'char' => array(Varien_Db_Ddl_Table::TYPE_TEXT, '255')
            );
        }

        if (!empty($customTypes)) {
            foreach ($customTypes as $type => $fieldType) {
                if (count($fieldType) != 2) {
                    throw Mage::exception('Mage_Eav', Mage::helper('eav')->__('Wrong type definition for %s', $type));
                }
                $types[$type] = $fieldType;
            }
        }

        /**
         * Create table array($baseTableName, $type)
         */
        foreach ($types as $type => $fieldType) {
            $eavTableName = array($baseTableName, $type);

            $eavTable = $connection->newTable($this->getTable($eavTableName));
            $eavTable
                    ->addColumn('value_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                            ), 'Value Id')
                    ->addColumn('entity_type_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                        'unsigned' => true,
                        'nullable' => false,
                        'default' => '0',
                            ), 'Entity Type Id')
                    ->addColumn('attribute_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                        'unsigned' => true,
                        'nullable' => false,
                        'default' => '0',
                            ), 'Attribute Id')
                    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
                        'unsigned' => true,
                        'nullable' => false,
                        'default' => '0',
                            ), 'Store Id')
                    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
                        'unsigned' => true,
                        'nullable' => false,
                        'default' => '0',
                            ), 'Entity Id')
                    ->addColumn('value', $fieldType[0], $fieldType[1], array(
                        'nullable' => false,
                            ), 'Attribute Value')
                    ->addIndex($this->getIdxName($eavTableName, array('entity_type_id')), array('entity_type_id'))
                    ->addIndex($this->getIdxName($eavTableName, array('attribute_id')), array('attribute_id'))
                    ->addIndex($this->getIdxName($eavTableName, array('store_id')), array('store_id'))
                    ->addIndex($this->getIdxName($eavTableName, array('entity_id')), array('entity_id'));
            
            $eavTable
                    ->addForeignKey($this->getFkName($eavTableName, 'entity_id', $baseTableName, 'entity_id'), 'entity_id', $this->getTable($baseTableName), 'entity_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
                    ->addForeignKey($this->getFkName($eavTableName, 'entity_type_id', 'eav/entity_type', 'entity_type_id'), 'entity_type_id', $this->getTable('eav/entity_type'), 'entity_type_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
                    ->addForeignKey($this->getFkName($eavTableName, 'store_id', 'core/store', 'store_id'), 'store_id', $this->getTable('core/store'), 'store_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
                    ->setComment('Eav Entity Value Table');

            $tables[$this->getTable($eavTableName)] = $eavTable;
        }

        // DDL operations are forbidden within transactions
        // See Varien_Db_Adapter_Pdo_Mysql::_checkDdlTransaction()
        try {
            foreach ($tables as $tableName => $table) {
                $connection->createTable($table);
                if($tableName != $baseTableName){
                    $indexName = $this->getIdxName($tableName,array('entity_id','attribute_id','store_id'),Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);
                    $connection->addIndex($tableName, $indexName, array('entity_id','attribute_id','store_id'),Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE);
                }
            }
        } catch (Exception $e) {
            throw Mage::exception('Mage_Eav', Mage::helper('eav')->__('Can\'t create table: %s', $tableName));
        }

        return $this;
    }

}
