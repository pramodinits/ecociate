<?php

class Fedobe_Tribeseller_Model_Resource_Order_Collection extends Mage_Reports_Model_Resource_Order_Collection {

    /**
     * Prepare report summary
     *
     * @param string $range
     * @param mixed $customStart
     * @param mixed $customEnd
     * @param int $isFilter
     * @return Mage_Reports_Model_Resource_Order_Collection
     */
    public function prepareSummary($range, $customStart, $customEnd, $isFilter = 0) {
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        $this->checkIsLive($range);
        if ($is_seller) {
//$this->_prepareSummaryAggregated($range, $customStart, $customEnd, $isFilter);
            $this->getSellerGraph($range, $customStart, $customEnd, $isFilter);
        } else {

            if ($this->_isLive) {
                $this->_prepareSummaryLive($range, $customStart, $customEnd, $isFilter);
            } else {
                $this->_prepareSummaryAggregated($range, $customStart, $customEnd, $isFilter);
            }
        }
        return $this;
    }

    private function getSellerGraph($range, $customStart, $customEnd, $isFilter) {
        $sellerid = Mage::helper('tribeseller')->getSellerIdFromadminUser();
        $this->setMainTable('sales/order_item');
        $adapter = $this->getConnection();
        $this->getSelect()->reset(Zend_Db_Select::COLUMNS);
        $this->getSelect()->where('tribe_seller_id = ? ', array($sellerid))->group('order_id');
        $this->getSelect()->join(
                array('order' => 'sales_flat_order'), 'main_table.order_id = order.entity_id', array()
        );

        $expr = "(IFNULL(main_table.base_row_invoiced, 0) - IFNULL(main_table.base_tax_invoiced, 0) - IFNULL(order.base_shipping_invoiced, 0) - (IFNULL(main_table.base_amount_refunded, 0) - IFNULL(main_table.base_tax_refunded, 0) - IFNULL(order.base_shipping_refunded, 0))) * order.base_to_global_rate";
        $this->getSelect()->columns(array(
            'revenue' => "(SUM($expr) * order.base_to_global_rate)"
        ));
        $dateRange = $this->getDateRange($range, $customStart, $customEnd);

        $tzRangeOffsetExpression = $this->_getTZRangeOffsetExpression(
                $range, 'order.created_at', $dateRange['from'], $dateRange['to']
        );
        $this->getSelect()
                ->columns(array(
                    'quantity' => 'COUNT(order.entity_id)',
                    'range' => $tzRangeOffsetExpression,
                ))
                ->where('order.state NOT IN (?)', array(
                    Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
                    Mage_Sales_Model_Order::STATE_NEW)
                )
                ->order('range', Zend_Db_Select::SQL_ASC)
                ->group($tzRangeOffsetExpression);

        $this->addFieldToFilter('order.created_at', $dateRange);
        return $this;
    }
}