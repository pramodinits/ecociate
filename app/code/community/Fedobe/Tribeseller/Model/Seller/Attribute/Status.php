<?php

class Fedobe_Tribeseller_Model_Seller_Attribute_Status extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    public function getAllOptions($withEmpty = false) {
        $status = array(1 => 'Inactive',2 => 'Active');
        return $status;
    }

    public function getOptionText($value) {
        $options = $this->getAllOptions(false);
        foreach ($options as $item) {
            if ($item['value'] == $value) {
                return $item['label'];
            }
        }
        return false;
    }

}