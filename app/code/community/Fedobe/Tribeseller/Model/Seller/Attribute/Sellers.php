<?php

class Fedobe_Tribeseller_Model_Seller_Attribute_Sellers extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    public function getAllOptions($withEmpty = false) {
        $default_attr_code = "tribe_seller";
        $is_own = 0;
        $allOptions = array();
        $seller_attributecode = trim(Mage::getStoreConfig('tribecoresettings/general_settings/tribe_seller_attribute_code'));
        if ($seller_attributecode && !($seller_attributecode == $default_attr_code)) {
            //Here let's check whether this attribute code is valid or not
            $tribe_seller_id = Mage::getResourceModel('eav/entity_attribute')
                    ->getIdByCode('catalog_product', $seller_attributecode);
            if($tribe_seller_id){
                $default_attr_code = $seller_attributecode;
            }else{
                $default_attr_code = $default_attr_code;
                $is_own = 1;
            }
        }else{
            $is_own = 1;
        }
        $store = Mage::app()->getRequest()->getParam('store');
        $store_id = ($store) ? $store : 0;
        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $default_attr_code);
        if($is_own){
            $valuesCollection = Mage::getResourceModel('eav/entity_attribute_option_collection')->setAttributeFilter($attribute->getId())->setStoreFilter($store_id, false);
            if($store != 0 && empty($valuesCollection->getData())){
                //Here let's check whether any store options there
                $valuesCollection = Mage::getResourceModel('eav/entity_attribute_option_collection')->setAttributeFilter($attribute->getId())->setStoreFilter(0, false);
            }
            $data = $valuesCollection->getData();
            foreach ($data as $key => $value) {
                $allOptions[$key] = array(
                    'value' => $value['option_id'],
                    'label' => $value['value'],
                );
            }
        }else{
            $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $default_attr_code);
            $allOptions = $attribute->setStoreId($store_id)->getSource()->getAllOptions(false);
        }
        return $allOptions;
//        $activesellers = Mage::getModel('tribeseller/seller')->getCollection()
//                ->addAttributeToFilter('seller_status', 2);
//        $sellers = array();
//        if (!empty($activesellers)) {
//            foreach ($activesellers as $k => $seller) {
//                $sellers[] = array(
//                    "label" => Mage::helper('tribeseller')->__($seller->getSellerStoreName())
//                    , "value" => $seller->getId()
//                );
//            }
//        }
//        return $sellers;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray() {
        $_options = array();
        foreach ($this->getAllOptions() as $option) {
            $_options[$option["value"]] = $option["label"];
        }
        return $_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string
     */
    public function getOptionText($value) {
        $options = $this->getAllOptions();
        foreach ($options as $option) {
            if ($option["value"] == $value) {
                return $option["label"];
            }
        }
        return false;
    }

    /**
     * Retrieve Column(s) for Flat
     *
     * @return array
     */
    public function getFlatColums() {
        $columns = array();
        $columns[$this->getAttribute()->getAttributeCode()] = array(
            "type" => "tinyint(1)",
            "unsigned" => false,
            "is_null" => true,
            "default" => null,
            "extra" => null
        );

        return $columns;
    }

    /**
     * Retrieve Indexes(s) for Flat
     *
     * @return array
     */
    public function getFlatIndexes() {
        $indexes = array();

        $index = "IDX_" . strtoupper($this->getAttribute()->getAttributeCode());
        $indexes[$index] = array(
            "type" => "index",
            "fields" => array($this->getAttribute()->getAttributeCode())
        );

        return $indexes;
    }

    /**
     * Retrieve Select For Flat Attribute update
     *
     * @param int $store
     * @return Varien_Db_Select|null
     */
    public function getFlatUpdateSelect($store) {
        return Mage::getResourceModel("eav/entity_attribute")
                        ->getFlatUpdateSelect($this->getAttribute(), $store);
    }

}
