<?php

class Fedobe_Tribeseller_Model_Sales_Order_Invoice extends Mage_Sales_Model_Order_Invoice {

    protected $seller_id = null;
    protected $is_seller = null;

    protected function _construct() {
        parent::_construct();
        if (is_null($this->is_seller)) {
            $this->is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        }
        if ($this->is_seller) {
            if (is_null($this->seller_id)) {
                $this->seller_id = Mage::helper('tribeseller')->getSellerIdFromadminUser();
            }
        }
    }
    
    public function getInvoiceDate()
    {
        $createdAt = $this->getCreatedAt();
        $invoiceDate = date('d M Y', strtotime("+7 days"));
        return $invoiceDate;
    }

    public function sendEmail($notifyCustomer = true, $comment = '') {//echo "hii"; exit;
        $order = $this->getOrder();
        $storeId = $order->getStore()->getId();

        if (!Mage::helper('sales')->canSendNewInvoiceEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_EMAIL_COPY_METHOD, $storeId);
        // Check if at least one recepient is found
        if (!$notifyCustomer && !$copyTo) {
            return $this;
        }

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($order->getPayment())
                    ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        // Retrieve corresponding email template id and customer name
        if ($order->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $order->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
            $customerName = $order->getCustomerName();
        }

        $mailer = Mage::getModel('core/email_template_mailer');
        if ($notifyCustomer) {
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($order->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }
            $mailer->addEmailInfo($emailInfo);
        }

        // Email copies are sent as separated emails if their copy method is 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy' || !$notifyCustomer)) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        //Here to add seller email in order mail
        if ($this->is_seller && $this->seller_id) {
            $seller = Mage::getModel('tribeseller/seller')->load($this->seller_id);
            $seller_mail = ($seller->getSellerOrderInvoiceMail()) ? $seller->getSellerOrderInvoiceMail() : $seller->getSellerEmail();
            $sellercopymethod = ($seller->getSellerInvoiceCopyMethod()) ? $seller->getSellerInvoiceCopyMethod() : 'copy';
            $emailInfo = Mage::getModel('core/email_info');
            if ($sellercopymethod == 'copy') {
                $emailInfo->addTo($seller_mail);
                $mailer->addEmailInfo($emailInfo);
            } else {
                $emailInfo->addBcc($seller_mail);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
            'order' => $order,
            'invoice' => $this,
            'comment' => $comment,
            'billing' => $order->getBillingAddress(),
            'payment_html' => $paymentBlockHtml,
            'dateAndTime'  => Mage::getModel('core/date')->date('d M Y', strtotime("+3 days")),
                )
        );
        $mailer->send();
        $this->setEmailSent(true);
        $this->_getResource()->saveAttribute($this, 'email_sent');

        return $this;
    }
     public static function getStates()
    {
        if (is_null(self::$_states)) {
            self::$_states = array(
                self::STATE_OPEN       => Mage::helper('sales')->__('Pending'),
                self::STATE_PAID       => Mage::helper('sales')->__('Invoiced'),
                self::STATE_CANCELED   => Mage::helper('sales')->__('Canceled'),
            );
        }
        return self::$_states;
    }


}