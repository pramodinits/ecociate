<?php

class Fedobe_Tribeseller_Model_Sales_Order extends Mage_Sales_Model_Order {

    protected $seller_id = null;
    protected $is_seller = null;

    protected function _construct() {
        parent::_construct();
        if (is_null($this->is_seller)) {
            $this->is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        }
        if ($this->is_seller) {
            if (is_null($this->seller_id)) {
                $this->seller_id = Mage::helper('tribeseller')->getSellerIdFromadminUser();
            }
        }
    }

    public function queueNewOrderEmail($forceMode = false) {
        $storeId = $this->getStore()->getId();
        if (!Mage::helper('sales')->canSendNewOrderEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_EMAIL_COPY_METHOD, $storeId);

        // Start store emulation process
        /** @var $appEmulation Mage_Core_Model_App_Emulation */
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($this->getPayment())
                    ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        // Retrieve corresponding email template id and customer name
        if ($this->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $this->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
            $customerName = $this->getCustomerName();
        }

        /** @var $mailer Mage_Core_Model_Email_Template_Mailer */
        $mailer = Mage::getModel('core/email_template_mailer');
        /** @var $emailInfo Mage_Core_Model_Email_Info */
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($this->getCustomerEmail(), $customerName);
        if ($copyTo && $copyMethod == 'bcc') {
            // Add bcc to customer email
            foreach ($copyTo as $email) {
                $emailInfo->addBcc($email);
            }
        }
        $mailer->addEmailInfo($emailInfo);

        // Email copies are sent as separated emails if their copy method is 'copy'
        if ($copyTo && $copyMethod == 'copy') {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }
        //Here to add seller email in order mail
        if ($this->is_seller && $this->seller_id) {
            $seller = Mage::getModel('tribeseller/seller')->load($this->seller_id);
            $seller_mail = ($seller->getSellerOrderMail()) ? $seller->getSellerOrderMail() : $seller->getSellerEmail();
            $sellercopymethod = ($seller->getSellerOrderMailCopyMethod()) ? $seller->getSellerOrderMailCopyMethod() : 'copy';
            $emailInfo = Mage::getModel('core/email_info');
            if ($sellercopymethod == 'copy') {
                $emailInfo->addTo($seller_mail);
                $mailer->addEmailInfo($emailInfo);
            } else {
                $emailInfo->addBcc($seller_mail);
            }
        } else {
           // $templateId1 = 'custom_email_template1';
            foreach ($this->getAllItems() as $item) {
                $mailer1 = Mage::getModel('core/email_template_mailer');
            $emailInfo = Mage::getModel('core/email_info');
                $tribe_seller_id = $item->getData('tribe_seller_id');
                Mage::getSingleton('customer/session')->setEmailtribesellerid($tribe_seller_id);
                $seller = Mage::getModel('tribeseller/seller')->load($tribe_seller_id);
                $seller_mail = ($seller->getSellerOrderMail()) ? $seller->getSellerOrderMail() : $seller->getSellerEmail();
                $emailInfo->addTo($seller_mail);
                //$mailer->addEmailInfo($emailInfo);
                $mailer1->addEmailInfo($emailInfo);
               $mailer1->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer1->setStoreId($storeId);
        $mailer1->setTemplateId($templateId);
        $mailer1->setTemplateParams(array(
            'order' => $this,
            'billing' => $this->getBillingAddress(),
            'payment_html' => $paymentBlockHtml,
            'seller_id'=>$tribe_seller_id
        ));
        /** @var $emailQueue Mage_Core_Model_Email_Queue */
        $emailQueue1 = Mage::getModel('core/email_queue');
        $emailQueue1->setEntityId($this->getId())
                ->setEntityType(self::ENTITY)
                ->setEventType(self::EMAIL_EVENT_NAME_NEW_ORDER)
                ->setIsForceCheck(!$forceMode);
        $mailer1->setQueue($emailQueue1)->send();
            }
        }
         Mage::getSingleton('customer/session')->unsEmailtribesellerid();
        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        /*$BaseGrandTotal = Mage::helper('tribeseller')->getSellerBaseGrandTotal($this->getId(),$this->seller_id);
        $sellerGrandTotal = Mage::helper('tribeseller')->getSellerGrandTotal($this->getId(),$this->seller_id);
        
        $this->setBaseGrandTotal($BaseGrandTotal);
        $this->setGrandTotal($sellerGrandTotal);echo "<pre>";print_r($this->getBaseGrandTotal());exit;*/
        $mailer->setTemplateParams(array(
            'order' => $this,
            'billing' => $this->getBillingAddress(),
            'payment_html' => $paymentBlockHtml
        ));
        /** @var $emailQueue Mage_Core_Model_Email_Queue */
        $emailQueue = Mage::getModel('core/email_queue');
        $emailQueue->setEntityId($this->getId())
                ->setEntityType(self::ENTITY)
                ->setEventType(self::EMAIL_EVENT_NAME_NEW_ORDER)
                ->setIsForceCheck(!$forceMode);

        $mailer->setQueue($emailQueue)->send();

        $this->setEmailSent(true);
        $this->_getResource()->saveAttribute($this, 'email_sent');

        return $this;
    }

}