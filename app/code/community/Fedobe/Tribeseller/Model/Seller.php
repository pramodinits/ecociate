<?php

class Fedobe_Tribeseller_Model_Seller extends Fedobe_Tribeseller_Model_Seller_Abstract {

    protected function _construct() {
        $this->_init('tribeseller/seller');
    }

    /**
     * Retrieve a collection of products associated with the splash page
     *
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
     */
    public function getProductCollection() {
        $this->setProductCollection($this->getResource()->getProductCollection($this));

        return $this->getData('product_collection');
    }

    public function doreviewaggregation($review_entityid) {
        $id = $this->getId();
        $resource = Mage::getSingleton('core/resource');
        $writeadapter = $resource->getConnection('core_write');
        $read = $resource->getConnection('core_read');
        $sql = "SELECT `review_id` FROM " . $resource->getTableName('review/review') . " WHERE `entity_id`=$review_entityid AND `entity_pk_value`= $id AND `status_id` = 1";
        $sqlattrQuery = $read->query($sql);
        $reviews = array();
        while ($row = $sqlattrQuery->fetch()) {
            $reviews[$row['review_id']] = $row['review_id'];
        }
        $avrage = 0;
        $totalreview = count($reviews);
        if ($totalreview) {
            //Here let's calculate the ratings
            $reviewids = implode(',', $reviews);
            $ratingsql = "SELECT AVG(`custom_rating`) AS `average` FROM " . $resource->getTableName('review/review_detail') . " WHERE FIND_IN_SET(`review_id`,'$reviewids') ";
            $sqlattrQuery = $read->query($ratingsql);
            $row = $sqlattrQuery->fetch();
            $avrage = ($row['average']) ? $row['average'] : 0;
        }
        $writeadapter->update(
                $resource->getTableName('tribeseller/seller_entity'), array('seller_avg_rating' => $avrage,
            'seller_reviews_cnt' => $totalreview), 'entity_id = ' . $id
        );
    }

}