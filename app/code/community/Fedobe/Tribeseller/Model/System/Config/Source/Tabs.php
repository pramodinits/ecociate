<?php

/**
 * @category    Fedobe
 * @package     Fedobe_Landingpage
 */
class Fedobe_Tribeseller_Model_System_Config_Source_Tabs {

    public function toOptionArray() {
        $options = Mage::helper('tribeseller')->getSellerTabs();
        return $options;
    }

}
