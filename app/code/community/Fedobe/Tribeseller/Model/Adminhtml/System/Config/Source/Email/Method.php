<?php

class Fedobe_Tribeseller_Model_Adminhtml_System_Config_Source_Email_Method extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    public function getAllOptions($withEmpty = false) {
        $countries = Mage::getModel('adminhtml/system_config_source_email_method')->toOptionArray(false);
        return $countries;
    }

    public function getOptionText($value) {
        $options = $this->getAllOptions(false);

        foreach ($options as $item) {
            if ($item['value'] == $value) {
                return $item['label'];
            }
        }
        return false;
    }

}
