<?php

/**
 * @category    Fedobe
 * @package     Fedobe_Landingpage
 */
class Fedobe_Tribeseller_Model_Layer extends Mage_Catalog_Model_Layer {

    /**
     * Retrieve the splash page
     * We add an array to children_categories so that it can act as a category
     *
     * @return false|Fedobe_Landingpage_Model_Page
     */
    public function getCurrentSeller() {
        if ($page = Mage::registry('current_seller')) {
            return $page;
        } 

        return false;
    }

    /**
     * Retrieve the product collection for the Splash Page
     *
     * @return
     */
    public function getProductCollection() {
        $key = 'seller_' . $this->getCurrentSeller()->getId();

        if (isset($this->_productCollections[$key])) {
            $collection = $this->_productCollections[$key];
        } else {
            $collection = $this->__getSellerCollection();// echo "<pre>"."---";print_r($collection->getData());
            $this->prepareProductCollection($collection);//echo "<pre>"."***";print_r($collection->getData());exit;
            $this->_productCollections[$key] = $collection;
        }
        return $collection;
    }
    
    protected function __getSellerCollection(){
        $seller = $this->getCurrentSeller();
       // $channel_id = Mage::helper('tribeseller')->getChannelIdForSeller($seller->getEntityId());
        $storeId = ((int) $seller->getStoreId() === 0) ? Mage::app()->getStore()->getId() : $seller->getStoreId();
        $collection = Mage::getResourceModel('catalog/product_collection')
                ->setStoreId($storeId)
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter('visibility', array('in' => array(
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH
        )));
        $productids = Mage::helper('tribeseller')->getSellersChannelProducts($seller);
//        $ids = $seller->getProductsData() ? explode(',', $seller->getProductsData()) : array(0);
        $collection->addFieldToFilter('entity_id', array('in' => $productids));
        /* $collection->getSelect()->join(array('prodinfo' => 'bridge_incomingchannel_product_info'),
    'e.entity_id = prodinfo.product_entity_id', array('prodinfo.converted_price'))->where('prodinfo.channel_id='.$channel_id);*/
        return $collection;
    }

}
