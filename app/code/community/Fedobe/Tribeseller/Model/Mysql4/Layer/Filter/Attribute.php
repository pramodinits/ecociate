<?php

/**
 * @category    Fedobe
 * @package     Fedobe_Landingpage
 */
class Fedobe_Tribeseller_Model_Mysql4_Layer_Filter_Attribute extends Mage_Core_Model_Resource_Db_Abstract {

    /**
     * Initialize connection and define main table name
     *
     */
    protected function _construct() {
        $this->_init('catalog/product_index_eav', 'entity_id');
    }

    /**
     * Apply attribute filter to product collection
     *
     * @param Mage_Catalog_Model_Layer_Filter_Attribute $filter
     * @param int $value
     * @return Mage_Catalog_Model_Resource_Layer_Filter_Attribute
     */
    public function applyFilterToCollection($filter, $value) {

        $filterSingleton = FilterSingleton::singleton();

        if (!isset($filterSingleton->return)) {

            $collection = $filter->getLayer()->getProductCollection();
            $attribute = $filter->getAttributeModel();
            $connection = $this->_getReadAdapter();
            $tableAlias = $attribute->getAttributeCode() . '_idx';
            $conditions = array(
                "{$tableAlias}.entity_id = e.entity_id",
                $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
                $connection->quoteInto("{$tableAlias}.store_id = ?", $collection->getStoreId()),
                $connection->quoteInto("{$tableAlias}.value = ?", $value)
            );

            $collection->getSelect()->join(
                    array($tableAlias => $this->getMainTable()), join(' AND ', $conditions), array()
            );

            $filterSingleton->return = $this;

            return $this;
        } else {
            return $filterSingleton->return;
        }
    }

    /**
     * Retrieve array with products counts per attribute option
     *
     * @param Mage_Catalog_Model_Layer_Filter_Attribute $filter
     * @return array
     */
    public function getCount($filter) {
        // clone select from collection with filters
        $select = clone $filter->getLayer()->getProductCollection()->getSelect();
        // reset columns, order and limitation conditions
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);

        $connection = $this->_getReadAdapter();
        $attribute = $filter->getAttributeModel();
        $tableAlias = sprintf('%s_idx', $attribute->getAttributeCode());
        $conditions = array(
            "{$tableAlias}.entity_id = e.entity_id",
            $connection->quoteInto("{$tableAlias}.attribute_id = ?", $attribute->getAttributeId()),
            $connection->quoteInto("{$tableAlias}.store_id = ?", $filter->getStoreId()),
        );

        $select
                ->join(
                        array($tableAlias => $this->getMainTable()), join(' AND ', $conditions), array('value', 'count' => new Zend_Db_Expr("COUNT({$tableAlias}.entity_id)")))
                ->group("{$tableAlias}.value");

        return $connection->fetchPairs($select);
    }

}

class FilterSingleton {

    static private $instance;
    public $return = null;

    private function __construct() {
        
    }

    static public function singleton() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }

}
