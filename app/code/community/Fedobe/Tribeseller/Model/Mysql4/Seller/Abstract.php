<?php

abstract class Fedobe_Tribeseller_Model_Mysql4_Seller_Abstract extends Mage_Eav_Model_Entity_Abstract {

    /**
     * Store firstly set attributes to filter selected attributes when used specific store_id
     *
     * @var array
     */
    protected $_attributes = array();

    /**
     * Returns default Store ID
     *
     * @return int
     */
    public function getDefaultStoreId() {
        return Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID;
    }

    /**
     * Retrieve select object for loading entity attributes values
     * Join attribute store value
     *
     * @param Varien_Object $object
     * @param string $table
     * @return Varien_Db_Select
     */
    protected function _getLoadAttributesSelect($object, $table) {
        /**
         * This condition is applicable for all cases when we was work in not single
         * store mode, customize some value per specific store view and than back
         * to single store mode. We should load correct values
         */
        if (Mage::app()->isSingleStoreMode()) {
            $storeId = (int) Mage::app()->getStore(true)->getId();
        } else {
            $storeId = (int) $object->getStoreId();
        }
        $setId = $object->getAttributeSetId();
        $storeIds = array($this->getDefaultStoreId());

        if ($storeId != $this->getDefaultStoreId()) {
            $storeIds[] = $storeId;
        }

        $select = $this->_getReadAdapter()->select()
                ->from(array('attr_table' => $table), array())
                ->where("attr_table.{$this->getEntityIdField()} = ?", $object->getId())
                ->where('attr_table.store_id IN (?)', $storeIds);
        if ($setId) {
            $select->join(
                    array('set_table' => $this->getTable('eav/entity_attribute')), $this->_getReadAdapter()->quoteInto('attr_table.attribute_id = set_table.attribute_id' .
                            ' AND set_table.attribute_set_id = ?', $setId), array()
            );
        }
        return $select;
    }

    /**
     * Prepare select object for loading entity attributes values
     *
     * @param array $selects
     * @return Varien_Db_Select
     */
    protected function _prepareLoadSelect(array $selects) {
        $select = parent::_prepareLoadSelect($selects);
        $select->order('store_id');
        return $select;
    }

    /**
     * Save entity attribute value
     *
     * Collect for mass save
     *
     * @param Mage_Core_Model_Abstract $object
     * @param Mage_Eav_Model_Entity_Attribute_Abstract $attribute
     * @param mixed $value
     * @return Mage_Eav_Model_Entity_Abstract
     */
    protected function _saveAttribute($object, $attribute, $value) {
        $table = $attribute->getBackend()->getTable();
        if (!isset($this->_attributeValuesToSave[$table])) {
            $this->_attributeValuesToSave[$table] = array();
        }
        if ($this->isScopeStore($attribute)) {
            $storeId = (int) Mage::app()->getStore($object->getStoreId())->getId();
        } else {
            $storeId = $this->getDefaultStoreId();
        }
        $entityIdField = $attribute->getBackend()->getEntityIdField();

        $data = array(
            'entity_type_id' => $object->getEntityTypeId(),
            $entityIdField => $object->getId(),
            'attribute_id' => $attribute->getId(),
            'store_id' => $storeId,
            'value' => $this->_prepareValueForSave($value, $attribute)
        );

        $this->_attributeValuesToSave[$table][] = $data;

        return $this;
    }

    public function getIsGlobal($attribute) {
        return $attribute->getData('is_global');
    }

    /**
     * Retrieve attribute is global scope flag
     *
     * @return bool
     */
    public function isScopeGlobal($attribute) {
        return $this->getIsGlobal($attribute) == 1;
    }

    /**
     * Retrieve attribute is website scope website
     *
     * @return bool
     */
    public function isScopeWebsite($attribute) {
        return $this->getIsGlobal($attribute) == 2;
    }

    /**
     * Retrieve attribute is store scope flag
     *
     * @return bool
     */
    public function isScopeStore($attribute) {
        return !$this->isScopeGlobal($attribute) && !$this->isScopeWebsite($attribute);
    }

    /**
     * Retrieve store id
     *
     * @return int
     */
    public function getStoreId() {
        $dataObject = $this->getDataObject();
        if ($dataObject) {
            return $dataObject->getStoreId();
        }
        return $this->getData('store_id');
    }

}