<?php

class Fedobe_Tribeseller_Model_Mysql4_Seller_Collection extends Mage_Eav_Model_Entity_Collection_Abstract {

    /**
     * Store id of application
     *
     * @var integer
     */
    protected $_storeId = null;

    public function _construct() {
        $this->_init('tribeseller/seller');
    }

    /**
     * Set store id
     *
     * @param integer $storeId
     * @return Fedobe_Tribeseller_Model_Mysql4_Seller_Collection
     */
    public function setStoreId($storeId) {
        $this->_storeId = $storeId;
        return $this;
    }

}
