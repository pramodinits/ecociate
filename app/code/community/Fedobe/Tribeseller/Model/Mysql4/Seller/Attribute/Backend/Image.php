<?php

class Fedobe_Tribeseller_Model_Mysql4_Seller_Attribute_Backend_Image extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract {

    /**
     * After save
     *
     * @param Varien_Object $object
     * @return Mage_Catalog_Model_Resource_Product_Attribute_Backend_Image
     */
    public function afterSave($object) {
        $value = $object->getData($this->getAttribute()->getName());
        if (is_array($value) && !empty($value['delete'])) {
            $object->setData($this->getAttribute()->getName(), '');
            $this->getAttribute()->getEntity()
                    ->saveAttribute($object, $this->getAttribute()->getName());
            return;
        }

        try {
            $uploader = new Mage_Core_Model_File_Uploader($this->getAttribute()->getName());
            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png', 'pdf'));
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
        } catch (Exception $e) {
            return $this;
        }
        if (!file_exists(Mage::getBaseDir('media') . '/tribe/sellers/images')) {
            mkdir(Mage::getBaseDir('media') . '/tribe/sellers/images');
            chmod(Mage::getBaseDir('media') . '/tribe/sellers/images', 0777);
        }
        $uploader->save(Mage::getBaseDir('media') . '/tribe/sellers/images');

        $fileName = $uploader->getUploadedFileName();
        if ($fileName) {
            $object->setData($this->getAttribute()->getName(), $fileName);
            $this->getAttribute()->getEntity()
                    ->saveAttribute($object, $this->getAttribute()->getName());
        }
        return $this;
    }

}
