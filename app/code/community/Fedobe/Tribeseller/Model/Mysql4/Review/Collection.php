<?php

class Fedobe_Tribeseller_Model_Mysql4_Review_Collection extends Mage_Review_Model_Resource_Review_Collection {

    protected function _initSelect() {
        $this->getSelect()->from(array('main_table' => $this->getMainTable()));
        $this->getSelect()
                ->join(array('detail' => $this->_reviewDetailTable), 'main_table.review_id = detail.review_id', array('detail_id', 'title', 'detail', 'nickname', 'customer_id','custom_rating'));
        return $this;
        ;
    }

}
