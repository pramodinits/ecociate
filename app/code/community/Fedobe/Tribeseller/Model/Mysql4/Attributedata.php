<?php

class Fedobe_Tribeseller_Model_Mysql4_Attributedata extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('tribeseller/eav_attribute', 'attribute_id');
    }

}
