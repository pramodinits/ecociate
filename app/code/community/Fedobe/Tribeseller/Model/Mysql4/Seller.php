<?php

class Fedobe_Tribeseller_Model_Mysql4_Seller extends Fedobe_Tribeseller_Model_Mysql4_Seller_Abstract {

    public function _construct() {
        $resource = Mage::getSingleton('core/resource');
        $this->setType('fedobe_tribe_seller');
        $this->setConnection(
                $resource->getConnection('tribeseller_read'), $resource->getConnection('tribeseller_write')
        );
    }

    protected function _getDefaultAttributes() {
        return array('entity_id', 'entity_type_id', 'attribute_set_id', 'type_id', 'seller_store_name', 'seller_email', 'seller_user_name', 'created_at', 'updated_at');
    }

    public function getCategoryIds($seller) {
        $adapter = $this->_getReadAdapter();
//      $entity_table = $this->getTable('tribeseller/seller_entity');
        $select = $adapter->select()
                ->from($this->getEntityTable(), 'category_ids')
                ->where('entity_id = ?', (int) $seller->getId());
        return $adapter->fetchCol($select);
    }

    public function getIdBySellerUsername($sellerusername) {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()
                ->from($this->getEntityTable(), 'entity_id')
                ->where('seller_user_name = :seller_user_name');

        $bind = array(':seller_user_name' => (string) $sellerusername);

        return $adapter->fetchOne($select, $bind);
    }

    public function getIdBySellerStorename($seller_store_name) {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()
                ->from($this->getEntityTable(), 'entity_id')
                ->where('seller_store_name = :seller_store_name');

        $bind = array(':seller_store_name' => (string) $seller_store_name);

        return $adapter->fetchOne($select, $bind);
    }

    public function getIdBySellerEmail($seller_email) {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()
                ->from($this->getEntityTable(), 'entity_id')
                ->where('seller_email = :seller_email');

        $bind = array(':seller_email' => (string) $seller_email);

        return $adapter->fetchOne($select, $bind);
    }

    public function getAdminUserId($username, $email) {
        $user = Mage::getModel('admin/user')->getCollection()
                ->addFieldToSelect('user_id')
                ->addFieldToFilter(
                array('username', 'email'), array(
            array('eq' => $username),
            array('eq' => $email)
                )
        );
        $user_id = $user->getData()[0]['user_id'];
        return $user_id;
    }

    public function checkAdminUser($username, $email, $userid = null) {
        //Here let's check user exeists or not
        $user = Mage::getModel('admin/user')->getCollection()
                ->addFieldToSelect('user_id')
                ->addFieldToFilter(
                array('username', 'email'), array(
            array('eq' => $username),
            array('eq' => $email)
                )
        );
        if ($userid) {
            $user->addFieldToFilter(array('user_id'), array(array('neq' => $userid)));
        }
        if (!$user->getData()) {
            return false;
        } else {
            return true;
        }
    }

    public function createAdminUser($seller, $roleid) {
        $userdata = array(
            'username' => $seller->getData('seller_user_name'),
            'firstname' => $seller->getData('seller_first_name'),
            'lastname' => $seller->getData('seller_last_name'),
            'email' => $seller->getData('seller_email'),
            'password' => $seller->getData('seller_password'),
            'is_active' => ($seller->getData('seller_status') == 2) ? 1 : 0
        );
        if (Mage::registry('update_seller')) {

            //Here to update seller information
            $adminuserid = Mage::registry('update_seller');
            $user = Mage::getModel('admin/user')->load($adminuserid);
            $user->addData($userdata)->save();
        } else {
            //Here to add new admin user
            $user = Mage::getModel('admin/user')->setData($userdata)->save();
            $adminuserid = $user->getUserId();
        }
        $user->setRoleIds(array($roleid))
                ->setRoleUserId($id)
                ->saveRelations();
    }

    protected function _beforeSave(Varien_Object $object) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $storeId = Mage::app()->getRequest()->getParam('store');
        if (!$object->getId()) {
            $object->setId($this->getIdBySellerStorename($object->getSellerStoreName()));
        }
        $customermodel = mage::getModel('customer/customer');
        $firstname = $object->getData('seller_first_name');
        $lastname = $object->getData('seller_last_name');
        $email = $object->getData('seller_email');
        $username = $object->getData('seller_user_name');
        $pasword = $object->getData('seller_password');

        //Here let's check the admin user exist or not
        $entity_id = $object->getData('entity_id');
        if ($entity_id) {
            //Seller Edit
            $seller = Mage::getModel('tribeseller/tribeseller')->load($entity_id);
            $adminuserid = $this->getAdminUserId($seller->getSellerUserName(), $seller->getSellerEmail());
            $adminusercheck = $this->checkAdminUser($username, $email, $adminuserid);
            if ($adminusercheck) {
                throw Mage::exception(
                        'Mage_Customer', Mage::helper('tribeseller')->__('User already exists'), Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS
                );
            }
            //Here to mark to update admin credentials
            Mage::unregister('update_seller');
            Mage::register('update_seller', $adminuserid);
        } else {
            //Seller New
            $adminusercheck = $this->checkAdminUser($username, $email);
            if ($adminusercheck) {
                throw Mage::exception(
                        'Mage_Customer', Mage::helper('tribeseller')->__('User already exists'), Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS
                );
            }
        }
        //Here let's check the customer type if new then create one customer
        $customerdat = array();
        $customerdat['firstname'] = $firstname;
        $customerdat['lastname'] = $lastname;
        $customerdat['username'] = $username;
        $customerdat['email'] = $email;
        $customerdat['password'] = $pasword;

        //if ($object->getData('seller_customer_type') == 1) {
            //New
            $tableName = $resource->getTableName('customer_entity');
            $query = "SELECT `entity_id` FROM $tableName WHERE `email` = '$email' LIMIT 1";
            $res = $readConnection->query($query);
            $cust = $res->fetch();
            if ($entity_id) {
                $selleremail = $seller->getData('seller_email');
                $sellerusername = $seller->getData('seller_user_name');
                //echo $email .'=='. $selleremail;exit;
                if ($email == $selleremail) {
                    //update informations checking username as this user name is unique
                    $customer_id = $cust['entity_id'];
                    $collection = Mage::getModel('customer/customer')->getCollection();
                    $collection->addAttributeToFilter('entity_id', array('neq' => $customer_id));
                    $collection->addAttributeToFilter('username', array('eq' => $username));
                    //print_r($cust);exit;
                    //echo $customer_id;
                    //echo "<pre>";print_r($collection->getData());exit;
                    if (empty($collection->getData())) {
                        //Here update the informations 
                        $customer = $customermodel->load($customer_id);
                        //Here let's update the informations for this customer
                        $customer->setFirstname($firstname);
                        $customer->setLastname($lastname);
                        $customer->setEmail($email);
                        $customer->setUsername($username);
                        $customer->save();
                        $customer_entity_id = $customermodel->getEntityId();
                        $object->setData('all_customers',$customer_entity_id);
                    } else {
                        //Raise exception
                        throw Mage::exception(
                                'Mage_Customer', Mage::helper('tribeseller')->__('This customer username already exists'), Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS
                        );
                    }
                } else {
                    if (!empty($cust)) {
                        //Raise exception
                        throw Mage::exception(
                                'Mage_Customer', Mage::helper('tribeseller')->__('This customer email already exists'), Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS
                        );
                    } else {
                        //Create new user
                        $collection = Mage::getModel('customer/customer')->getCollection();
                        $collection->addAttributeToFilter('username', array('eq' => $username));
                        if (empty($collection->getData())) {
                            $customermodel->setData($customerdat);
                            $customermodel->save();
                            $customer_entity_id = $customermodel->getEntityId();
                            $object->setData('all_customers',$customer_entity_id);
                        } else {
                            //Raise exception
                            /*throw Mage::exception(
                                    'Mage_Customer', Mage::helper('tribeseller')->__('This customer username already exists'), Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS
                            );*/
                        }
                    }
                }
            } else {
                //New
                
                if (empty($cust)) {
                    //Create new user
                    $collection = Mage::getModel('customer/customer')->getCollection();
                    $collection->addAttributeToFilter('username', array('eq' => $username));
                    if (empty($collection->getData())) {
                        $customermodel->setData($customerdat);
                        $customermodel->save();
                        $customer_entity_id = $customermodel->getEntityId();
                        $object->setData('all_customers',$customer_entity_id);
                    } else {
                        //Raise exception
                        throw Mage::exception(
                                'Mage_Customer', Mage::helper('tribeseller')->__('This customer username already exists'), Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS
                        );
                    }
                } else {
                    //Raise exception
                    throw Mage::exception(
                            'Mage_Customer', Mage::helper('tribeseller')->__('This customer email already exists'), Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS
                    );
                }
            }
      //  } 
       /* else {
            //Exisitng
            $customerid = $object->getData('all_customers');
            $customer = $customermodel->load($customerid);
            //Here let's update the informations for this customer
            $customer->setFirstname($firstname);
            $customer->setLastname($lastname);
            $customer->setEmail($email);
            $customer->setUsername($username);
            $customer->save();
        }*/
        return parent::_beforeSave($object);
    }

    protected function _afterSave(Varien_Object $seller) {
        $is_edit = $seller->getData('edit_mode');
        try {
            //Here to update more informations in Seller entity
            $sellerentity = Mage::getModel('tribeseller/tribeseller')->load($seller->getId());
            $extradatatosave = $this->extraSellerDataTosave();
            $geoaddr = explode(',', $seller->getData('seller_direction_map')); //print_r($geoaddr);exit;

            $extradata = array();
            foreach ($extradatatosave as $k => $v) {
                switch ($v) {
                    case 'is_active':
                        $extradata[$v] = $seller->getData('seller_status');
                        break;
                    case 'seller_geolat':
                        $extradata[$v] = $geoaddr[0];
                        break;
                    case 'seller_geolng':
                        $extradata[$v] = $geoaddr[1];
                        break;
                    default:
                        $extradata[$v] = $seller->getData($v);
                        break;
                }
            }
           // print_r($extradata);exit;
            if (!empty($extradata)) {
                $sellerentity->setData($extradata);
                $sellerentity->save();
            }
//            if (!$is_edit) {
            //End
            //Here let's create the admin user and assign the seller role
            $roleid = $this->getSellerRoleId();
            $this->createAdminUser($seller, $roleid);
            //End
//            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
        return parent::_afterSave($seller);
    }

    public function getSellerRoleId() {
        $roleid = 0;
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $tableName = $resource->getTableName('admin_role');
        $query = "SELECT `role_id` FROM $tableName WHERE role_name = 'Tribe Sellers' LIMIT 1";
        $res = $readConnection->query($query);
        while ($row = $res->fetch()) {
            $roleid = $row['role_id'];
        }
        return $roleid;
    }

    public function extraSellerDataTosave() {
        return array(
            'entity_id',
            'seller_store_name',
            'seller_partner',
            'seller_url',
            'seller_first_name',
            'seller_last_name',
            'seller_user_name',
            'seller_email',
            'seller_direction_map',
            'seller_geolat',
            'seller_geolng',
            'seller_password',
            'is_active',
            'products_data',
            'category_ids'
        );
    }

    public function getDefaultAttributeSourceModel() {
        return 'eav/entity_attribute_source_table';
    }

    /**
     * Duplicate product store values
     *
     * @param int $oldId
     * @param int $newId
     * @return Mage_Catalog_Model_Resource_Product
     */
    public function duplicate($oldId, $newId) {
        $adapter = $this->_getWriteAdapter();
        $eavTables = array('datetime', 'decimal', 'int', 'text', 'varchar');
        $adapter = $this->_getWriteAdapter();
        // duplicate EAV store values
        foreach ($eavTables as $suffix) {
            $tableName = $this->getTable(array('tribeseller/seller_entity', $suffix));
            $select = $adapter->select()
                    ->from($tableName, array(
                        'entity_type_id',
                        'attribute_id',
                        'store_id',
                        'entity_id' => new Zend_Db_Expr($adapter->quote($newId)),
                        'value'
                    ))
                    ->where('entity_id = ?', $oldId)
                    ->where('store_id > ?', 0);

            $adapter->query($adapter->insertFromSelect(
                            $select, $tableName, array(
                        'entity_type_id',
                        'attribute_id',
                        'store_id',
                        'entity_id',
                        'value'
                            ), Varien_Db_Adapter_Interface::INSERT_ON_DUPLICATE
            ));
        }

        // set status as disabled
        $statusAttribute = $this->getAttribute('status');
        $statusAttributeId = $statusAttribute->getAttributeId();
        $statusAttributeTable = $statusAttribute->getBackend()->getTable();
        $updateCond[] = 'store_id > 0';
        $updateCond[] = $adapter->quoteInto('entity_id = ?', $newId);
        $updateCond[] = $adapter->quoteInto('attribute_id = ?', $statusAttributeId);
        $adapter->update(
                $statusAttributeTable, array('value' => Mage_Catalog_Model_Product_Status::STATUS_DISABLED), $updateCond
        );

        return $this;
    }

    public function getSellersStoreName(array $sellerIds) {
        $select = $this->_getReadAdapter()->select()
                ->from($this->getEntityTable(), array('entity_id', 'seller_store_name'))
                ->where('entity_id IN (?)', $sellerIds);
        return $this->_getReadAdapter()->fetchAll($select);
    }

    public function getProductEntitiesInfo($columns = null) {
        if (!empty($columns) && is_string($columns)) {
            $columns = array($columns);
        }
        if (empty($columns) || !is_array($columns)) {
            $columns = $this->_getDefaultAttributes();
        }

        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()
                ->from($this->getEntityTable(), $columns);

        return $adapter->fetchAll($select);
    }

    public function getAssignedImages($seller, $storeIds) {
        if (!is_array($storeIds)) {
            $storeIds = array($storeIds);
        }
        $mainTable = $seller->getResource()->getAttribute('seller_media_gallery')
                ->getBackend()
                ->getTable();
        $read = $this->_getReadAdapter();
        $select = $read->select()
                ->from(
                        array('images' => $mainTable), array('value as filepath', 'store_id')
                )
                ->joinLeft(
                        array('attr' => $this->getTable('eav/attribute')), 'images.attribute_id = attr.attribute_id', array('attribute_code')
                )
                ->where('entity_id = ?', $seller->getId())
                ->where('store_id IN (?)', $storeIds)
                ->where('attribute_code IN (?)', array('small_image', 'thumbnail', 'image'));

        $images = $read->fetchAll($select);
        return $images;
    }

    /**
     * Retrieve a collection of products associated with the landingpage page
     *
     * @return Mage_Catalog_Model_Resource_Eav_Resource_Product_Collection
     */
    public function getProductCollection(Fedobe_Tribeseller_Model_Seller $seller) {
        $storeId = ((int) $seller->getStoreId() === 0) ? Mage::app()->getStore()->getId() : $page->getStoreId();

        $collection = Mage::getResourceModel('catalog/product_collection')
                ->setStoreId($storeId)
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter('visibility', array('in' => array(
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH
        )));
        $ids = $seller->getProductsData() ? explode(',', $seller->getProductsData()) : array(0);
        $collection->addFieldToFilter('entity_id', array('in' => $ids));
        return $collection;
    }

}