<?php

class Fedobe_Tribeseller_Model_Mysql4_Sellers_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    public function _construct() {
        $this->_init('tribeseller/sellers');
    }

    /**
     * initialize select object
     *
     * @return Mage_Catalog_Model_Resource_Product_Attribute_Collection
     */
    protected function _initSelect() {
        $this->getSelect()->from(array('main_table' => $this->getResource()->getMainTable()));
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
            $sellerid = Mage::helper('tribeseller')->getSellerIdFromadminUser();
            $this->getSelect()->where("entity_id = $sellerid");
        }
        return $this;
    }

}