<?php

class Fedobe_Tribeseller_ProductsellersController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        $simple_product_id = Mage::app()->getRequest()->getParam('simpleproductid');
        Mage::unregister('simple_product_id');
        Mage::register('simple_product_id', $simple_product_id);
        $response = $this->getLayout()->createBlock('tribeseller/product_sellers')->setProductId(Mage::app()->getRequest()->getParam('simpleproductid'))->toHtml();
        $returnres = array('sellers' => $response);
        echo Mage::helper('core')->jsonEncode($returnres);
    }

}