<?php

require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Sales' . DS . 'Order' . DS . 'InvoiceController.php';

class Fedobe_Tribeseller_Adminhtml_Sales_Order_InvoiceController extends Mage_Adminhtml_Sales_Order_InvoiceController {

    protected function _getItemQtys() {
        $qtys = array();
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
            $orderId = $this->getRequest()->getParam('order_id');
            $data = $this->getRequest()->getParam('invoice');
                $items = Mage::helper('tribeseller')->getSellerItems($orderId);
                if (!empty($items)) {
                foreach ($items as $k => $v) {
                        $qtys[$k]=(!empty($data) && isset($data['items'][$k]))?$data['items'][$k]: intval($v['qty_ordered']);
                }
                $oitems = Mage::helper('tribeseller')->getOtherItemIds($orderId);
                foreach ($oitems as $k => $v) {
                    $qtys[$k] = 0;
                }
            }
            
        } else {
            $data = $this->getRequest()->getParam('invoice');
            if (isset($data['items'])) {
                $qtys = $data['items'];
            }
        }
        return $qtys;
    }

}
