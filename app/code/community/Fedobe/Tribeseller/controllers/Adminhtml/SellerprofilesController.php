<?php

/**
 * Manage Seller Attribute controller file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribeseller
 * @author      Fedobe Magento Team
 */
class Fedobe_Tribeseller_Adminhtml_SellerprofilesController extends Mage_Adminhtml_Controller_Action {

    protected $_entityTypeId;

    public function preDispatch() {
        parent::preDispatch();
        $this->_entityTypeId = Mage::helper('tribeseller')->getEntityTypeId();
    }

    /**
     * Init actions
     *
     */
    protected function _initAction() {
        // load layout, set active menu and breadcrumbs
        $this->_title($this->__('Shop'))
                ->_title($this->__('Manage Profiles'));

        if ($this->getRequest()->getParam('popup')) {
            $this->loadLayout('popup');
        } else {
            $this->loadLayout()
                    ->_setActiveMenu('fedobe/tribeseller_manage_sellers');
        }
        return $this;
    }

    /**
     * Index action method
     */
    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }

    /**
     * Code to display the form
     * 
     * The main form container gets added to the content and the tabs block gets added to left.
     */
    public function newAction() {
        $attrsetid = $this->getRequest()->getParam('set');
        if ($attrsetid) {
            $this->_forward('edit');
        } else {
            $this->loadLayout();
            $this->renderLayout();
        }
    }

    public function checkurlkeyAction() {
        $data = array();
        $urlkey = $this->getRequest()->getPost('urlkey');
        $id = $this->getRequest()->getPost('id');
        $model = Mage::getModel('tribeseller/sellers')->getCollection()
                ->addFieldToSelect('entity_id')
                ->addFieldToFilter('seller_user_name', array('eq' => $urlkey))
                ->addFieldToFilter('entity_id', array('neq' => $id));
        $model->getSelect()->limit(1);
        
        if($id){
            $savedusername = Mage::getModel('tribeseller/sellers')->load($id)->getSellerUserName();
            $user = Mage::getModel('admin/user')->getCollection()
                ->addFieldToSelect('user_id')
                ->addFieldToFilter('username', array('eq' => $savedusername));
            $savedid = $user->getUserId();
        }
        $user = Mage::getModel('admin/user')->getCollection()
                ->addFieldToSelect('user_id')
                ->addFieldToFilter('username', array('eq' => $urlkey));
        if($savedid){
            $user->addFieldToFilter('user_id', array('neq' => $id));
        }
        $err = 1;
        if(!$model->getData() && !$user->getUserId()){
            $err = 0;
        }
        $data['err'] = $err;
        $data['msg'] = ($err) ? Mage::helper('tribeseller')->__("A Store with this username already exists!") :Mage::helper('tribeseller')->__("Username available") ;
        echo Mage::helper('core')->jsonEncode($data);
    }

    public function getCustomerInfoAction(){
        $data = array();
        $customer_id = $this->getRequest()->getPost('customer_id');
        $user = mage::getModel('customer/customer')->load($customer_id);
        $custdata = $user->getData();
        if ($custdata) {
            //Here let's check username exists or not if not there
            //then save it by auto creating from email
            if(!$custdata['username']){
                $username = Mage::helper('tribeseller')->clean($custdata['email']);
                $user->setUsername($username);
                $user->save();
            }
            $data['err'] = 0;
            $data = $user->getData();
            $data['url'] = Mage::helper('tribeseller')->getFrontendProductUrl($data['username']);
            $data['data'] = $data;
        } else {
            $data['err'] = 1;
            $data['data'] = NULL;
        }
        echo Mage::helper('core')->jsonEncode($data);
    }

    /**
     * Edit Seller Attribute
     */
    public function editAction() {
        $sellerId = $this->getRequest()->getParam('id');
        $seller = $this->_initSeller();
        if ($sellerId && !$seller->getId()) {
            $this->_getSession()->addError(Mage::helper('tribeseller')->__('This Shop no longer exists.'));
            $this->_redirect('*/*/');
            return;
        }
        Mage::dispatchEvent('tribe_seller_edit_action', array('seller' => $seller));
        // Sets the window title
        $this->_title($id ? $model->getSellerStoreName() : $this->__('New Shop'));
        // 5. Build edit form
        $this->_initAction()
                ->_addBreadcrumb(
                        $id ? Mage::helper('tribeseller')->__('Edit Shop ') : Mage::helper('tribeseller')->__('New Shop '), $id ? Mage::helper('tribeseller')->__('Edit Shop') : Mage::helper('tribeseller')->__('New Shop'));
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->renderLayout();
    }

    public function validateAction() {
        $response = new Varien_Object();
        $response->setError(false);

        $attributeCode = $this->getRequest()->getParam('attribute_code');
        $attributeId = $this->getRequest()->getParam('attribute_id');
        $entityType = Mage::getModel('eav/entity_type')->load($this->_entityTypeId)->getentityTypeCode();
        $attribute = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode($entityType, $attributeCode);
        if ($attribute && !$attributeId) {
            Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('tribeseller')->__('Attribute with the same code already exists'));
            $this->_initLayoutMessages('adminhtml/session');
            $response->setError(true);
            $response->setMessage($this->getLayout()->getMessagesBlock()->getGroupedHtml());
        }

        $this->getResponse()->setBody($response->toJson());
    }

    /**
     * Filter post data
     *
     * @param array $data
     * @return array
     */
    protected function _filterPostData($data) {
        if ($data) {
            /** @var $helperCatalog Mage_Catalog_Helper_Data */
            $helperCatalog = Mage::helper('tribeseller');
            //labels
            foreach ($data['frontend_label'] as & $value) {
                if ($value) {
                    $value = $helperCatalog->stripTags($value);
                }
            }

            if (!empty($data['option']) && !empty($data['option']['value']) && is_array($data['option']['value'])) {
                foreach ($data['option']['value'] as $key => $values) {
                    $data['option']['value'][$key] = array_map(array($helperCatalog, 'stripTags'), $values);
                }
            }
        }
        return $data;
    }

    public function wysiwygAction() {
        $elementId = $this->getRequest()->getParam('element_id', md5(microtime()));
        $storeId = $this->getRequest()->getParam('store_id', 0);
        $storeMediaUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);

        $content = $this->getLayout()->createBlock('adminhtml/catalog_helper_form_wysiwyg_content', '', array(
            'editor_element_id' => $elementId,
            'store_id' => $storeId,
            'store_media_url' => $storeMediaUrl,
        ));
        $this->getResponse()->setBody($content->toHtml());
    }

    public function productsAction() {
        $this->_initSeller();
        $this->loadLayout();
        $this->getLayout()->getBlock('tribe.sellers.products.tab');
        $this->renderLayout();
    }

    protected function _initSeller() {
        $sellerId = (int) $this->getRequest()->getParam('id');
        $seller = Mage::getModel('tribeseller/seller')
                ->setStoreId($this->getRequest()->getParam('store', 0))
                ->setEntityTypeId($this->_entityTypeId);
        if ($sellerId) {
            try {
                $seller->load($sellerId);
                $seller->setStoreId($this->getRequest()->getParam('store', 0));
                $seller->setData('edit_mode', 1);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        } else {
             if ($setId = (int) $this->getRequest()->getParam('set')) {
                $seller->setAttributeSetId($setId);
            }
            $seller->setData('edit_mode', 0);
        }
        Mage::register('sellerprofile_data', $seller);
        Mage::register('current_seller', $seller);
        return $seller;
    }

    /**
     * Initialize product before saving
     */
    protected function _initSellerSave() {
        $seller = $this->_initSeller();
        $sellerData = $this->getRequest()->getPost('seller');
        $seller->addData($sellerData);
        /**
         * Check "Use Default Value" checkboxes values
         */
        if ($useDefaults = $this->getRequest()->getPost('use_default')) {
            foreach ($useDefaults as $attributeCode) {
                $seller->setData($attributeCode, false);
            }
        }

        /**
         * Init product links data (related, upsell, crosssel)
         */
        $seller_products = array();
        $links = $this->getRequest()->getPost('sellerlinks');
        if (isset($links['products'])) {
            $seller_products = array_keys(Mage::helper('adminhtml/js')->decodeGridSerializedInput($links['products']));
            $seller_product_ids = implode(",", $seller_products);
            $seller->setProductsData($seller_product_ids);
        }
        /**
         * Initialize product categories
         */
        if ($seller_products) {
            $categoryIds = Mage::helper('tribeseller')->getProductCategories($seller_products);
            if (!$categoryIds) {
                $categoryIds = '';
            }
            $seller->setCategoryIds($categoryIds);
        }
        Mage::dispatchEvent(
                'tribe_seller_prepare_save', array('product' => $seller, 'request' => $this->getRequest())
        );
        return $seller;
    }

    /**
     * Get upsell products grid
     */
    public function productsgridAction() {
        $this->_initSeller();
        $this->loadLayout();
        $this->getLayout()->getBlock('tribe.sellers.products.tab')
                ->setProductsData($this->getRequest()->getPost('products_data', null));
        $this->renderLayout();
    }

    public function reviewsAction() {
        $this->_initSeller();
        $this->loadLayout();
        $this->getLayout()->getBlock('tribe.sellers.reviews')
                ->setSellerId(Mage::registry('sellerprofile_data')->getId())
                ->setUseAjax(true);
        $this->renderLayout();
    }

    /**
     * 
     * Save seller attributes
     */
    public function saveAction() {
        $storeId = $this->getRequest()->getParam('store');
        $redirectBack = $this->getRequest()->getParam('back', false);
        $sellerId = $this->getRequest()->getParam('id');
        $data = $this->getRequest()->getPost(); //echo "<pre>";print_r($data);exit;
        if ($data) {
            $seller = $this->_initSellerSave();
            try {
                $seller->save();
                $sellerId = $seller->getId();
                $this->_getSession()->addSuccess($this->__('The Shop has been saved.'));
                
                
                //bhagyashree
                 if($data['seller']['seller_status_old'] == "1" && $data['seller']['seller_status'] == "2"){
                    // inactive to active case
                    
                    
                    //code to send mail to seller       
    $fname = $data['seller']['seller_first_name'];
    $body ='Hello <b>'.$fname.',</b><br/><br/> Your account has been activated.The following are the details:<br/><br/> URL: http://dev.ecociate.com/manage  <br/> User Name: '.$data['seller']['seller_user_name'].' <br/> Password: '.$data['seller']['seller_password'];
    $name = 'Ecociate';
    $to_mail = $data['seller']['seller_email'];
    $seller_mail = Mage::getModel('core/email');
    $seller_mail->setToName($name);
    $seller_mail->setToEmail($to_mail); 
    //$seller_mail->setToEmail('bhagyashree@fedobe.com');
    $seller_mail->setBody($body);


    $seller_mail->setSubject('Registered Seller account is activated');
    $seller_mail->setFromEmail('dev.ecociate.com');
    $seller_mail->setFromName($name);
    $seller_mail->setType('html');// You can use 'html' or 'text'

    try {
    $seller_mail->send();
    }
    catch (Exception $e) {
    }
     //ends here 
     
     
                 }
                //ends here
                
                
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage())
                        ->setSellerData($data);
                $redirectBack = true;
            } catch (Exception $e) {
                Mage::logException($e);
                $this->_getSession()->addError($e->getMessage());
                $redirectBack = true;
            }
        }
        if ($redirectBack) {
            $this->_redirect('*/*/edit', array(
                'id' => $sellerId,
                '_current' => true
            ));
        } else {
            $this->_redirect('*/*/', array('store' => $storeId));
        }
    }

    /**
     * Delete seller attribute
     *
     * @return null
     */
    public function deleteAction() {
        if ($id = $this->getRequest()->getParam('id')) {
            $model = Mage::getModel('tribeseller/tribeseller');

            // entity type check
            $model->load($id);

            if ($model->getEntityTypeId() != $this->_entityTypeId) {
                Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('tribeseller')->__('This Shop cannot be deleted.'));
                $this->_redirect('*/*/');
                return;
            }

            try {
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('tribeseller')->__('The Shop has been deleted.'));
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('attribute_id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('tribeseller')->__('Unable to find a seller to delete.'));
        $this->_redirect('*/*/');
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    /*protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('tribeseller/manage_sellers');
    }*/

    public function massDeleteAction() {
        $sellerIds = $this->getRequest()->getParam('sellers');
        if (!is_array($sellerIds)) {
            $this->_getSession()->addError($this->__('Please select Shop(s).'));
        } else {
            if (!empty($sellerIds)) {
                try {
                    foreach ($sellerIds as $sellerId) {
                        $seller = Mage::getSingleton('tribeseller/tribeseller')->load($sellerId);
                        Mage::dispatchEvent('sellerprofiles_controller_seller_delete', array('seller' => $seller));
                        $seller->delete();
                    }
                    $this->_getSession()->addSuccess(
                            $this->__('Total of %d record(s) have been deleted.', count($sellerIds))
                    );
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * Update product(s) status action
     *
     */
    public function massStatusAction() {
        $sellerIds = (array) $this->getRequest()->getParam('sellers');
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        $status = (int) $this->getRequest()->getParam('status');
        try {
            foreach ($sellerIds as $sellerId) {
                $seller = Mage::getSingleton('tribeseller/seller')->load($sellerId);
                $seller->setData('seller_status', $status);
                $seller->setData('is_active', $status);
                $seller->save();
            }
            $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been updated.', count($sellerIds))
            );
        } catch (Mage_Core_Model_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()
                    ->addException($e, $this->__('An error occurred while updating the Shop (s) status.'));
        }

        $this->_redirect('*/*/', array('store' => $storeId));
    }
    public function isemailexistAction(){
        $data=array();
        $email_id = $this->getRequest()->getPost('email');
        $ret = Mage::helper('tribeseller')->checkemailidlinked($email_id);
        $data['ret'] = $ret;
        echo Mage::helper('core')->jsonEncode($data);
    }

}
