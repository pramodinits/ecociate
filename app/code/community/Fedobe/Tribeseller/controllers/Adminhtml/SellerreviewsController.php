<?php

class Fedobe_Tribeseller_Adminhtml_SellerreviewsController extends Mage_Adminhtml_Controller_Action {

    /**
     * Array of actions which can be processed without secret key validation
     *
     * @var array
     */
    protected $_publicActions = array('edit');

    public function indexAction() {
        $this->_title($this->__('Shop'))
                ->_title($this->__('Reviews and Ratings'));

        $this->_title($this->__('All Reviews'));

        if ($this->getRequest()->getParam('ajax')) {
            return $this->_forward('reviewGrid');
        }

        $this->loadLayout();
        $this->_setActiveMenu('fedobe/tribeseller_manage_reviews_rating_manage_pending_reviews');

        $this->_addContent($this->getLayout()->createBlock('tribeseller/adminhtml_review_main'));

        $this->renderLayout();
    }

    public function pendingAction() {
        $this->_title($this->__('Shop'))
                ->_title($this->__('Reviews and Ratings'));
        $this->_title($this->__('Pending Reviews'));

        if ($this->getRequest()->getParam('ajax')) {
            Mage::register('usePendingFilter', true);
            return $this->_forward('reviewGrid');
        }

        $this->loadLayout();
        $this->_setActiveMenu('fedobe/tribeseller_manage_reviews_rating_manage_pending_reviews');

        Mage::register('usePendingFilter', true);
        $this->_addContent($this->getLayout()->createBlock('tribeseller/adminhtml_review_main'));

        $this->renderLayout();
    }

    public function editAction() {
        $this->_title($this->__('Shop'))
                ->_title($this->__('Reviews and Ratings'));

        $this->_title($this->__('Edit Review'));

        $this->loadLayout();
        $this->_setActiveMenu('fedobe/tribeseller_manage_reviews_rating_manage_pending_reviews');

        $this->_addContent($this->getLayout()->createBlock('tribeseller/adminhtml_review_edit'));

        $this->renderLayout();
    }

    public function newAction() {
        $this->_title($this->__('Shop'))
                ->_title($this->__('Reviews and Ratings'));

        $this->_title($this->__('New Review'));

        $this->loadLayout();
        $this->_setActiveMenu('fedobe/tribeseller_manage_reviews_rating_manage_pending_reviews');

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addContent($this->getLayout()->createBlock('tribeseller/adminhtml_review_add'));
        $this->_addContent($this->getLayout()->createBlock('tribeseller/adminhtml_review_seller_grid'));

        $this->renderLayout();
    }

    public function saveAction() {
        if (($data = $this->getRequest()->getPost()) && ($reviewId = $this->getRequest()->getParam('id'))) {
            $review = Mage::getModel('review/review')->load($reviewId);
//            var_dump($review->getData());exit;
            $session = Mage::getSingleton('adminhtml/session');
            if (!$review->getId()) {
                $session->addError(Mage::helper('catalog')->__('The review was removed by another user or does not exist.'));
            } else {
                try {
                    //var_dump($data);exit;
                    $review->addData($data)->save();

                    $resource = Mage::getSingleton('core/resource');
                    $writeadapter = $resource->getConnection('core_write');
                    $writeadapter->update(
                            $resource->getTableName('review/review_detail'), array('custom_rating' => $data['custom_rating']), 'review_id = ' . $review->getId()
                    );
                    
                    //Here let's do reviews aggregation
                    $review_entityid = $review->getEntityIdByCode(Fedobe_Tribeseller_Block_Adminhtml_Review_Grid::ENTITY_SELLER_CODE);
                    $sellerid = $review->getEntityPkValue();
                    $seller = Mage::getModel('tribeseller/seller')->load($sellerid);
                    $seller->doreviewaggregation($review_entityid);

                    $session->addSuccess(Mage::helper('catalog')->__('The review has been saved.'));
                } catch (Mage_Core_Exception $e) {
                    $session->addError($e->getMessage());
                } catch (Exception $e) {
                    $session->addException($e, Mage::helper('catalog')->__('An error occurred while saving this review.'));
                }
            }

            return $this->getResponse()->setRedirect($this->getUrl($this->getRequest()->getParam('ret') == 'pending' ? '*/*/pending' : '*/*/'));
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction() {
        $reviewId = $this->getRequest()->getParam('id', false);
        $session = Mage::getSingleton('adminhtml/session');
        try {
            
            $model = Mage::getModel('review/review');
            $review = $model->load($reviewId);
            $sellerId = $review->getEntityPkValue();
            $review->delete();
            $review_entityid = $model->getEntityIdByCode(Fedobe_Tribeseller_Block_Adminhtml_Review_Grid::ENTITY_SELLER_CODE);
            
            //Here let's do reviews aggregation
            $seller = Mage::getModel('tribeseller/seller')->load($sellerId);
            $seller->doreviewaggregation($review_entityid);
            
            $session->addSuccess(Mage::helper('catalog')->__('The review has been deleted'));
            if ($this->getRequest()->getParam('ret') == 'pending') {
                $this->getResponse()->setRedirect($this->getUrl('*/*/pending'));
            } else {
                $this->getResponse()->setRedirect($this->getUrl('*/*/'));
            }
            return;
        } catch (Mage_Core_Exception $e) {
            $session->addError($e->getMessage());
        } catch (Exception $e) {
            $session->addException($e, Mage::helper('catalog')->__('An error occurred while deleting this review.'));
        }

        $this->_redirect('*/*/edit/', array('id' => $reviewId));
    }

    public function massDeleteAction() {
        $reviewsIds = $this->getRequest()->getParam('reviews');
        $session = Mage::getSingleton('adminhtml/session');

        if (!is_array($reviewsIds)) {
            $session->addError(Mage::helper('adminhtml')->__('Please select review(s).'));
        } else {
            try {
                $seller = array();
                $review_entityid = Mage::getModel('review/review')->getEntityIdByCode(Fedobe_Tribeseller_Block_Adminhtml_Review_Grid::ENTITY_SELLER_CODE);
                foreach ($reviewsIds as $reviewId) {
                    $model = Mage::getModel('review/review')->load($reviewId);
                    $sellerId = $model->getEntityPkValue();
                    $seller[$sellerId] = $sellerId;
                    $model->delete();
                }
                foreach ($seller as $k => $v) {
                    //Here let's do reviews aggregation
                    $seller = Mage::getModel('tribeseller/seller')->load($v);
                    $seller->doreviewaggregation($review_entityid);
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('adminhtml')->__('Total of %d record(s) have been deleted.', count($reviewsIds))
                );
            } catch (Mage_Core_Exception $e) {
                $session->addError($e->getMessage());
            } catch (Exception $e) {
                $session->addException($e, Mage::helper('adminhtml')->__('An error occurred while deleting record(s).'));
            }
        }

        $this->_redirect('*/*/' . $this->getRequest()->getParam('ret', 'index'));
    }

    public function massUpdateStatusAction() {
        $reviewsIds = $this->getRequest()->getParam('reviews');
        $session = Mage::getSingleton('adminhtml/session');

        if (!is_array($reviewsIds)) {
            $session->addError(Mage::helper('adminhtml')->__('Please select review(s).'));
        } else {
            /* @var $session Mage_Adminhtml_Model_Session */
            try {
                $status = $this->getRequest()->getParam('status');
                $seller = array();
                $review_entityid = Mage::getModel('review/review')->getEntityIdByCode(Fedobe_Tribeseller_Block_Adminhtml_Review_Grid::ENTITY_SELLER_CODE);
                foreach ($reviewsIds as $reviewId) {
                    $model = Mage::getModel('review/review')->load($reviewId);
                    $model->setStatusId($status)->save();
                    $sellerId = $model->getEntityPkValue();
                    $seller[$sellerId] = $sellerId;
                }
                foreach ($seller as $k => $v) {
                    //Here let's do reviews aggregation
                    $seller = Mage::getModel('tribeseller/seller')->load($v);
                    $seller->doreviewaggregation($review_entityid);
                }
                $session->addSuccess(
                        Mage::helper('adminhtml')->__('Total of %d record(s) have been updated.', count($reviewsIds))
                );
            } catch (Mage_Core_Exception $e) {
                $session->addError($e->getMessage());
            } catch (Exception $e) {
                $session->addException($e, Mage::helper('adminhtml')->__('An error occurred while updating the selected review(s).'));
            }
        }

        $this->_redirect('*/*/' . $this->getRequest()->getParam('ret', 'index'));
    }

    public function massVisibleInAction() {
        $reviewsIds = $this->getRequest()->getParam('reviews');
        $session = Mage::getSingleton('adminhtml/session');

        if (!is_array($reviewsIds)) {
            $session->addError(Mage::helper('adminhtml')->__('Please select review(s).'));
        } else {
            $session = Mage::getSingleton('adminhtml/session');
            /* @var $session Mage_Adminhtml_Model_Session */
            try {
                $stores = $this->getRequest()->getParam('stores');
                foreach ($reviewsIds as $reviewId) {
                    $model = Mage::getModel('review/review')->load($reviewId);
                    $model->setSelectStores($stores);
                    $model->save();
                }
                $session->addSuccess(
                        Mage::helper('adminhtml')->__('Total of %d record(s) have been updated.', count($reviewsIds))
                );
            } catch (Mage_Core_Exception $e) {
                $session->addError($e->getMessage());
            } catch (Exception $e) {
                $session->addException($e, Mage::helper('adminhtml')->__('An error occurred while updating the selected review(s).'));
            }
        }

        $this->_redirect('*/*/pending');
    }

    public function productGridAction() {
        $this->getResponse()->setBody($this->getLayout()->createBlock('adminhtml/review_product_grid')->toHtml());
    }

    public function reviewGridAction() {
        $this->getResponse()->setBody($this->getLayout()->createBlock('adminhtml/review_grid')->toHtml());
    }

    public function jsonProductInfoAction() {
        $response = new Varien_Object();
        $id = $this->getRequest()->getParam('id');
        if (intval($id) > 0) {
            $seller = Mage::getModel('tribeseller/seller')
                    ->load($id);

            $response->setId($id);
            $response->addData($seller->getData());
            $response->setError(0);
        } else {
            $response->setError(1);
            $response->setMessage(Mage::helper('catalog')->__('Unable to get the product ID.'));
        }
        $this->getResponse()->setBody($response->toJSON());
    }

    public function postAction() {
        $sellerId = $this->getRequest()->getParam('product_id', false);
        $session = Mage::getSingleton('adminhtml/session');

        if ($data = $this->getRequest()->getPost()) {
            if (Mage::app()->isSingleStoreMode()) {
                $data['stores'] = array(Mage::app()->getStore(true)->getId());
            } else if (isset($data['select_stores'])) {
                $data['stores'] = $data['select_stores'];
            }

            
            $review = Mage::getModel('review/review')->setData($data);

            $product = Mage::getModel('tribeseller/seller')->load($sellerId);
            $defaultStoreId = Mage::app()->getWebsite(true)->getDefaultGroup()->getDefaultStoreId();
            $model = Mage::getModel('review/review');
            $review_entityid = $model->getEntityIdByCode(Fedobe_Tribeseller_Block_Adminhtml_Review_Grid::ENTITY_SELLER_CODE);
            try {
                $review->setEntityId($review_entityid)
                        ->setEntityPkValue($sellerId)
                        ->setStoreId($defaultStoreId)
                        ->setStatusId($data['status_id'])
                        ->setCustomerId(null)//null is for administrator only
                        ->save();
                $resource = Mage::getSingleton('core/resource');
                $writeadapter = $resource->getConnection('core_write');
                $writeadapter->update(
                        $resource->getTableName('review/review_detail'), array('custom_rating' => $data['custom_rating']), 'review_id = ' . $review->getId()
                );
                //Here let's do reviews aggregation
                $seller = Mage::getModel('tribeseller/seller')->load($sellerId);
                $seller->doreviewaggregation($review_entityid);

                $session->addSuccess(Mage::helper('catalog')->__('The review has been saved.'));
                if ($this->getRequest()->getParam('ret') == 'pending') {
                    $this->getResponse()->setRedirect($this->getUrl('*/*/pending'));
                } else {
                    $this->getResponse()->setRedirect($this->getUrl('*/*/'));
                }

                return;
            } catch (Mage_Core_Exception $e) {
                $session->addError($e->getMessage());
            } catch (Exception $e) {
                $session->addException($e, Mage::helper('adminhtml')->__('An error occurred while saving review.'));
            }
        }
        $this->getResponse()->setRedirect($this->getUrl('*/*/'));
        return;
    }

    public function ratingItemsAction() {
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('adminhtml/review_rating_detailed')->setIndependentMode()->toHtml()
        );
    }

    protected function _isAllowed() {
        switch ($this->getRequest()->getActionName()) {
            case 'pending':
                return Mage::getSingleton('admin/session')->isAllowed('catalog/reviews_ratings/reviews/pending');
                break;
            default:
                return Mage::getSingleton('admin/session')->isAllowed('catalog/reviews_ratings/reviews/all');
                break;
        }
    }

}