<?php

/**
 * @category    Fedobe
 * @package     Fedobe__Landingpage
 */
class Fedobe_Tribeseller_SellerspageController extends Mage_Core_Controller_Front_Action {

    /**
     * Display the splash page
     *
     * @return void
     */
    public function indexAction() {
        if ($this->getRequest()->getPost('seller')) {
            $post_Data = $this->getRequest()->getPost('seller');
            //Here let's set zip 
            Mage::helper('tribeseller')->setCurrentZipInUse($post_Data['pin']);
        }

        if (!Mage::getModel('core/cookie')->get('loginskiped') && !Mage::getSingleton('customer/session')->isLoggedIn() && !$this->getRequest()->getPost('skiplogin')) {
            $this->_redirect('*/*/login');
        }
        if ($this->getRequest()->getPost('skiplogin') || Mage::getModel('core/cookie')->get('loginskiped') || Mage::getSingleton('customer/session')->isLoggedIn()) {
            Mage::getModel('core/cookie')->set('loginskiped', 1, 2592000);
            $this->_redirect('/');
        }
    }

    public function changepinAction() {
        if ($this->getRequest()->getPost('seller')) {
            $post_Data = $this->getRequest()->getPost('seller');
            //Here let's set zip 
            //Mage::helper('tribeseller')->getzipcodefromip($post_Data['pin']);
            $latlng = ltrim($post_Data['lat_lng'],'('); 
            $latlng = rtrim($latlng,')');
            $latlng = explode(',',$latlng); //print_r($latlng);exit;
            $latlng_arr =array();
            $latlng_arr =array('lat'=>$latlng[0],'lng'=>$latlng[1]);
            Mage::helper('tribeseller')->setCurrentZipInUse($post_Data['pin'],$latlng_arr);
            Mage::helper('tribeseller')->setCurrentRadiusInUse($post_Data['radius']);
        }
        $slug = Mage::helper('tribeseller')->getSellerUrlKeySlug();
        
        //$this->_redirect("$slug");
        $this->_redirect("");
    }

    public function setpinAction() {
        $pin = $this->getRequest()->getPost('pin');
        $latlng = $this->getRequest()->getPost('lat_lng');
        $latlng = ltrim($latlng,'('); 
        $latlng = rtrim($latlng,')');
        $latlng = explode(',',$latlng); //print_r($latlng);exit;
        $latlng_arr =array();
        $latlng_arr =array('lat'=>$latlng[0],'lng'=>$latlng[1]); //print_r($latlng_arr);exit;
        $res = Mage::helper('tribeseller')->setCurrentZipInUse($pin, $latlng_arr);
        echo 1;
    }

    public function allstoresAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function searchAction() {
        // Create a generic template block
        $block = $this->getLayout()->createBlock('core/template');

        // Assign your template to it
        // This path is relative to your current theme (eg: rwd/default/template/...)
        $block->setTemplate('fedobe/tribe/sellers/pin.phtml');

        // Render the template to the browser
        echo $block->toHtml();
    }

    public function loginAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function getDirectionAction() {
        $seller_id = $this->getRequest()->getParam('seller_id');
        $html = $this->getLayout()->createBlock("tribeseller/view_tabs")->setCurentSeller($seller_id)
                        ->setTemplate("fedobe/tribe/seller/direction-popup.phtml")->toHtml();
        echo $html;
    }

    public function setradiusAction() {
        $radius = $this->getRequest()->getPost('radius');
        $res = Mage::helper('tribeseller')->setCurrentRadiusInUse($radius);
        echo $res;
        exit;
    }
    
    // added by bhagyashree
     public function sellerregistrationAction(){
      // $html = $this->getLayout()->createBlock("tribeseller/view_register")->setTemplate("fedobe/tribe/seller/registerseller.phtml")->toHtml();
      //  echo $html;
      
      $this->loadLayout();
        $this->renderLayout();
    }
    

}