<?php

require_once Mage::getModuleDir('controllers', 'Mage_Cms') . DS . 'IndexController.php';

class Fedobe_Tribeseller_IndexController extends Mage_Cms_IndexController {

    public function indexAction() {
        if (Mage::getModel('core/cookie')->get('loginskiped') || Mage::getSingleton('customer/session')->isLoggedIn()) {
            $slug = trim(Mage::helper('tribeseller')->getSellerUrlKeySlug());
            $this->_redirect($slug);
        } else {
            $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_HOME_PAGE);
            if (!Mage::helper('cms/page')->renderPage($this, $pageId)) {
                $this->_forward('defaultIndex');
            }
        }
    }

}