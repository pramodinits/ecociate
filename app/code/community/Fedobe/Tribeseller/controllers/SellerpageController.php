<?php

/**
 * @category    Fedobe
 * @package     Fedobe__Landingpage
 */
class Fedobe_Tribeseller_SellerpageController extends Mage_Core_Controller_Front_Action {

    /**
     * Display the splash page
     *
     * @return void
     */
    const ENTITY_SELLER_CODE = 'seller';

    public function viewAction() {

        if (($seller = $this->_initSeller()) === false) {
            return $this->_forward('noRoute');
        }
        // Register the splash layer model
        Mage::register('current_layer', Mage::getSingleton('tribeseller/layer'));

        $this->_applyCustomViewLayout($seller);
        if ($rootBlock = $this->getLayout()->getBlock('root')) {
            $rootBlock->addBodyClass('splash-page-' . $seller->getId());
            $rootBlock->addBodyClass('splash-page-' . $seller->getAttributeCode());
        }
        $this->setHeaders();
        //echo 333;exit;
        if (Mage::helper('catalin_seo')->isAjaxEnabled() && $this->getRequest()->isAjax()) {//echo 666;exit;
            $block = $this->getLayout()->getBlock('seller.leftnav');
            $listing = $this->getLayout()->getBlock('product_list')->toHtml();
            $layer = $block->toHtml();

            $urlModel = Mage::getSingleton('core/url');
            $listing = $urlModel->sessionUrlVar($listing);
            $layer = $urlModel->sessionUrlVar($layer);
            $topcat_listing = $this->getCategoryFilterHtml();

            $response = array(
                'listing' => $listing,
                'layer' => $layer,
                'topcatlisitng' => $topcat_listing,
            );
            $this->getResponse()->setHeader('Content-Type', 'application/json', true);
            $this->getResponse()->setBody(json_encode($response));
        } else {
            $this->loadLayout();
            $rootlayout = Mage::helper('tribeseller')->getPageLayout();
            $this->getLayout()->getBlock('root')->setTemplate("page/$rootlayout");
            $this->renderLayout();
        }
    }

    public function setHeaders() {
        $seller = Mage::registry('current_seller');
        if ($headBlock = $this->getLayout()->getBlock('head')) {
            if ($seller->getSellerMetaTitle()) {
                $headBlock->setTitle($seller->getSellerMetaTitle());
            } else {
                $headBlock->setTitle($seller->getSellerStoreName());
            }
            if ($description = $seller->getSellerMetaDescription()) {
                $headBlock->setDescription($description);
            }
            if ($keywords = $seller->getSellerMetaKeywords()) {
                $headBlock->setKeywords($keywords);
            }
            $headBlock->addItem('link_rel', $seller->getSellerUrl(), 'rel="canonical"');
        }
    }

    public function homeAction() {
        if (($seller = $this->_initSeller()) === false) {
            return $this->_forward('noRoute');
        }
        $update = $this->getLayout()->getUpdate();
        $update->addHandle('default')
                ->addHandle('cms_page');
        $this->addActionLayoutHandles();
        $this->loadLayoutUpdates();
        if($seller->getSellerhomeLayoutupdate()){
            $update->addUpdate('<reference name="content">'.$seller->getSellerhomeLayoutupdate().'</reference>');
        }
        $this->generateLayoutXml()->generateLayoutBlocks();
        $this->setHeaders();
        $this->renderLayout();
    }

    public function aboutAction() {
        if (($seller = $this->_initSeller()) === false) {
            return $this->_forward('noRoute');
        }
        $update = $this->getLayout()->getUpdate();
        $update->addHandle('default')
                ->addHandle('cms_page');
        $this->addActionLayoutHandles();
        $this->loadLayoutUpdates();
        $this->generateLayoutXml()->generateLayoutBlocks();
        $this->setHeaders();
        $this->renderLayout();
    }

    public function policyAction() {
        if (($seller = $this->_initSeller()) === false) {
            return $this->_forward('noRoute');
        }
        $update = $this->getLayout()->getUpdate();
        $update->addHandle('default')
                ->addHandle('cms_page');
        $this->addActionLayoutHandles();
        $this->loadLayoutUpdates();
        $this->generateLayoutXml()->generateLayoutBlocks();
        $this->setHeaders();
        $this->renderLayout();
    }

    public function directionAction() {
        if (($seller = $this->_initSeller()) === false) {
            return $this->_forward('noRoute');
        }
        $this->loadLayout();
        $this->setHeaders();
        $this->renderLayout();
    }

    public function reviewsAction() {
        if (($seller = $this->_initSeller()) === false) {
            return $this->_forward('noRoute');
        }
        $this->loadLayout();
        $this->setHeaders();
        $this->renderLayout();
    }

    public function getmorereviewsAction() {
        $page = $this->getRequest()->getPost('page');
        $sellerid = $this->getRequest()->getPost('sellerid');
        $obj = new Fedobe_Tribecore_Block_Review_List();
        echo $obj->setPage($page)->setSeller($sellerid)->toHtml();
    }

    protected function _getRefererUrl() {
        $refererUrl = $this->getRequest()->getServer('HTTP_REFERER');
        if ($url = $this->getRequest()->getParam(self::PARAM_NAME_REFERER_URL)) {
            $refererUrl = $url;
        }
        if ($url = $this->getRequest()->getParam(self::PARAM_NAME_BASE64_URL)) {
            $refererUrl = Mage::helper('core')->urlDecode($url);
        }
        if ($url = $this->getRequest()->getParam(self::PARAM_NAME_URL_ENCODED)) {
            $refererUrl = Mage::helper('core')->urlDecode($url);
        }

        /* if (!$this->_isUrlInternal($refererUrl)) {
          $refererUrl = Mage::app()->getStore()->getBaseUrl();
          } */
        return $refererUrl;
    }

    public function postreviewAction() {
        if (!$this->_validateFormKey()) {
            // returns to the product item page
            $this->_redirectReferer();
            return;
        }
        $data = $this->getRequest()->getPost();
        if (($seller = $this->_initSeller()) && !empty($data)) {

            $session = Mage::getSingleton('core/session');
            /* @var $session Mage_Core_Model_Session */
            $review = Mage::getModel('review/review')->setData($data);
            /* @var $review Mage_Review_Model_Review */

            $validate = $review->validate();
            if ($validate === true) {
                try {
                    $review_entityid = $review->getEntityIdByCode(self::ENTITY_SELLER_CODE);
                    $review->setEntityId($review_entityid)
                            ->setEntityPkValue($seller->getId())
                            ->setStatusId(Mage_Review_Model_Review::STATUS_PENDING)
                            ->setCustomerId(Mage::getSingleton('customer/session')->getCustomerId())
                            ->setStoreId(Mage::app()->getStore()->getId())
                            ->setStores(array(Mage::app()->getStore()->getId()))
                            ->save();

                    $resource = Mage::getSingleton('core/resource');
                    $writeadapter = $resource->getConnection('core_write');
                    $writeadapter->update(
                            $resource->getTableName('review/review_detail'), array('custom_rating' => $data['custom_rating']), 'review_id = ' . $review->getId()
                    );

                    //Here let's do reviews aggregation
                    $seller->doreviewaggregation($review_entityid);



                    $session->addSuccess($this->__('Your review has been accepted for moderation.'));
                } catch (Exception $e) {
                    $session->setFormData($data);
                    $session->addError($this->__('Unable to post the review.'));
                }
            } else {
                $session->setFormData($data);
                if (is_array($validate)) {
                    foreach ($validate as $errorMessage) {
                        $session->addError($errorMessage);
                    }
                } else {
                    $session->addError($this->__('Unable to post the review.'));
                }
            }
        }
        if ($redirectUrl = Mage::getSingleton('review/session')->getRedirectUrl(true)) {
            $this->_redirectUrl($redirectUrl);
            return;
        }
        $this->_redirectReferer();
    }

    /**
     * Apply custom layout handles to the splash page
     *
     * @param Fedobe_AttribtueSplash_Model_Page $splashPage
     * @return Fedobe_AttribtueSplash_PageController
     */
    protected function _applyCustomViewLayout(Fedobe_Tribeseller_Model_Seller $seller) {//echo "hello11";exit;
        $update = $this->getLayout()->getUpdate();
        $update->addHandle('default');
        $this->addActionLayoutHandles();
        $update->addHandle('landingpage_page_view_' . $seller->getId());
        $this->loadLayoutUpdates();

        $update->addUpdate($seller->getLayoutUpdateXml());
        $this->generateLayoutXml()->generateLayoutBlocks();
        if ($seller->getPageLayout()) {
            $this->getLayout()->helper('page/layout')->applyTemplate($seller->getPageLayout());
        } else if ($pageLayout = Mage::getStoreConfig('landingpage/page/template')) {
            $this->getLayout()->helper('page/layout')->applyTemplate($pageLayout);
        }
        $this->_isLayoutLoaded = true;
        return $this;
    }

    /**
     * Initialise the Splash Page model
     *
     * @return false|Fedobe__Landingpage_Model_Page
     */
    protected function _initSeller() {
        $current_seller = Mage::getModel('tribeseller/seller')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load((int) $this->getRequest()->getParam('id', false));
        if ($current_seller->getIsActive() == 1) {
            return false;
        }
        if(Mage::registry('current_seller')){
            return Mage::registry('current_seller');
        }else{
            Mage::register('current_seller', $current_seller);
            return $current_seller;
        }
    }

    public function getCategoryFilterHtml() {
        $layerobj = Mage::app()->getLayout()->createBlock('tribeseller/layer_view');
        $categoryfilter = $layerobj->getCatFilter();
        $cathtml = "";
        if ($categoryfilter->getItemsCount()) {
            foreach ($categoryfilter->getItems() as $_item) {
                $cathtml .= "<a href='" . (($_item->getCount() > 0) ? Mage::helper('core')->urlEscape($_item->getUrl()) : "javascript:void(0)") . "'>" . $_item->getLabel() . "</a>";
            }
        }
        return $cathtml;
    }
    
    
    //bhagyashree
    //function is added for adding store to favorite list
    public function addtofavAction(){
        $get_fav_store_id = $this->getRequest()->getPost('fav_str');
       if(Mage::getSingleton('customer/session')->isLoggedIn()) {
       $customerData = Mage::getSingleton('customer/session')->getCustomer();
       $customer_id = $customerData->getId();
        }
        $attrid = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('customer','favourite_store');//'363';//temporay hardcoded
        //echo $get_fav_store_id."here".$customer_id;
       //fetch the customer id to check if its already exist or not 
        $ret = Mage::helper('tribecustomer')->customerfavstore($customer_id,$attrid);
       // checking for inserting or updating
        if($ret){
            //update
            $ret['value'] = $ret['value'].','.$get_fav_store_id;
            $ret['attribute_id'] =$attrid;
            Mage::helper('tribecustomer')->updatefavstore($ret);
        }else{
            //insert
            $data['entity_type_id']= '1'; 
            $data['attribute_id'] =$attrid;
            $data['entity_id'] =$customer_id;
            $data['value'] =$get_fav_store_id;
            Mage::helper('tribecustomer')->addfavstore($data);
        }
        
    }
    
    //function is added for removing store to favorite list
    public function remfrmfavAction(){
        $get_fav_store_id = $this->getRequest()->getPost('fav_str');
       if(Mage::getSingleton('customer/session')->isLoggedIn()) {
       $customerData = Mage::getSingleton('customer/session')->getCustomer();
       $customer_id = $customerData->getId(); 
        }
        $attrid = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('customer','favourite_store');//'363';//temporay hardcoded
       //fetch the customer id to check if its already exist or not 
        $ret = Mage::helper('tribecustomer')->customerfavstore($customer_id,$attrid);
        //check if its only one data present than delete the record else update
        $cust_fav_seller = explode(',',$ret['value']);//print_r($cust_fav_seller);
        $cust_fav_seller = array_flip($cust_fav_seller);
    foreach($cust_fav_seller as $k=>$v){
        if($k == $get_fav_store_id){
            unset($cust_fav_seller[$k]);
        }
    }//print_r($cust_fav_seller);
    if($cust_fav_seller){
        //update
        $cust_fav_seller = array_flip($cust_fav_seller);
        $ret['value'] = implode(',',$cust_fav_seller);
        $ret['attribute_id'] =$attrid;
    Mage::helper('tribecustomer')->updatefavstore($ret);
    }else{
        //delete record
        $ret['attribute_id'] =$attrid;
    Mage::helper('tribecustomer')->deletefavstore($ret);
    }
    }
    
    //function to check fav exist or not
    public function checkfavAction(){
        $get_fav_store_id = $this->getRequest()->getPost('fav_str');
       if(Mage::getSingleton('customer/session')->isLoggedIn()) {
       $customerData = Mage::getSingleton('customer/session')->getCustomer();
       $customer_id = $customerData->getId();
        }
        $attrid = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('customer','favourite_store');//'363';//temporay hardcoded
       //fetch the customer id to check if its already exist or not 
        $ret = Mage::helper('tribecustomer')->checkcustomerfavstore($customer_id,$attrid);
    }
  
}