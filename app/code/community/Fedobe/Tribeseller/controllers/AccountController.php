<?php

require_once Mage::getModuleDir('controllers', 'Mage_Customer') . DS . 'AccountController.php';

class Fedobe_Tribeseller_AccountController extends Mage_Customer_AccountController {

    public function loginAction() {
        if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        } else {
            $this->_redirect('/');
//            $this->_redirect('tribeseller/sellerspage/login');
        }
    }

    public function createAction() {
        if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('*/*');
            return;
        } else {
            Mage::getSingleton('core/session')->setData('create', 1);
            $this->_redirect('/');
//            $this->_redirect('tribeseller/sellerspage/login');
        }
    }

    public function _loginPostRedirect() {
        $session = $this->_getSession();
        if (!$session->getBeforeAuthUrl() || $session->getBeforeAuthUrl() == Mage::getBaseUrl()) {
            // Set default URL to redirect customer to
//            $session->setBeforeAuthUrl($this->_getHelper('customer')->getAccountUrl());
            $session->setBeforeAuthUrl(Mage::helper('core/http')->getHttpReferer());
            // Redirect customer to the last page visited after logging in
            if ($session->isLoggedIn()) {
                if (!Mage::getStoreConfigFlag(
                                Mage_Customer_Helper_Data::XML_PATH_CUSTOMER_STARTUP_REDIRECT_TO_DASHBOARD
                        )) {
                    $referer = $this->getRequest()->getParam(Mage_Customer_Helper_Data::REFERER_QUERY_PARAM_NAME);
                    if ($referer) {
                        // Rebuild referer URL to handle the case when SID was changed
                        $referer = $this->_getModel('core/url')
                                ->getRebuiltUrl($this->_getHelper('core')->urlDecodeAndEscape($referer));
                        if ($this->_isUrlInternal($referer)) {
                            $session->setBeforeAuthUrl($referer);
                        }
                    }
                } else if ($session->getAfterAuthUrl()) {
                    $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
                }
                $session->setBeforeAuthUrl(Mage::helper('core/http')->getHttpReferer());
            } else {
                $session->setBeforeAuthUrl(Mage::helper('core/http')->getHttpReferer());
//                $session->setBeforeAuthUrl($this->_getHelper('customer')->getLoginUrl());
            }
        } else if ($session->getBeforeAuthUrl() == $this->_getHelper('customer')->getLogoutUrl()) {
            $session->setBeforeAuthUrl($this->_getHelper('customer')->getDashboardUrl());
        } else {
            if (!$session->getAfterAuthUrl()) {
                $session->setAfterAuthUrl($session->getBeforeAuthUrl());
            }
            if ($session->isLoggedIn()) {
                $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
            }
            $session->setBeforeAuthUrl(Mage::helper('core/http')->getHttpReferer());
        }

        $this->_redirectUrl($session->getBeforeAuthUrl(true));
    }

}