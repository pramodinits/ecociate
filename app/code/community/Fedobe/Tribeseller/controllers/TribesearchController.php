<?php

class Fedobe_Tribeseller_TribesearchController extends Mage_Core_Controller_Front_Action {

    public function searchAction() {
        $width = $height = 30;
        $term = $_REQUEST['term'];
        $storeId = Mage::app()->getStore()->getStoreId();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $data = array();
        //Here to add default search term
         $data[] = array(
          'label' => $this->formatString($term),
          'title' => $term,
          'html' => "<span class='autosearchimg'></span><span class='item-info'>" . $term . "</span>",
          'type' => "Search",
          'url' => trim(Mage::getUrl('catalogsearch/result/?q='), '/') . urlencode($term)
          );

        $tribe_exists = Mage::helper('core')->isModuleEnabled('Fedobe_Tribeseller');
        if ($tribe_exists) {
            $tribe_helper = Mage::helper('tribeseller');
            //Here to serach for seller
            $sellersql = "SELECT `entity_id` FROM `tribe_seller_entity` WHERE `seller_store_name` LIKE '%$term%' LIMIT 10";
            $sellersqlQuery = $readConnection->query($sellersql);
            while ($row = $sellersqlQuery->fetch()) {
                $seller = Mage::getModel('tribeseller/seller')->load($row['entity_id']);
                $img = $tribe_helper->resizeImg($seller->getData('seller_profile_pic'), $width, $height);
                $data[] = array(
                    'label' => $this->formatString($row['seller_store_name']),
                    'title' => $row['seller_store_name'],
                    'html' => "<span class='autosearchimg here'><img src='$img' /></span><span class='item-info'>" . $seller->getData('seller_store_name') . "</span>",
                    'type' => "Seller",
                    'url' => Mage::helper('tribeseller')->getFrontendProductUrl($seller->getData('seller_user_name'))
                );
            }
        }

       /* //Here to search for categories
        $categorysql = "SELECT DISTINCT cc.entity_id as id, cc.value as path, cc1.value as name    
                        FROM catalog_category_entity_varchar cc    
                        JOIN catalog_category_entity_varchar cc1 ON cc.entity_id=cc1.entity_id    
                        JOIN eav_entity_type ee ON cc.entity_type_id=ee.entity_type_id
                        JOIN catalog_category_entity cce ON cc.entity_id=cce.entity_id
                        WHERE cc1.value LIKE '%$term%' AND cc.attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'url_path' AND entity_type_id = (SELECT entity_type_id FROM eav_entity_type WHERE entity_type_code = 'catalog_category')) AND cc1.attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'name' AND entity_type_id = (SELECT entity_type_id FROM eav_entity_type WHERE entity_type_code = 'catalog_category')) AND ee.entity_model = 'catalog/category'";
        
        $catsqlQuery = $readConnection->query($categorysql);
        while ($row = $catsqlQuery->fetch()) {
            $category = Mage::getModel('catalog/category')->load($row['id']);
            $catimg = $this->getCategoryImage($category, $width, $height);
            $data[] = array(
                'label' => $this->formatString($row['name']),
                'title' => $row['name'],
                'html' => "<span class='autosearchimg here2'>" . ($catimg ? "<img src='$catimg' />" : '') . "</span><span class='item-info'>" . $row['name'] . "</span>",
                'type' => "Category",
                'url' => trim(Mage::getUrl($row['path']), '/')
            );
        }

        //Here to serach for products
        //bhagyashree
        $configlimit = Mage::getStoreConfig( 'tribecoresettings/general_settings/ajaxearch_limit', Mage::app()->getStore() ); 
        
        $productCollection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect('name', 'image')
                ->addAttributeToFilter('visibility', '4')
                ->joinField('qty',
                     'cataloginventory/stock_item',
                     'qty',
                     'product_id=entity_id',
                     '{{table}}.is_in_stock=1',
                     'left')
                     //->addAttributeToFilter('qty', array("gt" => 1))
                ->addAttributeToFilter(
                array(
                    array('attribute' => 'name', array('like' => '%' . $term . '%')),
                    array('attribute' => 'sku', array('like' => '%' . $term . '%')),
                )
        )->setPageSize($configlimit)->setCurPage(1);
//echo $productCollection->getSelect()->__toString();exit;

        foreach ($productCollection as $_product) {
            $products = Mage::getModel('catalog/product')->load($_product->getId());
            $image = Mage::helper('catalog/image')->init($products, 'image')->resize($width);
            $_formattedActualPrice = Mage::helper('core')->currency($products->getPrice(), true, false);
            $data[] = array(
                'label' => $this->formatString($_product->getName()),
                'title' => $_product->getName(),
                'html' => "<span class='autosearchimg'><img src='$image' /></span><span class='item-info'>" . $products->getName() . "<br/>" . $this->getDisplayPrice($products) . "</span>",
                'type' => "Product",
                'url' => $_product->getProductUrl()
            );
        }
*/
        echo json_encode($data);
    }

    public function formatString($str) {
        $maxlength = 15;
        $actlength = strlen($str);
        if ($actlength > $maxlength) {
            $str = substr($str, 0, $maxlength) . "...";
        }
        return $str;
    }

    public function getDisplayPrice($product) {
        if ($product->getFinalPrice()) {
            return $product->getFormatedPrice();
        } else if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
            $optionCol = $product->getTypeInstance(true)
                    ->getOptionsCollection($product);
            $selectionCol = $product->getTypeInstance(true)
                    ->getSelectionsCollection(
                    $product->getTypeInstance(true)->getOptionsIds($product), $product
            );
            $optionCol->appendSelections($selectionCol);
            $price = $product->getPrice();

            foreach ($optionCol as $option) {
                if ($option->required) {
                    $selections = $option->getSelections();
                    $minPrice = min(array_map(function ($s) {
                                return $s->price;
                            }, $selections));
                    if ($product->getSpecialPrice() > 0) {
                        $minPrice *= $product->getSpecialPrice() / 100;
                    }

                    $price += round($minPrice, 2);
                }
            }
            return Mage::app()->getStore()->formatPrice($price);
        } else {
            return "";
        }
    }

    public function getCategoryImage(Mage_Catalog_Model_Category $category, $width = 250, $height = 250) {
        // return when no image exists
        if (!$category->getImage()) {
            return false;
        }

        // return when the original image doesn't exist
        $imagePath = Mage::getBaseDir('media') . DS . 'catalog' . DS . 'category'
                . DS . $category->getImage();
        if (!file_exists($imagePath)) {
            return false;
        }

        // resize the image if needed
        $rszImagePath = Mage::getBaseDir('media') . DS . 'catalog' . DS . 'category'
                . DS . 'cache' . DS . $width . 'x' . $height . DS
                . $category->getImage();
        if (!file_exists($rszImagePath)) {
            $image = new Varien_Image($imagePath);
            $image->resize($width, $height);
            $image->save($rszImagePath);
        }

        // return the image URL
        return Mage::getBaseUrl('media') . '/catalog/category/cache/' . $width . 'x'
                . $height . '/' . $category->getImage();
    }

}