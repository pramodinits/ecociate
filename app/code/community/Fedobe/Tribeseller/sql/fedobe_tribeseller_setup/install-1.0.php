<?php

$installer = $this;
$installer->startSetup();
$installer->createEntityTables(
        $this->getTable('tribeseller/seller_entity')
);
$installer->run("
    DROP TABLE IF EXISTS {$this->getTable('seller_eav_attribute')};
CREATE TABLE IF NOT EXISTS {$this->getTable('seller_eav_attribute')} (
  `attribute_id` smallint(5) unsigned NOT NULL COMMENT 'Attribute ID',
  `frontend_input_renderer` varchar(255) DEFAULT NULL COMMENT 'Frontend Input Renderer',
  `is_global` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Global',
  `is_visible` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Is Visible',
  `is_searchable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Searchable',
  `is_filterable` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable',
  `is_html_allowed_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is HTML Allowed On Front',
  `is_filterable_in_search` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Filterable In Search',
  `is_visible_in_advanced_search` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible In Advanced Search',
  `position` int(11) NOT NULL DEFAULT '0' COMMENT 'Position',
  `is_wysiwyg_enabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is WYSIWYG Enabled',
  `is_used_for_promo_rules` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Used For Promo Rules',
  PRIMARY KEY (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Seller EAV Attribute Table';

ALTER TABLE {$this->getTable('seller_eav_attribute')}
  ADD CONSTRAINT `FK_SELLER_EAV_ATTRIBUTE_ATTRIBUTE_ID_EAV_ATTRIBUTE_ATTRIBUTE_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE;
  
ALTER TABLE {$this->getTable('tribe_seller_entity')} ADD  `seller_store_name` varchar (64) default NULL AFTER `entity_id`;
ALTER TABLE {$this->getTable('tribe_seller_entity')} ADD  `seller_partner` varchar (255) default NULL AFTER `seller_store_name`;
ALTER TABLE {$this->getTable('tribe_seller_entity')} ADD  `seller_first_name` varchar (64) default NULL AFTER `seller_partner`;
ALTER TABLE {$this->getTable('tribe_seller_entity')} ADD  `seller_last_name` varchar (64) default NULL AFTER `seller_first_name`;
ALTER TABLE {$this->getTable('tribe_seller_entity')} ADD  `seller_user_name` varchar (64) default NULL AFTER `seller_last_name`;
ALTER TABLE {$this->getTable('tribe_seller_entity')} ADD  `seller_email` varchar (64) default NULL AFTER `seller_user_name`;
ALTER TABLE {$this->getTable('tribe_seller_entity')} ADD  `seller_password` varchar (64) default NULL AFTER `seller_email`;
ALTER TABLE {$this->getTable('tribe_seller_entity')} ADD  `seller_direction_map` varchar (64) default NULL AFTER `seller_password`;
ALTER TABLE {$this->getTable('tribe_seller_entity')} ADD  `seller_geolat` varchar (255) default NULL AFTER `seller_direction_map`;
ALTER TABLE {$this->getTable('tribe_seller_entity')} ADD  `seller_geolng` varchar (255) default NULL AFTER `seller_geolat`;
ALTER TABLE {$this->getTable('tribe_seller_entity')} ADD  `seller_avg_rating` FLOAT(14,2) default 0 AFTER `seller_geolng`;
ALTER TABLE {$this->getTable('tribe_seller_entity')} ADD  `seller_reviews_cnt` int (11) default 0 AFTER `seller_avg_rating`;
ALTER TABLE {$this->getTable('tribe_seller_entity')} ADD  `products_data` TEXT NULL AFTER `is_active`;
ALTER TABLE {$this->getTable('tribe_seller_entity')} ADD  `category_ids` TEXT NULL AFTER `products_data`;


DROP TABLE IF EXISTS {$this->getTable('tribe_seller_entity_media_gallery')};
CREATE TABLE IF NOT EXISTS {$this->getTable('tribe_seller_entity_media_gallery')}(
  `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value ID',
  `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute ID',
  `entity_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity ID',
  `value` varchar(255) DEFAULT NULL COMMENT 'Value',
  PRIMARY KEY (`value_id`),
  KEY `IDX_TRIBE_SELLER_ENTITY_MEDIA_GALLERY_ATTRIBUTE_ID` (`attribute_id`),
  KEY `IDX_TRIBE_SELLER_ENTITY_MEDIA_GALLERY_ENTITY_ID` (`entity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tribe Seller Media Gallery Attribute Backend Table';

ALTER TABLE {$this->getTable('tribe_seller_entity_media_gallery')}
  ADD CONSTRAINT `FK_TRB_SLR_ENTT_MDA_GLR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES `eav_attribute` (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_TRB_SLR_ENTT_MDA_GLR_ENTT_ID_TRB_SLR_ENTT_ENTT_ID` FOREIGN KEY (`entity_id`) REFERENCES `tribe_seller_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE;


DROP TABLE IF EXISTS {$this->getTable('tribe_seller_entity_media_gallery_value')};
CREATE TABLE IF NOT EXISTS {$this->getTable('tribe_seller_entity_media_gallery_value')} (
  `value_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Value ID',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  `label` varchar(255) DEFAULT NULL COMMENT 'Label',
  `position` int(10) unsigned DEFAULT NULL COMMENT 'Position',
  `disabled` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Disabled',
  PRIMARY KEY (`value_id`,`store_id`),
  KEY `IDX_TRIBE_SELLER_ENTITY_MEDIA_GALLERY_VALUE_STORE_ID` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tribe Seller Media Gallery Attribute Value Table';

ALTER TABLE {$this->getTable('tribe_seller_entity_media_gallery_value')}
  ADD CONSTRAINT `FK_TRB_SLR_ENTT_MDA_GLR_VAL_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES `core_store` (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_TRB_SLR_ENTT_MDA_GLR_VAL_VAL_ID_TRB_SLR_ENTT_MDA_GLR_VAL_ID` FOREIGN KEY (`value_id`) REFERENCES `tribe_seller_entity_media_gallery` (`value_id`) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO admin_role (role_id,parent_id,tree_level,sort_order,role_type,user_id,role_name) VALUES(NULL,0,1,0,'G',0,'Tribe Sellers');
INSERT INTO admin_rule SELECT NULL, role_id, 'admin/tribeseller',NULL,0,'G','allow' FROM admin_role WHERE role_name = 'Tribe Sellers';
INSERT INTO admin_rule SELECT NULL, role_id, 'admin/tribeseller/manage_sellers',NULL,0,'G','allow' FROM admin_role WHERE role_name = 'Tribe Sellers';
INSERT INTO admin_rule SELECT NULL, role_id, 'admin/tribeseller/seller_attributes',NULL,0,'G','allow' FROM admin_role WHERE role_name = 'Tribe Sellers';
INSERT INTO admin_rule SELECT NULL, role_id, 'admin/tribeseller/seller_attributes/manage_attr',NULL,0,'G','allow' FROM admin_role WHERE role_name = 'Tribe Sellers';
INSERT INTO admin_rule SELECT NULL, role_id, 'admin/tribeseller/seller_attributes/manage_attr_set',NULL,0,'G','allow' FROM admin_role WHERE role_name = 'Tribe Sellers';
INSERT INTO admin_rule SELECT NULL, role_id, 'admin/promo/catalog',NULL,0,'G','allow' FROM admin_role WHERE role_name = 'Tribe Sellers'; 

        ");

$installer->installEntities();
$installer->removeAttributeGroup('fedobe_tribe_seller', 'Default', 'General');

//Here add another Review entity into magento
$installer->run("
INSERT INTO {$this->getTable('review_entity')}(`entity_id`,`entity_code`) values (4,'seller');

ALTER TABLE {$this->getTable('review_detail')} ADD `custom_rating` FLOAT(14,2) AFTER `customer_id`;

");


//create tribe_seller attribute
//add a supplier attribute if not exist

$tribe_seller_id = Mage::getResourceModel('eav/entity_attribute')
        ->getIdByCode('catalog_product', 'tribe_seller');

$attributeSetIds = Mage::getModel('eav/entity_attribute_set')
                ->getCollection()
                ->addFieldToSelect('attribute_set_id')->getData();
if (!$tribe_seller_id) {
    $options = array(
        'values' => array(
            1 => 'Supplier One',
            2 => 'Supplier Two',
        )
    );
    $tribe_seller_dtls = array(
        'type' => 'varchar', // Attribute type
        'input' => 'multiselect', // Input type
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL, // Attribute scope
        'required' => false, // Is this attribute required?
        'user_defined' => false,
        'searchable' => true,
        'filterable' => true,
        'comparable' => false,
        'visible_on_front' => true,
        'unique' => false,
        'used_in_product_listing' => false,
        'option' => $options,
        // Filled from above:
        'label' => 'Seller',
        'source' => 'tribeseller/seller_attribute_sellers',
        'backend' => 'eav/entity_attribute_backend_array',
    );
    $installer->addAttribute('catalog_product', 'tribe_seller', $tribe_seller_dtls);
    // Add the supplier attribute to the proper sets/groups:
    foreach ($attributeSetIds as $attributeSetId) {
        $installer->addAttributeToGroup('catalog_product', $attributeSetId['attribute_set_id'], 'General', 'tribe_seller');
    }
}
//add a supplier attribute if not exist

$supplier_id = Mage::getResourceModel('eav/entity_attribute')
        ->getIdByCode('catalog_product', 'suppliers');
if (!$supplier_id) {
    $supplier_dtls = array(
        'type' => 'int', // Attribute type
        'input' => 'select', // Input type
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL, // Attribute scope
        'required' => false, // Is this attribute required?
        'user_defined' => false,
        'searchable' => true,
        'filterable' => true,
        'comparable' => false,
        'visible_on_front' => false,
        'unique' => false,
        'used_in_product_listing' => true,
        // Filled from above:
        'label' => 'Supplier'
    );
    $installer->addAttribute('catalog_product', 'suppliers', $supplier_dtls);
    // Add the supplier attribute to the proper sets/groups:
    foreach ($attributeSetIds as $attributeSetId) {
        $installer->addAttributeToGroup('catalog_product', $attributeSetId['attribute_set_id'], 'General', 'suppliers');
    }
}


//Here add seller id ti order item table
$installer->run("
ALTER TABLE {$this->getTable('sales/order_item')} ADD `tribe_seller_id` INT(11) AFTER `order_id`;
");


$installer->endSetup();