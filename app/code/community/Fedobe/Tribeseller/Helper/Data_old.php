<?php

class Fedobe_Tribeseller_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getAttributeSourceModelByInputType($inputType) {
        $inputTypes = $this->getAttributeInputTypes();
        if (!empty($inputTypes[$inputType]['source_model'])) {
            return $inputTypes[$inputType]['source_model'];
        }
        return null;
    }

    public function getAttributeBackendModelByInputType($inputType) {
        $inputTypes = $this->getAttributeInputTypes();
        if (!empty($inputTypes[$inputType]['backend_model'])) {
            return $inputTypes[$inputType]['backend_model'];
        }
        return null;
    }

    public function getAttributeInputTypes($inputType = null) {
        /**
         * @todo specify there all relations for properties depending on input type
         */
        $inputTypes = array(
            'multiselect' => array(
                'backend_model' => 'eav/entity_attribute_backend_array',
                'source_model' => 'eav/entity_attribute_source_table',
            ),
            'boolean' => array(
                'source_model' => 'eav/entity_attribute_source_boolean'
            ),
            'image' => array(
                'backend_model' => 'tribeseller/mysql4_seller_attribute_backend_image'
            )
        );

        if (is_null($inputType)) {
            return $inputTypes;
        } else if (isset($inputTypes[$inputType])) {
            return $inputTypes[$inputType];
        }
        return array();
    }

    function getDistance($zip1, $zip2) {
        $first_lat = $this->getLnt($zip1);
        $next_lat = $this->getLnt($zip2);
        $lat1 = $first_lat['lat'];
        $lon1 = $first_lat['lng'];
        $lat2 = $next_lat['lat'];
        $lon2 = $next_lat['lng'];
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +
                cos(deg2rad($lat1)) * cos(deg2rad($lat2)) *
                cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return ($miles * 1.609344); //return in km
    }

    public function haversine_sql($table, $latcol, $lngcol, $lat, $lng, $radius, $cols) {
        //3959 Miles , 6371 KM
        $output = "SELECT $cols , 
                    ( 6371 * acos( cos( radians( $lat ) ) * cos( radians( `$latcol` ) ) * cos( radians( `$lngcol` ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( `$latcol` ) ) ) ) AS distance
                    FROM `$table` HAVING distance <= $radius 
                    ORDER BY distance;";
        return $output;
    }

    /**
     * Retrieve attribute hidden fields
     *
     * @return array
     */
    public function getAttributeHiddenFields() {
        if (Mage::registry('attribute_type_hidden_fields')) {
            return Mage::registry('attribute_type_hidden_fields');
        } else {
            return array();
        }
    }

    /**
     * Retrieve attribute disabled types
     *
     * @return array
     */
    public function getAttributeDisabledTypes() {
        if (Mage::registry('attribute_type_disabled_types')) {
            return Mage::registry('attribute_type_disabled_types');
        } else {
            return array();
        }
    }

    public function getGlobalSearchurl() {
        return Mage::getUrl('tribeseller/tribesearch/search');
    }

    public function getCurrentZipInUse() {
        $cookiezip = Mage::getModel('core/cookie')->get('tribe_pin');
        $inuse = ($cookiezip) ? $cookiezip : $this->getDefaultZip();
        return $inuse;
    }

    public function getLnt($zip) {
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=
" . urlencode($zip) . "&sensor=false";
        $result_string = file_get_contents($url); //print_r($result_string);exit;
        $result = json_decode($result_string, true);
        $result1[] = $result['results'][0];
        $result2[] = $result1[0]['geometry'];
        $result3[] = $result2[0]['location'];
        return $result3[0];
    }

    public function getDefaultZip() {
        $configzip = Mage::getStoreConfig('tribecoresettings/tribe_default_zip_settings/tribe_default_zip');
        $defaultzip = ($configzip) ? $configzip : '751001';
        return $defaultzip;
    }

    public function getCurrentLatLng() {
        $lat = Mage::getModel('core/cookie')->get('tribe_pin_lat');
        $lng = Mage::getModel('core/cookie')->get('tribe_pin_lng');
        if (!$lat || !$lng) {
            $latlng = $this->setCurrentZipInUse();
        }
        
        $lat = Mage::getModel('core/cookie')->get('tribe_pin_lat');
        $lng = Mage::getModel('core/cookie')->get('tribe_pin_lng');
        $lat = ($lat) ? $lat : $latlng['lat'];
        $lng = ($lng) ? $lng : $latlng['lng'];
        return array('lat' => $lat, 'lng' => $lng);
    }

    public function setCurrentZipInUse() {//$zip
        $defaultzip = $this->getDefaultZip();
        $cookiezip = Mage::getModel('core/cookie')->get('tribe_pin');
        $zipinuse = ($zip) ? $zip : (($cookiezip) ? $cookiezip : $defaultzip);
        $latlng = $this->getLnt($zipinuse);
        if(empty($latlng)){//as api limit exceeded.
             $latlng['lat'] = '28.632743';
        $latlng['lng'] = '77.219597';
        }
        Mage::getModel('core/cookie')->set('tribe_pin', $zipinuse, 2592000);
        Mage::getModel('core/cookie')->set('tribe_pin_lng', $latlng['lng'], 2592000);
        Mage::getModel('core/cookie')->set('tribe_pin_lat', $latlng['lat'], 2592000);
        return $latlng;
    }

    public function getEntityTypeId($type = 'sellerattribute') {
        $module = ($type) ? $type : Mage::app()->getRequest()->getControllerName();
        switch ($module) {
            case 'sellerattribute':
            case 'sellerattributeset':
            case 'sellerprofiles':
                $entitytypeid = Mage::getModel('eav/entity')->setType(Mage::getModel('eav/config')->getEntityType('fedobe_tribe_seller'))->getTypeId();
                break;
            case 'customerattributeset':
            case 'customer':
                $entitytypeid = Mage::getModel('eav/entity')->setType(Mage::getModel('eav/config')->getEntityType('customer'))->getTypeId();
                break;
            default :
                $entitytypeid = Mage::getModel('eav/entity')->setType(Mage::getModel('eav/config')->getEntityType('catalog_product'))->getTypeId();
        }
        return $entitytypeid;
    }

    public function getEntityAttributeSetId() {
        $sellerobj = Mage::registry('current_seller');
        if ($sellerobj && $sellerobj->getAttributeSetId()) {
            $setId = $sellerobj->getAttributeSetId();
        } else {
            $attrsetId = (int) Mage::app()->getRequest()->getParam('set');
            if ($attrsetId) {
                $setId = $attrsetId;
            } else {
                $setId = Mage::getModel('eav/entity_attribute_set')->getCollection()
                                ->setEntityTypeFilter($this->getEntityTypeId())->getFirstItem()->getAttributeSetId();
            }
        }
        return $setId;
    }

    public function checkSellerentity($currentattrsetid) {
        $sellertypeid = $this->getEntityTypeId();
        $attributeSet = Mage::getModel('eav/entity_attribute_set')->load($currentattrsetid);
        $current_entity_type_id = $attributeSet->getentityTypeId();
        return ($sellertypeid == $current_entity_type_id);
    }

    public function getAttributeObject() {
        $module = Mage::app()->getRequest()->getControllerName();
        switch ($module) {
            case 'sellerattribute':
                $attrobject = Mage::registry('sellerattribute_data');
                break;
            default :
                $attrobject = Mage::registry('entity_attribute');
        }
        return $attrobject;
    }

    public function checkSeller() {
        $module = Mage::app()->getRequest()->getControllerName();
        switch ($module) {
            case 'sellerattribute':
            case 'sellerattributeset':
            case 'sellerprofiles':
                $seller = TRUE;
                break;
            default :
                $seller = FALSE;
                break;
        }
        return $seller;
    }

    public function getSellerProducts() {
        $products = array();
        if (Mage::registry('current_seller')) {
            $product_ids = explode(',', Mage::registry('current_seller')->getProductsData());
            $products = Mage::getModel('catalog/product')->getCollection()
                    ->addFieldToFilter('entity_id', array('in' => $product_ids));
        }
        return $products;
    }

    public function getvalidateurlkeyaction() {
        return Mage::helper("adminhtml")->getUrl("*/sellerprofiles/checkurlkey");
    }

    public function getCustomerInfourlkeyaction() {
        return Mage::helper("adminhtml")->getUrl("*/sellerprofiles/getCustomerInfo");
    }

    public function getFrontendProductUrl($urlkey) {
        $profileurl = trim($this->getProfileUrl($urlkey), '/');
        $urlslug = trim($this->getSellerUrlKeySlug(), '/');
        $urlkey = "$profileurl/$urlslug";
        return $urlkey;
    }

    public function getProfileUrl($urlkey) {
        $urlkey = "@$urlkey";
        $websites = Mage::app()->getWebsites();
        $stores = array();
        foreach ($websites as $wk => $wv) {
            $website_id = $wv->getId();
            break;
        }
        $website_id = (Mage::app()->getRequest()->getParam('website')) ? Mage::app()->getRequest()->getParam('website') : $website_id;
        $websites = Mage::getModel("core/website")->load($website_id);
        foreach ($websites->getStoreIds() as $sk => $sv) {
            $store_id = $sv;
            break;
        }
        $storeid = (Mage::app()->getRequest()->getParam('store')) ? Mage::app()->getRequest()->getParam('store') : $store_id;
        $use_store_code_in_url = Mage::getStoreConfig('web/url/use_store', $storeid);
        if ($use_store_code_in_url) {
            $store_code = Mage::getModel('core/store')->load($storeid)->getCode();
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $store_code . '/' . $urlkey;
        } else {
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $urlkey;
        }
    }

    public function getSellerTabUrl($urlkey, $tab) {
        $profileurl = $this->getProfileUrl($urlkey);
        $suffix = $this->getSellerUrlkeysuffix();
        $urlpart = ($suffix) ? "$tab.$suffix" : $tab;
        return $profileurl . DS . $urlpart;
    }

    public function getSellerUrlKeySlug() {
        $sellerconfigurlkey = Mage::getStoreConfig('tribecoresettings/seller_urlkey_settings/tribe_seller_urlkey');
        $sellerconfigurlkey = ($sellerconfigurlkey) ? $sellerconfigurlkey : 'shop';
        $sellerconfigurlkey = trim($sellerconfigurlkey);
        return $sellerconfigurlkey;
    }

    public function getSellerUrlkeysuffix() {
        $sellerconfigurlkeysuffix = Mage::getStoreConfig('tribecoresettings/seller_urlkey_settings/tribe_seller_urlkey_suffix');
        $sellerconfigurlkeysuffix = ($sellerconfigurlkeysuffix) ? $sellerconfigurlkeysuffix : '';
        $sellerconfigurlkeysuffix = trim($sellerconfigurlkeysuffix, '.');
        return $sellerconfigurlkeysuffix;
    }

    public function getProductCategories($productids) {
        $categoryids = array();
        if (is_array($productids)) {
            $productids = implode(',', $productids);
        }
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $tableName = $resource->getTableName('catalog/category_product');
        $query = "SELECT * FROM $tableName WHERE FIND_IN_SET(`product_id`,'$productids') ";
        $res = $readConnection->query($query);
        while ($row = $res->fetch()) {
            $categoryids[$row['category_id']] = $row['category_id'];
        }
        return implode(',', $categoryids);
    }

    public function getSupplierAttributeCode() {
        $supplierattrcode = Mage::getStoreConfig('tribecoresettings/general_settings/tribe_seller_attribute_code');
        $supplierattrcode = ($supplierattrcode) ? $supplierattrcode : 'suppliers';
        return $supplierattrcode;
    }

    public function addAttributeOption($option) {
        $attr_id = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'tribe_seller');
        $toption['attribute_id'] = $attr_id;
        $toption['value']['any_option_name'][0] = $option;

        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttributeOption($toption);
        $config_attr_code = $this->getSupplierAttributeCode();
        if ($config_attr_code)
            $this->addAttributeOption($config_attr_code, $option);
    }

    function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function getPageLayout() {
        $layoutconfig = Mage::getStoreConfig('tribecoresettings/page/template');
        $layouttemplate = "1column.phtml";
        switch ($layoutconfig) {
            case 'empty':
                $layouttemplate = "empty.phtml";
                break;
            case 'one_column':
                $layouttemplate = "1column.phtml";
                break;
            case 'two_columns_left':
                $layouttemplate = "2columns-left.phtml";
                break;
            case 'two_columns_right':
                $layouttemplate = "2columns-right.phtml";
                break;
            case 'three_columns':
                $layouttemplate = "3columns.phtml";
                break;
            default:
                $layouttemplate = "1column.phtml";
                break;
        }
        return $layouttemplate;
    }

    public function getSellerTabs() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $eas = $resource->getTableName('eav_attribute_set');
        $eag = $resource->getTableName('eav_attribute_group');
        $entity_type_id = $this->getEntityTypeId('sellerattribute');
        $sql = "SELECT eas.`entity_type_id`,eas.`attribute_set_name`,eag.* FROM $eas AS eas LEFT JOIN $eag AS eag ON eas.attribute_set_id = eag.attribute_set_id WHERE eas.entity_type_id = $entity_type_id";
        $res = $readConnection->query($sql);
        $sellertabs = array();
        while ($row = $res->fetch()) {
            $sellertabs[] = array(
                'value' => $row['attribute_group_id'],
                'label' => $this->__($row['attribute_set_name'] . " ({$row['attribute_group_name']})")
            );
        }
        return $sellertabs;
    }

    public function resizeImg($fileName, $width, $height = '') {
        $folderURL = Mage::getBaseUrl('media') . 'tribe/sellers/images' . DS;
        $imageURL = $folderURL . $fileName;
        $basePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'tribe/sellers/images' . $fileName;
        $newPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . "tribe/sellers/images/resized/$width" . DS . $fileName;
        //if width empty then return original size image's URL
        if ($width != '') {
            //if image has already resized then just return URL
            if (file_exists($basePath) && is_file($basePath) && !file_exists($newPath)) {
                $imageObj = new Varien_Image($basePath);
                $imageObj->constrainOnly(TRUE);
                $imageObj->keepAspectRatio(FALSE);
                $imageObj->keepFrame(FALSE);
                $imageObj->resize($width, $height);
                $imageObj->save($newPath);
            }
            $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "tribe/sellers/images/resized/$width" . DS . $fileName;
        } else {
            $resizedURL = $imageURL;
        }
        return $resizedURL;
    }

    public function getImagePath($image_name = '') {
        return Mage::getBaseUrl('media') . 'tribe/sellers/images' . $image_name;
    }

    public function getFormattedSellerAddress($sellerinfo) {
        $address = "";
        $streetaddr = trim($sellerinfo['seller_street_address_one']);
        $streetcity = trim($sellerinfo['seller_city']);
        $streetstate = trim($sellerinfo['seller_state']);
        if ($streetaddr) {
            $address .= $streetaddr . ",";
        }
        if ($streetcity) {
            $address .= $streetcity . ",";
        }
        if ($streetstate) {
            $address .= $streetstate;
        }
//        if($sellerinfo['seller_pin']){
//            $address .= ",".$sellerinfo['seller_pin'];
//        }
        return $address;
    }

    public function calculateDistanceBetweenTwoLatLng($source, $dest) {
        $info = $this->getGoogleDistance($source, $dest); //print_r($info);exit;
        if (!$info) {
            $info = $this->customDistanceCalculation($source[0], $source[1], $dest[0], $dest[1]);
        }
        return $info;
    }

    public function customDistanceCalculation($lat1, $lon1, $lat2, $lon2) {
        $pi80 = M_PI / 180;
        $lat1 *= $pi80;
        $lon1 *= $pi80;
        $lat2 *= $pi80;
        $lon2 *= $pi80;
        $r = 6371;
        $dlat = $lat2 - $lat1;
        $dlon = $lon2 - $lon1;
        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        $km = $r * $c;
        $speed = 50;
        $time = gmdate("i:s", round($km / $speed));
        return array('text' => round($km, 2), 'duration' => $time . "mins");
    }

    public function getGoogleDistance($source, $dest) {//print_r($source);print_r($dest);
        $sourcepoint = implode(',', $source);
        $destpoint = implode(',', $dest);
        $apikey = $this->getGoogleApiKey();
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=$sourcepoint&destinations=$destpoint&key=$apikey";
        $data = json_decode(file_get_contents($url));
        $info = $data->rows[0]->elements[0]; //echo "<pre>";print_r($info);echo "</pre>";
        $status = strtolower($info->status);
        if ($status == 'ok') {
            $distance = floatval($info->distance->text) * 1.60934;
            return array('text' => round($distance, 1), 'duration' => $info->duration->text);
        } else {
            return false;
        }
    }

    public function getGoogleApiKey() {
        $apikey = Mage::getStoreConfig('tribecoresettings/tribe_default_zip_settings/tribe_google_apikey');
        $apikey = ($apikey) ? $apikey : "AIzaSyD4FMox5PeJKHNsuPllDJzqso4QhHKqRXg";
        return $apikey;
    }

    public function getrSellerFromUrl($url) {
        $usrinfo = explode("@", $url);
        if (isset($usrinfo[1])) {
            $usrinfarr = explode("/", $usrinfo[1]);
            $username = $usrinfarr[0];
            $seller = Mage::getModel('tribeseller/seller')
                    ->setStoreId(Mage::app()->getStore()->getId())
                    ->loadByAttribute('seller_user_name', $username);
            if ($seller->getId() || $seller->getIsActive() != 2) {
                return $seller;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getDefaultSellerOptionId() {
        $sellerattr_code = $this->getSupplierAttributeCode();
        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $sellerattr_code);
        return $attribute->getDefaultValue();
    }

    public function getDefaultSeller() {
        $default_seller_id = Mage::getStoreConfig('tribecoresettings/general_settings/tribe_seller_default_id');
        if ($default_seller_id) {
            return $default_seller_id;
        } else {
            return false;
        }
    }

    public function getSellerFromOptionId($option_id) {
        $seller = Mage::getModel('tribeseller/seller')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->loadByAttribute('seller_partner', $option_id);
        if (!$seller || !$seller->getId() || $seller->getIsActive() != 2) {
            return false;
        }
        return $seller;
    }

    public function getSellerInfo($id) {
        if ($id) {
            $seller = Mage::getModel('tribeseller/seller')->setStoreId(Mage::app()->getStore()->getId())->load($id);
            if ($seller && $seller->getId()) {
                return $seller;
            }
        } else {
            return false;
        }
    }

    public function checkProductAvailWithSeller($seller, $prd_id = 0,$type=NULL) {//type=NULL abs
     if(!is_null($type) && $type=='configurable'){//abs
       $product_id = $prd_id ? $prd_id : 0;
     }else{
        $product_id = (Mage::registry('current_product')) ? Mage::registry('current_product')->getId() : ($prd_id ? $prd_id : 0);
     }
        $prdarr = $this->getSellersChannelProducts($seller, false);
        $res = false;
        if ($prdarr) {
            if (in_array($product_id, $prdarr)) {
                $res = true;
            } else {
                $res = false;
            }
        }
        return $res;
    }

    public function getCheckValidOptionId($option_id) {
        $allOptions = Mage::getModel('tribeseller/seller_attribute_sellers')->getAllOptions();
        foreach ($allOptions as $k => $v) {
            if ($v['value'] == $option_id) {
                return $option_id;
                break;
            }
        }
        return false;
    }

    public function getCurrentProductSeller($_product, $seller_id = '', $assfound = FALSE,$type= NULL) {//type=NULL abs
        if ($seller_id) {
            $seller = $this->getSellerInfo($seller_id);
            if ($seller) {
                $assfound = $this->checkProductAvailWithSeller($seller, $_product['id'],$type);//type=NULL abs
            }
        }
        if (!$assfound) {
             if($seller_id){//when seller id passed in url and product is oos. abs
                   return array($assfound, $seller);
                }
            $seller = $this->getrSellerFromUrl($url);
            $default_seller_id = $this->getDefaultSeller();
            $default_seller_opt_id = $this->getDefaultSellerOptionId();
            $sellerattr_code = $this->getSupplierAttributeCode();
            $valid_option_id = $this->getCheckValidOptionId($_product[$sellerattr_code]);
            if ($seller) {
                $assfound = $this->checkProductAvailWithSeller($seller, $_product['id'],$type);//type=NULL abs
                if (!$assfound) {
                    $seller = ($valid_option_id) ? $this->getSellerFromOptionId($valid_option_id) : $this->getSellerInfo($default_seller_id);
                }
            } else {
                $seller = ($valid_option_id) ? $this->getSellerFromOptionId($valid_option_id) : $this->getSellerInfo($default_seller_id);
            }
        }
        return array($assfound, $seller);
    }

    public function isSellerLoggedin() {
        $admin_user_session = Mage::getSingleton('admin/session');
        if ($admin_user_session->getUser()) {
            $adminuserId = $admin_user_session->getUser()->getUserId();
            $role_data = Mage::getModel('admin/user')->load($adminuserId)->getRole()->getData();
            if (!empty($role_data) && $role_data['role_name'] == 'Tribe Sellers')
                return TRUE;
            else
                return FALSE;
        }else {
            return FALSE;
        }
    }

    public function getSellerIdFromadminUser() {
        $user = Mage::getSingleton('admin/session');
        $username = $user->getUser()->getUsername();
        $userEmail = $user->getUser()->getEmail();
        $seller = Mage::getModel('tribeseller/seller')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->loadByAttribute('seller_user_name', $username);
        if (!$seller || !$seller->getId() || $seller->getIsActive() != 2) {
            return false;
        }
        return $seller->getId();
    }

    public function getChannelIdForSeller($sellerId) {
        $sellerCollection = Mage::getModel('tribeseller/seller')->getCollection();
        $partner = $sellerCollection
                ->addAttributeToFilter('entity_id', $sellerId)
                ->addAttributeToSelect('seller_partner')
                ->getFirstItem()
                ->getSellerPartner();
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $query = "SELECT id FROM bridge_incomingchannels WHERE channel_supplier=$partner ORDER BY id LIMIT 0,1;";
        $result = $read->fetchRow($query);
        return $result ? $result['id'] : false;
    }

    public function getSellerBaseGrandTotal($order_id, $seller_id) {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $table = $resource->getTableName('sales/order_item');
        $query = "SELECT SUM(`base_row_total`) AS `total`  FROM $table WHERE `tribe_seller_id`= $seller_id AND `order_id` =$order_id  ";
        $sqlattrQuery = $read->query($query);
        $total = 0;
        while ($row = $sqlattrQuery->fetch()) {
            $total = $row['total'];
        }
        return $total;
    }

    public function getSellerGrandTotal($order_id, $seller_id) {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $table = $resource->getTableName('sales/order_item');
        $query = "SELECT SUM(`row_total`) AS `total`  FROM $table WHERE `tribe_seller_id`= $seller_id AND `order_id` =$order_id  ";
        $sqlattrQuery = $read->query($query);
        $total = 0;
        while ($row = $sqlattrQuery->fetch()) {
            $total = $row['total'];
        }
        return $total;
    }

    public function getSellerOrderIds() {
        $tribesellerid = $this->getSellerIdFromadminUser();
        $ids = array();
        if ($tribesellerid) {
            $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
            $table = $resource->getTableName('sales/order_item');
            $query = "SELECT DISTINCT `order_id` FROM $table WHERE `tribe_seller_id`= $tribesellerid";
            $sqlattrQuery = $read->query($query);
            while ($row = $sqlattrQuery->fetch()) {
                $ids[$row['order_id']] = $row['order_id'];
            }
        }
        return $ids;
    }

    public function getSellerCustomerids() {
        $orderids = $this->getSellerOrderIds();
        $customerids = array();
        if (!empty($orderids)) {
            $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
            $table = $resource->getTableName('sales/order');
            $orderidsstr = implode(',', $orderids);
            $query = "SELECT `customer_id` FROM $table WHERE entity_id IN($orderidsstr) AND `customer_id` IS NOT NULL ";
            $sqlattrQuery = $read->query($query);
            while ($row = $sqlattrQuery->fetch()) {
                $customerids[$row['customer_id']] = $row['customer_id'];
            }
        }
        return $customerids;
    }

    public function getSellerItemIds($orderId) {
        $tribesellerid = $this->getSellerIdFromadminUser();
        $ids = array();
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $table = $resource->getTableName('sales/order_item');
        if ($tribesellerid && $orderId) {
            $query = "SELECT `item_id` FROM $table WHERE `tribe_seller_id`= $tribesellerid AND `order_id` = $orderId";
        } else {
            $query = "SELECT `item_id` FROM $table WHERE `tribe_seller_id`= $tribesellerid ";
        }
        $sqlattrQuery = $read->query($query);
        while ($row = $sqlattrQuery->fetch()) {
            $ids[$row['item_id']] = $row['item_id'];
        }
        return $ids;
    }

    public function getSellerItemIdsshiped($orderId) {
        $tribesellerid = $this->getSellerIdFromadminUser();
        $ids = array();
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $table = $resource->getTableName('sales/order_item');
        if ($tribesellerid && $orderId) {
            $query = "SELECT `item_id`,`order_id` FROM $table WHERE `tribe_seller_id`= $tribesellerid AND `order_id` = $orderId AND `qty_shipped` > 0 ";
        } else {
            $query = "SELECT `item_id`,`order_id` FROM $table WHERE `tribe_seller_id`= $tribesellerid AND `qty_shipped` > 0 ";
        }
        $sqlattrQuery = $read->query($query);
        while ($row = $sqlattrQuery->fetch()) {
            $ids[$row['item_id']] = $row['item_id'];
        }
        return $ids;
    }

    public function getOtherItemIds($orderId) {
        $tribesellerid = $this->getSellerIdFromadminUser();
        $ids = array();
        if ($tribesellerid && $orderId) {
            $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
            $table = $resource->getTableName('sales/order_item');
            $query = "SELECT `item_id` FROM $table WHERE `tribe_seller_id` != $tribesellerid AND `order_id` = $orderId";
            $sqlattrQuery = $read->query($query);
            while ($row = $sqlattrQuery->fetch()) {
                $ids[$row['item_id']] = $row['item_id'];
            }
        }
        return $ids;
    }

    public function getSellerItems($orderId) {
        $tribesellerid = $this->getSellerIdFromadminUser();
        $items = array();
        if ($tribesellerid && $orderId) {
            $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
            $table = $resource->getTableName('sales/order_item');
            $query = "SELECT * FROM $table WHERE `tribe_seller_id`= $tribesellerid AND `order_id` = $orderId";
            $sqlattrQuery = $read->query($query);
            while ($row = $sqlattrQuery->fetch()) {
                $items[$row['item_id']] = $row;
            }
        }
        return $items;
    }

    public function getInvoiceEntityIdsFromOrderItemsIds($itemids) {
        $items = array();
        if ($itemids) {
            $ids = implode(",", $itemids);
            $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
            $table = $resource->getTableName('sales/invoice_item');
            $query = "SELECT * FROM $table WHERE FIND_IN_SET(`order_item_id`,'$ids') ";
            $sqlattrQuery = $read->query($query);
            while ($row = $sqlattrQuery->fetch()) {
                $items[$row['entity_id']] = ($row['parent_id']) ? $row['parent_id'] : $row['entity_id'];
            }
        }
        return $items;
    }

    public function getShipmenteEntityIdsFromOrderItemsIds($itemids) {
        $items = array();
        if ($itemids) {
            $ids = implode(",", $itemids);
            $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
            $table = $resource->getTableName('sales/shipment_item');
            $query = "SELECT * FROM $table WHERE FIND_IN_SET(`order_item_id`,'$ids') ";
            $sqlattrQuery = $read->query($query);
            while ($row = $sqlattrQuery->fetch()) {
                $items[$row['parent_id']] = $row['parent_id'];
            }
        }
        return $items;
    }

    public function itemIgnoreCol() {
        return array(
            'item_id',
            'order_id',
            'tribe_seller_id',
            'quote_item_id',
            'store_id',
            'product_id',
            'weight',
            'is_virtual',
            'sku',
        );
    }

    public function getSellersChannelProducts($seller, $flag = TRUE) {
        $productids = array();
        $pricetoupdate = array();
        $prdinfo = array();
        $seller = ($seller) ? $seller : Mage::registry('current_seller');
        $partner_id = $seller->getSellerPartner();
        if (Mage::helper('core')->isModuleEnabled('Fedobe_Bridge')) {
            $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
            $write = $resource->getConnection('core_write');
            $channel_table = $resource->getTableName('bridge/incomingchannels');
            $query = "SELECT `id` FROM $channel_table WHERE `status`=1 AND `channel_supplier` = $partner_id ";
            $sqlattrQuery = $read->query($query);
            $channels = array();
            while ($row = $sqlattrQuery->fetch()) {
                $channels[$row['id']] = $row['id'];
            }
            if (!empty($channels)) {
                $table = $resource->getTableName('bridge/incomingchannels_product');
                $channel_ids = implode(',', $channels);
                $query = "SELECT `id`,`product_entity_id`,`converted_price`,`sku`,`channel_id`,`channel_supplier` FROM $table as bridgeinfo JOIN `cataloginventory_stock_item` as stck ON bridgeinfo.product_entity_id=stck.product_id WHERE stck.is_in_stock=1 AND FIND_IN_SET(channel_id,'$channel_ids') AND `channel_supplier`=$partner_id  AND `quantity` > 0 AND `channel_type` = 'incoming' ORDER BY `slab`,`order` ";
                $sqlattrQuery = $read->query($query);
                while ($row = $sqlattrQuery->fetch()) {
                    if ($row['converted_price']) {
                        if (!isset($productids[$row['product_entity_id']])) {
                            $productids[$row['product_entity_id']] = $row['product_entity_id'];
                            $prdinfo[$row['product_entity_id']] = $row;
                        }
                    } else {
                        $pricetoupdate[$row['id']] = $row['id'];
                    }
                }
                if (!empty($pricetoupdate) && $flag) {
                    //Here let flag those rows to calculate the price
                    $ids = implode(',', $pricetoupdate);
                    $updatesql = "UPDATE $table SET priority = 2 WHERE FIND_IN_SET(`id`,'$ids')";
                    $write->query($updatesql);
                }
                if (!empty($productids)) {
                    $productids = array_keys($productids);
                    $parent_productids_query = 'SELECT spl.parent_id,cn.sku FROM `catalog_product_super_link` as spl JOIN `catalog_product_entity` as cn ON spl.product_id=cn.entity_id  WHERE product_id IN ('.implode(',',$productids).')' ;
                    $parent_productids_arr = $read->fetchAll($parent_productids_query);
                    $parent_productids_ids = array_column($parent_productids_arr,'parent_id');
                    //$parent_productids_ids = array(188);
                   /* foreach($parent_productids_arr as $k=>$v){
                        $prdinfo[$v['product_id']]['product_entity_id'] =$v['product_id'];
                        $prdinfo[$v['product_id']]['sku'] = $v['sku'];
                         $prdinfo[$v['product_id']]['channel_id'] = 11;
                        $prdinfo[$v['product_id']]['channel_supplier'] = $partner_id;
                    }*/
                    //print_r($productids);exit;
                    $productids = array_merge($productids,$parent_productids_ids);
                    $productids=array_unique($productids);
                    Mage::getSingleton('core/session')->setSellerProductInfo($prdinfo);
                }
            }
        }
       // if($_SERVER['REMOTE_ADDR']=='45.251.36.164'){echo "<pre>";print_r($productids);exit;}
        return $productids;
    }

    public function getpindistance($lat, $lng, $radius) {
        $latcol = 'seller_geolat';
        $lngcol = 'seller_geolng';
        $cols = "entity_id";
        $ret = 0;
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $table = $resource->getTableName('tribeseller/seller_entity');
        $sql = $this->haversine_sql($table, $latcol, $lngcol, $lat, $lng, $radius, $cols);
        $sqlattrQuery = $readConnection->query($sql); //echo "<pre>";print_r($sqlattrQuery);exit;
        if (!empty($sqlattrQuery)) {
            $ret = count($sqlattrQuery);
        }
        return $ret;
    }

    public function getDefaultRad() {
        $cronrad = Mage::getStoreConfig('tribecoresettings/general_settings/tribe_seller_radius');
        $defaultrad = ($cronrad) ? $cronrad : 10;
        return $defaultrad;
    }

    public function setCurrentRadiusInUse($radius) {
        $defaultrad = $this->getDefaultRad();
        $cookierad = Mage::getModel('core/cookie')->get('tribe_radius');
        $radiusinuse = ($radius) ? $radius : (($cookierad) ? $cookierad : $defaultrad);
        Mage::getModel('core/cookie')->set('tribe_radius', $radiusinuse, 2592000);
        return $radiusinuse;
    }

    public function getCurrentradius() {
        $rad = Mage::getModel('core/cookie')->get('tribe_radius');
        $def_rad = $this->getDefaultRad();
        $radius = ($rad) ? $rad : $def_rad;
        return $radius;
    }

    public function getcurrentsellerstock() {
        //$seller_id = Mage::getResourceModel('tribeseller/seller')->getIdBySellerUsername('motherorganic');
        $url = Mage::helper('core/url')->getCurrentUrl();
        $seller = $this->getrSellerFromUrl($url);
        $seller_data = $seller->getData();
        $partner_id = $seller->getSellerPartner();
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $write = $resource->getConnection('core_write');
        $channel_table = $resource->getTableName('bridge/incomingchannels');
        $query = "SELECT `id` FROM $channel_table WHERE `status`=1 AND `channel_supplier` = $partner_id ";
        $sqlattrQuery = $read->query($query);
        $productids = $channels = array();
        while ($row = $sqlattrQuery->fetch()) {
            $channels[$row['id']] = $row['id'];
        }
        $table = $resource->getTableName('bridge/incomingchannels_product');
        $channel_ids = implode(',', $channels);
        if(!empty($channels)){
        $query = "SELECT `id`,`product_entity_id`,`converted_price`,`sku`,`channel_id`,`quantity`,`channel_supplier` FROM $table WHERE FIND_IN_SET(channel_id,'$channel_ids') AND `channel_supplier`=$partner_id  AND `channel_type` = 'incoming' ORDER BY `slab`,`order` ";
        $sqlattrQuery = $read->query($query);
        while ($row = $sqlattrQuery->fetch()) {
            if (!isset($productids[$row['product_entity_id']])) {
                $productids[$row['product_entity_id']] = $row['quantity'];
            }
         }
        }
        return $productids;
    }
     private function getencprefix() {
        $prefix = "Review for profiles start";
        return base64_encode($prefix);
    }

    private function getencsuffix() {
        $sufix = "Review for profiles end";
        return base64_encode($sufix);
    }

    public function decrypt($data) {
        $encpre = $this->getencprefix();
        $encsuffix = $this->getencsuffix();
        $data = str_replace($encsuffix, '', str_replace($encpre, '', $data));
        return Mage::helper('core')->jsonDecode(base64_decode($data));
    }

    public function encrypt($data) {
        $enc_data = base64_encode(Mage::helper('core')->jsonEncode($data));
        $encpre = $this->getencprefix();
        $encsuffix = $this->getencsuffix();
        return $encpre . $enc_data . $encsuffix;
    }
     public function isActionallowed($info) {
        $allow = false;
        if ($info['user_log_id'] || (Mage::app()->isInstalled() && Mage::getSingleton('customer/session')->isLoggedIn())) {
            if ($info['trcr_account_type'] == 2) {
                $id = $info['user_log_id']  ? $info['user_log_id'] : Mage::getSingleton('customer/session')->getCustomer()->getId();
                if ($info['trcr_customer_list'] == $id) {
                    $allow = true;
                }
            } else {
                $email = $info['logged_in_email']  ? $info['logged_in_email'] : Mage::getSingleton('customer/session')->getCustomer()->getEmail();
                if (trim($info['trcr_email']) == $email) {
                    $allow = true;
                }
            }
        } 
        return $allow;
    }
     public function reviewadded($profile_data){
         $summaryData=array();
         $count=0;
         $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
         $customer = $this->getLoggedincustomerdata(); //print_r($customer);exit;
         $customer_id = $customer['entity_id'];
         $seller = Mage::registry('current_seller');
         $seller_id = $seller['entity_id'];
      $qury = "SELECT count(*) FROM `review` as rvw 
      JOIN `review_detail` rvw_dtl 
      ON rvw.`review_id` = rvw_dtl.`review_id` 
      WHERE rvw_dtl.customer_id=$customer_id
      AND rvw.entity_id=4 
      AND rvw.`entity_pk_value`=$seller_id";
      $summaryData=$readConnection->fetchAll($qury);
      if(!empty($summaryData)){
         $count= $summaryData[0]['count(*)'];
      }
      return $count;
   }
    public function getLoggedincustomerdata(){
       return Mage::getSingleton('customer/session')->getCustomer()->getData();
   }
   public function checkemailidlinked($email) {
        $customers = array();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $tableName = $resource->getTableName('customer_entity');
        $profiles = array();
        $id = Mage::app()->getRequest()->getParam('id');
        $query = "SELECT `entity_id`,`email` FROM $tableName WHERE email='".$email."'";
        $customerdata = $readConnection->fetchAll($query);
        
        $adminuserid = Mage::getResourceModel('tribeseller/seller')->getAdminUserId($email, $email);
        //$adminusercheck = $this->checkAdminUser($email, $email, $adminuserid); return $adminusercheck;exit;
        if ($adminuserid || !empty($customerdata)) {
           $ret = 0;
        } else {
            $ret = 1;
        }
        return $ret;
    }

}
