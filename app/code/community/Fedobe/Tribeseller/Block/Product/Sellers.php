<?php

class Fedobe_Tribeseller_Block_Product_Sellers extends Mage_Core_Block_Template {

    protected $_sellerCollection;
    private $_productid = null;

    public function __construct() {
        parent::__construct();
        $this->setTemplate('fedobe/tribe/product/sellers.phtml');
    }

    public function getProductId() {
        if (!$this->_productid) {
            $this->_productid = Mage::registry('current_product')->getId();
        }
        return $this->_productid;
    }

    public function setProductId($productid) {
        $this->_productid = $productid;
        return $this;
    }

    public function getProduct() {
        return Mage::getModel('catalog/product')->load($this->_productid);
    }

    public function getProductSellers($flag = false) {
        $sellers = array();
        $product_id = $this->getProductId();
        if ($product_id) {
            if (Mage::helper('core')->isModuleEnabled('Fedobe_Bridge')) {
                $resource = Mage::getSingleton('core/resource');
                $read = $resource->getConnection('core_read');
                $write = $resource->getConnection('core_write');
                $channel_table = $resource->getTableName('bridge/incomingchannels');
                $query = "SELECT `id` FROM $channel_table WHERE `status`=1 ";
                $sqlattrQuery = $read->query($query);
                $channels = array();
                while ($row = $sqlattrQuery->fetch()) {
                    $channels[$row['id']] = $row['id'];
                }
                if (!empty($channels)) {
                    $sellerid = Mage::app()->getRequest()->getParam('s');
                    $assfound = false;
                    $_product = $this->getProduct();
                    $sellerinf = Mage::helper('tribeseller')->getCurrentProductSeller($_product, $sellerid);
                    $seller = $sellerinf[1];
                    $assfound = $sellerinf[0];
                    $active_seller_id = $seller->getId();
                    $seller = Mage::getModel('tribeseller/seller')->load($active_seller_id);
                    $partner_id = $seller->getSellerPartner();
                    $table = $resource->getTableName('bridge/incomingchannels_product');
                    $channel_ids = implode(',', $channels);
                    $query = "SELECT `id`,`product_entity_id`,`converted_price`,`sku`,`channel_id`,`channel_supplier` FROM $table WHERE FIND_IN_SET(channel_id,'$channel_ids') AND `product_entity_id`=$product_id AND `channel_supplier`<>$partner_id AND `quantity` > 0 AND `channel_type` = 'incoming' ORDER BY `slab`,`order` ";
                    $sqlattrQuery = $read->query($query);
                    while ($row = $sqlattrQuery->fetch()) {
                        if ($row['converted_price']) {
                            if (!isset($productids[$row['channel_supplier']])) {
                                $sellers[$row['channel_supplier']] = $row;
                            }
                        } else {
                            $pricetoupdate[$row['id']] = $row['id'];
                        }
                    }
                    if (!empty($pricetoupdate) && $flag) {
                        //Here let flag those rows to calculate the price
                        $ids = implode(',', $pricetoupdate);
                        $updatesql = "UPDATE $table SET priority = 2 WHERE FIND_IN_SET(`id`,'$ids')";
                        $write->query($updatesql);
                    }
                }
            }
        }
        return $sellers;
    }

}