<?php

class Fedobe_Tribeseller_Block_Product_List extends Mage_Catalog_Block_Product_List {

    public function getAddToCartUrl($product, $additional = array()) {
        if (Mage::registry('current_seller')) {
            $additional['product_seller'] = Mage::registry('current_seller')->getId();
        } else {
            $supplierscode = (Mage::getStoreConfig("bridge/bridge_supplier_settings/bridge_supplier_attrcode")) ? Mage::getStoreConfig("bridge/bridge_supplier_settings/bridge_supplier_attrcode") : "suppliers";
            $default_seller_id = $product->getData($supplierscode) ? Mage::helper('tribeseller')->getSellerFromOptionId($product->getData($supplierscode))->getId() : Mage::helper('tribeseller')->getDefaultSeller();
            if ($default_seller_id)
                $additional['product_seller'] = $default_seller_id;
        }
        return parent::getAddToCartUrl($product, $additional);
    }

}