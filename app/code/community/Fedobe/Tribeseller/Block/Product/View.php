<?php
class Fedobe_Tribeseller_Block_Product_View extends Mage_Catalog_Block_Product_View {

    public function getSubmitUrl($product, $additional = array(),$simple_product_id=FALSE) {
        if($simple_product_id){//abs
           $simple_product= Mage::getModel('catalog/product')->load($simple_product_id);
        }
        if(Mage::app()->getRequest()->getParam('s')){
            $additional['product_seller'] = Mage::app()->getRequest()->getParam('s');
        }elseif (Mage::registry('current_seller')) {
            $additional['product_seller'] = Mage::registry('current_seller')->getId();
        } else {
            $supplierscode = (Mage::getStoreConfig("bridge/bridge_supplier_settings/bridge_supplier_attrcode")) ? Mage::getStoreConfig("bridge/bridge_supplier_settings/bridge_supplier_attrcode") : "suppliers";
             if($simple_product_id){ //for configurable product abs
                $default_seller_id = $simple_product->getData($supplierscode) ? Mage::helper('tribeseller')->getSellerFromOptionId($simple_product->getData($supplierscode))->getId() : Mage::helper('tribeseller')->getDefaultSeller();
             }else{ //for simple product
                 $default_seller_id = $product->getData($supplierscode) ? Mage::helper('tribeseller')->getSellerFromOptionId($product->getData($supplierscode))->getId() : Mage::helper('tribeseller')->getDefaultSeller();
             }
            if ($default_seller_id)
                $additional['product_seller'] = $default_seller_id;
        }
        $submitRouteData = $this->getData('submit_route_data');
        if ($submitRouteData) {
            $route = $submitRouteData['route'];
            $params = isset($submitRouteData['params']) ? $submitRouteData['params'] : array();
            $submitUrl = $this->getUrl($route, array_merge($params, $additional));
        } else {
            $submitUrl = $this->getAddToCartUrl($product, $additional);
        }
        return $submitUrl;
    }

}