<?php

/**
 * Product attribute add/edit form options tab
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribeseller
 * @author      Fedobe Magento Team
 */
class Fedobe_Tribeseller_Block_Adminhtml_Tribeseller_Edit_Tab_Options extends Mage_Adminhtml_Block_Catalog_Product_Attribute_Edit_Tab_Options {

    /**
     * Retrieve attribute object from registry
     *
     * @return Mage_Eav_Model_Entity_Attribute_Abstract
     */
    public function getAttributeObject() {
        return Mage::helper('tribeseller')->getAttributeObject();
    }

}