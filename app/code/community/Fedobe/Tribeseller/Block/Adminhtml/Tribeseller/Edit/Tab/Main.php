<?php

/**
 * Customer attribute add/edit form main tab
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribeseller
 * @author      Fedobe Magento Team
 */
class Fedobe_Tribeseller_Block_Adminhtml_Tribeseller_Edit_Tab_Main extends Mage_Adminhtml_Block_Catalog_Product_Attribute_Edit_Tab_Main {

    public function getAttributeObject() {
        return Mage::helper('tribeseller')->getAttributeObject();
    }

    protected function _prepareForm() {
        parent::_prepareForm();
        $form = $this->getForm();
        $is_seller = Mage::helper('tribeseller')->checkSeller();
        if($is_seller){
            $fieldset = $form->getElement('base_fieldset');
            $fieldset->removeField('apply_to');  
            $frontendproperties = $form->getElement('front_fieldset');
            $frontendproperties->removeField('is_comparable');  
            $frontendproperties->removeField('is_visible_on_front');  
            $frontendproperties->removeField('used_in_product_listing');  
            $frontendproperties->removeField('used_for_sort_by');  
        }
        return $this;
    }

}
