<?php

/**
 * Manage Customer Attribute grid block
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribeseller
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Tribeseller_Block_Adminhtml_Tribeseller_Grid extends Mage_Eav_Block_Adminhtml_Attribute_Grid_Abstract {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Prepare customer attributes grid collection object
     *
     * @return Fedobe_Tribeseller_Block_Adminhtml_Tribeseller_Grid
     */
    protected function _prepareCollection() {
        $collection = Mage::getResourceModel('tribeseller/attribute_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumnAfter('is_visible', array(
            'header'=>Mage::helper('tribeseller')->__('Visible'),
            'sortable'=>true,
            'index'=>'is_visible_on_front',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('tribeseller')->__('Yes'),
                '0' => Mage::helper('tribeseller')->__('No'),
            ),
            'align' => 'center',
        ), 'frontend_label');

        $this->addColumnAfter('is_global', array(
            'header'=>Mage::helper('tribeseller')->__('Scope'),
            'sortable'=>true,
            'index'=>'is_global',
            'type' => 'options',
            'options' => array(
                Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE =>Mage::helper('tribeseller')->__('Store View'),
                Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE =>Mage::helper('tribeseller')->__('Website'),
                Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL =>Mage::helper('tribeseller')->__('Global'),
            ),
            'align' => 'center',
        ), 'is_visible');

        $this->addColumn('is_searchable', array(
            'header'=>Mage::helper('tribeseller')->__('Searchable'),
            'sortable'=>true,
            'index'=>'is_searchable',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('tribeseller')->__('Yes'),
                '0' => Mage::helper('tribeseller')->__('No'),
            ),
            'align' => 'center',
        ), 'is_user_defined');

        $this->addColumnAfter('is_filterable', array(
            'header'=>Mage::helper('tribeseller')->__('Use in Layered Navigation'),
            'sortable'=>true,
            'index'=>'is_filterable',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('tribeseller')->__('Filterable (with results)'),
                '2' => Mage::helper('tribeseller')->__('Filterable (no results)'),
                '0' => Mage::helper('tribeseller')->__('No'),
            ),
            'align' => 'center',
        ), 'is_searchable');
        return $this;
    }
}