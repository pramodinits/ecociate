<?php

class Fedobe_Tribeseller_Block_Adminhtml_Tribeseller_Attribute_Set_Main extends Mage_Adminhtml_Block_Catalog_Product_Attribute_Set_Main {

    /**
     * Retrieve Attribute Set Save URL
     *
     * @return string
     */
    public function getMoveUrl() {
        return $this->getUrl('*/sellerattributeset/save', array('id' => $this->_getSetId()));
    }

    /**
     * Retrieve Unused in Attribute Set Attribute Tree as JSON
     *
     * @return string
     */
    public function getAttributeTreeJson() {
        $is_seller = Mage::helper('tribeseller')->checkSellerentity($this->_getSetId());
        if ($is_seller) {
            $items = array();
            $attributes = Mage::getResourceModel('tribeseller/attribute_collection');
            foreach ($attributes as $child) {
                $attr = array(
                    'text' => $child->getAttributeCode(),
                    'id' => $child->getAttributeId(),
                    'cls' => 'leaf',
                    'allowDrop' => false,
                    'allowDrag' => true,
                    'leaf' => true,
                    'is_user_defined' => $child->getIsUserDefined(),
                    'is_configurable' => false,
                    'entity_id' => $child->getEntityId()
                );

                $items[] = $attr;
            }

            if (count($items) == 0) {
                $items[] = array(
                    'text' => Mage::helper('catalog')->__('Empty'),
                    'id' => 'empty',
                    'cls' => 'folder',
                    'allowDrop' => false,
                    'allowDrag' => false,
                );
            }

            return Mage::helper('core')->jsonEncode($items);
        } else {
           return parent::getAttributeTreeJson();
        }
    }

    /**
     * Retrieve Attribute Set Group Tree as JSON format
     *
     * @return string
     */
    public function getGroupTreeJson() {

        $is_seller = Mage::helper('tribeseller')->checkSellerentity($this->_getSetId());
        if ($is_seller) {
            $items = array();
            $setId = $this->_getSetId();

            /* @var $groups Mage_Eav_Model_Mysql4_Entity_Attribute_Group_Collection */
            $groups = Mage::getModel('eav/entity_attribute_group')
                    ->getResourceCollection()
                    ->setAttributeSetFilter($setId)
                    ->setSortOrder()
                    ->load();

            /* @var $node Mage_Eav_Model_Entity_Attribute_Group */
            foreach ($groups as $node) {
                $item = array();
                $item['text'] = $node->getAttributeGroupName();
                $item['id'] = $node->getAttributeGroupId();
                $item['cls'] = 'folder';
                $item['allowDrop'] = true;
                $item['allowDrag'] = true;

                $nodeChildren = Mage::getResourceModel('tribeseller/attribute_collection')
                        ->setAttributeGroupFilter($node->getId())
                        ->load();

                if ($nodeChildren->getSize() > 0) {
                    $item['children'] = array();
                    foreach ($nodeChildren->getItems() as $child) {
                        /* @var $child Mage_Eav_Model_Entity_Attribute */
                        $attr = array(
                            'text' => $child->getAttributeCode(),
                            'id' => $child->getAttributeId(),
                            'cls' => (!$child->getIsUserDefined()) ? 'system-leaf' : 'leaf',
                            'allowDrop' => false,
                            'allowDrag' => true,
                            'leaf' => true,
                            'is_user_defined' => $child->getIsUserDefined(),
                            'is_configurable' => false,
                            'entity_id' => $child->getEntityAttributeId()
                        );

                        $item['children'][] = $attr;
                    }
                }

                $items[] = $item;
            }

            return Mage::helper('core')->jsonEncode($items);
        } else {
            return parent::getGroupTreeJson();
        }
    }
}