<?php

class Fedobe_Tribeseller_Block_Adminhtml_Review_Add extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        parent::__construct();

        $this->_blockGroup = 'tribeseller';
        $this->_controller = 'adminhtml_review';
        $this->_mode = 'add';

        $this->_updateButton('save', 'label', Mage::helper('review')->__('Save Review'));
        $this->_updateButton('save', 'id', 'save_button');

        $this->_updateButton('reset', 'id', 'reset_button');

        $starsettings = intval(Mage::getStoreConfig('tribecoresettings/seller_starrating_settings/tribe_seller_star_rating_no'));
        $nostars = ($starsettings) ? $starsettings : 5;
        
        $this->_formScripts[] = '
            toggleParentVis("add_review_form");
            toggleVis("save_button");
            toggleVis("reset_button");
        ';

        $this->_formInitScripts[] = '
            //<![CDATA[
            jQuery(function ($) {
        $("#seller_rating_detail").rateYo({
            starWidth: "25px",
            readOnly: false,
            spacing: "5px",
            numStars: '.$nostars.',
            multiColor: {
                "startColor": "#FF0000", //RED
                "endColor": "#00FF00"  //GREEN
            },
            halfStar: true,
            onSet: function (rating, rateYoInstance) {
                        $(\'#custom_rating\').val(rating);
            }
        });
    });

            var review = function() {
                return {
                    productInfoUrl : null,
                    formHidden : true,

                    gridRowClick : function(data, click) {
                        if(Event.findElement(click,\'TR\').title){
                            review.productInfoUrl = Event.findElement(click,\'TR\').title;
                            review.loadProductData();
                            review.showForm();
                            review.formHidden = false;
                        }
                    },

                    loadProductData : function() {
                        var con = new Ext.lib.Ajax.request(\'POST\', review.productInfoUrl, {success:review.reqSuccess,failure:review.reqFailure}, {form_key:FORM_KEY});
                    },

                    showForm : function() {
                        toggleParentVis("add_review_form");
                        toggleVis("sellersGrid");
                        toggleVis("save_button");
                        toggleVis("reset_button");
                    },
                    reqSuccess :function(o) {
                        var response = Ext.util.JSON.decode(o.responseText);
                        if( response.error ) {
                            alert(response.message);
                        } else if( response.id ){
                            $("product_id").value = response.id;

                            $("product_name").innerHTML = \'<a href="' . $this->getUrl('*/catalog_product/edit') . 'id/\' + response.id + \'" target="_blank">\' + response.seller_store_name + \'</a>\';
                        } else if( response.message ) {
                            alert(response.message);
                        }
                    }
                }
            }();
           //]]>
        ';
    }

    public function getHeaderText() {
        return Mage::helper('review')->__('Add New Review');
    }

}