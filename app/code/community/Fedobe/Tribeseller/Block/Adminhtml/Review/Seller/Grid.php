<?php

class Fedobe_Tribeseller_Block_Adminhtml_Review_Seller_Grid extends Fedobe_Tribeseller_Block_Adminhtml_Sellers_Grid {

    public function __construct() {
        parent::__construct();
        $this->setRowClickCallback('review.gridRowClick');
        $this->setUseAjax(true);
        $this->setId('sellersGrid');
    }

    protected function _prepareColumns() {
        $this->addColumn('entity_id', array(
            'header' => Mage::helper('review')->__('ID'),
            'width' => '50px',
            'index' => 'entity_id',
        ));

        $this->addColumn('seller_store_name', array(
            'header' => Mage::helper('review')->__('Name'),
            'index' => 'seller_store_name',
        ));

        if ((int) $this->getRequest()->getParam('store', 0)) {
            $this->addColumn('custom_name', array(
                'header' => Mage::helper('review')->__('Name in Store'),
                'index' => 'custom_name'
            ));
        }

        $this->addColumn('seller_user_name', array(
            'header' => Mage::helper('review')->__('Seller Username'),
            'width' => '80px',
            'index' => 'seller_user_name'
        ));

        $this->addColumn('seller_email', array(
            'header' => Mage::helper('review')->__('Seller Email'),
            'index' => 'seller_email'
        ));



        $this->addColumn('is_active', array(
            'header' => Mage::helper('review')->__('Status'),
            'width' => '90px',
            'index' => 'is_active',
            'type' => 'options',
            'source' => 'catalog/product_status',
            'options' => Mage::getSingleton('tribeseller/seller_attribute_status')->getAllOptions(),
        ));
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/productGrid', array('_current' => true));
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/jsonProductInfo', array('id' => $row->getId()));
    }

    protected function _prepareMassaction() {
        return $this;
    }

}