<?php

class Fedobe_Tribeseller_Block_Adminhtml_Review_Rating_Detailed extends Mage_Adminhtml_Block_Template {

    public function __construct() {
        $this->setTemplate('fedobe/tribe/seller/rating/stars/detailed.phtml');
        if (Mage::registry('review_data')) {
            $this->setReviewId(Mage::registry('review_data')->getReviewId());
        }
    }

    public function getRating() {
        if (Mage::registry('review_data')) {
            return Mage::registry('review_data')->getCustomRating();
        }else{
            return 0;
        }
    }

}