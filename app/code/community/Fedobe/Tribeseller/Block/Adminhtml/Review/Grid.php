<?php

class Fedobe_Tribeseller_Block_Adminhtml_Review_Grid extends Mage_Adminhtml_Block_Widget_Grid {
    
    const ENTITY_SELLER_CODE = 'seller';
    
    public function __construct() {
        parent::__construct();
        $this->setId('reviwGrid');
        $this->setDefaultSort('created_at');
    }

    protected function _prepareCollection() {
        $model = Mage::getModel('review/review');
        $review_entityid = $model->getEntityIdByCode(self::ENTITY_SELLER_CODE);
        $collection = $model->getCollection()->addFieldToFilter('main_table.entity_id',array('eq'=>$review_entityid)) ;
        if(Mage::registry('usePendingFilter'))
            $collection->addFieldToFilter('main_table.status_id',array('eq'=>2));
        $collection->getSelect()->join(
                            'tribe_seller_entity',
                            'main_table.entity_pk_value = tribe_seller_entity.entity_id', 
                            array('seller_store_name','seller_user_name') 
                          );
        if ($this->getProductId() || $this->getRequest()->getParam('productId', false)) {
            $productId = $this->getProductId();
            if (!$productId) {
                $productId = $this->getRequest()->getParam('productId');
            }
            $this->setProductId($productId);
            $collection->addEntityFilter($this->getProductId());
        }

        if ($this->getCustomerId() || $this->getRequest()->getParam('customerId', false)) {
            $customerId = $this->getCustomerId();
            if (!$customerId) {
                $customerId = $this->getRequest()->getParam('customerId');
            }
            $this->setCustomerId($customerId);
            $collection->addCustomerFilter($this->getCustomerId());
        }

        if (Mage::registry('usePendingFilter') === true) {
            $collection->addStatusFilter($model->getPendingStatus());
        }

        $collection->addStoreData();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('review_id', array(
            'header' => Mage::helper('review')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'filter_index' => 'review_id',
            'index' => 'review_id',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('review')->__('Created On'),
            'align' => 'left',
            'type' => 'datetime',
            'width' => '100px',
            'filter_index' => 'created_at',
            'index' => 'created_at',
        ));

        if (!Mage::registry('usePendingFilter')) {
            $this->addColumn('status', array(
                'header' => Mage::helper('review')->__('Status'),
                'align' => 'left',
                'type' => 'options',
                'options' => Mage::helper('review')->getReviewStatuses(),
                'width' => '100px',
                'filter_index' => 'status_id',
                'index' => 'status_id',
            ));
        }

        $this->addColumn('title', array(
            'header' => Mage::helper('review')->__('Title'),
            'align' => 'left',
            'width' => '100px',
            'filter_index' => 'title',
            'index' => 'title',
            'type' => 'text',
            'truncate' => 50,
            'escape' => true,
        ));

        $this->addColumn('nickname', array(
            'header' => Mage::helper('review')->__('Nickname'),
            'align' => 'left',
            'width' => '100px',
            'filter_index' => 'nickname',
            'index' => 'nickname',
            'type' => 'text',
            'truncate' => 50,
            'escape' => true,
        ));

        $this->addColumn('detail', array(
            'header' => Mage::helper('review')->__('Review'),
            'align' => 'left',
            'index' => 'detail',
            'filter_index' => 'detail',
            'type' => 'text',
            'truncate' => 50,
            'nl2br' => true,
            'escape' => true,
        ));

        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('visible_in', array(
                'header' => Mage::helper('review')->__('Visible In'),
                'index' => 'stores',
                'type' => 'store',
                'store_view' => true,
            ));
        }

        $this->addColumn('type', array(
            'header' => Mage::helper('review')->__('Type'),
            'type' => 'select',
            'index' => 'type',
            'filter' => 'adminhtml/review_grid_filter_type',
            'renderer' => 'adminhtml/review_grid_renderer_type'
        ));

        $this->addColumn('seller_store_name', array(
            'header' => Mage::helper('review')->__('Seller Name'),
            'align' => 'left',
            'type' => 'text',
            'index' => 'seller_store_name',
            'escape' => true
        ));

        $this->addColumn('custom_rating', array(
            'header' => Mage::helper('review')->__('Seller Rating'),
            'align' => 'right',
            'type' => 'text',
            'width' => '50px',
            'index' => 'custom_rating',
            'escape' => true
        ));
        
        $this->addColumn('seller_user_name', array(
            'header' => Mage::helper('review')->__('Seller Username'),
            'align' => 'right',
            'type' => 'text',
            'width' => '50px',
            'index' => 'seller_user_name',
            'escape' => true
        ));

        $this->addColumn('action', array(
            'header' => Mage::helper('adminhtml')->__('Action'),
            'width' => '50px',
            'type' => 'action',
            'getter' => 'getReviewId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('adminhtml')->__('Edit'),
                    'url' => array(
                        'base' => '*/sellerreviews/edit',
                        'params' => array(
                            'productId' => $this->getProductId(),
                            'customerId' => $this->getCustomerId(),
                            'ret' => ( Mage::registry('usePendingFilter') ) ? 'pending' : null
                        )
                    ),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false
        ));

//        $this->addRssList('rss/catalog/review', Mage::helper('catalog')->__('Pending Reviews RSS'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('review_id');
        $this->setMassactionIdFilter('rt.review_id');
        $this->setMassactionIdFieldOnlyIndexValue(true);
        $this->getMassactionBlock()->setFormFieldName('reviews');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('review')->__('Delete'),
            'url' => $this->getUrl(
                    '*/*/massDelete', array('ret' => Mage::registry('usePendingFilter') ? 'pending' : 'index')
            ),
            'confirm' => Mage::helper('review')->__('Are you sure?')
        ));

        $statuses = Mage::helper('review')->getReviewStatusesOptionArray();
        array_unshift($statuses, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('update_status', array(
            'label' => Mage::helper('review')->__('Update Status'),
            'url' => $this->getUrl(
                    '*/*/massUpdateStatus', array('ret' => Mage::registry('usePendingFilter') ? 'pending' : 'index')
            ),
            'additional' => array(
                'status' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('review')->__('Status'),
                    'values' => $statuses
                )
            )
        ));
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/sellerreviews/edit', array(
                    'id' => $row->getReviewId(),
                    'productId' => $this->getProductId(),
                    'customerId' => $this->getCustomerId(),
                    'ret' => ( Mage::registry('usePendingFilter') ) ? 'pending' : null,
        ));
    }

    public function getGridUrl() {
        if ($this->getProductId() || $this->getCustomerId()) {
            return $this->getUrl(
                            '*/sellerreviews/' . (Mage::registry('usePendingFilter') ? 'pending' : ''), array(
                        'productId' => $this->getProductId(),
                        'customerId' => $this->getCustomerId(),
                            )
            );
        } else {
            return $this->getCurrentUrl();
        }
    }

}
