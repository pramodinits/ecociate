<?php

class Fedobe_Tribeseller_Block_Adminhtml_Review_Rating_Summary extends Mage_Adminhtml_Block_Template {

    public function __construct() {
        $this->setTemplate('fedobe/tribe/seller/rating/stars/summary.phtml');
        if (Mage::registry('review_data')) {
            $this->setReviewId(Mage::registry('review_data')->getReviewId());
        }
    }
    public function getAverageRating(){
        $sellerid = Mage::registry('review_data')->getEntityPkValue();
        if($sellerid){
            $seller = Mage::getModel('tribeseller/seller')->load($sellerid);
            return $seller->getSellerAvgRating();
        }
        return 0;
    }

}