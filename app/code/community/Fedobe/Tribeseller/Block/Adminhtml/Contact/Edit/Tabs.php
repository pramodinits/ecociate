<?php
/**
 * @category    AM
 * @package     AM_RevSlider
 * @copyright   Copyright (C) 2008-2014 ArexMage.com. All Rights Reserved.
 * @license     GNU General Public License version 2 or later
 * @author      ArexMage.com
 * @email       support@arexmage.com
 */

class Fedobe_Tribeseller_Block_Adminhtml_Contact_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs{
    public function __construct(){ //echo 876;exit;
        parent::__construct();
        $this->setId('contact_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('tribeseller')->__('Item Information'));
    }

    protected function _beforeToHtml(){
        $this->addTab('form_section', array(
            'label'  => Mage::helper('tribeseller')->__('Item Information'),
            'title'  => Mage::helper('tribeseller')->__('Item Information'),
            'content'    => $this->getLayout()->createBlock('tribeseller/adminhtml_contact_edit_tab_form')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
}
