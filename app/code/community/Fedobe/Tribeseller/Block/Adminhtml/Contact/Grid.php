<?php

/**
 * Manage Customer Attribute grid block
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribeseller
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Tribeseller_Block_Adminhtml_Contact_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Prepare customer attributes grid collection object
     *
     * @return Fedobe_Tribeseller_Block_Adminhtml_Tribeseller_Grid
     */
    protected function _prepareCollection() {
       $collection = Mage::getModel('tribeseller/contact')->getCollection();//echo $collection;exit;
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function getSelleStatus() {
        return Mage::getModel('tribeseller/seller_attribute_status')->getAllOptions();
    }

    protected function _prepareColumns() {
        $this->addColumn('id', array(
            'header' => Mage::helper('tribeseller')->__('ID'),
            'width' => '50px',
            'type' => 'number',
            'index' => 'id',
        ));
         $this->addColumn('entity_name', array(
            'header' => Mage::helper('tribeseller')->__('Entity Name'),
            'index' => 'entity_name',
        ));
         
          $this->addColumn('email', array(
            'header' => Mage::helper('tribeseller')->__('Email'),
            'index' => 'email',
        ));
          
           $this->addColumn('contact_no', array(
            'header' => Mage::helper('tribeseller')->__('Phone'),
            'index' => 'contact_no',
        ));
        $this->addColumn('created_at', array(
            'header' => Mage::helper('tribeseller')->__('Created At'),
            'index' => 'created_at',
        ));

        $this->addColumn('action', array(
            'header' => Mage::helper('tribeseller')->__('Action'),
            'width' => '50px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('tribeseller')->__('Edit'),
                    'url' => array(
                        'base' => '*/*/edit',
                        'params' => array('store' => $this->getRequest()->getParam('store'))
                    ),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
        ));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
            
        } else {
            $this->setMassactionIdField('entity_id');
            $this->getMassactionBlock()->setFormFieldName('sellers');

            $this->getMassactionBlock()->addItem('delete', array(
                'label' => Mage::helper('tribeseller')->__('Delete'),
                'url' => $this->getUrl('*/*/massDelete'),
                'confirm' => Mage::helper('tribeseller')->__('Are you sure?')
            ));

            $statuses = $this->getSelleStatus();

            array_unshift($statuses, array('label' => '', 'value' => ''));
            $this->getMassactionBlock()->addItem('status', array(
                'label' => Mage::helper('tribeseller')->__('Change status'),
                'url' => $this->getUrl('*/*/massStatus', array('_current' => true)),
                'additional' => array(
                    'visibility' => array(
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => Mage::helper('tribeseller')->__('Status'),
                        'values' => $statuses
                    )
                )
            ));
        }
        Mage::dispatchEvent('tribe_seller_adminhtml_sellers_grid_prepare_massaction', array('block' => $this));
        return $this;
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/index', array('_current' => true));
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array(
                    'store' => $this->getRequest()->getParam('store'),
                    'id' => $row->getId())
        );
    }

}