<?php
class Fedobe_Tribeseller_Block_Adminhtml_Contact_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
     public function __construct(){
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'Contact';
        $this->_controller = 'adminhtml_contact';
        $this->_mode = 'edit';

        $this->_updateButton('save', 'label', Mage::helper('tribeseller')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('tribeseller')->__('Delete Item'));
    }

    public function getHeaderText(){
        if(Mage::registry('tests_data') && Mage::registry('tests_data')->getId())
            return Mage::helper('tribeseller')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('tests_data')->getTitle()));
        return Mage::helper('tribeseller')->__('Add Item');
    }
}