<?php

/**
 * Grid container file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribeseller
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Tribeseller_Block_Adminhtml_Tribeseller extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_tribeseller';
        $this->_blockGroup = 'tribeseller';
        $this->_headerText = Mage::helper('tribeseller')->__('Manage Shop Attributes ');
        $this->_addButtonLabel = Mage::helper('tribeseller')->__('Add New Shop Attribute');
        parent::__construct();
    }

}