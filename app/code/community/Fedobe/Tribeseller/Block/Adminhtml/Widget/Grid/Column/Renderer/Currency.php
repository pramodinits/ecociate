<?php

class Fedobe_Tribeseller_Block_Adminhtml_Widget_Grid_Column_Renderer_Currency extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Currency {

    protected function _construct() {
        parent::_construct();
        if (is_null($this->is_seller)) {
            $this->is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        }
        if ($this->is_seller) {
            if (is_null($this->seller_id)) {
                $this->seller_id = Mage::helper('tribeseller')->getSellerIdFromadminUser();
            }
        }
    }

    public function render(Varien_Object $row) {
        if ($this->is_seller) {
            if ($this->getColumn()->getIndex() == 'base_grand_total') {
                $data = Mage::helper('tribeseller')->getSellerBaseGrandTotal($row->getId(),$this->seller_id);
            }
            if ($this->getColumn()->getIndex() == 'grand_total') {
                $data = Mage::helper('tribeseller')->getSellerGrandTotal($row->getId(),$this->seller_id);
            }
            if ($data) {
                $currency_code = $this->_getCurrencyCode($row);
                if (!$currency_code) {
                    return $data;
                }
                $data = floatval($data) * $this->_getRate($row);
                $sign = (bool) (int) $this->getColumn()->getShowNumberSign() && ($data > 0) ? '+' : '';
                $data = sprintf("%f", $data);
                $data = Mage::app()->getLocale()->currency($currency_code)->toCurrency($data);
                return $sign . $data;
            }
            return $this->getColumn()->getDefault();
        } else {
            return parent::render($row);
        }
    }

}
