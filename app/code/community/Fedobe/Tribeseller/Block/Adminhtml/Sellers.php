<?php
/**
 * Grid container file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribeseller
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Tribeseller_Block_Adminhtml_Sellers extends Mage_Adminhtml_Block_Widget_Grid_Container
{       
  public function __construct()
  {
    /*both these variables tell magento the location of our Grid.php(grid block) file.
     * $this->_blockGroup.'/' . $this->_controller . '_grid'
     * i.e clarion_Customerattribute/adminhtml_customerattribute_grid
     * $_blockGroup - is your module's name.
     * $_controller - is the path to your grid block. 
     */
    $this->_controller = 'adminhtml_sellers';
    $this->_blockGroup = 'tribeseller';
    
    $this->_headerText = Mage::helper('tribeseller')->__('Manage Shops');
    $this->_addButtonLabel = Mage::helper('tribeseller')->__('Add New Shop');
    parent::__construct(); 
    $this->_removeButton('add');
  }
}