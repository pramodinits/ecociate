<?php

class Fedobe_Tribeseller_Block_Adminhtml_Menu extends Mage_Adminhtml_Block_Page_Menu {

    public function getMenuArray() {
        //Load standard menu
        $parentArr = parent::getMenuArray();
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
            $sellerId = Mage::helper('tribeseller')->getSellerIdFromadminUser();
            $channelId = Mage::helper('tribeseller')->getChannelIdForSeller($sellerId);
            $parentArr['sales']['children']['createorder'] = array(
                'label' => 'Create Order',
                'sort_order' => 10,
                'url' => Mage::helper('adminhtml')->getUrl('adminhtml/sales_order_create/index'),
                'active' => false,
                'level' => 1
            );

            unset($parentArr['customer']['children']['manage']);
            $parentArr['customer']['children']['allcustomer'] = array(
                'label' => 'All Customers',
                'sort_order' => 1,
                'url' => Mage::helper('adminhtml')->getUrl('adminhtml/customer/index'),
                'active' => false,
                'level' => 1
            );
            $parentArr['customer']['children']['newcustomer'] = array(
                'label' => 'Add a New Customer',
                'sort_order' => 2,
                'url' => Mage::helper('adminhtml')->getUrl('adminhtml/customer/new'),
                'active' => false,
                'level' => 1
            );
            unset($parentArr['fedobe']);

            $parentArr['inventory'] = array(
                'label' => 'Inventory',
                'active' => false,
                'sort_order' => 0,
                'click' => 'return false;',
                'url' => '#',
                'level' => 0,
                'last' => true,
                'children' => array(
                    'manageinventory' => array(
                        'label' => 'Manage Inventory',
                        'sort_order' => 1,
                        'url' => Mage::helper('adminhtml')->getUrl('adminhtml/edi/index'),
                        'active' => false,
                        'level' => 1
                    )
                )
            );
            if ($channelId) {
                $parentArr['inventory']['children']['bulkupload'] = array(
                    'label' => 'Bulk Update',
                    'sort_order' => 2,
                    'url' => Mage::helper('adminhtml')->getUrl('adminhtml/incomingbridgecsv/edit', array('id' => $channelId)),
                    'active' => false,
                    'level' => 1
                );
            }
            $parentArr['profile'] = array(
                'label' => 'Profile',
                'active' => false,
                'sort_order' => 0,
                'url' => Mage::helper('adminhtml')->getUrl('adminhtml/sellerprofiles/edit/', array('id' => $sellerId)),
                'level' => 0,
                'last' => true,
                'children' => array()
            );
        }
        return $parentArr;
    }

}