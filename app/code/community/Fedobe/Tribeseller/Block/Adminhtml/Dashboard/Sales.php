<?php

class Fedobe_Tribeseller_Block_Adminhtml_Dashboard_Sales extends Mage_Adminhtml_Block_Dashboard_Sales {

    protected function _construct() {
        parent::_construct();
    }

    protected function _prepareLayout() {
        if (!Mage::helper('core')->isModuleEnabled('Mage_Reports')) {
            return $this;
        }
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
            //SELECT (IFNULL(item.base_row_invoiced, 0) - IFNULL(item.base_tax_invoiced, 0) - IFNULL(forder.base_shipping_invoiced, 0) - (IFNULL(item.base_amount_refunded, 0) - IFNULL(item.base_tax_refunded, 0) - IFNULL(forder.base_shipping_refunded, 0))) * forder.base_to_global_rate FROM `sales_flat_order_item` AS item LEFT JOIN `sales_flat_order` AS forder ON item.order_id = forder.entity_id WHERE item.`tribe_seller_id` = 1
            $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
            $sellerid = Mage::helper('tribeseller')->getSellerIdFromadminUser();
            $item_table = $resource->getTableName('sales/order_item');
            $order_table = $resource->getTableName('sales/order');
            $expr = "(IFNULL(item.base_row_invoiced, 0) - IFNULL(item.base_tax_invoiced, 0) - IFNULL(forder.base_shipping_invoiced, 0) - (IFNULL(item.base_amount_refunded, 0) - IFNULL(item.base_tax_refunded, 0) - IFNULL(forder.base_shipping_refunded, 0))) * forder.base_to_global_rate";
            $finalexpr = "SUM($expr) AS lifetime , AVG($expr) AS averageval";
            $sql = "SELECT $finalexpr FROM $item_table AS `item` LEFT JOIN $order_table AS `forder` ON `item`.order_id = `forder`.entity_id WHERE `item`.`tribe_seller_id` = $sellerid";
            $sqlattrQuery = $read->query($sql);
            $data = $sqlattrQuery->fetchAll();
            $this->addTotal($this->__('Total sales'), $data[0]['lifetime']);
            $this->addTotal($this->__('Average order value'), $data[0]['averageval']);
        } else {
            $isFilter = $this->getRequest()->getParam('store') || $this->getRequest()->getParam('website') || $this->getRequest()->getParam('group');
            $collection = Mage::getResourceModel('reports/order_collection')
                    ->calculateSales($isFilter);
            if ($this->getRequest()->getParam('store')) {
                $collection->addFieldToFilter('store_id', $this->getRequest()->getParam('store'));
            } else if ($this->getRequest()->getParam('website')) {
                $storeIds = Mage::app()->getWebsite($this->getRequest()->getParam('website'))->getStoreIds();
                $collection->addFieldToFilter('store_id', array('in' => $storeIds));
            } else if ($this->getRequest()->getParam('group')) {
                $storeIds = Mage::app()->getGroup($this->getRequest()->getParam('group'))->getStoreIds();
                $collection->addFieldToFilter('store_id', array('in' => $storeIds));
            }

            $collection->load();
            $sales = $collection->getFirstItem();

            $this->addTotal($this->__('Lifetime Sales'), $sales->getLifetime());
            $this->addTotal($this->__('Average Orders'), $sales->getAverage());
        }
    }

}