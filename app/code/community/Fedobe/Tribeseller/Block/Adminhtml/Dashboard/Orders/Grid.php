<?php

class Fedobe_Tribeseller_Block_Adminhtml_Dashboard_Orders_Grid extends Mage_Adminhtml_Block_Dashboard_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('lastOrdersGrid');
    }

    protected function _prepareCollection() {
        if (!Mage::helper('core')->isModuleEnabled('Mage_Reports')) {
            return $this;
        }

        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
            $sellerid = Mage::helper('tribeseller')->getSellerIdFromadminUser();
            $collection = Mage::getResourceModel('sales/order_item_collection')
                    ->addAttributeToSelect('order_id') 
                    ->addAttributeToSelect('base_row_total') 
                    ->addAttributeToFilter('tribe_seller_id', array('eq' => $sellerid));
            $collection->getSelect()->columns(
                    array(
                'items_count' => 'COUNT(*)',
                    ), 'main_table'
            );
            $collection->getSelect()->group('order_id');

            $collection->getSelect()->join(
                    array('order' => 'sales_flat_order'), 'main_table.order_id = order.entity_id', array('order.*')
            );
            $fieldConcat = " CONCAT(order.customer_firstname,' ',order.customer_lastname)";
            $collection->getSelect()->columns(array('customer' => $fieldConcat));
            
            $collection->getSelect()->columns(array(
                'revenue' => '(SUM(main_table.base_row_total) * order.base_to_global_rate)'
            ));
            $collection->setOrder('created_at', 'DESC');
        } else {
            $collection = Mage::getResourceModel('reports/order_collection')
                    ->addItemCountExpr()
                    ->joinCustomerName('customer')
                    ->orderByCreatedAt();

            if ($this->getParam('store') || $this->getParam('website') || $this->getParam('group')) {
                if ($this->getParam('store')) {
                    $collection->addAttributeToFilter('store_id', $this->getParam('store'));
                } else if ($this->getParam('website')) {
                    $storeIds = Mage::app()->getWebsite($this->getParam('website'))->getStoreIds();
                    $collection->addAttributeToFilter('store_id', array('in' => $storeIds));
                } else if ($this->getParam('group')) {
                    $storeIds = Mage::app()->getGroup($this->getParam('group'))->getStoreIds();
                    $collection->addAttributeToFilter('store_id', array('in' => $storeIds));
                }

                $collection->addRevenueToSelect();
            } else {
                $collection->addRevenueToSelect(true);
            }
        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _preparePage() {
        $this->getCollection()->setPageSize($this->getParam($this->getVarNameLimit(), $this->_defaultLimit));
        // Remove count of total orders $this->getCollection()->setCurPage($this->getParam($this->getVarNamePage(), $this->_defaultPage));
    }

    protected function _prepareColumns() {
        $this->addColumn('customer', array(
            'header' => $this->__('Customer'),
            'sortable' => false,
            'index' => 'customer',
            'default' => $this->__('Guest'),
        ));

        $this->addColumn('items', array(
            'header' => $this->__('Items'),
            'align' => 'right',
            'type' => 'number',
            'sortable' => false,
            'index' => 'items_count'
        ));

        $baseCurrencyCode = Mage::app()->getStore((int) $this->getParam('store'))->getBaseCurrencyCode();

        $this->addColumn('total', array(
            'header' => $this->__('Grand Total'),
            'align' => 'right',
            'sortable' => false,
            'type' => 'currency',
            'currency_code' => $baseCurrencyCode,
            'index' => 'revenue'
        ));

        $this->setFilterVisibility(false);
        $this->setPagerVisibility(false);

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        $order_id = ($is_seller) ? $row->getOrderId() : $row->getId();
        return $this->getUrl('*/sales_order/view', array('order_id' => $order_id));
    }

}
