<?php

/**
 * Manage Seller Attribute controller file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribeseller
 * @author      Fedobe Magento Team
 */
class Fedobe_Tribeseller_Block_Adminhtml_Sellers_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    protected $_attributeTabBlock = 'tribeseller/adminhtml_sellers_edit_tab_attributes';

    public function __construct() {
        parent::__construct();
        $this->setId('seller_info_tabs');
        $this->setDestElementId('seller_edit_form');
        $this->setTitle(Mage::helper('tribeseller')->__('Shop Information'));
    }

    protected function _prepareLayout() {
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        $ignoregroups = ($is_seller) ? array('Home', 'Meta Information', 'Images','') : array();

        $entityTypeId = Mage::helper('tribeseller')->getEntityTypeId();
        $setId = Mage::helper('tribeseller')->getEntityAttributeSetId();
        $storeId = 0;
        if ($this->getRequest()->getParam('store')) {
            $storeId = Mage::app()->getStore($this->getRequest()->getParam('store'))->getId();
        }
        if ($setId) {
            $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
                    ->setAttributeSetFilter($setId)
                    ->setSortOrder()
                    ->load();
            foreach ($groupCollection as $group) {
                if (!in_array($group->getAttributeGroupName(), $ignoregroups)) {
                    $attributeCollection = Mage::getResourceModel('eav/entity_attribute_collection')
                            ->setAttributeGroupFilter($group->getId());
                    $attributes = array();
                    foreach ($attributeCollection as $attribute) {
                        $attributes[] = $attribute;
                    }

                    if ($group->getAttributeGroupName() == 'Meta Information') {
                        $this->addTabAfter('group_' . $group->getId(), array(
                            'label' => Mage::helper('tribeseller')->__($group->getAttributeGroupName()),
                            'class' => Mage::helper('tribeseller')->__($group->getAttributeGroupName()),
                            'content' => $this->_translateHtml($this->getLayout()->createBlock($this->getAttributeTabBlock(), 'tribeseller.adminhtml.sellers.edit.tab.attributes')->setGroup($group)
                                            ->setGroupAttributes($attributes)
                                            ->setStoreId($storeId)
                                            ->setAttributeSetId($setId)
                                            ->setTypeId($entityTypeId)
                                            ->toHtml()),
                                ), 'products');
                    } else {
                        $this->addTab('group_' . $group->getId(), array(
                            'label' => Mage::helper('tribeseller')->__($group->getAttributeGroupName()),
                            'class' => Mage::helper('tribeseller')->__($group->getAttributeGroupName()),
                            'content' => $this->_translateHtml($this->getLayout()->createBlock($this->getAttributeTabBlock(), 'tribeseller.adminhtml.sellers.edit.tab.attributes')->setGroup($group)
                                            ->setGroupAttributes($attributes)
                                            ->setStoreId($storeId)
                                            ->setAttributeSetId($setId)
                                            ->setTypeId($entityTypeId)
                                            ->toHtml()),
                        ));
                    }
                }
            }

//            $this->addTab('products', array(
//                'label' => Mage::helper('tribeseller')->__('Products'),
//                'url' => $this->getUrl('*/*/products', array('_current' => true)),
//                'class' => 'ajax',
//            ));

            if ($this->getRequest()->getParam('id', false)) {
                if (Mage::helper('tribeseller')->isModuleEnabled('Mage_Review') && !$is_seller) {
                    $this->addTab('reviews', array(
                        'label' => Mage::helper('tribeseller')->__('Shop Reviews'),
                        'url' => $this->getUrl('*/*/reviews', array('_current' => true)),
                        'class' => 'ajax',
                    ));
                }
            }
        }

        return parent::_prepareLayout();
    }

    public function getSeller() {
        if (!$this->getData('sellerprofile_data')) {
            $this->setData('sellerprofile_data', Mage::registry('sellerprofile_data'));
        }
        return $this->getData('product');
    }

    /**
     * Getting attribute block name for tabs
     *
     * @return string
     */
    public function getAttributeTabBlock() {
        return $this->_attributeTabBlock;
    }

    public function setAttributeTabBlock($attributeTabBlock) {
        $this->_attributeTabBlock = $attributeTabBlock;
        return $this;
    }

    /**
     * Translate html content
     *
     * @param string $html
     * @return string
     */
    protected function _translateHtml($html) {
        Mage::getSingleton('core/translate_inline')->processResponseBody($html);
        return $html;
    }

}