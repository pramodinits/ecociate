<?php

class Fedobe_Tribeseller_Block_Adminhtml_Sellers_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareLayout() {
        Varien_Data_Form::setElementRenderer(
            $this->getLayout()->createBlock('adminhtml/widget_form_renderer_element')
        );
        Varien_Data_Form::setFieldsetRenderer(
            $this->getLayout()->createBlock('adminhtml/widget_form_renderer_fieldset')
        );
        Varien_Data_Form::setFieldsetElementRenderer(
            $this->getLayout()->createBlock('tribeseller/adminhtml_sellers_form_renderer_fieldset_element')
        );
    }

}
