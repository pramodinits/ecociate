<?php

class Fedobe_Tribeseller_Block_Adminhtml_Sellers_Form_Renderer_Attribute_Urlkey extends Fedobe_Tribeseller_Block_Adminhtml_Sellers_Form_Renderer_Fieldset_Element {

    public function getElementHtml() {
        $element = $this->getElement();
        $actionurl = Mage::helper('tribeseller')->getvalidateurlkeyaction();
        $id = Mage::app()->getRequest()->getParam('id')?Mage::app()->getRequest()->getParam('id'):0;
        $element->setOnkeyup("onUrlkeyChanged('".$id."','" . $element->getHtmlId() . "','".$actionurl."')");
        $element->setOnchange("onUrlkeyChanged('".$id."','" . $element->getHtmlId() . "','".$actionurl."')");
        return parent::getElementHtml()."<div id='seller_url_key'></div>";
    }

}