<?php

class Fedobe_Tribeseller_Block_Adminhtml_Sellers_Form_Element_Content extends Varien_Data_Form_Element_Editor {

    public function __construct($attributes = array()) {
        parent::__construct($attributes);
        $this->setType('tribecontent');
        $this->setType('wysiwyg');
        $this->setExtType('wysiwyg');

        $data = array(
            'enabled' => true,
            'hidden' => true,
            'use_container' => false,
            'add_variables' => true,
            'add_widgets' => true,
            'add_images' => true,
            'no_display' => false,
            'translator' => Mage::helper('cms'),
            'encode_directives' => true,
            'directives_url' => Mage::getSingleton('adminhtml/url')->getUrl('*/cms_wysiwyg/directive'),
            'popup_css' =>
            Mage::getBaseUrl('js') . 'mage/adminhtml/wysiwyg/tiny_mce/themes/advanced/skins/default/dialog.css',
            'content_css' =>
            Mage::getBaseUrl('js') . 'mage/adminhtml/wysiwyg/tiny_mce/themes/advanced/skins/default/content.css',
            'width' => '100%',
            'plugins' => array(),
            'widget_window_url' => Mage::getSingleton('adminhtml/url')->getUrl("adminhtml/widget/index"),
            'files_browser_window_url' => Mage::getSingleton('adminhtml/url')->getUrl('*/cms_wysiwyg_images/index'),
        );
        parent::getConfig()->setData($data);

        $settings = Mage::getModel('widget/widget_config')->getPluginSettings(parent::getConfig());
        parent::getConfig()->addData($settings);
        $plugins = new Mage_Core_Model_Variable_Config();
        $configplugins = $plugins->getWysiwygPluginSettings(parent::getConfig());
        parent::getConfig()->setData('plugins', $configplugins['plugins']);
    }

    public function getElementHtml() {
        return parent::getElementHtml();
    }

}