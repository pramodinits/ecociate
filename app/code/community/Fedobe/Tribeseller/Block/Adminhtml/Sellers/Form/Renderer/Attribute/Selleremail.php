<?php

class Fedobe_Tribeseller_Block_Adminhtml_Sellers_Form_Renderer_Attribute_Selleremail extends Fedobe_Tribeseller_Block_Adminhtml_Sellers_Form_Renderer_Fieldset_Element {

   public function getElementHtml() {
        $element = $this->getElement();
        $id = $element->getId();
        $entity_id = (int) Mage::app()->getRequest()->getParam('id');
        $html = parent::getElementHtml();
        $controllername = 'sellerprofiles';
        $url = Mage::helper("adminhtml")->getUrl("*/$controllername/isemailexist");
        $html .= "<div id='sl_email_err'></div><script type='text/javascript'>
                   $('seller_email').on('blur',function(){
                   var custurl = '".$url."';
                  var email = jQuery(this).val();
                  if(email)
                  matchselleremailid(custurl,email);
                    });
                   </script>";
        return $html;
    }

}