<?php

class Fedobe_Tribeseller_Block_Adminhtml_Sellers_Settings extends Mage_Adminhtml_Block_Widget_Tabs {

    protected function _prepareLayout() {
        $this->addTab('set', array(
            'label' => Mage::helper('catalog')->__('Settings'),
            'content' => $this->_translateHtml($this->getLayout()
                            ->createBlock('tribeseller/adminhtml_sellers_tab_settings')->toHtml()),
            'active' => true
        ));
        return parent::_prepareLayout();
    }

    protected function _translateHtml($html) {
        Mage::getSingleton('core/translate_inline')->processResponseBody($html);
        return $html;
    }

}
