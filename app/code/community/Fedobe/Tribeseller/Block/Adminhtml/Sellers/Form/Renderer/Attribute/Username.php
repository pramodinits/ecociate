<?php

class Fedobe_Tribeseller_Block_Adminhtml_Sellers_Form_Renderer_Attribute_Username extends Fedobe_Tribeseller_Block_Adminhtml_Sellers_Form_Renderer_Fieldset_Element {

    public function getElementHtml() {
        $element = $this->getElement();
        $actionurl = Mage::helper('tribeseller')->getvalidateurlkeyaction();
        $cusurl = Mage::helper('tribeseller')->getCustomerInfourlkeyaction();
        $id = Mage::app()->getRequest()->getParam('id')?Mage::app()->getRequest()->getParam('id'):0;
        $element->setOnkeyup("onStoreNameChanged('".$id."','" . $element->getHtmlId() .  "','".$actionurl."')");
        $element->setOnchange("onStoreNameChanged('".$id."','" . $element->getHtmlId() .  "','".$actionurl."')");
        $username = Mage::registry('sellerprofile_data')->getSellerUserName();
        $html  = parent::getElementHtml()."<div id='seller_store_name_msg'></div><div id='seller_store_url'></div>";
        $html .="<script type='text/javascript'>var customerurl = '$cusurl'; var sellerid = $id;</script>";
        if($username){
            $sellerurl = Mage::helper('tribeseller')->getFrontendProductUrl($username);
            $urlkey = '<div class="sellermsg" style="color:#3d6611 !important"><a href="'.$sellerurl.'">'.$sellerurl.'</a></div>';
            $html .= "<script type='text/javascript'>document.observe('dom:loaded', function() {
                            $('seller_store_url').update('$urlkey');
                    });</script>";
        }
        return $html;
    }

}