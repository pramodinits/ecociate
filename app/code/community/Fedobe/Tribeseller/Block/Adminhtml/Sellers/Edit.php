<?php

/**
 * Customer attribute edit block
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribeseller
 * @author      Fedobe Magento Team
 */
class Fedobe_Tribeseller_Block_Adminhtml_Sellers_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        $this->_controller = 'sellerprofiles';
        $this->_blockGroup = 'tribeseller';
        parent::__construct();
        $this->setTemplate('fedobe/tribe/seller/edit.phtml');
        $this->setId('seller_edit_form');
        $store_id = ($this->getRequest()->getParam('store')) ? $this->getRequest()->getParam('store') : Mage_Core_Model_App::ADMIN_STORE_ID;
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                seller_edit_form.submit($('seller_edit_form').action+'back/edit/store/{$store_id}/');
            }
        ";
    }

    protected function _prepareLayout() {
        $this->_updateButton('save', 'label', Mage::helper('tribeseller')->__('Save Shop'));
        $this->_updateButton('save', 'onclick', 'seller_edit_form.submit()');
        if (!Mage::registry('sellerprofile_data')->getId()) {
                $this->_removeButton('delete');
            } else {
                $this->_updateButton('delete', 'label', Mage::helper('tribeseller')->__('Delete Shop'));
            }
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
            $this->_removeButton('delete');
        }
        $this->_addButton(
                'saveandcontinue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit()',
            'class' => 'save',
                ), 100
        );
    }

    /**
     * This function return’s the Text to display as the form header.
     */
    public function getHeaderText() {
        if (Mage::registry('sellerprofile_data')->getId()) {
            $frontendLabel = Mage::registry('sellerprofile_data')->getSellerStoreName();
            if (is_array($frontendLabel)) {
                $frontendLabel = $frontendLabel[0];
            }
            return Mage::helper('tribeseller')->__('Edit Shop "%s"', $this->escapeHtml($frontendLabel)) . " ( " . $this->getAttributeSetName() . " )";
        } else {
            return Mage::helper('tribeseller')->__('New Shop') . " ( " . $this->getAttributeSetName() . " )";
        }
    }

    public function getAttributeSetName($setid) {
        $attrsetid = ($setid) ? $setid : Mage::registry('sellerprofile_data')->getAttributeSetId();
        $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
        $attributeSetModel->load($attrsetid);
        return $this->escapeHtml($attributeSetModel->getAttributeSetName());
    }

    public function getValidationUrl() {
        return $this->getUrl('*/*/validate', array('_current' => true));
    }

    public function getSaveUrl() {
        return $this->getUrl('*/' . $this->_controller . '/save', array('_current' => true, 'back' => null));
    }

}