<?php

class Fedobe_Tribeseller_Block_Adminhtml_Sellers_Form_Renderer_Fieldset_Element extends Mage_Adminhtml_Block_Widget_Form_Renderer_Fieldset_Element {

    const SCOPE_STORE = 0;
    const SCOPE_GLOBAL = 1;
    const SCOPE_WEBSITE = 2;

    /**
     * Initialize block template
     */
    protected function _construct() {
        $this->setTemplate('fedobe/tribe/seller/form/renderer/fieldset/element.phtml');
    }

    /**
     * Retrieve data object related with form
     *
     * @return Mage_Catalog_Model_Product || Mage_Catalog_Model_Category
     */
    public function getDataObject() {
        return $this->getElement()->getForm()->getDataObject();
    }

    /**
     * Retireve associated with element attribute object
     *
     * @return Mage_Catalog_Model_Resource_Eav_Attribute
     */
    public function getAttribute() {
        $attribute = $this->getElement()->getEntityAttribute();
        $addtionaldat = $this->getAddtionaldata($attribute);
        $attribute->addData($addtionaldat);
        return $attribute;
    }

    public function isScopeGlobal() {
        return $this->getIsGlobal() == self::SCOPE_GLOBAL;
    }

    /**
     * Retrieve attribute is website scope website
     *
     * @return bool
     */
    public function isScopeWebsite() {
        return $this->getIsGlobal() == self::SCOPE_WEBSITE;
    }

    /**
     * Retrieve attribute is store scope flag
     *
     * @return bool
     */
    public function isScopeStore() {
        return !$this->isScopeGlobal() && !$this->isScopeWebsite();
    }

    public function getAddtionaldata($attribute) {
        $attr_id = $attribute->getAttributeId();
        $addtionaldata = Mage::getModel('tribeseller/attributedata')->load($attr_id);
        return $addtionaldata->getData();
    }

    /**
     * Retrieve associated attribute code
     *
     * @return string
     */
    public function getAttributeCode() {
        return $this->getAttribute()->getAttributeCode();
    }

    /**
     * Check "Use default" checkbox display availability
     *
     * @return bool
     */
    public function canDisplayUseDefault() {
        if ($attribute = $this->getAttribute()) {
            $isScopeGlobal = $attribute->getData('is_global') == self::SCOPE_GLOBAL;
            if (!$isScopeGlobal && $this->getDataObject() && $this->getDataObject()->getId() && $this->getDataObject()->getStoreId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check default value usage fact
     *
     * @return bool
     */
    public function usedDefault() {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $attrDataValue = $this->getDataObject()->getAttributeDefaultValue($this->getAttribute());
        $store_id = $this->_getDefaultStoreId();
        $attribute_id = $this->getAttribute()->getAttributeId();
        $defaultValue = array_key_exists($attribute_id,$attrDataValue[$store_id]) ? $attrDataValue[$store_id][$attribute_id] : false;
        $storeview_id = Mage::app()->getRequest()->getParam('store');
        $storevalueflag = array_key_exists($attribute_id,$attrDataValue[$storeview_id]);
        if (!$storevalueflag) {
            return true;
        } else if ($this->getElement()->getValue() == $defaultValue &&
                $this->getDataObject()->getStoreId() != $this->_getDefaultStoreId()
        ) {
            return false;
        }
        if ($defaultValue === false && !$this->getAttribute()->getIsRequired() && $this->getElement()->getValue()) {
            return false;
        }
        return $defaultValue === false;
    }

    /**
     * Disable field in default value using case
     *
     * @return Mage_Adminhtml_Block_Catalog_Form_Renderer_Fieldset_Element
     */
    public function checkFieldDisable() {
        if ($this->canDisplayUseDefault() && $this->usedDefault()) {
            $this->getElement()->setDisabled(true);
        }
        return $this;
    }

    /**
     * Retrieve label of attribute scope
     *
     * GLOBAL | WEBSITE | STORE
     *
     * @return string
     */
    public function getScopeLabel() {
        $html = '';
        $attribute = $this->getElement()->getEntityAttribute();
        if (!$attribute || Mage::app()->isSingleStoreMode() || $attribute->getFrontendInput() == 'gallery') {
            return $html;
        }

        /*
         * Check if the current attribute is a 'price' attribute. If yes, check
         * the config setting 'Catalog Price Scope' and modify the scope label.
         */
        $isGlobalPriceScope = false;
        if ($attribute->getFrontendInput() == 'price') {
            $priceScope = Mage::getStoreConfig('catalog/price/scope');
            if ($priceScope == 0) {
                $isGlobalPriceScope = true;
            }
        }
        $isScopeGlobal = ($attribute->getData('is_global') == self::SCOPE_GLOBAL);
        $isScopeWebsite = ($attribute->getData('is_global') == self::SCOPE_WEBSITE);
        $isScopeStore = (!$isScopeGlobal && !$isScopeWebsite);
        if ($isScopeGlobal || $isGlobalPriceScope) {
            $html .= Mage::helper('adminhtml')->__('[GLOBAL]');
        } elseif ($isScopeWebsite) {
            $html .= Mage::helper('adminhtml')->__('[WEBSITE]');
        } elseif ($isScopeStore) {
            $html .= Mage::helper('adminhtml')->__('[STORE VIEW]');
        }

        return $html;
    }

    /**
     * Retrieve element label html
     *
     * @return string
     */
    public function getElementLabelHtml() {
        $element = $this->getElement();
        $label = $element->getLabel();
        if (!empty($label)) {
            $element->setLabel($this->__($label));
        }
        return $element->getLabelHtml();
    }

    /**
     * Retrieve element html
     *
     * @return string
     */
    public function getElementHtml() {
        return $this->getElement()->getElementHtml();
    }

    /**
     * Default sore ID getter
     *
     * @return integer
     */
    protected function _getDefaultStoreId() {
        return Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID;
    }

}