<?php

/**
 * Manage Seller Attribute controller file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribeseller
 * @author      Fedobe Magento Team
 */
class Fedobe_Tribeseller_Block_Adminhtml_Sellers_Edit_Tab_Attributes extends Fedobe_Tribeseller_Block_Adminhtml_Sellers_Form {

    /**
     * Load Wysiwyg on demand and prepare layout
     */
    protected function _prepareLayout() {
        parent::_prepareLayout();
        $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
    }

    /**
     * Prepare attributes form
     *
     * @return null
     */
    protected function _prepareForm() {
        $group = $this->getGroup();
        if ($group) {
            $form = new Varien_Data_Form();
            // Initialize product object as form property to use it during elements generation
            $form->setDataObject(Mage::registry('sellerprofile_data'));

            $fieldset = $form->addFieldset('group_fields' . $group->getId(), array(
                'legend' => Mage::helper('tribeseller')->__($group->getAttributeGroupName()),
                'class' => 'fieldset-wide'
            ));

            $attributes = $this->getGroupAttributes();

            $this->_setFieldset($attributes, $fieldset, array('gallery'));

            $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
            if($is_seller){
                $fieldset->removeField('seller_customer_type');
                $fieldset->removeField('all_customers');
            }
            $seller_customer_type = $form->getElement('seller_customer_type');
            if ($seller_customer_type) {
                $seller_customer_type->setOnchange("setCustomertype(this)");
            }
            $all_customers = $form->getElement('all_customers');
            if ($all_customers) {
                $all_customers->setOnchange("changeCustomer(this)");
            }

            $seller_user_name = $form->getElement('seller_user_name');
            if ($seller_user_name) {
                $seller_user_name->setRenderer(
                        $this->getLayout()->createBlock('tribeseller/adminhtml_sellers_form_renderer_attribute_username')
                );
            }
            
             $seller_email = $form->getElement('seller_email');
            if ($seller_email) {
                $seller_email->setRenderer(
                        $this->getLayout()->createBlock('tribeseller/adminhtml_sellers_form_renderer_attribute_selleremail')
                );
            }
            
            // Add new attribute button if it is not an image tab
            if (!$form->getElement('media_gallery')) {
                $headerBar = $this->getLayout()->createBlock('tribeseller/adminhtml_sellers_edit_tab_attributes_create');
                $headerBar->getConfig()
                        ->setTabId('group_' . $group->getId())
                        ->setGroupId($group->getId())
                        ->setStoreId($this->getStoreId())
                        ->setAttributeSetId($this->getAttributeSetId())
                        ->setTypeId($this->getTypeId());
                $fieldset->setHeaderBar($headerBar->toHtml());
            }

//            if ($form->getElement('seller_meta_description')) {
//                $form->getElement('seller_meta_description')->setOnkeyup('checkMaxLength(this, 255);');
//            }

            $values = Mage::registry('sellerprofile_data')->getData();

            // Set default attribute values for new product
            if (!Mage::registry('sellerprofile_data')->getId()) {
                foreach ($attributes as $attribute) {
                    if (!isset($values[$attribute->getAttributeCode()])) {
                        $values[$attribute->getAttributeCode()] = $attribute->getDefaultValue();
                    }
                }
            }

            if (Mage::registry('sellerprofile_data')->hasLockedAttributes()) {
                foreach (Mage::registry('product')->getLockedAttributes() as $attribute) {
                    $element = $form->getElement($attribute);
                    if ($element) {
                        $element->setReadonly(true, true);
                    }
                }
            }
            $form->addValues($values);
            $form->setFieldNameSuffix('seller');
            if (strcasecmp($group->getAttributeGroupName(), 'Profile') === 0 && Mage::registry('sellerprofile_data')->getId()) {
                $form->addType('script', 'Fedobe_Tribeseller_Block_Adminhtml_Script');
                $form->addField("", 'script', array());
            }
            Mage::dispatchEvent('adminhtml_tribe_product_edit_prepare_form', array('form' => $form));
            $this->setForm($form);
        }
    }

    /**
     * Retrieve additional element types
     *
     * @return array
     */
    protected function _getAdditionalElementTypes() {
        $result = array(
            'tribemap' => 'Fedobe_Tribeseller_Block_Adminhtml_Sellers_Form_Element_Map',
            'tribecontent' => 'Fedobe_Tribeseller_Block_Adminhtml_Sellers_Form_Element_Content',
            'gallery' => 'Fedobe_Tribeseller_Block_Adminhtml_Sellers_Form_Element_Gallery',
            'image' => 'Fedobe_Tribeseller_Block_Adminhtml_Sellers_Form_Element_Image',
            'boolean' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_boolean'),
            'textarea' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_helper_form_wysiwyg'),
            'price' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_price'),
            'weight' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_weight'),
        );

        $response = new Varien_Object();
        $response->setTypes(array());
        Mage::dispatchEvent('adminhtml_tribe_sellers_edit_element_types', array('response' => $response));

        foreach ($response->getTypes() as $typeName => $typeClass) {
            $result[$typeName] = $typeClass;
        }
        return $result;
    }

}