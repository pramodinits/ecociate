<?php

class Fedobe_Tribeseller_Block_Adminhtml_Sellers_Reviews extends Fedobe_Tribeseller_Block_Adminhtml_Review_Grid//Mage_Adminhtml_Block_Review_Grid
{
    /**
     * Hide grid mass action elements
     *
     * @return Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Reviews
     */
    public function __construct() {
        parent::__construct();
        $this->setId('reviews_grid');
        $this->setUseAjax(true);
    }
    
    
    protected function _prepareMassaction()
    {
        return $this;
    }

    /**
     * Determine ajax url for grid refresh
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/reviews', array('_current' => true));
    }
}
