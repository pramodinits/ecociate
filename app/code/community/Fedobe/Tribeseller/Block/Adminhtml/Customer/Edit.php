<?php

class Fedobe_Tribeseller_Block_Adminhtml_Customer_Edit extends Mage_Adminhtml_Block_Customer_Edit {

    public function __construct() {
        parent::__construct();
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
            $this->_removeButton('delete');
        }
    }
}