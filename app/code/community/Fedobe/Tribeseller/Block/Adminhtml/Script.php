<?php

class Fedobe_Tribeseller_Block_Adminhtml_Script extends Varien_Data_Form_Element_Abstract {

    protected $_element;

    public function getElementHtml() {
        $url = Mage::helper('tribeseller')->getvalidateurlkeyaction();
        $id = Mage::app()->getRequest()->getParam('id')?Mage::app()->getRequest()->getParam('id'):0;
        $html = '<script type="text/javascript">'
                . 'onStoreNameChanged("'.$id.'","seller_user_name","' . $url . '");'
                . '</script>';
        return $html;
    }

}
?>

