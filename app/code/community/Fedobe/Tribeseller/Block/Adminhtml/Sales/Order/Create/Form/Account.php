<?php

class Fedobe_Tribeseller_Block_Adminhtml_Sales_Order_Create_Form_Account extends Mage_Adminhtml_Block_Sales_Order_Create_Form_Account {

    /**
     * Prepare Form and add elements to form
     *
     * @return Mage_Adminhtml_Block_Sales_Order_Create_Form_Account
     */
    protected function _prepareForm() {
        parent::_prepareForm();
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller){
            $this->_form->getElement('group_id')->setData('after_element_html',"<script type='text/javascript'>$('group_id').up(1).hide();</script>");
}
        return $this;
    }

}