<?php

class Fedobe_Tribeseller_Block_Adminhtml_Sales_Invoice_Grid extends Mage_Adminhtml_Block_Sales_Invoice_Grid {

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass())->setSellerFilter();
        $this->setCollection($collection);
        return $this;
    }
}