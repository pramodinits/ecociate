<?php

class Fedobe_Tribeseller_Block_Adminhtml_Sales_Order_Totalbar extends Mage_Adminhtml_Block_Sales_Order_Totalbar {

    protected function _beforeToHtml() {
        $totals = array();
        if (!$this->getParentBlock()) {
            Mage::throwException(Mage::helper('adminhtml')->__('Invalid parent block for this block.'));
        }
        $this->setOrder($this->getParentBlock()->getOrder());
        $this->setSource($this->getParentBlock()->getSource());
        $this->setCurrency($this->getParentBlock()->getOrder()->getOrderCurrency());

        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
            $items = Mage::helper('tribeseller')->getSellerItems($this->getParentBlock()->getOrder()->getId());
            if (!empty($items)) {
                $totals = array();
                foreach ($items as $key => $item) {
                    foreach ($item as $k => $v) {
                        if (is_numeric($v) && !in_array($k, Mage::helper('tribeseller')->itemIgnoreCol())) {
                            $totals[$k] = $totals[$k] + $v;
                        }
                    }
                }
            }
        }
        foreach ($this->getParentBlock()->getOrderTotalbarData() as $k => $v) {
            if ($k == 1 && $totals['amount_refunded']) {
                $v[1] = $this->displayPrices($totals['base_amount_refunded'],$totals['base_amount_refunded']);
                $v[2] = 1;
            }
            if ($k == 4 && $totals['row_total']) {
                $v[1] = $this->displayPrices($totals['base_row_total'],$totals['row_total']);
            }
            $this->addTotal($v[0], $v[1], $v[2]);
        }
        return $this;
    }
}