<?php

class Fedobe_Tribeseller_Block_Adminhtml_Sales_Order_Totals extends Mage_Adminhtml_Block_Sales_Order_Totals {

    public function _initTotals() {
        parent::_initTotals();

        $this->_totals['paid'] = new Varien_Object(array(
            'code' => 'paid',
            'strong' => true,
            'value' => $this->getSource()->getTotalPaid(),
            'base_value' => $this->getSource()->getBaseTotalPaid(),
            'label' => $this->helper('sales')->__('Total Paid'),
            'area' => 'footer'
        ));
        $this->_totals['refunded'] = new Varien_Object(array(
            'code' => 'refunded',
            'strong' => true,
            'value' => $this->getSource()->getTotalRefunded(),
            'base_value' => $this->getSource()->getBaseTotalRefunded(),
            'label' => $this->helper('sales')->__('Total Refunded'),
            'area' => 'footer'
        ));
        $this->_totals['due'] = new Varien_Object(array(
            'code' => 'due',
            'strong' => true,
            'value' => $this->getSource()->getTotalDue(),
            'base_value' => $this->getSource()->getBaseTotalDue(),
            'label' => $this->helper('sales')->__('Total Due'),
            'area' => 'footer'
        ));
        $this->showCustomTotals();
        return $this;
    }

    public function showCustomTotals() {
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
            $items = Mage::helper('tribeseller')->getSellerItems($this->getSource()->getId());
            if (!empty($items)) {
                $totals = array();
                foreach ($items as $key => $item) {
                    foreach ($item as $k => $v) {
                        if (is_numeric($v) && !in_array($k, Mage::helper('tribeseller')->itemIgnoreCol())) {
                            $totals[$k] = $totals[$k] + $v;
                        }
                    }
                }
                foreach ($this->_totals as $key => $obj) {
                    if ($key == 'subtotal' || $key == 'grand_total') {
                        $this->_totals[$key]->setValue($totals['row_total']);
                        $this->_totals[$key]->setBaseValue($totals['base_row_total']);
                    } elseif ($key == 'paid') {
                        $this->_totals[$key]->setValue($totals['row_invoiced']);
                        $this->_totals[$key]->setBaseValue($totals['base_row_invoiced']);
                    } elseif ($key == 'refunded') {
                        $this->_totals[$key]->setValue($totals['amount_refunded']);
                        $this->_totals[$key]->setBaseValue($totals['base_amount_refunded']);
                    } elseif ($key == 'due') {
                        $this->_totals[$key]->setValue($totals['row_total'] - $totals['row_invoiced']);
                        $this->_totals[$key]->setBaseValue($totals['base_row_total'] - $totals['base_row_invoiced']);
                    }
                }
            }
        }
    }
}