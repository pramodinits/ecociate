<?php

class Fedobe_Tribeseller_Block_Adminhtml_Sales_Order_Create_Billing_Address extends Mage_Adminhtml_Block_Sales_Order_Create_Billing_Address {

    /**
     * Prepare Form and add elements to form
     *
     * @return Mage_Adminhtml_Block_Sales_Order_Create_Billing_Address
     */
    protected function _prepareForm() {
        parent::_prepareForm();
        $this->_form->getElement('prefix')->setNoDisplay(true);
        $this->_form->getElement('suffix')->setNoDisplay(true);
        $this->_form->getElement('middlename')->setNoDisplay(true);
        $this->_form->getElement('company')->setNoDisplay(true);
        $this->_form->getElement('fax')->setNoDisplay(true);
        $this->_form->getElement('vat_id')->setNoDisplay(true);
        return $this;
    }

}