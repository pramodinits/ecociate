<?php
class Fedobe_Tribeseller_Block_Adminhtml_Sales_Order_Create_Sidebar extends Mage_Adminhtml_Block_Sales_Order_Create_Abstract {

    protected function _prepareLayout() {
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
            $this->unsetChild('top_button');
            $this->unsetChild('bottom_button');
        } else {
            if ($this->getCustomerId()) {
                $button = $this->getLayout()->createBlock('adminhtml/widget_button')->setData(array(
                    'label' => Mage::helper('sales')->__('Update Changes'),
                    'onclick' => 'order.sidebarApplyChanges()',
                    'before_html' => '<div class="sub-btn-set">',
                    'after_html' => '</div>'
                ));
                $this->setChild('top_button', $button);
            }

            if ($this->getCustomerId()) {
                $button = clone $button;
                $button->unsId();
                $this->setChild('bottom_button', $button);
            }
            
        }
        return parent::_prepareLayout();
    }

    public function canDisplay($child) {
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
            return false;
        } else {
            if (method_exists($child, 'canDisplay')) {
                return $child->canDisplay();
            }
            return true;
        }
    }

}