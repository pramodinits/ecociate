<?php
class Fedobe_Tribeseller_Block_Adminhtml_Sales_Order_View_Tabs extends Mage_Adminhtml_Block_Sales_Order_View_Tabs
{
    protected function _beforeToHtml()
    {
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        if ($is_seller) {
          $this->removeTab('order_creditmemos');
        }
        return parent::_beforeToHtml();
    }

}