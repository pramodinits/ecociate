<?php

class Fedobe_Tribeseller_Block_Adminhtml_Sales_Order_Invoice_Totals extends Mage_Adminhtml_Block_Sales_Order_Invoice_Totals {

    public function getInvoice() {
        if ($this->_invoice === null) {
            if ($this->hasData('invoice')) {
                $this->_invoice = $this->_getData('invoice');
            } elseif (Mage::registry('current_invoice')) {
                $this->_invoice = Mage::registry('current_invoice');
            } elseif ($this->getParentBlock()->getInvoice()) {
                $this->_invoice = $this->getParentBlock()->getInvoice();
            }
            $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
            if ($is_seller) {
                $order_id = ($this->getRequest()->getParam('order_id')) ? $this->getRequest()->getParam('order_id') : $order = $invoice->getOrder()->getId();
                $items = Mage::helper('tribeseller')->getSellerItems($order_id);
                if (!empty($items)) {
                    $totals = array();
                    foreach ($items as $key => $item) {
                        foreach ($item as $k => $v) {
                            if (is_numeric($v) && !in_array($k, Mage::helper('tribeseller')->itemIgnoreCol())) {
                                $totals[$k] = $totals[$k] + $v;
                            }
                        }
                    }
                    $itemstotal = $totals['row_total'];
                    $itemsbasetotal = $totals['base_row_total'];

                    $amttormv = $this->getSource()->getSubtotal() - $itemstotal;
                    $bseamttormv = $this->getSource()->getBaseSubtotal() - $itemsbasetotal;

                    $tot_amttormv = $this->getSource()->getGrandTotal() - $amttormv;
                    $base_tot_amttormv = $this->getSource()->getBaseGrandTotal() - $itemsbasetotal;


                    $this->getSource()->setSubtotal($itemstotal);
                    $this->getSource()->setBaseSubtotal($itemsbasetotal);

                    $this->getSource()->setGrandTotal($tot_amttormv);
                    $this->getSource()->setBaseGrandTotal($base_tot_amttormv);
                }
            }
        }
        return $this->_invoice;
    }

}
