<?php

class Fedobe_Tribeseller_Block_Comment extends Mage_Core_Block_Template {
    private $_collection = null;
    
    public function __construct() {
        parent::__construct();
        $this->setTemplate('fedobe/tribe/seller/comment.phtml');
    }
    public function getComments(){
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $readConnection = $resource->getConnection('core_read');
        $data = $readConnection->fetchAll('select * FROM signpledge WHERE pledge_comment IS NOT NULL order by created_at desc');
        
        return $data;
    }
}