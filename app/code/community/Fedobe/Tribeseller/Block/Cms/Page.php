<?php

class Fedobe_Tribeseller_Block_Cms_Page extends Mage_Cms_Block_Page {

    protected function _toHtml() {
        if (Mage::registry('current_seller')) {
            $seller = Mage::registry('current_seller');
            $helper = Mage::helper('cms');
            $processor = $helper->getPageTemplateProcessor();
            $handles = Mage::app()->getLayout()->getUpdate()->getHandles();
            if(in_array('tribeseller_sellerpage_home', $handles)){
                $html = $processor->filter($seller->getSellerhome());
            }elseif(in_array('tribeseller_sellerpage_about', $handles)){
                $html = $processor->filter($seller->getSellerAbout());
            }  elseif (in_array('tribeseller_sellerpage_policy', $handles)) {
                $html = $processor->filter($seller->getSellerPolicies());
            }
            return $html;
        } else {
            return parent::_toHtml();
        }
    }

}