<?php

class Fedobe_Tribeseller_Block_Sales_Order_Totals extends Mage_Sales_Block_Order_Totals {

    /**
     * Get order object
     *
     * @return Mage_Sales_Model_Order
     */
    /*public function getOrder() {echo "dsd";
        if ($this->_order === null) {
            if ($this->hasData('order')) {
                $this->_order = $this->_getData('order');
            } elseif (Mage::registry('current_order')) {
                $this->_order = Mage::registry('current_order');
            } elseif ($this->getParentBlock()->getOrder()) {
                $this->_order = $this->getParentBlock()->getOrder();
            }
            $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
            if ($is_seller) {
                $items = Mage::helper('tribeseller')->getSellerItems($this->getSource()->getId());
                if (!empty($items)) {
                    $totals = array();
                    foreach ($items as $key => $item) {
                        foreach ($item as $k => $v) {
                            if (is_numeric($v) && !in_array($k, Mage::helper('tribeseller')->itemIgnoreCol())) {
                                $totals[$k] = $totals[$k] + $v;
                            }
                        }
                    }
                    $itemstotal = $totals['row_total'];
                    $itemsbasetotal = $totals['base_row_total'];
                    
                    $amttormv = $this->getSource()->getSubtotal()-$itemstotal;
                    $bseamttormv = $this->getSource()->getBaseSubtotal()-$itemsbasetotal;
                    
                    $tot_amttormv = $this->getSource()->getGrandTotal()-$amttormv;
                    $base_tot_amttormv = $this->getSource()->getBaseGrandTotal()-$itemsbasetotal;
                    
                    
                    $this->getSource()->setSubtotal($itemstotal);
                    $this->getSource()->setBaseSubtotal($itemsbasetotal);
                    
                    $this->getSource()->setGrandTotal($tot_amttormv);
                    $this->getSource()->setBaseGrandTotal($base_tot_amttormv);
                    
                    echo "<pre>";
                    print_r($totals);
                    echo "</pre>";
                }
            }
        }
        return $this->_order;
    }*/
    
   
     public function getTotals($area=null)
    {
        $data = $this->getData();
        $seller_id_1=NULL;
        $seller_id_1 = Mage::getSingleton('customer/session')->getEmailtribesellerid();
        $totals = array();
        if ($area === null) {
            $this->is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
            if($this->is_seller || $seller_id_1){
              if($seller_id_1){
                $seller_id = $seller_id_1;
              }else{
                $admin_user_session = Mage::getSingleton('admin/session');
                if ($admin_user_session->getUser()) {
                   $seller_id = Mage::helper('tribeseller')->getSellerIdFromadminUser();//$admin_user_session->getUser()->getUserId();
                }
            }
             $order = $this->getOrder();//echo $order->getId().'--'.$seller_id."<br>";
            $BaseGrandTotal = Mage::helper('tribeseller')->getSellerBaseGrandTotal($order->getId(),$seller_id);
            $sellerGrandTotal = Mage::helper('tribeseller')->getSellerGrandTotal($order->getId(),$seller_id);//echo $sellerGrandTotal;exit;
            $this->_totals['subtotal']['value']=$BaseGrandTotal;
            $this->_totals['grand_total']['value']=$sellerGrandTotal;
            }
            $totals = $this->_totals;
        } else {
            $area = (string)$area;
            foreach ($this->_totals as $total) {
                $totalArea = (string) $total->getArea();
                if ($totalArea == $area) {
                    $totals[] = $total;
                }
            }
        }
        return $totals;
    }
}
