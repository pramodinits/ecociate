<?php

/**
 * @category    Fedobe
 * @package     Fedobe_Landingpage
 */
class Fedobe_Tribeseller_Block_View_Product_List extends Mage_Catalog_Block_Product_List {

    protected function _getProductCollection() {

        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            $productCollection = $layer->getProductCollection();
            $this->_productCollection = $productCollection;
        }
        return $this->_productCollection;
    }

}
