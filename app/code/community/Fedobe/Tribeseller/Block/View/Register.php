<?php

class Fedobe_Tribeseller_Block_View_Register extends Mage_Core_Block_Template {
    
    protected function _construct(){
        parent::_construct();
    }
    
    public function getattributes(){
        $attributes = array();
        //if(Mage::helper('tribecore')->is_seller()){
            
        $allowedattributes = $this->allowedattributes();  //print_r($allowedattributes);
        $attribute_set_id = Mage::helper('tribeseller')->getAttributeSetId();
        $groupCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')
                ->setAttributeSetFilter($attribute_set_id)
                ->addFieldToFilter('attribute_group_name', array('in' => array('Profile','Content')))
                ->setSortOrder()
                ->load();
        $attributes = array();
        foreach ($groupCollection as $group) {
            $attributeCollection = Mage::getResourceModel('eav/entity_attribute_collection')
                    ->setAttributeGroupFilter($group->getId());
            foreach ($attributeCollection as $attribute) {//print_r($attributeCollection);
                if(in_array($attribute->getAttributeCode(),$allowedattributes)){
                    if ($attribute->getFrontendInput() == 'textarea') {
                        $attr_id = $attribute->getAttributeId();
                        $addtionaldata = Mage::getModel('arccore/attributedata')->load($attr_id);
                        $attributes[$group->getAttributeGroupName()][] = array_merge($attribute->getData(), $addtionaldata->getData());
                    } else {
                        $attributes[$group->getAttributeGroupName()][] = $attribute->getData();
                    }
                }
            }
      //  }
        }
        return $attributes;
    }
    
    public function allowedattributes(){
        return array('seller_first_name','seller_last_name','seller_user_name','seller_email','seller_store_name');
    }
    
    
}
?>