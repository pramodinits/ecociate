<?php

class Fedobe_Tribeseller_Block_View_Tabs extends Mage_Core_Block_Template {

    protected function _construct() {
        parent::_construct();
    }

    public function getCurrentSeller() {
        if (!$this->hasData('current_seller')) {
            $this->setData('current_seller', Mage::registry('current_seller'));
        }
        return $this->getData('current_seller');
    }

    public function setCurentSeller($id){
        $seller = Mage::getModel('tribeseller/seller')->load($id);
        $this->setData('current_seller',$seller);
        return $this;
    }
}
