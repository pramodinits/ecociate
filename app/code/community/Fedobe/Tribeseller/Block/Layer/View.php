<?php

/**
 * @category    Fedobe
 * @package     Fedobe_Landingpage
 */
class Fedobe_Tribeseller_Block_Layer_View extends Mage_Catalog_Block_Layer_View {

    /**
     * Returns the layer object for the landingpage model
     *
     * @return Fedobe_Landingpage_Model_Layer
     */
    public function getLayer() {
        return Mage::getSingleton('tribeseller/layer');
    }

    /**
     * Ensure the default Magento blocks are used
     *
     * @return $this
     */
    protected function _initBlocks() {
        $this->_stateBlockName = 'Fedobe_Tribeseller_Block_Layer_State';
//        $this->_stateBlockName              = 'catalog/layer_state';
        $this->_categoryBlockName           = 'catalog/layer_filter_category';
        $this->_attributeFilterBlockName    = 'catalog/layer_filter_attribute';
        $this->_priceFilterBlockName        = 'catalog/layer_filter_price';
        $this->_decimalFilterBlockName      = 'catalog/layer_filter_decimal';
        return $this;
    }
    
    public function getCatFilter(){
        return $this->getChild('category_filter');
    }


}