<?php

/**
 * @category    Fedobe
 * @package     Fedobe_Landingpage
 */
class Fedobe_Tribeseller_Block_Layer_Filter_Attribute extends Mage_Catalog_Block_Layer_Filter_Attribute {

    public function __construct()
    {
        parent::__construct();
        $this->_filterModelName = 'tribeseller/layer_filter_attribute';
    }

}