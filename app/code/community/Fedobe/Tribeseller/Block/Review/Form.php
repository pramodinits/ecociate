<?php

class Fedobe_Tribeseller_Block_Review_Form extends Mage_Core_Block_Template {

    public function __construct() {
        $customerSession = Mage::getSingleton('customer/session');

        parent::__construct();

        $data = Mage::getSingleton('review/session')->getFormData(true);
        $data = new Varien_Object($data);

        // add logged in customer name as nickname
        if (!$data->getNickname()) {
            $customer = $customerSession->getCustomer();
            if ($customer && $customer->getId()) {
                $data->setNickname($customer->getFirstname());
            }
        }

        $this->setAllowWriteReviewFlag($customerSession->isLoggedIn() || Mage::helper('review')->getIsGuestAllowToWrite());
//        $this->setAllowWriteReviewFlag($customerSession->isLoggedIn());
        if (!$this->getAllowWriteReviewFlag) {
            $this->setLoginLink(
                    Mage::getUrl('customer/account/login/', array(
                        Mage_Customer_Helper_Data::REFERER_QUERY_PARAM_NAME => Mage::helper('core')->urlEncode(
                                Mage::getUrl('*/*/*', array('_current' => true)) .
                                '#review-form')
                            )
                    )
            );
        }

        $this->setTemplate('review/form.phtml')
                ->assign('data', $data)
                ->assign('messages', Mage::getSingleton('review/session')->getMessages(true));
    }

    public function getSellerInfo() {
        if (($current_seller = Mage::registry('current_seller')) !== null) {
            return $current_seller;
        }
        $current_seller = Mage::getModel('tribeseller/seller')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load((int) $this->getRequest()->getParam('id', false));

        return $current_seller;
    }

    public function getAction() {
        $seller = $this->getSellerInfo();
        $sellerId = $seller->getID();
        return Mage::getUrl('tribeseller/sellerpage/postreview', array('id' => $sellerId));
    }

    public function getRatings() {
        $ratingCollection = Mage::getModel('rating/rating')
                ->getResourceCollection()
                ->addEntityFilter('product')
                ->setPositionOrder()
                ->addRatingPerStoreName(Mage::app()->getStore()->getId())
                ->setStoreFilter(Mage::app()->getStore()->getId())
                ->load()
                ->addOptionToItems();
        return $ratingCollection;
    }

}