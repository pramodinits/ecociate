<?php

class Fedobe_Tribeseller_Block_Review_List extends Mage_Core_Block_Template {

    const ENTITY_SELLER_CODE = 'seller';

    private $_collection = null;
    private $_entity_id;
    private $_limit;
    private $_page;
    private $_sellerId;
    public $_reviewcollection = null;
    public $_profileinfo = null;

    public function __construct() {
        parent::__construct();
        $this->setTemplate('fedobe/tribe/seller/review/list.phtml');
        $review = Mage::getModel('review/review');
        $this->_entity_id = $review->getEntityIdByCode(self::ENTITY_SELLER_CODE);
        $this->_limit = 10;
        $this->_page = 1;
        $this->_sellerId = (Mage::registry('current_seller')) ? Mage::registry('current_seller')->getId() : 0;
    }

    public function setPage($page) {
        $this->_page = $page;
        return $this;
    }
    
    public function setSeller($sellerid) {
        $this->_sellerId = $sellerid;
        return $this;
    }

    public function getReviewCollection($p = 1) {
        $model = Mage::getModel('review/review');
        if (!$this->_collection) {
            $offset = ($this->_page - 1) * $this->_limit;
            $this->_collection = $model->getCollection()
                    ->addFieldToFilter('entity_id', $this->_entity_id)
                    ->addFieldToFilter('entity_pk_value', $this->_sellerId)
                    ->addFieldToFilter('status_id', Mage_Review_Model_Review::STATUS_APPROVED);
            $this->_collection->getSelect()->order('created_at DESC')->limit($this->_limit, $offset);
        }
        return $this->_collection;
    }
     public function getCurrentProfileInfo() {
      $loggedindata= Mage::getSingleton('customer/session')->getCustomer()->getData();
      //getprofile
       $collection = Mage::getModel('arccore/arc')->getCollection()
                      ->addAttributeToSelect('*')
                     ->addAttributeTofilter('trcr_customer_list',$loggedindata['entity_id'])
                      ->addAttributeTofilter('trcr_profile_type', array('notnull' => true))
                     ->getFirstItem();
      return $collection;
    }
    public function setReviewCollectionInfo($reviewcollection,$info){
        $this->_reviewcollection = $reviewcollection;
        $this->_profileinfo = $info;
        return $this;
    }
     public function getsellerReviewCollection(){
        return $this->_reviewcollection;
    }
    public function getsellerReviewProfileInfo(){
        return $this->_profileinfo;
    }
}