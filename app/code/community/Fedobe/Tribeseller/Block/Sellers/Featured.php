<?php

class Fedobe_Tribeseller_Block_Sellers_Featured extends Mage_Core_Block_Template {

    protected $_productCollection;

    protected function _getProductCollection() {
        if (is_null($this->_productCollection)) {
            $maxsellercount = intval(Mage::getStoreConfig('tribecoresettings/general_settings/max_featured_seller_to_show'));
            $collection = Mage::getModel('tribeseller/seller')->getCollection();
            $collection->addAttributeToFilter('featured_seller', array('eq' => 1))
                    ->setPageSize($maxsellercount)
                    ->setCurPage(1)
                    ->addAttributeToFilter('seller_status', array('eq' => 2));

//                echo $collection->getSelect()->__toString();exit;
//                $collection->getSelect()->order('entity_id DESC');

            //Here to add distance filter
            $this->_productCollection = $this->pindistancefilter($collection);
        }
        return $this->_productCollection;
    }

    public function pindistancefilter($collection) {
        $latcol = 'seller_geolat';
        $lngcol = 'seller_geolng';
        $cols = "entity_id";
        $radius = Mage::helper('tribeseller')->getCurrentradius();
        $latlnginfo = Mage::helper('tribeseller')->getCurrentLatLng();
        $lat = $latlnginfo['lat'];
        $lng = $latlnginfo['lng'];
        if ($lat && $lng) {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $table = $resource->getTableName('tribeseller/seller_entity');
            $sql = Mage::helper('tribeseller')->haversine_sql($table, $latcol, $lngcol, $lat, $lng, $radius, $cols);
            $sqlattrQuery = $readConnection->query($sql);
            $sellersid = array();
            while ($row = $sqlattrQuery->fetch()) {
                $sellersid[] = $row['entity_id'];
            }
            if (!empty($sellersid)) {
                $collection->addAttributeToFilter('entity_id', array('in' => $sellersid));
            } else {
                $collection->addAttributeToFilter('entity_id', array('eq' => -1));
            }
        }else{
            $collection->addAttributeToFilter('entity_id', array('eq' => -1));
        }
        return $collection;
    }

    public function getLoadedProductCollection() {
        return $this->_getProductCollection();
    }

}