<?php

/**
 * @category    Fedobe
 * @package     Fedobe_Landingpage
 */
class Fedobe_Tribeseller_Block_Sellers_View extends Mage_Core_Block_Template {

    /**
     * Default toolbar block name
     *
     * @var string
     */
    protected $_defaultToolbarBlock = 'tribeseller/sellers_list_toolbar';

    /**
     * Product Collection
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected $_productCollection;

    /**
     * Adds the META information to the resulting page
     */
    protected function _prepareLayout() {
        parent::_prepareLayout();
        return $this;
    }

    protected function _getProductCollection() {
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            $collection = $layer->getSellerCollection();
            //Here to add distance filter
            $this->_productCollection = $this->preapreFilterCollection($collection);
        }
        return $this->_productCollection;
    }

    public function preapreFilterCollection($collection) {
        $toolbar = $this->getToolbarBlock();
        $order = Mage::app()->getRequest()->getParam('order');
        $order = ($order) ? $order : $toolbar->getCurrentOrder();
        $dir = Mage::app()->getRequest()->getParam('dir');
        $dir = ($dir) ? $dir : $toolbar->getCurrentDirection();
        
        $sellersids = $this->pindistancefilter();
        if (!empty($sellersids)) {
            $collection->addAttributeToFilter('entity_id', array('in' => $sellersids));
        } else {
            $collection->addAttributeToFilter('entity_id', array('eq' => -1));
        }  
        
        if($order == "distance"){
            if(strtolower($dir) =='desc'){
               $sellersids = array_reverse($sellersids);
            }
            $ids = implode(',', $sellersids);
            $collection->getSelect()->order("FIELD(e.entity_id, $ids)");
        }else{
            $collection->getSelect()->order("$order $dir");
        }
        return $collection;
    }

    public function pindistancefilter($collection) {
        $latcol = 'seller_geolat';
        $lngcol = 'seller_geolng';
        $cols = "entity_id";
        $sellersid = array();
        $radius = Mage::helper('tribeseller')->getCurrentradius();
        $latlnginfo = Mage::helper('tribeseller')->getCurrentLatLng();
        $lat = $latlnginfo['lat'];
        $lng = $latlnginfo['lng'];
        if ($lat && $lng) {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $table = $resource->getTableName('tribeseller/seller_entity');
            $sql = Mage::helper('tribeseller')->haversine_sql($table, $latcol, $lngcol, $lat, $lng, $radius, $cols);
            $sqlattrQuery = $readConnection->query($sql);
            while ($row = $sqlattrQuery->fetch()) {
                $sellersid[] = $row['entity_id'];
            }
        }
        return $sellersid;
    }

    public function getLayer() {
        $layer = Mage::registry('current_layer');
        if ($layer) {
            return $layer;
        }
        return Mage::getSingleton('tribeseller/seller_layer');
    }

    public function getSellersCollection() {
        $sellers_collection = Mage::getModel('tribeseller/seller')->getCollection()
                ->addAttributeToSelect(array('seller_profile_pic', 'seller_url', 'seller_pin', 'seller_street_address_one', 'seller_city', 'seller_state'));
        $customer_postcode = $this->getCustomerPostCode();
        $seller_details = array();
        $distance = array();
        foreach ($sellers_collection as $key => $seller_info) {
            $seller_pin = $seller_info->getSellerPin();
            if (($dist = Mage::helper('tribeseller')->getDistance($seller_pin, $customer_postcode)) > 10)
                continue;
            $id = $seller_info->getId();
            $seller_details[$id] = $seller_info->getData();
            $distance[$id] = $dist;
        }
        return array('seller_info' => $seller_details, 'distance' => $distance);
    }

    public function getCategoryList() {
        $resource = Mage::getSingleton('core/resource');
        $category_table = $resource->getTableName('catalog/category_product');
        $product_list = array();
        $sellers_product_list = Mage::getModel('tribeseller/seller')->getCollection()
                ->addAttributeToSelect("seller_pin");
        $customer_postcode = $this->getCustomerPostCode();
        foreach ($sellers_product_list as $seller_dtls) {

            $seller_pincode = $seller_dtls->getSellerPin();
            if ($customer_postcode) {
                if ($dist = Mage::helper('tribeseller')->getDistance($seller_pincode, $customer_postcode) > 10)
                    continue;
            }
            $product_list[] = $seller_dtls->getProductsData();
        }
        $product_list = trim(implode(',', $product_list), ",");
        if ($product_list) {
            $readConnection = $resource->getConnection("core_read");
            $result = $readConnection->fetchAll("SELECT category_id,count(*) cnt FROM $category_table WHERE product_id IN($product_list) GROUP BY category_id;");
            $result = array_column($result, 'cnt', 'category_id');
            $parent_category_details = array();
            foreach ($result as $category_id => $count) {
                $category = Mage::getModel('catalog/category')->load($category_id);
                $path = $category->getPath();
                $ids = explode('/', $path);
                if (isset($ids[2])) {
                    $parent_category_details[$ids[2]] = isset($parent_category_details[$ids[2]]) ? $parent_category_details[$ids[2]] + $count : $count;
                }
            }
            return $parent_category_details;
        }
    }

    public function getCustomerPostCode() {
        if (Mage::getSingleton("core/session")->getData('tribe_pin')) {
            return Mage::getSingleton("core/session")->getData('tribe_pin');
        }
        return false;
    }

    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     */
    protected function _beforeToHtml() {
        $toolbar = $this->getToolbarBlock();

        // called prepare sortable parameters
        $collection = $this->_getProductCollection();

        // use sortable parameters
        if ($orders = $this->getAvailableOrders()) {
            $toolbar->setAvailableOrders($orders);
        }
        if ($sort = Mage::app()->getRequest()->getParam('order')) {
            $toolbar->setDefaultOrder($sort);
        }
        if ($dir = Mage::app()->getRequest()->getParam('dir')) {
            $toolbar->setDefaultDirection($dir);
        }
        if ($modes = Mage::app()->getRequest()->getParam('mode')) {
            $toolbar->setModes($modes);
        }

        // set collection to toolbar and apply sort
        $toolbar->setCollection($collection);

        $this->setChild('toolbar', $toolbar);


        $this->_getProductCollection()->load();

        return parent::_beforeToHtml();
    }

    /**
     * Retrieve Toolbar block
     *
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function getToolbarBlock() {
        if ($blockName = $this->getToolbarBlockName()) {
            if ($block = $this->getLayout()->getBlock($blockName)) {
                return $block;
            }
        }
        $block = $this->getLayout()->createBlock($this->_defaultToolbarBlock, microtime());
        return $block;
    }

    /**
     * Retrieve current view mode
     *
     * @return string
     */
    public function getMode() {
        return $this->getChild('toolbar')->getCurrentMode();
    }

    /**
     * Retrieve list toolbar HTML
     *
     * @return string
     */
    public function getToolbarHtml() {
        return $this->getChildHtml('toolbar');
    }

    public function setCollection($collection) {
        $this->_productCollection = $collection;
        return $this;
    }

    public function addAttribute($code) {
        $this->_getProductCollection()->addAttributeToSelect($code);
        return $this;
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getLoadedProductCollection() {
        return $this->_getProductCollection();
    }

}
