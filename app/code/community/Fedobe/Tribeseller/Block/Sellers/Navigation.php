<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog navigation
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Fedobe_Tribeseller_Block_Sellers_Navigation extends Mage_Catalog_Block_Layer_View {

    /**
     * Get layer object
     *
     * @return Mage_Catalog_Model_Layer
     */
    public function getLayer() {
        return Mage::getSingleton('tribeseller/seller_layer');
    }

    /**
     * Ensure the default Magento blocks are used
     *
     * @return $this
     */
    protected function _initBlocks() {
        $this->_stateBlockName = 'Mage_Catalog_Block_Layer_State';
        $this->_attributeFilterBlockName = 'Mage_Catalog_Block_Layer_Filter_Attribute';
         
        return $this;
    }

    protected function _prepareLayout() {
        $stateBlock = $this->getLayout()->createBlock($this->_stateBlockName)
                ->setLayer($this->getLayer());


        $this->setChild('layer_state', $stateBlock);
        $filterableAttributes = $this->_getFilterableAttributes();
        foreach ($filterableAttributes as $attribute) {
            $filterBlockName = $this->_attributeFilterBlockName;
            $this->setChild($attribute->getAttributeCode() . '_filter', $this->getLayout()->createBlock($filterBlockName)
                            ->setLayer($this->getLayer())
                            ->setAttributeModel($attribute)
                            ->init());
        }

        $this->getLayer()->apply();
        
        return $this;
    }

    protected function _getFilterableAttributes() {
        $attributes = $this->getData('_filterable_attributes');
        if (is_null($attributes)) {
            $attributes = $this->getFilterableAttributes();
            $this->setData('_filterable_attributes', $attributes);
        }

        return $attributes;
    }

    public function getFilterableAttributes() {
        /** @var $collection Mage_Catalog_Model_Resource_Product_Attribute_Collection */
        $collection = Mage::getResourceModel('tribeseller/attribute_collection');

        $collection
                ->setItemObjectClass('catalog/resource_eav_attribute')
                ->addStoreLabel(Mage::app()->getStore()->getId())
                ->setOrder('position', 'ASC');

        $collection->addIsFilterableFilter(); // = $this->_prepareAttributeCollection($collection);
        

        $collection->load();

        return $collection;
    }

}
