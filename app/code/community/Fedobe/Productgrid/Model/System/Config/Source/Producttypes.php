<?php

class Fedobe_Productgrid_Model_System_Config_Source_Producttypes {

   public function toOptionArray() {
        return array(
            /*array('value' => 'newproduct', 'label' => 'New at Kg Products'),
            array('value' => 'supersaver', 'label' => 'Super Saver Products'),
            array('value' => 'same-day-shipping', 'label' => 'Same Day Shipping Products'),
            array('value' => 'best-seller', 'label' => 'Best Seller Products'),
            array('value' => 'in-sale', 'label' => 'Sale Products'),*/
            array('value' => '', 'label' => 'Select'),            
            array('value' => 'allproduct', 'label' => 'All Product'),
            array('value' => 'featured', 'label' => 'Featured Product'),
            array('value' => 'best-seller', 'label' => 'Best Seller'),
            array('value' => 'recently-added', 'label' => 'Recently Added'),
            );
    }

}