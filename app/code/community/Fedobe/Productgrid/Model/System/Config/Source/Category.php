<?php

class Fedobe_Productgrid_Model_System_Config_Source_Category
{
    public function toOptionArray($isMultiselect = true)
    {
        $collection = Mage::getModel('catalog/category')->getCollection()->addAttributeToSelect('*');

        $options = array(); 

        foreach($collection as $category){
            if($category->getName() != 'Root Catalog'){
                $options[] = array(
                   'label' => $category->getName(),
                   'value' => $category->getId()
                );
            }
        }
         if($isMultiSelect)
           {
        array_unshift($options, array('value'=>'same-day-shipping', 'label'=> Mage::helper('adminhtml')->__('Same Day Shipping Products')));
            }
        return $options;
    }
}
