<?php

class Fedobe_Productgrid_Model_System_Config_Source_Device {

   public function toOptionArray() {
        return array(
            array('value' => '3', 'label' => 'Both Mobile & Desktop'),
            array('value' => '2', 'label' => 'For Desktop Only'),
            array('value' => '1', 'label' => 'For Mobile Only'),

            );
    }

}