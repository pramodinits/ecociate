<?php

class Fedobe_Productgrid_Block_Productgrid extends Mage_Core_Block_Template 
{
    public function parameterValue()
    {   
	    $global_theme_color = Mage::getStoreConfig('global_options/general/global_color');        
       // $style_type = $this->getData('style_type');

        $multi_seller = $this->getData('multi_seller');// echo $multi_seller;exit;
        $product_type = $this->getData('product_type');
        $product_categories= $this->getData('product_categories');
        $product_attributes= $this->getData('product_attributes');
        $product_device= $this->getData('product_device');
        $columns= $this->getData('columns');
        $show_product_name = $this->getData('show_product_name');
        $show_product_sku = $this->getData('show_product_sku');
        $show_product_price = $this->getData('show_product_price');
        $show_product_addtocart = $this->getData('show_product_addtocart');
        $show_product_discount= $this->getData('show_product_discount');
        $productgrid_items = $this->getData('items');
        $heading = $this->getData('heading');   
         $style_type = Mage::helper('productgrid')->getGlobalStyle();     
        return array(
            "global_theme_color"=>$global_theme_color,
            "style_type"=>$style_type,
            "multi_seller"=>$multi_seller,
            "product_type"=>$product_type,
            "product_categories"=>$product_categories,
            "product_attributes"=>$product_attributes,
            "product_device"=>$product_device,
            "columns"=>$columns,
            "show_product_name"=>$show_product_name,
            "show_product_sku"=>$show_product_sku,
            "show_product_price"=>$show_product_price,
            "show_product_addtocart"=>$show_product_addtocart,
            "show_product_discount"=>$show_product_discount,
            "productgrid_items"=>$productgrid_items,
            "heading" => $heading,
            );
    } 
} 
