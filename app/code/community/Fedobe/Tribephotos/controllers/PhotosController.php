<?php require_once Mage::getBaseDir('code') . DS . 'community/Fedobe/Arccore/controllers/Adminhtml/ProfilesController.php';

class Fedobe_Tribephotos_PhotosController extends Fedobe_Arccore_Adminhtml_ProfilesController {

    protected $_entityTypeId;

    public function preDispatch() {
        //parent::preDispatch();
        $this->_entityTypeId = Mage::helper('arccore')->getEntityTypeId('arccore');
        $this->_attributesetId = Mage::helper('tribephotos')->getAttributeSetId();
        $bypassaction = array('save', 'editinfo', 'delete','loadmorephotoUrl');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }
     public function editinfoAction() {
        $status_id = $this->getRequest()->getPost('id');
        if ($status_id) {
            $width = Mage::getStoreConfig('photos/general/profile_icon_width');
            $height = Mage::getStoreConfig('photos/general/profile_icon_height');
            $arc = Mage::getModel('arccore/arc')
                    ->setStoreId($this->getRequest()->getParam('store', 0))
                    ->setAttributeSetId($this->_attributesetId)
                    ->setEntityTypeId($this->_entityTypeId);
            $data = $arc->load($status_id);
            $data_arr = $data->getData();
            $arrtcodes = $this->getImageArrtibutecodes();
            if (!empty($arrtcodes)) {
                foreach ($arrtcodes as $k => $val) {
                    if (isset($data_arr[$val])) {
                        $image = Mage::helper('arccore')->resizeImg($data_arr[$val], $width, $height);
                        $data_arr[$val] = "<img src='$image' width='$width' height='$height'/>";
                    }
                }
            }
            echo Mage::helper('core')->jsonEncode($data_arr);
        }
    }
      public function deleteAction() {
        $id = $this->getRequest()->getPost('id');
        if ($id) {
            $err = parent::deleteAction($id);
            echo $err;
        } else {
            echo 'error';
        }
    }
     private function getImageArrtibutecodes() {
        $attribute_set_id = Mage::helper('tribephotos')->getAttributeSetId();
        $arrtcodes = Mage::helper('arccore')->getImageAttributeCodes($attribute_set_id);
        return $arrtcodes;
    }
     public function loadmorephotoUrlAction() {
        $info = Mage::helper('tribephotos')->decrypt($this->getRequest()->getPost('info'));
        //echo "<pre>";print_r($info);exit;
        if ($info) {
            $entity_id = $info['id'];
            $photoscollection = Mage::helper('tribephotos')->getAllPhotos($entity_id);
            $photo_cnt = count($photoscollection);
            $data = array('ids' => array(), 'content' => '');
            if ($photo_cnt) {
                $info['profile_info']['user_log_id'] = $info['is_logged_in'];
                $info['profile_info']['logged_in_email'] = $info['logged_in_email'];
                $photo_info = $this->getPhotoContent($photoscollection, $info['profile_info']);
                $data = array_merge($data, $photo_info);
            }
            $result = array('count' => $photo_cnt, 'ids' => $data['ids'], 'content' => $data['content']);
            echo Mage::helper('core')->jsonEncode($result);
        }
    }
     private function getPhotoContent($photoscollection, $profile_info) {
        $photoids = array();
        foreach ($photoscollection as $k => $photo) {
            $photoids[] = 'photo_item_' . $photo->getId();
        }
        $content = Mage::app()->getLayout()->createBlock('tribephotos/photos')->setPhotosCollectionInfo($photoscollection, $profile_info)->setTemplate('fedobe/tribe/photos/ajaxphotos.phtml')->toHtml();
        $info = array('ids' => $photoids, 'content' => $content);
        return $info;
    }
}
?>