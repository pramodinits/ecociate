<?php

/**
 * Manage Photos Attribute controller file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribephotos
 * @author      Fedobe Magento Team
 */
require_once Mage::getBaseDir('code').DS.'community/Fedobe/Arccore/controllers/Adminhtml/ProfilesController.php';
class Fedobe_Tribephotos_Adminhtml_Tribe_PhotosController extends Fedobe_Arccore_Adminhtml_ProfilesController {

    protected $_entityTypeId;

    public function preDispatch() {
        parent::preDispatch();
        $this->_entityTypeId = Mage::helper('arccore')->getEntityTypeId('arccore');
    }

    /**
     * Init actions
     *
     */
    protected function _initAction() {
        // load layout, set active menu and breadcrumbs

        $this->_title($this->__('Tribe'))
                ->_title($this->__('Manage Photos'));
 
        if ($this->getRequest()->getParam('popup')) {
            $this->loadLayout('popup');
        } else {
            $this->loadLayout()
                    ->_setActiveMenu('fedobe/tribephotos_manage_photo');
        }
        return $this;
    }

    /**
     * Index action method
     */
    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }

    /**
     * Code to display the form
     * 
     * The main form container gets added to the content and the tabs block gets added to left.
     */
    public function newAction() {
        $attrsetid = $this->getRequest()->getParam('set');
        if ($attrsetid) {
            $this->_forward('edit');
        } else {
            $this->loadLayout();
            $this->renderLayout();
        }
    }

    /**
     * Edit Photo Attribute
     */
    public function editAction() {
         parent::editAction();
         $id = $this->getRequest()->getParam('id');
         $this->_initAction()
               ->_addBreadcrumb(
                        $id ? Mage::helper('tribephotos')->__('Edit') : Mage::helper('tribephotos')->__('New'), $id ? Mage::helper('tribephotos')->__('Edit') : Mage::helper('tribephotos')->__('New'));
        $arc = parent::loadProfile();
        if($id)
         $this->_title($this->__($arc->getTrrvName()));
        else
         $this->_title($this->__('New Photo'));
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->renderLayout();
    }

    public function validateAction() {
        parent::validateAction();
    }

    /**
     * Delete tribephotos attribute
     *
     * @return null
     */
    public function deleteAction() {
        if ($id = $this->getRequest()->getParam('id')) {
            $res = parent::deleteAction();
            
             if($res['success']){
                 Mage::getSingleton('adminhtml/session')->addSuccess($res['message']);
                 $this->_redirect('*/*/');
                 return;
             }else{
                  Mage::getSingleton('adminhtml/session')->addError($res['message']);
                  $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('attribute_id')));
                  return;
             }
        }
        Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('tribephotos')->__('Unable to find a photo to delete.'));
        $this->_redirect('*/*/');
    }

}
