<?php

/**
 * Customer attribute edit block
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribephoto
 * @author      Fedobe Magento Team
 */
class Fedobe_Tribephotos_Block_Adminhtml_Photos_Edit extends Fedobe_Arccore_Block_Adminhtml_Arc_Edit {

    public function __construct() {
        parent::__construct();
        $this->_controller = 'tribe_photos';
        $this->_blockGroup = 'tribephotos';
    }
}
