<?php
/**
 * Grid container file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribephotos
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Tribephotos_Block_Adminhtml_Photos extends Mage_Adminhtml_Block_Widget_Grid_Container
{       
  public function __construct()
  {
    /*both these variables tell magento the location of our Grid.php(grid block) file.
     * $this->_blockGroup.'/' . $this->_controller . '_grid'
     * i.e clarion_Customerattribute/adminhtml_customerattribute_grid
     * $_blockGroup - is your module's name.
     * $_controller - is the path to your grid block. 
     */
    
    $this->_controller = 'adminhtml_photos';
    $this->_blockGroup = 'tribephotos';
    
    $this->_headerText = Mage::helper('tribephotos')->__('Manage Photos');
    $this->_addButtonLabel = Mage::helper('tribephotos')->__('Add New Photo');
    parent::__construct();
    $this->_removeButton('add');
  }
}