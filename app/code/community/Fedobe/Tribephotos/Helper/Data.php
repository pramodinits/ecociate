<?php

class Fedobe_Tribephotos_Helper_Data extends Mage_Core_Helper_Abstract {
    const ATTRIBUTESET_NAME = 'Tribe Photos';

    public function getAttributeSetId() {
        $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                ->load(SELF::ATTRIBUTESET_NAME, 'attribute_set_name')
                ->getAttributeSetId();
              return $attributeSetId;

    }
   public function getProfileType() {
        return 'Photos';
    }
   public function getInfo($label,$info){
           $collection = $this->getAllPhotos($info['entity_id']);
           $count = $collection->getSize();
      // $collection = $this->getAllReview($info['entity_id']);
       return array('label' => $label, 'count' => $count,'url' => $this->getPhotoUrl($info['trcr_username']));
   } 
   public function getAllPhotos($id){
       $limit = Mage::getStoreConfig('photos/general/page_limit') ? Mage::getStoreConfig('photos/general/page_limit') : 10;
        $limit = intval($limit);
        $page = Mage::app()->getRequest()->getParam('page') ? Mage::app()->getRequest()->getParam('page') * $limit : 0;
         $collection = Mage::getModel('arccore/arc')->getCollection()->addAttributeToSelect('*');
          $collection->addAttributeTofilter('attribute_set_id',Mage::helper('tribephotos')->getAttributeSetId())
       ->addAttributeTofilter('trcr_username_list',$id)
       ->addAttributeTofilter('trcr_status',2);
       $collection->getSelect()->limit($limit, $page)
       ->order('created_at DESC');
       return $collection;
   } 
   public function getPhotoUrl($parenturl_key){
        $identifier = Mage::getStoreConfig('tribe_photos/edit/identifier') ?  Mage::getStoreConfig('tribe_photos/edit/identifier') : 'photos';
        return trim(Mage::getUrl("$parenturl_key/$identifier"), "/");
    }
     public function isActionallowed($info) {
        $allow = false;
        if ($info['user_log_id'] || (Mage::app()->isInstalled() && Mage::getSingleton('customer/session')->isLoggedIn())) {
            if ($info['trcr_account_type'] == 2) {
                $id = $info['user_log_id']  ? $info['user_log_id'] : Mage::getSingleton('customer/session')->getCustomer()->getId();
                if($info['trcr_customer_list'] == $id){
                    $allow = true;
                }
            } else {
                $email = $info['logged_in_email']  ? $info['logged_in_email'] : Mage::getSingleton('customer/session')->getCustomer()->getEmail();
                if(trim($info['trcr_email']) == $email){
                    $allow = true;
                }
            }
        }
        return $allow;
    }
     public function getcustomerdata($id){
       $customerData = Mage::getModel('customer/customer')->load($id)->getData();
       return $customerData;
   }
   public function getLoggedincustomerdata(){
       return Mage::getSingleton('customer/session')->getCustomer()->getData();
   }
    public function editInfoPhotourl() {
        return Mage::getUrl("tribephotos/photos/editinfo");
    }

    public function photodeleteUrl() {
        return Mage::getUrl("tribephotos/photos/delete");
    }
    public function loadmorephotoUrl() {
        return Mage::getUrl("tribephotos/photos/loadmorephotoUrl");
    }
     public function decrypt($data) {
        $encpre = $this->getencprefix();
        $encsuffix = $this->getencsuffix();
        $data = str_replace($encsuffix, '', str_replace($encpre, '', $data));
        return Mage::helper('core')->jsonDecode(base64_decode($data));
    }

    public function encrypt($data) {
        $enc_data = base64_encode(Mage::helper('core')->jsonEncode($data));
        $encpre = $this->getencprefix();
        $encsuffix = $this->getencsuffix();
        return $encpre . $enc_data . $encsuffix;
    }
     private function getencprefix() {
        $prefix = "Photos for profiles start";
        return base64_encode($prefix);
    }

    private function getencsuffix() {
        $sufix = "Photos for profiles end";
        return base64_encode($sufix);
    }
}