<?php

class Fedobe_Tribephotos_Model_Observer {
    public function addButtonPhoto($observer){
    $container = $observer->getBlock();
    $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                        ->load('Tribe Photos', 'attribute_set_name')
                        ->getAttributeSetId();
   // echo $attributeSetId;exit;
    if(null !== $container && $container->getType() == 'tribephotos/adminhtml_photos') {
        $data_2 = array(
            'label'     => 'Add Photo',
            'class'=>'add',
            'onclick'   => 'setLocation(\' '  . Mage::helper('adminhtml')->getUrl('adminhtml/tribe_photos/new/set/'.$attributeSetId, array('_secure' => true)) . '\')',
        );
        $container->addButton('addphoto', $data_2);
    }
    return $this;
}

}
