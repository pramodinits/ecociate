<?php

class Fedobe_Tribecore_Block_Adminhtml_System_Config_Form_Tabs extends Mage_Adminhtml_Block_System_Config_Form_Field {

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        $html = $element->getElementHtml();
        $id = $element->getHtmlId();
        $html .= "<script type='text/javascript'>
				jQuery(function($){
                                    var tabs = [];
                                    var alloedtabs_id = $('#" . $id . "').closest('tbody').find('.multiselect').attr('id')
                                    $('#'+alloedtabs_id+' option:selected').each(function () {
                                       var obj = $(this);
                                       if (obj.length) {
                                        tabs.push(obj.text());
                                       }
                                    });
                                    if(!$.trim($('#" . $id . "').val())){
                                        $('#" . $id . "').val(tabs.join());
                                    }
                                    $(document).on('change','#'+alloedtabs_id,function(){
                                        var tabs = [];
                                        var exist_tabs = $('#" . $id . "').val().split(',');
                                        $('#'+alloedtabs_id+' option:selected').each(function () {
                                            var obj = $(this);
                                            if (obj.length) {
                                             tabs.push(obj.text());
                                             if($.inArray(obj.text(),exist_tabs) == -1){
                                                exist_tabs.push(obj.text());
                                             }
                                            }
                                        });
                                        $(exist_tabs).each(function (i,v) {
                                            if($.inArray(v,tabs) == -1)
                                                exist_tabs.pop(v);
                                        });
                                        if(!$.trim($('#" . $id . "').val())){
                                            $('#" . $id . "').val(tabs.join());
                                        }else{
                                            $('#" . $id . "').val(exist_tabs.join());
                                        }
                                    });
				});
			</script>";
        return $html;
    }

}