<?php

class Fedobe_Tribecore_Block_Adminhtml_Menu extends Mage_Adminhtml_Block_Page_Menu {

    public function getMenuArray() {
        $parentArr = parent::getMenuArray();
        $role_name = Mage::helper('tribecore')->isProfileLoggedin();
        
        if($role_name == 'Tribe Sellers'){
            $seller_obj = Mage::app()->getLayout()->getBlockSingleton('Fedobe_Tribeseller_Block_Adminhtml_Menu');
            $parentArr = $seller_obj->getMenuArray();
        }else if($role_name == 'TribeBrand'){
            $brand_obj = Mage::app()->getLayout()->getBlockSingleton('Fedobe_Tribebrand_Block_Adminhtml_Menu');
            $parentArr = $brand_obj->getMenuArray();   
        }
        return $parentArr;
    }

}