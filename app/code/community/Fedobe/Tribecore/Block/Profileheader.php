<?php

class Fedobe_Tribecore_Block_Profileheader extends Mage_Core_Block_Template {

    private $_info = null;

    public function getAllInfo() {
        if (is_null($this->_info)) {
            $this->_info = $this->allinfo();
        }
        return $this->_info;
    }

    public function setAllInfo($info) {
        $this->_info = $info;
    }

    public function allinfo() {
        $info = Mage::helper('tribecore/page')->getInfo();
        if (!empty($info)) {
            $helper = Mage::helper('cms');
            $processor = $helper->getPageTemplateProcessor();
            $info['trcr_content_top'] = $processor->filter($info['trcr_content_top']);
            $info['trcr_content_bottom'] = $processor->filter($info['trcr_content_bottom']);
        }
        return $info;
    }

    public function getFeaturedHtml() {
        $info = $this->getAllInfo();
        $featuredcontent = '';
        if (!empty($info)) {
            $helper = Mage::helper('cms');
            $processor = $helper->getPageTemplateProcessor();
            $featuredcontent = $processor->filter($info['trcr_content_header']);
        }
        return $featuredcontent;
    }

}