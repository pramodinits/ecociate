<?php

class Fedobe_Tribecore_PageController extends Mage_Core_Controller_Front_Action {
    
    protected function commonDispatchAction(){
        $brandId = $this->getRequest()->getParam('brand_id', $this->getRequest()->getParam('id', false));
        $attr_set_id = $this->getRequest()->getParam('attribute_set_id');
        if (!Mage::helper('tribecore/page')->_renderPage($this, $brandId,$attr_set_id)) {
            $this->_forward('noRoute');
        }
    }

    public function viewAction() {
        $this->commonDispatchAction();
    }
    
    public function notesAction() {
       $this->commonDispatchAction();
    }
    
    public function singlenotesAction() {
       $this->commonDispatchAction();
    }
    
    public function reviewsAction() {
       $this->commonDispatchAction();
    }
    public function photosAction() {
       $this->commonDispatchAction();
    }
    public function videosAction() {
       $this->commonDispatchAction();
    }
     public function singlevideosAction() {
       $this->commonDispatchAction();
    }
   
}