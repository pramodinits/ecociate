<?php

class Fedobe_Tribecore_Helper_Page extends Mage_Core_Helper_Abstract {

    private $_handle = 'tribe_page';
    private $_info = null;

    const DEFAULT_ROOT_TEMPALTE = 'two_columns_left';

    public function setHandle($handle) {

        $this->_handle = $handle;
    }

    public function getHandle() {
        return $this->_handle;
    }

    public function setInfo($info) {
        $this->_info = $info;
    }

    public function getInfo() {
        if (is_null($this->_info)) {
            $this->_info = Mage::registry('tribeprofileinfo');
        }
        return $this->_info;
    }

    /**
     * Renders CMS page
     *
     * @param Mage_Core_Controller_Front_Action $action
     * @param integer $pageId
     * @param bool $renderLayout
     * @return boolean
     */
    public function _renderPage(Mage_Core_Controller_Varien_Action $action, $pageId = null, $attrset_id = null, $renderLayout = true) {
        //var_dump(Mage::getSingleton('core/session')->getMessages());exit;
        $page = Mage::getSingleton('arccore/arc');
        if (!is_null($pageId) && $pageId !== $page->getId()) {
            $delimeterPosition = strrpos($pageId, '|');
            if ($delimeterPosition) {
                $pageId = substr($pageId, 0, $delimeterPosition);
            }

            $page->setStoreId(Mage::app()->getStore()->getId());
            if (!$page->load($pageId)) {
                return false;
            }
        }
        if (!$page->getId()) {
            return false;
        }

        $inRange = Mage::app()->getLocale()
                ->isStoreDateInInterval(null, $page->getTrcrDesignFrom(), $page->getTrcrDesignTo());

        if ($page->getTrcrCustomDesign()) {
            if ($inRange) {
                list($package, $theme) = explode('/', $page->getTrcrCustomDesign());
                Mage::getSingleton('core/design_package')
                        ->setPackageName($package)
                        ->setTheme($theme);
            }
        }

        $cushandle = $this->getHandle();
        $globalhandle = "profile_" . Mage::app()->getRequest()->getActionName();
        $action->getLayout()->getUpdate()
                ->addHandle('default')
                ->addHandle($globalhandle)
                ->addHandle('almagos_tribe_core')
                ->addHandle($cushandle);

        $action->addActionLayoutHandles();

        //Here let's check for root template if not set then deadult 2columns left
        $handle = ($page->getTrcrPageLayout() && $page->getTrcrPageLayout() != 'empty') ? $page->getTrcrPageLayout() : self::DEFAULT_ROOT_TEMPALTE;

        $action->getLayout()->helper('page/layout')->applyHandle($handle);

        Mage::dispatchEvent($cushandle . '_render', array('page' => $page, 'controller_action' => $action));

        $action->loadLayoutUpdates();

        if ($page->getTrcrCustomLayoutUpdate()) {
            $layoutUpdate = $page->getTrcrCustomLayoutUpdate();
            $action->getLayout()->getUpdate()->addUpdate($layoutUpdate);
        }
        $action->generateLayoutXml()->generateLayoutBlocks();


        $contentHeadingBlock = $action->getLayout()->getBlock('page_content_heading');
        if ($contentHeadingBlock) {
            $contentHeading = $this->escapeHtml($page->getTrcrHeaderTitle());
            $contentHeadingBlock->setContentHeading($contentHeading);
        }

        if ($page->getTrcrPageLayout()) {
            $action->getLayout()->helper('page/layout')
                    ->applyTemplate($page->getTrcrPageLayout());
        } else {
            $action->getLayout()->helper('page/layout')
                    ->applyTemplate(self::DEFAULT_ROOT_TEMPALTE);
        }

        /* @TODO: Move catalog and checkout storage types to appropriate modules */
        $messageBlock = $action->getLayout()->getMessagesBlock();
        foreach (array('catalog/session', 'checkout/session', 'customer/session') as $storageType) {
            $storage = Mage::getSingleton($storageType);
            if ($storage) {
                $messageBlock->addStorageType($storageType);
                $messageBlock->addMessages($storage->getMessages(true));
            }
        }

        //Here let's set SEO if any
        if ($page->getTrcrMetaTitle())
            $action->getLayout()->getBlock('head')->setTitle($page->getTrcrMetaTitle());
        if ($page->getTrcrMetaDescription())
            $action->getLayout()->getBlock('head')->setDescription($page->getTrcrMetaDescription());
        if ($page->getTrcrMetaKeywords())
            $action->getLayout()->getBlock('head')->setKeywords($page->getTrcrMetaKeywords());
        //End
        //Here to set ROBOTS
        $followoptions = array();
        $attrfollow = Mage::getSingleton('eav/config')->getAttribute(Mage::helper('arccore')->getEntityType(), 'trcr_meta_follow');
        if ($attrfollow->usesSource()) {
            $options = $attrfollow->getSource()->getAllOptions(false);
            foreach ($options as $key => $val) {
                $followoptions[$val['value']] = strtoupper($val['label']);
            }
        }
        $indexoptions = array();
        $attrindex = Mage::getSingleton('eav/config')->getAttribute(Mage::helper('arccore')->getEntityType(), 'trcr_meta_index');
        if ($attrindex->usesSource()) {
            $options = $attrindex->getSource()->getAllOptions(false);
            foreach ($options as $key => $val) {
                $indexoptions[$val['value']] = strtoupper($val['label']);
            }
        }
        if (isset($followoptions[$page->getTrcrMetaFollow()]) && isset($indexoptions[$page->getTrcrMetaIndex()])) {
            $robotstring = "{$indexoptions[$page->getTrcrMetaIndex()]},{$followoptions[$page->getTrcrMetaFollow()]}";
            $action->getLayout()->getBlock('head')->setRobots($robotstring);
        }
        //End

        $custom = '';
        //Here to inject all SMO
        //For Open Graph Meta property (Facebook and Google +)
        if ($page->getTrcrOgTitle()) {
            $custom .= '<meta property="og:title" content="' . $page->getTrcrOgTitle() . '" />';
            $custom .= '<meta property="og:type" content="article" />';
            $custom .= '<meta property="og:url" content="' . Mage::helper('core/url')->getCurrentUrl() . '" />';
            $custom .= '<meta property="og:site_name" content="Site Name, i.e. ' . Mage::getStoreConfig('general/store_information/name') . '" />';
        }
        if ($page->getTrcrOgPic()) {
            //Here let's resize the image to facebook recomendation i.e 1200 x 630
            $resizedimg = Mage::helper('arccore')->resizeImg($page->getTrcrOgPic(), 1200, 630);
            $custom .= '<meta property="og:image" content="' . $resizedimg . '" />';
        }
        if ($page->getTrcrOgDescription())
            $custom .= '<meta property="og:description" content="' . $page->getTrcrOgDescription() . '" />';

        //Here for Twitter
        if ($page->getTrcrTwitterTitle()) {
            $custom .= '<meta name="twitter:card" content="summary_large_image" />';
            $custom .= '<meta name="twitter:title" content="' . $page->getTrcrTwitterTitle() . '" />';
        }
        if ($page->getTrcrTwitterDescription())
            $custom .= '<meta name="twitter:description" content="' . $page->getTrcrTwitterDescription() . '" />';
        if ($page->getTrcrTwitterPic()) {
            $twresizedimg = Mage::helper('arccore')->resizeImg($page->getTrcrTwitterPic(), 1200, 630);
            $custom .= '<meta property="twitter:image" content="' . $twresizedimg . '" />';
        }
        //End
        //Here to inject canonical URL if any
        if ($page->getTrcrCanonicalUrl())
            $custom .= '<link href="' . $page->getTrcrCanonicalUrl() . '" rel="canonical" />';
        //End
        //Here for redirect 
        $redirectoptions = array();
        $attrred = Mage::getSingleton('eav/config')->getAttribute(Mage::helper('arccore')->getEntityType(), 'trcr_redirect_type');
        if ($attrindex->usesSource()) {
            $options = $attrred->getSource()->getAllOptions(false);
            foreach ($options as $key => $val) {
                $redirectoptions[$val['value']] = strtoupper($val['label']);
            }
        }
        if ($page->getTrcrRedirectUrl()) {
            $url = $page->getTrcrRedirectUrl();
            if ($page->getTrcrRedirectType()) {
                $opt = $redirectoptions[$page->getTrcrRedirectType()];
                $int = filter_var($opt, FILTER_SANITIZE_NUMBER_INT);
                if ($int) {
                    header("Location: $url", true, $int);
                    exit();
                } else {
                    $custom .= '<meta http-equiv="refresh" content="0;URL=\'' . $url . '\'" />';
                }
            } else {
                //Here gores the deafult 301 reditect
                header("Location: $url", true, 301);
                exit();
            }
        }

        //Here let's include the custom css and javascript
        if ($page->getTrcrCustomCss())
            $custom .= $page->getTrcrCustomCss();
        if ($page->getTrcrCustomJs())
            $custom .= $page->getTrcrCustomJs();
        //End
        $action->getLayout()->getBlock('head')->setIncludes($custom);
        if ($renderLayout) {
            $modulename = Mage::app()->getRequest()->getModuleName();
            $classname = Mage::getConfig()->getHelperClassName($modulename);
            if (class_exists($classname)) {
                $helper = Mage::helper($modulename);
                $data = $helper->getInfo($page->getData());
            } else {
                $data = $page->getData();
            }
            $this->setInfo($data);
            Mage::register('tribeprofileinfo', $data);
            Mage::dispatchEvent('tribe_info_set_after', array('tribeobj' => $this));
            $this->_initLayoutMessages(array('core/session','customer/session','adminhtml/session'), $action);
            $action->renderLayout();
        }
        return true;
    }

    protected function _initLayoutMessages($messagesStorage, $action) {
        if (!is_array($messagesStorage)) {
            $messagesStorage = array($messagesStorage);
        }
        foreach ($messagesStorage as $storageName) {
            $storage = Mage::getSingleton($storageName);
            if ($storage) {
                $block = $action->getLayout()->getMessagesBlock();
                $block->addMessages($storage->getMessages(true));
                $block->setEscapeMessageFlag($storage->getEscapeMessages(true));
                $block->addStorageType($storageName);
            } else {
                Mage::throwException(
                        Mage::helper('core')->__('Invalid messages storage "%s" for layout messages initialization', (string) $storageName)
                );
            }
        }
        return $action;
    }

}