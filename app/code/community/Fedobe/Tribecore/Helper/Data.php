<?php

class Fedobe_Tribecore_Helper_Data extends Mage_Core_Helper_Abstract {
    public function isActionallowed($info) {
        $allow = true;
        if ($info['user_log_id'] || (Mage::app()->isInstalled() && Mage::getSingleton('customer/session')->isLoggedIn())) {
            if ($info['trcr_account_type'] == 2) {
                $id = $info['user_log_id']  ? $info['user_log_id'] : Mage::getSingleton('customer/session')->getCustomer()->getId();
                if ($info['trcr_customer_list'] == $id) {
                    $allow = false;
                }
            } else {
                $email = $info['logged_in_email']  ? $info['logged_in_email'] : Mage::getSingleton('customer/session')->getCustomer()->getEmail();
                if (trim($info['trcr_email']) == $email) {
                    $allow = false;
                }
            }
        }
        return $allow;
    }
    public function is_customer(){
       $is_cust = 0;
       $getLoggedincustomerdata = Mage::getSingleton('customer/session')->getCustomer()->getData();
       $sellercollection = $this->getseller_by_customer($getLoggedincustomerdata['entity_id']);
       $seller_profile = array();
       $seller_profile = $sellercollection->getData();
       $email = $getLoggedincustomerdata['email'];
       $data = $this->getprofile_by_emailoption($getLoggedincustomerdata['entity_id'])->getData();
        $data_option = Mage::helper('arccore')->getattributeoptions('trcr_profile_type');
        $data_opt_value = array_combine(array_column($data_option,'value'),array_column($data_option,'label'));
       if(strtolower($data_opt_value[$data['trcr_profile_type']])=='customer' && empty($seller_profile)){
           $is_cust = 1;
       }
       return $is_cust;
   }
    public function getprofile_by_emailoption($option_id){
       $collection = Mage::getModel('arccore/arc')->getCollection()
                      ->addAttributeToSelect('*')
                     ->addAttributeTofilter('trcr_customer_list',$option_id)
                      ->addAttributeTofilter('trcr_profile_type', array('notnull' => true))
                     //->addAttributeTofilter('attribute_set_id',20)
                     ->getFirstItem();
         return $collection;
   }
    public function getLoggedincustomerdata(){
       return Mage::getSingleton('customer/session')->getCustomer()->getData();
   }
   public function getProfileUrl(){
      $customer= $this->getLoggedincustomerdata();
      $customer_prof = $this->getprofile_by_customerlist($customer['entity_id']);
      $sellercollection = $this->getseller_by_customer($customer['entity_id']);
       $seller_profile = array();
       $seller_profile = $sellercollection->getData();
       if(!empty($customer_prof) && empty($seller_profile))
        $url = Mage::getBaseUrl().$customer_prof['trcr_username'];
       elseif(!empty($seller_profile))
       $url = Mage::helper('tribeseller')->getFrontendProductUrl($seller_profile['seller_user_name']);
       return $url;
   }
   public function getprofile_by_customerlist($option_id){
       $collection = Mage::getModel('arccore/arc')->getCollection()
                      ->addAttributeToSelect('*')
                     ->addAttributeTofilter('trcr_customer_list',$option_id)
                      ->addAttributeTofilter('trcr_profile_type', array('notnull' => true))
                     //->addAttributeTofilter('attribute_set_id',20)
                     ->getFirstItem();
         return $collection;
   }
   public function getseller_by_customer($option_id){
       $collection = Mage::getModel('tribeseller/seller')->getCollection()
                     ->addFieldToFilter('all_customers',$option_id)
                     ->getFirstItem();
                     //return $collection->getSelect()->__toString();
        return $collection;
   }
   public function is_seller(){
        $is_slr = 0;
       $customer = Mage::getSingleton('customer/session')->getCustomer()->getData();
         $sellercollection = $this->getseller_by_customer($customer['entity_id']);
       $seller_profile = array();
       $seller_profile = $sellercollection->getData();
       if(!empty($seller_profile))
       $is_slr = 1;
       return $is_slr;
   }
   public function isProfileLoggedin() {
        $admin_user_session = Mage::getSingleton('admin/session');
        if ($admin_user_session->getUser()) {
            $adminuserId = $admin_user_session->getUser()->getUserId();
            $role_data = Mage::getModel('admin/user')->load($adminuserId)->getRole()->getData();
            if (!empty($role_data) && $role_data['role_name'])
                return $role_data['role_name'];
            else
                return FALSE;
        }else {
            return FALSE;
        }
    }
 public function getProfileIdFromadminUser() {
        $user = Mage::getSingleton('admin/session');
        $userEmail = $user->getUser()->getEmail();
        $option_id = Mage::helper('arccore')->getattributeoptionsid('trcr_customer_list',$userEmail);
        $profile = Mage::helper('tribecore')->getprofile_by_emailoption($option_id);
        if(!$profile->getId()){
           $profile = Mage::getModel('arccore/arc')->getCollection()
                      ->addAttributeToSelect('*')
                     ->addAttributeTofilter('trcr_email',$userEmail)
                     ->getFirstItem();
        }
        if (!$profile->getId() || $profile->getTrcrStatus() != 2) {
            return false;
        }
        return $profile->getId();
    }

}