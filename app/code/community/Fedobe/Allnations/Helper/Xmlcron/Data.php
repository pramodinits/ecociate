<?php

class Fedobe_Allnations_Helper_Xmlcron_Data extends Mage_Core_Helper_Abstract {

    public function setXmlCron($crontiming, $cronurl) {
        $cronurl = $this->removeKeyFromUrl($cronurl);
        if (!file_exists($_SERVER["DOCUMENT_ROOT"] . '/bridgecron')) {
            mkdir($_SERVER["DOCUMENT_ROOT"] . '/bridgecron');
            chmod($_SERVER["DOCUMENT_ROOT"] . '/bridgecron', 0777);
        }
        $crons = shell_exec('crontab -l');
        $cron_ready_str = $this->getCronUrl($crontiming, $cronurl);
        $findcron = str_replace('/', '\/', preg_quote($cron_ready_str));
        $pattern = "/$findcron/i";
        $match = preg_match($pattern, $crons);
        if (!$match) {
            $crons .= $cron_ready_str . "\n";
        }
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/bridgecron/crontab.txt', $crons);
        exec("crontab {$_SERVER['DOCUMENT_ROOT']}/bridgecron/crontab.txt");
    }

    public function removeXmlCron($crontiming, $cronurl) {
        $cronurl = $this->removeKeyFromUrl($cronurl);
        $crons = shell_exec('crontab -l');
        $cronurl = $this->getCronUrl($crontiming, $cronurl);
        $crons = str_replace($cronurl . "\n", "", $crons);
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/bridgecron/crontab.txt', $crons);
        exec("crontab {$_SERVER['DOCUMENT_ROOT']}/bridgecron/crontab.txt");
    }

    public function getCronUrl($crontiming, $cronurl) {
        $cronstarturl = 'wget -o /dev/null -q -O -';
        $endcronurl = '> /dev/null';
        $cron_ready_str = "$crontiming " . $cronstarturl . ' ' . $cronurl . ' ' . $endcronurl;
        return $cron_ready_str;
    }

    public function removeKeyFromUrl($url) {
        $test = explode("/", trim($url, "/"));
        $total = count($test);
        unset($test[$total - 1]);
        unset($test[$total - 2]);
        return implode("/", $test);
    }

}
