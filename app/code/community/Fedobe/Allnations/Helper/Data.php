<?php

class Fedobe_Allnations_Helper_Data extends Fedobe_Bridge_Helper_Data {

    public function getIncomingAddons() {
        $defaultaddons = parent::getIncomingAddons();
        $defaultaddons['allnations'] = "Allnations";
        asort($defaultaddons);
        //Here let's check any Allnations incoming channel added
        $id = Mage::app()->getRequest()->getParam('id') ? Mage::app()->getRequest()->getParam('id') : 0;
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $inchatable = $resource->getTableName('bridge/incomingchannels');
        $sql = "SELECT id FROM $inchatable WHERE channel_type= 'allnations' AND id <> $id LIMIT 1";
        $sqlQuery = $readConnection->query($sql);
        while ($row = $sqlQuery->fetch()) {
            unset($defaultaddons['allnations']);
        }
        //End
        return $defaultaddons;
    }

    public function getAllnationsDataDirectory() {
        return Mage::getModuleDir('', 'Fedobe_Allnations') . DS . 'Helper' . DS . 'Data';
    }

    public function getXmlData($id) {
        if ($id) {
            $xmlprocess = Mage::getModel('allnations/process');
            $processinfo = $xmlprocess->loadByChannelId($id);
            if ($processinfo->getId() && $processinfo->getProcessed()) {
                //As the CSV data been processed let's read and format the data for mapping
                $csvdata = array();
                $resource = Mage::getSingleton('core/resource');
                $xmldatatable = $resource->getTableName('allnations/allnationsdata');
                $readConnection = $resource->getConnection('core_read');
                $sql = $readConnection->select()->from($xmldatatable, array('data'))->where("bridge_channel_id=$id");
                $sqlQuery = $readConnection->query($sql);
                $ignorelist = $this->getIgnoreAttributelist();
                $dept = $cat = $subcat = array();
                while ($row = $sqlQuery->fetch()) {
                    //Here let's unserialize the data and format it
                    $unserdata = unserialize($row['data']);
                    foreach ($unserdata as $kk => $vv) {
                        $kk = $this->clean($kk);
                        $vv = $this->clean($vv);
                        if (!in_array($kk, $ignorelist)) {
                            if ($vv != '')
                                    $csvdata[$kk][$vv] = $vv;
                            if ($kk == 'DEPARTAMENTO') {
                                $dept[$vv] = $vv;
                            } else if ($kk == 'CATEGORIA') {
                                $deptsring = $this->clean($unserdata['DEPARTAMENTO']);
                                $cat[$deptsring][$vv] = $vv;
                            } else if ($kk == 'SUBCATEGORIA') {
                                $catsring = $this->clean($unserdata['CATEGORIA']);
                                $subcat[$catsring][$vv] = $vv;
                            }
                        }
                    }
                }
                $finalres = array(
                    'attrinfo' => $csvdata,
                    'catinfo' => array('dept' => $dept, 'cat' => $cat, 'subcat' => $subcat)
                );
                return $finalres;
            } else {
                if ($processinfo->getInitiated()) {
                    return 2;
                } else {
                    return 1;
                }
            }
        } else {
            return 0;
        }
    }

    function clean($string) {
        $string = str_replace(' ', '-', htmlspecialchars(trim($string)));
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }

    public function getsyncCronStatus($channel_id) {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $logtable = $resource->getTableName('allnations/allnationslastupdatedat');
        $logsql = "SELECT `sync_to_store` FROM $logtable WHERE channel_id=$channel_id LIMIT 1 ";
        $sync_to_store = $read->fetchOne($logsql);
        return $sync_to_store;
    }

    public function getmigrationprocessstatus() {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $table = $resource->getTableName('allnations/allnationstempdata');
        $sql = "SELECT count(*) AS `migratedrows` FROM $table WHERE `is_migrated`=1";
        $totalsql = "SELECT count(*) AS `total` FROM $table";
        $totalmgrt = $read->query($sql)->fetch();
        $totalmgrt = $totalmgrt['migratedrows'];
        $total = $read->query($totalsql)->fetch();
        $total = $total['total'];
        if ($totalmgrt) {
            $toalperc = round((($totalmgrt / $total) * 100), 0);
            return $toalperc;
        } else {
            return 0;
        }
    }

    public function getPimCronOptions() {
        $globalcron = $this->globalCronOptions();
        $pimcron = array();
        foreach ($globalcron as $k => $v) {
            $pimcron[$k] = $v['label'];
        }
        return $pimcron;
    }

    public function getEdiCronOptions() {
        $globalcron = $this->globalCronOptions();
        $edicron = array();
        foreach ($globalcron as $k => $v) {
            $edicron[$k] = $v['label'];
        }
        return $edicron;
    }

    public function globalCronOptions() {
        $globalcronopt = array(
            'never' => array('opt' => '0', 'label' => Mage::helper('bridge')->__('Never'), 'time' => 0),
            '0 0 * * *' => array('opt' => '0 0 * * *', 'label' => Mage::helper('bridge')->__('Daily'), 'time' => 1),
            '0 0 * * 0' => array('opt' => '0 0 * * 0', 'label' => Mage::helper('bridge')->__('Weekly'), 'time' => 7),
            '0 0 1 * *' => array('opt' => '0 0 1 * *', 'label' => Mage::helper('bridge')->__('Monthly'), 'time' => 30)
        );
        return $globalcronopt;
    }

    public function getCrondaysFromOptions($opt) {
        $globalcron = $this->globalCronOptions();
        if (array_key_exists($opt, $globalcron)) {
            return $globalcron[$opt]['time'];
        } else {
            return 0;
        }
    }

    public function getAllnationsLargeImageUrlPath($sku) {
        return "http://images.allnations.com.br/imagens/produtos/imagemSite.aspx?h=3000&l=3000&src=$sku";
    }

    public function addProductGalleryImages($mediaArray, $sku, $product) {
        $importDir = Mage::getBaseDir('media') . DS . 'Allnationsimages';
        $importmediaarray = array();
        if (!file_exists('path/to/directory')) {
            mkdir($importDir, 0777, true);
        }
        $mediaimagetype = array('image', 'small_image', 'thumbnail');
        $flagarr = array();
        //Here let's copy from remote server to current store
        foreach ($mediaArray as $k => $imageinfo) {
            $imageinfo = (array) $imageinfo;
            $k++;
            $source = $imageinfo['url'];
            $imagefile = $this->imageExists($importDir, $sku, 'jpg', $k); //echo $imagefile;exit;
            $dest = "$importDir/$imagefile";
            if (!empty($imageinfo['types'])) {
                $types = $imageinfo['types'];
                $flag = 1;
            } else {
                $types = array();
                $flag = 0;
            }
            if (file_put_contents($dest, file_get_contents($source))) {
                $importmediaarray[] = array(
                    "filename" => $imagefile,
                    "types" => $types
                );
                $flagarr[] = $flag;
            }
        }
        //Here to to check if not image types found then set the first image as base image, small iamge and thumnail
        if (array_sum($flagarr) == 0) { //echo 34;exit;
            $importmediaarray[0]['types'] = $mediaimagetype;
        }
        foreach ($importmediaarray as $pos => $fileinfo) {
            $filePath = $importDir . '/' . $fileinfo['filename'];
            if (file_exists($filePath)) {
                try {
                    $product->addImageToMediaGallery($filePath, $fileinfo['types'], false, false);
                    unlink($filePath);
                } catch (Exception $e) {
                    echo $e->getMessage();
                }//echo 13;exit;
            } else {
                echo "Product does not have an image or the path is incorrect. Path was: {$filePath}<br/>";
            }
        }
        //echo $product->getSku();exit;
        $product->save();
    }

    public function imageExists($importDir, $sku, $ext, $k) {
        if (!file_exists("$importDir/$sku" . "_$k.$ext")) {
            return "$sku" . "_$k.$ext";
        } else {
            $k = $k . "_" . $k;
            return imageExists($importDir, $sku, $ext, $k);
        }
    }

    //Here this function to get the Amazon Attributes
    public function getIncomingAttributeBysku($sku, $channel_id) {
        $arr_list = array();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $table = Mage::getSingleton('core/resource')->getTableName('allnations/allnationsdata');
        $sql = 'SELECT data FROM ' . $table . ' WHERE bridge_channel_id=' . $channel_id . ' AND sku="' . $sku . '" LIMIT 1';
        $attributes = $readConnection->fetchAll($sql);
        $ignorelist = $this->getIgnoreAttributelist();
        $m = 0;
        if (!empty($attributes)) {
            $attr_arr = unserialize($attributes[0]['data']);
            foreach ($attr_arr as $k1 => $v1) {
                if (!in_array($k1, $ignorelist)) {
                    $arr_list[$m]['id'] = $v1;
                    $arr_list[$m]['value'] = $v1;
                    $arr_list[$m]['attribute_id'] = $k1;
                }
                $m++;
            }
        }
        return $arr_list;
    }

    public function getIgnoreAttributelist() {
        return array('TIMESTAMP', 'DESCRICAO', 'DESCRTEC', 'URLFOTOPRODUTO');
    }

    public function getStoreCategoryTreeHtml($list = 0) {
        // Get category collection
        $categories = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToSelect('name')
                ->addAttributeToSort('path', 'asc')
                ->addFieldToFilter('is_active', array('eq' => '1'))
                ->load()
                ->toArray();

        // Arrange categories in required array
        $categoryList = array();
        foreach ($categories as $catId => $category) {
            if (isset($category['name'])) {
                $categoryList[] = array(
                    'label' => htmlspecialchars($category['name']),
                    'level' => htmlspecialchars($category['level']),
                    'value' => $catId
                );
            }
        }
        if ($list) {
            return $categoryList;
        } else {
            $catsring = "";
            foreach ($categoryList as $value) {
                $catName = $value['label'];
                $catId = $value['value'];
                $catLevel = $value['level'];

                $space = '---';
                for ($i = 1; $i < $catLevel; $i++) {
                    $space = $space . "---";
                }
                $catName = $space . $catName;
                $catsring .='<option value="' . $catId . '">' . $catName . '</option>';
            }
            return $catsring;
        }
    }

    public function getAllnationsTreecategory($catinfo,$list=0) {
        $dept = $catinfo['dept'];
        $cat = $catinfo['cat'];
        $subcat = $catinfo['subcat'];
        sort($dept);
        $cat = $this->recursive_ksort($cat);
        $subcat = $this->recursive_ksort($subcat);
        $catind = "--";
        $subcatind = "----";
        $opt = "";
        $optasarray = array();
        foreach ($dept as $key => $value) {
            $tmpoptarr = $this->getrichoptions($value,$value);
            $opt.=$tmpoptarr;
            $optasarray[] = array('val'=>$value,'opt'=>$tmpoptarr);
            if (array_key_exists($value, $cat)) {
                foreach ($cat[$value] as $ck => $cv) {
                    $tmpopt = "$catind $cv";
                    $tmpoptarr = $this->getrichoptions($cv,$tmpopt);
                    $opt.=$tmpoptarr;
                    $optasarray[] = array('val'=>$cv,'opt'=>$tmpoptarr);
                    if (array_key_exists($cv, $subcat)) {
                        foreach ($subcat[$cv] as $sck => $scv) {
                            $tmpsopt = "$subcatind $scv";
                            $tmpoptarr = $this->getrichoptions($scv,$tmpsopt);
                            $opt.=$tmpoptarr;
                            $optasarray[] = array('val'=>$scv,'opt'=>$tmpoptarr);
                        }
                    }
                }
            }
        }
        if($list){
            return $optasarray;
        }else{
            return $opt;
        }
    }

    public function recursive_ksort($array) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                sort($v);
                $array[$k] = $v;
            }
        }
        return $array;
    }

    public function getrichoptions($value,$label) {
        $opt = '<option value="'.$value.'" id="'.$value.'" sel>'.$label.'</option>';
        return $opt;
    }

    public function getErrorCodeList(){
        return array(
            'AN1000' => "Attribute Set Not Mapped",
            'AN2000' => "Category Not Mapped",
            'AN3000' => "Store View Not Mapped",
            'AN4000' => "Channel Id Not Found",
            'AN5000' => "Channel Status Not Actived",
            'AN6000' => "Channel Mode Set to Demo and product creation qouta excedded (10 Products)",
            'AN7000' => "Price and Quantity Not Mapped",
            'AN8000' => "Sync to Store been stopped",
            'AN9000' => "No Attribute Mapped",
            'AN10000' => "Product Skipped",
            'AN11000' => "Product Updated",
            'AN12000' => "New Product Created",
            'AN13000' => "Product Image Not Found",
            'AN14000' => "Allnations CODIGO used as SKU for the product",
            'AN20000' => "EDI Synced",
            'AN30000' => "Codigo Used as SKU",
        );
    }
    
    public function getIncomingProdcutCode($bridge_channel_id,$product_id){
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $allnationstable = $resource->getTableName('allnations/allnationsdata');
        $sql = "SELECT `codigo` FROM $allnationstable WHERE `bridge_channel_id` = $bridge_channel_id AND `id_product`=$product_id AND is_synced = 1 LIMIT 1";
        $code = $read->fetchOne($sql);
        return $code;
    }
    
    public function reProcessLogs($channel_id,$sku_list=null){
        $resource = Mage::getSingleton('core/resource');
        $write = $resource->getConnection('core_write');
        $allnationstable = $resource->getTableName('allnations/allnationsdata');
        $sql = "UPDATE $allnationstable SET `is_skipped` = 0 WHERE `bridge_channel_id` = $channel_id AND `is_skipped`= 1 ";
        $write->fetchOne($sql);
    }
}
