<?php

class Fedobe_Allnations_Helper_Xmlparser_Data extends Mage_Core_Helper_Abstract {

    var $product;
    var $product_elem;
    var $productinfo;
    var $producthelper;
    private $channel_id;

    public function __construct($channel_id) {
        $this->productinfo = array('TIMESTAMP', 'DEPARTAMENTO', 'CATEGORIA', 'SUBCATEGORIA', 'FABRICANTE', 'CODIGO', 'DESCRICAO', 'DESCRTEC', 'PARTNUMBER', 'EAN', 'GARANTIA', 'PESOKG', 'PRECOREVENDA', 'DISPONIVEL', 'URLFOTOPRODUTO', 'ESTOQUE', 'NCM', 'LARGURA', 'ALTURA', 'PROFUNDIDADE', 'ATIVO', 'SUBSTTRIBUTARIA', 'ORIGEMPRODUTO');
        $this->producthelper = array();
    }

    public function setChannelId($channel_id){
        $this->channel_id = $channel_id;
    }
    
    public function getChannelId(){
        return $this->channel_id;
    }
    // invoked every time open tag is encountered in the stream
    // $tag contains the name of the tag and $attributes is a key-value array of tag attributes
    function startElement($parser, $tag, $attributes) {
        switch ($tag) {
            case 'Produtos':
                $this->product = array('CODIGO' => '', 'ORIGEMPRODUTO' => '');
                break;
            case 'TIMESTAMP':
            case 'DEPARTAMENTO':
            case 'CATEGORIA':
            case 'SUBCATEGORIA':
            case 'FABRICANTE':
            case 'CODIGO':
            case 'DESCRICAO':
            case 'DESCRTEC':
            case 'PARTNUMBER':
            case 'EAN':
            case 'GARANTIA':
            case 'PESOKG':
            case 'PRECOREVENDA':
            case 'DISPONIVEL':
            case 'URLFOTOPRODUTO':
            case 'ESTOQUE':
            case 'NCM':
            case 'LARGURA':
            case 'ALTURA':
            case 'PROFUNDIDADE':
            case 'ATIVO':
            case 'SUBSTTRIBUTARIA':
            case 'ORIGEMPRODUTO':
                if ($this->product) {
                    $this->product_elem = $tag;
                }
                break;
        }
    }

    // invoked on each closing tag
    function endElement($parser, $tag) {
        switch ($tag) {
            case 'Produtos':
                if ($this->product) {
                    $this->handle_product();
                    $this->product = null;
                }
                break;
            case 'TIMESTAMP':
            case 'DEPARTAMENTO':
            case 'CATEGORIA':
            case 'SUBCATEGORIA':
            case 'FABRICANTE':
            case 'CODIGO':
            case 'DESCRICAO':
            case 'DESCRTEC':
            case 'PARTNUMBER':
            case 'EAN':
            case 'GARANTIA':
            case 'PESOKG':
            case 'PRECOREVENDA':
            case 'DISPONIVEL':
            case 'URLFOTOPRODUTO':
            case 'ESTOQUE':
            case 'NCM':
            case 'LARGURA':
            case 'ALTURA':
            case 'PROFUNDIDADE':
            case 'ATIVO':
            case 'SUBSTTRIBUTARIA':
            case 'ORIGEMPRODUTO':
                $this->product_elem = null;
                break;
        }
    }

    // invoked each time cdata text is processed
    // note that this may be just a fragment of cdata element so 
    // consider that single cdata element can be processed 
    // with multiple invocations of this method
    function cdata($parser, $cdata) {
        if ($this->product && $this->product_elem) {
            $this->product[$this->product_elem] .= $cdata;
        }
    }

    // invoked each time a complete product is decoded from the stream
    function handle_product() {
        $this->print_product();
    }

    // prints decoded product
    function print_product() {
        $temp = $tempser = array();
        foreach ($this->productinfo as $k => $v) {
            $tempser[$v] = $this->product[$v];
        }
        $temp['data'] = serialize($tempser);
        $this->producthelper[] = $temp;
//        echo $this->product['EAN'] . ": " . $this->product['TIMESTAMP'] . "<br/>";
    }

}
