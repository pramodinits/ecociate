<?php
$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$installer->startSetup();
$installer->run("
    
CREATE TABLE IF NOT EXISTS `bridge_allnationsdata_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `bridge_channel_id` int(50) DEFAULT NULL,
  `last_updated_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS `bridge_allnationsdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_product` int(11) DEFAULT NULL,
  `bridge_channel_id` int(50) DEFAULT NULL,
  `ean` varchar(255) DEFAULT NULL,
  `codigo` varchar(255) DEFAULT NULL UNIQUE KEY,
  `sku` varchar(255) DEFAULT NULL,
  `data` text,
  `is_synced` TINYINT NOT NULL DEFAULT '0',
  `is_skipped` TINYINT NOT NULL DEFAULT '0',
  `pim_updated_at` DATE NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS `bridge_allnations_tempdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `data` text,
  `is_migrated` TINYINT NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

 CREATE TABLE IF NOT EXISTS `bridge_allnationslastupdatedat`
 (
 `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `channel_id` INT(11) NOT NULL,
 `lastupdateddate` DATE NULL,
 `initiated` TINYINT NOT NULL DEFAULT '0',
 `sync_to_store` TINYINT NOT NULL DEFAULT '0',
 `processed` TINYINT NOT NULL DEFAULT '0',
 `filename` VARCHAR(255) NULL
 );
 ");

$installer->endSetup();
 ?>
 

