<?php

class Fedobe_Allnations_Model_Process extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('allnations/process');
    }
    
    public function loadByChannelId($id){
        if($id){
            $this->_getResource()->loadByField($this, $id);
            return $this;
        }
    }
}
