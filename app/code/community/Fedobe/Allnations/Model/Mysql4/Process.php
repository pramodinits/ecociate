<?php

class Fedobe_Allnations_Model_Mysql4_Process extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('allnations/allnationslastupdatedat', 'id');
    }

    public function loadByField(Fedobe_Bridgecsv_Model_Process $Object, $fieldvalue) {
        $adapter = $this->_getReadAdapter();
        $bind = array('channel_id' => $fieldvalue);
        $select = $adapter->select()
                ->from($this->getMainTable(), 'id')
                ->where('channel_id = :channel_id');
        $modelId = $adapter->fetchOne($select, $bind);
        if ($modelId) {
            $this->load($Object, $modelId);
        } else {
            $Object->setData(array());
        }
        return $this;
    }

}
