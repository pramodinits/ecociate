<?php

class Fedobe_Allnations_Adminhtml_AllnationsController extends Mage_Adminhtml_Controller_Action {

    public function _initAction() {
        $this->_title(Mage::helper('allnations')->__('Fedobe Extension'));
    }

    public function indexAction() {
        $this->_initAction();
        $this->_title(Mage::helper('bridge')->__('Manage Incoming Channels'));
        $this->loadLayout();
        $this->renderLayout();
    }

    public function preDispatch() {
        $bypassaction = array('instantautoprocess', 'synctotemptable', 'getsyncprogressstatus', 'getlatestallnationsdata', 'product_create', 'syncordertoallnations');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }

    public function getsyncprogressstatusAction() {
        $toalperc = Mage::helper('allnations')->getmigrationprocessstatus();
        echo $toalperc;
    }

    public function parseXmlAction($chnid = 0) {
        $id = ($chnid) ? $chnid : $this->getRequest()->getPost('id');
        if ($id) {
            $processinfo = Mage::getModel('allnations/process')->loadByChannelId($id);
            $filename = $processinfo->getFilename();
            if ($filename) {
                try {
                    $fullpathxml = Mage::helper('allnations')->getAllnationsDataDirectory() . DS . $filename;
                    $xml_handler = Mage::helper('xmlparser');
                    $xml_handler->setChannelId($id);
                    $parser = xml_parser_create();
                    xml_set_object($parser, $xml_handler);
                    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, false);
                    xml_set_element_handler($parser, "startElement", "endElement");
                    xml_set_character_data_handler($parser, "cdata");
                    $fp = fopen($fullpathxml, 'r');
                    while ($data = fread($fp, 4096)) {
                        xml_parse($parser, $data, feof($fp));
                        flush();
                    }
                    fclose($fp);
                    //Here let's insert the parse XML data into temp table
                    $resource = Mage::getSingleton('core/resource');
                    $writeConnection = $resource->getConnection("core_write");
                    $temptable = $resource->getTableName('allnations/allnationstempdata');

                    //Here let's delete all those temp records been migrated already
                    $delsql = "DELETE FROM $temptable WHERE `is_migrated`=1 ";
                    $writeConnection->query($delsql);

                    $processinfo->setData('initiated', 1);
//                    $processinfo->setData('processed', 0);
                    $processinfo->save();

                    $temparr = array();
                    $cnt = 1;
                    foreach ($xml_handler->producthelper as $k => $tempdata) {
                        $temparr[] = $tempdata;
                        if ($cnt % 100 == 0) {
                            $writeConnection->insertMultiple($temptable, $temparr);
                            $temparr = array();
                        }
                        $cnt++;
                    }
                    if (count($temparr) > 0) {
                        $writeConnection->insertMultiple($temptable, $temparr);
                    }
                    //End

                    //Here let's remove that autoprocess cron
                    $autoprocesstime = "*/1 * * * *";
                    $autoprocesscronurl = Mage::helper("adminhtml")->getUrl("*/allnations/instantautoprocess");
                    Mage::helper('xmlcron')->removeXmlCron($autoprocesstime, $autoprocesscronurl);
                    //End
                    //Here let's auto start the Parsing CRON
                    $cronurl = Mage::helper("adminhtml")->getUrl('*/allnations/synctotemptable');
                    Mage::helper('xmlcron')->setXmlCron('*/1 * * * *', $cronurl);
                    echo $this->__('Parse process started!');
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            } else {
                echo $this->__('XML data not found!');
            }
        } else {
            echo $this->__('Channel Id not found!');
        }
    }

    public function synctotemptableAction() {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $write = $resource->getConnection("core_write");
        $table = $resource->getTableName('allnations/allnationstempdata');
        $allnationstable = $resource->getTableName('allnations/allnationsdata');
        //Here to get channel id
        $logtable = $resource->getTableName('allnations/allnationslastupdatedat');
        $logsql = "SELECT channel_id FROM $logtable LIMIT 1 ";
        $channel_id = $read->fetchOne($logsql);
        //End

        $datafound = $batchdata = $batchdataid = $datatoinsert = array();
        $sql = "SELECT * FROM $table WHERE `is_migrated` = 0 ORDER BY `created_at` ASC LIMIT 100 ";
        $sqlQuery = $read->query($sql);
        //Here let's prepare the data for insert and update
        while ($row = $sqlQuery->fetch()) {
            $rowasarr = unserialize($row['data']);
            $batchdata[$rowasarr['CODIGO']] = array(
                'bridge_channel_id' => $channel_id,
                'ean' => $rowasarr['EAN'],
                'codigo' => $rowasarr['CODIGO'],
                'data' => $row['data'],
                'pim_updated_at' => strtotime(date('Y-m-d', $row['created_at']))
            );
            $datafound[] = "'" . $rowasarr['CODIGO'] . "'";
            $batchdataid[] = $row['id'];
        }

        /** Migration process started here * */
        if (!empty($datafound)) {
            //Here to fetch data those are already migrated
            $codigolist = implode(',', $datafound);
            $idlist = implode(',', $batchdataid);
            $allnatiosdatasql = "SELECT `id`,`codigo` FROM $allnationstable WHERE `bridge_channel_id`=$channel_id AND `codigo` IN ($codigolist)";
            $codgiquery = $read->query($allnatiosdatasql);
            while ($row = $codgiquery->fetch()) {
                //Here let's update the informations
                $foundcodigo = $row['codigo'];
                $write->beginTransaction();
                $fields = $batchdata[$foundcodigo];
                $where = $write->quoteInto('id =?', $row['id']);
                $write->update($allnationstable, $fields, $where);
                $write->commit();
                unset($batchdata[$foundcodigo]);
            }
            //Here let's batch insert the data to table if found
            if (!empty($batchdata)) {
                $write->insertMultiple($allnationstable, $batchdata);
            }
            //Here to update the flag as migrated
            $write->beginTransaction();
            $updatesql = "UPDATE $table SET `is_migrated` = 1 WHERE id IN ($idlist) ";
            $write->query($updatesql);
            $write->commit();
        }
        /** Migration process End  * */
        //Here to remove those records been processed
        if (empty($datafound)) {
            $condition = array($write->quoteInto('is_migrated=?', '1'));
            $write->delete($table, $condition);

            $lastupdateddate = date('Y-m-d');
            $write->beginTransaction();
            $fields = array('initiated' => 0, 'processed' => 1, 'lastupdateddate' => $lastupdateddate);
            $where = $write->quoteInto('channel_id =?', $channel_id);
            $write->update($logtable, $fields, $where);
            $write->commit();

            //Here let's remove the migration cron
            $cronurl = Mage::helper("adminhtml")->getUrl('*/allnations/synctotemptable');
            Mage::helper('xmlcron')->removeXmlCron('*/1 * * * *', $cronurl);
        }
    }

    public function instantautoprocessAction() {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $logtable = $resource->getTableName('allnations/allnationslastupdatedat');
        $logsql = "SELECT `channel_id` FROM $logtable WHERE `processed` = 0 LIMIT 1 ";
        $loginfo = $read->fetchOne($logsql);
        $channel_id = $loginfo['channel_id'];
        if ($channel_id) {
            $this->parseXmlAction($channel_id);
        }
    }

    public function manageAllnationsDataCron($data) {
        if ($channel_id = $data['id']) {
            $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
            //Here to get channel id
            $logtable = $resource->getTableName('allnations/allnationslastupdatedat');
            $channel_id = $data['id'];
            $logsql = "SELECT `lastupdateddate`,`processed` FROM $logtable WHERE `channel_id`=$channel_id AND `processed` = 0 LIMIT 1 ";
            $loginfo = $read->fetchAll($logsql)[0];
            //End
            if (!empty($loginfo)) {
                $removecron = 0;
                //Here let's set the CRON to fetch updated data from Allnations
                if ($data['pim_cron'] == 'never' || $data['edi_cron'] == 'never' || $data['channel_status'] == '0' || $data['mode'] == 'demo') {
                    $removecron = 1;
                } else if ($data['product_info'] == '0' && $data['product_pricenquantity'] == '0') {
                    $removecron = 1;
                } else if (($data['edi_cron'] != 'never' || $data['product_pricenquantity'] == '1') && $data['create_new'] == '0') {
                    $removecron = 1;
                }
                $fetchdatacronurl = Mage::helper("adminhtml")->getUrl("*/allnations/getlatestallnationsdata");
                $pimfreq = Mage::helper('allnations')->getCrondaysFromOptions($data['pim_cron']);
                $edifreq = Mage::helper('allnations')->getCrondaysFromOptions($data['edi_cron']);
                if ($edifreq < $pimfreq) {
                    $final_cron_timing = $edifreq;
                    $final_cron_opt = $data['edi_cron'];
                } else {
                    $final_cron_timing = $pimfreq;
                    $final_cron_opt = $data['pim_cron'];
                }
                //Here to check the cron frequency greater than 0
                if ($final_cron_timing) {
                    //Here let's get cron timing options
                    if ($removecron) {
                        Mage::helper('xmlcron')->removeXmlCron($final_cron_opt, $fetchdatacronurl);
                    } else {
                        Mage::helper('xmlcron')->setXmlCron($final_cron_opt, $fetchdatacronurl);
                    }
                }
            }
        }
    }

    public function connectoallnationsAction($usrname = '', $passwd = '', $lastdate = '') {
        $today = ($lastdate) ? $lastdate : Mage::getModel('core/date')->date('Y-m-d');
        $uname = ($usrname) ? $usrname : $this->getRequest()->getParam('ussername');
        $pass = ($passwd) ? $passwd : $this->getRequest()->getParam('password');
        if ($uname && $pass) {
            $allnationsurl = "http://wspub.allnations.com.br/wsIntEstoqueClientes/ServicoReservasPedidosExt.asmx/RetornarListaProdutos?CodigoCliente=$uname&Senha=$pass&Data=$today";
            $rep = file_get_contents($allnationsurl);
            if ($usrname && $passwd) {
                return $rep;
            } else {
                if ($rep) {
                    echo "Connection Succed";
                } else {
                    echo "Connection Failed";
                }
            }
        } else {
            echo $this->__('Allnations Usernameand Password not found!;');
        }
    }

    public function getAttributeOptions($store_id) {
        $defaultstoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
        if (!$store_id)
            $store_id = $defaultstoreId;
        $resource = Mage::getSingleton('core/resource');
        $result = array();
        $readConnection = $resource->getConnection('core_read');
        $label_sql1 = "SELECT eav_attribute.attribute_code attribute_id,optvalues.option_id,optvalues.value FROM
        eav_attribute_option AS options  JOIN eav_attribute ON options.attribute_id=eav_attribute.attribute_id JOIN eav_attribute_option_value  AS optvalues ON options.option_id=optvalues.option_id WHERE store_id=$store_id AND eav_attribute.entity_type_id=4";
        $result = $readConnection->fetchAll($label_sql1);
        return $result;
    }

    public function product_createAction() {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $write = $resource->getConnection("core_write");
        $table = $resource->getTableName('allnations/allnationsdata');
        $mapping_table = $resource->getTableName('bridge/incomingchannels_mapping');
        $incomingchannel_table = $resource->getTableName('bridge/incomingchannels');
        $logtable = $resource->getTableName('allnations/allnationslastupdatedat');
        $logsql = "SELECT channel_id,sync_to_store FROM $logtable LIMIT 1 ";
        $channelinfo = $read->fetchAll($logsql);
        $channel_id = $channelinfo[0]['channel_id']; //$read->fetchAll($logsql);
        $logarr = array(
            'bridge_channel_id' => $channel_id,
            'channel_type' => 'allnations',
            'status' => 'PIM',
        );
        //Here if channel sync status stopped
        if (!$channelinfo[0]['sync_to_store']) {
            $logarr['sku'] = "AN8000";
            $logarr['status_code'] = "AN8000";
            Mage::getModel('bridge/incomingchannels')->savelog($logarr);
            return;
        }
        $model = Mage::getModel('bridge/incomingchannels');
        $channelobj = $model->load($channel_id);
        if (!$model->getId()) {
            $logarr['sku'] = "AN4000";
            $logarr['status_code'] = "AN4000";
            Mage::getModel('bridge/incomingchannels')->savelog($logarr);
            return;
        }
        $mode_channel = $model->getMode();
        $model_mapping = Mage::getModel('bridge/incomingchannels_mapping')->loadAllByBrandId($channel_id)->getData();
        $mode_channel = $model->getMode();
        $status_channel = $model->getStatus();
        $create_new = $model->getCreateNew();
        $update_exist = $model->getUpdateExist();
        $multi_price_rule = $model->getMultiPriceRule();
        $content_source = $model->getContentSource();
        $qty = $model->getAmazonQuantity();
        $currency = ($model->getCurrency()) ? $model->getCurrency() : NULL;
        $product_status = $model->getProductStatus();
        $syncpim = $model->getProductInfo();
        $syncedi = $model->getProductPricenquantity();
        $incomingcurrency = $model->getCurrency();
        $slab = $model->getSlab();
        $slaborder = $model->getChannelOrder();
        $others = unserialize($model->getOthers());
        $defaultqty = intval($others['product_defaultquantity']);
        $defaultqty = ($defaultqty) ? $defaultqty : 10;

        //If Channel is inactive then do nothing
        if (!$status_channel) {
            $logarr['sku'] = "AN5000";
            $logarr['status_code'] = "AN5000";
            Mage::getModel('bridge/incomingchannels')->savelog($logarr);
            return;
        }
        //End
        //Here let's pick one fresh record
        $picksql = "SELECT * FROM $table WHERE is_synced = 0 AND is_skipped = 0 ORDER BY `created_at` LIMIT 1";
        $pickdata = $read->fetchAll($picksql)[0];
        $updid = $pickdata['id'];
        // product status to be in magento store when created new
        $product_status = 2;
        if ($mode_channel == 'live') {
            if ($model->getProductStatus() == 1)
                $product_status = 1;
        }else {
            //Here in Demo mode
            //This will create only 10 products the date channel status become demo
            $demomodesql = "SELECT count(*) AS demoqouta FROM $table WHERE is_synced = 1 ";
            $demomodecnt = $read->fetchOne($demomodesql);
            if ($demomodecnt > 10) {
                $logarr['sku'] = "AN6000";
                $logarr['status_code'] = "AN6000";
                Mage::getModel('bridge/incomingchannels')->savelog($logarr);
                return;
            }
        }

        $edimodel = Mage::getModel('bridge/incomingchannels_product');
        $attrmapinginfo = unserialize($model_mapping[0]['data'])['import'];
        $finalproductdata = $cleanedprdinfo = array();
        $productinfo = unserialize($pickdata['data']);
        foreach ($productinfo as $k => $v) {
            $cleanedprdinfo[$k] = Mage::helper('allnations')->clean($v);
        }
        $sku = $price = 0;


        //attribute set mapping
        if ($attrmapinginfo['attributesettype'] == 1) {
            $attrset_id = $attrmapinginfo['default_attribute_set'];
            $mapped_option = $attrmapinginfo['option_mapping']['all'] ? $attrmapinginfo['option_mapping']['all'] : array();
            $mapped_Attribute = $attrmapinginfo['attributes'][$attrset_id];
            $custom_maping = $attrmapinginfo['custom_maping'][$attrset_id];
        } else {
            if (isset($attrmapinginfo['attributeset'][$cleanedprdinfo[$attrmapinginfo['csvattrsetcol']]])) {
                $attrset_id = $attrmapinginfo['attributeset'][$cleanedprdinfo[$attrmapinginfo['csvattrsetcol']]];
                $mapped_option = $attrmapinginfo['option_mapping'][strtolower($cleanedprdinfo[$attrmapinginfo['csvattrsetcol']])] ? $attrmapinginfo['option_mapping'][strtolower($productinfo[$attrmapinginfo['csvattrsetcol']])] : array();
                $mapped_Attribute = $attrmapinginfo['attributes'][strtolower($cleanedprdinfo[$attrmapinginfo['csvattrsetcol']])];
                $custom_maping = $attrmapinginfo['custom_maping'][strtolower($cleanedprdinfo[$attrmapinginfo['csvattrsetcol']])];
            } else {
                //Here something wrong
                $logarr['sku'] = $productinfo['CODIGO'];
                $logarr['status_code'] = "AN1000";
                Mage::getModel('bridge/incomingchannels')->savelog($logarr);
                //Here let's skip this product
                $write->update($table, array('is_skipped' => 1), "id=$updid");
                //End
                return;
            }
        }
        //end
        $finalproductdata['attribute_set_id'] = $attrset_id;



        //Here adding supplier to this product
        $supplier = $channelobj->getData('channel_supplier');
        $supplierattributecode = Mage::helper('bridge')->getSupplierCode();
        $finalproductdata[$supplierattributecode] = $supplier;
        //End

        if (!empty($attrmapinginfo)) {
            $patchfix = $cleanedprdinfo[$attrmapinginfo['csvattrsetcol']];
            if (isset($attrmapinginfo['attributes'][$attrset_id]) || isset($attrmapinginfo['attributes'][$patchfix])) {
                if (isset($attrmapinginfo['attributes'][$patchfix])) {
                    $mappedkey = $patchfix;
                } else {
                    $mappedkey = $attrset_id;
                }
                foreach ($attrmapinginfo['attributes'][$mappedkey] as $k => $v) {
                    if($k == 'ORIGEMPRODUTO'){
                        $productinfo[$k] = Mage::helper('allnations')->clean($productinfo[$k]);
                    }
                    $$v = $finalproductdata[$v] = $productinfo[$k];
                }
            } else {
                //Here something wrong 
                $logarr['sku'] = $productinfo['CODIGO'];
                $logarr['status_code'] = "AN9000";
                Mage::getModel('bridge/incomingchannels')->savelog($logarr);
                //Here let's skip this product
                $write->update($table, array('is_skipped' => 1), "id=$updid");
                //End
                return;
            }
        }

        $qty = ($fedobe_quantity) ? $defaultqty  : $fedobe_quantity;
        
        //Here added code if no sku found then use Allnations CODIGO as sku
        if ($sku) {
            $finalproductdata['sku'] = $sku;
        } else {
            $finalproductdata['sku'] = $productinfo['CODIGO'];
            $logarr['sku'] = $productinfo['CODIGO'];
            $logarr['status_code'] = "AN14000";
            Mage::getModel('bridge/incomingchannels')->savelog($logarr);
        }
        //End
        //Here globally assign sku to all such log
        $logarr['sku'] = $finalproductdata['sku'];
        //End
        //Here to decide the quantity availability
        
        unset($finalproductdata['fedobe_availability']);
        $finalproductdata['short_description'] = ($finalproductdata['description']) ? mb_substr($finalproductdata['description'], 0, 50) : $finalproductdata['name'];
        $finalproductdata['tax_class_id'] = 0;
        $finalproductdata['type_id'] = 'simple';
        $finalproductdata['created_at'] = date('Y-m-d H:i:s', strtotime($productinfo['TIMESTAMP']));
        $finalproductdata['bridge_channel_id'] = $channel_id;
        $finalproductdata['bridge_pricenqty_channel_id'] = $channel_id;
        $finalproductdata['bridge_content_edited'] = 0;
        //storeview mapping
        if ($attrmapinginfo['storeviewtype'] == 1) {
            $store_view_map = $attrmapinginfo['allstoreviewmap'];
        } else {
            if (isset($attrmapinginfo['storeview_mapping'][$productinfo[$attrmapinginfo['storeviewmappingcolname']]]))
                $store_view_map = $attrmapinginfo['storeview_mapping'][$productinfo[$attrmapinginfo['storeviewmappingcolname']]];
            else {
                //Here something went wrong in store view mapping
                $logarr['status_code'] = "AN3000";
                Mage::getModel('bridge/incomingchannels')->savelog($logarr);
                //Here let's skip this product
                $write->update($table, array('is_skipped' => 1), "id=$updid");
                //End
                return;
            }
        }
        //Here to set the website ids
        $websites = Mage::app()->getWebsites();
        $websiteids = array();
        foreach ($websites as $wk => $wv) {
            $websiteid[$wv->getId()] = $wv->getId();
        }
        //End

        $finalproductdata['website_ids'] = $websiteid;
        $finalproductdata['store_id'] = ($store_view_map) ? $store_view_map : Mage_Core_Model_App::ADMIN_STORE_ID;
        //end
        //cateory mapping

        if ($attrmapinginfo['categorymaptype'] == 1) {
            $category_ids = $attrmapinginfo['allcategoryintomap'];
        } else {
            if (isset($attrmapinginfo['category_mapping'])) {
                $prd_dept = Mage::helper('allnations')->clean($productinfo['DEPARTAMENTO']);
                $prd_cat = Mage::helper('allnations')->clean($productinfo['CATEGORIA']);
                $prd_subcat = Mage::helper('allnations')->clean($productinfo['SUBCATEGORIA']);
                $findcatmapaar = array($prd_dept, $prd_cat, $prd_subcat);
                foreach ($findcatmapaar as $catk => $catv) {
                    $catmapfound = $attrmapinginfo['category_mapping'][$catv];
                    if ($catmapfound) {
                        foreach ($catmapfound as $cmk => $cmv) {
                            $category_ids[$cmv] = $cmv;
                        }
                    }
                }
            }
        }
        if (!$category_ids) {
            //Here Something went wrong in category
            $logarr['status_code'] = "AN2000";
            Mage::getModel('bridge/incomingchannels')->savelog($logarr);
            //Here let's skip this product
            $write->update($table, array('is_skipped' => 1), "id=$updid");
            //End
            return;
        }

 $productinfo['DESCRICAO'] = nl2br($productinfo['DESCRICAO']);
        $productinfo['DESCRTEC'] = nl2br($productinfo['DESCRTEC']);

        //Here hardcoding Name ,description , short description
        $finalproductdata['name'] = $productinfo['DESCRICAO'];
        $finalproductdata['description'] = $productinfo['DESCRTEC'];
        $finalproductdata['short_description'] = $productinfo['DESCRTEC'];
        //end

        $finalproductdata['category_ids'] = $category_ids;
        //Here goes the product status setting
        $finalproductdata['status'] = ($model->getProductStatus() == 3 && $finalproductdata['status']) ? $finalproductdata['status'] : $product_status;
        $finalproductdata['visibility'] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
        $finalproductdata['entity_type_id'] = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();

        //Here added values for incoming channels data
        $finalproductdata['inc_name'] = $finalproductdata['name'];
        $finalproductdata['inc_description'] = $finalproductdata['description'];
        $finalproductdata['inc_short_description'] = $finalproductdata['short_description'];


        $raw_ret_options = $this->getAttributeOptions(0);
        $ret_options = array();
        foreach ($raw_ret_options as $optk => $optv) {
            $ret_options[$optv['attribute_id']][$optv['option_id']] = htmlspecialchars(strtolower($optv['value']), ENT_QUOTES);
        }
        $finalproductdata = $this->map_option($finalproductdata, $mapped_option, $mapped_Attribute, $ret_options, 0);


        //Here let's check and decide whether to sync product infomations
        $skip = $codigo = 0;
        if ($syncpim) {
            //Here let's check whether to create new or update existing products
            if ($create_new || $update_exist) {
                $product = Mage::getmodel('catalog/product');
                $product_exist = $product->loadByAttribute('sku', $sku);
                if ($product_exist) {
                    $product_id = $product_exist->getId();
                    //Here to handle duplicate product with same part number
                    //Here you will use the codigo as SKU and notify the user to review manually
                    $duplsql = "SELECT * FROM $table WHERE id_product = $product_id AND bridge_channel_id = $channel_id LIMIT 1";
                    $dupldata = $read->fetchAll($duplsql)[0];
                    if(!empty($dupldata)){
                        $currentproceescodigo = $pickdata['codigo'];
                        $extprodcutcodigo = $dupldata['codigo'];
                        if($currentproceescodigo == $extprodcutcodigo){
                            //Update
                            if ($update_exist) {
                                $finalproductdata['entity_id'] = $product_id;
                            } else {
                                //Do not update simple flag to skip 
                                $skip = 1;
                            }
                        }  else {
                            //Here assign Codigo as product SKU
                            $finalproductdata['sku'] = $currentproceescodigo;
                            $codigo = 1;
                        }
                    }else{
                         //Do not update simple flag to skip 
                         $skip = 1;
                    }
                }
                try {
                    if (!$skip) {
                        $product->setStoreId($finalproductdata['store_id'])->setData($finalproductdata);
                        if ($finalproductdata['entity_id']) {
                            $logarr['status_code'] = "AN11000";
                            Mage::getModel('bridge/incomingchannels')->savelog($logarr);
                        } else {
                            $logarr['status_code'] = "AN12000";
                            Mage::getModel('bridge/incomingchannels')->savelog($logarr);
                        }
                        $product->save();
                        
                        //Here to update the price according to the price rule
                        $convprice = Mage::helper('bridge')->addPriceRule($channelobj->getData(), $product->getId(), $finalproductdata['price']);
                        $product->load($product->getId());
                        $product->addData(array('price' => $convprice,'stock_data' => array('qty' => $qty, 'is_in_stock' => ($qty ? 1 : 0 )), 'entity_id' => $product->getId()));
                        $product->save();
                        
                        if($codigo){
                            $logarr['status_code'] = "AN30000";
                            Mage::getModel('bridge/incomingchannels')->savelog($logarr);
                        }
                        //End
                        //Here let's set the image
                        if ($create_new) {
                            $mediaarr = array();
                            $allnationid = $productinfo['CODIGO'];
                            $imageurl = Mage::helper('allnations')->getAllnationsLargeImageUrlPath($allnationid);
                            for ($i = 1; $i <= 5; $i++) {
                                $formattedimgurl = "$imageurl-" . sprintf("%02d", $i);
                                $imageurlinfo = getimagesize($formattedimgurl);
                                if ($imageurlinfo[0] > 2000) {
                                    $mediaarr[] = array('url' => $formattedimgurl);
                                } else {
                                    break;
                                }
                            }
                            if (!empty($mediaarr)) {
                                $product = $product->load($product->getId());
                                Mage::helper('allnations')->addProductGalleryImages($mediaarr, $allnationid, $product);
                            } else {
                                $logarr['status_code'] = "AN13000";
                                Mage::getModel('bridge/incomingchannels')->savelog($logarr);
                            }
                            //Here let's update the product id and sku
                            $updid = $pickdata['id'];
                            $write->update($table, array('is_synced' => 1, 'id_product' => $product->getId(), 'sku' => $sku), "id=$updid");
                        }
                        //End
                    } else {
                        $logarr['status_code'] = "AN10000";
                        Mage::getModel('bridge/incomingchannels')->savelog($logarr);
                        $write->update($table, array('is_skipped' => 1), "id=$updid");
                    }
                } catch (Exception $ex) {
                    echo "<pre>";
                    print_r($ex->getMessage());
                    exit;
                }
            }
        }
        //End
        //Here to sync EDI
        if ($syncedi) {
            if ($sku && $price) {
                //Here add the EDI informations as mapping found
                $edimodel->saveSkuInfo($channel_id, $incomingcurrency, $sku, $price, $qty, $slab, $slaborder, $convprice);
                $logarr['status_code'] = "AN20000";
                Mage::getModel('bridge/incomingchannels')->savelog($logarr);
                $write->update($table, array('is_synced' => 1), "id=$updid");
            } else {
                //Here to raise the notification to have SKU,Price and Quantity attribute mapping
                $logarr['status_code'] = "AN7000";
                Mage::getModel('bridge/incomingchannels')->savelog($logarr);
                $write->update($table, array('is_skipped' => 1), "id=$updid");
            }
        }
        //End
    }

    function map_option($array, $option, $mapped_attribute, $ret_options, $option_create) {

        //map the mpped options
        if ($option):
            foreach ($option as $option_key => $option_value) {
                $moption_key = $mapped_attribute[$option_key];
                if (isset($array[$moption_key]) && isset($option[$option_key][$array[$moption_key]])) {
                    $array[$moption_key] = $option[$option_key][$array[$moption_key]];
                    unset($ret_options[$mapped_attribute[$option_key]]);
                }
            }
        endif;
        if ($ret_options)
            $attribute_intersect = array_intersect_key(array_flip($mapped_attribute), $ret_options);

        if ($attribute_intersect)
            $attribute_mapped_intersect = array_intersect_key($array, array_flip($attribute_intersect));
        //map the unmapped options for hich only attribute is map
        if ($attribute_mapped_intersect) {
            foreach ($attribute_mapped_intersect as $csv_attr => $option_val) {
                if (in_array($option_val, $ret_options[$mapped_attribute[$csv_attr]]))
                    $array[$csv_attr] = array_search($option_val, $ret_options[$mapped_attribute[$csv_attr]]);
                else if ($option_create) {
                    //create new option if not exist
                    $array[$csv_attr] = Mage::getmodel('bridge/incomingchannels')->add_new_option($mapped_attribute[$csv_attr], $option_val);
                }
            }
        }
        return $array;
    }

    public function getlatestallnationsdataAction() {
        $model = Mage::getModel('bridge/incomingchannels');
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $write = $resource->getConnection("core_write");
        $logtable = $resource->getTableName('allnations/allnationslastupdatedat');
        $datalogtable = $resource->getTableName('allnations/allnationsdatalog');
        $logsql = "SELECT `channel_id`,`lastupdateddate` FROM $logtable LIMIT 1 ";
        $loginfo = $read->fetchAll($logsql)[0];
        $lastupdated_date = $loginfo['lastupdateddate'];
        $current_date = date('Y-m-d');
        $channel_id = $loginfo['channel_id'];
        $allnationsdata = $model->load($channel_id)->getData();

        //Step one to download the data since last updated
        $userinfo = unserialize($allnationsdata['others']);
        $usrname = $userinfo['allnations_username'];
        $passwd = $userinfo['allnations_password'];

        $logfilename = $this->saveAllnationsXmlData($usrname, $passwd, $current_date, $channel_id);
        if ($logfilename) {
            $logdata = array('lastupdateddate' => $lastupdated_date, 'filename' => $logfilename);
            $write->update($logtable, $logdata, "channel_id=$channel_id");
            //Step two to Auto process the data silently
            //This process will auto creat the migration CRON once XML data process and saved to temp table
            //Here to set one cron for instant processing 
            $autoprocesstime = "*/1 * * * *";
            $autoprocesscronurl = Mage::helper("adminhtml")->getUrl("*/allnations/instantautoprocess");
            Mage::helper('xmlcron')->setXmlCron($autoprocesstime, $autoprocesscronurl);
            
            //Here let's clean the directory
            $cleanpath = Mage::helper('allnations')->getAllnationsDataDirectory() . DS . "*";
            $files = glob($cleanpath);
            foreach ($files as $file) {
                if (is_file($file)) {
                    $name_of_file = pathinfo($file)['basename'];
                    if ($name_of_file != $logfilename) {
                        unlink($file);
                    }
                }
            }
            //Here to maintain one log for allnations data
            $inssql = "INSERT INTO $datalogtable (`bridge_channel_id`,`last_updated_at`,`updated_at`) VALUES ($channel_id,'$lastupdated_date','$current_date') ";
            $write->query($inssql);
        }
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction() {
        $this->_initAction();
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('bridge/incomingchannels');
        $allnations_model = Mage::getModel('bridge/incomingchannels_mapping');
        if ($id) {
            $model->load($id);
            $allnations_model->loadAllByBrandId($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('bridge')->__('This Channel no longer exists.')
                );
                $this->_redirect('*/*');
                return;
            }
            $mapping_Data = Mage::getmodel('bridge/incomingchannels_mapping')->getCollection()->addFieldToFilter('brand_id', $id)->addFieldToSelect('data')->getFirstItem()->getData();
            $mapping_Data = @$mapping_Data['data'];
            $store_view = Mage::getmodel('bridge/storeview_label')->getCollection()
                            ->addFieldToSelect(array('value', 'type'))
                            ->addFieldToFilter('brand_id', $id)
                            ->addFieldToFilter('store_id', $this->getCurrentStore())
                            ->addFieldToFilter('type', array('in' => array('meta_title', 'meta_description')))->getData();
            foreach ($store_view as $view) {
                $$view['type'] = $view['value'];
            }
            $model->setMetaTitle(@$meta_title);
            $model->setMetaDescription(@$meta_description);
            $_SESSION['channel_type'] = $model->getChannelType();
            $_SESSION['brand_id'] = $id;
        }

        $this->_title($model->getId() ? $model->getBrandName() : $this->__('New Incomingchannel'));
        $data = Mage::getSingleton('adminhtml/session')->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }
        if (@$mapping_Data) {
            $model->setMappingData($mapping_Data);
        }
        Mage::register('incomingchannel_data', $model);
        Mage::register('allnationsmapping_data', $allnations_model);
        $this->loadLayout();
        //remove store switcher for new add of a channel
        if (!$id) {
            $this->getLayout()->getBlock('left')->unsetChild('store_switcher');
        }
        $this->renderLayout();
    }

    public function saveAction() {
        $data = $this->getRequest()->getPost();
        if ($data) {
            $resource = Mage::getSingleton('core/resource');
            $write = $resource->getConnection('core_write');
            $read = $resource->getConnection('core_read');
            try {
                $data['channel_type'] = 'allnations';
                Mage::getSingleton('adminhtml/session')->setPageData($data);
                $model = Mage::getModel('bridge/incomingchannels');
                $model->load($this->getRequest()->getParam('id'));
                $data['status'] = $data['channel_status'];
                $data['authorized'] = 1;
                $usrname = $data['others']['allnations_username'];
                $passwd = $data['others']['allnations_password'];
                if ($data['others']['allnations_lastupdateddate']) {
                    $fetchdate = strtotime($data['others']['allnations_lastupdateddate']);
                    $data['others']['allnations_lastupdateddate'] = date("Y-m-d", $fetchdate);
                }
                $lastupddate = $data['others']['allnations_lastupdateddate'];

                //Sync order setting
                $syncorder = $data['others']['sync_channel_order'];

                $data['others'] = serialize($data['others']);

                $model->setData($data);
                $model->save();

                if ($this->getRequest()->getParam('id')) {
                    $resource = Mage::getSingleton('core/resource');
                    $writeConnection = $resource->getConnection("core_write");
                    //if slab or order is changed the ulpdate this in edi table if any inventory is exist for this channel
                    $bridge_edi_crontable = $resource->getTableName('bridge/incomingchannels_product');
                    $writeConnection->raw_query("UPDATE $bridge_edi_crontable SET `slab`={$data['slab']},`order`={$data['channel_order']},channel_supplier={$data['channel_supplier']} WHERE channel_type='incoming' AND channel_id={$model->getId()};");
                }


                //Here to download the xml file and save to process that file later
                //Allnations-2010-3-14-1.xml
                $channelid = $model->getId();
                $logtable = "bridge_allnationslastupdatedat";
                $sql = "SELECT * FROM $logtable LIMIT 1";
                $loginfo = $read->fetchAll($sql);
                $filename = $loginfo[0]['filename'];
                $flag = $processflag = 0;
                if ($filename) {
                    $datepat = "/$lastupddate/";
                    $match = preg_match($datepat, $filename);
                    if (!$match) {
                        //Here if the channel Allnations date settings changed
                        $unlinkfile = Mage::helper('allnations')->getAllnationsDataDirectory() . DS . $filename;
                        unlink($unlinkfile);
                        $where = "id = {$loginfo[0]['id']}";
                        $write->delete($logtable, $where);
                        $flag = 1;
                        $processflag = $loginfo[0]['processed'];
                    }
                } else {
                    $flag = 1;
                }
                if ($flag) {
                    //Here let's download the file and save
                    $logfilename = $this->saveAllnationsXmlData($usrname, $passwd, $lastupddate, $channelid);
                    if ($logfilename) {
                        $logdata = array('channel_id' => $channelid, 'lastupdateddate' => $lastupddate, 'filename' => $logfilename,'processed'=>$processflag);
                        $write->insert($logtable, $logdata);
                    }
                    //Here to set one cron for instant processing 
                    $autoprocesstime = "*/1 * * * *";
                    $autoprocesscronurl = Mage::helper("adminhtml")->getUrl("*/allnations/instantautoprocess");
                    Mage::helper('xmlcron')->setXmlCron($autoprocesstime, $autoprocesscronurl);
                }

                //Here to manage the Allnations Data Cron
                $this->manageAllnationsDataCron($data);
                //End
                //Here to add CRON for allnations order syncing
                $crontiming = "*/5 * * * *";
                $ordersync_cronurl = Mage::helper("adminhtml")->getUrl("*/allnations/syncordertoallnations");
                if ($syncorder) {
                    Mage::helper('xmlcron')->setXmlCron($crontiming, $ordersync_cronurl);
                } else {
                    Mage::helper('xmlcron')->removeXmlCron($crontiming, $ordersync_cronurl);
                }
                //End
                //Here to store the mapping information
                $mappingdata = array();
                $mappingkeys = array('storeviewtype', 'storeviewmappingcolname', 'storeviewmappingcol', 'categorymaptype', 'csvcategorycolumnname', 'csvcategorycolumn', 'attributesettype', 'mapping', 'csvattrsetcol', 'allcategoryintomap', 'allstoreviewmap');
                foreach ($mappingkeys as $k => $v) {
                    if (isset($data[$v])) {
                        $mappingdata[$v] = $data[$v];
                        unset($data[$v]);
                    }
                }

                if ($mappingdata) {
                    $mapping['brand_id'] = $model->getId();
                    //make the data format in the way will efficient for at time of product mapping
                    $import_mapping = $mappingdata;
                    $only_mapping = $import_mapping['mapping'];
                    if ($mappingdata['csvcategorycolumn']) {
                        unset($import_mapping['csvcategorycolumn']);
                        foreach ($mappingdata['csvcategorycolumn']['csvcategory'] as $csv_map_key => $csv_map_value) {
                            $import_mapping['category_mapping'][$csv_map_value[0]] = is_array($mappingdata['csvcategorycolumn']['storecategory'][$csv_map_key]) ? $mappingdata['csvcategorycolumn']['storecategory'][$csv_map_key] : $mappingdata['csvcategorycolumn']['storecategory'][$csv_map_key][0];
                        }
                    }
                    if ($mappingdata['storeviewmappingcol']) {
                        unset($import_mapping['storeviewmappingcol']);
                        foreach ($mappingdata['storeviewmappingcol'] as $storeview_map_value) {
                            $import_mapping['storeview_mapping'][$storeview_map_value['brand']] = $storeview_map_value['store'];
                        }
                    }
                    if ($mappingdata['attributesettype'] == 1) {
                        $import_mapping['default_attribute_set'] = $mappingdata['mapping']['storefieldset'][0];
                    }


                    $allowed_field_mappings = array('storefieldset', 'attributes', 'custommappingattr', 'brandfieldset', 'attributeoptions');
                    unset($import_mapping['mapping']);
                    foreach ($only_mapping as $mapping_name => $mapping_value) {
                        foreach ($mapping_value as $mapping_name_key => $mapping_name_value) {
                            if ($mapping_name == 'brandfieldset') {
                                $import_mapping['attributeset'][$mapping_name_value] = $only_mapping['storefieldset'][$mapping_name_key];
                            }
                            foreach ($mapping_name_value as $map_key => $map_value) {
                                if ($mapping_name == 'attributes') {
                                    $attribute_id = $mapping_name_key == 'all' ? $only_mapping['storefieldset'][0] : $mapping_name_key;
                                    $import_mapping['attributes'][$attribute_id][$map_value['brand']] = $map_value['store'];
                                } else if ($mapping_name == 'custommappingattr') {
                                    if ($only_mapping['custommappingvalue'][$mapping_name_key][$map_key] != '')
                                        $import_mapping['custom_maping'][$mapping_name_key][$map_value] = $only_mapping['custommappingvalue'][$mapping_name_key][$map_key];
                                } else if ($mapping_name == 'attributeoptions') {
                                    foreach ($map_value as $opt_key => $opt_val)
                                        if ($opt_val != '')
                                            $import_mapping['option_mapping'][$mapping_name_key][$map_key][$opt_val['brand']] = $opt_val['store'];
                                }
                            }
                        }
                    }


                    $mapping['data'] = serialize(array('interface' => $mappingdata, 'import' => $import_mapping));
                }

                $mapping_model = Mage::getmodel('bridge/incomingchannels_mapping');
                $mapping['id'] = $mapping_model->getCollection()->addFieldToFilter('brand_id', $model->getId())->addFieldToSelect('id')->getFirstItem()->getId();
                if ($mapping['id'] && !$mapping['data']) {
                    $mapping_model->load($mapping['id']);
                    $mapping_model->delete();
                } else if ($mapping['data']) {
                    $mapping_model->setData($mapping);
                    $mapping_model->save();
                }
                //End 
                //add the incoming channel to attribute channel priority option
                if (!$this->getRequest()->getParam('id')) {
                    $option_id = Mage::getModel('bridge/incomingchannels')->addChannelOption($model->getBrandName());
                    $inc_model = Mage::getmodel('bridge/incomingchannels');
                    $inc_model->setData(array('id' => $model->getId(), 'option_id' => $option_id));
                    $inc_model->save();
                }
                //end

                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('bridge')->__('The Channel has been saved.')
                );
                Mage::getSingleton('adminhtml/session')->setPageData(false);
                $tab_name = ($data['tab_name']) ? $data['tab_name'] : 'main_section';
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        // The following line decides if it is a "save" or "save and continue"
        if ($this->getRequest()->getParam('back')) {
            $this->_redirect('*/allnations/edit', array('activeTab' => $tab_name, 'id' => $model->getId(), 'store' => $this->getRequest()->getParam('store')));
        } else {
            $this->_redirect('*/incomingchannels');
        }
    }

    public function saveAllnationsXmlData($usrname, $passwd, $lastupddate, $channelid) {
        $lastupddate = trim($lastupddate);
        $xmlresponse = $this->connectoallnationsAction($usrname, $passwd, $lastupddate);
        $filepath = Mage::helper('allnations')->getAllnationsDataDirectory();
        $filename = "Allnations-$lastupddate-$channelid.xml";
        if ($xmlresponse) {
            $fp = fopen($filepath . DS . $filename, 'w+');
            fwrite($fp, $xmlresponse);
            fclose($fp);
            return $filename;
        }
    }

    public function getCurrentStore() {
        $defaultstoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
        $store_id = Mage::app()->getRequest()->getParam('store') ? Mage::app()->getRequest()->getParam('store') : $defaultstoreId;
        return $store_id;
    }

    public function managesynccronAction() {
        $channel_id = $this->getRequest()->getPost('id');
        if ($channel_id) {
            $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
            $write = $resource->getConnection("core_write");
            $logtable = $resource->getTableName('allnations/allnationslastupdatedat');
            //Here let's update the sync flag automatically
            $sql = "UPDATE $logtable SET sync_to_store = IF(sync_to_store,0,1) WHERE channel_id=$channel_id ";
            $write->query($sql);
            //Here to do action for sync cron
            $logsql = "SELECT `sync_to_store` FROM $logtable WHERE channel_id=$channel_id LIMIT 1 ";
            $sync_to_store = $read->fetchOne($logsql);
            $syncurl = Mage::helper("adminhtml")->getUrl("*/allnations/product_create");
            if ($sync_to_store) {
                //Here to set the sync cron
                Mage::helper('xmlcron')->setXmlCron('*/1 * * * *', $syncurl);
            } else {
                //Here to remove the sync cron
                Mage::helper('xmlcron')->removeXmlCron('*/1 * * * *', $syncurl);
            }
            echo $sync_to_store;
        }
    }

    //This function will Sync the order to Allnations
    public function syncordertoallnationsAction() {
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $write = $resource->getConnection("core_write");
        $ordersynctable = $resource->getTableName('bridge/ordersync');
        $sql = "SELECT *,FLOOR(HOUR(TIMEDIFF(NOW(),`created_at`))) As `difftime` FROM $ordersynctable WHERE `channel_type`='allnations' AND `is_completed`=0 AND `is_cancelled` = 0 ORDER BY `updated_at` DESC LIMIT 5";
        $sqlattrQuery = $read->query($sql);
        while ($row = $sqlattrQuery->fetch()) {
            $channel_id = $row['channel_id'];
            $supplier_id = $row['supplier_id'];
            $ordertableid = $row['id'];
            $channeldetails = $this->getChannelinfo($channel_id, $supplier_id);
            if (!empty($channeldetails) && ($channeldetails['sync_channel_order'] && ($channeldetails['allnations_username'] && $channeldetails['allnations_password']))) {
                //Here let's check whether order has been created at Allnations of not
                $is_order_placed_at_allnations = $row['is_processed'];
                $is_changed = $row['is_changed'];
                $is_cancelled = $row['is_cancelled'];
                $is_confirmed = $row['is_confirmed'];
                $allnations_username = $channeldetails['allnations_username'];
                $allnations_password = $channeldetails['allnations_password'];
                $product_code = $row['incoming_product_code'];
                $customerorderstring = "QPORD{$row['order_id']}-{$row['item_id']}";
                $qty = $row['item_quantity'];
                if ($is_order_placed_at_allnations) {
                    //Here need to confirm or cancell based on status
                    if ($is_changed) {
                        switch ($row['order_status']) {
                            case 'complete':
                                if (!$is_confirmed) {
                                    $httpcode = $this->confirmOrderAtAllnations($allnations_username, $allnations_password, $customerorderstring, $qty);
                                    if ($httpcode == '200') {
                                        $sql = "UPDATE $ordersynctable SET `is_confirmed`=1 WHERE `id` = $ordertableid";
                                        $read->query($sql);
                                    }
                                }
                                $sql = "UPDATE $ordersynctable SET `is_completed`=1 WHERE `id` = $ordertableid";
                                $read->query($sql);
                                break;
                            case 'canceled':
                                if (!$is_cancelled) {
                                    $httpcode = $this->cancelOrderAtAllnations($allnations_username, $allnations_password, $customerorderstring, $qty);
                                    if ($httpcode == '200') {
                                        $sql = "UPDATE $ordersynctable SET `is_cancelled`=1 WHERE `id` = $ordertableid";
                                        $read->query($sql);
                                    }
                                }
                                break;
                            case 'processing':
                                if (!$is_confirmed) {
                                    $httpcode = $this->confirmOrderAtAllnations($allnations_username, $allnations_password, $customerorderstring, $qty);
                                    if ($httpcode == '200') {
                                        $sql = "UPDATE $ordersynctable SET `is_confirmed`=1 WHERE `id` = $ordertableid";
                                        $read->query($sql);
                                    }
                                }
                                break;
                        }
                    } else {
                        //Not changed since 90 hours, so let's auto cancel order
                        if ((!$is_cancelled && !$is_confirmed) && $row['difftime'] > 90) {
                            $httpcode = $this->cancelOrderAtAllnations($allnations_username, $allnations_password, $customerorderstring, $qty);
                            if ($httpcode == '200') {
                                $sql = "UPDATE $ordersynctable SET `is_cancelled`=1 WHERE `id` = $ordertableid";
                                $read->query($sql);
                            }
                        }
                    }
                } else {
                    if ($row['order_status'] == 'processing' || $row['order_status'] == 'new') {
                        //Here we need to reserve the order at allnations
                        $httpcode = $this->reserveOrderAtAllnations($allnations_username, $allnations_password, $customerorderstring,$product_code,$qty);
                        if ($httpcode == '200') {
                            //Here response returned as 200 that means order been created successfully
                            //Let's update the falg as is_processed to 1
                            $sql = "UPDATE $ordersynctable SET `is_processed`=1 WHERE `id` = $ordertableid";
                            $read->query($sql);
                        }
                    }
                }
            } else {
                //Here channel info credentials not found
            }
        }
    }

    public function reserveOrderAtAllnations($username, $password, $orderstring, $product_code, $qty) {
        $httpcode = $this->commonaction($username, $password, $orderstring, $qty, 'InserirReserva', $product_code);
        return $httpcode;
    }

    public function cancelOrderAtAllnations($username, $password, $orderstring, $qty) {
        $httpcode = $this->commonaction($username, $password, $orderstring, $qty, 'CancelarReserva');
        return $httpcode;
    }

    public function confirmOrderAtAllnations($username, $password, $orderstring, $qty) {
        $httpcode = $this->commonaction($username, $password, $orderstring, $qty, 'ConfirmarReserva');
        return $httpcode;
    }

    public function getChannelinfo($channel_id, $supplier_id) {
        $channel_type = '';
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $incomingchanneltable = $resource->getTableName('bridge/incomingchannels');
        $logsql = "SELECT `others` FROM $incomingchanneltable WHERE id = $channel_id AND channel_supplier = $supplier_id LIMIT 1 ";
        $others = $read->fetchOne($logsql);
        $others = (!empty($others)) ? unserialize($others) : array();
        return $others;
    }

    public function get_contents($url) {
        $response = file_get_contents($url);
        $headers = $http_response_header;
        return array('response' => $response, 'headers' => $headers);
    }

    function parseHeaderstoGetCode($headers) {
        $httpcode = '';
        foreach ($headers as $k => $v) {
            if (preg_match("#HTTP/[0-9\.]+\s+([0-9]+)#", $v, $out)) {
                $httpcode = intval($out[1]);
                break;
            }
        }
        return $httpcode;
    }

    public function commonaction($username, $password, $orderstring, $qty, $action, $product_code = '') {
        if ($qty < 10) {
            $url = "http://wspub.allnations.com.br/wsIntEstoqueClientes/ServicoReservasPedidosExt.asmx/$action?CodigoCliente=$username&Senha=$password&PedidoCliente=$orderstring&CodigoProduto=$product_code&Qtd=$qty";
            $result = $this->get_contents($url);
            $httpcode = $this->parseHeaderstoGetCode($result['headers']);
        } else {
            $loop = ($qty % 10);
            for ($i = 0; $i < $loop; $i++) {
                $orderstring = $orderstring . "-$i";
                $url = "http://wspub.allnations.com.br/wsIntEstoqueClientes/ServicoReservasPedidosExt.asmx/$action?CodigoCliente=$username&Senha=$password&PedidoCliente=$orderstring&CodigoProduto=$product_code&Qtd=9";
                $result = $this->get_contents($url);
                $httpcode = $this->parseHeaderstoGetCode($result['headers']);
            }
        }
        return $httpcode;
    }

    //End
}
