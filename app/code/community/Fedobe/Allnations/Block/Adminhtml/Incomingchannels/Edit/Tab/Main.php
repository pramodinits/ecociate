<?php

class Fedobe_Allnations_Block_Adminhtml_Incomingchannels_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel() {
        return Mage::helper('allnations')->__('Settings');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle() {
        return Mage::helper('allnations')->__('Settings');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab() {
        if ($this->getRequest()->getParam('id') || @$this->getRequest()->getParam('channel_type'))
            return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden() {
        if (!@$this->getRequest()->getParam('id') && !@$this->getRequest()->getParam('channel_type'))
            return false;
    }

    protected function _prepareForm() {
        $model = Mage::registry('incomingchannel_data');
        $data = $model->getData();
        $otherarrdata = unserialize($data['others']);
        foreach ($otherarrdata as $k => $v) {
            $data[$k] = $v;
        }
        $form = new Varien_Data_Form();
        $form->setFieldsetRenderer($this->getLayout()->createBlock('bridge/adminhtml_managebridgecontent_form_renderer_fieldset'));

        $fieldset1 = $form->addFieldset('channel_settings', array(
            'legend' => Mage::helper('allnations')->__('Channel Settings'),
            'fieldset_container_id' => 'channel_settings'
                )
        );
        $fieldset1->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
        $fieldset1->addField('general_informations', 'hbar', array(
            'id' => 'General Informations'
        ));
        $fieldset1->addField('tab_name', 'hidden', array(
                'name' => 'tab_name',
            ));
        if ($model->getId())
            $fieldset1->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        if ($model->getChannelType()) {
            $fieldset1->addField('channel_type', 'hidden', array('name' => 'channel_type'));
        }


        $fieldset1->addField('brand_name', 'text', array(
            'name' => 'brand_name',
            'label' => Mage::helper('allnations')->__('Name'),
            'title' => Mage::helper('allnations')->__('Name'),
            'required' => true,
        ));
        //Add supplier dropdown
        $get_supplier_list=Mage::helper('allnations')->getSupplierList();
        $fieldset1->addField('channel_supplier', 'select', array(
            'name' => 'channel_supplier',
            'label' => Mage::helper('bridgecsv')->__('Partner'),
            'title' => Mage::helper('bridgecsv')->__('Partner'),
            'options' => $get_supplier_list,
            'required' => true,
        ));
        //end
        
        $fieldset1->addField('mode', 'select', array(
            'name' => 'mode',
            'label' => Mage::helper('allnations')->__('Mode'),
            'title' => Mage::helper('allnations')->__('Mode'),
            'required' => true,
            'note' =>"If mode set to Demo then only 10 products will be synced,before demo mode if 10 or more products synced then this channel will be like as if inactive",
            'options' => array('demo' => Mage::helper('allnations')->__('Demo'), 'live' => Mage::helper('allnations')->__('Live')),
        ));
        $fieldset1->addField('status', 'select', array(
            'name' => 'channel_status',
            'label' => Mage::helper('allnations')->__('Status'),
            'title' => Mage::helper('allnations')->__('Status'),
            'options' => array(1 => Mage::helper('allnations')->__('Active'), 0 => Mage::helper('allnations')->__('Inactive')),
            'after_element_html' => $afterstatushtml,
            'required' => true,
        ));

        $fieldset1->addField('allnations_credentials', 'hbar', array(
            'id' => 'Allnations Credentials'
        ));

        $ftpusername = $fieldset1->addField('allnations_username', 'text', array(
            'name' => 'others[allnations_username]',
            'label' => Mage::helper('allnations')->__('Username'),
            'title' => Mage::helper('allnations')->__('Username'),
            'required' => true,
            'class' => 'customvalidation',
        ));
        $ftppassword = $fieldset1->addField('allnations_password', 'password', array(
            'name' => 'others[allnations_password]',
            'label' => Mage::helper('allnations')->__('Password'),
            'title' => Mage::helper('allnations')->__('Password'),
            'required' => true,
            'class' => 'customvalidation',
        ));
        $dateTimeFormatIso = 'yyyy-M-d';
        $allnations_lastupdateddate = $fieldset1->addField('allnations_lastupdateddate', 'date', array(
            'label'    => Mage::helper('allnations')->__('Last Updated Date'),
            'title'    => Mage::helper('allnations')->__('Last Updated Date'),
            'name'     => 'others[allnations_lastupdateddate]',
            'image'    => $this->getSkinUrl('images/grid-cal.gif'),
            'format'   => $dateTimeFormatIso,
            'required' => true,
            'class' => 'customvalidation',
        ));
        $ftptestbtn = $fieldset1->addField('test_allnations_connection', 'button', array(
            'title' => Mage::helper('allnations')->__('Test Connection'),
            'value' => "Test Connection",
            'class' => 'form-button',
            'onclick' => "return testconnection();"
        ));

        $fieldset2 = $form->addFieldset('pim_settings', array(
            'legend' => Mage::helper('allnations')->__('PIM Settings'),
            'fieldset_container_id' => 'pim_settings'
                )
        );
        $fieldset2->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
        $fieldset2->addField('sync_settings', 'hbar', array(
            'id' => 'Sync Settings'
        ));
        $fieldset2->addField('product_info', 'select', array(
            'name' => 'product_info',
            'label' => Mage::helper('allnations')->__('Sync Product Information'),
            'title' => Mage::helper('allnations')->__('Sync Product Information'),
            'options' => array(1 => Mage::helper('allnations')->__('Yes'), 0 => Mage::helper('allnations')->__('No')),
            'required' => true,
        ));

        $fieldset2->addField('basic_settings', 'hbar', array(
            'id' => 'Basic Settings'
        ));
        $fieldset2->addField('weightage', 'text', array(
            'name' => 'weightage',
            'label' => Mage::helper('allnations')->__('Weightage'),
            'title' => Mage::helper('allnations')->__('Weightage'),
            'class' => 'validate-number validate-greater-than-zero unique-weightage',
            'required' => true,
        ));
        $fieldset2->addField('content_source', 'select', array(
            'name' => 'content_source',
            'label' => Mage::helper('allnations')->__('Product Content Destination'),
            'title' => Mage::helper('allnations')->__('Product Content Destination'),
            'options' => array('original' => Mage::helper('allnations')->__('Replace With original content'), 'copy' => Mage::helper('allnations')->__('Keep Separate Copy')),
            'required' => true,
        ));
        $fieldset2->addField('product_status', 'select', array(
            'name' => 'product_status',
            'label' => Mage::helper('allnations')->__('Default Status Of Listed Product'),
            'title' => Mage::helper('allnations')->__('Default Status Of Listed Product'),
            'options' => array(1 => Mage::helper('allnations')->__('Enabled'), 2 => Mage::helper('allnations')->__('Disabled'), 3 => Mage::helper('allnations')->__('As it in Source')),
            'required' => true
        ));
        $fieldset2->addField('create_new', 'select', array(
            'name' => 'create_new',
            'label' => Mage::helper('allnations')->__('Add New Product Automatically'),
            'title' => Mage::helper('allnations')->__('Add New Product Automatically'),
            'options' => array(1 => Mage::helper('allnations')->__('Yes'), 0 => Mage::helper('allnations')->__('No')),
            'required' => true,
        ));
        $fieldset2->addField('update_exist', 'select', array(
            'name' => 'update_exist',
            'label' => Mage::helper('allnations')->__('Update Existing Product Automatically'),
            'title' => Mage::helper('allnations')->__('Update Existing Product Automatically'),
            'options' => array(1 => Mage::helper('allnations')->__('Yes'), 0 => Mage::helper('allnations')->__('No')),
            'required' => true,
        ));
        $fieldset2->addField('create_option', 'select', array(
            'name' => 'create_option',
            'label' => Mage::helper('allnations')->__('Create New Attribute Options Automatically'),
            'title' => Mage::helper('allnations')->__('Create New Attribute Options Automatically'),
            'options' => array(1 => Mage::helper('allnations')->__('Yes'), 0 => Mage::helper('allnations')->__('No')),
            'required' => true,
        ));
        $fieldset2->addField('pim_cron', 'select', array(
            'name' => 'pim_cron',
            'label' => Mage::helper('allnations')->__('Cron Frequency'),
            'title' => Mage::helper('allnations')->__('Cron Frequency'),
            'options' => Mage::helper('allnations')->getPimCronOptions(),
            'required' => true,
        ));


        $fieldset3 = $form->addFieldset('edi_settings', array(
            'legend' => Mage::helper('allnations')->__('EDI Settings'),
            'fieldset_container_id' => 'edi_settings'
                )
        );
        $fieldset3->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');

        $fieldset3->addField('sync_settings_', 'hbar', array(
            'id' => 'Sync Settings'
        ));
        $fieldset3->addField('product_pricenquantity', 'select', array(
            'name' => 'product_pricenquantity',
            'label' => Mage::helper('allnations')->__('Sync Prices & Quantity'),
            'title' => Mage::helper('allnations')->__('Sync Prices & Quantity'),
            'options' => array(1 => Mage::helper('allnations')->__('Yes'), 0 => Mage::helper('allnations')->__('No')),
            'required' => true,
        ));
        $fieldset3->addField('product_defaultquantity', 'text', array(
            'name' => 'others[product_defaultquantity]',
            'label' => Mage::helper('allnations')->__('Default Quantity'),
            'title' => Mage::helper('allnations')->__('Default Quantity'),
            'required' => true,
        ));

        $fieldset3->addField('basic_settins_', 'hbar', array(
            'id' => 'Basic Settings'
        ));
        $multiplepricerule = $fieldset3->addField('multi_price_rule', 'select', array(
            'name' => 'multi_price_rule',
            'label' => Mage::helper('allnations')->__('Multiple Pricing Rules'),
            'title' => Mage::helper('allnations')->__('Multiple Pricing Rules'),
            'options' => array('max' => Mage::helper('allnations')->__('Take Max Price Only')
                , 'min' => Mage::helper('allnations')->__('Take Min Price Only')
                , 'sum_on_base' => Mage::helper('allnations')->__('Apply Multiple on Base Price')
                , 'sum_on_cumulative' => Mage::helper('allnations')->__('Apply Multiple on Cumulative Price')
            )
        ));
        $fieldset3->addField('slab', 'text', array(
            'name' => 'slab',
            'label' => Mage::helper('allnations')->__('Slab'),
            'title' => Mage::helper('allnations')->__('Slab'),
            'required' => true,
        ));
        $fieldset3->addField('channel_order', 'text', array(
            'name' => 'channel_order',
            'label' => Mage::helper('allnations')->__('Order'),
            'title' => Mage::helper('allnations')->__('Order'),
            'required' => true,
        ));
        $field_nm = $fieldset3->addField('edi_cron', 'select', array(
            'name' => 'edi_cron',
            'label' => Mage::helper('allnations')->__('Cron Frequency'),
            'title' => Mage::helper('allnations')->__('Cron Frequency'),
            'options' => Mage::helper('allnations')->getEdiCronOptions(),
            'required' => true,
        ));
        
        $fieldset4 = $form->addFieldset('channel_order_settings', array(
            'legend' => Mage::helper('allnations')->__('Order Sync Settings'),
            'fieldset_container_id' => 'channel_order_settings'
                )
        );
        $fieldset4->addField('sync_channel_order', 'select', array(
            'name' => 'others[sync_channel_order]',
            'label' => Mage::helper('allnations')->__('Sync Orders'),
            'title' => Mage::helper('allnations')->__('Sync Orders'),
            'options' => array(1 => Mage::helper('allnations')->__('Yes'), 0 => Mage::helper('allnations')->__('No')),
            'class' => 'customvalidation',
        ));
        
        $unique_wightage_url = $this->getUrl('adminhtml/incomingchannels/weightageunique_validation') . "?isAjax=true";
        $form_key = Mage::getSingleton('core/session')->getFormKey();
        if (!$model->getWeightage()) {
            $model->setWeightage(10);
        }
        $form->setValues($data);
        $ftptestbtn->setValue('Test Connection');
        $this->setForm($form);
        $allnations_lastupdateddate->setAfterElementHtml($this->getLayout()->createBlock('core/template')->setTemplate('fedobe/allnations/allnationsjs.phtml')->toHtml());

        Mage::dispatchEvent('fedobe_bridge_adminhtml_incomingchannels_edit_tab_main_prepare_form', array('form' => $form));

        return parent::_prepareForm();
    }

}