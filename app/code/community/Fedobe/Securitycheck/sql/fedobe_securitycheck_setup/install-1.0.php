<?php

$installer = $this;

$installer->startSetup();
$cur_time = time();

$installer->run("
CREATE TABLE IF NOT EXISTS `bridge_securitysalt` (
`id` int(11) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `incomingchannel_id` int(11) DEFAULT NULL,
  `outgoingchannel_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `bridge_securitysalt`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `bridge_securitysalt`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;");
$installer->endSetup();

