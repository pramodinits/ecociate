<?php

class Fedobe_Securitycheck_Adminhtml_SaltController extends Mage_Adminhtml_Controller_Action {

    private $salt = 'Fedobe Solutions Private Limited';

    public function generateAction() {
        $ogc_id = $this->getRequest()->getParam('id');
        $current_salt = $ogc_id ? "{$ogc_id}_" : "";
        if ($this->getRequest()->getParam('channel') == "outgoing") {
            if (!Mage::getSingleton('core/session')->getData("{$current_salt}outgoing_security_salt")) {
                Mage::getSingleton('core/session')->setData("{$current_salt}outgoing_security_salt", $this->generateSalt());
            }
            echo Mage::getSingleton('core/session')->getData("{$current_salt}outgoing_security_salt");
        } else if ($this->getRequest()->getParam('channel') == "incoming") {
            if (!Mage::getSingleton('core/session')->getData("{$current_salt}incoming_security_salt")) {
                Mage::getSingleton('core/session')->setData("{$current_salt}incoming_security_salt", $this->generateSalt());
            }
            echo Mage::getSingleton('core/session')->getData("{$current_salt}incoming_security_salt");
        }
        exit;
    }

    public function clearAction() {
        $ogc_id = $this->getRequest()->getParam('id');
        $channel_type = $this->getRequest()->getParam('channel');
        $current_salt = $ogc_id ? "{$ogc_id}_{$channel_type}_" : "{$channel_type}_";

        Mage::getSingleton('core/session')->unsetData( "{$current_salt}security_salt");
        exit;
    }

    private function generateSalt() {
        return password_hash(time(), PASSWORD_BCRYPT, array('salt' => $this->salt));
    }

}
