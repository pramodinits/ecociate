<?php

class Fedobe_Securitycheck_Block_Adminhtml_Incomingchannel_Main extends Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Edit_Tab_Main {

    protected function _prepareForm() {
        parent::_prepareForm();
        $form = $this->getForm();
        $element = $form->getElement('salt');
        $generate_url = $this->getUrl('adminhtml/salt/generate') . "?isAjax=true";
        $clear_url = $this->getUrl('adminhtml/salt/clear') . "?isAjax=true";
        $form_key = Mage::getSingleton('core/session')->getFormKey();
        $inc_id=$this->getRequest()->getParam('id');
        $security_salt=$inc_id?"{$inc_id}_incoming_security_salt":"incoming_security_salt";
        $salt = Mage::getSingleton('core/session')->getData($security_salt);
        $element->setAfterElementHtml('<script>
    $j(document).ready(function () {
    var salt="' . $salt . '";
        var inc_id = "' . $inc_id . '";
        if(salt!="")
         $j("#incomingchannel_salt").prop("readonly",true);
        $j(".generate_salt").click(function () {
         $j("#loading-mask").show();
            url = "' . $generate_url . '";
            data = {channel:"incoming",form_key: "' . $form_key . '",id:inc_id};
            $j.ajax({
                type: "POST",
                async: false,
                url: url,
                data: data,
                success: function (res) {
                   $j("#incomingchannel_salt").val(res);
                   $j("#incomingchannel_salt").prop("readonly",true);
                                    $j("#loading-mask").hide();

                }
            });
        });
        $j(".clear_salt").click(function () {
        $j("#loading-mask").show();
            url = "' . $clear_url . '";
            data = {channel:"incoming",form_key: "' . $form_key . '",id:inc_id};
            $j.ajax({
                type: "POST",
                async: false,
                url: url,
                data: data,
                success: function (res) {
                   $j("#incomingchannel_salt").val("");
                   $j("#incomingchannel_salt").prop("readonly",false);
                                    $j("#loading-mask").hide();

                }
               
            });
        });
    });
</script>
                <div style="margin-top:7px;"><input id="generate_salt" class="form-button generate_salt" type="button" title="Generate Salt" value="Generate Salt" name="Generate Salt" style="margin-right:10px;" />
                <input id="clear_salt" class="form-button clear_salt" type="button" title="Clear Salt" value="Clear Salt" name="Clear Salt"  /></div>
                ');

        $this->setForm($form);
    }

}
