<?php

class Fedobe_Securitycheck_Block_Adminhtml_Outgoingchannel_Main extends Fedobe_Bridge_Block_Adminhtml_Outgoingchannels_Edit_Tab_Main {

    protected function _prepareForm() {
        parent::_prepareForm();
        $form = $this->getForm();
        $is_retailer = Mage::getmodel('bridge/outgoingchannels')->isRetailer(Mage::getSingleton('admin/Session')->getUser()->getUserId());
        $channel_condition = @Mage::registry('channel_condition') ? Mage::registry('channel_condition') : Mage::getmodel('bridge/outgoingchannels');
        $element = $form->getElement('salt');
        $generate_url = $this->getUrl('adminhtml/salt/generate') . "?isAjax=true";
        $clear_url = $this->getUrl('adminhtml/salt/clear') . "?isAjax=true";
        $form_key = Mage::getSingleton('core/session')->getFormKey();
        $ogc_id = $this->getRequest()->getParam('id');
        $security_salt = $ogc_id ? "{$ogc_id}_outgoing_security_salt" : "outgoing_security_salt";
        $salt = Mage::getSingleton('core/session')->getData($security_salt);
        $element->setAfterElementHtml('<script>
    var salt = "' . $salt . '";
    var ogc_id = "' . $ogc_id . '";
    var is_retailer = "' . $is_retailer . '";
    $j(document).ready(function () {
        $j(".generate_salt").click(function () {
            $j("#loading-mask").show();
            url = "' . $generate_url . '";
            data = {channel: "outgoing", form_key: "' . $form_key . '",id:ogc_id};
            $j.ajax({
                type: "POST",
                async: false,
                url: url,
                data: data,
                success: function (res) {
                    $j("#outgoingchannel_salt").val(res);
                    $j("#outgoingchannel_salt").prop("readonly", true);
                    $j("#loading-mask").hide();

                }
            });
        });
        $j(".clear_salt").click(function () {
            $j("#loading-mask").show();
            url = "' . $clear_url . '";
            data = {channel: "outgoing", form_key: "' . $form_key . '",id:ogc_id};
            $j.ajax({
                type: "POST",
                async: false,
                url: url,
                data: data,
                success: function (res) {
                    $j("#outgoingchannel_salt").val("");
                    $j("#outgoingchannel_salt").prop("readonly", false);
                    $j("#loading-mask").hide();

                }

            });
        });
        if (parseInt(is_retailer)) {
        if(ogc_id == "")
            $j(".generate_salt").click();
            $j(".saltbtn").remove();
        }
        if (salt != "")
            $j("#outgoingchannel_salt").prop("readonly", true);

    });
</script>
                <div style="margin-top:7px;" class="saltbtn"><input id="generate_salt" class="form-button generate_salt" type="button" title="Generate Salt" value="Generate Salt" name="Generate Salt" style="margin-right:10px;" />
                <input id="clear_salt" class="form-button clear_salt" type="button" title="Clear Salt" value="Clear Salt" name="Clear Salt"  /></div>
                ');

        $this->setForm($form);
    }

}
