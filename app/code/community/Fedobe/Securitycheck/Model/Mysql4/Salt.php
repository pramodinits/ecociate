<?php
class Fedobe_Securitycheck_Model_Mysql4_Salt extends Mage_Core_Model_Mysql4_Abstract{
    public function _construct(){
        $this->_init('securitycheck/salt', 'id');
    }
    public function loadByOutgoingChannel($ogc_id)
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('securitycheck/salt'))
            ->where('outgoingchannel_id=:outgoingchannel_id');
        return $adapter->fetchRow($select, array('outgoingchannel_id'=>$ogc_id));
    }
    public function loadByIncomingChannel($inc_id)
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('securitycheck/salt'))
            ->where('incomingchannel_id=:incomingchannel_id');
        return $adapter->fetchRow($select, array('incomingchannel_id'=>$inc_id));
    }
}