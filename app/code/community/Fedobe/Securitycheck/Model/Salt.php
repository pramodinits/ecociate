<?php

class Fedobe_Securitycheck_Model_Salt extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('securitycheck/salt');
    }
    public function loadByOutgoingChannel($ogc_id) {
        $this->setData($this->getResource()->loadByOutgoingChannel($ogc_id));
        return $this;
    }

    public function loadByIncomingChannel($inc_id) {
        $this->setData($this->getResource()->loadByIncomingChannel($inc_id));
        return $this;
    }

}
