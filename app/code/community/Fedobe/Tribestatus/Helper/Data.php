<?php

class Fedobe_Tribestatus_Helper_Data extends Mage_Core_Helper_Abstract {

    const ATTRIBUTESET_NAME = 'Tribe Status';

    public function getAttributeSetId() {
        $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                ->load(SELF::ATTRIBUTESET_NAME, 'attribute_set_name')
                ->getAttributeSetId();
        return $attributeSetId;
    }

    public function getSingleStatus($id, $url_key) {
        $collection = Mage::getModel('arccore/arc')->getCollection()->addAttributeToSelect('*');
        $collection->addAttributeTofilter('attribute_set_id', Mage::helper('tribestatus')->getAttributeSetId())
                ->addAttributeTofilter('trcr_status', 2)
                ->addAttributeTofilter('trcr_username_list', $id)
                ->addAttributeTofilter('trcr_urlkey', $url_key);
        return $collection;
    }

    public function getAllStatus($id) {
        $limit = Mage::getStoreConfig('notes/general/status_page_limit') ? Mage::getStoreConfig('notes/general/status_page_limit') : 10;
        $limit = intval($limit);
        $page = Mage::app()->getRequest()->getParam('page') ? Mage::app()->getRequest()->getParam('page') * $limit : 0;
        $collection = Mage::getModel('arccore/arc')->getCollection()->addAttributeToSelect('*');
        $collection->addAttributeTofilter('attribute_set_id', Mage::helper('tribestatus')->getAttributeSetId())
                ->addAttributeTofilter('trcr_status', 2)
                ->addAttributeTofilter('trcr_username_list', $id);
        $collection->getSelect()->limit($limit, $page)->order('created_at DESC');
        return $collection;
    }

    public function getInfo($label, $info) {
        $collection = $this->getAllStatus($info['entity_id']);
        return array('label' => $label, 'count' => $collection->getSize(), 'url' => $this->getStatusUrl($info['trcr_username']));
    }

    public function getProfileType() {
        return 'Notes';
    }

    public function getOptionId() {
        $options = array();
        $option_id = 0;
        $data_option = Mage::helper('arccore')->getattributeoptions('trcr_post_type');
        foreach ($data_option as $k => $v) {
            $options[strtolower($v['label'])] = $v['value'];
        }
        if (isset($options[strtolower($this->getProfileType())])) {
            $option_id = $options[strtolower($this->getProfileType())];
        }
        return $option_id;
    }

    public function getStatusUrl($parenturl_key) {
        $identifier = Mage::getStoreConfig('tribe_status/edit/identifier') ? Mage::getStoreConfig('tribe_status/edit/identifier') : 'notes';
        return trim(Mage::getUrl("$parenturl_key/$identifier"), "/");
    }

    public function getSingleStatusUrl($parenturl_key, $urlkey) {
        $identifier = Mage::getStoreConfig('tribe_status/edit/identifier') ? Mage::getStoreConfig('tribe_status/edit/identifier') : 'notes';
        $suffix = Mage::getStoreConfig('notes/general/single_url_suffix') ? Mage::getStoreConfig('notes/general/single_url_suffix') : '';
        return trim(Mage::getUrl("$parenturl_key/$identifier/$urlkey" . $suffix), "/");
    }

    public function getFrontendProductUrl($key) {
        $urlkey = $key;
        $preffix = Mage::getStoreConfig('tribe_status/edit/url_prefix');
        $suffix = Mage::getStoreConfig('tribe_status/edit/url_suffix');
        if ($preffix) {
            $urlkey = "$preffix/$urlkey";
        }
        if ($suffix) {
            $urlkey .=$suffix;
        }
        return trim(Mage::getUrl("$urlkey"), "/");
    }

    public function editInfoStatusurl() {
        return Mage::getUrl("tribestatus/status/editinfo");
    }

    public function statusdeleteUrl() {
        return Mage::getUrl("tribestatus/status/delete");
    }

    public function loadmorestatusUrl() {
        return Mage::getUrl("tribestatus/status/loadmorestatus");
    }

    private function getencprefix() {
        $prefix = "Status for profiles start";
        return base64_encode($prefix);
    }

    private function getencsuffix() {
        $sufix = "Status for profiles end";
        return base64_encode($sufix);
    }

    public function decrypt($data) {
        $encpre = $this->getencprefix();
        $encsuffix = $this->getencsuffix();
        $data = str_replace($encsuffix, '', str_replace($encpre, '', $data));
        return Mage::helper('core')->jsonDecode(base64_decode($data));
    }

    public function encrypt($data) {
        $enc_data = base64_encode(Mage::helper('core')->jsonEncode($data));
        $encpre = $this->getencprefix();
        $encsuffix = $this->getencsuffix();
        return $encpre . $enc_data . $encsuffix;
    }

    public function isActionallowed($info) {
        $allow = false;
        if ($info['user_log_id'] || (Mage::app()->isInstalled() && Mage::getSingleton('customer/session')->isLoggedIn())) {
            if ($info['trcr_account_type'] == 2) {
                $id = $info['user_log_id']  ? $info['user_log_id'] : Mage::getSingleton('customer/session')->getCustomer()->getId();
                if ($info['trcr_customer_list'] == $id) {
                    $allow = true;
                }
            } else {
                $email = $info['logged_in_email']  ? $info['logged_in_email'] : Mage::getSingleton('customer/session')->getCustomer()->getEmail();
                if (trim($info['trcr_email']) == $email) {
                    $allow = true;
                }
            }
        }
        return $allow;
    }

}