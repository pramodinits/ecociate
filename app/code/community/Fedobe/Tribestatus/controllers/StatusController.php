<?php

require_once Mage::getBaseDir('code') . DS . 'community/Fedobe/Arccore/controllers/Adminhtml/ProfilesController.php';

class Fedobe_Tribestatus_StatusController extends Fedobe_Arccore_Adminhtml_ProfilesController {

    protected $_entityTypeId;

    public function preDispatch() {
        //parent::preDispatch();
        $this->_entityTypeId = Mage::helper('arccore')->getEntityTypeId('arccore');
        $this->_attributesetId = Mage::helper('tribestatus')->getAttributeSetId();
        $bypassaction = array('getReport','save', 'editinfo', 'delete', 'loadmorestatus');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }

    private function getImageArrtibutecodes() {
        $attribute_set_id = Mage::helper('tribestatus')->getAttributeSetId();
        $arrtcodes = Mage::helper('arccore')->getImageAttributeCodes($attribute_set_id);
        return $arrtcodes;
    }

    public function deleteAction() {
        $id = $this->getRequest()->getPost('id');
        if ($id) {
            $err = parent::deleteAction($id);
            echo $err;
        } else {
            echo 'error';
        }
    }

    public function loadmorestatusAction() {
        $info = Mage::helper('tribestatus')->decrypt($this->getRequest()->getPost('info'));
        if ($info) {
            $entity_id = $info['id'];
            $statuscollection = Mage::helper('tribestatus')->getAllStatus($entity_id);
            $status_cnt = count($statuscollection);
            $data = array('ids' => array(), 'content' => '');
            if ($status_cnt) {
                $info['profile_info']['user_log_id'] = $info['is_logged_in'];
                $info['profile_info']['logged_in_email'] = $info['logged_in_email'];
                $stastus_info = $this->getStatusContent($statuscollection, $info['profile_info']);
                $data = array_merge($data, $stastus_info);
            }
            $result = array('count' => $status_cnt, 'ids' => $data['ids'], 'content' => $data['content']);
            echo Mage::helper('core')->jsonEncode($result);
        }
    }

    private function getStatusContent($statuscollection, $profile_info) {
        $statusids = array();
        foreach ($statuscollection as $k => $status) {
            $statusids[] = 'note_item_' . $status->getId();
        }
        $content = Mage::app()->getLayout()->createBlock('tribestatus/status')->setStatusCollectionInfo($statuscollection, $profile_info)->setTemplate('fedobe/tribe/status/ajaxstatus.phtml')->toHtml();
        $info = array('ids' => $statusids, 'content' => $content);
        return $info;
    }

    public function editinfoAction() {
        $status_id = $this->getRequest()->getPost('id');
        if ($status_id) {
            $width = Mage::getStoreConfig('notes/general/profile_icon_width');
            $height = Mage::getStoreConfig('notes/general/profile_icon_height');
            $arc = Mage::getModel('arccore/arc')
                    ->setStoreId($this->getRequest()->getParam('store', 0))
                    ->setAttributeSetId($this->_attributesetId)
                    ->setEntityTypeId($this->_entityTypeId);
            $data = $arc->load($status_id);
            $data_arr = $data->getData();
            $arrtcodes = $this->getImageArrtibutecodes();
            if (!empty($arrtcodes)) {
                foreach ($arrtcodes as $k => $val) {
                    if (isset($data_arr[$val])) {
                        $image = Mage::helper('arccore')->resizeImg($data_arr[$val], $width, $height);
                        $data_arr[$val] = "<img src='$image' width='$width' height='$height'/>";
                    }
                }
            }
            echo Mage::helper('core')->jsonEncode($data_arr);
        }
    }
    
    
    
     //added by bhagyashree
     public function getReportAction(){
     $data = $this->getRequest()->getParams();
     $notename = $data['note_title'];
    
    //code to send mail         
    $uname = $data['cust_uname'];
    $note_link = $data['note_link'];
    $profile_url = $data['profile_url'];
    $body ='Hi admin, <br/><br/>One customer named by <a href="'.$profile_url.'"><b>'.$uname.'</b></a> reported a note having title: <a href="'.$note_link.'"><b>'.$notename.'</b></a> as an unappropriated note from your site.';
    $name = 'Ecociate';
    $mail = Mage::getModel('core/email');
    $mail->setToName($name);
    $mail->setToEmail('admin@ecociate.com');
    //$mail->setToEmail('bhagyashree@fedobe.com');
    $mail->setBody($body);

    $mail->setSubject('Unappropriated Note');
    $mail->setFromEmail('dev.ecociate.com');
    $mail->setFromName($name);
    $mail->setType('html');// You can use 'html' or 'text'

    try {
    $mail->send();
    }
    catch (Exception $e) {
    }
     //ends here 
     }
     
     

}