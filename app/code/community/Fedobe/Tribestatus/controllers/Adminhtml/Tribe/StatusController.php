<?php

/**
 * Manage Status Attribute controller file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribestatus
 * @author      Fedobe Magento Team
 */
require_once Mage::getBaseDir('code').DS.'community/Fedobe/Arccore/controllers/Adminhtml/ProfilesController.php';
class Fedobe_Tribestatus_Adminhtml_Tribe_StatusController extends Fedobe_Arccore_Adminhtml_ProfilesController {

    protected $_entityTypeId;

    public function preDispatch() {
        parent::preDispatch();
        $this->_entityTypeId = Mage::helper('arccore')->getEntityTypeId('arccore');
    }

    /**
     * Init actions
     *
     */
    protected function _initAction() {
        // load layout, set active menu and breadcrumbs

        $this->_title($this->__('Tribe'))
                ->_title($this->__('Manage Status'));
 
        if ($this->getRequest()->getParam('popup')) {
            $this->loadLayout('popup');
        } else {
            $this->loadLayout()
                    ->_setActiveMenu('fedobe/tribestatus_manage_status');
        }
        return $this;
    }

    /**
     * Index action method
     */
    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }

    /**
     * Code to display the form
     * 
     * The main form container gets added to the content and the tabs block gets added to left.
     */
    public function newAction() {
        $attrsetid = $this->getRequest()->getParam('set');
        if ($attrsetid) {
            $this->_forward('edit');
        } else {
            $this->loadLayout();
            $this->renderLayout();
        }
    }

    /**
     * Edit Status Attribute
     */
    public function editAction() {
         parent::editAction();
         $id = $this->getRequest()->getParam('id');
         $this->_initAction()
               ->_addBreadcrumb(
                        $id ? Mage::helper('tribestatus')->__('Edit') : Mage::helper('tribestatus')->__('New'), $id ? Mage::helper('tribestatus')->__('Edit') : Mage::helper('tribestatus')->__('New'));
        $arc = parent::loadProfile();
        if($id)
         $this->_title($this->__($arc->getTrrvName()));
        else
         $this->_title($this->__('New Status'));
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->renderLayout();
    }

    public function validateAction() {
        parent::validateAction();
    }

    /**
     * Delete tribestatus attribute
     *
     * @return null
     */
    public function deleteAction() {
        if ($id = $this->getRequest()->getParam('id')) {
            $res = parent::deleteAction();
            
             if($res['success']){
                 Mage::getSingleton('adminhtml/session')->addSuccess($res['message']);
                 $this->_redirect('*/*/');
                 return;
             }else{
                  Mage::getSingleton('adminhtml/session')->addError($res['message']);
                  $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('attribute_id')));
                  return;
             }
        }
        Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('tribestatus')->__('Unable to find a status to delete.'));
        $this->_redirect('*/*/');
    }

}
