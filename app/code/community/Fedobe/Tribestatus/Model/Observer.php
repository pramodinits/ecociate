<?php

class Fedobe_Tribestatus_Model_Observer {
    public function addButtonStatus($observer){
    $container = $observer->getBlock();
    $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                        ->load('Tribe Status', 'attribute_set_name')
                        ->getAttributeSetId();
   // echo $attributeSetId;exit;
    if(null !== $container && $container->getType() == 'tribestatus/adminhtml_status') {
        $data_2 = array(
            'label'     => 'Add Status',
            'class'=>'add',
            'onclick'   => 'setLocation(\' '  . Mage::helper('adminhtml')->getUrl('adminhtml/tribe_status/new/set/'.$attributeSetId, array('_secure' => true)) . '\')',
        );
        $container->addButton('addstatus', $data_2);
    }
    return $this;
}

}
