<?php

class Fedobe_Tribestatus_Block_Status extends Mage_Core_Block_Template {

    private $_statuscollection;
    private $_profileinfo;

    public function __construct() {
        parent::__construct();
        if (!$this->_template)
            $this->setTemplate('fedobe/tribe/status/status.phtml');
        $this->_statuscollection = null;
        $this->_profileinfo = null;
    }

    public function getCurrentProfileInfo() {
        return Mage::registry('tribeprofileinfo');
    }
    
    public function getAllStatus(){
        $info = $this->getCurrentProfileInfo();
        $statuscollection = Mage::helper('tribestatus')->getAllStatus($info['entity_id']);
        return $statuscollection;
    }
    
    public function setStatusCollectionInfo($statuscollection,$info){
        $this->_statuscollection = $statuscollection;
        $this->_profileinfo = $info;
        return $this;
    }
 
    public function getStatusProfileInfo(){
        return $this->_profileinfo;
    }

    public function getStatusCollection(){
        return $this->_statuscollection;
    }
}
