<?php
/**
 * Grid container file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribestatus
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Tribestatus_Block_Adminhtml_Status extends Mage_Adminhtml_Block_Widget_Grid_Container
{       
  public function __construct()
  {
    /*both these variables tell magento the location of our Grid.php(grid block) file.
     * $this->_blockGroup.'/' . $this->_controller . '_grid'
     * i.e clarion_Customerattribute/adminhtml_customerattribute_grid
     * $_blockGroup - is your module's name.
     * $_controller - is the path to your grid block. 
     */
    
    $this->_controller = 'adminhtml_status';
    $this->_blockGroup = 'tribestatus';
    
    $this->_headerText = Mage::helper('tribestatus')->__('Manage Status');
    $this->_addButtonLabel = Mage::helper('tribestatus')->__('Add New Status');
    parent::__construct();
    $this->_removeButton('add');
  }
}