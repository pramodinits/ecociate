<?php

/**
 * Customer attribute edit block
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribestatus
 * @author      Fedobe Magento Team
 */
class Fedobe_Tribestatus_Block_Adminhtml_Status_Edit extends Fedobe_Arccore_Block_Adminhtml_Arc_Edit {

    public function __construct() {
        parent::__construct();
        $this->_controller = 'tribe_status';
        $this->_blockGroup = 'tribestatus';
    }
}
