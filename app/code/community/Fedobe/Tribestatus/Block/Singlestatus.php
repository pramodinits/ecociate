<?php

class Fedobe_Tribestatus_Block_Singlestatus extends Mage_Core_Block_Template {

    public function __construct() {
        parent::__construct();
        if (!$this->_template)
            $this->setTemplate('fedobe/tribe/status/singlestatus.phtml');
    }

    public function getCurrentProfileInfo() {
        return Mage::registry('tribeprofileinfo');
    }
    
    public function getStatusInfo(){
        $info = $this->getCurrentProfileInfo();
        $url_key = Mage::app()->getRequest()->getParam('singlepostkey');
        $statuscollection = Mage::helper('tribestatus')->getSingleStatus($info['entity_id'],$url_key);
        return $statuscollection;
    }

}