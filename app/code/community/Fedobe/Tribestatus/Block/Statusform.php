<?php

class Fedobe_Tribestatus_Block_Statusform extends Mage_Core_Block_Template {

    public function __construct() {
        parent::__construct();
        if (!$this->_template)
            $this->setTemplate('fedobe/tribe/status/statusform.phtml');
    }

    public function getCurrentProfileInfo() {
        return Mage::registry('tribeprofileinfo');
    }
    
    public function getHideenElements(){
        $info = $this->getCurrentProfileInfo();
        return array('trcr_post_type','trcr_profile_list','trcr_username_list','trcr_status','trcr_urlkey');
    }
    
    public function getHideenElementswithValues(){
        $info = $this->getCurrentProfileInfo();
        return array('trcr_urlkey'=>'','trcr_status'=>2,'trcr_post_type'=>Mage::helper('tribestatus')->getOptionId(),'trcr_profile_list'=>$info['trcr_profile_type'],'trcr_username_list'=>$info['entity_id']);
    }
    
    public function getFormdata(){
        $attribute_set_id = Mage::helper('tribestatus')->getAttributeSetId();
        $exclude = $this->getHideenElements();
        $visible_attributes = Mage::helper('arccore')->getFormAttributes($attribute_set_id,$exclude);
        $attributes['attributes'] = $visible_attributes;
        $attributes['hidden_attributes'] = $this->getHideenElementswithValues();
        return $attributes;
    }
    
    public function getAllStatus(){
        $info = $this->getCurrentProfileInfo();
        $statuscollection = Mage::helper('tribestatus')->getAllStatus($info['entity_id']);
        return $statuscollection;
    }

    public function getSubmitUrl(){
        return Mage::getUrl("tribestatus/status/save");
    }
}
