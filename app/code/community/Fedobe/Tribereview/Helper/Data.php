<?php

class Fedobe_Tribereview_Helper_Data extends Mage_Core_Helper_Abstract {
    const ATTRIBUTESET_NAME = 'Tribe Review';

    public function getAttributeSetId() {
        $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                ->load(SELF::ATTRIBUTESET_NAME, 'attribute_set_name')
                ->getAttributeSetId();
              return $attributeSetId;

    }
   public function getProfileType() {
        return 'Review';
    }
  public function getFrontendProductUrl($key) {
        $urlkey = $key;
        $preffix = Mage::getStoreConfig('tribe_review/edit/url_prefix');
        $suffix = Mage::getStoreConfig('tribe_review/edit/url_suffix');
        if ($preffix) {
            $urlkey = "$preffix/$urlkey";
        }
        if ($suffix) {
            $urlkey .=$suffix;
        }
        return trim(Mage::getUrl("$urlkey"), "/");
    }
    public function getProfileInfourlkeyaction() {
        $controllername = Mage::app()->getRequest()->getControllerName();
        return Mage::helper("adminhtml")->getUrl("*/$controllername/getProfiledata");
    }
  public function getInfo($label,$info){
      if($this->is_customer() && $this->isownprofile($info)){
              $collection = $this->getAllReview($info['trcr_customer_list'],'trcr_customer_list');
              $product_review = $this->allproductreviews();
              $seller_review = $this->allsellerreviews();
              $count = $collection->getSize()+count($product_review) + count($seller_review);
      }else{
               $collection = $this->getAllReview($info['entity_id'],'trcr_username_list');
               $count = $collection->getSize();
      }
       return array('label' => $label, 'count' => $count,'url' => $this->getReviewUrl($info['trcr_username']));
   } 
  public function getReviewUrl($parenturl_key){
        $identifier = Mage::getStoreConfig('tribe_review/edit/identifier') ?  Mage::getStoreConfig('tribe_review/edit/identifier') : 'reviews';
        return trim(Mage::getUrl("$parenturl_key/$identifier"), "/");
    }
    public function getAllReview($id,$field){
        $page = Mage::app()->getRequest()->getParam('page') ? Mage::app()->getRequest()->getParam('page') * $limit : 0;
         $collection = Mage::getModel('arccore/arc')->getCollection()->addAttributeToSelect('*');
          $collection->addAttributeTofilter('attribute_set_id',Mage::helper('tribereview')->getAttributeSetId())
       ->addAttributeTofilter($field,$id)
       ->addAttributeTofilter('trcr_status',2)->setOrder('created_at','DESC');
       return $collection;
   }  
   public function getLoggedincustomerdata(){
       return Mage::getSingleton('customer/session')->getCustomer()->getData();
   }
   public function getcustomerdata($id){
       $customerData = Mage::getModel('customer/customer')->load($id)->getData();
       return $customerData;
   }
   public function isownprofile($profile_data){
       $flag=0;
        $customer = $this->getLoggedincustomerdata();//loggedin customer data
        $email_option_id = $customer['entity_id'];
        if($profile_data['trcr_customer_list']==$email_option_id){
            $flag=1;
        }
        return $flag;
   }
    public function getprofiledata($id){
       $Data = Mage::getModel('arccore/arc')->load($id)->getData();
       return $Data;
   }
   
   public function allproductreviews($review_id=NULL){
     $summaryData=array();
    $resource = Mage::getSingleton('core/resource');
    $readConnection = $resource->getConnection('core_read');
     $loggin = $this->getLoggedincustomerdata(); //echo "<pre>";print_r($loggin);exit;
     $attribute_id = Mage::helper('arccore')->getattributeid('catalog_product','name');
     $urlkey_id = Mage::helper('arccore')->getattributeid('catalog_product','url_key');
     $condition='';
     if(!is_null($review_id)){
         $condition = ' AND main_table.review_id='.$review_id;
     }
     $thumbnailattribute_id = Mage::helper('arccore')->getattributeid('catalog_product','thumbnail');
     if(!empty($loggin)){
        $query = "SELECT `main_table`.*,catvar.entity_id as entity_id,catvarurlkey.value as url_key,catvarimg.value as productimage,catvar.value as productname, `detail`.`detail_id`, `detail`.`title`, `detail`.`detail`, `detail`.`nickname`, `detail`.`customer_id`, `detail`.`custom_rating`, `review_entity`.`entity_code` 
          FROM `review` AS `main_table`
          INNER JOIN `review_detail` AS `detail` ON main_table.review_id = detail.review_id
          INNER JOIN `review_store` AS `store` ON main_table.review_id=store.review_id 
          INNER JOIN `review_entity` ON main_table.entity_id=review_entity.entity_id
          INNER JOIN `catalog_product_entity_varchar` AS catvar ON catvar.entity_id=main_table.entity_pk_value
          INNER JOIN `catalog_product_entity_varchar` AS catvarimg ON catvarimg.entity_id=main_table.entity_pk_value
          INNER JOIN `catalog_product_entity_varchar` AS catvarurlkey ON catvarurlkey.entity_id=main_table.entity_pk_value
          WHERE (store.store_id IN('1')) 
          AND (review_entity.entity_code='product') 
          AND (main_table.status_id=1) 
          AND (catvar.attribute_id=$attribute_id) 
           AND (catvarurlkey.attribute_id=$urlkey_id)
          AND (catvarimg.attribute_id=".$thumbnailattribute_id.")";
         if(!is_null($review_id)){
            $query.=$condition;
        }else{
            $query.=" AND `detail`.customer_id='".$loggin['entity_id']."'";
        }
         $query.=" ORDER BY main_table.created_at DESC";
     $summaryData=$readConnection->fetchAll($query);
     }
     return  $summaryData;
   } 
   public function getprofilebyemail($option_id){
       $collection = Mage::getModel('arccore/arc')->getCollection()
                      ->addAttributeToSelect('*')
                     ->addAttributeTofilter('trcr_customer_list',$option_id)
                     ->addAttributeTofilter('attribute_set_id',20)->getFirstItem();
         return $collection;
   }
    public function allsellerreviews($review_id=NULL){
     $summaryData=array();
    $resource = Mage::getSingleton('core/resource');
    $readConnection = $resource->getConnection('core_read');
     $loggin = $this->getLoggedincustomerdata();
     $attribute_id = Mage::helper('arccore')->getattributeid('fedobe_tribe_seller','seller_store_name');
     $seller_attribute_id = Mage::helper('arccore')->getattributeid('fedobe_tribe_seller','seller_profile_pic');
     $seller_user_name = Mage::helper('arccore')->getattributeid('fedobe_tribe_seller','seller_user_name');
    $condition='';
     if(!is_null($review_id)){
         $condition = ' AND main_table.review_id='.$review_id;
     }
     if(!empty($loggin)){
   $query = "SELECT `main_table`.*,tribesellerimg.value as sellerimg,tribesellername.value as trcr_username,tribeseller.entity_id as entity_id,tribeseller.value as sellername, `detail`.`detail_id`, `detail`.`title`, `detail`.`detail`, `detail`.`nickname`, `detail`.`customer_id`, `detail`.`custom_rating`, `review_entity`.`entity_code` "
           . "FROM `review` AS `main_table` "
           . "INNER JOIN `review_detail` AS `detail` ON main_table.review_id = detail.review_id "
           . "INNER JOIN `review_store` AS `store` ON main_table.review_id=store.review_id "
           . "INNER JOIN `review_entity` ON main_table.entity_id=review_entity.entity_id "
           . "INNER JOIN `tribe_seller_entity_varchar` as tribeseller ON main_table.entity_pk_value=tribeseller.entity_id "
           . "INNER JOIN `tribe_seller_entity_varchar` as tribesellerimg ON main_table.entity_pk_value=tribesellerimg.entity_id "
           . "INNER JOIN `tribe_seller_entity_varchar` as tribesellername ON main_table.entity_pk_value=tribesellername.entity_id "
           . "WHERE (store.store_id IN('1')) "
           . "AND (review_entity.entity_code='seller') "
           . "AND (main_table.status_id=1) "
           . "AND (tribeseller.attribute_id=$attribute_id) "
           . "AND (tribesellername.attribute_id=$seller_user_name) "
           . "AND (tribesellerimg.attribute_id=$seller_attribute_id) ";
    if(!is_null($review_id)){
            $query.=$condition;
        }else{
            $query.=" AND `detail`.customer_id='".$loggin['entity_id']."'";
        }
         $query.=" ORDER BY main_table.created_at DESC";
   $summaryData=$readConnection->fetchAll($query);
     }
     return  $summaryData;
    }
     public function getprofile_by_customerlist($option_id){
       $collection = Mage::getModel('arccore/arc')->getCollection()
                      ->addAttributeToSelect('*')
                     ->addAttributeTofilter('trcr_customer_list',$option_id)
                      ->addAttributeTofilter('trcr_profile_type', array('notnull' => true))
                     //->addAttributeTofilter('attribute_set_id',20)
                     ->getFirstItem();
         return $collection;
   }
   public function getprofile_by_entityid($id){
       $Data = Mage::getModel('arccore/arc')->load($id)->getData();
       return $Data;
   }
   public function is_customer(){
       $is_cust = 0;
       $getLoggedincustomerdata = $this->getLoggedincustomerdata();
       $email = $getLoggedincustomerdata['email'];
       $data = $this->getprofile_by_customerlist($getLoggedincustomerdata['entity_id'])->getData();
        $data_option = Mage::helper('arccore')->getattributeoptions('trcr_profile_type');
        $data_opt_value = array_combine(array_column($data_option,'value'),array_column($data_option,'label'));
      // print_r($data_opt_value);print_r($data);exit;
       //echo $data['trcr_profile_type'];exit;
       if(strtolower($data_opt_value[$data['trcr_profile_type']])=='customer'){
           $is_cust = 1;
       }
       return $is_cust;
       
   }
   public function reviewadded($profile_data){
       $getLoggedincustomerdata = $this->getLoggedincustomerdata();
       $data_option = Mage::helper('arccore')->getattributeoptions('trcr_post_type');
        $data_opt_value = array_combine(array_column($data_option,'label'),array_column($data_option,'value')); //print_r($data_opt_value);echo "<br>".$profile_data['entity_id'];
       $collection = Mage::getModel('arccore/arc')->getCollection()
                      ->addAttributeToSelect('*')
                     ->addAttributeTofilter('trcr_customer_list',$getLoggedincustomerdata['entity_id'])
                     ->addAttributeTofilter('trcr_username_list',$profile_data['entity_id'])
                      ->addAttributeTofilter('trcr_post_type', $data_opt_value['Review'])
                     ->addAttributeTofilter('attribute_set_id',$this->getAttributeSetId())->getSize();
         return $collection;
   }
    public function resizeImg($fileName, $width, $height = '',$aspect_ratio = FALSE ) {
        $dimension_slug = ($width && $height) ? $width . "_" . $height : $width;
        $folderURL =Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog/product/';
        $imageURL = $folderURL . $fileName;
        $basePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog/product/' . $fileName;
        $newPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . "arccore/arcs/images/resized/$dimension_slug" . DS . $fileName;
        //if width empty then return original size image's URL
        if ($width != '') {
            //if image has already resized then just return URL
            if (file_exists($basePath) && is_file($basePath) && !file_exists($newPath)) {
                $imageObj = new Varien_Image($basePath);
                $imageObj->constrainOnly(TRUE);
                $imageObj->keepAspectRatio($aspect_ratio);
                $imageObj->keepFrame(FALSE);
                $imageObj->resize($width, $height);
                $imageObj->save($newPath);
            }
            $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "arccore/arcs/images/resized/$dimension_slug" . $fileName;
        } else {
            $resizedURL = $imageURL;
        }
        return $resizedURL;
    }
    public function encrypt($data) {
        $enc_data = base64_encode(Mage::helper('core')->jsonEncode($data));
        $encpre = $this->getencprefix();
        $encsuffix = $this->getencsuffix();
        return $encpre . $enc_data . $encsuffix;
    }
    public function decrypt($data) {
        $encpre = $this->getencprefix();
        $encsuffix = $this->getencsuffix();
        $data = str_replace($encsuffix, '', str_replace($encpre, '', $data));
        return Mage::helper('core')->jsonDecode(base64_decode($data));
    }
    private function getencprefix() {
        $prefix = "Status for profiles start";
        return base64_encode($prefix);
    }

    private function getencsuffix() {
        $sufix = "Status for profiles end";
        return base64_encode($sufix);
    }
    public function loadmorereviewUrl() {
        return Mage::getUrl("tribereview/tribereview/loadmorereview");
    }
    public function isActionallowed($info) {
        $allow = false;
        if (Mage::app()->isInstalled() && Mage::getSingleton('customer/session')->isLoggedIn()) {
            if ($info['trcr_account_type'] == 2) {
                $id = Mage::getSingleton('customer/session')->getCustomer()->getId();
                if($info['trcr_customer_list'] == $id){
                    $allow = true;
                }
            } else {
                $email = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
                if(trim($info['trcr_email']) == $email){
                    $allow = true;
                }
            }
        }
        return $allow;
    }
     public function editInfoReviewurl() {
        return Mage::getUrl("tribereview/tribereview/editinfo");
    }

    public function reviewdeleteUrl() {
        return Mage::getUrl("tribereview/tribereview/delete");
    }
}