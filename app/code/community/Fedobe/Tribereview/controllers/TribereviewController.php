<?php
require_once Mage::getBaseDir('code').DS.'community/Fedobe/Arccore/controllers/Adminhtml/ProfilesController.php';
class Fedobe_Tribereview_TribereviewController extends Fedobe_Arccore_Adminhtml_ProfilesController {
    protected $_entityTypeId;

    public function preDispatch() {
        //parent::preDispatch();
        $this->_entityTypeId = Mage::helper('arccore')->getEntityTypeId('arccore');
        $this->_attributesetId = Mage::helper('tribereview')->getAttributeSetId();
        $bypassaction = array('save','index','loadmorereview', 'editinfo', 'sellersave','productsave');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }
  public function loadmorereviewAction() {
        $info = Mage::helper('tribereview')->decrypt($this->getRequest()->getPost('info'));
        if ($info) {
            $entity_id = $info['id'];
            $reviewcollection = Mage::app()->getLayout()->createBlock('tribereview/review')->getAllreviews();
            $review_cnt = count($reviewcollection);
            $data = array('ids' => array(), 'content' => '');
            if ($review_cnt) {
                $info['profile_info']['user_log_id'] = $info['is_logged_in'];
                $info['profile_info']['logged_in_email'] = $info['logged_in_email'];
                $review_info = $this->getReviewContent($reviewcollection, $info['profile_info']);
                $data = array_merge($data, $review_info);
            }
            $result = array('count' => $review_cnt, 'ids' => $data['ids'], 'content' => $data['content']);
            echo Mage::helper('core')->jsonEncode($result);
        }
    }
    
    private function getReviewContent($reviewcollection, $profile_info) {
        $reviewids = array();
        foreach ($reviewcollection as $k => $review) {
            $reviewids[] = 'note_item_' . $review->getId();
        }
        $content = Mage::app()->getLayout()->createBlock('tribereview/review')->getAllreviews()->toHtml();
        $info = array('ids' => $reviewids, 'content' => $content);
        return $info;
    }
    public function editinfoAction() {
        $review_id = $this->getRequest()->getPost('id');
        $review_entity = $this->getRequest()->getPost('entity_type');
        $data_arr = array();
        if ($review_id && $review_entity) {
            if($review_entity=='arc'){
            $arc = Mage::getModel('arccore/arc')
                    ->setStoreId($this->getRequest()->getParam('store', 0))
                    ->setAttributeSetId($this->_attributesetId)
                    ->setEntityTypeId($this->_entityTypeId);
            $data = $arc->load($review_id);
            $data_arr = $data->getData();
            }else if($review_entity=='seller'){
                $seller_review = Mage::helper('tribereview')->allsellerreviews($review_id);
                 if(!empty($seller_review)){
                 $data_arr['rate_class'] = 'star-ratereview-profile';
                 $data_arr['trrv_rating'] =  (int) round($seller_review[0]['custom_rating']);
                 $data_arr['trrv_review'] = $seller_review[0]['detail'];
                  $data_arr['entity_id'] = $seller_review[0]['review_id'];
                 }
            }else if($review_entity=='catalog_product'){
                $product_review = Mage::helper('tribereview')->allproductreviews($review_id);//echo "<pre>";print_r($product_review);exit;
                 if(!empty($product_review)){
                  $summary_collection=  Mage::getModel('rating/rating')->getReviewSummary($product_review[0]['review_id']);//echo $summary_collection->getSelect()->__toString();exit;
                  $review_data = $summary_collection->getData();
                   $rate_percent = ceil($review_data['sum'] / $review_data['count']);
                   $rate = ($rate_percent/100) * 5;
                 $data_arr['rate_class'] = 'star-ratereview-profile';
                 $data_arr['trrv_rating'] =  (int) round($rate);
                 $data_arr['trrv_review'] = $product_review[0]['detail'];
                  $data_arr['entity_id'] = $product_review[0]['review_id'];
                 }
            }
            echo Mage::helper('core')->jsonEncode($data_arr);
        }
    }
      public function deleteAction() {
        $id = $this->getRequest()->getPost('id');
        if ($id) {
            $err = parent::deleteAction($id);
            echo $err;
        } else {
            echo 'error';
        }
    }
    public function sellersaveAction(){
       // echo $this->_redirectReferer();exit;
        //print_r($_SERVER['HTTP_REFERER']);exit;
         $id = $this->getRequest()->getParam('id');
          $resource = Mage::getSingleton('core/resource');
          $writeConnection = $resource->getConnection('core_write');
          $data = $this->getRequest()->getPost();
          $data = $data['arc'];
          $data['trrv_review'] = addslashes($data['trrv_review']);
          $upd_query = "UPDATE `review_detail` SET detail='".$data['trrv_review']."', custom_rating='".$data['trrv_rating']."' WHERE review_id=".$id;
          $writeConnection->query($upd_query);
          header("Location:".$_SERVER['HTTP_REFERER']);
    }
     public function productsaveAction(){
         $id = $this->getRequest()->getParam('id');
          $resource = Mage::getSingleton('core/resource');
          $writeConnection = $resource->getConnection('core_write');
          $readConnection = $resource->getConnection('core_read');
          $data = $this->getRequest()->getPost();
         $data = $data['arc'];
          $data['trrv_review'] = addslashes($data['trrv_review']);
         $rating=array();
         //$rating = array(1=>$data['trrv_rating'],2=>$data['trrv_rating'],3=>$data['trrv_rating']);
        
         $upd_query = "UPDATE `review_detail` SET detail='".$data['trrv_review']."' WHERE review_id=".$id;
         $writeConnection->query($upd_query);
          $review     = Mage::getModel('review/review');
          $rating_option=$readConnection->fetchAll('SELECT * FROM `rating_option` WHERE `value` ='.$data['trrv_rating']);
           $rating_option_id = array_combine(array_column( $rating_option,'rating_id'),array_column( $rating_option,'option_id'));
          $rating= $data['trrv_rating']*20;
         //echo "<pre>";print_r($rating_option_id);exit;
         $writeConnection->query("update `rating_option_vote` SET percent=$rating,value=".$data['trrv_rating']." WHERE review_id= $id");
         foreach($rating_option_id as $k=>$v){
             $writeConnection->query("update `rating_option_vote` SET option_id=$v WHERE review_id= $id AND rating_id =$k");
         }
        $this->_redirectReferer();
    }

}
