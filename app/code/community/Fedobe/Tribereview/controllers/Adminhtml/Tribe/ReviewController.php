<?php

/**
 * Manage Review Attribute controller file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribereview
 * @author      Fedobe Magento Team
 */
require_once Mage::getBaseDir('code').DS.'community/Fedobe/Arccore/controllers/Adminhtml/ProfilesController.php';
class Fedobe_Tribereview_Adminhtml_Tribe_ReviewController extends Fedobe_Arccore_Adminhtml_ProfilesController {

    protected $_entityTypeId;

    public function preDispatch() {
        parent::preDispatch();
        $this->_entityTypeId = Mage::helper('arccore')->getEntityTypeId('arccore');
    }

    /**
     * Init actions
     *
     */
    protected function _initAction() {
        // load layout, set active menu and breadcrumbs

        $this->_title($this->__('Tribe'))
                ->_title($this->__('Manage Review'));
 
        if ($this->getRequest()->getParam('popup')) {
            $this->loadLayout('popup');
        } else {
            $this->loadLayout()
                    ->_setActiveMenu('fedobe/tribereview_manage_review');
        }
        return $this;
    }

    /**
     * Index action method
     */
    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }

    /**
     * Code to display the form
     * 
     * The main form container gets added to the content and the tabs block gets added to left.
     */
    public function newAction() {
        $attrsetid = $this->getRequest()->getParam('set');
        if ($attrsetid) {
            $this->_forward('edit');
        } else {
            $this->loadLayout();
            $this->renderLayout();
        }
    }

    /**
     * Edit Review Attribute
     */
    public function editAction() {
         parent::editAction();
         $id = $this->getRequest()->getParam('id');
         $this->_initAction()
               ->_addBreadcrumb(
                        $id ? Mage::helper('tribereview')->__('Edit') : Mage::helper('tribereview')->__('New'), $id ? Mage::helper('tribereview')->__('Edit') : Mage::helper('tribereview')->__('New'));
        $arc = parent::loadProfile();
        if($id)
         $this->_title($this->__($arc->getTrrvName()));
        else
         $this->_title($this->__('New Review'));
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->renderLayout();
    }

    public function validateAction() {
        parent::validateAction();
    }

    /**
     * Delete tribereview attribute
     *
     * @return null
     */
    public function deleteAction() {
        if ($id = $this->getRequest()->getParam('id')) {
            $res = parent::deleteAction();
            
             if($res['success']){
                 Mage::getSingleton('adminhtml/session')->addSuccess($res['message']);
                 $this->_redirect('*/*/');
                 return;
             }else{
                  Mage::getSingleton('adminhtml/session')->addError($res['message']);
                  $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('attribute_id')));
                  return;
             }
        }
        Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('tribereview')->__('Unable to find a review to delete.'));
        $this->_redirect('*/*/');
    }
    public function getProfiledataAction(){
     $data = array();
        $profile_id = $this->getRequest()->getPost('profile_id');
        $collection_data = Mage::getModel('arccore/arc')->getCollection()
         ->addAttributeToSelect('*')
        ->addFieldToFilter('trcr_profile_type',$profile_id)->load();
        $str='';
         $reviewId = $this->getRequest()->getPost('id');
         $username = 0;
         if($reviewId){
            $review_data = Mage::getModel('arccore/arc')->getCollection()->addAttributeToSelect('*')
        ->addFieldToFilter('entity_id',$reviewId)->getFirstItem()->getData();
           $username = $review_data['trcr_username_list'];
        }
        foreach($collection_data as $val){
              if($username==$val->getEntityId())
               $str.="<option value='".$val->getEntityId()."' selected='selected'>".$val->getTrcrName()."</option>";
             else
               $str.="<option value='".$val->getEntityId()."'>".$val->getTrcrName()."</option>";
        }
        $data['data'] = $str;
        echo Mage::helper('core')->jsonEncode($data);
    }

}
