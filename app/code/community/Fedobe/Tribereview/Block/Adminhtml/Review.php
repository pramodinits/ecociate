<?php
/**
 * Grid container file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribereview
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Tribereview_Block_Adminhtml_Review extends Mage_Adminhtml_Block_Widget_Grid_Container
{       
  public function __construct()
  {
    /*both these variables tell magento the location of our Grid.php(grid block) file.
     * $this->_blockGroup.'/' . $this->_controller . '_grid'
     * i.e clarion_Customerattribute/adminhtml_customerattribute_grid
     * $_blockGroup - is your module's name.
     * $_controller - is the path to your grid block. 
     */
    
    $this->_controller = 'adminhtml_review';
    $this->_blockGroup = 'tribereview';
    
    $this->_headerText = Mage::helper('tribereview')->__('Manage Review');
    $this->_addButtonLabel = Mage::helper('tribereview')->__('Add New Review');
    parent::__construct();
    $this->_removeButton('add');
  }
}