<?php

/**
 * Customer attribute edit block
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribereview
 * @author      Fedobe Magento Team
 */
class Fedobe_Tribereview_Block_Adminhtml_Review_Edit extends Fedobe_Arccore_Block_Adminhtml_Arc_Edit {

    public function __construct() {
        parent::__construct();
        $this->_controller = 'tribe_review';
        $this->_blockGroup = 'tribereview';
    }
}
