<?php

/**
 * Manage Customer Attribute grid block
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribereview
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Tribereview_Block_Adminhtml_Review_Grid extends Fedobe_Arccore_Block_Adminhtml_Arccore_Grid {

    public function __construct() {
        parent::__construct();
        $this->setAttributeSetId(Mage::helper('tribereview')->getAttributeSetId());
    }

    /**
     * Prepare customer attributes grid collection object
     *
     * @return Fedobe_Tribereview_Block_Adminhtml_Review_Grid
     */
    protected function _prepareCollection() {
        $collection = Mage::getModel('arccore/arc')->getCollection()->addAttributeToSelect('*');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('entity_id', array(
            'header' => Mage::helper('tribereview')->__('Review ID'),
            'width' => '50px',
            'type' => 'number',
            'index' => 'entity_id',
        ));

        $profileopt = Mage::helper('arccore')->attribute_options('trcr_profile_list');
        $final_profileopt = array();
        foreach ($profileopt as $k => $v) {
            if ($v['value'])
                $final_profileopt[$v['value']] = $v['label'];
        }
        $this->addColumn('trcr_profile_list', array(
            'header' => Mage::helper('tribereview')->__('Profile Type'),
            'index' => 'trcr_profile_list',
            'width' => '80px',
            'type' => 'options',
            'options' => $final_profileopt
        ));


        $attribute = Mage::getModel('eav/config')->getAttribute('fedobe_arccore_arc', 'trcr_profile_type');
        $profile_type_attr_id = $attribute->getId();
        $attribute = Mage::getModel('eav/config')->getAttribute('fedobe_arccore_arc', 'trcr_name');
        $trcr_name_attr_id = $attribute->getId();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $arc_int = $resource->getTableName('arccore/arc_entity_int');
        $arc_varchar = $resource->getTableName('arccore/arc_entity_varchar');
        $query = "SELECT entity_id,value FROM `$arc_varchar` WHERE  attribute_id = $trcr_name_attr_id AND entity_id IN (SELECT entity_id FROM $arc_int WHERE `attribute_id` = $profile_type_attr_id)";
        $res = $readConnection->fetchAll($query);
        $finalopt = array();
        foreach ($res as $k => $v) {
            $finalopt[$v['entity_id']] = $v['value'];
        }
        $this->addColumn('trcr_username_list', array(
            'header' => Mage::helper('tribereview')->__('Name'),
            'index' => 'trcr_username_list',
            'width' => '100px',
            'type' => 'options',
            'options' => $finalopt
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('tribereview')->__('Created At'),
            'index' => 'created_at',
            'width' => '130px',
        ));


        $attributeSetId = Mage::helper('tribereview')->getAttributeSetId();
        $this->addColumn('action', array(
            'header' => Mage::helper('tribereview')->__('Action'),
            'width' => '50px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('tribereview')->__('Edit'),
                    'url' => array(
                        'base' => '*/*/edit/set/' . $attributeSetId,
                        'params' => array('store' => $this->getRequest()->getParam('store'))
                    ),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
        ));


        return parent::_prepareColumns();
    }

}