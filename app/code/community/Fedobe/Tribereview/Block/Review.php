<?php
class Fedobe_Tribereview_Block_Review extends Mage_Core_Block_Template {
 protected function _prepareLayout() {
        parent::_prepareLayout();
        return $this;
    }
    public function getAllreviews() {
        $profile_data = $seller_review = $product_review = $all_reviews=array();
        $profile_data = Mage::helper('tribecore/page')->getInfo();
         if(Mage::helper('tribereview')->is_customer() && Mage::helper('tribereview')->isownprofile($profile_data)){
          $collection = Mage::helper('tribereview')->getAllReview($profile_data['trcr_customer_list'],'trcr_customer_list');
          $product_review = $this->getallproductreviews();
          $seller_review = $this->getAllsellerreviews();
         }else{
             $collection = Mage::helper('tribereview')->getAllReview($profile_data['entity_id'],'trcr_username_list');
         }
         $width = Mage::getStoreConfig('notes/general/profile_icon_width');
        $height = Mage::getStoreConfig('notes/general/profile_icon_height');
       /* if(!Mage::helper('tribereview')->isownprofile($profile_data)){
         $collection = Mage::helper('tribereview')->getAllReview($profile_data['entity_id'],'trcr_username_list');
        }else{
           $collection = Mage::helper('tribereview')->getAllReview($profile_data['trcr_customer_list'],'trcr_customer_list');
          $product_review = $this->getallproductreviews();
          $seller_review = $this->getAllsellerreviews();
        }*/
        $in_own_profile = 0;
        if(Mage::helper('tribereview')->is_customer() && Mage::helper('tribereview')->isownprofile($profile_data))
            $in_own_profile=1;
        $profileimage = $customerprofileimage='';
         if($collection->getSize()){
            foreach($collection as $k=>$v){
                $reviewdata=array();
                $reviewdata = $v->getData();
                $reviewdata['rate_class'] = 'star-ratereview-profile';
                $reviewdata['entity_type'] = 'arc';
                if($in_own_profile){
                    $reviewdata['own_profile']=1;
                    $profdata = Mage::helper('tribereview')->getprofile_by_entityid($v['trcr_username_list']);
                    $reviewdata['review_for'] = $profdata['trcr_name'];
                    $profileimage = Mage::helper('arccore')->resizeImg($profdata['trcr_profile_pic'], $width, $height);
                    $reviewdata['profile_image'] = $profileimage;
                    $reviewdata['profile_url'] =($profdata['trcr_username'])?Mage::helper('tribecustomer')->getFrontendProductUrl($profdata['trcr_username']):'javascript:void(0);';
                }else{
                    $reviewdata['own_profile']=0;
                    $customerinfo = Mage::helper('tribereview')->getcustomerdata($v['trcr_customer_list']);
                    $customerdata=array();
                    $customerdata = Mage::helper('tribereview')->getprofile_by_customerlist($v['trcr_customer_list']);
                   // echo $customerdata['trcr_profile_pic']."================";
                    if($customerdata['trcr_profile_pic']){
                    $customerprofileimage = Mage::helper('arccore')->resizeImg($customerdata['trcr_profile_pic'], $width, $height);
                    $reviewdata['profile_image'] = $customerprofileimage;
                    }
                    if(!isset($customerdata['trcr_name']))
                    $customer_name = $customerinfo['firstname']."&nbsp;".$customerinfo['lastname'];
                    else
                    $customer_name =$customerdata['trcr_name'];
                    $reviewdata['review_by'] = $customer_name;
                    $reviewdata['profile_url'] =($customerdata['trcr_username'])? Mage::helper('tribecustomer')->getFrontendProductUrl($customerdata['trcr_username']):'javascript:void(0);';
                }
                $all_reviews[]=$reviewdata;
            }
        }
        if(!empty($product_review)){
        foreach($product_review as $k1=>$v1){
            $productdata=array();
            $summary_collection = $data =array();
            $summary_collection=  Mage::getModel('rating/rating')->getReviewSummary($v1['review_id']);
            $data = $summary_collection->getData();
            $productdata['rate_class'] = 'star-ratereview-profile';
            $rate_percent = ceil($data['sum'] / $data['count']);
            $rate = ($rate_percent/100) * 5;
            $productdata['trrv_rating'] = $rate;
            $productdata['trrv_review'] = $v1['detail'];
            $productdata['created_at'] = $v1['created_at'];
            $productdata['entity_id'] = $v1['review_id'];
            $productdata['entity_type'] ='catalog_product';
            if($in_own_profile){
                    $productdata['own_profile']=1;
                    $productdata['review_for'] = $v1['productname'];
                   // $prd_img = Mage::getBaseUrl('media').'catalog/product'.$v1['productimage'];
                    $productdata['profile_image'] = Mage::helper('tribereview')->resizeImg($v1['productimage'],  $width, $height);
                    $productdata['profile_url'] =($v1['url_key'])?Mage::getBaseUrl().$v1['url_key'].'.html':'javascript:void(0);';
                }else{
                    $productdata['own_profile']=0;
                    $customerdata = Mage::helper('tribereview')->getprofile_by_customerlist($v['trcr_customer_list']);
                    if($customerdata['trcr_profile_pic']){
                    $customerprofileimage = Mage::helper('arccore')->resizeImg($customerdata['trcr_profile_pic'],  $width, $height);
                    $productdata['profile_image'] = $customerprofileimage;
                    $productdata['profile_url'] =($v1['url_key'])? Mage::getBaseUrl().$v1['url_key'].'.html':'javascript:void(0);';
                    }
                }
                array_push($all_reviews,$productdata);
        }
        }
       // echo "<pre>";print_r($seller_review);exit;
        if(!empty($seller_review)){
        foreach($seller_review as $k2=>$v2){
            $sellerdata=array();
            $sellerdata['rate_class'] = 'star-ratereview-profile';
            $sellerdata['trrv_rating'] = $v2['custom_rating'];
            $sellerdata['trrv_review'] = $v2['detail'];
            $sellerdata['created_at'] = $v2['created_at'];
            $sellerdata['entity_id'] = $v2['review_id'];
            $sellerdata['entity_type'] = 'seller';
            if($in_own_profile){
                    $sellerdata['own_profile']=1;
                    $sellerdata['review_for'] = $v2['sellername'];
                    $prd_img = Mage::helper('tribeseller')->resizeImg($v2['sellerimg'],  $width, $height);
                    $sellerdata['profile_image'] =  $prd_img;
                    $sellerdata['profile_url'] = ($v2['trcr_username'])? Mage::helper('tribeseller')->getFrontendProductUrl($v2['trcr_username']):'javascript:void(0);';
                }else{
                    $sellerdata['own_profile']=0;
                    $customerdata = Mage::helper('tribereview')->getprofile_by_customerlist($v2['trcr_customer_list']);
                    if($customerdata['trcr_profile_pic']){
                    $customerprofileimage = Mage::helper('arccore')->resizeImg($customerdata['trcr_profile_pic'],  $width, $height);
                    $sellerdata['profile_image'] = $customerprofileimage;
                    $sellerdata['profile_url'] = ($customerdata['trcr_username'])? Mage::helper('tribeseller')->getFrontendProductUrl($customerdata['trcr_username']):'javascript:void(0);';
                    }
                }
                array_push($all_reviews,$sellerdata);
        }
        }
        
       


// $sort - key or property name 
// $dir - ASC/DESC sort order or empty
     usort($all_reviews, function ($a, $b) use ($key, $dir) {
        $t1 = strtotime($a['created_at']);
        $t2 = strtotime($b['created_at']);
        if ($t1 == $t2) return 0;
        return (strtoupper($dir) == 'DESC' ? ($t1 < $t2) : ($t1 > $t2)) ? -1 : 1;
    });
     //echo "<pre>";
        //print_r($all_reviews);exit;
         $limit = Mage::getStoreConfig('review/general/review_page_limit') ? Mage::getStoreConfig('review/general/review_page_limit') : 2;
        return $all_reviews;
    }
    public function getFormdata(){
        $formdata =array();
         $profile_data = Mage::helper('tribecore/page')->getInfo(); 
         //if(!Mage::helper('tribereview')->isownprofile($profile_data)){//if the logged in email id is same as profile email id
       $customer = Mage::helper('tribereview')->getLoggedincustomerdata();//loggedin customer data
        $profile_data = Mage::helper('tribecore/page')->getInfo();//current profile type regisrty
            $email_option_id = $customer['entity_id'];//Mage::helper('arccore')->getattributeoptionsid('trcr_customer_list',$customer['email']);
            //echo $email_option_id;exit;
            $hidden_value=array();
            $posttype = Mage::helper('arccore')->getattributeoptionsid('trcr_post_type','Review');
            $hidden_value ['trcr_post_type'] = $posttype;//'Review';
            $hidden_value['trcr_customer_list'] = $email_option_id;
          
            $hidden_value['trcr_profile_list'] = $profile_data['trcr_profile_type'];//'Brand';
            $hidden_value['trcr_username_list'] = $profile_data['entity_id'];//'test';
            $config_status = Mage::getStoreConfig('review/general/status');
            $hidden_value['trcr_status'] = $config_status;
            
            $exclude = array_keys($hidden_value);
            $attribute_set_id = Mage::helper('tribereview')->getAttributeSetId();
            $attributes = Mage::helper('arccore')->getFormAttributes($attribute_set_id,$exclude);
            $formdata['attributes'] = $attributes;
            $formdata['hidden_attributes'] = $hidden_value;
          //  }
            return $formdata;
    }
    public function getallproductreviews(){
        $all_product_review = array();
        $customer = Mage::helper('tribereview')->getLoggedincustomerdata();
        if(!empty($customer)){
        $all_product_review=array();
        $all_product_review = Mage::helper('tribereview')->allproductreviews();
        }
        return $all_product_review;
    }
    public function getAllsellerreviews(){
        $all_seller_review=array();
        $all_seller_review = Mage::helper('tribereview')->allsellerreviews();
        return  $all_seller_review;
    }
    public function reviewexist(){
         $profile_data = Mage::helper('tribecore/page')->getInfo();
       return Mage::helper('tribereview')->reviewadded($profile_data);
    }
     public function getCurrentProfileInfo() {
        return Mage::registry('tribeprofileinfo');
    }
}
?>