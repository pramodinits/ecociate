<?php

class Fedobe_Tribereview_Model_Entity_Attribute_Source_Reviewstatus extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    public function getAllOptions($withEmpty = false) {
        $status=array();
        $params_all = Mage::app()->getRequest()->getParams();
        $allOptions = Mage::helper('arccore')->attribute_options('trcr_status');
        $status_id = array_combine(array_column($allOptions,'label'),array_column($allOptions,'value'));
        if(!isset($params_all['id'])){
           unset($status_id['Enabled']);
           $status=array_flip($status_id);
        }else{
           $status=array_flip($status_id);
        }
        return $status;
    }

    public function getOptionText($value) {
        $options = $this->getAllOptions(false);
        foreach ($options as $item) {
            if ($item['value'] == $value) {
                return $item['label'];
            }
        }
        return false;
    }

}