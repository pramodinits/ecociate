<?php

class Fedobe_Tribereview_Model_Entity_Attribute_Source_Rating extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    public function getAllOptions($withEmpty = false) {
        $rating = Mage::getStoreConfig('review/general/rating');//print_r($rating);exit;
        if($rating=='5-star')
           $profiles=array(array('label'=>1,'value'=>1),array('label'=>2,'value'=>2),array('label'=>3,'value'=>3),array('label'=>4,'value'=>4),array('label'=>5,'value'=>5));
        else if($rating=='thumbs')
           $profiles=array(array('label'=>'up','value'=>1),array('label'=>'down','value'=>-1));
        return $profiles;
    }

    public function getOptionText($value) {
        $options = $this->getAllOptions(false);
        foreach ($options as $item) {
            if ($item['value'] == $value) {
                return $item['label'];
            }
        }
        return false;
    }

}