<?php

class Fedobe_Tribereview_Model_Observer {
    public function addButtonReview($observer){
    $container = $observer->getBlock();
    $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                        ->load('Tribe Review', 'attribute_set_name')
                        ->getAttributeSetId();
   // echo $attributeSetId;exit;
    if(null !== $container && $container->getType() == 'tribereview/adminhtml_review') {
        $data_2 = array(
            'label'     => 'Add Review',
            'class'=>'add',
            'onclick'   => 'setLocation(\' '  . Mage::helper('adminhtml')->getUrl('adminhtml/tribe_review/new/set/'.$attributeSetId, array('_secure' => true)) . '\')',
        );
        $container->addButton('addreview', $data_2);
    }
    return $this;
}

public function addscript(Varien_Event_Observer $observer){
    $block = $observer->getEvent()->getBlock();
    $current_attribute_set_id = Mage::helper('arccore')->getEntityAttributeSetId();
   $all_attribute_set = Mage::helper('arccore')->all_attributeset();
    $review_set_id=$all_attribute_set['Tribe Review'];
    
            if (!isset($block)) {
                return $this;
            }//print_r($block->getType());exit;
             if ($block->getType() == 'arccore/adminhtml_arc_edit_tab_attributes' && $current_attribute_set_id==$review_set_id) {
                $form = $block->getForm();
                $review_id=0;
                $review_id = (int) Mage::app()->getRequest()->getParam('id');
               // $cusurl = Mage::helper('arccore')->getProfileInfourlkeyaction(); //echo $cusurl;exit;
                //$profile_type = $form->getElement('trcr_profile_list');
                //$profile_type->setOnchange("setProfiletype(this)");
                
                $status = $form->getElement('trcr_status');
                $config_status = Mage::getStoreConfig('review/general/status');
                if(!$review_id){
                 $status->setAfterElementHtml("<style type='text/css'>#trcr_status{pointer-events:none;}</style>");
                 $status->setValue($config_status);
                }
            }
}
}
