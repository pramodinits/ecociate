<?php

class Fedobe_Tribereview_Model_System_Config_Rating {

   public function toOptionArray() {
        return array(
            array('value' => 'disable', 'label' => 'Disable Ratings'),
            array('value' => "5-star", 'label' => "5 Star Ratings"),
            array('value' => "thumbs", 'label' => "Thumbs Up/Down Ratings")
            );
    }

}
