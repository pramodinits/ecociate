<?php
/**
 * Grid container file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribevideos
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Tribevideos_Block_Adminhtml_Videos extends Mage_Adminhtml_Block_Widget_Grid_Container
{       
  public function __construct()
  {
    /*both these variables tell magento the location of our Grid.php(grid block) file.
     * $this->_blockGroup.'/' . $this->_controller . '_grid'
     * i.e clarion_Customerattribute/adminhtml_customerattribute_grid
     * $_blockGroup - is your module's name.
     * $_controller - is the path to your grid block. 
     */
    
    $this->_controller = 'adminhtml_videos';
    $this->_blockGroup = 'tribevideos';
    
    $this->_headerText = Mage::helper('tribevideos')->__('Manage Videos');
    $this->_addButtonLabel = Mage::helper('tribevideos')->__('Add New Video');
    parent::__construct();
    $this->_removeButton('add');
  }
}