<?php

/**
 * Customer attribute edit block
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribevideos
 * @author      Fedobe Magento Team
 */
class Fedobe_Tribevideos_Block_Adminhtml_Videos_Edit extends Fedobe_Arccore_Block_Adminhtml_Arc_Edit {

    public function __construct() {
        parent::__construct();
        $this->_controller = 'tribe_videos';
        $this->_blockGroup = 'tribevideos';
    }
}
