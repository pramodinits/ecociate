<?php

class Fedobe_Tribevideos_Block_Singlevideo extends Mage_Core_Block_Template {

    public function __construct() {
        parent::__construct();
        if (!$this->_template)
            $this->setTemplate('fedobe/tribe/videos/singlevideo.phtml');
    }

    public function getCurrentProfileInfo() {
        return Mage::registry('tribeprofileinfo');
    }
    
    public function getVideoInfo(){
        $info = $this->getCurrentProfileInfo();
        $url_key = Mage::app()->getRequest()->getParam('singlepostkey');
        $videocollection = Mage::helper('tribevideos')->getSingleVideo($info['entity_id'],$url_key);
        return $videocollection;
    }

}