<?php
class Fedobe_Tribevideos_Block_Videos extends Mage_Core_Block_Template {
    
    private $_videoscollection;
    private $_profileinfo;
    
     protected function _prepareLayout() {
        parent::_prepareLayout();
        return $this;
    }
     public function getCurrentProfileInfo() {
        return Mage::registry('tribeprofileinfo');
    }
    public function getAllVideos(){
        $info = $this->getCurrentProfileInfo();
        $videoscollection = Mage::helper('tribevideos')->getAllVideos($info['entity_id']);
        return $videoscollection;
    }
     public function getFormdata(){
       $hidden_value= $formdata =array();
        $hidden_value = $this->gethidden_attribues();
            $exclude = array_keys($hidden_value);
            $attribute_set_id = Mage::helper('tribevideos')->getAttributeSetId();
            $attributes = Mage::helper('arccore')->getFormAttributes($attribute_set_id,$exclude);
            $formdata['attributes'] = $attributes;
            $formdata['hidden_attributes'] = $hidden_value;
            return $formdata;
    }
    public function gethidden_attribues(){
         $customer = Mage::helper('tribecore')->getLoggedincustomerdata();//loggedin customer data
           $profile_data = Mage::helper('tribecore/page')->getInfo();//current profile type regisrty
            $email_option_id = $customer['entity_id'];
         $hidden_value=array();
            $posttype = Mage::helper('arccore')->getattributeoptionsid('trcr_post_type','Videos');
            $hidden_value ['trcr_post_type'] = $posttype;
            $hidden_value['trcr_customer_list'] = $email_option_id;
          
            $hidden_value['trcr_profile_list'] = $profile_data['trcr_profile_type'];//'Brand';
            $hidden_value['trcr_username_list'] = $profile_data['entity_id'];//'test';
            $hidden_value['trcr_status'] = 2;
            $hidden_value['trvd_video_oembed'] = '';
             $hidden_value['trcr_urlkey'] = '';
            return $hidden_value;
    }
    public function setVideosCollectionInfo($videoscollection,$info){
        $this->_videoscollection = $videoscollection;
        $this->_profileinfo = $info;
        return $this;
    }
     public function getVideosCollection(){
        return $this->_videoscollection;
    }
    public function getVideosProfileInfo(){
        return $this->_profileinfo;
    }
}
?>