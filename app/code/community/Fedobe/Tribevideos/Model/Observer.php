<?php

class Fedobe_Tribevideos_Model_Observer {
    public function addButtonVideo($observer){
    $container = $observer->getBlock();
    $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                        ->load('Tribe Videos', 'attribute_set_name')
                        ->getAttributeSetId();
    if(null !== $container && $container->getType() == 'tribevideos/adminhtml_videos') {
        $data_2 = array(
            'label'     => 'Add Video',
            'class'=>'add',
            'onclick'   => 'setLocation(\' '  . Mage::helper('adminhtml')->getUrl('adminhtml/tribe_videos/new/set/'.$attributeSetId, array('_secure' => true)) . '\')',
        );
        $container->addButton('addvideo', $data_2);
    }
    return $this;
}

}
