<?php

require_once Mage::getBaseDir('code') . DS . 'community/Fedobe/Arccore/controllers/Adminhtml/ProfilesController.php';

class Fedobe_Tribevideos_VideosController extends Fedobe_Arccore_Adminhtml_ProfilesController {

    protected $_entityTypeId;

    public function preDispatch() {
        Mage::getSingleton('core/session', array('name' => 'frontend'));
        $this->_entityTypeId = Mage::helper('arccore')->getEntityTypeId('arccore');
        $this->_attributesetId = Mage::helper('tribevideos')->getAttributeSetId();
        $bypassaction = array('save','editinfo','delete','loadmorevideos');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }
    public function saveAction(){
        $data = $this->getRequest()->getPost();
        $Arcdata=$data['arc'];
        $content = '';
        if(isset($data['arc']['trvd_video'] )&& $data['arc']['trvd_video']){
        $url_data = parse_url($data['arc']['trvd_video']);
        $url = $url_data['scheme'].'://'.$url_data['host'].'/oembed?url='.urlencode($data['arc']['trvd_video']);
        $content = file_get_contents($url);
        if($content){
        $Arcdata['trvd_video_oembed']= $content;
         $this->getRequest()->setPost('arc',$Arcdata);
        }
        parent::saveAction();
        }
    }
     public function deleteAction() {
        $id = $this->getRequest()->getPost('id');
        if ($id) {
            $err = parent::deleteAction($id);
            echo $err;
        } else {
            echo 'error';
        }
    }

    public function loadmorevideosAction() {
        $info = Mage::helper('tribevideos')->decrypt($this->getRequest()->getPost('info')); //echo "<pre>";print_r($info);exit;
        if ($info) {
            $entity_id = $info['id'];
            $videoscollection = Mage::helper('tribevideos')->getAllVideos($entity_id);
            $videos_cnt = count($videoscollection); //echo $videos_cnt;exit;
            $data = array('ids' => array(), 'content' => '');
            if ($videos_cnt) {
                $info['profile_info']['user_log_id'] = $info['is_logged_in'];
                $info['profile_info']['logged_in_email'] = $info['logged_in_email'];
                $videos_info = $this->getVideosContent($videoscollection, $info['profile_info']);
                $data = array_merge($data, $videos_info);
            }
            $result = array('count' => $videos_cnt, 'ids' => $data['ids'], 'content' => $data['content']);
            echo Mage::helper('core')->jsonEncode($result);
        }
    }

    private function getVideosContent($videoscollection, $profile_info) {
        $videosids = array();
        foreach ($videoscollection as $k => $video) {
            $videosids[] = 'photo_item_' . $video->getId();
        }
        
        $update = $this->getLayout()->getUpdate();
        $update->addHandle('profile_videos');
        $this->addActionLayoutHandles();
        $this->loadLayoutUpdates();
        $this->generateLayoutXml()->generateLayoutBlocks();
        $content = $this->getLayout()->getBlock('list_videos')->setVideosCollectionInfo($videoscollection, $profile_info)->setTemplate('fedobe/tribe/videos/ajaxvideo.phtml')->toHtml();
        $info = array('ids' => $videosids, 'content' => $content);
        return $info;
    }

    public function editinfoAction() {
        $videos_id = $this->getRequest()->getPost('id');
        if ($videos_id) {
            $width = Mage::getStoreConfig('videos/general/profile_icon_width');
            $height = Mage::getStoreConfig('videos/general/profile_icon_height');
            $arc = Mage::getModel('arccore/arc')
                    ->setStoreId($this->getRequest()->getParam('store', 0))
                    ->setAttributeSetId($this->_attributesetId)
                    ->setEntityTypeId($this->_entityTypeId);
            $data = $arc->load($videos_id);
            $data_arr = $data->getData();
            echo Mage::helper('core')->jsonEncode($data_arr);
        }
    }

}