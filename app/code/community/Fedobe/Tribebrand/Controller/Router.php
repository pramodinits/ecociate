<?php

class Fedobe_Tribebrand_Controller_Router extends Mage_Core_Controller_Varien_Router_Standard {

    public function match(Zend_Controller_Request_Http $request) {
        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                    ->setRedirect(Mage::getUrl('install'))
                    ->sendResponse();
            exit;
        }

        $identifier = trim($request->getPathInfo(), '/');
        //echo $identifier;exit;
        $condition = new Varien_Object(array(
            'identifier' => $identifier,
            'continue' => true
        ));
        Mage::dispatchEvent('cms_controller_router_match_before', array(
            'router' => $this,
            'condition' => $condition
        ));
        $identifier = $condition->getIdentifier();
//echo $identifier;exit;
        $customkey = $defaultaction = '';
        //Here to get all allowed post types
        $allowed_posttypes = Mage::helper('tribebrand')->getAllowedPosttypes();
        if (!empty($allowed_posttypes)) {
            foreach ($allowed_posttypes as $pk => $pv) {
                if (in_array($pv, array('profile', 'products'))) {
                    $url_identifier = $pv;
                } else {
                    $configpath_id = "$pv/general/identifier";
                    $url_identifier = Mage::getStoreConfig($configpath_id);
                }
                if ($url_identifier) {
                    $pos = strpos($identifier, $url_identifier);
                    if ($pos !== FALSE) {
                        $post_type_str = substr($identifier, $pos, strlen(trim($identifier, '/')));
                        //Here to check for individual Notes or any individual post type
                        if (strlen($post_type_str) > strlen($url_identifier)) {
                            $single_configpath_id = "$pv/general/single_url_suffix";
                            $single_url_suffix = Mage::getStoreConfig($single_configpath_id);
                            $indurl_key = str_replace("$url_identifier/", "", str_replace($single_url_suffix, '', $post_type_str));
                            $defaultaction = 'single' . $url_identifier;
                            $customkey = $indurl_key;
                            $identifier = trim(substr($identifier, 0, $pos), '/');
                        } else {
                            $defaultaction = $url_identifier;
                        }
                        $identifier = trim(substr($identifier, 0, $pos), '/');
                    }
                }
            }
        }
        if ($condition->getRedirectUrl()) {
            Mage::app()->getFrontController()->getResponse()
                    ->setRedirect($condition->getRedirectUrl())
                    ->sendResponse();
            $request->setDispatched(true);
            return true;
        }

        if (!$condition->getContinue()) {
            return false;
        }
        $brand = Mage::getModel('tribebrand/brand');
        $brandId = $brand->checkIdentifier($identifier, Mage::app()->getStore()->getId());
        if (!$brandId) {
            return false;
        }
        if (!$defaultaction) {
            $allowed_tabs = Mage::helper('tribebrand')->getAllowedPosttypes();
            if (!empty($allowed_tabs)) {
                if ($allowed_tabs[0] == 'profile') {
                    $content = $brand->load($brandId);
                    if ($content['trcr_content_top']) {
                        $defaultaction = $allowed_tabs[0];
                    } else {
                        $defaultaction = 'products';
                    }
                } else {
                    $defaultaction = $allowed_tabs[0];
                }
            } else {
                $defaultaction = 'profile';
            }
        }

//echo $defaultaction;exit;
        $attributesetid = Mage::helper('tribebrand')->getAttributeSetId();

        $request->setModuleName('tribebrand')
                ->setControllerName('page')
                ->setActionName($defaultaction)
                ->setParam('attribute_set_id', $attributesetid)
                ->setParam('brand_id', $brandId); //echo $defaultaction;exit;
        if ($customkey) {
            $request->setParam('singlepostkey', $customkey);
        }
        
        $rewrite_url = $identifier;
        if($defaultaction=='products')
        $rewrite_url = "$identifier/$defaultaction";
        
        $request->setAlias(
                Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS, $rewrite_url
        );
        return true;
    }

}