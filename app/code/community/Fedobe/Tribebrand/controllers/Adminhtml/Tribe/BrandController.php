<?php

/**
 * Manage Brand Attribute controller file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribebrand
 * @author      Fedobe Magento Team
 */
require_once Mage::getBaseDir('code') . DS . 'community/Fedobe/Arccore/controllers/Adminhtml/ProfilesController.php';

class Fedobe_Tribebrand_Adminhtml_Tribe_brandController extends Fedobe_Arccore_Adminhtml_ProfilesController {

    protected $_entityTypeId;

    public function preDispatch() {
        parent::preDispatch();
        $this->_entityTypeId = Mage::helper('arccore')->getEntityTypeId('arccore');
    }

    public function checkurlkeyAction() {
        $data = array();
        $urlkey = $this->getRequest()->getPost('urlkey');
        $attrcode = $this->getRequest()->getPost('attrcode');
        $id = $this->getRequest()->getPost('id');
        $model = Mage::getModel('arccore/arc')->getCollection()
                ->addAttributeToSelect('entity_id')
                ->addFieldToFilter($attrcode, array('eq' => $urlkey))
                ->addFieldToFilter('entity_id', array('neq' => $id));
        $model->getSelect()->limit(1);
        if ($id) {
            $username = Mage::getModel('arccore/arc')->load($id)->getData($attrcode);
            $user = Mage::getModel('admin/user')->loadByUsername($username);
            $savedid = $user->getUserId();
        }
        $usercollection = Mage::getModel('admin/user')->getCollection()
                ->addFieldToSelect('user_id')
                ->addFieldToFilter('username', array('eq' => $urlkey));
        if ($savedid) {
            $user->addFieldToFilter('user_id', array('neq' => $savedid));
        }
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $result = $read->query($usercollection->getSelect());
        $row = $result->fetch();
        $flag = ($row['user_id']) ? 1 : 0;

        //Here let's check the URL for Category and product and CMS pages
        $urlrewritetable = $resource->getTableName('core_url_rewrite');
        $prodsuffix = Mage::getStoreConfig('catalog/seo/product_url_suffix');
        $catsuffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
        $sql = "SELECT `url_rewrite_id` FROM `$urlrewritetable` WHERE REPLACE(REPLACE(`request_path`,'$prodsuffix',''),'$catsuffix','') = '$urlkey' LIMIT 1";
        $urlmore = $read->query($sql);
        $urldata = $urlmore->fetch();
        $flag = ($urldata['url_rewrite_id']) ? 1 : 0;

        $err = 1;
        if (!$model->getData() && !$flag) {
            $err = 0;
        }
        $data['err'] = $err;
        $data['msg'] = ($err) ? Mage::helper('tribebrand')->__("This username already exists!") : Mage::helper('tribebrand')->__("Username available");
        $data['fronturl'] = ($err) ? "" : Mage::helper('tribebrand')->getFrontendProductUrl($urlkey);


        $res = Mage::helper('core')->jsonEncode($data);
        echo $res;
    }

    public function getCustomerInfoAction() {
        $data = array();
        $customer_id = $this->getRequest()->getPost('customer_id');
        $user = mage::getModel('customer/customer')->load($customer_id);
        $custdata = $user->getData();
        if ($custdata) {
            //Here let's check username exists or not if not there
            //then save it by auto creating from email
            if (!$custdata['username']) {
                $username = Mage::helper('arccore')->clean($custdata['email']);
            }
            $data['err'] = 0;
            $data = $user->getData();
            $data['url'] = Mage::helper('tribebrand')->getFrontendProductUrl($data['username']);
            $data['data'] = $data;
        } else {
            $data['err'] = 1;
            $data['data'] = NULL;
        }
        echo Mage::helper('core')->jsonEncode($data);
    }

    /**
     * Init actions
     *
     */
    protected function _initAction() {
        // load layout, set active menu and breadcrumbs

        $this->_title($this->__('Tribe'))
                ->_title($this->__('Manage Brands'));
        //$this->_title($this->__('New Brand'));

        if ($this->getRequest()->getParam('popup')) {
            $this->loadLayout('popup');
        } else {
            $this->loadLayout()
                    ->_setActiveMenu('fedobe/tribebrand_manage_brands');
        }
        return $this;
    }

    /**
     * Index action method
     */
    public function indexAction() {
        $this->_initAction()
                ->renderLayout();
    }

    /**
     * Code to display the form
     * 
     * The main form container gets added to the content and the tabs block gets added to left.
     */
    public function newAction() {
        $attrsetid = $this->getRequest()->getParam('set');
        if ($attrsetid) {
            $this->_forward('edit');
        } else {
            $this->loadLayout();
            $this->renderLayout();
        }
    }

    /**
     * Edit Brand Attribute
     */
    public function editAction() {

        parent::editAction();
        $id = $this->getRequest()->getParam('id');
        $this->_initAction()
                ->_addBreadcrumb(
                        $id ? Mage::helper('tribebrand')->__('Edit') : Mage::helper('tribebrand')->__('New'), $id ? Mage::helper('tribebrand')->__('Edit') : Mage::helper('tribebrand')->__('New'));
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $arc = parent::loadProfile();
        if ($id)
            $this->_title($this->__($arc->getTrcrName()));
        else
            $this->_title($this->__('New Brand'));
        $this->renderLayout();
    }

    public function validateAction() {
        parent::validateAction();
    }

    /**
     * 
     * Save brand attributes
     */
    public function saveAction() {
        $error = 0;
        $data = $this->getRequest()->getPost('arc');
        $Arcdata=$data;
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $storeId = Mage::app()->getRequest()->getParam('store');
        $customermodel = Mage::getModel('customer/customer');
        $email = $data['trcr_email'];
        $customercreate = 0;
        $usercreate = 0;
        $brand_entity_id = $this->getRequest()->getParam('id');
        $redirectBack = $this->getRequest()->getParam('back', false);
        if ($brand_entity_id) {
            $branddata = Mage::getModel('arccore/arc')->load($brand_entity_id);
            //Here let's check if any User or Customer information changed
            $ext_fname = $branddata->getData('trcr_first_name');
            $ext_lname = $branddata->getData('trcr_last_name');
            $ext_email = $branddata->getData('trcr_email');
            $ext_status = $branddata->getData('trcr_status');
            $ext_password = $branddata->getData('trcr_password');
        }
        $new_fname = $data['trcr_first_name'];
        $new_lname = $data['trcr_last_name'];
        $new_email = $data['trcr_email'];
        $new_password = $data['trcr_password'];
        $new_status = $data['trcr_status'];

        //Here let's check the Cutomer exists or not if new Customer
        if (isset($data['trcr_customer_list']) && $data['trcr_customer_list'] && $data['trcr_account_type'] == 2) {
            //Here for existing
            //No need to create the Customer let's create the admin user only by checking
            if ($brand_entity_id) {
                if ($ext_email != $new_email) {
                    $adminusercheck = Mage::helper('tribebrand')->checkAdminUser($email, $email);
                } else {
                    $adminuserid = Mage::helper('tribebrand')->getAdminUserId($email, $email);
                    $adminusercheck = Mage::helper('tribebrand')->checkAdminUser($email, $email, $adminuserid);
                }
            } else {
                $adminusercheck = Mage::helper('tribebrand')->checkAdminUser($email, $email);
            }
            if ($adminusercheck) {
                $error = 1;
                Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('tribebrand')->__("A User with username or email '$email' already exists!.")
                );
            } else {
                try {
                    //Here let's create the User
                    $roleid = Mage::helper('tribebrand')->getBrandRoleId();
                    if ($roleid) {
                        if ($brand_entity_id) {
                            if (($new_status != $ext_status ) || (($ext_fname != $new_fname) || ($ext_lname != $new_lname) || ($ext_email != $new_email))) {
                                $adminuserid = Mage::helper('tribebrand')->getAdminUserId($ext_email, $ext_email);
                                Mage::helper('tribebrand')->createAdminUser($data, $roleid, $adminuserid);
                            }
                        } else {
                            Mage::helper('tribebrand')->createAdminUser($data, $roleid);
                        }
                    } else {
                        $error = 1;
                        Mage::getSingleton('adminhtml/session')->addError(
                                Mage::helper('tribebrand')->__("Brand user Role Not Found !.")
                        );
                    }
                } catch (Exception $e) {
                    $error = 1;
                    Mage::getSingleton('adminhtml/session')->addError(
                            Mage::helper('tribebrand')->__($e->getMessage())
                    );
                }
            }
        } else {
            //Here for new, let's check whether exists with the same email
            $tableName = $resource->getTableName('customer_entity');
            if ($brand_entity_id) {
                if ($ext_email != $new_email) {
                    $query = "SELECT `entity_id` FROM $tableName WHERE `email` = '$new_email' LIMIT 1";
                } else {
                    $query = "SELECT `entity_id` FROM $tableName WHERE `email` = '$email' AND `email` <> '$email' LIMIT 1";
                }
            } else {
                $query = "SELECT `entity_id` FROM $tableName WHERE `email` = '$email' LIMIT 1";
            }
            $res = $readConnection->query($query);
            $cust = $res->fetch();
            $customerdat = array();
            $customerdat['firstname'] = $new_fname;
            $customerdat['lastname'] = $new_lname;
            $customerdat['email'] = $email;
            $customerdat['password'] = $new_password;
            if (empty($cust)) {
                //Here let's proceed and create the customer
                $customercreate = 1;
            } else {
                $error = 1;
                Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('tribebrand')->__("A Customer with email '$email' already exists!.")
                );
            }
            //Here let's check for admin user creation and role assignment
            if ($brand_entity_id) {
                if ($ext_email != $new_email) {
                    $adminusercheck = Mage::helper('tribebrand')->checkAdminUser($email, $email);
                } else {
                    $adminuserid = Mage::helper('tribebrand')->getAdminUserId($email, $email);
                    $adminusercheck = Mage::helper('tribebrand')->checkAdminUser($email, $email, $adminuserid);
                }
            } else {
                $adminusercheck = Mage::helper('tribebrand')->checkAdminUser($email, $email);
            }
            if ($adminusercheck) {
                $error = 1;
                Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('tribebrand')->__("A User with username or email '$email' already exists!.")
                );
            } else {
                $usercreate = 1;
            }
            if ($usercreate && $customercreate) {
                try {
                    //Here let's create the User
                    $roleid = Mage::helper('tribebrand')->getBrandRoleId();
                    if ($roleid) {
                        if ($brand_entity_id) {
                            if (($new_status != $ext_status ) || (($ext_fname != $new_fname) || ($ext_lname != $new_lname) || ($ext_email != $new_email) || ($ext_password != $new_password))) {
                                $adminuserid = Mage::helper('tribebrand')->getAdminUserId($ext_email, $ext_email);
                                Mage::helper('tribebrand')->createAdminUser($data, $roleid, $adminuserid);
                                $query = "SELECT `entity_id` FROM $tableName WHERE `email` = '$ext_email' LIMIT 1";
                                $res = $readConnection->query($query);
                                $cust = $res->fetch();
                                $customer_id = $cust['entity_id'];
                                //Here let's create the Customer
                                $customer = $customermodel->load($customer_id);
                                $customer->setFirstname($new_fname);
                                $customer->setLastname($new_lname);
                                $customer->setEmail($email);
                                $customer->setPassword($new_password);
                                $customer->save();
                            }
                        } else {
                            Mage::helper('tribebrand')->createAdminUser($data, $roleid);
                            //Here let's create the Customer
                            $customermodel->setData($customerdat);
                            $customermodel->save();
                        }
                        $customer_entity_id = $customermodel->getEntityId();
                        $Arcdata['trcr_customer_list'] = $customer_entity_id;
                        $this->getRequest()->setPost('arc',$Arcdata);
                    } else {
                        $error = 1;
                        Mage::getSingleton('adminhtml/session')->addError(
                                Mage::helper('tribebrand')->__("Brand user Role Not Found !.")
                        );
                    }
                } catch (Exception $e) {
                    $error = 1;
                    Mage::getSingleton('adminhtml/session')->addError(
                            Mage::helper('tribebrand')->__($e->getMessage())
                    );
                }
            }
        }
        if ($error) {
            if ($brand_entity_id && $redirectBack) {
                $this->_redirect('*/*/edit', array(
                    'id' => $brand_entity_id,
                    '_current' => true
                ));
            } else {
                $this->_redirect('*/*/', array('store' => $storeId));
            }
        } else {
            parent::saveAction();
        }
    }

    /**
     * Delete brand attribute
     *
     * @return null
     */
    public function deleteAction() {
        if ($id = $this->getRequest()->getParam('id')) {
            $branddata = Mage::getModel('arccore/arc')->load($id);
            $email = $branddata->getData('trcr_email');
            if ($email) {
                $res = parent::deleteAction();
                if ($res['success']) {
                    //Here let's delete the Customer associated
                    $resource = Mage::getSingleton('core/resource');
                    $readConnection = $resource->getConnection('core_read');
                    $tableName = $resource->getTableName('customer_entity');
                    $query = "SELECT `entity_id` FROM $tableName WHERE `email` = '$ext_email' LIMIT 1";
                    $result = $readConnection->query($query);
                    $cust = $result->fetch();
                    $customer_id = $cust['entity_id'];
                    $customer = Mage::getModel('customer/customer')->load($customer_id);
                    $customer->delete();
                    //End 
                    //
                    //Here let's delete the admin user associated 
                    $adminuserid = Mage::helper('tribebrand')->getAdminUserId($email, $email);
                    $user = Mage::getModel('admin/user')->load($adminuserid);
                    $user->delete();
                    //End
                    Mage::getSingleton('adminhtml/session')->addSuccess($res['message']);
                    $this->_redirect('*/*/');
                    return;
                } else {
                    Mage::getSingleton('adminhtml/session')->addError($res['message']);
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('attribute_id')));
                    return;
                }
            } else {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('tribebrand')->__('Custome Or User email not exists!'));                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('tribebrand')->__('Unable to find a brand to delete.'));
        $this->_redirect('*/*/');
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('admin/fedobe/tribebrand/manage_brands');
    }

}