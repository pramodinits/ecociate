<?php

class Fedobe_Tribebrand_Helper_Page extends Fedobe_Tribecore_Helper_Page{

    /**
     * Renders CMS page on front end
     *
     * Call from controller action
     *
     * @param Mage_Core_Controller_Front_Action $action
     * @param integer $pageId
     * @return boolean
     */
    public function renderPage(Mage_Core_Controller_Front_Action $action, $pageId = null,$attrset_id=null) {
        $this->setHandle('brand_page');
        return $this->_renderPage($action, $pageId,$attrset_id);
    }
}