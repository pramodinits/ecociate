<?php

class Fedobe_Tribebrand_Helper_Data extends Fedobe_Tribecore_Helper_Core {

    private $brand_id = null;
    private $trcr_attribute_option = null;
    private $trcr_store_list = null;

    const ATTRIBUTESET_NAME = 'Tribe Brand';

    public function getAttributeSetId() {
        $entityTypeId = Mage::helper('arccore')->getEntityTypeId();
        $attributeSetName = SELF::ATTRIBUTESET_NAME;
        $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                ->getCollection()
                ->setEntityTypeFilter($entityTypeId)
                ->addFieldToFilter('attribute_set_name', $attributeSetName)
                ->getFirstItem()
                ->getAttributeSetId();
        return $attributeSetId;
    }

    public function getProfileType() {
        return 'Brand';
    }

    public function getUrlkeyAttributeCode() {
        return 'trcr_username';
    }

    public function getFrontendProductUrl($key) {
        $urlkey = $key;
        $preffix = Mage::getStoreConfig('tribe_brand/edit/url_prefix');
        $suffix = Mage::getStoreConfig('tribe_brand/edit/url_suffix');
        if ($preffix) {
            $urlkey = "$preffix/$urlkey";
        }
        if ($suffix) {
            $urlkey .=$suffix;
        }
        return trim(Mage::getUrl("$urlkey"), "/");
    }

    public function extractUrlKey($identifier) {
        $key = $identifier;
        $preffix = Mage::getStoreConfig('tribe_brand/edit/url_prefix');
        if ($preffix) {
            $key = str_replace("$preffix/", "", $key);
        }
        $suffix = Mage::getStoreConfig('tribe_brand/edit/url_suffix');
        if ($suffix) {
            $key = str_replace($suffix, "", $key);
        }
        return $key;
    }

    public function getAllowedPosttypes(){
        $final_posttypes = array();
        /*if ($tabs = Mage::getStoreConfig('tribe_brand/tab/posttypes')) {
            $posttypes = Mage::helper('arccore')->attribute_options('trcr_post_type');
            $_options = array();
            foreach ($posttypes as $option) {
                if ($option["value"])
                    $_options[$option["value"]] = $option["label"];
            }
            $tabsarr = explode(',', $tabs);
            if (!empty($tabsarr)) {
                foreach ($tabsarr as $k => $v) {
                    $final_posttypes[$v] = strtolower($_options[$v]);
                }
            }
        }*/
        if ($tabs = Mage::getStoreConfig('tribe_brand/tab/posttypes_taborder')) {
            $final_posttypes = explode(',', $tabs);
            $final_posttypes = array_map('strtolower', $final_posttypes);
        }
        return $final_posttypes;
    }

    private function prepareDefaultUrl($url){
        $allowed_tabs = $this->getAllowedPosttypes();
        if(!empty($allowed_tabs)){
            $default_tab = $allowed_tabs[0];
            return str_replace($default_tab,'',$url);
        }
        return $url;
        
    }

    public function getInfo($data) {
        $data['url'] = $this->getFrontendProductUrl($data[$this->getUrlkeyAttributeCode()]);
        if (Mage::helper('tribecore')->isActionallowed($data)) {
                $data['canbefollowed'] = 1;
        }
        $tabsarr = $this->getAllowedPosttypes();//echo "<pre>";print_r($tabsarr);
        $rmv = -1;
        if (!empty($tabsarr)) {
            foreach ($tabsarr as $k => $v) {
                $helper = "tribe$v";
                if($v == 'notes'){
                    $helper = "tribestatus";
                    $status_data = Mage::helper($helper)->getInfo($v,$data);
                    $status_data['url'] = $this->prepareDefaultUrl($status_data['url']);
                }else if($v == 'profile'){
                    if($data['trcr_content_top']){
                        $url = trim(Mage::getUrl("{$data['trcr_username']}/profile"), "/");
                        $status_data = array('label' => $v, 'count' => 0, 'url' => $this->prepareDefaultUrl($url));
                    }else{
                        $rmv = $k;
                    }
                }else if($v == 'products'){
                    $url = trim(Mage::getUrl("{$data['trcr_username']}/products"), "/");
                    $count = Mage::app()->getLayout()->createBlock('tribebrand/product_list')->getProductCount($data);
                    $status_data = array('label' => $v, 'count' => $count, 'url' => $this->prepareDefaultUrl($url));
                }
                //else if($v == 'followers'){
                  //  $url = trim(Mage::getUrl("{$data['trcr_username']}/followers"), "/");
                  //  $status_data = array('label' => $v, 'count' => 0, 'url' => $this->prepareDefaultUrl($url));
               // }
                else{
                    $status_data = Mage::helper($helper)->getInfo($v,$data);
                    $status_data['url'] = $this->prepareDefaultUrl($status_data['url']);
                }
                $data['tabs'][$k] = $status_data;
            }
            if($rmv !== -1){
                unset($data['tabs'][$rmv]);
            }
        }
        //print_r($data['tabs']);exit;
        return $data;
    }

    public function getAttributeCode() {
        $attrcode = Mage::getStoreConfig('tribe_brand/edit/attributecode');
        $finalattrcode = $attrcode ? $attrcode : 'manufacturer';
        return $finalattrcode;
    }

    function getManufacturerList() {
        $storeId_for_supplier_attr = 0;
        $config = Mage::getModel('eav/config');
        $supplierattr_code = $this->getAttributeCode();
        $attribute = $config->getAttribute(Mage_Catalog_Model_Product::ENTITY, $supplierattr_code);
        //$values = $attribute->setStoreId($storeId)->getSource()->getAllOptions(); //here is another method to get the attribute value
        $supplier_options = Mage::getResourceModel('eav/entity_attribute_option_collection');
        $Supplier_values = $supplier_options->setAttributeFilter($attribute->getId())->setStoreFilter($storeId_for_supplier_attr)->toOptionArray();
        $supplier_opt_value = array_column($Supplier_values, 'label', 'value');
        return $supplier_opt_value;
    }


    public function getAdminUserId($username, $email) {
        $user = Mage::getModel('admin/user')->getCollection()
                ->addFieldToSelect('user_id')
                ->addFieldToFilter(
                array('username', 'email'), array(
            array('eq' => $username),
            array('eq' => $email)
                )
        );
        $user_id = $user->getData()[0]['user_id'];
        return $user_id;
    }

    public function checkAdminUser($username, $email, $userid = null) {
        //Here let's check user exeists or not
        $user = Mage::getModel('admin/user')->getCollection()
                ->addFieldToSelect('user_id')
                ->addFieldToFilter(
                array('username', 'email'), array(
            array('eq' => $username),
            array('eq' => $email)
                )
        );
        if ($userid) {
            $user->addFieldToFilter(array('user_id'), array(array('neq' => $userid)));
        }
        if (!$user->getData()) {
            return false;
        } else {
            return true;
        }
    }

    public function createAdminUser($data, $roleid, $userid = 0) {
        $userdata = array(
            'username' => $data['trcr_email'],
            'firstname' => $data['trcr_first_name'],
            'lastname' => $data['trcr_last_name'],
            'email' => $data['trcr_email'],
            'password' => $data['trcr_password'],
            'is_active' => ($data['trcr_status'] == 2) ? 1 : 0
        );
        if ($userid) {
            //Here to update seller information
            $id = $userid;
            $user = Mage::getModel('admin/user')->load($id);
            $user->addData($userdata)->save();
        } else {
            //Here to add new admin user
            $user = Mage::getModel('admin/user')->setData($userdata)->save();
            $id = $user->getUserId();
        }
        $user->setRoleIds(array($roleid))
                ->setRoleUserId($id)
                ->saveRelations();
    }

    public function getBrandRoleId() {
        $roleid = 0;
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $tableName = $resource->getTableName('admin_role');
        $query = "SELECT `role_id` FROM $tableName WHERE role_name = 'TribeBrand' LIMIT 1";
        $res = $readConnection->query($query);
        while ($row = $res->fetch()) {
            $roleid = $row['role_id'];
        }
        return $roleid;
    }

    public function getBrandOptionId(){
        return $this->trcr_attribute_option;
    }
    
    public function getBrandSellerOptionId(){
        return $this->trcr_store_list;
    }

    public function getBrandSellerIdFromadminUser() {
        if (is_null($this->brand_id)) {
            $user = Mage::getSingleton('admin/session');
            $username = $user->getUser()->getUsername();
            $userEmail = $user->getUser()->getEmail();
            $brand = Mage::getModel('arccore/arc')->getCollection()
                            ->setStoreId(Mage::app()->getStore()->getId())
                            ->addAttributeToFilter('attribute_set_id', $this->getAttributeSetId())
                            ->addAttributeToFilter('trcr_email', $userEmail)->getFirstItem()->load();
            if (!$brand || !$brand->getId() || $brand->getTrcrStatus() != 2) {
                $this->brand_id = false;
            }
            $this->brand_id = $brand->getTrcrStoreList();
            $this->trcr_attribute_option = $brand->getTrcrAttributeOption();
            $this->trcr_store_list = $brand->getTrcrStoreList();
        }
        return $this->brand_id;
    }
     public function getSellerFromOptionId($option_id) {
        $seller = Mage::getModel('tribeseller/seller')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->loadByAttribute('seller_partner', $option_id);
        if (!$seller || !$seller->getId() || $seller->getIsActive() != 2) {
            return false;
        }
        return $seller;
    }
    
    public function getsellerid($_product){//fromchannelid
    if($_product->getTypeId()=='configurable'){
        $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$_product);
     foreach($childProducts as $k=>$v){
         $entity_id[] = $v->getData('entity_id');
     }
     $simple_product_id = $entity_id[0];
     $simple_product = Mage::getModel('catalog/product')->load($simple_product_id);
     $channel_id = $simple_product->getBridgePricenqtyChannelId();
    }else{
      $channel_id = $_product->getBridgePricenqtyChannelId();    
    }
      $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        if($channel_id){
        $query = "SELECT channel_supplier FROM bridge_incomingchannels WHERE id=$channel_id;";
        $result = $read->fetchRow($query);
        $partner_id = $result['channel_supplier'];
        $seller = $this->getSellerFromOptionId($partner_id);
        if($seller)
        return $seller->getId();
        else
        return 0;
        }else{
            return 0;
        }
 }

}