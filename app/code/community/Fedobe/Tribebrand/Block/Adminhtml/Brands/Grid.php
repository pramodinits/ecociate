<?php

/**
 * Manage Customer Attribute grid block
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribebrand
 * @author      Fedobe Magento Team
 * 
 */


class Fedobe_Tribebrand_Block_Adminhtml_Brands_Grid extends Fedobe_Arccore_Block_Adminhtml_Arccore_Grid {

    public function __construct() {
        parent::__construct();
        $this->setAttributeSetId(Mage::helper('tribebrand')->getAttributeSetId());

    }

    /**
     * Prepare customer attributes grid collection object
     *
     * @return Fedobe_Tribebrand_Block_Adminhtml_Tribebrand_Grid
     */
    protected function _prepareCollection() {
        $collection = Mage::getModel('arccore/arc')->getCollection()->addAttributeToSelect('*');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('entity_id', array(
            'header' => Mage::helper('tribebrand')->__('Brand ID'),
            'width' => '50px',
            'type' => 'number',
            'index' => 'entity_id',
        ));
        
        $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
            ->setEntityTypeFilter(Mage::helper('arccore')->getEntityTypeId('arccore'))
            ->load()
            ->toOptionHash();
       
        $this->addColumn('trcr_name', array(
            'header' => Mage::helper('tribebrand')->__('Brand Name'),
            'index' => 'trcr_name',
        ));
       
        $this->addColumn('created_at', array(
            'header' => Mage::helper('tribebrand')->__('Created At'),
            'index' => 'created_at',
        ));

      
 $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                        ->load('Tribe Brand', 'attribute_set_name')
                        ->getAttributeSetId();
        $this->addColumn('action', array(
            'header' => Mage::helper('tribebrand')->__('Action'),
            'width' => '50px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('tribebrand')->__('Edit'),
                    'url' => array(
                        'base' => '*/*/edit/set/'.$attributeSetId,
                        'params' => array('store' => $this->getRequest()->getParam('store'))
                    ),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
        ));


        return parent::_prepareColumns();
    }

}