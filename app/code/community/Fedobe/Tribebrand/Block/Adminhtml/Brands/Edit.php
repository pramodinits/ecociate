<?php

/**
 * Customer attribute edit block
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribebrand
 * @author      Fedobe Magento Team
 */
class Fedobe_Tribebrand_Block_Adminhtml_Brands_Edit extends Fedobe_Arccore_Block_Adminhtml_Arc_Edit {

    public function __construct() {
        parent::__construct();
        $this->_controller = 'tribe_brand';
        $this->_blockGroup = 'tribebrand';
    }
}
