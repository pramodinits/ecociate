<?php

class Fedobe_Tribebrand_Block_Adminhtml_Menu extends Mage_Adminhtml_Block_Page_Menu {

    public function getMenuArray() {
        //Load standard menu
        $parentArr = parent::getMenuArray();
        $brandId = Mage::helper('tribecore')->getProfileIdFromadminUser();
        unset($parentArr['fedobe']['children']['tribebrand']);
            $parentArr['profile'] = array(
                'label' => 'Profile',
                'active' => false,
                'sort_order' => 0,
                'url' => Mage::helper('adminhtml')->getUrl('adminhtml/tribe_brand/edit/', array('id' => $brandId)),
                'level' => 0,
                'last' => true,
                'children' => array()
            );
        return $parentArr;
    }

}