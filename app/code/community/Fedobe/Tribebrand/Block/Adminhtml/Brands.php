<?php
/**
 * Grid container file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribebrand
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Tribebrand_Block_Adminhtml_Brands extends Mage_Adminhtml_Block_Widget_Grid_Container
{       
  public function __construct()
  {
    /*both these variables tell magento the location of our Grid.php(grid block) file.
     * $this->_blockGroup.'/' . $this->_controller . '_grid'
     * i.e clarion_Customerattribute/adminhtml_customerattribute_grid
     * $_blockGroup - is your module's name.
     * $_controller - is the path to your grid block. 
     */
    
    $this->_controller = 'adminhtml_brands';
    $this->_blockGroup = 'tribebrand';
    
    $this->_headerText = Mage::helper('tribebrand')->__('Manage Brands');
    $this->_addButtonLabel = Mage::helper('tribebrand')->__('Add New Brand');
    parent::__construct();
    $this->_removeButton('add');
  }
}