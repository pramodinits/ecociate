<?php

class Fedobe_Tribebrand_Block_Product_List extends Mage_Catalog_Block_Product_List {

   
    protected function _getProductCollection($info_pass = array(),$is_count=0) {
        if (is_null($this->_productCollection)) {
            $layer = $this->getLayer();
            /* @var $layer Mage_Catalog_Model_Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }
            // if this is a product view page
            if (Mage::registry('product')) {
                // get collection of categories this product is associated with
                $categories = Mage::registry('product')->getCategoryCollection()
                        ->setPage(1, 1)
                        ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }
            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                    $this->addModelTags($category);
                }
            }
            $collection = $layer->getProductCollection();
            if(!$is_count){
            $collection->setPageSize($this->get_prod_count())
            ->addAttributeToSort($this->get_order(), $this->get_order_dir())
            ->setCurPage($this->get_cur_page());
           }
            $this->_productCollection = $collection;
            //echo "<pre>";echo implode(",",array_column($this->_productCollection->getData(),'entity_id'));exit;
            $attribute = Mage::helper('tribebrand')->getAttributeCode(); //print_r($attribute);exit;
            if($info_pass){
                $info = $info_pass;
            }else{
                $info = Mage::registry('tribeprofileinfo');    
            }
            if(!empty($info)){
                $this->_productCollection->addAttributeToFilter($attribute,array('eq'=>$info['trcr_attribute_option']));
            }
            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());
            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
        }//echo $this->_productCollection->getSelect()->__toString();exit;
        return $this->_productCollection;
    }
    
    public function getProductCount($info){
        $collection = $this->_getProductCollection($info,1);
        return $collection->getSize();
    }
    public function get_prod_count(){
		//unset any saved limits
	    Mage::getSingleton('catalog/session')->unsLimitPage();
	    return (isset($_REQUEST['limit'])) ? intval($_REQUEST['limit']) : 24;
	}// get_prod_count

   public function get_cur_page(){
		return (isset($_REQUEST['p'])) ? intval($_REQUEST['p']) : 1;
	}// get_cur_page
    public function get_order(){
		return (isset($_REQUEST['order'])) ? ($_REQUEST['order']) : 'position';
	}// get_order
    public function get_order_dir(){
		return (isset($_REQUEST['dir'])) ? ($_REQUEST['dir']) : 'desc';
	}// get_direction
	 public function getAddToCartUrl($product, $additional = array()) {
           /* $supplierscode = (Mage::getStoreConfig("bridge/bridge_supplier_settings/bridge_supplier_attrcode")) ? Mage::getStoreConfig("bridge/bridge_supplier_settings/bridge_supplier_attrcode") : "suppliers";
            $default_seller_id = $product->getData($supplierscode) ? Mage::helper('tribeseller')->getSellerFromOptionId($product->getData($supplierscode))->getId() : Mage::helper('tribeseller')->getDefaultSeller();
            if ($default_seller_id)
                $additional['product_seller'] = $default_seller_id;*/
              $additional['product_seller'] = Mage::helper('tribebrand')->getsellerid($product);
        return parent::getAddToCartUrl($product, $additional);
    }
}