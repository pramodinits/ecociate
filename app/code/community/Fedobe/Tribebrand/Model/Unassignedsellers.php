<?php

class Fedobe_Tribebrand_Model_Unassignedsellers extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    public function getAllOptions($withEmpty = false) {
        $allOptions = Mage::getModel('tribeseller/seller_attribute_sellers')->getAllOptions();
        
        $allactivesellers = Mage::getModel('tribeseller/seller')->getCollection()
                         ->addAttributeToSelect('*')
                         ->addAttributeToFilter('seller_partner',array('notnull'=>true))->getData();
        $allactivesellers_arr =  array_column($allactivesellers,'seller_partner');
        
        $attrset_id = Mage::app()->getRequest()->getParam('set');
        $id = Mage::app()->getRequest()->getParam('id');
        
        $assignedbrands = Mage::getModel('arccore/arc')->getCollection()
                            ->addAttributeToSelect('*')
                            ->addAttributeToFilter('attribute_set_id',$attrset_id)
                            ->addAttributeToFilter('trcr_store_list',array('notnull'=>true));
                           
        if($id)
        $assignedbrands = $assignedbrands->addAttributeToFilter('entity_id',array('neq'=>$id));
        $assignedbrands = $assignedbrands->getData();
        $excludeoptions = array_column($assignedbrands,'trcr_store_list'); 
        $finaloptions[] = array('value' => '', 'label' => "Select");
        
        foreach ($allOptions as $k => $opt) {
            if (!in_array($opt['value'], $excludeoptions) && in_array($opt['value'], $allactivesellers_arr)) {
                $finaloptions[] = $opt;
            }
        }
        return $finaloptions;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray() {
        $_options = array();
        foreach ($this->getAllOptions() as $option) {
            $_options[$option["value"]] = $option["label"];
        }
        return $_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string
     */
    public function getOptionText($value) {
        $options = $this->getAllOptions();
        foreach ($options as $option) {
            if ($option["value"] == $value) {
                return $option["label"];
            }
        }
        return false;
    }

    /**
     * Retrieve Column(s) for Flat
     *
     * @return array
     */
    public function getFlatColums() {
        $columns = array();
        $columns[$this->getAttribute()->getAttributeCode()] = array(
            "type" => "tinyint(1)",
            "unsigned" => false,
            "is_null" => true,
            "default" => null,
            "extra" => null
        );

        return $columns;
    }

    /**
     * Retrieve Indexes(s) for Flat
     *
     * @return array
     */
    public function getFlatIndexes() {
        $indexes = array();

        $index = "IDX_" . strtoupper($this->getAttribute()->getAttributeCode());
        $indexes[$index] = array(
            "type" => "index",
            "fields" => array($this->getAttribute()->getAttributeCode())
        );

        return $indexes;
    }

    /**
     * Retrieve Select For Flat Attribute update
     *
     * @param int $store
     * @return Varien_Db_Select|null
     */
    public function getFlatUpdateSelect($store) {
        return Mage::getResourceModel("eav/entity_attribute")
                        ->getFlatUpdateSelect($this->getAttribute(), $store);
    }

}