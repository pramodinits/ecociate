<?php

class Fedobe_Tribebrand_Model_Observer {
    public function addButtonTestng($observer){
    $container = $observer->getBlock();
    $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                        ->load('Tribe Brand', 'attribute_set_name')
                        ->getAttributeSetId();
   // echo $attributeSetId;exit;
    if(null !== $container && $container->getType() == 'tribebrand/adminhtml_brands') {
        $data_2 = array(
            'label'     => 'Add Brand',
            'class'=>'add',
            'onclick'   => 'setLocation(\' '  . Mage::helper('adminhtml')->getUrl('adminhtml/tribe_brand/new/set/'.$attributeSetId, array('_secure' => true)) . '\')',
        );
        $container->addButton('addbrand', $data_2);
      /* $data_3 = array(
            'label'     => 'Add Profile',
            'class'=>'add',
            'onclick'   => 'setLocation(\' '  . Mage::helper('adminhtml')->getUrl('adminhtml/profiles/new/set/'.$attributeSetId, array('_secure' => true)) . '\')',
        );
        $container->addButton('addprofile', $data_3);*/
    }
    return $this;
}

}
