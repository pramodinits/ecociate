<?php

class Fedobe_Tribebrand_Model_Source_Posttype extends Fedobe_Tribecore_Model_Source_Posttype {
    public function getAllOptions() {
        $_options = parent::getAllOptions();
        $_options[] =  array('label' =>  Mage::helper('tribebrand')->__('Profile'), 'value' => 'profile');
        $_options[] =  array('label' =>  Mage::helper('tribebrand')->__('Products'), 'value' => 'products');
        return $_options;
    }
}