<?php
class Fedobe_Tribebrand_Model_Brand extends Fedobe_Arccore_Model_Arc {

    const NOROUTE_PAGE_ID = 'no-route';

    /**
     * Page's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    const CACHE_TAG = 'tribebrand_page';

    protected $_cacheTag = 'tribebrand_page';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'tribebrand_page';

    /**
     * Initialize resource model
     *
     */
    protected function _construct() {
        parent::_construct();
    }

    /**
     * Load object data
     *
     * @param mixed $id
     * @param string $field
     * @return Mage_Cms_Model_Page
     */
    public function load($id, $field = null) {
        if (is_null($id)) {
            return $this->noRoutePage();
        }
        return parent::load($id, $field);
    }

    /**
     * Load No-Route Page
     *
     * @return Mage_Cms_Model_Page
     */
    public function noRoutePage() {
        return $this->load(self::NOROUTE_PAGE_ID, $this->getIdFieldName());
    }

    /**
     * Check if page identifier exist for specific store
     * return page id if page exists
     *
     * @param string $identifier
     * @param int $storeId
     * @return int
     */
    public function checkIdentifier($identifier, $storeId) {
        $key = Mage::helper('tribebrand')->extractUrlKey($identifier);
        $attributesetid = Mage::helper('tribebrand')->getAttributeSetId();
        $attributetofilter = Mage::helper('tribebrand')->getUrlkeyAttributeCode();
        $brandid = $this->getResourceCollection()
                ->setStoreId($storeId)
                ->addAttributeToFilter('attribute_set_id', $attributesetid)
                ->addAttributeToFilter($attributetofilter, array('eq' => $key))
                ->addAttributeToFilter('trcr_status', array('eq' => 2))
                ->getFirstItem()
                ->getId();
        return $brandid;
    }

}