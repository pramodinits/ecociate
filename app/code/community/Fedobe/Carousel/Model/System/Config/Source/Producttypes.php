<?php

class Fedobe_Carousel_Model_System_Config_Source_Producttypes {

   public function toOptionArray() {
        return array(
//            array('value' => 'related', 'label' => 'Related Product'),
//            array('value' => 'recommended', 'label' => 'Recommended'),
//            array('value' => 'same-category', 'label' => 'Items from same category'),
        	array('value' => '', 'label' => 'Select'),            
            array('value' => 'allproductt', 'label' => 'All Product'),
            array('value' => 'featuredd', 'label' => 'Featured Product'),
            array('value' => 'best-sellerr', 'label' => 'Best Seller'),
            array('value' => 'recently-addedd', 'label' => 'Recently Added'),
            );
    }
 
}
