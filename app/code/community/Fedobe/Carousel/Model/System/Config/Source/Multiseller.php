<?php

/**
 * 
 * @category    CategoryCarousel
 * @package     Avatar_CategoryCarousel
 * @author        Fedobe Solution Pvt Ltd <support@fedobe.com>
 *
 */

  
class Fedobe_Carousel_Model_System_Config_Source_Multiseller extends Mage_Eav_Model_Entity_Attribute_Source_Abstract 
{
   public function getAllOptions($withEmpty = false) {
        $activesellers = Mage::getModel('tribeseller/seller')->getCollection()
                ->addAttributeToFilter('seller_status', 2);
        $sellers = array();
        if (!empty($activesellers)) {
            foreach ($activesellers as $k => $seller) {
                $sellers[] = array(
                    "label" => Mage::helper('tribeseller')->__($seller->getSellerStoreName())
                    , "value" => $seller->getSellerUserName()
                );
            }
        }
        return $sellers;
    } 

    public function toOptionArray() {
        $options = $this->getAllOptions();

        return $options;
    }
    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray() {
        $_options = array();
        foreach ($this->getAllOptions() as $option) {
            $_options[$option["value"]] = $option["label"];
        }
        return $_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string
     */
    public function getOptionText($value) {
        $options = $this->getAllOptions();
        foreach ($options as $option) {
            if ($option["value"] == $value) {
                return $option["label"];
            }
        }
        return false;
    }

    /**
     * Retrieve Column(s) for Flat
     *
     * @return array
     */
    public function getFlatColums() {
        $columns = array();
        $columns[$this->getAttribute()->getAttributeCode()] = array(
            "type" => "tinyint(1)",
            "unsigned" => false,
            "is_null" => true,
            "default" => null,
            "extra" => null
        );

        return $columns;
    }

    /**
     * Retrieve Indexes(s) for Flat
     *
     * @return array
     */
    public function getFlatIndexes() {
        $indexes = array();

        $index = "IDX_" . strtoupper($this->getAttribute()->getAttributeCode());
        $indexes[$index] = array(
            "type" => "index",
            "fields" => array($this->getAttribute()->getAttributeCode())
        );

        return $indexes;
    }

    /**
     * Retrieve Select For Flat Attribute update
     *
     * @param int $store
     * @return Varien_Db_Select|null
     */
    public function getFlatUpdateSelect($store) {
        return Mage::getResourceModel("eav/entity_attribute")
                        ->getFlatUpdateSelect($this->getAttribute(), $store);
    }
}
