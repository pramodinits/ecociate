<?php

class Fedobe_Bridgeapi_Adminhtml_BridgeapiController extends Mage_Adminhtml_Controller_Action {

    public function _initAction() {
        $this->loadLayout()->_setActiveMenu('fedobe/bridge/outgoing');
        $this->_title(Mage::helper('bridge')->__('Fedobe Extension'));
    }

    public function indexAction() {
        $this->_initAction();
        $this->_title(Mage::helper('bridge')->__('Manage Outgoing Channels'));
//        $this->loadLayout();
        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction() {
        $this->_initAction();
        Mage::getSingleton('core/session')->unsetData('channel_condition_skus');
        $id = $this->getRequest()->getParam('id');
        $otgchannel = Mage::getModel('bridge/outgoingchannels');
        $consumer_model = Mage::getModel('oauth/consumer');
        $session_security_salt = $id ? "{$id}_outgoing_security_salt" : "outgoing_security_salt";
        Mage::getSingleton('core/session')->unsetData("$session_security_salt");
        if (@$id) {
            $otgchannel->load($id);

            if (!$otgchannel->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('bridge')->__('This Channel  no longer exists.')
                );
                $this->_redirect('*/*');
                return;
            }

            if ($otgchannel->getConsumerId()) {
                $consumer_model->load($otgchannel->getConsumerId());
                $otgchannel->setKey($consumer_model->getKey());
                $otgchannel->setSecret($consumer_model->getSecret());
                $otgchannel->setName($consumer_model->getName());
            }

            if ($otgchannel->getProductStatus()) {
                $otgchannel->setBridge(array('product_status' => $otgchannel->getProductStatus()));
            }
//            $model->setOutgoingchannelId($otgchannel->getId());
//            if (!$model->getId()) {
//                Mage::getSingleton('adminhtml/session')->addError(
//                        Mage::helper('bridge')->__('This Channel no longer exists.')
//                );
//                $this->_redirect('*/*');
//                return;
//            }
            //set the security salt for this outgoing channel if paid module is installed
            if ($consumer_model->getId() && !$otgchannel->getSalt()) {
                Mage::getSingleton('core/session')->unsetData($session_security_salt);
                if (Mage::helper('core')->isModuleEnabled('Fedobe_Securitycheck')) {
                    $security_salt = Mage::getModel('securitycheck/salt')->loadByOutgoingChannel($otgchannel->getId())->getSalt();
                    Mage::getSingleton('core/session')->setData("$session_security_salt", $security_salt);
                }
            }
        }
        if (Mage::getmodel('bridge/outgoingchannels')->isAdminstrator(Mage::getSingleton('admin/Session')->getUser()->getUserId())) {
            if ($otgchannel->getAdminUserId()) {
                $user_data = Mage::getModel("admin/user")->load($otgchannel->getAdminUserId());
                $otgchannel->setUsername($user_data->getUsername());
                $otgchannel->setFirstname($user_data->getFirstname());
                $otgchannel->setLastname($user_data->getLastname());
                $otgchannel->setEmail($user_data->getEmail());
            }
        }

        $this->_title($otgchannel->getId() ? $this->__('Edit Outgoingchannel') : $this->__('New Outgoingchannel'));
// set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getChannelData(true);
        if (!empty($data)) {
            $otgchannel->setData($data);
        }
        if ($otgchannel->getId()) {
            Mage::getSingleton('adminhtml/session')->setData("{$id}_listed_sku", explode(',', $otgchannel->getSkuList()));
        }

        if (!@$otgchannel['secret']) {
            $formData = $this->_getFormData();
            if ($formData) {
                $this->_setFormData($formData);
                $otgchannel->setKey($formData['key']);
                $otgchannel->setSecret($formData['secret']);
            } else {
                /** @var $helper Mage_Oauth_Helper_Data */
                $helper = Mage::helper('oauth');
                $otgchannel->setKey($helper->generateConsumerKey());
                $otgchannel->setSecret($helper->generateConsumerSecret());
                $this->_setFormData($otgchannel->getData());
            }
        }
//        Mage::register('current_consumer', $model);
        $default_settings = Mage::helper('bridge')->getDefaultSettings();
        Mage::register('default_settings', $default_settings);
        Mage::register('channel_condition', $otgchannel);
        $this->renderLayout();
    }

    public function conditionsGridAction() {
        $page = Mage::getModel('bridge/outgoingchannels')->load($this->getRequest()->getParam('id'));
        $encodedIds = $page->getSkuList();
        $encodedIds = explode(',', $encodedIds);
        echo $this->getLayout()->createBlock('fedobe_bridge_block_adminhtml_outgoingchannels_edit_tab_conditions_grid')->setSkuList($encodedIds)->toHtml();
    }

    public function saveAction() {
        $id = $this->getRequest()->getParam('id');
        if (!$this->_validateFormKey()) {
            if ($id) {
                $this->_redirect('*/*/edit', array('id' => $id));
            } else {
                $this->_redirect('*/*/new', array('id' => $id));
            }
            return;
        }

        if ($this->getRequest()->getPost()) {
            //save outgoing channel mapping data
            $session_security_salt = $id ? "{$id}_outgoing_security_salt" : "outgoing_security_salt";
            $data = $this->getRequest()->getPost();
            $otgchannel = Mage::getModel('bridge/outgoingchannels');
            $otgchannel->load($id);
            $defaul_settings = Mage::helper('bridge')->getDefaultSettings();
            $allow_condition = false;
            $is_retailer = Mage::getmodel('bridge/outgoingchannels')->isRetailer(Mage::getSingleton('admin/Session')->getUser()->getUserId());
            $is_adminstrator = Mage::getmodel('bridge/outgoingchannels')->isAdminstrator(Mage::getSingleton('admin/Session')->getUser()->getUserId());
            if ((@$data['allow_condition'] == "" && $defaul_settings['ogc_allow_conditiontab'] == "allow_editing") || @$data['allow_condition'] == "allow_editing" || $is_adminstrator)
                $allow_condition = true;

            $data['name'] = @$data['consumer_name'];


            if (@$data['consumer_name']) {
                $consumer_model = Mage::getModel('oauth/consumer');
                $consumer_model->load($otgchannel->getConsumerId());
                //save the user data if loged in user is admin
                if ($is_adminstrator) {
                    //if user exist then update
                    $user_model = Mage::getModel('Admin/User')->load($this->getRequest()->getPost('admin_user_id'));
                    $user_data = array('username' => $data['username'], 'firstname' => $data['firstname'], 'lastname' => $data['lastname'], 'email' => $data['email'], 'user_id' => $user_model->getId());
                    if (isset($data['password']))
                        $user_data['password'] = $data['password'];
                    else if (isset($data['new_password']))
                        $user_data['new_password'] = $data['new_password'];
                    $user_data['password_confirmation'] = $data['password_confirmation'];
                    $user_model->setData($user_data);
                    /*
                     * Unsetting new password and password confirmation if they are blank
                     */
                    if ($user_model->hasNewPassword() && $user_model->getNewPassword() === '') {
                        $user_model->unsNewPassword();
                    }
                    if ($user_model->hasPasswordConfirmation() && $user_model->getPasswordConfirmation() === '') {
                        $user_model->unsPasswordConfirmation();
                    }

                    $user_validation = $user_model->validate();
                    if (is_array($user_validation)) {
                        Mage::getSingleton('adminhtml/session')->setChannelData($data);
                        foreach ($user_validation as $message) {
                            Mage::getSingleton('adminhtml/session')->addError($message);
                        }
                        $this->_redirect('*/*/edit', array('_current' => true));
                        return $this;
                    }
                    try {

                        //if user is not created before then create the user and assign bridge user role and  bridge api api role
                        if (!$user_model->getId()) {
                            $api2_role_id = Mage::getmodel('bridge/outgoingchannels')->getRestRoleId();
                            $user_model->setApi2Roles(array($api2_role_id));
                            $user_model->save();
                            $user_id = $user_model->getId();
                            $role_id = Mage::getModel('Admin/Role')->getCollection()->addFieldToFilter('role_name', 'Bridge User')->addFieldToSelect('role_id')->getFirstItem()->getData();
                            $user_model->setRoleIds($role_id)
                                    ->setRoleUserId($user_model->getUserId())
                                    ->saveRelations();
                        } else {
                            //otherwise update the user
                            $user_model->save();
                        }
                    } catch (Mage_Core_Exception $e) {

                        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                        Mage::getSingleton('adminhtml/session')->setChannelData($data);
                        $this->_redirect('*/*/edit', array('id' => $otgchannel->getUserId()));
                        return;
                    }
                    $data['admin_user_id'] = $user_model->getId();
                }
                //save
                //save api inforamtion
                if (!$id) {
                    $dataForm = $this->_getFormData();
                    if ($dataForm) {
                        $data['key'] = $dataForm['key'];
                        $data['secret'] = $dataForm['secret'];
                    } else {
                        // If an admin was started create a new consumer and at this moment he has been edited an existing
                        // consumer, we save the new consumer with a new key-secret pair
                        /** @var $helper Mage_Oauth_Helper_Data */
                        $helper = Mage::helper('oauth');

                        $data['key'] = $helper->generateConsumerKey();
                        $data['secret'] = $helper->generateConsumerSecret();
                    }
                }
                //save oauth consumer data
                $consumer_model->addData($data);
                $consumer_model->save();
                $data['consumer_id'] = $consumer_model->getId();
            }

////Here let add the rules data
            $data['relative_product'] = @array_sum($data['relative_product']);
            $data['others'] = isset($data['others']) ? serialize($data['others']) : '';
            $product_ids = '';
            if ($allow_condition) {
                if ($selectedids = $this->getRequest()->getPost('product_id')) {
                    $product_ids = Mage::helper('adminhtml/js')->decodeGridSerializedInput($selectedids);
                    $product_ids = implode(',', array_keys($product_ids));
                }

                $data['sku_list'] = $product_ids;
                $condition_rules = @$data['rule'];
                $arr = array();
                foreach ($condition_rules['conditions'] as $id => $cdata) {
                    $path = explode('--', $id);
                    $node = & $arr;
                    for ($i = 0, $l = sizeof($path); $i < $l; $i++) {
                        if (!isset($node['conditions'][$path[$i]])) {
                            $node['conditions'][$path[$i]] = array();
                        }
                        $node = & $node['conditions'][$path[$i]];
                    }
                    foreach ($cdata as $k => $v) {
//check for gender attribute array issue
                        if (is_array($v)) {
                            $v = implode(',', $v);
                        }
                        $node[$k] = $v;
                    }
                }
                $data['conditions_serialized'] = serialize($arr['conditions'][1]);
                $data['product_status'] = @$data['bridge']['product_status'];
            } else {
                unset($data['sku_list']);
            }
            unset($data['rule']);

            try {
                if (!@$data['admin_user_id'])
                    $data['admin_user_id'] = Mage::getSingleton('admin/Session')->getUser()->getUserId();
                //save security salt if paid module exist
                if (Mage::getSingleton('core/session')->getData($session_security_salt) && Mage::helper('core')->isModuleEnabled('Fedobe_Securitycheck')) {
                    $data['salt'] = "";
                }


//                if (@$data['admin_user_id']) {
//                    if (@$data['consumer_name']) {
//                        $otgchannel->loadByConsumerId($oauthmodel->getId());
//                        $data['consumer_id'] = $oauthmodel->getId();
//                    } else {
//                        $otgchannel->load($id);
//                    }
//                    $data['id'] = $otgchannel->getId();
//                } else {
//                    $data['id'] = $this->getRequest()->getParam('id');
//                }
                //if retiler logged in and creating a new channel then set the dafaultsettings
                $old_skulist = ($otgchannel->getSkuList()) ? $otgchannel->getSkuList() : "";
                $old_filterby = ($otgchannel->getFilterBy()) ? $otgchannel->getFilterBy() : "";
                if ($is_retailer && !$otgchannel->getId()) {
                    $data['allow_condition'] = $defaul_settings['ogc_allow_conditiontab'];
                    $data['product_info'] = $defaul_settings['ogc_sync_pim'];
                    $data['relative_product'][] = $defaul_settings['ogc_sync_related'] ? 1 : 0;
                    $data['relative_product'][] = $defaul_settings['ogc_sync_upsell'] ? 2 : 0;
                    $data['relative_product'][] = $defaul_settings['ogc_sync_cross_sell'] ? 4 : 0;
                    $data['content_source'] = $defaul_settings['ogc_content_source'];
                    $data['create_new'] = $defaul_settings['ogc_create_new'];
                    $data['cond_apply_on_relative_products'] = $defaul_settings['ogc_apply_cond_on_related_product'];
                    $data['filter_by'] = $defaul_settings['ogc_filter_by'];
                    $data['product_pricenquantity'] = $defaul_settings['ogc_sync_pricenquantity'];
                    $data['sync_order'] = $defaul_settings['ogc_sync_order'];
                    $data['price_source'] = $defaul_settings['ogc_price_source'];
                    $data['relative_product'] = array_sum($data['relative_product']);
                    $data['multiprice_rule'] = $data['ogc_multiprice_rule'];
                    $data['update_exist'] = $data['ogc_update_exist'];
                }

                $otgchannel->setData($data);
                $otgchannel->save();

                //update edi cron table for update convert price

                if (!$data['id']) {
                    $edicrondata['channel_id'] = $otgchannel->getId();
                    $edicrondata['channel_type'] = "outgoing"; //$data['channel_type'];
                    $edicrondata['status'] = $data['channel_status'];
                    $cronmodel = Mage::getModel('bridge/bridgeapi_edichannelcron')->setData($edicrondata)->save();
                } else {
                    $edicrondata = array("status" => $data['channel_status']);
                    if (($data['filter_by'] == 'condition' && unserialize($prev_condition) !== $arr['conditions'][1]) || ($old_filterby != $data['filter_by']) || ($data['filter_by'] == 'sku' && array_filter(explode(',', $data['sku_list'])) != array_filter(explode(',', $old_skulist)))) {
                        $edicrondata['condition_changed'] = 1;
                    }
                    $resource = Mage::getSingleton('core/resource');
                    $write = $resource->getConnection('core_write');
                    $where = "channel_id = {$otgchannel->getId()} AND channel_type ='outgoing'";
                    $write->update("bridge_edi_channelcron", $edicrondata, $where);
                }
                //End of Saving channel information to bridge_edi_channelcron table   
                //save security salt if paid module exist
                if (@$data['consumer_name']) {
                    if (Mage::helper('core')->isModuleEnabled('Fedobe_Securitycheck')) {
                        $security_salt_model = Mage::getModel('securitycheck/salt')->loadByOutgoingChannel($otgchannel->getId());
                        if (Mage::getSingleton('core/session')->getData($session_security_salt)) {
                            $security_salt_model->addData(array('id' => $security_salt_model->getId(), 'salt' => Mage::getSingleton('core/session')->getData($session_security_salt), 'outgoingchannel_id' => $otgchannel->getId(), 'updated' => date('Y-m-d H:i:s')));
                            $security_salt_model->save();
                        } else {
                            $security_salt_model->delete();
                        }
                    }
                }
                //end
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('bridge')->__('The Channel has been saved.')
                );
                Mage::getSingleton('adminhtml/session')->setChannelData(false);
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->setChannelData($data);
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/');
                return;
            }
        }
// The following line decides if it is a "save" or "save and continue"
        if ($this->getRequest()->getParam('back')) {
            $this->_redirect('*/*/edit', array('id' => $otgchannel->getId()));
        } else {
            $this->_redirect('*/*/');
        }
    }

    public function generateAction() {
        if (!$this->getRequest()->isAjax()) {
            $this->_forward('noRoute');
            return;
        }
        if ($this->getRequest()->getPost()) {
            $data = $this->getRequest()->getPost();
//Here let add the rules data
            $condition_rules = $data['rule'];
            foreach ($condition_rules['conditions'] as $id => $cdata) {
                $path = explode('--', $id);
                $node = & $arr;
                for ($i = 0, $l = sizeof($path); $i < $l; $i++) {
                    if (!isset($node['conditions'][$path[$i]])) {
                        $node['conditions'][$path[$i]] = array();
                    }
                    $node = & $node['conditions'][$path[$i]];
                }
                foreach ($cdata as $k => $v) {
//check for gender attribute array issue
                    if (is_array($v)) {
                        $v = implode(',', $v);
                    }
                    $node[$k] = $v;
                }
            }
            $conditions_serialized = serialize($arr['conditions'][1]);
        }
        if ($this->getRequest()->getParam('id'))
            $skus = Mage::getmodel('bridge/rule')->getMatchingProductIds($conditions_serialized);
        else {
            $otgchannel = Mage::getModel('bridge/outgoingchannels');
            $otgchannel->setConditionsSerialized($conditions_serialized);
            Mage::register('channel_condition', $otgchannel);
            $skus = Mage::getmodel('bridge/rule')->getMatchingProductIds();
        }
        $product_status = @$data['bridge']['product_status'];
        if ($product_status) {
            $skus = Mage::getmodel('bridge/outgoingchannels')->getFilteredSkus($skus, $product_status);
        }
        Mage::getSingleton('core/session')->setData('channel_condition_skus', $skus);
    }

//    public function saveSkuAction() {
//        if ($id = $this->getRequest()->getParam('id')) {
//            try {
//                $channel_id = Mage::getModel('bridge/outgoingchannels')->loadByConsumerId($id)->getId();
//                $model = Mage::getModel('bridge/outgoingchannels');
//                $model->setId($channel_id);
//                $model->setSkuList(implode(',', $this->getRequest()->getPost('ids')));
//                $model->save();
//                Mage::getSingleton('adminhtml/session')->addSuccess(
//                        Mage::helper('bridge')->__('The sku has been saved.'));
//                $this->_redirect('*/*/');
//                return;
//            } catch (Mage_Core_Exception $e) {
//                $this->_getSession()->addError($e->getMessage());
//            } catch (Exception $e) {
//                $this->_getSession()->addError(
//                        Mage::helper('bridge')->__('An error occurred while saving the sku. Please review the log and try again.'));
//                Mage::logException($e);
//                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
//            }
//        }
//        Mage::getSingleton('adminhtml/session')->addError(
//                Mage::helper('bridge')->__('Sorry you have not added the channel'));
//        return;
//    }

    /**
     * Get form data
     *
     * @return array
     */
    protected function _getFormData() {
        return $this->_getSession()->getData('consumer_data', true);
    }

    /**
     * Set form data
     *
     * @param $data
     * @return Mage_Oauth_Adminhtml_Oauth_ConsumerController
     */
    protected function _setFormData($data) {
        $this->_getSession()->setData('consumer_data', $data);
        return $this;
    }

    /**
     * return user information by ajax call
     */
    public function userinfoAction() {
        $user_id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('admin/user')->load($user_id)->getData();
        print_r(json_encode($model));
        exit;
    }

    /**
     * check url is exist?
     */
    public function urlunique_validationAction() {
        $exist = Mage::getmodel('bridge/outgoingchannels')->checkUniqueUrl($this->getRequest()->getParam('id'), $this->getRequest()->getParam('url'));
        echo $exist;
        exit;
    }

    /**
     * check unique user
     */
    public function uniqueuser_validationAction() {
        $user_model = Mage::getModel('Admin/User');
        $user_model->setData($this->getRequest()->getParams());
        echo $user_model->userExists();
        exit;
    }

    public function ajax_addruleAction() {
        if ($this->getRequest()->getPost()) {
            $serdata = $this->getRequest()->getPost();
            parse_str($serdata['rule'], $unserdata);
            $data = $unserdata;
            $data['channel_id'] = $serdata['channel_id'];
            $data['conditions_html'] = $serdata['conditions_html'];
            unset($data['rule']);
            $arr = array();
            $unserdata['rule']['actions'][1] = $unserdata['rule']['conditions'][1];
            foreach ($unserdata['rule']['actions'] as $id => $cdata) {
                $path = explode('--', $id);
                $node = & $arr;
                for ($i = 0, $l = sizeof($path); $i < $l; $i++) {
                    if (!isset($node['conditions'][$path[$i]])) {
                        $node['conditions'][$path[$i]] = array();
                    }
                    $node = & $node['conditions'][$path[$i]];
                }
                foreach ($cdata as $k => $v) {
//check for gender attribute array issue
                    if (is_array($v)) {
                        $v = implode(',', $v);
                    }
                    $node[$k] = $v;
                }
            }
            $data['conditions_serialized'] = serialize($arr['conditions'][1]);
            $data['created'] = date('Y-m-d H:i:s');
            $model = Mage::getModel('bridge/outgoingchannels_action');
            $model->setData($data);
            $model->save();
            echo $model->getId();
            exit;
        }
    }

    public function ajax_deleteruleAction() {
        if ($this->getRequest()->getPost('id')) {
            $model = Mage::getModel('bridge/outgoingchannels_action');
            $model->load($this->getRequest()->getPost('id'));
            $model->delete();
            exit;
        }
    }

    public function checkaddonAction() {
        $channel_type = $this->getRequest()->getParam('channel_type');
        if ($channel_type != 'magento') {
            $module = 'Fedobe_' . ucfirst($channel_type);
            $addon_enabled = Mage::helper('core')->isModuleEnabled($module);
            if ($addon_enabled) {
                $this->_redirect('adminhtml/' . $channel_type . '/edit/channel_type/' . $channel_type);
            } else {
                //Here to check the decsription of the corresponding sales description
                Mage::register('addon', $channel_type);
                $this->loadLayout();
                $this->renderLayout();
            }
        } else {
            $this->_redirect('*/*/edit/channel_type/magento');
        }
    }

    public function edicronAction() {

        Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $bridge_edi_crontable = $resource->getTableName('bridge/bridgeapi_edichannelcron');

//Get channelid from EdiCron table
        $sql = "SELECT * FROM " . $bridge_edi_crontable . " WHERE is_processed = 0 AND status = 1 limit 1";
        $edicron_detail = $readConnection->fetchAll($sql);
        if (count($edicron_detail)) {
            $channel_type = $edicron_detail[0]['channel_type'];
            $channel_id = $edicron_detail[0]['channel_id'];

//Get Outgoing channel details
            $otgchannel = Mage::getModel('bridge/outgoingchannels')->load($channel_id);

//get the product id using filter by condition or sku
            if (strcasecmp($otgchannel->getFilterBy(), 'sku') === 0) {
                $product_ids = explode(',', $otgchannel->getSkuList());
            } else {
                Mage::register('channel_condition', $otgchannel);
                $product_ids = Mage::getmodel('bridge/rule')->getMatchingProductIds();
                $product_status = $otgchannel->getProductStatus();
                if ($product_status)
                    $product_ids = Mage::getmodel('bridge/outgoingchannels')->getFilteredSkus($product_ids, $product_status);
            }


            //Get the product skulist 
            $products = Mage::getmodel('catalog/product')->getCollection()
                    ->addAttributeToFilter('entity_id', array('in' => $product_ids))
                    ->addAttributeToFilter('type_id', 'simple');
            //->addAttributeToSelect('sku')->getData();
            $count_Totalproduct = $products->getSize();

            if ($count_Totalproduct) {
                //loads the product details with all attribute
                $product_details = array();
                foreach ($products as $prod) {
                    $product_details[] = Mage::getModel('catalog/product')->load($prod->getId())->getData();
                }

                //Data to be insert in the table bridge_incomingchannel_product_info
                $product_insert_detail = array();
                $i = 0;
                foreach ($product_details as $product_insert) {
                    $product_insert_detail[$i]['sku'] = $product_insert['sku'];
                    $product_insert_detail[$i]['channel_id'] = $otgchannel->getId();
                    $product_insert_detail[$i]['quantity'] = $product_insert['stock_item']->getQty();
                    $product_insert_detail[$i]['price'] = $product_insert['price'];
                    $product_insert_detail[$i]['special_price'] = $product_insert['special_price'];
                    $product_insert_detail[$i]['channel_type'] = "outgoing";

                    //update price by price rule if exist
                    $base_price = $otgchannel->getPriceSource() != "ogc_price" ? $product_insert[$otgchannel->getPriceSource()] : ($product_insert['ogc_price'] ? $product_insert['ogc_price'] : ($product_insert['price'] ? $product_insert['price'] : $product_insert['msrp']));
                    $base_price = $base_price ? $base_price : 0;
                    $product_insert_detail[$i]['converted_price'] = Mage::helper('bridge')->addPriceRule($otgchannel, $product_insert['entity_id'], $base_price, 'ogc');
                    $i = $i + 1;
                }

                $productinfo_table_name = $resource->getTableName('bridge/incomingchannels_product');

                //Delete all record of the channel_id
                $channel_id = $otgchannel->getId();
                $where = "channel_id =$channel_id  AND channel_type ='outgoing' ";
                $writeConnection->delete($productinfo_table_name, $where);

                //Insert all record of the channel_id
                $writeConnection->insertMultiple($productinfo_table_name, $product_insert_detail);

                //Update the table bridge_edi_channelcron  after all products of channelid inseted to the table bridge_incomingchannel_product_info
                $edicrondata = array("is_processed" => 1);
                $where = "channel_id = $channel_id AND channel_type ='outgoing' ";
                $writeConnection->update("bridge_edi_channelcron", $edicrondata, $where);

                echo "Successfully saved the products";
            } else {
                echo "No products found";
                exit;
            }
        } else {
            echo "No Channel Found";
            exit;
        }
    }

    public function preDispatch() {
        $bypassaction = array('edicron');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }

}
