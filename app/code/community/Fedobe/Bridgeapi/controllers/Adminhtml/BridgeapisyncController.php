<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SyncingController
 *
 * @author Fedobe1
 */
class Fedobe_Bridgeapi_Adminhtml_BridgeapisyncController extends Mage_Adminhtml_Controller_Action {

    
    
    public function preDispatch() {
        $bypassaction = array('sync_inventory', 'testbridgapi');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }
    public function testbridgapiAction(){
        $channel_id = @$this->getRequest()->getParam('id');
        $incomingchannels = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('id', $channel_id)
                ->addFieldToFilter('authorized', 1)
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('product_info', 1)
                ->getFirstItem()
                ->getData();
        $this->getApiResult($incomingchannels);
    }

    /**
     * sync the category and attribute for a incoming channel
     */
    public function sync_category_n_attributeAction() {
        $api_id = $this->getRequest()->getParam('id');
        if ($api_id) {
            $incoming_channel = Mage::getmodel('bridge/incomingchannels')->load($api_id)->getData();
            if (@$incoming_channel['id'] && $this->getSalt($incoming_channel)) {
                $attr_cat_result = $this->getApiResult($incoming_channel, 'mappingdata');

                $unser_categories = unserialize($attr_cat_result['categories']);
                $unser_attributes = unserialize($attr_cat_result['attributes']);
                $options = $unser_attributes['options'];
                unset($unser_attributes['options']);
                $attr_cat_result['categories'] = serialize(array_unique(array_column($unser_categories, 'id')));
                $attr_cat_result['attributes'] = serialize($unser_attributes);
                foreach ($unser_attributes as $attribute) {
                    unset($attribute['name']);
                    $attribute_labels = array_map(function($atrlabel) {
                        if ($atrlabel['value'])
                            return array('id' => $atrlabel['attribute_id'], 'value' => $atrlabel['value'], 'store_id' => $atrlabel['store_id']);
                    }, $attribute);
                }
                $attribute_labels = array_filter($attribute_labels);
//            echo "<pre>";
//            print_r($attribute_labels);exit;
                Mage::getmodel('bridge/storeview_label')->deleteLabel($api_id);
                if (@$unser_categories)
                    Mage::getmodel('bridge/storeview_label')->insertMutipleRows($unser_categories, $api_id, 'category');
                if (@$attribute_labels)
                    Mage::getmodel('bridge/storeview_label')->insertMutipleRows($attribute_labels, $api_id, 'attribute');
                if (@$options)
                    Mage::getmodel('bridge/storeview_label')->insertMutipleRows($options, $api_id, 'options');
                $attr_cat_result['brand_id'] = $api_id;
                $att_cat_model = Mage::getmodel('bridge/attribute_categories');
                $att_cat_model->deleteByBrandId($api_id);
                $att_cat_model->setData($attr_cat_result);
                $att_cat_model->save();
            }
        }
        $this->_redirect('*/incomingchannels/edit', array('id' => $api_id, 'activeTab' => 'mapping_section'));
    }

    public function manualAction() {
        $this->sync_batched_skus_for_pimAction();
    }

    public function getSalt(&$incomingchannels) {
//        if (empty($incomingchannels['salt'])) {
////save security salt if paid module exist
//            if (Mage::helper('core')->isModuleEnabled('Fedobe_Securitycheck')) {
//                $securitysalt = Mage::getModel('securitycheck/salt')->loadByIncomingChannel($incomingchannels['id'])->getSalt();
//                if ($securitysalt) {
//                    $incomingchannels['salt'] = $securitysalt;
//                    $incomingchannels['salt_type'] = 'paid';
//                } else {
//                    return false;
//                }
//            } else {
//                return false;
//            }
//        } else {
            $incomingchannels['salt_type'] = 'free';
            $incomingchannels['salt'] = 'free';
//        }
        return true;
    }

    /**
     * sync matched product skus from the specified inc
     * oming channel
     * @param type $brand_id
     */
    public function sync_batched_skus_for_pimAction() {
        $channel_id = @$this->getRequest()->getParam('id');
        $incomingchannels = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('id', $channel_id)
                ->addFieldToFilter('authorized', 1)
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('product_info', 1)
                ->addFieldToFilter(array(
                    'create_new', //attribute_1 with key 0
                    'update_exist', //attribute_2 with key 1
                        ), array(
                    array('eq' => 1), //condition for attribute_1 with key 0
                    array('eq' => 1), //condition for attribute_2
                        )
                )
                ->getFirstItem()
                ->getData();
//get salt
         echo "<pre>";
            print_r($incomingchannels);exit;
        if (@$incomingchannels['id'] && $this->getSalt($incomingchannels)) {
            $matched_skus = $this->getApiResult($incomingchannels, 'batchsku');
            echo "<pre>";
            print_r($matched_skus);exit;
            Mage::getmodel('bridge/incomingchannels_skus')->insert($matched_skus, $incomingchannels);
        }
        $tab_name = ($this->getRequest()->getParam('activeTab')) ? $this->getRequest()->getParam('activeTab') : 'main_section';
        if (isset($_SERVER['argv']) && !empty($_SERVER['argv'])) {
            exit;
        } else {//echo $msg;exit;
            $this->_redirect('*/incomingchannels/edit', array('id' => $channel_id));
        }
    }

    public function sync_batch_ediAction() {
        $channel_id = @$this->getRequest()->getParam('id');
        $incomingchannels = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('id', $channel_id)
                ->addFieldToFilter('authorized', 1)
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('product_pricenquantity', 1)
                ->getFirstItem()
                ->getData();
        echo "<pre>";
        print_r($incomingchannels);
        exit;
//get salt
        if ($incomingchannels['id'] && $this->getSalt($incomingchannels)) {
            $edi_result = $this->getApiResult($incomingchannels, 'edi');
            echo "<pre>";
            print_r($edi_result);
            exit;

            if (count($matched_skus) == 1) {
                $this->trackError($incomingchannels['id'], array_values($matched_skus)[0]);
            }
            Mage::getmodel('bridge/incomingchannels_skus')->insert($matched_skus, $incomingchannels);
        }
    }

//if any error occuer in syncing then track the error in error log table
    public function trackError($inc_channel, $message) {
        $errorlog = Mage::getmodel('bridge/errorlog');
        $errorlog->setData(array('channel_id' => $inc_channel['id'], 'sku' => $inc_channel['sku'], 'message' => $message));
        $errorlog->save();
        $this->changeQueueProcessStatus($inc_channel['sku'], 1, NULL, $inc_channel['store']);
        $this->changeQueueRelationStatus($inc_channel['sku'], $inc_channel['id']);
        exit;
    }

    /**
     * sync inventory for a incoming channel
     */
    public function sync_inventoryAction() {
        $api_id = @$this->getRequest()->getParam('id') ? $this->getRequest()->getParam('id') : null;
//get a sku from matchinng sku list to sync it details
        $inc_channel = Mage::getmodel('bridge/incomingchannels_skus')->getsku($api_id);
//        echo "<pre>";
//        print_r($inc_channel);exit;


        if (1){//$inc_channel['id'] && $this->getSalt($inc_channel)) {
            //check if sku exist
            $sku_exist_in_catalog = Mage::getSingleton('catalog/product')->loadByAttribute('sku', $inc_channel['sku']);
            //check the channel setup is it allowing to create and update product
            if (($sku_exist_in_catalog && !$inc_channel['update_exist']) || (!$sku_exist_in_catalog && !$inc_channel['create_new'])) {
                $this->trackError($inc_channel, 'Now SKU is not matched with current channel condition');
            }
            $api_result = $this->getApiResult($inc_channel, 'pim');
            echo "<pre>";
            print_r($api_result);exit;
            if (count($api_result) == 1) {
                $this->trackError($inc_channel, $api_result[0]);
            }
            if ($inc_channel['relation']) {
                $inc_channel['sku'] = explode(',', $inc_channel['sku'])[0];
            }
            $product_data = unserialize($api_result['pim']);
            //current attribute set id and status  and product type of synced product
            $synced_product_data = array_keys($product_data['store']);
            $current_attributeset = $product_data['store'][$synced_product_data[0]]['attribute_set_id'];
            $current_status = $product_data['store'][$synced_product_data[0]]['status'];
            $product_type_id = $product_data['store'][$synced_product_data[0]]['type_id'];


            //if product exist and type id is differeent then track the error message
            if ($sku_exist_in_catalog && $sku_exist_in_catalog->getTypeId() != $product_type_id) {
                $this->trackError($inc_channel, "Changing the existing product type is not prossible");
            }
            $entityId = $sku_exist_in_catalog ? $sku_exist_in_catalog->getId() : '';
            $event = $entityId ? 'content-updated' : 'created';
            $status = $sku_exist_in_catalog ? $sku_exist_in_catalog->getStatus() : 2; //$this->getProductStatus($entityId, $current_status, $inc_channel);
//            echo "<pre>";
//            print_r($api_result);
//            exit;

            $store_attr = Mage::getModel('eav/entity_attribute')->getCollection()
                    ->addFieldToFilter('is_user_defined', 1)
                    ->addFieldToSelect(array('attribute_id', 'attribute_code'))
                    ->getData();
            $store_attr = array_column($store_attr, 'attribute_code', 'attribute_id');
//end
//get mapping data
            $mapping_data = unserialize($inc_channel['mapping']);
            $store_mapping = $mapping_data['store'];
//get attribute code for mapping with attribute id
            $attr_cat = Mage::getmodel('bridge/attribute_categories')->getCollection()->addFieldToFilter('brand_id', $inc_channel['id'])->addFieldToSelect(array('attributes', 'categories'))->getFirstItem()->getData();
            $attr_codes = array();
            $brand_mapping_attributes = array();
            $store_mapping_attributes = array();

            if (isset($attr_cat['attributes']))
                foreach (unserialize($attr_cat['attributes']) as $attr_Set) {
                    if (isset($attr_Set['name']))
                        unset($attr_Set['name']);
                    $attr_codes +=array_column($attr_Set, 'attribute_code', 'attribute_id');
                }

//            $updated_at = $product_data['store'][array_keys($product_data['store'])[0]]['updated_at'];
            if ($mapping_data) {
                if (@$mapping_data[$current_attributeset]) {
                    $brand_mapping_attributes = array_keys($mapping_data[$current_attributeset]);
                    $store_mapping_attributes = $mapping_data[$current_attributeset];
                    $bnd_atr_code = $this->get_values_for_keys($attr_codes, $brand_mapping_attributes);
                    $str_atr_code = $this->get_values_for_keys($store_attr, $store_mapping_attributes);
                    $mapping_data[$current_attributeset] = array_combine($bnd_atr_code, $str_atr_code);
                }
                if (isset($mapping_data['option_mapping']))
                    foreach ($mapping_data['option_mapping'] as $attr_id => $opt) {
                        unset($mapping_data['option_mapping'][$attr_id]);
                        $mapping_data['option_mapping'][$attr_codes[$attr_id]] = $opt;
                    }
//get all options for attributes for brand
                if (!empty($brand_mapping_attributes)) {
                    $brand_options_collection = Mage::getmodel('bridge/storeview_label')->getCollection();
                    $brand_option_data = $brand_options_collection->addFieldToSelect(array('attribute_id', 'entity_id', 'value'))
                            ->addFieldToFilter('brand_id', $inc_channel['id'])
                            ->addFieldToFilter('type', array('eq' => 'options'))
//                        ->addFieldToFilter('attribute_id', array('in' => $brand_mapping_attributes))
                            ->addGroupByEntityId()
                            ->getData();
                    $brand_options = array();
                    foreach ($brand_option_data as $bnd_data) {
                        $brand_options[$attr_codes[$bnd_data['attribute_id']]][$bnd_data['entity_id']] = $bnd_data['value'];
                    }
                }
//end
                if (!empty($store_mapping_attributes))
//get all options for attributes which are mapped for own store
                    $store_options = Mage::getmodel('bridge/incomingchannels')->getoptions($store_mapping_attributes, $store_attr);
            }



//save product content store wise
            $storeview = array();
            foreach ($product_data['store'] as $storeId => $product) {
//set the default value for this product
                $product['entity_id'] = $entityId;
                $product['status'] = $status;
                $product['bridge_channel_id'] = $inc_channel['id'];
//replace the attribute set in attribute map
                $get_attr_codes = @$mapping_data[$current_attributeset];
                $replace_attribute = $this->get_values_for_keys($product, @array_keys($get_attr_codes)); //@array_intersect_key($get_attr_codes, $product);

                $replaced_values = @array_combine($get_attr_codes, $replace_attribute);
//map the attribute options if it is mapped
                if (@$replaced_values)
                    $product = @array_merge($product, $replaced_values);
                if (@$mapping_data[$current_attributeset]) {
                    $option_attributes = array_intersect_key($product, $mapping_data[$current_attributeset]);
                    $option_attributes = array_intersect_key($option_attributes, $brand_options);

                    if (array_filter($option_attributes))
                        foreach ($option_attributes as $bnd_attr_code => $product_value) {
                            if (isset($mapping_data['option_mapping'][$bnd_attr_code][$product_value])) {
                                $product[$bnd_attr_code] = $mapping_data['option_mapping'][$bnd_attr_code][$product_value];
                            } else if ($store_opt_id = @array_search($brand_options[$bnd_attr_code][$product[$bnd_attr_code]], $store_options[$mapping_data[$current_attributeset][$bnd_attr_code]])) {
                                $product[$bnd_attr_code] = $store_opt_id;
                            } else if ($inc_channel['create_option']) {//then create the option for this attribute
                                $product[$bnd_attr_code] = Mage::getmodel('bridge/incomingchannels')->add_new_option($mapping_data[$current_attributeset][$bnd_attr_code], $brand_options[$bnd_attr_code][$product[$bnd_attr_code]]);
                            }
                        }
                }

                if (@$get_attr_codes)
                    $product['attribute_set_id'] = $mapping_data['fieldset'][$current_attributeset];
//check the condition for content source
                if (strcasecmp(@$inc_channel['content_source'], 'original') === 0) {
                    $product['description'] = @$product['description'] ? $product['description'] : @$product['ogc_description'];
                    $product['short_description'] = @$product['short_description'] ? $product['short_description'] : @$product['ogc_short_description'];
                    $product['name'] = @$product['name'] ? $product['name'] : @$product['ogc_name'];
                } else {
                    $product['inc_description'] = @$product['description'] ? $product['description'] : @$product['ogc_description'];
                    $product['inc_short_description'] = @$product['short_description'] ? $product['short_description'] : @$product['ogc_short_description'];
                    $product['inc_name'] = @$product['name'] ? $product['name'] : @$product['ogc_name'];
                    if (!$entityId) {
                        $product['description'] = $product['inc_description'];
                        $product['short_description'] = $product['inc_short_description'];
                        $product['name'] = $product['inc_name'];
                    }
                }
                unset($product['ogc_description']);
                unset($product['ogc_short_description']);
                unset($product['ogc_name']);
                unset($product['stock_item']);
//get product model for storeid
                $product_model = '';
                $product_model = Mage::getModel('catalog/product')->setData($product);
                $product_model->setStoreId($store_mapping[$storeId]);
                try {
                    if (!$product['entity_id']) {
//if the synced produt is configurable
                        if (isset($product_data['ConfigAttr'])) {
//                        $product_model->setCanSaveCustomOptions(true);
                            $product_model->getTypeInstance()->setUsedProductAttributeIds($product_data['ConfigAttr']); //attribute ID of attribute 'color' in my store
                            $configurableAttributesData = $product_model->getTypeInstance()->getConfigurableAttributesAsArray();
                            $product_model->setConfigurableAttributesData($configurableAttributesData);
                            $product_model->setCanSaveConfigurableAttributes(true);
                            $product_model->setConfigurableProductsData(array());
                        }
                    }
//save the product 

                    $product_model->save();
                    $entityId = $product_model->getId();
                    $storeview[] = $store_mapping[$storeId];
                } catch (Exception $e) {
                    echo "<pre>";
                    print_r($product);
                    print_r($e->getMessage());
                    exit;
                }
            }
            if (!$sku_exist_in_catalog) {
                //save product image
                if ($product_data['image']) {
                    $image = $product_data['image'];
                    $this->addProductGalleryImages($image, $inc_channel['sku'], $product_model);
                }
                //if product not exist then set the default channel id
                $defaultstoreId = $this->getDefaultStoreId();
                $product_model = Mage::getModel('catalog/product')->load($entityId);
                $product_model->setStoreId($defaultstoreId);
                $product_model->setBridgeChannelId($inc_channel['id']);
                $product_model->save();
            }
//save product category
            if (isset($product_data['categories']) && $product_data['categories']) {
                $categories = $mapping_data['category'] ? $this->get_values_for_keys($mapping_data['category'], $product_data['categories']) : $product_data['categories'];
                $exist_categories = $product_model->getCategoryIds();

                if ($exist_categories) {
                    $categories = array_merge($categories, $exist_categories);
                    $categories = array_unique($categories);
                }
                $product_model->setCategoryIds($categories);
                $product_model->save();
            }
//end 
//keep the synced sku price and quantity in edi table
            if (isset($api_result['edi'])) {
                if ($inc_channel['product_pricenquantity']) {
                    $edi = unserialize($api_result['edi']);
                    $stockData = $edi['stock'];
                    Mage::getmodel('bridge/incomingchannels_product')->saveSkuInfo($inc_channel['id'], $inc_channel['sku'], $edi['price'], $stockData['qty']);
                    //save the product price and quantity if product isnot prvioulsy exist
                    if (!$sku_exist_in_catalog) {
                        $price = $this->addPriceRule($inc_channel, $product_model->getId(), $edi['price']);
                        //change the product quantity if synced quantity is not 0
                        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product_model->getId());
                        if (intval($stockData['qty'])) {
                            $product_model->addData(array('price' => $price, 'msrp' => $edi['msrp'], 'bridge_pricenqty_channel_id' => $inc_channel['id']));
                            $product_model->save();
                            unset($stockData['item_id']);
                            $stockData['stock_id'] = $stockItem->getStockId();
                            $stockData['product_id'] = $product_model->getId();
                            $stockData['item_id'] = $stockItem->getItemId();
                            // Create the initial stock item object
                            $stockItem->setData($stockData);
                            $stockItem->save();
                        }
                    }
                }
            }
//keep this product updation track in history table
            $storeview = implode(',', $storeview);
            $history_data = array('brand_id' => $inc_channel['id'], 'sku' => $inc_channel['sku'], 'event' => $event, 'store_view' => "$storeview", 'created' => date('Y-m-d H:i:s'));
            $history_model = Mage::getmodel('bridge/incomingchannels_history');
            $history_model->setData($history_data);
            $history_model->save();
            if (@$api_result['related']) {
                Mage::getmodel('bridge/incomingchannels_skus')->insert($api_result, $inc_channel, 'related');
            }
            if (@$api_result['upsell']) {
                Mage::getmodel('bridge/incomingchannels_skus')->insert($api_result, $inc_channel, 'upsell');
            }
            if (@$api_result['crosssell']) {
                Mage::getmodel('bridge/incomingchannels_skus')->insert($api_result, $inc_channel, 'crosssell');
            }
            if (@$api_result['associated']) {
                Mage::getmodel('bridge/incomingchannels_skus')->insert($api_result, $inc_channel, 'associated');
            }
//change the queue staus as processed sku for this channel
            $product_status = $sku_exist_in_catalog ? null : ($inc_channel['product_status'] == 3 ? $current_status : $inc_channel['product_status']);
            $this->changeQueueProcessStatus($inc_channel['sku'], 1, $product_status, $storeview);
            if (!@$api_result['upsell'] && !@$api_result['crosssell'] && !@$api_result['related'] && !@$api_result['associated']) {
                if (!is_null($product_status))
                    $this->changeProductStatus($inc_channel['sku'], $product_status);
                $this->changeQueueRelationStatus($inc_channel['sku'], $inc_channel['id']);
            }
//change the status of the sku from in process to waiting for related product process
//            $status = $sku_exist_in_catalog ? null : ($inc_channel['product_status'] === 0 ? 2 : ($inc_channel['product_status'] === 1 ? 1 : $current_status));
        } else {
// if no sku found for syncing then build relation
            $this->buildRelation();
        }
        exit;
    }

    public function getDefaultStoreId() {
        return Mage::app()->getWebsite()
                        ->getDefaultGroup()
                        ->getDefaultStoreId();
    }

    /**
     * update the product price and quantity
     * @param type $data
     * @param type $inc_channel
     */
    public function updateEDI($data, $inc_channel) {
        $product = Mage::getmodel('catalog/product')->loadByAttribute('sku', $inc_channel['sku']);
        $change_price = false;
        if ($product->getId()) {
            $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
//get the current product price and quantity before save
            $prev_price = $product->getPrice();
            $prev_quantity = $stock->getQty();

            if (!intval($stock->getQty())) {
                $change_price = true;
            } else {
                $prev_updated_cahnnel_id = Mage::getmodel('bridge/incomingchannels_history')->getCollection()
                        ->addFieldToFilter('event', 'price-updated')
                        ->addFieldToFilter('sku', $product->getSku())
                        ->setOrder('id', 'DESC')
                        ->getFirstItem()
                        ->getBrandId();
                if ($prev_updated_cahnnel_id == $inc_channel['id']) {
                    $change_price = true;
                } else if ($prev_updated_cahnnel_id) {
                    $prev_slab = Mage::getmodel('bridge/channels')->load($prev_slab)->getSlab();
                    if ($prev_slab > $inc_channel['slab']) {
                        $change_price = true;
                    } else {
                        if ($product->getPrice() > $data['price']) {
                            $change_price = true;
                        }
                    }
                }
            }
//            echo "change price" . $change_price;
//            exit;
//keep the track of price and quantity of this sku for this incoming channel
            $track_id = Mage::getmodel('bridge/incomingchannels_product')->getCollection()
                    ->addFieldToFilter('channel_id', $inc_channel['id'])
                    ->addFieldToFilter('sku', $inc_channel['sku'])
                    ->getFirstItem()
                    ->getId();
            $track_data = array('id' => $track_id, 'sku' => "{$inc_channel['sku']}", 'channel_id' => $inc_channel['id'], 'quantity' => $data['stock']['qty'], 'price' => $data['price'], 'created' => date('Y-m-d H:i:s'));
            $track_model = Mage::getmodel('bridge/incomingchannels_product');
            $track_model->setData($track_data);
            $track_model->save();
//if product price change is true
            if ($change_price) {
                $stockData = $data['stock'];
                unset($stockData['item_id']);
                $stockData['stock_id'] = $stock->getStockId();
                $stockData['product_id'] = $product->getId();
                $stockData['item_id'] = $stock->getItemId();
// Create the initial stock item object
                $stock->setData($stockData);
                $stock->save();

//change the product price
                $product->setPrice($data['price']);
                $product->save();
//get the current product price and quantity after save
                $cur_price = $product->getPrice();
                $cur_quantity = $stock->getQty();
//keep this product updation track in history table
                $history_data = array('brand_id' => $inc_channel['id'], 'sku' => $inc_channel['sku'], 'event' => 'price-updated', 'previous_price' => $prev_price, 'current_price' => $cur_price, 'previous_quantity' => $prev_quantity, 'current_quantity' => $cur_quantity, 'created' => date('Y-m-d H:i:s'));
                $history_model = Mage::getmodel('bridge/incomingchannels_history');
                $history_model->setData($history_data);
                $history_model->save();
                return true;
            }
        }
        return false;
    }

    /**
     * update the price rule for the product with the specified incoming channel if any price rule is set
     * @param type $productID
     * @param type $channelId
     */
    public function addPriceRule($inc_channel, $entity_id, $edi_price) {
        $price_rule = $inc_channel['multi_price_rule'];
        $use_base_price = $price_rule == 'sum_on_cumulative' ? 0 : 1;
        $all_rule = $price_rule == 'sum_on_cumulative' || $price_rule == 'sum_on_base' ? 1 : 0;
        $actions_model = Mage::getmodel('bridge/incomingchannels_action');
        $actions = $actions_model->getCollection()
                ->addFieldToFilter('brand_id', $inc_channel['id'])
                ->addFieldToSelect(array('type', 'amount', 'conditions_serialized', 'apply', 'custom_price'))
                ->getData();
        $base_price = $last_price = $edi_price;
        $price = array();
//        return $actions;
        foreach ($actions as $action) {
            Mage::unregister('channel_condition');
            $actions_model->setData(array('product_id' => $entity_id, 'conditions_serialized' => $action['conditions_serialized']));
            Mage::register('channel_condition', $actions_model);
            if ($action['apply'] == 'custom_price') {
                $custom_price_rule = $action['custom_price'];
                if ($all_rule) {
                    if ($use_base_price) {
                        $last_price += Mage::helper('bridge')->customPricecalculation($custom_price_rule, $base_price) - $base_price;
                    } else {
                        $last_price = Mage::helper('bridge')->customPricecalculation($custom_price_rule, $last_price);
                    }
                } else {
                    $price[] = Mage::helper('bridge')->customPricecalculation($custom_price_rule, $base_price);
                }
            } else if (Mage::getmodel('bridge/rule')->getMatchingProductIds()) {
                if ($action['type'] == 'increase' && $action['apply'] == 'by_percent') {
                    if ($all_rule) {
                        if ($use_base_price)
                            $last_price += $base_price * ($action['amount'] / 100);
                        else
                            $last_price +=$last_price * $action['amount'] / 100;
                    }else {
                        $price[] = $base_price + ($base_price * $action['amount'] / 100);
                    }
                } else if ($action['type'] == 'decrease' && $action['apply'] == 'by_percent') {
                    if ($all_rule) {
                        if ($use_base_price)
                            $last_price -= $base_price * ($action['amount'] / 100);
                        else
                            $last_price -=$last_price * $action['amount'] / 100;
                    }else {
                        $price[] = $base_price - ($base_price * $action['amount'] / 100);
                    }
                } else if ($action['type'] == 'increase' && $action['apply'] == 'by_fixed') {
                    if ($all_rule) {
                        $last_price += $action['amount'];
                    } else {
                        $price[] = $base_price + $action['amount'];
                    }
                } else if ($action['type'] == 'decrease' && $action['apply'] == 'by_fixed') {
                    if ($all_rule) {
                        $last_price -= $action['amount'];
                    } else {
                        $price[] = $base_price - $action['amount'];
                    }
                }
            }
        }
        $ret_price = $all_rule ? $last_price : (empty($price) ? $base_price : ($price_rule == 'min' ? min($price) : max($price)));
        return $ret_price;
    }

    /**
     * add image gallery to a product
     * @param type $mediaArray
     * @param type $sku
     * @param type $product
     */
    function addProductGalleryImages($mediaArray, $sku, $product) {
        $importDir = Mage::getBaseDir('media') . DS . 'Fedobebridgeimport';
        $importmediaarray = array();
        if (!file_exists($importDir)) {
            mkdir($importDir, 0777, true);
        }
        $mediaimagetype = array('image', 'small_image', 'thumbnail');
        $flagarr = array();
//Here let's copy from remote server to current store
        foreach ($mediaArray as $k => $imageinfo) {
            $imageinfo = (array) $imageinfo;
            $k++;
            $source = $imageinfo['url'];
            $imginf = pathinfo($source);
            $imagefile = $this->imageExists($importDir, $sku, $imginf['extension'], $k);
            $dest = "$importDir/$imagefile";
            if (!empty($imageinfo['types'])) {
                $types = $imageinfo['types'];
                $flag = 1;
            } else {
                $types = array();
                $flag = 0;
            }
            if (copy($source, $dest)) {
                $importmediaarray[] = array(
                    "filename" => $imagefile,
                    "types" => $types
                );
                $flagarr[] = $flag;
            }
        }
//Here to to check if not image types found then set the first image as base image, small iamge and thumnail
        if (array_sum($flagarr) == 0) {
            $importmediaarray[0]['types'] = $mediaimagetype;
        }

        foreach ($importmediaarray as $pos => $fileinfo) {
            $filePath = $importDir . '/' . $fileinfo['filename'];
            if (file_exists($filePath)) {
                try {
                    $product->addImageToMediaGallery($filePath, $fileinfo['types'], false, false);
                    unlink($filePath);
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            } else {
                echo "Product does not have an image or the path is incorrect. Path was: {$filePath}<br/>";
            }
        }
        $product->save();
    }

    function imageExists($importDir, $sku, $ext, $k) {
        if (!file_exists("$importDir/$sku" . "_$k.$ext")) {
            return "$sku" . "_$k.$ext";
        } else {
            $k = $k . "_" . $k;
            return imageExists($importDir, $sku, $ext, $k);
        }
    }

    public function buildRelation() {
        $sku_list = Mage::getmodel('bridge/pimqueue')->getUnmappedSkus();
//        echo "<pre>";
//        print_r($sku_list);exit;
        foreach ($sku_list as $sku_dtls) {
            $parent_skulist = $sku_dtls['sku_dtls'];
            $parent_skulist_array = explode(',', $parent_skulist);
            foreach ($parent_skulist_array as $single_sku) {
                $single_sku = explode(':', $single_sku);
                $product_status[$single_sku[0]] = $single_sku[1];
            }
            $channel_wise_sku_list = Mage::getmodel('bridge/incomingchannels_skus')->getSkuRelation(array_keys($product_status), $sku_dtls['channel_id']);
            if (!$channel_wise_sku_list)
                continue;
            $current_parent_sku = $channel_wise_sku_list[0]['parent_sku'];
            $count = count($channel_wise_sku_list) - 1;
            foreach ($channel_wise_sku_list as $key => $sku_relation) {
                if (($sku_relation['parent_sku'] != $current_parent_sku) || $count == $key) {
                    if ($product_status[$current_parent_sku] !== 0 && !is_null($product_status[$current_parent_sku]) && $product_status[$current_parent_sku] != 'null')
                        $this->changeProductStatus($current_parent_sku, $product_status[$current_parent_sku]);
                    $this->changeQueueRelationStatus($current_parent_sku, $sku_dtls['channel_id']);
                    $current_parent_sku = $sku_relation['parent_sku'];
                }
                $sku_relation['sku'] = explode(',', $sku_relation['sku']);
                if ($sku_relation['relation'] == 'upsell')
                    $this->saveUpSellProduct($sku_relation['parent_sku'], $sku_relation['sku']);
                else if ($sku_relation['relation'] == 'crosssell')
                    $this->saveCrossSellProduct($sku_relation['parent_sku'], $sku_relation['sku']);
                else if ($sku_relation['relation'] == 'related')
                    $this->saveRelatedProduct($sku_relation['parent_sku'], $sku_relation['sku']);
                else if ($sku_relation['relation'] == 'associated')
                    $this->saveConfigurableProduct($sku_relation['parent_sku'], $sku_relation['sku']);
            }
        }
    }

    /**
     * create cross sell relation
     * 
     */
    public function saveCrossSellProduct($parent_sku, $sku_list) {
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $parent_sku);
        $crosssells = $product->getCrossSellProducts();
        $param = array();
        foreach ($crosssells as $item) {
            $param[$item->getId()] = array('position' => $item->getPosition());
        }
        $counter = count($param);
        $entity_ids = $this->getIdBySku($sku_list);
        foreach ($entity_ids as $eid) {
            if (!isset($param[$eid])) { //prevent elements from beeing overwritten
                $param[$eid] = array(
                    'position' => ++$counter
                );
            }
        }

        $product->setCrossSellLinkData($param);
        $product->save();
    }

    /**
     * create up sell relation
     * 
     */
    public function saveUpSellProduct($parent_sku, $sku_list) {
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $parent_sku);
        $upsells = $product->getUpSellProducts();
        $param = array();
        foreach ($upsells as $item) {
            $param[$item->getId()] = array('position' => $item->getPosition());
        }
        $counter = count($param);
        $entity_ids = $this->getIdBySku($sku_list);
        foreach ($entity_ids as $eid) {
            if (!isset($param[$eid])) { //prevent elements from beeing overwritten
                $param[$eid] = array(
                    'position' => ++$counter
                );
            }
        }
        $product->setUpSellLinkData($param);
        try {
            $product->save();
        } catch (Exception $e) {

            print_r($e);
            exit;
        }
    }

    /**
     * create related product relation
     * 
     */
    public function saveRelatedProduct($parent_sku, $sku_list) {
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $parent_sku);
        $related = $product->getRelatedProducts();
        $param = array();
        foreach ($related as $item) {
            $param[$item->getId()] = array('position' => $item->getPosition());
        }
        $counter = count($param);
        $entity_ids = $this->getIdBySku($sku_list);
        foreach ($entity_ids as $eid) {
            if (!isset($param[$eid])) { //prevent elements from beeing overwritten
                $param[$eid] = array(
                    'position' => ++$counter
                );
            }
        }
        $product->setRelatedLinkData($param);
        $product->save();
    }

    /**
     * assign simple product to configurable product
     * 
     */
    public function saveConfigurableProduct($parent_sku, $sku_list) {
        $mainConfigrableProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $parent_sku);
        $entity_ids = $this->getIdBySku($sku_list);
////add simple product to configurable product
        Mage::getResourceSingleton('catalog/product_type_configurable')->saveProducts($mainConfigrableProduct, $entity_ids);
    }

    /**
     */
    public function getProductStatus($exist, $current_status, $incoming_channel) {
        $status = $exist ? ($incoming_channel['product_status'] === 0 ? 2 : ($incoming_channel['product_status'] === 1 ? 1 : $current_status)) : 2;
        return $status;
    }

    /**
     * get product id by product sku
     * @param type $sku_list
     * @return type
     */
    public function getIdBySku($sku_list) {
        $product_skus = Mage::getmodel('catalog/product')->getCollection()
                        ->addAttributeToFilter('sku', array('in' => $sku_list))
                        ->addAttributeToSelect('entity_id')->getData();
        return array_column($product_skus, 'entity_id', 'sku');
    }

    /**
     * change the status process of incoming channel sku
     * @param type $id
     */
    public function changeQueueProcessStatus($sku, $is_processed, $product_status, $storeview) {
        Mage::getModel('bridge/pimqueue')->changeProcessStatus($sku, $is_processed, $product_status, $storeview);
    }

    /**
     * change the status process of incoming channel sku
     * @param type $id
     */
    public function changeQueueRelationStatus($sku, $channel_id) {
        Mage::getModel('bridge/pimqueue')->changeRelationStatus($sku, $channel_id);
    }

    /**
     * change the product stataus
     * @param type $sku
     * @param type $product_status
     */
    public function changeProductStatus($sku, $product_status) {
//        $defaultstoreId = $this->getDefaultStoreId();
        $product_id = Mage::getModel("catalog/product")->getIdBySku($sku);
        try {
            $product_status = (int) $product_status;
            Mage::getModel('catalog/product_status')->updateProductStatus($product_id, 0, $product_status);
        } catch (Exception $e) {
//            echo "<pre>";
//            print_r($e->getMessage());
        }
    }

    /**
     * replace the place holder by the variable
     */
    public function varReplace($string, $product) {
        $place_holders = array('{name}', '{sku}');
        $replace = array(@$product['name'], @$product['sku']);
        return str_replace($place_holders, $replace, $string);
    }

    /**
     * return the channel priority
     */
    public function checkChannelPriority($channel1, $channel2) {
        $group_id1 = Mage::getmodel('bridge/groups')->getCollection()
                ->addFieldToFilter("channel_id", array('finset' => "$channel1"))
                ->getFirstItem()
                ->getId();
        $group_id2 = Mage::getmodel('bridge/groups')->getCollection()
                ->addFieldToFilter("channel_id", array('finset' => "$channel2"))
                ->getFirstItem()
                ->getId();
        $rtrn = @$group_id1 ? ((!$group_id2 || $group_id1 < $group_id2) ? true : ($group_id1 == $group_id2 ? "same" : false)) : false;
        return $rtrn;
    }

    /**
     * get the result of the api call
     * @param type $api_id 
     * @param type $action method that need to call
     * @return type array of data
     */
    public function getApiResult($incoming_channel=null, $method = '') {
//oAuth parameters
        $params = array(
            'siteUrl' => $incoming_channel['url'] . '/oauth',
            'requestTokenUrl' => $incoming_channel['url'] . '/oauth/initiate',
            'accessTokenUrl' => $incoming_channel['url'] . '/oauth/token',
            'consumerKey' => $incoming_channel['key'],
            'consumerSecret' => $incoming_channel['secret']
        );

// Get session
        $session = Mage ::getSingleton('core/session');
// Read and unserialize request token from session
//        $requestToken = unserialize($session->getRequestToken());
// Initiate oAuth consumer
        $consumer = new Zend_Oauth_Consumer($params);
        $acessToken = new Zend_Oauth_Token_Access();
//set the permanent token and secret
        $acessToken->setToken($incoming_channel['permanent_token']);
        $acessToken->setTokenSecret($incoming_channel['permanent_secret']);
// Get HTTP client from access token object
        $restClient = $acessToken->getHttpClient($params);

          //$restClient->setUri("http://localhost/magento/api/rest/products/sku/0");//for particular sku
          //$restClient->setUri("http://localhost/magento/api/rest/products/sku/0");//for witout sku
          //$restClient->setUri("http://dev.fedobe.org/bridge/reseller/api/rest/products/sku/msj000");//for particular sku
        
          $restClient->setUri("http://dev.fedobe.org/bridge/reseller/api/rest/products/sku/0");//for witout sku
        
//         echo $restClient->getUri();exit;

// In Magento it is neccesary to set json or xml headers in order to work
        $restClient->setHeaders('Accept', 'application/json');
        $restClient->setMethod(Zend_Http_Client::GET);
// Get method
//Make REST request
        $response = $restClient->request();
//        $pim = (array) json_decode($response->getBody());
//        echo "<pre>";print_r(unserialize($pim['pim']));exit;
        echo "<pre>";print_r(json_decode(json_decode($response->getBody())));exit;
        return (array) json_decode($response->getBody());
    }

    /**
     * get the values of mapping array of the keys provided in keys array
     * @param type $mapping
     * @param type $keys
     * @return type array
     */
    public function get_values_for_keys($mapping, $keys) {
        if ($keys)
            foreach ($keys as $key) {
                $output_arr[] = @$mapping[$key];
            }
        return @$output_arr;
    }

}