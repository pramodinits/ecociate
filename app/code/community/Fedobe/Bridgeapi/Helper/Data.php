<?php

class Fedobe_Bridgeapi_Helper_Data extends Mage_Core_Helper_Abstract {
//get offernote from the action table    
public function getOfferNote($channel_info, $product_id, $type = "inc", $store_id = null) {
        if ($type == "inc") {
            $actions_model = Mage::getmodel('bridge/incomingchannels_action');
            $actions = $actions_model->getCollection()
                    ->addFieldToFilter('brand_id', $channel_info['id'])
                    ->addFieldToFilter('content_type', 3)
                    ->addFieldToSelect(array('name_rule', 'short_desc_rule', 'desc_rule', 'conditions_serialized'))
                    ->getData();
        } else {
            $actions_model = Mage::getmodel('bridge/outgoingchannels_action');
            $actions = $actions_model->getCollection()
                    ->addFieldToFilter('channel_id', $channel_info['id'])
                    ->addFieldToFilter('content_type', 3)
                    ->addFieldToSelect(array('offer_note', 'conditions_serialized'))
                    ->getData();
        }
        
        $result = array();
        $store_id = !is_null($store_id) ? $store_id : Mage::app()
                        ->getWebsite()
                        ->getDefaultGroup()
                        ->getDefaultStoreId();

        $product = Mage::getmodel('catalog/product')->setStoreId($store_id)->load($product_id);
        
        foreach ($actions as $action) {
            Mage::unregister('channel_condition');
            $actions_model->setData(array('product_id' => $product_id, 'conditions_serialized' => $action['conditions_serialized']));
            Mage::register('channel_condition', $actions_model);
            if (Mage::getmodel('bridge/rule')->getMatchingProductIds()) {
                $result['offer_note'] = $action['offer_note'];
                return $result;
            }
        }
    }

}
