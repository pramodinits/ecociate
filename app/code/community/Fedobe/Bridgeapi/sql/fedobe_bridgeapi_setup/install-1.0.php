<?php

$installer = $this;
$installer->startSetup();
//echo $installer->getTable('bridgeapi/bridgeapi');exit;
$installer->run("

CREATE TABLE IF NOT EXISTS bridgeapi_outgoing (
  `id` int(11) unsigned NOT NULL auto_increment,
  `incoming_id` int(11) NULL default 0,
  `product_id` int(11) NULL default 0,
  `product_sku` varchar(255) NULL default 0,
  `is_send` smallint(6) NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO api2_acl_rule SELECT NULL, entity_id, 'bridgeapi','retrieve' FROM api2_acl_role WHERE role_name = 'Bridge API';
ALTER TABLE `bridge_outgoingchannel_action` ADD `offer_note` TEXT NULL AFTER `desc_rule`;
    ");

$installer->endSetup();