<?php

class Fedobe_Bridgeapi_Model_Api2_Product_Rest_Admin_V1 extends Mage_Api2_Model_Resource {

    public function _retrieveCollection() {
        $params = $this->getRequest()->getParams();
        //apache_request_headers ();
//check if the request channel is  valid or not
        if ($otgchannel = $this->validateRequest()) {
            //return $otgchannel;
            //return $otgchannel->getFilterBy();
            $result = "No Result";
            if ($otgchannel->getProductInfo() && ($otgchannel->getUpdateExist() || $otgchannel->getFilterBy())) {
                if (!isset($params['sku'])) {
                    if (isset($params['reset'])) {
                        $result = $this->resetBatchSKU($otgchannel);
                    } else {
                        $result = $this->getBatchSKU($otgchannel);
                    }
                } else if ($params['sku'] == "0") {
                    if (!isset($params['edi'])) {
                        $result = $this->getPIMNoSku($otgchannel);
                    } else {
                        if (isset($params['changed'])) {
                            $result = $this->getEDINoSku($otgchannel, $params['changed']); //send the changed edi data
                        } else {
                            $result = $this->getEDINoSku($otgchannel); //send all edi data(synced and changed)
                        }
                    }
                } else {
                    if (!isset($params['edi'])) {
                        $result = $this->getPIM($otgchannel);
                    } else {
                        $result = $this->getEDI($otgchannel);
                    }
                }
            }
            return $result;
        } else {
            return "Sorry invalid request";
        }
    }

    public function getBatchSKU($otgchannel) {
        $sku_list = array();
//        if (strcasecmp($otgchannel->getFilterBy(), 'sku') === 0) {
//            $product_ids = explode(',', $otgchannel->getSkuList());
//        } else {
//            Mage::register('channel_condition', $otgchannel);
//            $product_ids = Mage::getmodel('bridge/rule')->getMatchingProductIds();
//            $product_status = $otgchannel->getProductStatus();
//            if ($product_status)
//                $product_ids = Mage::getmodel('bridge/outgoingchannels')->getFilteredSkus($product_ids, $product_status);
//        }
//        $sku_list = $this->getSkuById($product_ids);
//        return $sku_list;
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $productinfo_table_name = $resource->getTableName('bridge/incomingchannels_product');
        //$sql = "SELECT product_entity_id,sku FROM " . $productinfo_table_name . " WHERE  channel_id = " . $otgchannel->getData('id') . " AND channel_type ='outgoing' ORDER BY FIELD( is_processed, '3', '0', '-1', '2', '1', '4' ) ";
        $sql = "SELECT product_entity_id,sku FROM " . $productinfo_table_name . " WHERE  channel_id = " . $otgchannel->getData('id') . " AND channel_type ='outgoing' ORDER BY FIELD( is_processed, '3', '0', '-1', '2', '-2', '1', '4' ) ";
        $sqlattrQuery = $readConnection->query($sql);
        $skus_str = array();
        while ($row = $sqlattrQuery->fetch()) {
            $skus_str[$row['product_entity_id']] = $row['sku'];
        }
        //$sku_list = $this->getSkuById($skus_str);
        return $skus_str;
    }

    public function getAttributesAsDescription($product) {
        $desc = '<ul>';
        $attributes = $product->getAttributes();
        foreach ($attributes as $attribute) {
            if ($attribute->getIsVisibleOnFront() && ($product->getData($attribute->getAttributeCode())) ) {
                $desc .= "<li>" . $product->getResource()->getAttribute($attribute->getAttributeCode())->getStoreLabel() . " : " . $attribute->getFrontend()->getValue($product) . "</li>";
            }
        }
        $desc .= "</ul>";
        return $desc;
    }

    //Reseting the batched sku for pim call
    public function resetBatchSKU($otgchannel) {
        $channel_id = $otgchannel->getData('id'); //outgoing channel_id
        $resource = Mage::getSingleton('core/resource');
        $bridgeapioutgoing = $resource->getTableName('bridgeapi/bridgeapi');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');

        //Delete all record of the channel_id from bridge_outgoing table
        $where = "incoming_id =" . $channel_id;
        $writeConnection->delete($bridgeapioutgoing, $where);
        return Mage::helper('core')->jsonEncode("sucessfully reset");
    }

    public function getPIMNoSku($otgchannel) {
        $sku_list = $this->getBatchSKU($otgchannel);
        $sku_list_str = implode(',', $sku_list);
        //$match_product_ids_str = implode(',', $match_product_ids);
        $incoming_id = $otgchannel->getData('id'); //outgoing channel_id

        $resource = Mage::getSingleton('core/resource');
        $bridgeapioutgoing = $resource->getTableName('bridgeapi/bridgeapi');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');
        $sql = $readConnection->select()->from($bridgeapioutgoing, 'product_sku')->where("FIND_IN_SET(product_sku,'$sku_list_str') AND incoming_id = $incoming_id AND is_send = 1 ");
        $processdata = $readConnection->fetchAll($sql);

        if (!count($processdata)) {
            $unsent_product_sku = array();
            $unsent_product_sku = $sku_list;
        } else {
            foreach ($processdata as $bridgeapidata) {
                $sent_product_sku[] = $bridgeapidata['product_sku'];
            }
            $unsent_product_sku = array_diff($sku_list, $sent_product_sku);
        }

        $products = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('*') // <- careful with this
                ->addAttributeToFilter('sku', array('in' => $unsent_product_sku))
                ->setPageSize(2);

        //Remove extra product information
        $remove_fields = array('inc_name', 'inc_description', 'inc_short_description', 'channel_priority', 'bridge_channel_id', 'bridge_pricenqty_channel_id', 'bridge_content_edited', 'meta_title', 'meta_description', 'url_path', 'ogc_price', 'created_at', 'updated_at', 'meta_keyword');

        $updated_at = strtotime(str_replace("_", " ", $this->getRequest()->getParam('updated')));
        $remove_fields = array_flip($remove_fields);


        //These settings will be used after the data sent 
        $sendproducts_bridgeapi = array();
        $sendproducts_bridgeapi['incoming_id'] = $otgchannel->getData('id');
        $sendproducts_bridgeapi['is_send'] = 1;
        //End
        //Here let's iterate the product collection and prepare the data to be sent
        foreach ($products as $temp) {
            $attributes = $temp->getAttributes();
            foreach ($attributes as $attribute) {
                $options = $attribute->getSource()->getAllOptions(false);
                if (is_array($options) && !empty($options)) {
                    $temp->setData($attribute->getAttributeCode(), $attribute->getFrontend()->getValue($temp));
                }
            }
            foreach ($remove_fields as $key => $val) {
                $temp->unsetData($key);
            }
            $temp->unsetData('_cache_editable_attributes');
            //Here goes for content rule
            if ($otgchannel->getContentSource() == 'contentrule') {
                $contentruledata = Mage::helper('bridge')->getConvertedString($otgchannel, $temp['entity_id'], 'ogc');
                $temp['name'] = $contentruledata['name'];
                $temp['description'] = ($contentruledata['description']) ? $contentruledata['description'] : $this->getAttributesAsDescription($temp);
                $temp['short_description'] = $contentruledata['short_description'];
            }
            if ($otgchannel->getContentSource() == 'copy') {
                $temp['name'] = $temp['ogc_name'];
                $temp['description'] = $temp['ogc_description'];
                $temp['short_description'] = $temp['ogc_short_description'];
            }
            if ($otgchannel->getContentSource() == 'original') {
                $temp['name'] = $temp['name'];
                $temp['description'] = $temp['description'];
                $temp['short_description'] = $temp['short_description'];
            }
            if ($otgchannel->getContentSource() == 'contentrule_or_copy') {
                $contentruledata = Mage::helper('bridge')->getConvertedString($otgchannel, $temp['entity_id'], 'ogc');
                $cont_name = $contentruledata['name'];
                $cont_desc = ($contentruledata['description']) ? $contentruledata['description'] : $this->getAttributesAsDescription($temp);
                $cont_short_desc = $contentruledata['short_description'];
                $temp['name'] = $cont_name ? $cont_name : $temp['ogc_name'];
                $temp['description'] = $cont_desc ? $cont_desc : $temp['ogc_description'];
                $temp['short_description'] = $cont_short_desc ? $cont_short_desc : $temp['ogc_short_description'];
            }
            if ($otgchannel->getContentSource() == 'copy_or_contentrule') {
                $contentruledata = Mage::helper('bridge')->getConvertedString($otgchannel, $temp['entity_id'], 'ogc');
                $cont_name = $contentruledata['name'];
                $cont_desc = ($contentruledata['description']) ? $contentruledata['description'] : $this->getAttributesAsDescription($temp);
                $cont_short_desc = $contentruledata['short_description'];
                $temp['name'] = $temp['ogc_name'] ? $temp['ogc_name'] : $cont_name;
                $temp['description'] = $temp['ogc_description'] ? $temp['ogc_description'] : $cont_desc;
                $temp['short_description'] = $temp['ogc_short_description'] ? $temp['ogc_short_description'] : $cont_short_desc;
            }
            $temp->unsetData('_cache_editable_attributes');
            unset($temp['ogc_name']);
            unset($temp['ogc_description']);
            unset($temp['ogc_short_description']);
            //End of content rule
            //Get offer_note and assign to the product
            $offernote_data = Mage::helper('bridgeapi')->getOfferNote($otgchannel, $temp['entity_id'], 'ogc');
            $temp['offer_note'] = $offernote_data['offer_note'] ? $offernote_data['offer_note'] : "";
            //End of offer_note  
            //Deleivery time
            if (Mage::helper('core')->isModuleEnabled('Fedobe_Suppliers')) {
                $feeobj = new Fedobe_Suppliers_Block_Suppliers();
                $shipppingfee = $feeobj->getProductDeliveryTime($temp['sku'], 1);
                $temp['estimate_time'] = $shipppingfee['estimate_time'];
                $temp['custom_estimate_time'] = $shipppingfee['custom_estimate_time'];
            }

            //end
            //For sending product status of product from the bridge_incomingchannel_product_info wheather synced or change etc
            $productinfo_table_name = $resource->getTableName('bridge/incomingchannels_product');
            $sql = "SELECT * FROM " . $productinfo_table_name . " WHERE sku = '" . $temp['sku'] . "' AND channel_id = " . $otgchannel['id'] . " AND channel_type ='outgoing' ";
            $productinfodata = $readConnection->fetchAll($sql);
            //$status_arr = array(-1 => "new" ,0 => "changed", 1 => "synced", 2 => "processing", 3 => "no access", 4 => "trashed");

            $temp["product_status"] = $this->getEdiStatus($productinfodata[0]["is_processed"]);
            //End of sending of product status of product from the bridge_incomingchannel_product_info wheather synced or change etc
            //Here let go for image
            $temp['image'] = $this->getImageInformations($temp->getId());
            //End of image
            //Here let gore for Category
            $catname = array();
            $store_id = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();
            $category_collection = Mage::getModel('catalog/category')
                    ->getCollection()
                    ->setStoreId($store_id)
                    ->addAttributeToSelect(array('entity_id', 'parent_id', 'category_id', 'name'))
                    ->addAttributeToFilter('entity_id', array('in' => $temp->getCategoryIds()))
                    ->addIsActiveFilter();
            foreach ($category_collection as $category) {
                $catname[] = $category->getName();
            }
            $temp['category_name'] = implode(',', $catname);
            //End of category
            //Here let goes for price settings
            if ($otgchannel->getProductPricenquantity()) {
                $base_price = $otgchannel->getPriceSource() != "ogc_price" ? $temp[$otgchannel->getPriceSource()] : ($temp['ogc_price'] ? $temp['ogc_price'] : ($temp['price'] ? $temp['price'] : $temp['msrp']));
                $base_price = $base_price ? $base_price : 0;
                $temp['price'] = Mage::helper('bridge')->addPriceRule($otgchannel, $temp['entity_id'], $base_price, 'ogc');
                $temp['msrp'] = $temp->getMsrp();
                $temp['quantity'] = Mage::getModel('cataloginventory/stock_item')->loadByProduct($temp)->getQty();
                unset($temp['special_price']);
                unset($temp['msrp']);
            } else {
                unset($temp['price']);
                unset($temp['special_price']);
                unset($temp['msrp']);
            }
            //End of price settings
            //Here let's mark these data as sent
            $sendproducts_bridgeapi['product_id'] = $temp['entity_id'];
            $sendproducts_bridgeapi['product_sku'] = $temp['sku']; //sku
            Mage::getModel('bridgeapi/bridgeapi')->setData($sendproducts_bridgeapi)->save();
            //End 
            //Making sku uppercase when to send 
            $temp['sku'] = strtoupper(trim($temp['sku']));
            //end
        }
        return Mage::helper('core')->jsonEncode($products);
    }

//Send product information for the requested single sku
    public function getPIM($otgchannel) {
        $sku = urldecode($this->getRequest()->getParam('sku'));
        //$match_product_ids = $this->getBatchSKU($otgchannel);
        //$sku_list = $this->getSkuById($match_product_ids);
        $sku_list = $this->getBatchSKU($otgchannel);
        if (in_array($sku, $sku_list)) {
            $product_ext = Mage::getmodel('catalog/product')->loadByAttribute('sku', $sku);
            //return Mage::helper('core')->jsonEncode($product_ext);
        } else {
            $product_ext = array();
            return Mage::helper('core')->jsonEncode($product_ext);
        }
        //if (!$product_ext->getId()) {
        if (!$product_ext) {
            //return array('Invalid SKU');
            $invalid_arr = array('Invalid SKU');
            Mage::helper('core')->jsonEncode($invalid_arr);
        }
        $updated_at = strtotime(str_replace("_", " ", $this->getRequest()->getParam('updated')));
        $product_updated = strtotime($product_ext->getUpdatedAt());
        if ($product_updated - $updated_at <= 0) {
            return '';
        }

        $product_info = array();

        //end
        //Remove extra product information
        $remove_fields = array('inc_name', 'inc_description', 'inc_short_description', 'channel_priority', 'bridge_channel_id', 'bridge_pricenqty_channel_id', 'bridge_content_edited', 'meta_title', 'meta_description', 'url_path', 'ogc_price', 'created_at', 'updated_at', 'meta_keyword');

        $remove_fields = array_flip($remove_fields);

        $product_data = $product_ext->getData();
        $attributes = $product_ext->getAttributes();
        foreach ($attributes as $attribute) {
            $options = $attribute->getSource()->getAllOptions(false);
            if (!empty($options)) {
                $product_data[$attribute->getAttributeCode()] = $attribute->getFrontend()->getValue($product_ext);
            }
        }
        $product_data = array_diff_key($product_data, $remove_fields);
        //for content rule
        if ($otgchannel->getContentSource() == 'contentrule') {
            $contentruledata = Mage::helper('bridge')->getConvertedString($otgchannel, $product_data['entity_id'], 'ogc');
            $product_data['name'] = $contentruledata['name'];
            $product_data['description'] = ($contentruledata['description']) ? $contentruledata['description'] : $this->getAttributesAsDescription($product_ext);
            $product_data['short_description'] = $contentruledata['short_description'];
        }
        //for outgoing content
        if ($otgchannel->getContentSource() == 'copy') {
            $product_data['name'] = $product_data['ogc_name'];
            $product_data['description'] = $product_data['ogc_description'];
            $product_data['short_description'] = $product_data['ogc_short_description'];
        }
        //for orginal content
        if ($otgchannel->getContentSource() == 'original') {
            $product_data['name'] = $product_data['name'];
            $product_data['description'] = $product_data['description'];
            $product_data['short_description'] = $product_data['short_description'];
        }
        //for contentrule else outgoing
        if ($otgchannel->getContentSource() == 'contentrule_or_copy') {
            $contentruledata = Mage::helper('bridge')->getConvertedString($otgchannel, $product_data['entity_id'], 'ogc');
            $product_data['contentrule_name'] = $contentruledata['name'];
            $product_data['contentrule_description'] = ($contentruledata['description']) ? $contentruledata['description'] : $this->getAttributesAsDescription($product_ext);
            $product_data['contentrule_short_description'] = $contentruledata['short_description'];

            $product_data['name'] = $product_data['contentrule_name'] ? $product_data['contentrule_name'] : $product_data['ogc_name'];
            $product_data['description'] = $product_data['contentrule_description'] ? $product_data['contentrule_description'] : $product_data['ogc_description'];
            $product_data['short_description'] = $product_data['contentrule_short_description'] ? $product_data['contentrule_short_description'] : $product_data['ogc_short_description'];

            unset($product_data['contentrule_name']);
            unset($product_data['contentrule_description']);
            unset($product_data['contentrule_short_description']);
        }
        //for outgoing else contentrule
        if ($otgchannel->getContentSource() == 'copy_or_contentrule') {
            $contentruledata = Mage::helper('bridge')->getConvertedString($otgchannel, $product_data['entity_id'], 'ogc');
            $product_data['contentrule_name'] = $contentruledata['name'];
            $product_data['contentrule_description'] = ($contentruledata['description']) ? $contentruledata['description'] : $this->getAttributesAsDescription($product_ext);
            $product_data['contentrule_short_description'] = $contentruledata['short_description'];

            $product_data['name'] = $product_data['ogc_name'] ? $product_data['ogc_name'] : $product_data['contentrule_name'];
            $product_data['description'] = $product_data['ogc_description'] ? $product_data['ogc_description'] : $product_data['contentrule_description'];
            $product_data['short_description'] = $product_data['ogc_short_description'] ? $product_data['ogc_short_description'] : $product_data['contentrule_short_description'];

            unset($product_data['contentrule_name']);
            unset($product_data['contentrule_description']);
            unset($product_data['contentrule_short_description']);
        }
        unset($product_data['ogc_name']);
        unset($product_data['ogc_description']);
        unset($product_data['ogc_short_description']);

        //Get offer_note and assign to the product
        $offernote_data = Mage::helper('bridgeapi')->getOfferNote($otgchannel, $product_data['entity_id'], 'ogc');
        $product_data['offer_note'] = $offernote_data['offer_note'] ? $offernote_data['offer_note'] : "";
        //End of offer_note  
        //Deleivery time
        if (Mage::helper('core')->isModuleEnabled('Fedobe_Suppliers')) {
            $feeobj = new Fedobe_Suppliers_Block_Suppliers();
            $shipppingfee = $feeobj->getProductDeliveryTime($product_data['sku'], 1);
            $product_data['estimate_time'] = $shipppingfee['estimate_time'];
            $product_data['custom_estimate_time'] = $shipppingfee['custom_estimate_time'];
        }

        //end
//        if ($entityId && $updated) {
        //set images for pim
        /*
          $image_model = Mage::getmodel('catalog/product')->load($product_ext->getId());
          $images = $image_model->getData('media_gallery');
          foreach ($images['images'] as $image) {
          $result[] = array(
          'id' => $image['value_id'],
          'label' => $image['label'],
          'position' => $image['position'],
          'exclude' => $image['disabled'],
          'url' => Mage::getSingleton('catalog/product_media_config')->getMediaUrl($image['file']),
          'types' => $this->_getImageTypesAssignedToProduct($image_model, $image['file'])
          );
          }
          $product_data['image_gallery'] = $result;
         */
        //set images for pim
        $product_data['image_gallery'] = $this->getImageInformations($product_ext->getId()); //$result;
        $product_data1['categories'] = $product_ext->getCategoryIds();
        $catname = array();
        $store_id = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();
        $category_collection = Mage::getModel('catalog/category')
                ->getCollection()
                ->setStoreId($store_id)
                ->addAttributeToSelect(array('entity_id', 'parent_id', 'category_id', 'name'))
                ->addAttributeToFilter('entity_id', array('in' => $product_data1['categories']))
                ->addIsActiveFilter();
        foreach ($category_collection as $category) {
            $catname[] = $category->getName();
        }
        $product_data['category_name'] = implode(',', $catname);

        if ($otgchannel->getProductPricenquantity()) {
            //$product_data['price'] = $this->applyPriceRule($otgchannel, $product_ext);
            $base_price = $otgchannel->getPriceSource() != "ogc_price" ? $product_data[$otgchannel->getPriceSource()] : ($product_data['ogc_price'] ? $product_data['ogc_price'] : ($product_data['price'] ? $product_data['price'] : $product_data['msrp']));
            $base_price = $base_price ? $base_price : 0;
            $product_data['price'] = Mage::helper('bridge')->addPriceRule($otgchannel, $product_data['entity_id'], $base_price, 'ogc');
            $product_data['msrp'] = $product_ext->getMsrp();
            $product_data['quantity'] = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product_ext)->getQty();
            unset($product_data['special_price']);
            unset($product_data['msrp']);
        } else {
            unset($product_data['price']);
            unset($product_data['special_price']);
            unset($product_data['msrp']);
        }

        //For sending product status of product from the bridge_incomingchannel_product_info wheather synced or change etc
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');
        $productinfo_table_name = $resource->getTableName('bridge/incomingchannels_product');
        $sql = "SELECT * FROM " . $productinfo_table_name . " WHERE sku = '" . $product_data['sku'] . "' AND channel_id = " . $otgchannel['id'] . " AND channel_type ='outgoing' ";
        $productinfodata = $readConnection->fetchAll($sql);
        //$status_arr = array(-1 => "new" ,0 => "changed", 1 => "synced", 2 => "processing", 3 => "no access", 4 => "trashed");
        $product_data["product_status"] = $this->getEdiStatus($productinfodata[0]["is_processed"]);
        //End of sending of product status of product from the bridge_incomingchannel_product_info wheather synced or change etc
//        } 
        $product_data['sku'] = strtoupper(trim($product_data['sku']));
        return Mage::helper('core')->jsonEncode($product_data);
    }

    //Get EDI function for multiple sku
    public function getEDINosku($otgchannel, $changed = 0) {
        $channel_id = $otgchannel->getId(); //outgoing channel_id
        $sku_list = $this->getBatchSKU($otgchannel);
        $sku_list_str = implode(',', $sku_list);

        if ($otgchannel->getProductPricenquantity()) {
            //Get unsend product details from the product_info table
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $writeConnection = $resource->getConnection('core_write');
            $productinfo_table_name = $resource->getTableName('bridge/incomingchannels_product');
            if ($changed) {
                $sql = "SELECT * FROM " . $productinfo_table_name . " WHERE FIND_IN_SET(sku,'$sku_list_str') AND channel_id = " . $channel_id . " AND channel_type ='outgoing' AND is_processed = 0";
            } else {
                $sql = "SELECT * FROM " . $productinfo_table_name . " WHERE FIND_IN_SET(sku,'$sku_list_str') AND channel_id = " . $channel_id . " AND channel_type ='outgoing'";
            }
            //echo $sql;exit;
            //$sql = "SELECT * FROM " . $productinfo_table_name . " WHERE FIND_IN_SET(sku,'$sku_list_str') AND channel_id = " . $channel_id . " AND channel_type ='outgoing'";
            $processdata = $readConnection->fetchAll($sql);

            //set EDI data (sku,price,quantiy)
            $edi_data = array();
            $history = array();
            $i = 0;
            foreach ($processdata as $k => $v) {
                $edi_data[$i]['sku'] = $v['sku'];
                $history[$i]['current_price'] = $edi_data[$i]['price'] = $v['converted_price'];
                $history[$i]['current_quantity'] = $edi_data[$i]['quantity'] = $v['quantity'];
                //product status in the product_info table
                //$status_arr = array(-1 => "new" ,0 => "changed", 1 => "synced", 2 => "processing", 3 => "no access", 4 => "trashed");
                $productstatus = $this->getEdiStatus($v["is_processed"]);
                $edi_data[$i]['product_status'] = ($v["deleted"]) ? "Deleted" : $productstatus;
                //Deleivery time
                if (Mage::helper('core')->isModuleEnabled('Fedobe_Suppliers')) {
                    $feeobj = new Fedobe_Suppliers_Block_Suppliers();
                    $shipppingfee = $feeobj->getProductDeliveryTime($v['sku'], 1);
                    $edi_data[$i]['estimate_time'] = $shipppingfee['estimate_time'];
                    $edi_data[$i]['custom_estimate_time'] = $shipppingfee['custom_estimate_time'];
                }
                //end
                //Update the is_processed = 1 if it is 0 in table bridge_incomingchannel_product_info  after sending ediInfo
                if ($v['is_processed'] == "0" || $v['is_processed'] == "-1") {
                    $update_Is_processed = array("is_processed" => 1);
                    $where = "sku = '" . $v['sku'] . "' AND channel_id = " . $channel_id . " AND channel_type ='outgoing' ";
                    $writeConnection->update($productinfo_table_name, $update_Is_processed, $where);
                }
                //end
                //Update the is_processed = 4 if it is 3 in table bridge_incomingchannel_product_info  after sending ediInfo
                if ($v['is_processed'] == "3") {
                    $update_Is_processed = array("is_processed" => 4);
                    $where = "sku = '" . $v['sku'] . "' AND channel_id = " . $channel_id . " AND channel_type ='outgoing' ";
                    $writeConnection->update($productinfo_table_name, $update_Is_processed, $where);
                }
                //end
                //Make sku uppercase when to Send
                $history[$i]['sku'] = $edi_data[$i]['sku'] = strtoupper(trim($edi_data[$i]['sku']));
                $history[$i]['brand_id'] = $channel_id;
                $history[$i]['channel_type'] = 'outgoing';
                $history[$i]['event'] = 'synced';
                $history[$i]['source'] = 'api';
                $history[$i]['status'] = $productstatus;
                
                //end
                $i = $i + 1;
            }
            if(!empty($history)){
                $history_table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_history');
                $writeConnection->insertMultiple($history_table, $history);
            }
            return Mage::helper('core')->jsonEncode($edi_data);
        }
    }

    //Get EDI function for single sku
    public function getEDI($otgchannel) {
        $sku = urldecode($this->getRequest()->getParam('sku'));
        $channel_id = $otgchannel->getId();

        //Get the product details from the product_info table
        if ($otgchannel->getProductPricenquantity()) {
            $resource = Mage::getSingleton('core/resource');
            $productinfo_table_name = $resource->getTableName('bridge/incomingchannels_product');
            $readConnection = $resource->getConnection('core_read');
            $writeConnection = $resource->getConnection('core_write');
            $sql = "SELECT * FROM " . $productinfo_table_name . " WHERE sku = '" . $sku . "' AND channel_id = " . $channel_id . " AND channel_type ='outgoing' limit 1";
            $processdata = $readConnection->fetchAll($sql);

            //Set EDI data (sku,price and quantity)
            $edi_data = array();
            $edi_data['sku'] = $processdata[0]['sku'];
            $history[0]['current_price'] =  $edi_data['price'] = $processdata[0]['converted_price'];
            $history[0]['current_quantity'] = $edi_data['quantity'] = $processdata[0]['quantity'];
            //product status in the product_info table
            //$status_arr = array(-1 => "new" ,0 => "changed", 1 => "synced", 2 => "processing", 3 => "no access", 4 => "trashed");
            $productstatus = $this->getEdiStatus($processdata[0]["is_processed"]);
            $edi_data['product_status'] = ($processdata[0]["deleted"]) ? "Deleted" : $productstatus;
            //Deleivery time
            if (Mage::helper('core')->isModuleEnabled('Fedobe_Suppliers')) {
                $feeobj = new Fedobe_Suppliers_Block_Suppliers();
                $shipppingfee = $feeobj->getProductDeliveryTime($processdata[0]['sku'], 1);
                $edi_data['estimate_time'] = $shipppingfee['estimate_time'];
                $edi_data['custom_estimate_time'] = $shipppingfee['custom_estimate_time'];
            }
            //end
            //Update the is_processed = 1 if it is 0 in table bridge_incomingchannel_product_info  after sending ediInfo
            if ($processdata[0]['is_processed'] == "0" || $processdata[0]['is_processed'] == "-1") {
                $update_Is_processed = array("is_processed" => 1);
                $where = "sku = '" . $sku . "' AND channel_id = " . $channel_id . " AND channel_type ='outgoing' ";
                $writeConnection->update($productinfo_table_name, $update_Is_processed, $where);
            }
            //end
            //Update the is_processed = 4 if it is 3 in table bridge_incomingchannel_product_info  after sending ediInfo
            if ($processdata[0]['is_processed'] == "3") {
                $update_Is_processed = array("is_processed" => 4);
                $where = "sku = '" . $sku . "' AND channel_id = " . $channel_id . " AND channel_type ='outgoing' ";
                $writeConnection->update($productinfo_table_name, $update_Is_processed, $where);
            }  
            //end
            //Send sku with upper case when to send
            $history[0]['sku'] =  $edi_data['sku'] = strtoupper(trim($edi_data['sku']));
            $history[0]['brand_id'] = $channel_id;
            $history[0]['channel_type'] = 'outgoing';
            $history[0]['event'] = 'synced';
            $history[0]['source'] = 'api';
            $history[0]['status'] = $productstatus;
            //end
            if(!empty($history)){
                $history_table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_history');
                $writeConnection->insertMultiple($history_table, $history);
            }
            return Mage::helper('core')->jsonEncode($edi_data);
        }
    }

    /* public function getEDI($otgchannel, $sku) {
      if ($otgchannel->getProductPricenquantity()) {
      if (!isset($product_ext))
      $product_ext = Mage::getmodel('catalog/product')->loadByAttribute('sku', $this->getRequest()->getParam('sku'));
      if (!$product_ext->isConfigurable())
      $product_info['edi'] = serialize(array('price' => $product_ext->getPrice(), 'stock' => Mage::getModel('cataloginventory/stock_item')->loadByProduct($product_ext)->getData()));
      }
      } */

    /**
     * filter the product by price rule and apply the aciton
     * @param type $otgchannel
     * @param type $product
     * @return the price
     */
    public function applyPriceRule($otgchannel, $product) {
        $base_price = $otgchannel->getPriceSource() != "ogc_price" ? $product[$otgchannel->getPriceSource()] : ($product['ogc_price'] ? $product['ogc_price'] : ($product['price'] ? $product['price'] : $product['msrp']));
        $price_rule = $otgchannel->getMultipriceRule();
        $use_base_price = $price_rule == 'cumulative_price' ? 0 : 1;
        $all_rule = $price_rule == 'cumulative_price' || $price_rule == 'base_price' ? 1 : 0;
        $actions_model = Mage::getmodel('bridge/outgoingchannels_action');
        $actions = $actions_model->getCollection()
                ->addFieldToFilter('channel_id', $otgchannel->getId())
                ->addFieldToSelect(array('type', 'amount', 'conditions_serialized', 'apply'))
                ->getData();
        $last_price = $base_price;
        $price = array();
        foreach ($actions as $action) {
            Mage::unregister('channel_condition');
            $actions_model->setData(array('product_id' => $product->getId(), 'conditions_serialized' => $action['conditions_serialized']));
            Mage::register('channel_condition', $actions_model);
            if (Mage::getmodel('bridge/rule')->getMatchingProductIds()) {
                if ($action['type'] == 'increase' && $action['apply'] == 'by_percent') {
                    if ($all_rule) {
                        if ($use_base_price)
                            $last_price += $base_price * ($action['amount'] / 100);
                        else
                            $last_price +=$last_price * $action['amount'] / 100;
                    }else {
                        $price[] = $base_price + ($base_price * $action['amount'] / 100);
                    }
                } else if ($action['type'] == 'decrease' && $action['apply'] == 'by_percent') {
                    if ($all_rule) {
                        if ($use_base_price)
                            $last_price -= $base_price * ($action['amount'] / 100);
                        else
                            $last_price -=$last_price * $action['amount'] / 100;
                    }else {
                        $price[] = $base_price - ($base_price * $action['amount'] / 100);
                    }
                } else if ($action['type'] == 'increase' && $action['apply'] == 'by_fixed') {
                    if ($all_rule) {
                        $last_price += $action['amount'];
                    } else {
                        $price[] = $base_price + $action['amount'];
                    }
                } else if ($action['type'] == 'decrease' && $action['apply'] == 'by_fixed') {
                    if ($all_rule) {
                        $last_price -= $action['amount'];
                    } else {
                        $price[] = $base_price - $action['amount'];
                    }
                }
            }
        }
        $ret_price = $all_rule ? $last_price : (empty($price) ? $base_price : ($price_rule == 'min_price' ? min($price) : max($price)));
        return $ret_price;
    }

    /**
     * Retrieve image types assigned to product (base, small, thumbnail)
     *
     * @param object $product
     * @param string $imageFile
     * @return array
     */
    protected function _getImageTypesAssignedToProduct($product, $imageFile) {
        $types = array();
        foreach ($product->getMediaAttributes() as $attribute) {
            if ($product->getData($attribute->getAttributeCode()) == $imageFile) {
                $types[] = $attribute->getAttributeCode();
            }
        }
        return $types;
    }

    /**
     * validate the current request if v
     * valid the return the channel details other wise return false
     */
    private function validateRequest() {
        $consumer_key = Mage::helper('bridge')->getOauthConsumerKey(Mage::app()->getRequest()->getHeader('Authorization'));
//get the consumer id of parameter secret
        $consume_id = Mage::getModel('oauth/consumer')->load($consumer_key, 'key')->getId();
//get the outgoing channel of the given consumer id     
        $otgchannel = Mage::getModel('bridge/outgoingchannels')->getChannelDetails($consume_id);
        $url = parse_url($otgchannel->getUrl());
        $callbackurl = parse_url($otgchannel->getCallbackUrl());
        //if ($otgchannel->getId() && $url['host'] == $callbackurl['host'] && $otgchannel->getChannelStatus()) {
        if ($otgchannel->getId() && $otgchannel->getChannelStatus()) {
            return $otgchannel;
        }

        return false;
    }

    /**
     * get sku list by product id
     * @param type $entity_ids
     * @return type array of sku
     */
    private function getSkuById($entity_ids) {
        $product_skus = Mage::getmodel('catalog/product')->getCollection()
                        ->addAttributeToFilter('entity_id', array('in' => $entity_ids))
                        //->addAttributeToFilter('type_id', array('in' => array('simple', 'configurable')))
                        ->addAttributeToFilter('type_id', 'simple')
                        ->addAttributeToSelect('sku')->getData();
        return array_column($product_skus, 'sku', 'entity_id');
    }

    function getImageInformations($product_id) {
        $product = Mage::getModel('catalog/product')->load($product_id);
        $result = $temp = array();
        $img_u_arr = array_reverse(explode('/', $product->getImageUrl()));
        $defaultimg = $img_u_arr[2] . '/' . $img_u_arr[1] . '/' . $img_u_arr[0];
        $image_arr = $product->getMediaGalleryImages();
        foreach ($image_arr as $image) {
            $match_u = $image->getUrl();
            $match_u_arr = array_reverse(explode('/', $match_u));
            $match_img_u_str = $match_u_arr[2] . '/' . $match_u_arr[1] . '/' . $match_u_arr[0];
            $imginfo = array(
                'id' => $image['value_id'],
                'label' => $image['label'],
                'position' => $image['position'],
                'exclude' => $image['disabled'],
                'url' => Mage::getSingleton('catalog/product_media_config')->getMediaUrl($image['file']),
                'types' => $this->_getImageTypesAssignedToProduct($product, $image['file'])
            );
            if ($defaultimg == $match_img_u_str) {
                $temp[0] = $imginfo;
                foreach ($result as $k => $v) {
                    $temp[] = $v;
                }
                $result = $temp;
            } else {
                $result[] = $imginfo;
            }
        }
        unset($temp);
        return $result;
    }

    //Product_status for sending api 
    public function getEdiStatus($status_flag = 1) {
        $status_arr = array("-1" => "new", "0" => "changed", "1" => "synced", "2" => "processing", "3" => "noaccess", "4" => "trashed" , "-2" => "new_outofstock");
        return $status_arr[$status_flag];
    }

}

?>
