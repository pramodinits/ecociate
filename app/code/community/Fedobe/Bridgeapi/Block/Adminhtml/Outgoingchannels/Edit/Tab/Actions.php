<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * description
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Fedobe_Bridgeapi_Block_Adminhtml_Outgoingchannels_Edit_Tab_Actions extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel() {
        return Mage::helper('bridge')->__('Actions');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle() {
        return Mage::helper('bridge')->__('Actions');
    }

   /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab() {
        if (Mage::getmodel('bridge/outgoingchannels')->isAdminstrator(Mage::getSingleton('admin/Session')->getUser()->getUserId()) && $this->getRequest()->getParam('id'))
            return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden() {
        if (!Mage::getmodel('bridge/outgoingchannels')->isAdminstrator(Mage::getSingleton('admin/Session')->getUser()->getUserId()) || !@$this->getRequest()->getParam('id'))
            return false;
    }

    protected function _prepareForm() {
        $channel_data = Mage::registry('incomingchannel_data');
        $model = Mage::getModel('bridge/rule');
        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('rule_');

//        $fieldset = $form->addFieldset('action_fieldset', array(
//            'legend' => Mage::helper('bridge')->__('Update Meta title and Meta description Using the Following Information')
//                )
//        );
//        $fieldset->addField('meta_title', 'text', array(
//            'name' => 'meta_title',
//            'label' => Mage::helper('bridge')->__('Meta Title'),
//        ));
//        $fieldset->addField('meta_description', 'text', array(
//            'name' => 'meta_description',
//            'label' => Mage::helper('bridge')->__('Meta Description'),
//        ));
        if ($this->getRequest()->getParam('id')) {
            $renderer = Mage::getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
                    ->setTemplate('fedobe/bridgeapi/fieldset.phtml')
                    ->setNewChildUrl($this->getUrl('*/promo_quote/newActionHtml/form/rule_actions_fieldset'));

            $fieldset = $form->addFieldset('actions_fieldset', array(
                        'legend' => Mage::helper('bridge')->__('Update Prices description Using the Following Information')
                    ))->setRenderer($renderer);


            $fieldset->addField('conditions', 'text', array(
                'name' => 'conditions',
                'label' => Mage::helper('bridge')->__('Conditions'),
                'title' => Mage::helper('bridge')->__('Conditions'),
                'required' => true,
            ))->setRule($model)->setRenderer(Mage::getBlockSingleton('rule/conditions'));
        }
//        $form->setValues($channel_data->getData());
        $this->setForm($form);

        return $this;
    }

}
