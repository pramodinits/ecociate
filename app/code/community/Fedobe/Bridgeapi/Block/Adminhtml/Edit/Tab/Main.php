<?php

class Fedobe_Bridgeapi_Block_Adminhtml_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel() {
        return Mage::helper('bridge')->__('Settings');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle() {
        return Mage::helper('bridge')->__('Settings');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab() {
        if ($this->getRequest()->getParam('id') || @$this->getRequest()->getParam('channel_type'))
            return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden() {
        if (!@$this->getRequest()->getParam('id') && !@$this->getRequest()->getParam('channel_type'))
            return true;
    }

    protected function _prepareForm() {
//        $model = Mage::registry('current_consumer');
        $channel_condition = @Mage::registry('channel_condition') ? Mage::registry('channel_condition') : Mage::getmodel('bridge/outgoingchannels');
        $session_security_salt=$channel_condition->getId()?"{$channel_condition->getId()}_outgoing_security_salt":"outgoing_security_salt";
//        $consumer_data = @Mage::registry('current_consumer') ? Mage::registry('current_consumer') : Mage::getModel("admin/user");
        $default_settings = @Mage::registry('default_settings') ? Mage::registry('default_settings') : array();
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('outgoingchannel_');
        $is_adminstrator = Mage::getmodel('bridge/outgoingchannels')->isAdminstrator(Mage::getSingleton('admin/Session')->getUser()->getUserId());
//        $relative_products = $model->getRelativeProduct();

        $fieldset1 = $form->addFieldset('main_fieldset1', array(
            'legend' => Mage::helper('bridge')->__('Channel Settings')
                )
        );
        $fieldset1->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
        $fieldset1->addField('General_Informations', 'hbar', array(
            'id' => 'General Informations'
        ));
        $fieldset1->addField('name', 'text', array(
            'name' => 'consumer_name',
            'label' => Mage::helper('bridge')->__('Name'),
            'title' => Mage::helper('bridge')->__('Name'),
            'required' => true,
            'value' => $channel_condition->getName(),
        ));
        $fieldset1->addField('url', 'text', array(
            'name' => 'url',
            'label' => Mage::helper('bridge')->__('URL'),
            'title' => Mage::helper('bridge')->__('URL'),
            'required' => true,
            'class' => 'validate-url unique-url',
            'value' => $channel_condition->getUrl(),
        ));

        if ($is_adminstrator) {
            $fieldset1->addField('channel_status', 'select', array(
                'name' => 'channel_status',
                'label' => Mage::helper('bridge')->__('Status'),
                'title' => Mage::helper('bridge')->__('Status'),
                'options' => array(1 => Mage::helper('bridge')->__('Active'), 0 => Mage::helper('bridge')->__('Inactive')),
                'required' => true,
                'value' => $channel_condition->getChannelStatus()
            ));
        }

        $fieldset1->addField('Channel_Informations', 'hbar', array(
            'id' => 'Channel Informations'
        ));
        $fieldset1->addField('', 'select', array(
            'name' => '',
            'label' => Mage::helper('bridge')->__('Channel Type'),
            'title' => Mage::helper('bridge')->__('Channel Type'),
            'disabled' => true,
            'options' => $this->getRequest()->getParam('channel_type') ? array(uc_words($this->getRequest()->getParam('channel_type'))) : array(uc_words($channel_condition->getChannelType())),
        ));

        if ($channel_condition->getId()) {
            $fieldset1->addField('id', 'hidden', array('name' => 'id', 'value' => $channel_condition->getId()));
        }

        if (@$this->getRequest()->getParam('channel_type')) {
            $fieldset1->addField('channel_type', 'hidden', array('name' => 'channel_type', 'value' => $this->getRequest()->getParam('channel_type')));
        }



        $fieldset1->addField('key', 'text', array(
            'name' => 'key',
            'label' => Mage::helper('bridge')->__('Key'),
            'title' => Mage::helper('bridge')->__('Key'),
            'disabled' => true,
            'required' => true,
            'value' => $channel_condition->getKey(),
        ));
        $fieldset1->addField('secret', 'text', array(
            'name' => 'secret',
            'label' => Mage::helper('bridge')->__('Secret'),
            'title' => Mage::helper('bridge')->__('Secret'),
            'disabled' => true,
            'required' => true,
            'value' => $channel_condition->getSecret(),
        ));
        
        //if admin is logged in then get list of retailer
        if ($is_adminstrator) {
            $fieldset2 = $form->addFieldset('main_fieldset2', array(
                'legend' => Mage::helper('bridge')->__('PIM Settings')
                    )
            );
            $fieldset2->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
            $fieldset2->addField('sync_settings', 'hbar', array(
                'id' => 'Sync Settings'
            ));
            $product_info = $fieldset2->addField('product_info', 'select', array(
                'name' => 'product_info',
                'label' => Mage::helper('bridge')->__('Sync Product Information'),
                'title' => Mage::helper('bridge')->__('Sync Product Information'),
                'options' => array(1 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'required' => true,
                'value' => !$channel_condition->getId() ? @['ogc_sync_pim'] : $channel_condition->getProductInfo()
            ));
    
            $fieldset2->addField('basic_settings', 'hbar', array(
                'id' => 'Basic Settings'
            ));
            $fieldset2->addField('content_source', 'select', array(
                'name' => 'content_source',
                'label' => Mage::helper('bridge')->__('Product Content Source'),
                'title' => Mage::helper('bridge')->__('Product Content Source'),
                'options' => array('contentrule' => Mage::helper('bridge')->__('Rule Based Content Only'),'copy' => Mage::helper('bridge')->__('Outgoing Content Only'), 'original' => Mage::helper('bridge')->__('Original Content Only'), "contentrule_or_copy" => Mage::helper('bridge')->__('Rule Based else Outgoing Content'), "copy_or_contentrule" => Mage::helper('bridge')->__('Outgoing else Rule Based Content')),
                'required' => true,
                'value' => !$channel_condition->getId() ? @$default_settings['ogc_content_source'] : $channel_condition->getContentSource()
            ));
            $fieldset2->addField('filter_by', 'select', array(
                'name' => 'filter_by',
                'label' => Mage::helper('bridge')->__('Product Selection Based On'),
                'title' => Mage::helper('bridge')->__('Product Selection Based On'),
                'required' => true,
                'options' => array('condition' => Mage::helper('bridge')->__('Condition'), 'sku' => Mage::helper('bridge')->__('SKU')),
                'value' => !$channel_condition->getId() ? @$default_settings['ogc_filter_by'] : $channel_condition->getFilterBy(),
            ));
            $fieldset3 = $form->addFieldset('main_fieldset3', array(
                'legend' => Mage::helper('bridge')->__('EDI Settings')
                    )
            );
            $fieldset3->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
            $fieldset3->addField('sync_settings_', 'hbar', array(
                'id' => 'Sync Settings'
            ));
            $sync_price = $fieldset3->addField('product_pricenquantity', 'select', array(
                'name' => 'product_pricenquantity',
                'label' => Mage::helper('bridge')->__('Sync Prices & Quantity'),
                'title' => Mage::helper('bridge')->__('Sync Prices & Quantity'),
                'options' => array(1 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'required' => true,
                'value' => !$channel_condition->getId() ? @$default_settings['ogc_sync_pricenquantity'] : $channel_condition->getProductPricenquantity()
            ));
            
            $fieldset3->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
            $fieldset3->addField('basic_settings_', 'hbar', array(
                'id' => 'Basic Settings'
            ));
            $bestprice = $fieldset3->addField('price_source', 'select', array(
                'name' => 'price_source',
                'label' => Mage::helper('bridge')->__('Base Price'),
                'title' => Mage::helper('bridge')->__('Base Price'),
                'options' => array('ogc_price' => Mage::helper('bridge')->__('Wholesale Price'), 'price' => Mage::helper('bridge')->__('Selling Price'), 'msrp' => Mage::helper('bridge')->__('MSRP')),
                'value' => !$channel_condition->getId() ? @$default_settings['ogc_price_source'] : $channel_condition->getPriceSource(),
                'required' => true,
            ));
            $multipricerule = $fieldset3->addField('multiprice_rule', 'select', array(
                'name' => 'multiprice_rule',
                'label' => Mage::helper('bridge')->__('Multiple Pricing Rules'),
                'title' => Mage::helper('bridge')->__('Multiple Pricing Rules'),
                'options' => array('max_price' => Mage::helper('bridge')->__('Take Max Price Only'), 'min_price' => Mage::helper('bridge')->__('Take Min Price Only'), 'base_price' => Mage::helper('bridge')->__('Apply Multiple on Base Price'), 'cumulative_price' => Mage::helper('bridge')->__('Apply Multiple on Cumulative Price')),
                'value' => !$channel_condition->getId() ? @$default_settings['ogc_multiprice_rule'] : $channel_condition->getMultipriceRule(),
                'required' => true,
            ));


            $fieldset4 = $form->addFieldset('main_fieldset4', array(
                'legend' => Mage::helper('bridge')->__('User Settings')
                    )
            );
            $fieldset4->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
            $fieldset4->addField('User_Informations', 'hbar', array(
                'id' => 'User Informations'
            ));

            $retiler_list = Mage::getmodel('bridge/outgoingchannels')->getRetailers();
            $retailer = $fieldset4->addField('admin_user_id', 'select', array(
                'name' => 'admin_user_id',
                'label' => Mage::helper('adminhtml')->__('User'),
                'id' => 'admin_user_id',
                'title' => Mage::helper('adminhtml')->__('User'),
                'options' => $retiler_list,
                'onchange' => 'getUserData();',
                'value' => $channel_condition->getAdminUserId()
            ));
            $fieldset4->addField('username', 'text', array(
                'name' => 'username',
                'label' => Mage::helper('adminhtml')->__('User Name'),
                'id' => 'username',
                'title' => Mage::helper('adminhtml')->__('User Name'),
                'class' => 'unique_username',
                'required' => true,
                'required' => true,
                'value' => $channel_condition->getUsername()
            ));

            $fieldset4->addField('firstname', 'text', array(
                'name' => 'firstname',
                'label' => Mage::helper('adminhtml')->__('First Name'),
                'id' => 'firstname',
                'title' => Mage::helper('adminhtml')->__('First Name'),
                'required' => true,
                'value' => $channel_condition->getFirstname()
            ));

            $fieldset4->addField('lastname', 'text', array(
                'name' => 'lastname',
                'label' => Mage::helper('adminhtml')->__('Last Name'),
                'id' => 'lastname',
                'title' => Mage::helper('adminhtml')->__('Last Name'),
                'required' => true,
                'value' => $channel_condition->getLastname()
            ));

            $fieldset4->addField('email', 'text', array(
                'name' => 'email',
                'label' => Mage::helper('adminhtml')->__('Email'),
                'id' => 'customer_email',
                'title' => Mage::helper('adminhtml')->__('User Email'),
                'class' => 'required-entry validate-email unique_email',
                'required' => true,
                'value' => $channel_condition->getEmail()
            ));
            $fieldset4->addField('password', 'password', array(
                'name' => 'password',
                'label' => Mage::helper('adminhtml')->__('Password'),
                'id' => 'customer_pass',
                'title' => Mage::helper('adminhtml')->__('Password'),
                'class' => 'input-text required-entry validate-admin-password',
                'required' => true,
            ));
            $fieldset4->addField('confirmation', 'password', array(
                'name' => 'password_confirmation',
                'label' => Mage::helper('adminhtml')->__('Password Confirmation'),
                'id' => 'confirmation',
                'title' => Mage::helper('adminhtml')->__('Password Confirmation'),
                'class' => 'input-text required-entry validate-cpassword',
                'required' => true,
            ));

            if ($is_adminstrator) {
                $fieldset4->addField('Access_Settings', 'hbar', array(
                    'id' => 'Access Settings'
                ));
                $fieldset4->addField('allow_condition', 'select', array(
                    'name' => 'allow_condition',
                    'label' => Mage::helper('bridge')->__('Conditions Tab Settings'),
                    'title' => Mage::helper('bridge')->__('Conditions Tab Settings'),
                    'options' => array('not_visible' => Mage::helper('bridge')->__('Not Visible'), 'view_only' => Mage::helper('bridge')->__('View Only'), 'allow_editing' => Mage::helper('bridge')->__('Allow Editing')),
                    'required' => true,
                    'value' => !$channel_condition->getId() ? @$default_settings['ogc_allow_conditiontab'] : $channel_condition->getAllowCondition()
                ));
            }
        }
//        $form->setFieldsetRenderer($fieldset1);
        $this->setForm($form);
        if ($is_adminstrator) {
            unset($retiler_list[0]);
            $retailer_values = array_map('strval', array_keys($retiler_list));
            $this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
                            ->addFieldMap($product_info->getHtmlId(), $product_info->getName())
                            /*->addFieldMap($relative_product->getHtmlId(), $relative_product->getName())
                            ->addFieldMap($up_sell->getHtmlId(), $up_sell->getName())
                            ->addFieldMap($cross_sell->getHtmlId(), $cross_sell->getName())*/
                    
                            /*->addFieldMap($sync_price->getHtmlId(), $sync_price->getName())
                            ->addFieldMap($bestprice->getHtmlId(), $bestprice->getName())
                            ->addFieldMap($multipricerule->getHtmlId(), $multipricerule->getName())*/
                    
                            ->addFieldMap($retailer->getHtmlId(), $retailer->getName())
                           /* ->addFieldDependence(
                                    $relative_product->getName(), $product_info->getName(), 1
                            )
                            ->addFieldDependence(
                                    $up_sell->getName(), $product_info->getName(), 1
                            )
                            ->addFieldDependence(
                                    $cross_sell->getName(), $product_info->getName(), 1
                            )*/
                            /*->addFieldDependence(
                                    $bestprice->getName(), $sync_price->getName(), 1
                            )
                            ->addFieldDependence(
                                    $multipricerule->getName(), $sync_price->getName(), 1
                            )*/
            );
        }


        return parent::_prepareForm();
    }

    /**
     * check the logged in user is retailer or not
     */
    public function isRetailer() {
        return Mage::getmodel('bridge/outgoingchannels')->isRetailer(Mage::getSingleton('admin/session')->getUser()->getId());
    }

}
