<?php

class Fedobe_Bridgeapi_Block_Adminhtml_Form_Field_Keygenerator extends Mage_Adminhtml_Block_System_Config_Form_Field {

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        return parent::_getElementHtml($element).$this->getKeygenerateButton($element);
    }
    public function getKeygenerateButton($element){
        $elementid = (string)$element->getHtmlId();
        $label = trim($element->getData('label'));
        $keystring = "<input type='button' class='form-button' onclick='return genratenewkey(\"$elementid\");' value='Generate New Key'>";
        return $keystring;
    }
}
