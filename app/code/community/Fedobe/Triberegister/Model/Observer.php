<?php

class Fedobe_Triberegister_Model_Observer {
    public function addButtonRegister($observer){
    $container = $observer->getBlock();
    $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                        ->load('Tribe Register', 'attribute_set_name')
                        ->getAttributeSetId();
   // echo $attributeSetId;exit;
    if(null !== $container && $container->getType() == 'triberegister/adminhtml_register') {
        $data_2 = array(
            'label'     => 'Add Register',
            'class'=>'add',
            'onclick'   => 'setLocation(\' '  . Mage::helper('adminhtml')->getUrl('adminhtml/tribe_register/new/set/'.$attributeSetId, array('_secure' => true)) . '\')',
        );
        $container->addButton('addregister', $data_2);
    }
    return $this;
}

}
