<?php

require_once Mage::getBaseDir('code') . DS . 'community/Fedobe/Arccore/controllers/Adminhtml/ProfilesController.php';

class Fedobe_Triberegister_RegisterController extends Fedobe_Arccore_Adminhtml_ProfilesController {

    protected $_entityTypeId;

    public function preDispatch() {
        //parent::preDispatch();
        $this->_entityTypeId = Mage::helper('arccore')->getEntityTypeId('arccore');
        $this->_attributesetId = Mage::helper('triberegister')->getAttributeSetId();
        $bypassaction = array('save');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }
    
    public function saveAction() {
        $datas = $this->getRequest()->getPost('arc');
        //code to send mail      
        $mail_present = '<br/> Email ID : ' . $datas["trrg_email_id"];
        $prof_description = $datas["trrg_desc"] ? '<br/> Profile Description : ' . $datas["trrg_desc"] : "";
        $body = 'Following are the details:<br /><br/>  I am a : ' . $datas["trrg_entity"] .
                '<br/> Entity Name : ' . $datas["trrg_entity_name"] . '<br/> Contact Person Name : ' . $datas["trrg_contact_name"] .
                '<br/> Contact No : ' . $datas["trrg_contact_no"] . $mail_present . '<br/> Address : ' . $datas["trrg_address"] . $prof_description . 
                '<br/><br/> Kind Regards,<br/>EarthynGreen Team (<a href="mailto:info@earthyngreen.com">info@earthyngreen.com</a>)<br/><br/><a href="http://www.earthyngreen.com/">www.earthyngreen.com</a>';
        $name = 'Ecociate';
        $subject = "Welcome to EarthynGreen";
        require('email/class.phpmailer.php');
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = TRUE;
        $mail->SMTPSecure = "tls";
        $mail->Port = SMTPPORT;
        $mail->Username = SMTPUSERNAME;
        $mail->Password = SMTPPASSWORD;
        $mail->Host = SMTPHOST;
        $mail->Mailer = "smtp";
        $mail->From = SMTPFROMEMAIL;
        $mail->FromName = SMTPFROMNAME;
        $mail->AddAddress(SMTPTOEMAIL);
        $mail->Subject = $subject;
        $mail->WordWrap = 80;
        $msg = "Dear admin, <br/><br/>Someone has been registered from your site. ";
        $mail->MsgHTML($msg . $body);
        $mail->IsHTML(true);
        if (!$mail->Send()) {
            $res['haserror'] = true;
            $res['resultmail'] = $mail->ErrorInfo;
        } else {
            $res['haserror'] = false;
        }
        //user mail start
        $mailuser = new PHPMailer();
        $mailuser->IsSMTP();
        $mailuser->SMTPDebug = 0;
        $mailuser->SMTPAuth = TRUE;
        $mailuser->SMTPSecure = "tls";
        $mailuser->Port = SMTPPORT;
        $mailuser->Username = SMTPUSERNAME;
        $mailuser->Password = SMTPPASSWORD;
        $mailuser->Host = SMTPHOST;
        $mailuser->Mailer = "smtp";
        $mailuser->From = SMTPFROMEMAIL;
        $mailuser->FromName = SMTPFROMNAME;
        $mailuser->AddAddress($datas['trrg_email_id']);
        $mailuser->Subject = $subject;
        $mailuser->WordWrap = 80;
        $message = "Dear " . $datas["trrg_contact_name"] . ", <br/><br/>Welcome to EarthynGreen. Thank you for taking a step towards being a responsible consumer. ";
        $mailuser->MsgHTML($message . $body);
        $mailuser->IsHTML(true);
        $mailuser->Send();
        echo json_encode($res);
        exit();
    }

    public function saveActionPhoto() {
        $datas = $this->getRequest()->getPost('arc');
        $imagefile = $_FILES['trcr_profile_pic']['name'];
        //save image
        if ($imagefile) {
            $target_dir = "uploads/";
            $target_file = $target_dir . basename($_FILES["trcr_profile_pic"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
            // Check if image file is a actual image or fake image
            $check = getimagesize($_FILES["trcr_profile_pic"]["tmp_name"]);
            if ($check !== false) {
//                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
//                echo "File is not an image.";
                $uploadOk = 0;
            }
            // Allow certain file formats
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
//            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
            } else {
                if (move_uploaded_file($_FILES["trcr_profile_pic"]["tmp_name"], $target_file)) {
                    echo "The file " . basename($_FILES["trcr_profile_pic"]["name"]) . " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
        }
        //code to send mail         
        $mail_present = '<br/> Email ID : ' . $datas["trrg_email_id"];
        $prof_description = $datas["trrg_desc"] ? '<br/> Profile Description : ' . $datas["trrg_desc"] : "";
        $body = 'Following are the details:<br /><br/>  I am a : ' . $datas["trrg_entity"] .
                '<br/> Entity Name : ' . $datas["trrg_entity_name"] . '<br/> Contact Person Name : ' . $datas["trrg_contact_name"] .
                '<br/> Contact No : ' . $datas["trrg_contact_no"] . $mail_present . '<br/> Address : ' . $datas["trrg_address"] . $prof_description . '<br/><br/> Regards, <br/> Ecociate';
        $name = 'Ecociate';
        $subject = "Registration Request";
        require('email/class.phpmailer.php');
        //$toemail = "sahoo.purusottama@gmail.com";//Mage::getStoreConfig('tribecoresettings/general_settings/admin_email_register');
        //admin mail start
//            $toemail = "info@earthyngreen.com"; //admin mail
        $toemail = "pramodinidas100@gmail.com";
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = TRUE;
        $mail->SMTPSecure = "tls";
        $mail->Port = SMTPPORT;
        $mail->Username = SMTPUSERNAME;
        $mail->Password = SMTPPASSWORD;
        $mail->Host = SMTPHOST;
        $mail->Mailer = "smtp";
        $mail->From = SMTPFROMEMAIL;
        $mail->FromName = SMTPFROMNAME;
        $mail->AddAddress(SMTPTOEMAIL);
        $mail->Subject = $subject;
        $mail->WordWrap = 80;
        $msg = "Hi admin, <br/><br/>Someone has been registered from your site. ";
        $mail->MsgHTML($msg . $body);
        if ($imagefile) {
            $mail->addAttachment("uploads/" . $imagefile);
        }
        $mail->IsHTML(true);
        $mail->Send();

        //user mail start
        $mailuser = new PHPMailer();
        $mailuser->IsSMTP();
        $mailuser->SMTPDebug = 0;
        $mailuser->SMTPAuth = TRUE;
        $mailuser->SMTPSecure = "tls";
        $mailuser->Port = SMTPPORT;
        $mailuser->Username = SMTPUSERNAME;
        $mailuser->Password = SMTPPASSWORD;
        $mailuser->Host = SMTPHOST;
        $mailuser->Mailer = "smtp";
        $mailuser->From = SMTPFROMEMAIL;
        $mailuser->FromName = SMTPFROMNAME;
        $mailuser->AddAddress($datas['trrg_email_id']);
        $mailuser->Subject = $subject;
        $mailuser->WordWrap = 80;
        $message = "Hi " . $datas["trrg_contact_name"] . ", <br/><br/>Your registration was successful. ";
        $mailuser->MsgHTML($message . $body);
        if ($imagefile) {
            $mailuser->addAttachment("uploads/" . $imagefile);
        }
        $mailuser->IsHTML(true);
        $mailuser->Send();

        parent::saveAction();
        $session = Mage::getSingleton('customer/session');
        $session->addSuccess($this->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
        $url = Mage::getBaseUrl()."#ecoregister";
        $this->_redirectSuccess($url);
        return $this;

       // $this->_redirect("#ecoregister");
//        $redirectUrl = Mage::getBaseUrl()."#ecoregister";
//        header("Location: ".$redirectUrl."");
        die();
//                
//            if ($mail->Send()) {
//                Mage::getSingleton('customer/session')->addSuccess(Mage::helper('contacts')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
//                $this->_redirect("");
//            }
    }

    public function saveActionOld() {
        $datas = $this->getRequest()->getPost();
        $iam = $datas['iam'];
        $datas = $datas['arc'];
        if ($datas['trrg_entity'] && $datas['trrg_entity_name'] && $datas['trrg_contact_name'] && $datas['trrg_contact_no'] && $datas['trrg_address']) {
            //code to send mail         
//    $body ='Hi admin, <br/><br/>Someone has been registered from your site.Following are the details:<br /><br/>  I am a : '.$iam.'<br/> Entity Name : '.$datas["trrg_entity_name"].'<br/> Contact Person Name : '.$datas["trrg_contact_name"].'<br/> Contact No : '.$datas["trrg_contact_no"].'<br/> Email ID : '.$datas["trrg_email_id"].'<br/> Address : '.$datas["trrg_address"].'<br/> Profile Description : '.$datas["trrg_desc"].' ';
            $mail_present = $datas["trrg_email_id"] ? '<br/> Email ID : ' . $datas["trrg_email_id"] : "";
            $prof_description = $datas["trrg_desc"] ? '<br/> Profile Description : ' . $datas["trrg_desc"] : "";
            $body = 'Hi admin, <br/><br/>Someone has been registered from your site.Following are the details:<br /><br/>  I am a : ' . $iam . '<br/> Entity Name : ' . $datas["trrg_entity_name"] . '<br/> Contact Person Name : ' . $datas["trrg_contact_name"] . '<br/> Contact No : ' . $datas["trrg_contact_no"] . $mail_present . '<br/> Address : ' . $datas["trrg_address"] . $prof_description . ' ';
            $name = 'Ecociate';
            $mail = Mage::getModel('core/email');
            $mail->setToName($name);
            $mail->setToEmail(Mage::getStoreConfig('tribecoresettings/general_settings/admin_email_register'));
//    $mail->setToEmail('bhagyashree@fedobe.com');
            $mail->setBody($body);
            $mail->setSubject('Registration Request');
            $mail->setFromEmail('ecociate.com');
            $mail->setFromName($name);
            $mail->setType('html'); // You can use 'html' or 'text'

            try {
                $mail->send();
            } catch (Exception $e) {
                
            }
            parent::saveAction();
            //ends here           
            //echo 1;exit;
        } else {
            //if any of the mandetory field is missing
            //code to send mail         
            echo 2;
        }
    }

    public function saveActionMail() {
        require('email/class.phpmailer.php');
        $mail = new PHPMailer();
        $subject = "SMTP TEST EMAIL FROM MAGENTO 1.9";
        $content = "<b>SMTP TEST EMAIL FROM MAGENTO 1.9 LOCAL HOST</b>";
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = TRUE;
        $mail->SMTPSecure = "tls";
        $mail->Port = 587;
        $mail->Username = "infoilift2018@gmail.com";
        $mail->Password = "infoilift18";
        $mail->Host = "smtp.googlemail.com";
        $mail->Mailer = "smtp";
        $mail->SetFrom("purusottama@thougtspheres.com", "Purusottama");
        $mail->AddReplyTo("purusottama@thougtspheres.com", "Purusottama");
        //$mail->AddReplyTo("vincy@phppot.com", "PHPPot");
        $mail->AddAddress("sahoo.purusottama@gmail.com");
        $mail->Subject = $subject;
        $mail->WordWrap = 80;
        $mail->MsgHTML($content);
        $mail->IsHTML(true);

        if (!$mail->Send())
            echo "Problem on sending mail";
        else
            echo "Mail sent";
    }

}

?>