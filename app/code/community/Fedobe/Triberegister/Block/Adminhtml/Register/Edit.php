<?php

/**
 * Customer attribute edit block
 * 
 * @category    Fedobe
 * @package     Fedobe_Triberegister
 * @author      Fedobe Magento Team
 */
class Fedobe_Triberegister_Block_Adminhtml_Register_Edit extends Fedobe_Arccore_Block_Adminhtml_Arc_Edit {

    public function __construct() {
        parent::__construct();
        $this->_controller = 'tribe_register';
        $this->_blockGroup = 'triberegister';
    }
}
