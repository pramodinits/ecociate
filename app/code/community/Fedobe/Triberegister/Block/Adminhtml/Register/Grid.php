<?php

/**
 * Manage Customer Attribute grid block
 * 
 * @category    Fedobe
 * @package     Fedobe_Triberegister
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Triberegister_Block_Adminhtml_Register_Grid extends Fedobe_Arccore_Block_Adminhtml_Arccore_Grid {

    public function __construct() {
        parent::__construct();
        $this->setAttributeSetId(Mage::helper('triberegister')->getAttributeSetId());
    }

    /**
     * Prepare customer attributes grid collection object
     *
     * @return Fedobe_Triberegister_Block_Adminhtml_Photo_Grid
     */
    protected function _prepareCollection() {
        $collection = Mage::getModel('arccore/arc')->getCollection()->addAttributeToSelect('*');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('entity_id', array(
            'header' => Mage::helper('triberegister')->__('ID'),
            'width' => '50px',
            'type' => 'number',
            'index' => 'entity_id',
        ));

       

        $attribute = Mage::getModel('eav/config')->getAttribute('fedobe_arccore_arc', 'trcr_profile_type');
        $profile_type_attr_id = $attribute->getId();
        $attribute = Mage::getModel('eav/config')->getAttribute('fedobe_arccore_arc', 'trcr_name');
        $trcr_name_attr_id = $attribute->getId();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $arc_int = $resource->getTableName('arccore/arc_entity_int');
        $arc_varchar = $resource->getTableName('arccore/arc_entity_varchar');
        $query = "SELECT entity_id,value FROM `$arc_varchar` WHERE  attribute_id = $trcr_name_attr_id AND entity_id IN (SELECT entity_id FROM $arc_int WHERE `attribute_id` = $profile_type_attr_id)";
        $res = $readConnection->fetchAll($query);
        $finalopt = array();
        foreach ($res as $k => $v) {
            $finalopt[$v['entity_id']] = $v['value'];
        }
        $profileopt = Mage::helper('arccore')->attribute_options('trrg_entity');
        $final_profileopt = array();
        foreach ($profileopt as $k => $v) {
            if ($v['value'])
                $final_profileopt[$v['value']] = $v['label'];
        }
        //$finalopt = array('Organic Brand','Store','Organic Farmer','Producer Organization');
        $this->addColumn('trrg_entity', array(
            'header' => Mage::helper('triberegister')->__('Name'),
            'index' => 'trrg_entity',
            'width' => '100px',
            'type' => 'options',
            'options' => $final_profileopt
        ));
         $this->addColumn('trrg_entity_name', array(
            'header' => Mage::helper('triberegister')->__('Entity Name'),
            'width' => '50px',
            'index' => 'trrg_entity_name',
        ));
         $this->addColumn('trrg_contact_name', array(
            'header' => Mage::helper('triberegister')->__('Contact Name'),
            'width' => '50px',
            'index' => 'trrg_contact_name',
        ));
         $this->addColumn('trrg_contact_no', array(
            'header' => Mage::helper('triberegister')->__('Contact No'),
            'width' => '50px',
            'index' => 'trrg_contact_no',
        ));
         $this->addColumn('trrg_email_id', array(
            'header' => Mage::helper('triberegister')->__('Email ID'),
            'width' => '50px',
            'index' => 'trrg_email_id',
        ));
        $this->addColumn('created_at', array(
            'header' => Mage::helper('triberegister')->__('Created At'),
            'index' => 'created_at',
            'width' => '130px',
        ));


        $attributeSetId = Mage::helper('triberegister')->getAttributeSetId();
        $this->addColumn('action', array(
            'header' => Mage::helper('triberegister')->__('Action'),
            'width' => '50px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('triberegister')->__('Edit'),
                    'url' => array(
                        'base' => '*/*/edit/set/' . $attributeSetId,
                        'params' => array('store' => $this->getRequest()->getParam('store'))
                    ),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
        ));


        return parent::_prepareColumns();
    }

}