<?php
/**
 * Grid container file
 * 
 * @category    Fedobe
 * @package     Fedobe_Triberegister
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Triberegister_Block_Adminhtml_Register extends Mage_Adminhtml_Block_Widget_Grid_Container
{       
  public function __construct()
  {
    /*both these variables tell magento the location of our Grid.php(grid block) file.
     * $this->_blockGroup.'/' . $this->_controller . '_grid'
     * i.e clarion_Customerattribute/adminhtml_customerattribute_grid
     * $_blockGroup - is your module's name.
     * $_controller - is the path to your grid block. 
     */
    
    $this->_controller = 'adminhtml_register';
    $this->_blockGroup = 'triberegister';
    
    $this->_headerText = Mage::helper('triberegister')->__('Manage Register');
    $this->_addButtonLabel = Mage::helper('triberegister')->__('Add New Register');
    parent::__construct();
    $this->_removeButton('add');
  }
}