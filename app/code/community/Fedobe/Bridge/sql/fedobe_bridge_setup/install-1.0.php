<?php

//$installer = $this;
//$installer->startSetup();
//create 4 attributes and add this to bridge group to each attriubte set
$attributeSetIds = Mage::getModel('eav/entity_attribute_set')
                ->getCollection()
                ->addFieldToSelect('attribute_set_id')->getData();

// Set data:
$attributeName_inc = array('Name', 'Description', 'Short Description', 'Channel Priority', 'Product Content  Channel Info', 'Product Price And Quantity  Channel Info', 'Bridge Content Edited'); // Name of the attribute
$attributeCode_inc = array('inc_name', 'inc_description', 'inc_short_description', 'channel_priority', 'bridge_channel_id', 'bridge_pricenqty_channel_id', 'bridge_content_edited'); // Code of the attribute
$attributeName_ogc = array('Name', 'Description', 'Short Description', 'Price'); // Name of the attribute
$attributeCode_ogc = array('ogc_name', 'ogc_description', 'ogc_short_description', 'ogc_price'); // Code of the attribute
$attributeGroup_inc = 'Incoming Channel(Bridge)';          // Group to add the attribute to
$attributeGroup_ogc = 'Outgoing Channel(Bridge)';          // Group to add the attribute to
// Configuration:
$data_inc = array(array(
        'type' => 'text', // Attribute type
        'input' => 'text', // Input type
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE, // Attribute scope
        'required' => false, // Is this attribute required?
        'user_defined' => false,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => false,
        'unique' => false,
        'used_in_product_listing' => true,
        // Filled from above:
        'label' => $attributeName_inc[0]
    ),
    array('type' => 'text', // Attribute type
        'input' => 'textarea', // Input type
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE, // Attribute scope
        'required' => false, // Is this attribute required?
        'user_defined' => false,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => false,
        'unique' => false,
        'used_in_product_listing' => true,
        // Filled from above:
        'label' => $attributeName_inc[1]
    ),
    array(
        'type' => 'text', // Attribute type
        'input' => 'textarea', // Input type
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE, // Attribute scope
        'required' => false, // Is this attribute required?
        'user_defined' => false,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => false,
        'unique' => false,
        'used_in_product_listing' => true,
        // Filled from above:
        'label' => $attributeName_inc[2]
    ),
    array(
        'type' => 'text', // Attribute type
        'input' => 'select', // Input type
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE, // Attribute scope
        'required' => false, // Is this attribute required?
        'user_defined' => false,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => false,
        'unique' => false,
        'used_in_product_listing' => true,
        // Filled from above:
        'label' => $attributeName_inc[3],
        'option' => array(
            'values' => array(
                0 => 'My Store'
            )
        ),
    ),
    array(
        'type' => 'int', // Attribute type
        'input' => 'hidden', // Input type
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE, // Attribute scope
        'required' => false, // Is this attribute required?
        'user_defined' => false,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => false,
        'unique' => false,
        'used_in_product_listing' => true,
        'is_visible' => false,
        // Filled from above:
        'label' => $attributeName_inc[4]
    ),
    array(
        'type' => 'int', // Attribute type
        'input' => 'hidden', // Input type
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL, // Attribute scope
        'required' => false, // Is this attribute required?
        'user_defined' => false,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => false,
        'unique' => false,
        'used_in_product_listing' => true,
        'is_visible' => false,
        // Filled from above:
        'label' => $attributeName_inc[5]
    ),
    array(
        'type' => 'int', // Attribute type
        'input' => 'hidden', // Input type
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE, // Attribute scope
        'required' => false, // Is this attribute required?
        'user_defined' => false,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => false,
        'unique' => false,
        'used_in_product_listing' => true,
        'is_visible' => false,
        // Filled from above:
        'label' => $attributeName_inc[6]
        ));
$data_ogc = array(array(
        'type' => 'text', // Attribute type
        'input' => 'text', // Input type
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE, // Attribute scope
        'required' => false, // Is this attribute required?
        'user_defined' => false,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => false,
        'unique' => false,
        'used_in_product_listing' => true,
        // Filled from above:
        'label' => $attributeName_ogc[0]
    ),
    array('type' => 'text', // Attribute type
        'input' => 'textarea', // Input type
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE, // Attribute scope
        'required' => false, // Is this attribute required?
        'user_defined' => false,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => false,
        'unique' => false,
        'used_in_product_listing' => true,
        // Filled from above:
        'label' => $attributeName_ogc[1]
    ),
    array(
        'type' => 'text', // Attribute type
        'input' => 'textarea', // Input type
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE, // Attribute scope
        'required' => false, // Is this attribute required?
        'user_defined' => false,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => false,
        'unique' => false,
        'used_in_product_listing' => true,
        // Filled from above:
        'label' => $attributeName_ogc[2]
    ),
    array(
        'type' => 'decimal', // Attribute type
        'input' => 'text', // Input type
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL, // Attribute scope
        'required' => false, // Is this attribute required?
        'user_defined' => false,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => false,
        'unique' => false,
        'used_in_product_listing' => true,
        // Filled from above:
        'label' => $attributeName_ogc[3]
        ));

// Create attribute:
// We create a new installer class here so we can also use this snippet in a non-EAV setup script.
$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$installer->startSetup();
//add attribute for incoming channel
foreach ($data_inc as $key => $attribute) {
    $installer->addAttribute('catalog_product', $attributeCode_inc[$key], $attribute);
}

// Add the attribute to the proper sets/groups:
foreach ($attributeSetIds as $attributeSetId) {
    $installer->addAttributeGroup('catalog_product', $attributeSetId['attribute_set_id'], $attributeGroup_inc);
    foreach ($attributeCode_inc as $code) {
        $installer->addAttributeToGroup('catalog_product', $attributeSetId['attribute_set_id'], $attributeGroup_inc, $code);
    }
}
//add attribute for outgoing channel
foreach ($data_ogc as $key => $attribute) {
    $installer->addAttribute('catalog_product', $attributeCode_ogc[$key], $attribute);
}

// Add the attribute to the proper sets/groups:
foreach ($attributeSetIds as $attributeSetId) {
    $installer->addAttributeGroup('catalog_product', $attributeSetId['attribute_set_id'], $attributeGroup_ogc);
    foreach ($attributeCode_ogc as $code) {
        $installer->addAttributeToGroup('catalog_product', $attributeSetId['attribute_set_id'], $attributeGroup_ogc, $code);
    }
}
//end
//add a supplier attribute if not exist

$supplier_id = Mage::getResourceModel('eav/entity_attribute')
        ->getIdByCode('catalog_product', 'suppliers');
if (!$supplier_id) {
    $supplier_dtls = array(
        'type' => 'int', // Attribute type
        'input' => 'select', // Input type
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL, // Attribute scope
        'required' => false, // Is this attribute required?
        'user_defined' => false,
        'searchable' => true,
        'filterable' => true,
        'comparable' => false,
        'visible_on_front' => false,
        'unique' => false,
        'used_in_product_listing' => true,
        // Filled from above:
        'label' => 'Supplier',
	 'source'=>'eav/entity_attribute_source_table'
    );
    $installer->addAttribute('catalog_product', 'suppliers', $supplier_dtls);
    // Add the supplier attribute to the proper sets/groups:
    foreach ($attributeSetIds as $attributeSetId) {
            $installer->addAttributeToGroup('catalog_product', $attributeSetId['attribute_set_id'], 'General', 'suppliers');
    }
}

//end

$cur_time = date('Y-m-d H:i:s');

$installer->run("
    CREATE TABLE IF NOT EXISTS `bridge_syncorders` (
      `id` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
      `supplier_id` INT(11) DEFAULT NULL,
      `item_id` INT(11) DEFAULT NULL,
      `channel_id` INT(11) DEFAULT NULL,
      `channel_type` VARCHAR(255) DEFAULT NULL,
      `order_id` VARCHAR(255) DEFAULT NULL,
      `ack_order_id` VARCHAR(255) DEFAULT NULL,
      `incoming_product_code` VARCHAR(255) DEFAULT NULL,
      `item_quantity` INT(11) DEFAULT NULL,
      `item_price` FLOAT(14,2) DEFAULT 0,
      `order_status` VARCHAR(255) DEFAULT NULL,
      `ack_order_status` VARCHAR(255) DEFAULT NULL,
      `is_processed` TINYINT(4) DEFAULT 0,
      `is_changed` TINYINT(4) DEFAULT 0,
      `is_completed` TINYINT(4) DEFAULT 0,
      `is_cancelled` TINYINT(4) DEFAULT 0,
      `is_confirmed` TINYINT(4) DEFAULT 0,
      `created_at` datetime DEFAULT NULL,
      `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    
   CREATE TABLE IF NOT EXISTS `bridge_attribute_categories` (
   `id` int(11) NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `categories` text,
  `attributes` text,
  `store` text,
  `default_store` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `bridge_incomingchannels` (
`id` int(11) NOT NULL,
  `brand_name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `secret` varchar(255) DEFAULT NULL,
  `multi_price_rule` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `mode` varchar(255) DEFAULT NULL,
  `product_status` int(2) DEFAULT NULL COMMENT '0=>inactive,1=>active,2=>as it in brand',
  `slab` int(4) NOT NULL DEFAULT '0',
  `channel_type` varchar(255) NOT NULL DEFAULT 'magento',
  `weightage` int(11) DEFAULT NULL,
  `content_source` varchar(255) DEFAULT NULL,
  `authorized` tinyint(1) NOT NULL DEFAULT '0',
  `permanent_token` varchar(255) DEFAULT NULL,
  `permanent_secret` varchar(255) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `create_new` tinyint(1) NOT NULL DEFAULT '0',
  `update_exist` tinyint(1) NOT NULL DEFAULT '0',
  `product_info` tinyint(1) NOT NULL DEFAULT '0',
  `product_pricenquantity` tinyint(1) NOT NULL DEFAULT '0',
  `create_option` tinyint(1) NOT NULL DEFAULT '1',
  `relative_product` int(11) NOT NULL DEFAULT '0' COMMENT '1=>relative product,2=>up-sell,4=>cross-sell',
  `salt` varchar(255) DEFAULT NULL,
  `channel_order` int(11) DEFAULT NULL,
  `sync_order` tinyint(1) DEFAULT NULL,
  `pim_cron` varchar(255) DEFAULT NULL,
  `edi_cron` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `others` TEXT DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `bridge_incomingchannels_history` (
`id` int(11) NOT NULL,
  `brand_id` int(11) DEFAULT '0',
  `sku` varchar(255) DEFAULT NULL,
  `event` enum('created','updated','content-updated','price-updated','conflict') DEFAULT NULL,
  `previous_price` double DEFAULT '0',
  `current_price` double DEFAULT '0',
  `previous_quantity` int(11) DEFAULT '0',
  `current_quantity` int(11) DEFAULT '0',
  `store_view` text,
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `bridge_incomingchannel_action` (
 `id` int(11),
 `brand_id` int(11) DEFAULT NULL,
 `type` varchar(255) DEFAULT NULL,
 `apply` varchar(100) DEFAULT NULL,
 `amount` double DEFAULT NULL,
 `custom_price` text,
 `conditions_serialized` text,
 `conditions_html` text,
 `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `bridge_incomingchannel_mapping` (
`id` int(11) NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `data` text,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `bridge_incomingchannel_notifications` (
`id` int(11) NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `product_info` text,
  `created` time DEFAULT NULL,
  `resolved` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `bridge_incomingchannel_product_info` (
`id` int(11) NOT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `channel_id` int(11) DEFAULT '0',
  `quantity` int(11) DEFAULT '0',
  `price` double(10,2) DEFAULT '0.00',
  `special_price` double(10,2) NOT NULL DEFAULT '0.00',
  `slab` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `bridge_incomingchannel_skus` (
`id` int(11) NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `relation` enum('upsell','crosssell','related','associated') DEFAULT NULL,
  `parent_sku` varchar(255) DEFAULT NULL,
  `current_flag` enum('new','update','not_available') DEFAULT NULL,
  `weightage` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `bridge_outgoingchannels` (
`id` int(11) NOT NULL,
  `admin_user_id` int(11) DEFAULT NULL,
  `url` text,
  `consumer_id` int(11) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `channel_type` varchar(255) DEFAULT NULL COMMENT 'channel type e.g,magento,amazon',
  `channel_mode` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'channel mode e.g 0-Demo & 1-Live',
  `conditions_serialized` text,
  `filter_by` varchar(100) DEFAULT NULL,
  `sku_list` text,
  `product_status` varchar(20) DEFAULT NULL,
  `content_source` varchar(255) DEFAULT NULL,
  `price_source` varchar(255) DEFAULT NULL,
  `channel_status` tinyint(1) DEFAULT '0',
  `relative_product` int(11) DEFAULT NULL COMMENT '1=>cross-sells,2=>up-sells,4=>related-products',
  `cond_apply_on_relative_products` tinyint(1) DEFAULT NULL,
  `create_new` tinyint(1) DEFAULT NULL,
  `update_exist` tinyint(1) DEFAULT NULL,
  `product_info` tinyint(1) DEFAULT NULL,
  `product_pricenquantity` tinyint(1) DEFAULT NULL,
  `sync_order` tinyint(1) DEFAULT NULL,
  `allow_condition` varchar(100) DEFAULT NULL,
  `multiprice_rule` varchar(100) DEFAULT NULL,
  `others` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `bridge_outgoingchannel_action` (
`id` int(11) NOT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `apply` varchar(255) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `conditions_serialized` text,
  `conditions_html` text,
  `created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `bridge_storeview_label` (
`id` int(11) NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `type` enum('category','attribute','options','meta_title','meta_description') DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `attribute_id` int(11) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE IF NOT EXISTS `bridge_pim_processing_queue` (
`id` int(11) NOT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `sku` varchar(100) DEFAULT NULL,
  `store` int(11) DEFAULT NULL,
  `is_processed` tinyint(1) DEFAULT NULL,
  `relation_updated` tinyint(1) NOT NULL DEFAULT '0',
  `product_status` int(1) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `bridge_sync_errorlog` (
`id` int(11) NOT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `sku` varchar(100) DEFAULT NULL,
  `message` text,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `bridge_outgoingchannel_skus` (
`id` int(11) NOT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `relation` enum('upsell','crosssell','related','associated') DEFAULT NULL,
  `parent_sku` varchar(255) DEFAULT NULL,
  `is_processed` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `bridge_order_details` (
`id` int(11) NOT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `status` varchar(200) DEFAULT 'new',
  `status_changed_by` enum('supplier','own_store') DEFAULT 'own_store',
  `store_order_id` int(11) DEFAULT NULL,
  `remote_order_id` int(11) DEFAULT NULL,
  `last_updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS bridge_edi_channelcron (
  `id` int(11) unsigned NOT NULL auto_increment,
  `channel_id` int(11) NULL default 0,
  `channel_type` varchar(255) NULL default 0,
  `status` smallint(6) NULL default '0',
  `is_processed` smallint(6) NULL default '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `bridge_order_details`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `bridge_order_details`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bridge_outgoingchannel_skus`
 ADD PRIMARY KEY (`id`);
 
ALTER TABLE `bridge_outgoingchannel_skus`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `bridge_incomingchannel_product_info`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `bridge_incomingchannel_product_info`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `bridge_sync_errorlog`
 ADD PRIMARY KEY (`id`);


ALTER TABLE `bridge_sync_errorlog`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bridge_pim_processing_queue`
 ADD PRIMARY KEY (`id`);
 
 ALTER TABLE `bridge_pim_processing_queue`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bridge_attribute_categories`
 ADD PRIMARY KEY (`id`);


ALTER TABLE `bridge_incomingchannels`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `bridge_incomingchannels_history`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `bridge_incomingchannel_action`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `bridge_incomingchannel_mapping`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `bridge_incomingchannel_notifications`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `bridge_incomingchannel_skus`
 ADD PRIMARY KEY (`id`);


ALTER TABLE `bridge_outgoingchannels`
 ADD PRIMARY KEY (`id`);


ALTER TABLE `bridge_outgoingchannel_action`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `bridge_storeview_label`
 ADD PRIMARY KEY (`id`);


ALTER TABLE `bridge_attribute_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bridge_incomingchannels`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bridge_incomingchannels_history`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bridge_incomingchannel_action`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bridge_incomingchannel_mapping`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bridge_incomingchannel_notifications`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bridge_incomingchannel_skus`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bridge_outgoingchannels`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bridge_outgoingchannel_action`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bridge_storeview_label`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `bridge_outgoingchannels` ADD `name` VARCHAR(255) NULL AFTER `url`;
ALTER TABLE `bridge_outgoingchannel_action` ADD `custom_price` TEXT NULL AFTER `amount`;
ALTER TABLE `bridge_outgoingchannel_action` ADD `content_type` TINYINT NULL AFTER `custom_price`;
ALTER TABLE `bridge_outgoingchannel_action` ADD `name_rule` VARCHAR(255) NULL AFTER `content_type`;
ALTER TABLE `bridge_outgoingchannel_action` ADD `short_desc_rule` TEXT NULL AFTER `name_rule`;
ALTER TABLE `bridge_outgoingchannel_action` ADD `desc_rule` TEXT NULL AFTER `short_desc_rule`;


ALTER TABLE `bridge_incomingchannel_action` ADD `content_type` TINYINT NULL AFTER `custom_price`;
ALTER TABLE `bridge_incomingchannel_action` ADD `name_rule` VARCHAR(255) NULL AFTER `content_type`;
ALTER TABLE `bridge_incomingchannel_action` ADD `short_desc_rule` TEXT NULL AFTER `name_rule`;
ALTER TABLE `bridge_incomingchannel_action` ADD `desc_rule` TEXT NULL AFTER `short_desc_rule`;
ALTER TABLE `bridge_incomingchannel_product_info` ADD `channel_type` VARCHAR(255) NULL AFTER `is_processed`, ADD `converted_price` DOUBLE(10,2) NULL AFTER `channel_type`, ADD `last_updated_time` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP AFTER `converted_price`;
ALTER TABLE `bridge_edi_channelcron` ADD `condition_changed` BOOLEAN NOT NULL DEFAULT TRUE AFTER `is_processed`;
ALTER TABLE `bridge_incomingchannels` ADD `currency` VARCHAR(100) NULL AFTER `edi_cron`;
ALTER TABLE `bridge_incomingchannel_product_info` CHANGE `sku` `sku` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;
ALTER TABLE `bridge_incomingchannel_product_info` ADD `deleted` BOOLEAN NOT NULL DEFAULT FALSE AFTER `converted_price`, ADD `is_live` BOOLEAN NOT NULL DEFAULT FALSE AFTER `deleted`;
ALTER TABLE `bridge_incomingchannel_product_info` CHANGE `special_price` `special_price` DOUBLE NULL;
ALTER TABLE `bridge_incomingchannel_product_info` CHANGE `is_processed` `is_processed` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0=>changed,1=>synced,2=>processing,3=>no access,4=>trashed';
ALTER TABLE `bridge_incomingchannel_product_info` ADD `product_entity_id` INT NULL AFTER `sku`;
ALTER TABLE `bridge_edi_channelcron` ADD `new_added` BOOLEAN NOT NULL DEFAULT FALSE AFTER `condition_changed`;

INSERT INTO admin_role (role_id,parent_id,tree_level,sort_order,role_type,user_id,role_name) VALUES(NULL,0,1,0,'G',0,'Bridge User');


INSERT INTO admin_rule SELECT NULL, role_id, 'admin/fedobe',NULL,0,'G','allow' FROM admin_role WHERE role_name = 'Bridge User';
INSERT INTO admin_rule SELECT NULL, role_id, 'admin/fedobe/bridge',NULL,0,'G','allow' FROM admin_role WHERE role_name = 'Bridge User';
INSERT INTO admin_rule SELECT NULL, role_id, 'admin/fedobe/bridge/incoming',NULL,0,'G','deny' FROM admin_role WHERE role_name = 'Bridge User';    
INSERT INTO admin_rule SELECT NULL, role_id, 'admin/fedobe/bridge/outgoing',NULL,0,'G','allow' FROM admin_role WHERE role_name = 'Bridge User';    
INSERT INTO admin_rule SELECT NULL, role_id, 'admin/fedobe/bridge/edi',NULL,0,'G','deny' FROM admin_role WHERE role_name = 'Bridge User';    
INSERT INTO admin_rule SELECT NULL, role_id, 'admin/promo/quote',NULL,0,'G','allow' FROM admin_role WHERE role_name = 'Bridge User';    
INSERT INTO admin_rule SELECT NULL, role_id, 'admin/promo/quote',NULL,0,'G','allow' FROM admin_role WHERE role_name = 'Bridge User';    
INSERT INTO admin_rule SELECT NULL, role_id, 'admin/promo/catalog',NULL,0,'G','deny' FROM admin_role WHERE role_name = 'Bridge User';    


INSERT INTO api2_acl_role VALUES(NULL,'{$cur_time}','{$cur_time}','Bridge API');


INSERT INTO api2_acl_rule SELECT NULL, entity_id, 'bridge_magento','retrieve' FROM api2_acl_role WHERE role_name = 'Bridge API';
CREATE TABLE `bridge_incominglog`
 ( `id` INT(50) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 `sku` VARCHAR(255) NULL ,
 `channel_type` VARCHAR(255) NULL ,
 `status` VARCHAR(255) NULL ,
 `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
 `updated_at` TIMESTAMP NULL);
 ALTER TABLE `bridge_incominglog` ADD `bridge_channel_id` INT(100) NULL AFTER `sku`;
 ALTER TABLE `bridge_incominglog` ADD `status_code` VARCHAR( 100 ) NULL AFTER `status`;
 ALTER TABLE `bridge_incominglog` ADD UNIQUE (
`sku` ,
`bridge_channel_id` ,
`channel_type` ,
`status_code`
);
ALTER TABLE `bridge_incomingchannels` ADD `channel_supplier` VARCHAR(255) NULL DEFAULT NULL AFTER `mode`;
ALTER TABLE `bridge_outgoingchannels` ADD `channel_supplier` VARCHAR(255) NULL DEFAULT NULL AFTER `others`;
ALTER TABLE `bridge_incomingchannel_product_info` ADD `channel_supplier` INT NOT NULL AFTER `special_price`;
ALTER TABLE `bridge_incomingchannel_product_info` CHANGE `channel_supplier` `channel_supplier` INT(11) NULL;
ALTER TABLE `bridge_incomingchannels` ADD `cron_status` VARCHAR(255) NULL AFTER `others`;
ALTER TABLE `bridge_incomingchannel_product_info` CHANGE `is_processed` `is_processed` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '-2=>new out of stock,-1=>new,0=>changed,1=>synced,2=>processing,3=>no access,4=>trashed';
ALTER TABLE `bridge_incomingchannels_history` CHANGE `event` `event` ENUM( 'created', 'updated', 'content-updated', 'price-updated', 'conflict', 'quantity-updated' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
ALTER TABLE `bridge_incomingchannels_history` ADD `channel_type` VARCHAR( 255 ) NULL AFTER `brand_id`;
ALTER TABLE `bridge_incomingchannels_history` CHANGE `event` `event` ENUM( 'created', 'updated', 'content-updated', 'price-updated', 'conflict', 'quantity-updated', 'price-qty-updated' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
ALTER TABLE `bridge_incomingchannels_history` ADD `source` ENUM( 'api', 'inline-edit', 'page-edit' ) NULL DEFAULT NULL AFTER `event` ;
ALTER TABLE `bridge_incomingchannels_history` ADD `status` VARCHAR( 255 ) NULL AFTER `current_quantity` ;
ALTER TABLE `bridge_incomingchannel_action` ADD `apply_on` TEXT NULL AFTER `conditions_html` ;
CREATE TABLE IF NOT EXISTS `bridge_incomingchannel_contentrulecron` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entity_id` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `bridge_channel_id` int(50) DEFAULT NULL,
  `is_processed` int(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
);
ALTER TABLE `bridge_incomingchannels_history` CHANGE `source` `source` ENUM( 'api', 'inline-edit', 'page-edit', 'converted-price', 'observer' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
ALTER TABLE `bridge_incomingchannels_history` CHANGE `event` `event` ENUM( 'created', 'updated', 'content-updated', 'price-updated', 'conflict', 'quantity-updated', 'price-qty-updated', 'processing', 'nochange' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
ALTER TABLE `bridge_incomingchannels_history` CHANGE `source` `source` ENUM( 'api', 'inline-edit', 'page-edit', 'converted-price', 'observer', 'product-create-cron' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
ALTER TABLE `bridge_incomingchannel_product_info` ADD INDEX ( `sku` ) ;
ALTER TABLE `bridge_incomingchannels` ADD `is_hidden` BOOLEAN NOT NULL DEFAULT FALSE AFTER `currency` ;
ALTER TABLE `bridge_outgoingchannels` ADD `is_hidden` BOOLEAN NOT NULL DEFAULT FALSE AFTER `channel_status` ;");

$installer->endSetup();
