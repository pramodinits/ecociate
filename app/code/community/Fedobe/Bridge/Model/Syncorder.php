<?php

class Fedobe_Bridge_Model_Syncorder {

    public function addOrderToCustomtable($observer) {
        $order = $observer->getEvent()->getOrder();
        Mage::log('testing');
        $orderitemstosync = $this->getProductMinimumInfo($order);
    }

    public function updateOrderOnCustomtableStatusChanged($observer) {
        $order = $observer->getEvent()->getOrder();
        $orderitemstosync = $this->getProductMinimumInfo($order);
    }

    public function updateOrderOnCustomtablePaymentMade($observer) {
        $order = $observer->getEvent()->getPayment()->getOrder();
        $orderitemstosync = $this->getProductMinimumInfo($order);
    }

    public function getChannelinfo($channel_id, $supplier_id) {
        $channel_type = '';
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $incomingchanneltable = $resource->getTableName('bridge/incomingchannels');
        $logsql = "SELECT channel_type,channel_supplier FROM $incomingchanneltable WHERE id = $channel_id  LIMIT 1 ";
        $channelinfo = $read->fetchRow($logsql);
        return $channelinfo;
    }

   public function getProductMinimumInfo($order) {
        //Here to fetch only suppliers and bridge_channel_id attribute value of the product
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $write = $resource->getConnection("core_write");
        $attributecodetable = $resource->getTableName('eav_attribute');
        $attributevaluetable = $resource->getTableName('catalog_product_entity_int');
        $order_id = $order->getRealOrderId(); //$order->getIncrementId();
        $items = $order->getAllItems();
        $productids = $orderitemdetails = array();
        foreach ($items as $item) {
            $item = $item->getParentItem() ? $item->getParentItem() : $item;
            $productids[$item->getProductId()] = $item->getProductId();
            $orderitemdetails[$item->getProductId()] = array(
                'quantity' => $item->getQtyOrdered(),
                'price' => $item->getPrice(),
                'order_status' => $order->getState(),
            );
        }
        $orderitemstosync = array();
        $bridge_attr_id = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'bridge_pricenqty_channel_id');
//        $attributecodes = array('bridge_pricenqty_channel_id');
//        $supplier_code = Mage::helper('bridge')->getSupplierCode();
//        $attributecodes[] = $supplier_code;
//        $attrstring = implode(',', $attributecodes);
        //Here for EDI thing
        Mage::helper('bridge')->changeEdiProductQty($orderitemdetails,$order->getState(),$order_id);
        //End
        $product_ids_string = implode(',', $productids);
        $sql = "SELECT entity_id,value FROM $attributevaluetable WHERE  FIND_IN_SET(entity_id,'$product_ids_string') AND attribute_id=$bridge_attr_id AND value IS NOT NULL;";
        $sqlattrQuery = $read->query($sql);
        while ($row = $sqlattrQuery->fetch()) {
            $orderitemstosync[$row['entity_id']] = $row['value'];
        }

        if (!empty($orderitemstosync)) {
            $ordersynctable = $resource->getTableName('bridge/ordersync');
            //Here let's insert these items to the order sync table
            foreach ($orderitemstosync as $product_id => $bridge_channel_id) {
                $syncorderdata = array();
                if ($bridge_channel_id) {
//                    $supplier_id = $attributes[$supplier_code];
                    $channel_info = $this->getChannelinfo($bridge_channel_id);

                    if ($channel_info) {
                        $supplier_id = $channel_info['channel_supplier'];
                        $channel_type = $channel_info['channel_type'];
                        $incoming_productcode = $this->getIncomingProdcutCode($bridge_channel_id, $product_id, $channel_type);
                        if ($incoming_productcode) {
                            $syncorderdata['supplier_id'] = $supplier_id;
                            $syncorderdata['item_id'] = $product_id;
                            $syncorderdata['channel_id'] = $bridge_channel_id;
                            $syncorderdata['channel_type'] = $channel_type;
                            $syncorderdata['order_id'] = $order_id;
                            $syncorderdata['item_quantity'] = $orderitemdetails[$product_id]['quantity'];
                            $syncorderdata['item_price'] = $orderitemdetails[$product_id]['price'];
                            $syncorderdata['order_status'] = $orderitemdetails[$product_id]['order_status'];
                            $syncorderdata['incoming_product_code'] = $incoming_productcode === true ? null : $incoming_productcode;
                            //Here let's have a look into the order sync table whether one order exist.
                            $sql = "SELECT `id` FROM $ordersynctable WHERE `order_id` = $order_id AND `item_id` = $product_id LIMIT 1 ";
                            $syncoderid = $read->fetchOne($sql);
                            if ($syncoderid) {
                                //Here to update the status and change the changed flag
                                $order_status = $orderitemdetails[$product_id]['order_status'];
                                $updsql = "UPDATE $ordersynctable SET `order_status` = '$order_status',`is_changed`=1,`updated_at` = NOW() WHERE `id` = $syncoderid";
                                $write->query($updsql);
                            } else {
                                //Here let's insert the data into suncorder table
                                $columns = "`" . implode('`,`', array_keys($syncorderdata)) . "`,`created_at`";
                                $values = "'" . implode("','", array_values($syncorderdata)) . "',NOW()";
                                $insql = "INSERT INTO $ordersynctable ($columns) VALUES ($values)";
                                $write->query($insql);
                            }
                        } else {
                            //Here no code found for product
                        }
                    }
                }
            }
        }
    }

    public function getIncomingProdcutCode($bridge_channel_id, $product_id, $channel_type) {
        $code = '';
        $helper_class = Mage::getConfig()->getHelperClassName($channel_type);
        if (class_exists($helper_class)) {
            $helper = Mage::helper($channel_type);
            if (is_object($helper) && method_exists($helper, 'getIncomingProdcutCode')) {
                $code = $helper->getIncomingProdcutCode($bridge_channel_id, $product_id);
                return $code;
            }
        }
        return true;
    }

}
