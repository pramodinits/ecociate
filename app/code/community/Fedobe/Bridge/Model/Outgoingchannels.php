<?php

class Fedobe_Bridge_Model_Outgoingchannels extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('bridge/outgoingchannels');
    }

    public function delete($id) {
        $write_connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        try {
            $outgoing_model = $this->load($id);
            if (!$outgoing_model->getId()) {
                return array('status' => 'error', 'msg' => "Unable to find the outgoingchannel to delete");
            }
            $channel_type = $outgoing_model->getChannelType(); //echo $channel_type;exit;
            $helperobj = Mage::helper($channel_type);
            $method = "deleteChannel";
            if (method_exists($helperobj, $method)) {
                $write_connection->beginTransaction();
                //delete the channel type based things
                $helperobj->deleteChannel($id);
                //delete data from edi
                $write_connection->raw_query("DELETE FROM bridge_incomingchannel_product_info WHERE channel_type='outgoing' AND channel_id=$id;");
                //delete data from log
                $write_connection->raw_query("DELETE FROM bridge_incominglog WHERE channel_type='outgoing' AND bridge_channel_id=$id;");
                //delete data from history
                $write_connection->raw_query("DELETE FROM bridge_incomingchannels_history WHERE channel_type='outgoing' AND brand_id=$id;");

                //delte the outgoing channel
                $write_connection->raw_query("DELETE FROM bridge_outgoingchannels WHERE id=$id;");
                $write_connection->commit();
            } else {
                return array('status' => 'error', 'msg' => "Sorry $channel_type Addon have restriction to delete Channel.");
            }
            // Make saves and other actions that affect the database
        } catch (Exception $e) {
            $write_connection->rollback();
            echo $e->getMessage();
            exit;
            return array('status' => 'error', 'msg' => $e->getMessage());
        }
        return array('status' => 'success', 'msg' => "The Outgoing Channel has been deleted.");
    }

    /**
     * Load incomingchannel  by id
     *
     * @param intiger $id
     */
    public function loadById($id) {
        $this->setData($this->getResource()->loadById($id));
        return $this;
    }

    /**
     * Load incomingchannel  by id
     *
     * @param intiger $id
     */
    public function loadByConsumerId($consumer_id) {
        $this->setData($this->getResource()->loadByConsumerId($consumer_id));
        return $this;
    }

    /**
     * get retailer list
     *
     * @param intiger $id
     */
    public function getRetailers() {
        $retailer_ids = $this->getRetailerIds();
        $userCollection = Mage::getModel('admin/user')->getCollection()
                        ->addFieldToSelect(array('user_id', 'username'))->addFieldToFilter('user_id', $retailer_ids)->getData();
        array_unshift($userCollection, array("user_id" => 0, "username" => 'Select'));
        $retailer_list = array_column($userCollection, 'username', 'user_id');
        return $retailer_list;
    }

    /**
     * get retailer list
     *
     * @param intiger $id
     */
    public function getmatchedRetailers($tag) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $inc_ids = '';
        $option_sql = "SELECT admin_user_id value,username name FROM bridge_outgoingchannels JOIN admin_user ON admin_user.user_id=bridge_outgoingchannels.admin_user_id WHERE username like '%$tag%' group by admin_user_id order by username";
        $result = $readConnection->fetchAll($option_sql);
        return $result;
    }

    /**
     * get array of user ids for user for whome bridge role is assigned
     * @return type array of user ids
     */
    public function getRetailerIds() {
        $role_id = Mage::getmodel('admin/roles')->getCollection()->addFieldToFilter('role_name', 'Bridge User')->addFieldToSelect('role_id')->getFirstItem()->getId();
        $user_collections = Mage::getmodel('admin/roles')->getCollection()->addFieldTOSelect('user_id');
        $user_collections->getSelect()->where('role_type="U"')->orwhere('parent_id=' . $role_id . ' AND role_type="U"');
        $result = array_values(array_map('array_values', $user_collections->getData()));
        return @call_user_func_array('array_merge', $result);
    }

    /**
     * check the user is retailer or not
     * @return type array of user ids
     */
    public function isRetailer($id) {
        $rolename = Mage::getModel('admin/user')->load($id)->getRole()->getRoleName();
        return strcasecmp($rolename, 'Bridge User') === 0;
    }

    /**
     * check the user is retailer or not
     * @return type array of user ids
     */
    public function isAdminstrator($id) {
        $rolename = Mage::getModel('admin/user')->load($id)->getRole()->getRoleName();
        return strcasecmp($rolename, 'Administrators') === 0;
    }

    /**
     * get attribute set from list of product ids
     * $product_ids array
     * @return type array of attribute set
     */
    public function getAttributeSetIds($product_ids) {
        $product_colections = Mage::getmodel('catalog/product')->getCollection();
        $product_colections->addAttributeToFilter('entity_id', array('in' => $product_ids));
        $product_colections->addAttributeToSelect('attribute_set_id');
        $product_colections->getSelect()
                ->group('attribute_set_id');
        $collection = $product_colections->getData();
        $attribute_set = array_map(function($product) {
            return $product['attribute_set_id'];
        }, $collection);
        return $attribute_set;
    }

    /**
     * 
     * @param type $product_ids
     * @return type
     */
    public function getAttributes($product_ids) {
        $attr_result = array();
        if ($product_ids) {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $attribute_set_ids = implode(',', $this->getAttributeSetIds($product_ids));
            $attr_sql = "SELECT entity_attribute.attribute_id,main_table.attribute_code,main_table.frontend_label,entity_attribute.attribute_set_id,label.store_id,label.value
            FROM `eav_attribute`
 AS `main_table`
 INNER JOIN `eav_entity_attribute` AS `entity_attribute` ON entity_attribute.attribute_id = main_table
.attribute_id  LEFT JOIN eav_attribute_label AS label ON label.attribute_id=entity_attribute.attribute_id  WHERE main_table.is_user_defined=1 AND entity_attribute.attribute_set_id IN($attribute_set_ids) order by entity_attribute.attribute_set_id asc"; //
            $attributes = $readConnection->fetchAll($attr_sql);
            $attribute_ids = array();
            foreach ($attributes as $attribute) {
                $attr_set_id = $attribute['attribute_set_id'];
                $attribute_ids[] = $attribute['attribute_id'];
                unset($attribute['attribute_set_id']);
                if (!isset($attr_result[$attr_set_id])) {
                    $attr_set_name = Mage::getModel("eav/entity_attribute_set")->getCollection()
                                    ->addFieldToFilter('attribute_set_id', $attr_set_id)->getFirstItem()->getAttributeSetName();

                    $attr_result[$attr_set_id]['name'] = $attr_set_name;
                }
                $attr_result[$attr_set_id][] = $attribute;
            }
            $option_sql = "SELECT options.attribute_id,optvalues.option_id id,optvalues.store_id,optvalues.value FROM
            eav_attribute_option AS options LEFT JOIN eav_attribute_option_value  AS optvalues ON options.option_id=optvalues.option_id WHERE options.attribute_id IN(" . implode(',', $attribute_ids) . ")";
            $options = $readConnection->fetchAll($option_sql);
            if (@$options) {
                $attr_result['options'] = $options;
            }
        }
        return $attr_result;
    }

    /**
     * 
     * @return type array
     */
    public function getCategories() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $attr_id_sql = "SELECT eav_attribute.attribute_id FROM eav_attribute "
                . "JOIN eav_entity_type ON eav_entity_type.entity_type_id=eav_attribute.entity_type_id"
                . " WHERE eav_attribute.attribute_code='name' AND eav_entity_type.entity_type_code='catalog_category'";
        $attribute_id = $readConnection->fetchRow($attr_id_sql);

        $cat_sql = "SELECT category.entity_id id,label.store_id,label.value FROM catalog_category_entity AS category JOIN catalog_category_entity_varchar AS label ON category.entity_id=label.entity_id WHERE category.parent_id!=0 AND attribute_id={$attribute_id['attribute_id']}";
        $categories = $readConnection->fetchAll($cat_sql);
        return $categories;
    }

    /**
     * filtered the skus by status of product given
     */
    public function getFilteredSkus($skus, $status) {
        $entity_ids = implode(',', $skus);
        $attributeId = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'status');
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $attr_sql = "SELECT product.entity_id FROM `catalog_product_entity` 
 AS `product`
 INNER JOIN `catalog_product_entity_int` AS `product_status` ON product.entity_id = product_status.entity_id 
 WHERE product.entity_id IN($entity_ids) AND product_status.attribute_id=$attributeId AND product_status.store_id=0 AND product_status.value=$status group by product.entity_id";
        $filtered_skus = $readConnection->fetchAll($attr_sql);
        return $filtered_skus ? array_map('current', $filtered_skus) : array(0);
    }

    /**
     * get all stores
     */
    public function getStores() {
        $store_result = array();
        $stores = Mage::app()->getStores();
        foreach ($stores as $store) {
            $store_result[$store->getId()] = $store->getName();
        }
        return $store_result;
    }

    /**
     * get Bridge api role id
     */
    public function getRestRoleId() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $attr_sql = "SELECT entity_id FROM `api2_acl_role` WHERE role_name='Bridge API'";
        $filtered_skus = $readConnection->fetchRow($attr_sql);
        return $filtered_skus['entity_id'];
    }

    /**
     * get getChannelUsers
     */
    public function checkUniqueUrl($id, $url) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $sql = "SELECT id FROM bridge_outgoingchannels WHERE url='{$url}'";
        if ($id) {
            $sql = $sql . " AND id!=$id";
        }
        $sql = $sql . " LIMIT 0,1";
        $result = $readConnection->fetchRow($sql);
        return $result['id'] ? 1 : 0;
    }

    /**
     * return channel details
     */
    public function getChannelDetails($consumer_id) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $sql = "SELECT bridge_outgoingchannels.*,oauth_token.callback_url FROM bridge_outgoingchannels JOIN oauth_token ON bridge_outgoingchannels.consumer_id=oauth_token.consumer_id AND oauth_token.type='access' AND authorized=1 WHERE bridge_outgoingchannels.consumer_id=$consumer_id LIMIT 0,1";
        $result = $readConnection->fetchRow($sql);
        $otg_model = Mage::getModel('bridge/outgoingchannels')->setData($result);
        return $otg_model;
    }

    /**
     * 
     * @return type array for rule
     */
    public function getCategoriesDetails() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $attr_id_sql = "SELECT eav_attribute.attribute_id FROM eav_attribute "
                . "JOIN eav_entity_type ON eav_entity_type.entity_type_id=eav_attribute.entity_type_id"
                . " WHERE eav_attribute.attribute_code='name' AND eav_entity_type.entity_type_code='catalog_category'";
        $attribute_id = $readConnection->fetchRow($attr_id_sql);

        $cat_sql = "SELECT category.entity_id id,label.value FROM catalog_category_entity AS category JOIN catalog_category_entity_varchar AS label ON category.entity_id=label.entity_id WHERE attribute_id={$attribute_id['attribute_id']} AND store_id=" . Mage_Core_Model_App::ADMIN_STORE_ID;
        $categories = $readConnection->fetchAll($cat_sql);
        foreach ($categories as $cat) {
            $cat['value'] = htmlspecialchars($cat['value'], ENT_QUOTES);
            $result[$cat['value']] = $cat['id'];
        }
        //$categories = array_column($categories, 'id', 'value');
        return $result;
    }

}
