<?php
class Fedobe_Bridge_Model_Mysql4_Incomingchannels_Mapping extends Mage_Core_Model_Mysql4_Abstract{
    public function _construct(){
        $this->_init('bridge/incomingchannels_mapping', 'id');
    }
    
    public function loadAllByBrandId($brand_id)
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('bridge/incomingchannels_mapping'))
            ->where('brand_id=:brand_id');
        return $adapter->fetchAll($select, array('brand_id'=>$brand_id));
    }
}