<?php

class Fedobe_Bridge_Model_Mysql4_Incomingchannels_Notification extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('bridge/incomingchannels_notification', 'id');
    }

    /**
     * Load data by brand_id
     *
     * @param integer $brand_id
     * @return array
     */
    public function loadAllByBrandId($brand_id) {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('bridge/incomingchannels_notification'))
                ->where('brand_id=:brand_id');
        return $adapter->fetchAll($select, array('brand_id' => $brand_id));
    }


    /**
     * Load incomingchannel track by product sku
     *
     * @param string $sku
     */
    public function loadAllBySku($sku) {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('bridge/incomingchannels_notification'))
                ->where('sku=:sku');
        return $adapter->fetchRow($select, array('sku' => $sku));
    }

}
