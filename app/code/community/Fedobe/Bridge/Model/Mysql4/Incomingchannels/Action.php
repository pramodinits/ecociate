<?php
class Fedobe_Bridge_Model_Mysql4_Incomingchannels_Action extends Mage_Core_Model_Mysql4_Abstract{
    public function _construct(){
        $this->_init('bridge/incomingchannels_action', 'id');
    }
     /**
     * Load data by brand_id
     *
     * @param integer $brand_id
     * @return array
     */
    public function loadAllByBrandId($brand_id)
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('bridge/incomingchannels_action'))
            ->where('brand_id=:brand_id');
        return $adapter->fetchAll($select, array('brand_id'=>$brand_id));
    }
     /**
     * Load data by id
     *
     * @param integer $id
     * @return array
     */
    public function loadById($id)
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('bridge/incomingchannels_action'))
            ->where('id=:id');
        return $adapter->fetchRow($select, array('id'=>$id));
    }
}