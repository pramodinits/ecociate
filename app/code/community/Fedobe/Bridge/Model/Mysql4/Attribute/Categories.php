<?php

class Fedobe_Bridge_Model_Mysql4_Attribute_Categories extends Mage_Core_Model_Mysql4_Abstract {

    private $table_name="bridge_attribute_categories";
    public function _construct() {
        $this->_init('bridge/attribute_categories', 'id');
    }

    /**
     * Load data by brand id
     *
     * @param integer $brand_id
     * @return array
     */
    public function loadByBrandId($brand_id) {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('bridge/attribute_categories'))
                ->where('brand_id=:brand_id');
        return $adapter->fetchRow($select, array('brand_id' => $brand_id));
    }

    /**
     * delete data by brand id
     *
     * @param integer $brand_id
     */
    public function deleteByBrandId($brand_id) {
        $connection = Mage::getSingleton('core/resource')
                ->getConnection('core_write');
        $connectionWrite->beginTransaction();
        $condition = array($connection->quoteInto('brand_id=?', "$brand_id"));
        $connection->delete($this->table_name, $condition);
        $connection->commit();
    }

}
