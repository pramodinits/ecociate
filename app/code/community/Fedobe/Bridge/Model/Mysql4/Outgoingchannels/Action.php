<?php
class Fedobe_Bridge_Model_Mysql4_Outgoingchannels_Action extends Mage_Core_Model_Mysql4_Abstract{
    public function _construct(){
        $this->_init('bridge/outgoingchannels_action', 'id');
    }
     /**
     * Load data by brand_id
     *
     * @param integer $brand_id
     * @return array
     */
    public function loadAllByChannelId($channelId)
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('bridge/outgoingchannels_action'))
            ->where('channel_id=:channel_id');
        return $adapter->fetchAll($select, array('channel_id'=>$channelId));
    }
     /**
     * Load data by id
     *
     * @param integer $id
     * @return array
     */
    public function loadById($id)
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('bridge/outgoingchannels_action'))
            ->where('id=:id');
        return $adapter->fetchRow($select, array('id'=>$id));
    }
}