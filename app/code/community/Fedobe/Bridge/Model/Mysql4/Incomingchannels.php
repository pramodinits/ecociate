<?php
class Fedobe_Bridge_Model_Mysql4_Incomingchannels extends Mage_Core_Model_Mysql4_Abstract{
    public function _construct(){
        $this->_init('bridge/incomingchannels', 'id');
    }
     /**
     * Load data by id
     *
     * @param integer $id
     * @return array
     */
    public function loadById($id)
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('bridge/incomingchannels'))
            ->where('id=:id');
        return $adapter->fetchRow($select, array('id'=>$id));
    }
}