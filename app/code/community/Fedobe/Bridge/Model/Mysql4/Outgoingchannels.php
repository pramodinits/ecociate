<?php

class Fedobe_Bridge_Model_Mysql4_Outgoingchannels extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('bridge/outgoingchannels', 'id');
    }

    /**
     * Load data by id
     *
     * @param integer $id
     * @return array
     */
    public function loadById($id) {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('bridge/outgoingchannels'))
                ->where('id=:id');
        return $adapter->fetchRow($select, array('id' => $id));
    }
    /**
     * Load data by id
     *
     * @param integer $id
     * @return array
     */
    public function loadByConsumerId($consumer_id) {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('bridge/outgoingchannels'))
                ->where('consumer_id=:consumer_id');
        return $adapter->fetchRow($select, array('consumer_id' => $consumer_id));
    }

}
