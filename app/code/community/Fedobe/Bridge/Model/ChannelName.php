<?php

class Fedobe_Bridge_Model_ChannelName extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('bridge/channel_name');
    }

    public function getOptionArray() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $sql = "SELECT CONCAT('outgoing',id) id,name FROM bridge_outgoingchannels";
        $inc_result = $readConnection->fetchAll($sql);
        $sql = "SELECT CONCAT('incoming',id) id,brand_name name FROM bridge_incomingchannels";
        $ogc_result = $readConnection->fetchAll($sql);
        $result=  array_merge($ogc_result,$inc_result);
        return array_column($result, 'name', 'id');
    }

}
