<?php

class Fedobe_Bridge_Model_Observer {

    public function addProductiewButton($observer) {
        $_block = $observer->getBlock();
        $_type = $_block->getType();
        if ($_type == 'adminhtml/catalog_product_edit') {

            $_block->setChild('product_view_button', $_block->getLayout()->createBlock('bridge/adminhtml_managebridgecontent_widget_button')
            );

            //Here to add Detail Product view page of incoming channels if any
            $channel_id = Mage::registry('current_product')->getData('bridge_channel_id');
            if ($channel_id) {
                $this->addIncomingProductViewPageButton($observer, $channel_id);
            }
            //End

            $_deleteButton = $_block->getChild('delete_button');
            /* Prepend the new button to the 'Delete' button if exists */
            if (is_object($_deleteButton)) {
                $_deleteButton->setBeforeHtml($_block->getChild('product_view_button')->toHtml());
            } else {
                /* Prepend the new button to the 'Reset' button if 'Delete' button does not exist */
                $_resetButton = $_block->getChild('reset_button');
                if (is_object($_resetButton)) {
                    $_resetButton->setBeforeHtml($_block->getChild('product_view_button')->toHtml());
                }
            }
        }
    }

    public function addIncomingProductViewPageButton($observer, $channel_id) {
        $_block = $observer->getBlock();
        $model = Mage::getModel('bridge/incomingchannels');
        $model->load($channel_id);
        $channel_type = $model->getChannelType();
        $channel_block = "$channel_type" . "_view_button";
        $viewblock = $_block->getLayout()->createBlock("$channel_type/adminhtml_viewpage_widget_button");
        if ($viewblock) {
            $_block->setChild($channel_block, $_block->getLayout()->createBlock('souq/adminhtml_viewpage_widget_button'));
            $productviewbtn = $_block->getChild('product_view_button');
            if (is_object($productviewbtn)) {
                $productviewbtn->setBeforeHtml($_block->getChild($channel_block)->toHtml());
            } else {
                $_deleteButton = $_block->getChild('delete_button');
                /* Prepend the new button to the 'Delete' button if exists */
                if (is_object($_deleteButton)) {
                    $_deleteButton->setBeforeHtml($_block->getChild($channel_block)->toHtml());
                } else {
                    /* Prepend the new button to the 'Reset' button if 'Delete' button does not exist */
                    $_resetButton = $_block->getChild('reset_button');
                    if (is_object($_resetButton)) {
                        $_resetButton->setBeforeHtml($_block->getChild($channel_block)->toHtml());
                    }
                }
            }
        }
    }

    public function checkRetailer($observer) {
        if (Mage::getmodel('bridge/outgoingchannels')->isRetailer(Mage::getSingleton('admin/Session')->getUser()->getUserId())) {
            $url = Mage::helper('bridge')->getOgcGridUrl();
            Mage::app()->getResponse()->setRedirect($url);
        }
    }

    public function checkout_type_onepage_save_order_after($observer) {
        $order = $observer->getOrder();
        $order_id = $order->getId();


        /**
         * Grab all product ids from the order
         */
        $productIds = array();
        foreach ($order->getItemsCollection() as $item) {
            $productIds[$item->getProductId()] = $item; //$item;
        }
        //get the channel ids for the order items
        $product_channels = Mage::getmodel('catalog/product')
                        ->getCollection()
                        ->addAttributeToFilter('entity_id', array_keys($productIds))
                        ->addAttributeToFilter('bridge_pricenqty_channel_id', array('notnull' => true))
                        ->addAttributeToSelect(array('entity_id', 'bridge_pricenqty_channel_id'))->getData();
        if ($product_channels) {
            $product_channel_ids = array_column($product_channels, 'bridge_pricenqty_channel_id', 'entity_id');
            $channel_info = Mage::getmodel('bridge/incomingchannels')
                            ->getCollection()
                            ->addFieldToFilter('id', array_unique(array_values($product_channel_ids)))
                            ->addFieldToFilter('authorized', 1)
                            ->addFieldToFilter('status', 1)
                            ->addFieldToFilter('product_pricenquantity', 1)
                            ->addFieldToSelect('id')->getData();


            $transactionSave = Mage::getModel('core/resource_transaction');
            foreach ($channel_info as $channel_data) {
                $product_entities = array_keys($product_channel_ids, $channel_data['id']);
                foreach ($product_entities as $product_id) {
                    $customModel = Mage::getModel('bridge/order');
                    $sku = $productIds[$product_id]->getSku();
                    $price = $productIds[$product_id]->getProduct()->getPrice();
                    $qty = $productIds[$product_id]->getQtyOrdered();
                    $customModel->setData(array('channel_id' => $channel_data['id'], 'sku' => "{$sku}", 'quantity' => $qty, 'amount' => $price * $qty, 'store_order_id' => $order_id));
                    $transactionSave->addObject($customModel);
                }
            }
            $transactionSave->save();
        }
    }

    public function catalog_product_save_before($observer) {
        if (Mage::app()->getRequest()->getActionName() == 'save' && Mage::app()->getRequest()->getControllerName() == 'catalog_product'):
            $product = $observer->getProduct();
            //added by sital for history change
            $channel_change = 0;
            $event = array();
            $product_prev = Mage::getmodel('catalog/product')->load($product->getId());
            $product_prev_data = $product_prev->getData();
            $curr_price = $product->getPrice();
            $curr_qty = intval($product->getStockData()['qty']);
            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
            $previous_qty = $stockItem->getQty();
            //added by sital for history change end

            $product->setPrice(round($product->getPrice(), 0));
            if ($product->getSpecialPrice())
                $product->setSpecialPrice(round($product->getSpecialPrice(), 0));
            if ($product->getMsrp())
                $product->setMsrp(round($product->getMsrp(), 0));
            $product_data = $product->getData();
            if ($product_data['entity_id']) {
                $inventory_model = Mage::getmodel('bridge/incomingchannels_product');

                $own_store_change = 0;
                $price = @$product_data['price'];
                $specialprice = @$product_data['special_price'];
                $suppliers = @$product_data['suppliers'];
                $quantity = intval($product->getStockData()['qty']);
                $own_inventory = $inventory_model->getCollection()
                        ->addFieldToFilter('channel_id', 0)
                        ->addFieldToFilter('product_entity_id', @$product_data['entity_id'])
                        ->getFirstItem();
                if (!$own_inventory->getId() && !$product_data['bridge_pricenqty_channel_id']) {
                    $own_store_change = 1;
                    $own_inventory->setData(array('sku' => $product_data['sku'], 'price' => $price, 'quantity' => $quantity, 'special_price' => $specialprice, 'product_entity_id' => $product_data['entity_id'], 'channel_supplier' => $product_data['suppliers']));
                } else if ($own_inventory->getId()) {
                    if (isset($product->getStockData()['qty']) && !isset($product_data['channel']['use_channel_qty'])) {
                        $own_store_change = 1;
                        $own_inventory->setQuantity($quantity);
                    }
                    if (isset($product_data['price']) && !isset($product_data['channel']['use_channel_price'])) {
                        $own_store_change = 1;
                        $own_inventory->setPrice($price);
                    }
                    if (isset($product_data['special_price']) && !isset($product_data['channel']['use_channel_special_price'])) {
                        $own_store_change = 1;
                        $specialprice = $specialprice ? $specialprice : NULL;
                        $own_inventory->setSpecialPrice($specialprice);
                    }
                    if (isset($product_data['suppliers']) && !isset($product_data['channel']['use_channel_suppliers'])) {
                        $own_store_change = 1;
                        $suppliers = $suppliers ? $suppliers : NULL;
                        $own_inventory->setChannelSupplier($suppliers);
                    }
                    if ($product_data['sku'] != $own_inventory->getSku()) {
                        $own_store_change = 1;
                        $own_inventory->setSku($product_data['sku']);
                    }
                }
                if ($own_store_change && $own_inventory->getSku()) {
                    $bf_save = $own_inventory->getData();
                    $own_inventory->save();
                }
                if ($product->getBridgePricenqtyChannelId()) {
                    $product_channel = $product_data['channel'];
                    $sku = $product_data['sku'];
                    $bridge_inventory = $inventory_model->getCollection()
                            ->addFieldToFilter('channel_id', $product->getBridgePricenqtyChannelId())
                            ->addFieldToFilter('product_entity_id', $product->getId())
                            ->addFieldToSelect(array('price', 'quantity', 'id', 'converted_price'))
                            ->getFirstItem();


                    if ($quantity <= 0) {
                        $product->unsetData('price');
                        $product->unsetData('special_price');
                        $product->unsetData('suppliers');
                    } else {
                        $product->setBridgePricenqtyChannelId(null);
                        if ($own_inventory->getId()) {
                            if ($product_channel['use_channel_price'])
                                $product->setPrice($own_inventory->getPrice());
                            if ($product_channel['use_channel_special_price'])
                                $product->setSpecialPrice($own_inventory->getSpecialPrice());
                            if ($product_channel['use_channel_suppliers'])
                                $product->setSuppliers($own_inventory->getChannelSupplier());
                        }
                        Mage::getmodel('bridge/inventory')->changeLiveStatus(array(0 => array('channel_id' => 0, 'sku' => $product_data['sku'])));
                    }

                    if (!$product->getBridgePricenqtyChannelId()) {
                        //save datain history
                        $history_model = Mage::getmodel('bridge/incomingchannels_history');
                        $history_model->setData(array('sku' => $sku, 'event' => 'price-updated', 'previous_price' => $bridge_inventory->getPrice(), 'current_price' => $price, 'previous_quantity' => $bridge_inventory->getQuantity(), 'current_quantity' => $quantity));
                        $history_model->save();
                        $channel_change = 1;
                    }
                }
                //added by sital for history change
                if (!$channel_change && !Mage::app()->getRequest()->getParam('isAjax')) {
                    if ($curr_price && (round($curr_price, 2) != round($product_prev_data['price'], 2))) {
                        $event[] = 'price-updated';
                    }
                    if ($curr_qty && (intval($previous_qty) != intval($curr_qty))) {
                        $event[] = 'quantity-updated';
                    }
                    if (count($event) == 1) {
                        $event_msg = $event[0];
                    } else if (count($event) == 2) {
                        $event_msg = 'price-qty-updated';
                    } else {
                        $event_msg = 'no';
                    }
                    if ($event_msg != 'no') {
                        $history_model = Mage::getmodel('bridge/incomingchannels_history');
                        $history_1 = array('sku' => $product_prev_data['sku'], 'source' => 'page-edit', 'event' => $event_msg, 'previous_price' => $product_prev_data['price'], 'current_price' => $curr_price, 'previous_quantity' => $previous_qty, 'current_quantity' => $curr_qty);

                        $history_model->setData($history_1);
                        $history_model->save();
                    }
                }
                //added by sital for history change end
            }
        endif;
    }

    public function catalog_product_save_after($observer) {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection("core_write");
        $readConnection = $resource->getConnection("core_read");
        $product = Mage::getModel('catalog/product')->load($observer->getProduct()->getId());
        $sku = $product->getSku();
        $history = array();
        //get the product updated time
        $now_time = $product->getUpdatedAt();
        $inventory_model = Mage::getmodel('bridge/incomingchannels_product');
        //get the outging channel ids which are involved in this sku
        $channel_ids_arr = $inventory_model->getCollection()
                        ->addFieldToFilter('channel_id', array('neq' => 0))
                        ->addFieldToFilter('product_entity_id', $product->getId())
                        ->addFieldToFilter('channel_type', 'outgoing')
                        ->addFieldToSelect('*')->getData();
        $bridge_edi_crontable = $resource->getTableName('bridge/bridgeapi_edichannelcron');
        if ($channel_ids_arr) {
            $channel_ids = array_column($channel_ids_arr, 'channel_id');
            $channel_ids_str = implode(',', $channel_ids);
            $writeConnection->raw_query("UPDATE bridge_incomingchannel_product_info SET quantity=0,is_processed=2,`last_updated_time`='{$now_time}',sku='{$product->getSku()}' WHERE channel_id  AND product_entity_id={$product->getId()} AND channel_type='outgoing' AND deleted=0 AND is_processed NOT IN (-1,-2);");
            //update the edi cron table as condition changeds
            $writeConnection->raw_query("UPDATE " . $bridge_edi_crontable . " SET condition_changed=1 WHERE channel_type='outgoing' AND channel_id IN($channel_ids_str);");
            $i = 0;
            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
            $previous_qty = $stockItem->getQty();
            foreach ($channel_ids_arr as $k => $v) {
                $history[$i]['sku'] = $v['sku'];
                $history[$i]['brand_id'] = $v['channel_id']; //$remaining_products[$product_insert['entity_id']]['channel_id'];
                $history[$i]['previous_quantity'] = $previous_qty;
                $history[$i]['current_quantity'] = 0;
                $history[$i]['previous_price'] = $v['converted_price'];
                $history[$i]['current_price'] = $v['converted_price'];
                $history[$i]['channel_type'] = 'outgoing';
                $history[$i]['status'] = 'processing';
                $history[$i]['event'] = 'processing';
                $history[$i]['source'] = 'observer';
                $i++;
            }
            //print_r($history);exit;
            if (!empty($history)) {
                $history_table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_history');
                $writeConnection->insertMultiple($history_table, $history);
            }
        }
        //set the cron table as new added sku if sku was not exist previously for these
        $channel_ids_str = @$channel_ids_str ? $channel_ids_str : 0;
        $writeConnection->raw_query("UPDATE " . $bridge_edi_crontable . " SET new_added=1 WHERE channel_type='outgoing' AND channel_id NOT IN($channel_ids_str);");


        //get the incoming channel ids which are involved in this sku
        if (Mage::app()->getRequest()->getActionName() != 'edi_process' || Mage::app()->getRequest()->getControllerName() != 'sync'):
            $incchannel_ids = $inventory_model->getCollection()
                            ->addFieldToFilter('channel_id', array('neq' => 0))
                            ->addFieldToFilter('product_entity_id', $product->getId())
                            ->addFieldToFilter('channel_type', 'incoming')
                            ->addFieldToFilter('deleted', 0)
                            ->addFieldToSelect('channel_id')->getData();
            if ($incchannel_ids) {
                $channel_ids = array_column($incchannel_ids, 'channel_id', 'channel_id');
//            if ($product->getBridgePricenqtyChannelId()) {
//                unset($channel_ids[$product->getBridgePricenqtyChannelId()]);
//            }
                if ($channel_ids) {
                    $channel_ids_str = implode(',', $channel_ids);
                    //delete the edi info for this product for inc channel
                    $writeConnection->raw_query("UPDATE bridge_incomingchannel_product_info SET is_processed=2,sku='{$product->getSku()}' WHERE channel_id IN($channel_ids_str)  AND product_entity_id={$product->getId()} AND channel_type='incoming';");
                    $bridge_edi_crontable = $resource->getTableName('bridge/bridgeapi_edichannelcron');
                    //update the edi cron table as condition changeds
                    $writeConnection->raw_query("UPDATE " . $bridge_edi_crontable . " SET condition_changed=1 WHERE channel_type='incoming' AND channel_id IN($channel_ids_str);");
                }
            }
        endif;

        //insert the data to the edi table as own invenotry if not exist
        $channel_id = $product->getBridgePricenqtyChannelId() ? $product->getBridgePricenqtyChannelId() : 0;
        if ($channel_id === 0 && !Mage::app()->getRequest()->getParam('isAjax')) {
            $own_inventory = $inventory_model->getCollection()
                    ->addFieldToFilter('channel_id', $channel_id)
                    ->addFieldToFilter('product_entity_id', $product->getId())
                    ->getFirstItem();

            $quantity = intval($product->getStockItem()->getData()['qty']);
            $own_inventory->setData(array('sku' => $product->getSku(), 'price' => $product->getPrice(), 'quantity' => $quantity, 'special_price' => $product->getSpecialPrice(), 'id' => $own_inventory->getId(), 'is_live' => 1, 'channel_supplier' => $product->getSuppliers(), 'product_entity_id' => $product->getId()));
            $own_inventory->save();
        }
        $writeConnection->raw_query("UPDATE bridge_incomingchannel_product_info SET `last_updated_time`='{$now_time}',sku='{$sku}' WHERE  product_entity_id={$product->getId()}");
    }

    public function catalog_product_delete_after($observer) {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection("core_write");
        $sku = $observer->getProduct()->getSku();
        $entity_id = $observer->getProduct()->getId();
        $productinfo_table_name = $resource->getTableName('bridge/incomingchannels_product');
        $query = "UPDATE  $productinfo_table_name SET deleted=1,price=0,quantity=0,is_processed=0,converted_price=0 WHERE channel_type ='outgoing' AND product_entity_id=$entity_id;";
        $query .= "DELETE FROM  $productinfo_table_name WHERE (channel_type ='incoming' OR channel_id=0) AND product_entity_id=$entity_id;";
        $writeConnection->raw_query($query);
    }

    //change the edi quantity after placting order
    public function checkout_submit_all_after($observer) {
        if ($observer->getEvent()->hasOrders()) {
            $orders = $observer->getEvent()->getOrders();
        } else {
            $orders = array($observer->getEvent()->getOrder());
        }
        $stockItems = array();
        foreach ($orders as $order) {
            foreach ($order->getAllItems() as $orderItem) {
                if ($orderItem->getQtyOrdered()) {
                    $entityId[] = $orderItem->getProductId();
                }
            }
        }
        if ($entityId) {
            $this->changeEdiProductQty($entityId);
        }
    }
    

    /**
     * if order is canceled or reorder or new order place the update the edi inventory table
     * @param type $event
     */
    public function invoicedStatusChange($event) {
        $orders = $event->getOrder();
        $entityId = array();
        if ($orders->getState() == Mage_Sales_Model_Order::STATE_CANCELED || $orders->getState() == Mage_Sales_Model_Order::STATE_NEW) {
            foreach ($orders->getAllItems() as $orderItem) {
                if ($orderItem->getQtyOrdered()) {
                    $entityId[] = $orderItem->getProductId();
                }
            }
        }
        if ($entityId) {
            $this->changeEdiProductQty($entityId);
        }
    }

    public function handle_adminSystemConfigChangedSection($observer) {
        $cron_flag = Mage::getStoreConfig('bridge/magento_settings/inc_edi_cron', Mage::app()->getStore());
        if (!file_exists($_SERVER["DOCUMENT_ROOT"] . '/bridgecron')) {
            mkdir($_SERVER["DOCUMENT_ROOT"] . '/bridgecron');
        }
        chmod($_SERVER["DOCUMENT_ROOT"] . '/bridgecron', 0777);
        $edi_link = Mage::getBaseUrl() . "admin/sync/edi_process";

        $crons = shell_exec('crontab -l'); //echo $crons;exit;
        //wget -o /dev/null -q -O - "http://kanarygifts.com/index.php/admin/admin/amazon/cronroute?name=productupdate&channel_id=3" > /dev/null
        $cronurl = 'wget -o /dev/null -q -O -';
        $endcronurl = '> /dev/null';
        $edi_cron_link = "*/2 * * * *  $cronurl $edi_link $endcronurl" . PHP_EOL;

        $crons = str_replace($edi_cron_link, "", $crons, $count);
        if ((!$cron_flag && $count) || ($cron_flag && !$count)) {
            if ((!$cron_flag && $count))
                file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/bridgecron/crontab.txt', $crons);
            else {
                $crons .=$edi_cron_link;
                file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/bridgecron/crontab.txt', $crons);
            }
            exec("crontab {$_SERVER['DOCUMENT_ROOT']}/bridgecron/crontab.txt");
        }
    }

}
