<?php

class Fedobe_Bridge_Model_Api2_Category_Rest_Admin_V1 extends Mage_Api2_Model_Resource {

    public function _retrieveCollection() {
//        return $this->getRequest()->getParams();
        if ($otgchannel = $this->validateRequest()) {
            Mage::register('channel_condition', $otgchannel);
            if (strcasecmp($otgchannel->getFilterBy(), 'sku') === 0) {
                $skus = explode(',', $otgchannel->getSkuList());
            } else {
                $skus = Mage::getmodel('bridge/rule')->getMatchingProductIds();
            }
            $attributes = Mage::getModel('bridge/outgoingchannels')->getAttributes($skus);
            $catagories = Mage::getModel('bridge/outgoingchannels')->getCategories();
            $stores = Mage::getModel('bridge/outgoingchannels')->getStores();
            $iDefaultStoreId = Mage::app()
                    ->getWebsite()
                    ->getDefaultGroup()
                    ->getDefaultStoreId();
//get the category set
            $result = array('categories' => serialize($catagories), 'attributes' => serialize($attributes), 'store' => serialize($stores), 'default_store' => $iDefaultStoreId, 'created' => date('Y-m-d H:i:s'));
            return $result;
        } else {
            return "Sorry invalid request";
        }
    }

     /**
     * validate the current request if v
     * valid the return the channel details other wise return false
     */
    private function validateRequest() {
        $consumer_key = Mage::helper('bridge')->getOauthConsumerKey(Mage::app()->getRequest()->getHeader('Authorization'));
//get the consumer id of parameter secret
        $consume_id = Mage::getModel('oauth/consumer')->load($consumer_key, 'key')->getId();
//get the outgoing channel of the given consumer id     
        $otgchannel = Mage::getModel('bridge/outgoingchannels')->getChannelDetails($consume_id);
        $url=  parse_url($otgchannel->getUrl());
        $callbackurl=  parse_url($otgchannel->getCallbackUrl());
       if ($otgchannel->getId() && $url['host'] == $callbackurl['host'] && $otgchannel->getChannelStatus()) {
//check for salt
            if ($this->getRequest()->getParam('salt_type') == "paid" && base64_encode($otgchannel->getSalt()) == $this->getRequest()->getParam('salt')) {
                return $otgchannel;
            } else if ($this->getRequest()->getParam('salt_type') == "free") {
                if (Mage::helper('core')->isModuleEnabled('Fedobe_Securitycheck')) {
                    $securitysalt = Mage::getModel('securitycheck/salt')->loadByOutgoingChannel($otgchannel->getId())->getSalt();
                    if ($securitysalt && base64_encode($securitysalt) == $this->getRequest()->getParam('salt'))
                        return $otgchannel;
                }
            }
        }

        return false;
    }

}

?>