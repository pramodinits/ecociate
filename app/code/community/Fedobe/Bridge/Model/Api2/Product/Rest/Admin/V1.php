<?php

class Fedobe_Bridge_Model_Api2_Product_Rest_Admin_V1 extends Mage_Api2_Model_Resource {

    public function _retrieveCollection() {
//        return $this->getRequest()->getParams();
//apache_request_headers ();
//check if the request channel is  valid or not
        if ($otgchannel = $this->validateRequest()) {
            $result = "No Result";
            if ($this->getRequest()->getParam('fetch') === 'mappingdata' && $otgchannel->getProductInfo() && ($otgchannel->getCreateNew() || $otgchannel->getUpdateExist())) {
                $result = $this->getMappingData($otgchannel);
            } else if ($this->getRequest()->getParam('fetch') === 'batchsku' && $otgchannel->getProductInfo() && ($otgchannel->getCreateNew() || $otgchannel->getUpdateExist())) {
                $result = $this->getBatchSKU($otgchannel);
            } else if ($this->getRequest()->getParam('fetch') === 'pim' && $otgchannel->getProductInfo() && ($otgchannel->getCreateNew() || $otgchannel->getUpdateExist())) {
                $result = $this->getPIM($otgchannel);
            } else if ($this->getRequest()->getParam('fetch') === 'edi' && $otgchannel->getProductPricenquantity()) {
                $result = $this->getEDI($otgchannel);
            } else if ($this->getRequest()->getParam('fetch') === 'edisku' && $otgchannel->getProductPricenquantity()) {
                $result = $this->updateBatchSKU($otgchannel);
            }
            return $result;
        } else {
            return "Sorry invalid request";
        }
    }

    public function getBatchSKU($otgchannel, $return_type = NULL) {
        $sku_list = array();
        if (strcasecmp($otgchannel->getFilterBy(), 'sku') === 0) {
            $product_ids = explode(',', $otgchannel->getSkuList());
        } else {
            Mage::register('channel_condition', $otgchannel);
            $product_ids = Mage::getmodel('bridge/rule')->getMatchingProductIds();
            $product_status = $otgchannel->getProductStatus();
            if ($product_status)
                $product_ids = Mage::getmodel('bridge/outgoingchannels')->getFilteredSkus($product_ids, $product_status);
        }
        if (strcasecmp($return_type, 'id') === 0)
            return $product_ids;
        $poduct_skus = $this->getSkuById($product_ids);
        if (strcasecmp($return_type, 'sku') === 0)
            return $poduct_skus;
        $sku_list['skus'] = serialize($poduct_skus);
        $sku_list['channel_info'] = serialize(array('create_new' => $otgchannel['create_new'], 'update_exist' => $otgchannel['update_exist']));
        return $sku_list;
    }

    /**
     * update the batch sku table for edi
     * 
     */
    public function updateBatchSKU($otgchannel) {
        $sku_list = $this->getBatchSKU($otgchannel, 'sku');
        $sku_list = $sku_list ? $sku_list : 0;
        Mage::getmodel('bridge/outgoingchannels_skus')->insert($sku_list, $otgchannel['id']);
        $sku_list = Mage::getmodel('bridge/outgoingchannels_skus')->getCollection()
                ->addFieldToFilter('channel_id', $otgchannel['id'])
                ->addFieldToSelect('sku')
                ->getData();
        $sku_list = array_unique(array_column($sku_list, 'sku'));
        return $sku_list;
    }

    public function getPIM($otgchannel) {
//        return $this->getRequest()->getParams('sku');
        $sku = trim(urldecode($this->getRequest()->getParam('sku')));
        if ($this->getRequest()->getParam('relation')) {
            $sku_withparent = explode(',', $sku);
            $sku = $sku_withparent[0];
            $parent_sku = $sku_withparent[1];
            $parent_product_ext = Mage::getmodel('catalog/product')->loadByAttribute('sku', $parent_sku);
            if (!$parent_product_ext->getId()) {
                return array('Parent SKU for ' . $this->getRequest()->getParam('relation') . ' NOT Valid');
            }
        }
        $product_ext = Mage::getmodel('catalog/product')->loadByAttribute('sku', $sku);
        if (!$product_ext->getId()) {
            return array('Invalid SKU');
        }
        $match_product_ids = $this->getBatchSKU($otgchannel, 'id');
        $sku_list = $this->getSkuById($match_product_ids);
        $product_info = array();
        $updated = true;
//check the sku fall in current conditon or not
        if (!$this->getRequest()->getParam('relation')) {
            if (!in_array($sku, $sku_list))
                return array('Access Denied For This SKU');
        } else if ($this->getRequest()->getParam('relation') == 'related') {
            if (!$otgchannel->getRelativeProduct() & 1)
                return array('Related Products Are Not Allowed TO Sync');
            $related = $parent_product_ext->getRelatedProductIds();
            if (!in_array($product_ext->getId(), $related) || ($otgchannel->getCondApplyOnRelatgetCrossSellProductIds() && !in_array($product_ext->getId(), $match_product_ids))) {
                return array('Access Denied For This SKU');
            }
        } else if ($this->getRequest()->getParam('relation') == 'upsell') {
            if (!$otgchannel->getRelativeProduct() & 2)
                return array('Up-sell Products Are Not Allowed TO Sync');
            $upsell = $parent_product_ext->getUpSellProductIds();
            if (!in_array($product_ext->getId(), $upsell) || ($otgchannel->getCondApplyOnRelatgetCrossSellProductIds() && !in_array($product_ext->getId(), $match_product_ids))) {
                return array('Access Denied For This SKU');
            }
        } else if ($this->getRequest()->getParam('relation') == 'crosssell') {
            if (!$otgchannel->getRelativeProduct() & 4)
                return array('Cross-sell Products Are Not Allowed TO Sync');
            $crosssell = $parent_product_ext->getCrossSellProductIds();
            if (!in_array($product_ext->getId(), $crosssell) || ($otgchannel->getCondApplyOnRelatgetCrossSellProductIds() && !in_array($product_ext->getId(), $match_product_ids))) {
                return array('Access Denied For This SKU');
            }
        } else if ($this->getRequest()->getParam('relation') == 'associated') {
            if (!$parent_product_ext->isConfigurable())
                return array('Parent SKU ' . $parent_product_ext->getSku() . " is not Configurable Product");
            $childProductsId = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($parent_product_ext->getId());
            if (!in_array($product_ext->getId(), $childProductsId[0])) {
                return array('Access Denied For This SKU');
            }
        }

//end
//get store wise product information
        $remove_fields = array('inc_name', 'inc_description', 'inc_short_description', 'channel_priority', 'bridge_channel_id', 'bridge_pricenqty_channel_id', 'bridge_content_edited', 'price', 'special_price', 'msrp', 'meta_title', 'meta_description', 'url_path', 'ogc_price', 'created_at', 'updated_at', 'meta_keyword', 'special_from_date', 'special_to_date', 'url_key', 'media_gallery', 'group_price', 'group_price_changed', 'tier_price', 'tier_price_changed', 'gift_wrapping_price');
        if ($otgchannel->getContentSource() != 'original') {
            $remove_fields = array_merge($remove_fields, array('name', 'description', 'short_description'));
        }
        $updated_at = strtotime(str_replace("_", " ", $this->getRequest()->getParam('updated')));
        $remove_fields = array_flip($remove_fields);
        $store_ids = explode(',', $this->getRequest()->getParam('store'));
        $entityId = $product_ext->getId();
        foreach ($store_ids as $storeId) {
            $store_product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($entityId);
            $product_data = $store_product->getData();
            unset($product_data['stock_item']);
            $product_data = array_diff_key($product_data, $remove_fields);

//            if ($otgchannel->getContentSource() == 'copy_or_original') {
//                $product_data['name'] = $product_data['ogc_name'] ? $product_data['ogc_name'] : $product_data['name'];
//                $product_data['description'] = $product_data['ogc_description'] ? $product_data['ogc_description'] : $product_data['description'];
//                $product_data['short_description'] = $product_data['ogc_short_description'] ? $product_data['ogc_short_description'] : $product_data['short_description'];
//                unset($product_data['ogc_name']);
//                unset($product_data['ogc_description']);
//                unset($product_data['ogc_short_description']);
//            }
//Here goes for content rule
            if ($otgchannel->getContentSource() == 'contentrule') {
                $contentruledata = Mage::helper('bridge')->getConvertedString($otgchannel, $entityId, 'ogc', $storeId);
                $product_data['name'] = $contentruledata['name'];
                $product_data['description'] = $contentruledata['description'];
                $product_data['short_description'] = $contentruledata['short_description'];
            } else if ($otgchannel->getContentSource() == 'copy') {
                $product_data['name'] = $product_data['ogc_name'];
                $product_data['description'] = $product_data['ogc_description'];
                $product_data['short_description'] = $product_data['ogc_short_description'];
            } else if ($otgchannel->getContentSource() == 'contentrule_or_copy') {
                $contentruledata = Mage::helper('bridge')->getConvertedString($otgchannel, $entityId, 'ogc', $storeId);
                $product_data['name'] = $contentruledata['name'] ? $contentruledata['name'] : $product_data['ogc_name'];
                $product_data['description'] = $contentruledata['description'] ? $contentruledata['description'] : $product_data['ogc_description'];
                $product_data['short_description'] = $contentruledata['short_description'] ? $contentruledata['short_description'] : $product_data['ogc_short_description'];
            } else if ($otgchannel->getContentSource() == 'copy_or_contentrule') {
                if (!$product_data['ogc_name'] || !$product_data['ogc_description'] || !$product_data['ogc_short_description'])
                    $contentruledata = Mage::helper('bridge')->getConvertedString($otgchannel, $entityId, 'ogc', $storeId);
                $product_data['name'] = $product_data['ogc_name'] ? $product_data['ogc_name'] : $contentruledata['name'];
                $product_data['description'] = $product_data['ogc_description'] ? $product_data['ogc_description'] : $contentruledata['description'];
                $product_data['short_description'] = $product_data['ogc_short_description'] ? $product_data['ogc_short_description'] : $contentruledata['short_description'];
            }
            unset($product_data['ogc_name']);
            unset($product_data['ogc_description']);
            unset($product_data['ogc_short_description']);
            if ($product_data) {
//check any updation for this sku is done or not after last sync date
                $product_updated = strtotime($product_data['updated_at']);
//                if ($product_updated - $updated_at <= 0) {
//                    $updated = false;
//                    break;
//                }
                $entityId = $product_data['entity_id'];
                $pim['store'][$storeId] = $product_data;
            }
        }
//get product image and categories and check if configurable product then get attribute variation
//            return $updated;
        if ($entityId && $updated) {
//            $product_image = Mage::getmodel('catalog/product')->load($entityId);

            $images = $store_product->getData('media_gallery');
            foreach ($images['images'] as $image) {
                $image_types = $this->_getImageTypesAssignedToProduct($store_product, $image['file']);
                if ($image_types) {
                    $result[] = array(
                        'id' => $image['value_id'],
                        'label' => $image['label'],
                        'position' => $image['position'],
                        'exclude' => $image['disabled'],
                        'url' => Mage::getSingleton('catalog/product_media_config')->getMediaUrl($image['file']),
                        'types' => $image_types
                    );
                }
            }
            $pim['image'] = $result;
            $pim['categories'] = $product_ext->getCategoryIds();
            if ($product_ext->isConfigurable()) {
                $configAttr = $product_ext->getTypeInstance(true)->getUsedProductAttributeIds($product_ext);
                $pim['ConfigAttr'] = $configAttr;
                $childProductsId = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($product_ext->getId());
                if (@$childProductsId[0])
                    $product_info['associated'] = serialize($this->getSkuById($childProductsId[0]));
//                $product_info['associated'] = //$this->getSkuById($childProductsId[0]);
            }
            $product_info['pim'] = serialize($pim);
            $product_info['updated_at'] = $product_ext->getUpdatedAt();
            if (!$this->getRequest()->getParam('relation')) {
//get relative products
                if (($otgchannel->getRelativeProduct() & 1) && ($this->getRequest()->getParam('relative_product') & 1)) {
                    $relative_product = $product_ext->getRelatedProductIds();
                    if ($otgchannel->getCondApplyOnRelativeProducts()) {
                        $relative_product = array_intersect($match_product_ids, $relative_product);
                    }
                    if ($relative_product)
                        $product_info['related'] = serialize($this->getSkuById($relative_product));
                }
//get upsell products
                if (($otgchannel->getRelativeProduct() & 2) && ($this->getRequest()->getParam('relative_product') & 2)) {
                    $upsell = $product_ext->getUpSellProductIds();
                    if ($otgchannel->getCondApplyOnRelativeProducts()) {
                        $upsell = array_intersect($match_product_ids, $upsell);
                    }
                    if ($upsell)
                        $product_info['upsell'] = serialize($this->getSkuById($upsell));
                }
//get crosssell products
                if (($otgchannel->getRelativeProduct() & 4) && ($this->getRequest()->getParam('relative_product') & 4)) {
                    $crosssell = $product_ext->getCrossSellProductIds();
                    if ($otgchannel->getCondApplyOnRelativeProducts()) {
                        $crosssell = array_intersect($match_product_ids, $crosssell);
                    }
                    if ($crosssell)
                        $product_info['crosssell'] = serialize($this->getSkuById($crosssell));
                }
            }
            $product_info['channel_info'] = serialize(array('create_new' => $otgchannel['create_new'], 'update_exist' => $otgchannel['update_exist']));
            if ($otgchannel->getProductPricenquantity()) {
                if (!$product_ext->isConfigurable()) {
                    $edi['price'] = Mage::helper('bridge')->getConvertedPrice($otgchannel->getId(), $product_ext->getSku());
                    if (!$edi['price']) {
                        $base_price = $otgchannel->getPriceSource() != "ogc_price" ? $product_ext[$otgchannel->getPriceSource()] : ($product_ext['ogc_price'] ? $product_ext['ogc_price'] : ($product_ext['price'] ? $product_ext['price'] : $product_ext['msrp']));
                        $base_price = $base_price ? $base_price : 0;
                        $multiprice_data = array('id' => $otgchannel->getId(), 'multi_price_rule' => $otgchannel->getMultipriceRule());
                        $edi['price'] = Mage::helper('bridge')->addPriceRule($multiprice_data, $product_ext->getId(), $base_price, 'ogc');
                    }

//                    $edi['price'] = $this->applyPriceRule($otgchannel, $product_ext);
                    $edi['msrp'] = $product_ext->getMsrp();
                    $edi['qty'] = Mage::getModel('cataloginventory/stock_item')->loadByProduct($entityId)->getQty();
                    $product_info['edi'] = serialize($edi);
                }
            }
        }
        return $product_info;
    }

    public function getEDI($otgchannel) {
        $match_product_ids = $this->getBatchSKU($otgchannel, 'id');
        $current_matched = $this->getSkuById($match_product_ids);
        $sku_infos = Mage::getmodel('bridge/outgoingchannels_skus')->read_sku($otgchannel['id'], 10);
//        return json_encode($sku_infos);
        if (!$sku_infos)
            return "No SKU is available for EDI processing";
        $delete_ids = null;
        $qunatiy_price_infos = array();
        $linked_products = array();
        $deleted_skus = array();
        $multiprice_data = array('id' => $otgchannel->getId(), 'multi_price_rule' => $otgchannel->getMultipriceRule());
        foreach ($sku_infos as $sku_info) {
            if (@$sku_info['deleted']) {
                $qunatiy_price_infos[$sku_info['sku']] = serialize(array('price' => 0, 'qty' => 0, 'deleted' => 1));
                $deleted_skus[] = $sku_info['sku'];
                continue;
            }
            $product_infos = array();
            if ((is_null($sku_info['relation']) && !in_array($sku_info['sku'], $current_matched)) || (!is_null($sku_info['relation']) && !in_array($sku_info['parent_sku'], $current_matched))) {
                $delete_ids[] = $sku_info['id'];
            } else {
                $product = Mage::getmodel('catalog/product')->loadByAttribute('sku', $sku_info['sku']);
                if ($product->isConfigurable()) {
                    $childProductsId = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($product->getId());
                    if (@$childProductsId[0])
                        $linked_products[$sku_info['sku']]['associated'] = $this->getSkuById($childProductsId[0]);
//                $product_info['associated'] = //$this->getSkuById($childProductsId[0]);
                }
                if (is_null($sku_info['relation'])) {
//get relative products
                    if (($otgchannel->getRelativeProduct() & 1) && ($this->getRequest()->getParam('relative_product') & 1)) {
                        $relative_product = $product->getRelatedProductIds();
                        if ($otgchannel->getCondApplyOnRelativeProducts()) {
                            $relative_product = array_intersect($match_product_ids, $relative_product);
                        }
                        if ($relative_product)
                            $linked_products[$sku_info['sku']]['related'] = $this->getSkuById($relative_product);
                    }
//get upsell products
                    if (($otgchannel->getRelativeProduct() & 2) && ($this->getRequest()->getParam('relative_product') & 2)) {
                        $upsell = $product->getUpSellProductIds();
                        if ($otgchannel->getCondApplyOnRelativeProducts()) {
                            $upsell = array_intersect($match_product_ids, $upsell);
                        }
                        if ($upsell)
                            $linked_products[$sku_info['sku']]['upsell'] = $this->getSkuById($upsell);
                    }
//get crosssell products
                    if (($otgchannel->getRelativeProduct() & 4) && ($this->getRequest()->getParam('relative_product') & 4)) {
                        $crosssell = $product->getCrossSellProductIds();
                        if ($otgchannel->getCondApplyOnRelativeProducts()) {
                            $crosssell = array_intersect($match_product_ids, $crosssell);
                        }
                        if ($crosssell)
                            $linked_products[$sku_info['sku']]['crosssell'] = $this->getSkuById($crosssell);
                    }
                }
                $edi['price'] = Mage::helper('bridge')->getConvertedPrice($otgchannel->getId(), $product->getSku());
                if (!$edi['price']) {
                    $base_price = $otgchannel->getPriceSource() != "ogc_price" ? $product[$otgchannel->getPriceSource()] : ($product['ogc_price'] ? $product['ogc_price'] : ($product['price'] ? $product['price'] : $product['msrp']));
                    $base_price = $base_price ? $base_price : 0;
                    $product_infos['price'] = Mage::helper('bridge')->addPriceRule($multiprice_data, $product->getId(), $base_price, 'ogc');
                }
//                $product_infos['price']=$this->applyPriceRule($otgchannel, $product);

                $product_infos['qty'] = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty();
                $qunatiy_price_infos[$sku_info['sku']] = serialize($product_infos);
            }
        }
        if ($linked_products)
            Mage::getmodel('bridge/outgoingchannels_skus')->insert($linked_products, $otgchannel['id'], 1);
        Mage::getmodel('bridge/outgoingchannels_skus')->updateProcessStatus($otgchannel['id'], array_keys($qunatiy_price_infos));
        //delete outgoing inventory deltails which are actually deleted in my store but delted status for this sku is not synced for this 
//        channel
        if ($deleted_skus)
             Mage::getmodel('bridge/inventory')->deleteNonExistProduct($deleted_skus, $otgchannel->getId());
        if ($delete_ids)
            Mage::getmodel('bridge/outgoingchannels_skus')->deleteNotMatchedProductById($delete_ids);
        return $qunatiy_price_infos;
    }

    /**
     * Retrieve image types assigned to product (base, small, thumbnail)
     *
     * @param object $product
     * @param string $imageFile
     * @return array
     */
    protected function _getImageTypesAssignedToProduct($product, $imageFile) {
        $types = array();
        foreach ($product->getMediaAttributes() as $attribute) {
            if ($product->getData($attribute->getAttributeCode()) == $imageFile) {
                $types[] = $attribute->getAttributeCode();
            }
        }
        return $types;
    }

    /**
     * validate the current request if v
     * valid the return the channel details other wise return false
     */
    private function validateRequest() {
        $consumer_key = Mage::helper('bridge')->getOauthConsumerKey(Mage::app()->getRequest()->getHeader('Authorization'));
//get the consumer id of parameter secret
        $consume_id = Mage::getModel('oauth/consumer')->load($consumer_key, 'key')->getId();
//get the outgoing channel of the given consumer id     
        $otgchannel = Mage::getModel('bridge/outgoingchannels')->getChannelDetails($consume_id);
        $url = parse_url($otgchannel->getUrl());
        $callbackurl = parse_url($otgchannel->getCallbackUrl());
        if ($otgchannel->getId() && $url['host'] == $callbackurl['host'] && $otgchannel->getChannelStatus()) {
//check for salt
            if ($this->getRequest()->getParam('salt_type') == "paid" && base64_encode($otgchannel->getSalt()) == $this->getRequest()->getParam('salt')) {
                return $otgchannel;
            } else if ($this->getRequest()->getParam('salt_type') == "free") {
                if (Mage::helper('core')->isModuleEnabled('Fedobe_Securitycheck')) {
                    $securitysalt = Mage::getModel('securitycheck/salt')->loadByOutgoingChannel($otgchannel->getId())->getSalt();
                    if ($securitysalt && base64_encode($securitysalt) == $this->getRequest()->getParam('salt'))
                        return $otgchannel;
                }
            }
        }

        return false;
    }

    /**
     * get sku list by product id
     * @param type $entity_ids
     * @return type array of sku
     */
    private function getSkuById($entity_ids) {
        $product_skus = Mage::getmodel('catalog/product')->getCollection()
                        ->addAttributeToFilter('entity_id', array('in' => $entity_ids))
                        ->addAttributeToFilter('type_id', array('in' => array('simple', 'configurable')))
                        ->addAttributeToSelect('sku')->getData();
        return array_column($product_skus, 'sku', 'entity_id');
    }

    public function getMappingData($otgchannel) {
        Mage::register('channel_condition', $otgchannel);
        if (strcasecmp($otgchannel->getFilterBy(), 'sku') === 0) {
            $skus = explode(',', $otgchannel->getSkuList());
        } else {
            $skus = Mage::getmodel('bridge/rule')->getMatchingProductIds();
        }
        $attributes = Mage::getModel('bridge/outgoingchannels')->getAttributes($skus);
        $catagories = Mage::getModel('bridge/outgoingchannels')->getCategories();
        $stores = Mage::getModel('bridge/outgoingchannels')->getStores();
        $iDefaultStoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
//get the category set
        $result = array('categories' => serialize($catagories), 'attributes' => serialize($attributes), 'store' => serialize($stores), 'default_store' => $iDefaultStoreId, 'created' => date('Y-m-d H:i:s'));
        return $result;
    }

}

?>