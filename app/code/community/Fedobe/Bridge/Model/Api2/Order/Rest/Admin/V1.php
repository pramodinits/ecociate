<?php

class Fedobe_Bridge_Model_Api2_Order_Rest_Admin_V1 extends Fedobe_Bridge_Model_Api2_Order {

    public function _retrieveCollection() {
        return '$data';
    }

    public function _retrieve() {
        return '$data';
    }

    /**
     * Create order
     *
     * @param array $data
     * @return string
     */
    public function createMulti(array $data) {
        return $data;
//        return array('category_id' => 45);
        $firstName = $data['firstname'];
        $lastName = $data['lastname'];
        $email = $data['email'];
        $password = $data['password'];
        $customer = Mage::getModel("customer/customer");
        $customer->setFirstname($firstName);
        $customer->setLastname($lastName);
        $customer->setEmail($email);
        $customer->setPasswordHash(md5($password));
        $customer->save();
        return $this->_getLocation($customer);
    }

    public function dispatch() {
        switch ($this->getActionType() . $this->getOperation()) {
            /* Create */
            case self::ACTION_TYPE_ENTITY . self::OPERATION_CREATE:
                // Creation of objects is possible only when working with collection
                $this->_critical(self::RESOURCE_METHOD_NOT_IMPLEMENTED);
                break;
            case self::ACTION_TYPE_COLLECTION . self::OPERATION_CREATE:
                // If no of the methods(multi or single) is implemented, request body is not checked
                if (!$this->_checkMethodExist('_create') && !$this->_checkMethodExist('_multiCreate')) {
                    $this->_critical(self::RESOURCE_METHOD_NOT_IMPLEMENTED);
                }
                // If one of the methods(multi or single) is implemented, request body must not be empty
                $requestData = $this->getRequest()->getBodyParams();
                if (empty($requestData)) {
                    $this->_critical(self::RESOURCE_REQUEST_DATA_INVALID);
                }
                // The create action has the dynamic type which depends on data in the request body
                if ($this->getRequest()->isAssocArrayInRequestBody()) {
                    $this->_errorIfMethodNotExist('_create');
                    $filteredData = $this->getFilter()->in($requestData);
                    if (empty($filteredData)) {
                        $this->_critical(self::RESOURCE_REQUEST_DATA_INVALID);
                    }
                    $newItemLocation = $this->_create($filteredData);
                    $filteredData = $this->getFilter()->out($newItemLocation);
                    $this->_render($filteredData);
//                    $this->getRe  sponse()->setHeader('Location', $newItemLocation);
                } else {
                    $this->_errorIfMethodNotExist('_multiCreate');
                    $filteredData = $this->getFilter()->collectionIn($requestData);
                    $this->_multiCreate($filteredData);
                    $this->_render($this->getResponse()->getMessages());
                    $this->getResponse()->setHttpResponseCode(Mage_Api2_Model_Server::HTTP_MULTI_STATUS);
                }
                break;
            /* Retrieve */
            case self::ACTION_TYPE_ENTITY . self::OPERATION_RETRIEVE:
                $this->_errorIfMethodNotExist('_retrieve');
                $retrievedData = $this->_retrieve();
                $filteredData = $this->getFilter()->out($retrievedData);
                $this->_render($filteredData);
                break;
            case self::ACTION_TYPE_COLLECTION . self::OPERATION_RETRIEVE:
                $this->_errorIfMethodNotExist('_retrieveCollection');
                $retrievedData = $this->_retrieveCollection();
                $filteredData = $this->getFilter()->collectionOut($retrievedData);
                $this->_render($filteredData);
                break;
            /* Update */
            case self::ACTION_TYPE_ENTITY . self::OPERATION_UPDATE:
                $this->_errorIfMethodNotExist('_update');
                $requestData = $this->getRequest()->getBodyParams();
                if (empty($requestData)) {
                    $this->_critical(self::RESOURCE_REQUEST_DATA_INVALID);
                }
                $filteredData = $this->getFilter()->in($requestData);
                if (empty($filteredData)) {
                    $this->_critical(self::RESOURCE_REQUEST_DATA_INVALID);
                }
                $this->_update($filteredData);
                break;
            case self::ACTION_TYPE_COLLECTION . self::OPERATION_UPDATE:
                $this->_errorIfMethodNotExist('_multiUpdate');
                $requestData = $this->getRequest()->getBodyParams();
                if (empty($requestData)) {
                    $this->_critical(self::RESOURCE_REQUEST_DATA_INVALID);
                }
                $filteredData = $this->getFilter()->collectionIn($requestData);
                $this->_multiUpdate($filteredData);
                $this->_render($this->getResponse()->getMessages());
                $this->getResponse()->setHttpResponseCode(Mage_Api2_Model_Server::HTTP_MULTI_STATUS);
                break;
            /* Delete */
            case self::ACTION_TYPE_ENTITY . self::OPERATION_DELETE:
                $this->_errorIfMethodNotExist('_delete');
                $this->_delete();
                break;
            case self::ACTION_TYPE_COLLECTION . self::OPERATION_DELETE:
                $this->_errorIfMethodNotExist('_multiDelete');
                $requestData = $this->getRequest()->getBodyParams();
                if (empty($requestData)) {
                    $this->_critical(self::RESOURCE_REQUEST_DATA_INVALID);
                }
                $this->_multiDelete($requestData);
                $this->getResponse()->setHttpResponseCode(Mage_Api2_Model_Server::HTTP_MULTI_STATUS);
                break;
            default:
                $this->_critical(self::RESOURCE_METHOD_NOT_IMPLEMENTED);
                break;
        }
    }

    public function _create(array $data) {
        return $data;
        $firstName = $data['firstname'];
        $lastName = $data['lastname'];
        $email = $data['email'];
        $password = $data['password'];
        $customer = Mage::getModel("customer/customer");
        $customer->setFirstname($firstName);
        $customer->setLastname($lastName);
        $customer->setEmail($email);
        $customer->setPasswordHash(md5($password));
        $customer->save();
        return $this->_getLocation($customer);
    }

    /**
     * Update order
     *
     * @param array $data
     */
    public function _update(array $data) {
        return $data;
    }

    /**
     * validate the current request if v
     * valid the return the channel details other wise return false
     */
    private function validateRequest() {
        $consumer_key = Mage::helper('bridge')->getOauthConsumerKey(Mage::app()->getRequest()->getHeader('Authorization'));
//get the consumer id of parameter secret
        $consume_id = Mage::getModel('oauth/consumer')->load($consumer_key, 'key')->getId();
//get the outgoing channel of the given consumer id     
        $otgchannel = Mage::getModel('bridge/outgoingchannels')->getChannelDetails($consume_id);
        $url = parse_url($otgchannel->getUrl());
        $callbackurl = parse_url($otgchannel->getCallbackUrl());
        if ($otgchannel->getId() && $url['host'] == $callbackurl['host'] && $otgchannel->getChannelStatus()) {
//check for salt
            if ($this->getRequest()->getParam('salt_type') == "paid" && base64_encode($otgchannel->getSalt()) == $this->getRequest()->getParam('salt')) {
                return $otgchannel;
            } else if ($this->getRequest()->getParam('salt_type') == "free") {
                if (Mage::helper('core')->isModuleEnabled('Fedobe_Securitycheck')) {
                    $securitysalt = Mage::getModel('securitycheck/salt')->loadByOutgoingChannel($otgchannel->getId())->getSalt();
                    if ($securitysalt && base64_encode($securitysalt) == $this->getRequest()->getParam('salt'))
                        return $otgchannel;
                }
            }
        }

        return false;
    }

}

?>