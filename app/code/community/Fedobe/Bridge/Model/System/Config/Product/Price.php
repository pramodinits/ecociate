<?php

class Fedobe_Bridge_Model_System_Config_Product_Price {

   public function toOptionArray() {
        return array(
            array('value' => 'ogc_price', 'label' => 'Wholesale Price'),
            array('value' => "price", 'label' => "Selling Price"),
            array('value' => "msrp", 'label' => "MSRP")
            );
    }

}
