<?php

class Fedobe_Bridge_Model_System_Config_Product_Condition {

   public function toOptionArray() {
        return array(
            array('value' => "condition", 'label' => "Condition"),
            array('value' => 'sku', 'label' => 'SKU')
            );
    }

}
