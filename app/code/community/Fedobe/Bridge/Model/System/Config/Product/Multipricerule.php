<?php

class Fedobe_Bridge_Model_System_Config_Product_Multipricerule {

   public function toOptionArray() {
        return array(
            array('value' => 'max_price', 'label' => 'Take Max Price Only'),
            array('value' => "min_price", 'label' => "Take Min Price Only"),
            array('value' => "base_price", 'label' => "Apply Multiple on Base Price"),
            array('value' => "cumulative_price", 'label' => "Apply Multiple on Cumulative Price")
            );
    }

}
