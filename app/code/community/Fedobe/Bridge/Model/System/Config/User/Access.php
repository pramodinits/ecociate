<?php

class Fedobe_Bridge_Model_System_Config_User_Access {

   public function toOptionArray() {
        return array(
            array('value' => 'not_visible', 'label' => 'Not Visible'),
            array('value' => "view_only", 'label' => "View Only"),
            array('value' => "allow_editing", 'label' => "Allow Editing")
            );
    }

}
