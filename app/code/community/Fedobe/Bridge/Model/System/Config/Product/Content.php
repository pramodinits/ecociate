<?php

class Fedobe_Bridge_Model_System_Config_Product_Content {

   public function toOptionArray() {
        return array(
            array('value' => "copy", 'label' => "Outgoing Content Only"),
            array('value' => 'original', 'label' => 'Original Content Only'),
            array('value' => 'copy_or_original', 'label' => 'Outgoing else Original Content'),
            );
    }

}
