<?php

class Fedobe_Bridge_Model_Incomingchannels extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('bridge/incomingchannels');
    }

    public function delete($id) {
        $write_connection = Mage::getSingleton('core/resource')->getConnection('core_write');
        $read_connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        try {
            $incoming_model = $this->load($id);
            if (!$incoming_model->getId()) {
                return array('status' => 'error', 'msg' => "Unable to find the incomingchannel to delete");
            }
            $channel_type = $incoming_model->getChannelType();
            $helperobj = Mage::helper($channel_type);
            $method = "deleteChannel";
            if (method_exists($helperobj, $method)) {
                $write_connection->beginTransaction();
                //delete the channel type based things
                $helperobj->deleteChannel($id);
                $entityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
                //change the product as  out of stock that belong to these channel and change the bridge_pricenquantity_id and 
                $bridge_edi_attr = Mage::getResourceModel('eav/entity_attribute')
                        ->getIdByCode('catalog_product', 'bridge_pricenqty_channel_id');
                $bridge_pim_attr = Mage::getResourceModel('eav/entity_attribute')
                        ->getIdByCode('catalog_product', 'bridge_channel_id');

//change the pim source for current active products
                $write_connection->raw_query("UPDATE catalog_product_entity_int SET value=NULL WHERE attribute_id=$bridge_pim_attr AND entity_type_id=$entityTypeId AND value=$id;");

//change the inventory as out of stock for current active product from this channel
                $edi_products = $read_connection->fetchAll("SELECT entity_id FROM catalog_product_entity_int WHERE attribute_id=$bridge_edi_attr AND entity_type_id=$entityTypeId AND value=$id;");
                $edi_entity_ids = $edi_products ? implode(',', array_column($edi_products, 'entity_id')) : 0;
//                echo $edi_entity_ids;exit;
                if ($edi_entity_ids)
                    $write_connection->raw_query("UPDATE cataloginventory_stock_item SET is_in_stock=0 WHERE product_id IN ($edi_entity_ids);");

                //delete data from edi
                $write_connection->raw_query("DELETE FROM bridge_incomingchannel_product_info WHERE channel_type='incoming' AND channel_id=$id;");
                //delete data from log
                $write_connection->raw_query("DELETE FROM bridge_incominglog WHERE channel_type='incoming' AND bridge_channel_id=$id;");
                //delete data from history
                $write_connection->raw_query("DELETE FROM bridge_incomingchannels_history WHERE channel_type='incoming' AND brand_id=$id;");
                //delete channel name option
                $optionsDelete = array();
                $optionsDelete['delete'][$incoming_model->getOptionId()] = true;
                $optionsDelete['value'][$incoming_model->getOptionId()] = true;

                $installer = new Mage_Eav_Model_Entity_Setup('core_setup');
                $installer->addAttributeOption($optionsDelete);
                //delte the incoming channel
                $write_connection->raw_query("DELETE FROM bridge_incomingchannels WHERE id=$id;");
                $write_connection->commit();
            } else {
                return array('status' => 'error', 'msg' => "Sorry $channel_type Addon have restriction to delete Channel.");
            }
            // Make saves and other actions that affect the database
        } catch (Exception $e) {
            $write_connection->rollback();
            return array('status' => 'error', 'msg' => $e->getMessage());
        }
        return array('status' => 'success', 'msg' => "The Incomingchannel has been deleted.");
    }

    /**
     * Load incomingchannel  by id
     *
     * @param intiger $id
     */
    public function loadById($id) {
        $this->setData($this->getResource()->loadById($id));
        return $this;
    }

    /**
     * //get all attribute set with $systm_attr parameter for including system attribute
     * @param type $product_ids
     * @return type
     */
    public function getAttributes($systm_attr = 0) {
        // Gets the current store's id
        $defaultstoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
        $entityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
        $quantity_attribute = array('attribute_id' => 'fedobe_quantity', 'attribute_code' => 'fedobe_quantity', 'frontend_input' => 'text', 'frontend_label' => 'quantity');
        $availability_attribute = array('attribute_id' => 'fedobe_availability', 'attribute_code' => 'fedobe_availability', 'frontend_input' => 'text', 'frontend_label' => 'fedobe_availability');
        $store_id = Mage::app()->getRequest()->getParam('store') ? Mage::app()->getRequest()->getParam('store') : $defaultstoreId;
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        if ($systm_attr) {
            $bridge_attributes = Mage::helper('bridge')->getBridgeAttributes();
            $bridge_attributes = "'" . implode("','", $bridge_attributes) . "'";
            $attr_sql = "SELECT entity_attribute.attribute_id,main_table.attribute_code,main_table.frontend_input,main_table.frontend_label,label.value,entity_attribute.attribute_set_id FROM `eav_attribute`
 AS `main_table`
 INNER JOIN `eav_entity_attribute` AS `entity_attribute` ON entity_attribute.attribute_id = main_table
.attribute_id
 LEFT JOIN eav_attribute_label AS label ON label.attribute_id=entity_attribute.attribute_id AND label.store_id=$store_id WHERE main_table.entity_type_id=$entityTypeId AND main_table.frontend_input!='hidden' AND main_table.attribute_code NOT IN ($bridge_attributes) order by entity_attribute.attribute_set_id asc,main_table.attribute_code asc";
        } else {
            $attr_sql = "SELECT entity_attribute.attribute_id,main_table.attribute_code,main_table.frontend_input,main_table.frontend_label,label.value,entity_attribute.attribute_set_id FROM `eav_attribute`
 AS `main_table`
 INNER JOIN `eav_entity_attribute` AS `entity_attribute` ON entity_attribute.attribute_id = main_table
.attribute_id
 LEFT JOIN eav_attribute_label AS label ON label.attribute_id=entity_attribute.attribute_id AND label.store_id=$store_id WHERE main_table.entity_type_id=$entityTypeId AND main_table.frontend_input!='hidden' AND main_table.is_user_defined=1 order by entity_attribute.attribute_set_id asc,main_table.attribute_code asc";
        }
        $attributes = $readConnection->fetchAll($attr_sql);
        $attr_result = array();
        foreach ($attributes as $attribute) {
            $attr_set_id = $attribute['attribute_set_id'];
            unset($attribute['attribute_set_id']);
            if (!isset($attr_result[$attr_set_id])) {
                $attr_set_name = Mage::getModel("eav/entity_attribute_set")->getCollection()
                                ->addFieldToFilter('attribute_set_id', $attr_set_id)
                                ->addFieldToFilter('entity_type_id', $entityTypeId)
                                ->getFirstItem()->getAttributeSetName();
                if (!$attr_set_name)
                    continue;

                $attr_result[$attr_set_id]['name'] = $attr_set_name;
                if ($systm_attr)
                    $attr_result[$attr_set_id][] = $quantity_attribute;
                $attr_result[$attr_set_id][] = $availability_attribute;
            }
            $attr_result[$attr_set_id][] = $attribute;
        }
        return $attr_result;
    }

//get all attribute set with attribute including system attribute
    public function getAllAttributes() {

        // Gets the current store's id
        $defaultstoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
        $entityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
        $quantity_attribute = array('attribute_id' => 'fedobe_quantity', 'attribute_code' => 'fedobe_quantity', 'frontend_input' => 'text', 'frontend_label' => 'quantity');
        $availability_attribute = array('attribute_id' => 'fedobe_availability', 'attribute_code' => 'fedobe_availability', 'frontend_input' => 'text', 'frontend_label' => 'fedobe_availability');
        $store_id = Mage::app()->getRequest()->getParam('store') ? Mage::app()->getRequest()->getParam('store') : $defaultstoreId;
        $resource = Mage::getSingleton('core/resource');
        $bridge_attributes = Mage::helper('bridge')->getBridgeAttributes();
        $bridge_attributes = "'" . implode("','", $bridge_attributes) . "'";
        $readConnection = $resource->getConnection('core_read');
        $attr_sql = "SELECT entity_attribute.attribute_id,main_table.attribute_code,main_table.frontend_input,main_table.frontend_label,label.value,entity_attribute.attribute_set_id FROM `eav_attribute`
 AS `main_table`
 INNER JOIN `eav_entity_attribute` AS `entity_attribute` ON entity_attribute.attribute_id = main_table
.attribute_id
 LEFT JOIN eav_attribute_label AS label ON label.attribute_id=entity_attribute.attribute_id AND label.store_id=$store_id WHERE main_table.entity_type_id=$entityTypeId AND main_table.frontend_input!='hidden'  AND main_table.attribute_code NOT IN ($bridge_attributes) order by entity_attribute.attribute_set_id asc,main_table.attribute_code asc";
        $attributes = $readConnection->fetchAll($attr_sql);
        $attr_result = array();
        foreach ($attributes as $attribute) {
            $attribute['org_attribute_id'] = $attribute['attribute_id'];
            $attribute['attribute_id'] = $attribute['attribute_code'];
            $attr_set_id = $attribute['attribute_set_id'];
            unset($attribute['attribute_set_id']);
            if (!isset($attr_result[$attr_set_id])) {
                $attr_set_name = Mage::getModel("eav/entity_attribute_set")->getCollection()
                                ->addFieldToFilter('attribute_set_id', $attr_set_id)
                                ->addFieldToFilter('entity_type_id', $entityTypeId)
                                ->getFirstItem()->getAttributeSetName();
                if (!$attr_set_name)
                    continue;
                $attr_result[$attr_set_id]['name'] = $attr_set_name;
                $attr_result[$attr_set_id][] = $quantity_attribute;
                $attr_result[$attr_set_id][] = $availability_attribute;
            }
            $attr_result[$attr_set_id][] = $attribute;
        }

        return $attr_result;
    }

    public function getAttributeOptions($store_id) {
        $defaultstoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
        if (!$store_id)
            $store_id = $defaultstoreId;
        $resource = Mage::getSingleton('core/resource');
        $result = array();
        $readConnection = $resource->getConnection('core_read');
        $label_sql1 = "SELECT options.attribute_id,optvalues.option_id,optvalues.value FROM
            eav_attribute_option AS options JOIN eav_attribute_option_value  AS optvalues ON options.option_id=optvalues.option_id WHERE store_id=$store_id";
        $result = $readConnection->fetchAll($label_sql1);
        $option_id = implode(',', array_column($result, 'option_id'));
        if ($store_id != $defaultstoreId) {
            $option_id = $option_id ? $option_id : 0;
            $label_sql2 = "SELECT options.attribute_id,optvalues.option_id,optvalues.value FROM
            eav_attribute_option AS options JOIN eav_attribute_option_value  AS optvalues ON options.option_id=optvalues.option_id WHERE store_id=$defaultstoreId and optvalues.option_id not in($option_id)";
            $result2 = $readConnection->fetchAll($label_sql2);
            $result = array_merge($result, $result2);
        }
        return $result;
    }

    public function getOptions($attribute_ids, $attr_codes) {
        $resource = Mage::getSingleton('core/resource');
        $result = array();
        $readConnection = $resource->getConnection('core_read');
        $attribute_ids = implode(',', $attribute_ids);
        $label_sql = "SELECT options.attribute_id,optvalues.option_id,optvalues.value FROM
            eav_attribute_option AS options JOIN eav_attribute_option_value  AS optvalues ON options.option_id=optvalues.option_id WHERE attribute_id in ($attribute_ids) group by options.option_id";
        $store_option_data = $readConnection->fetchAll($label_sql);
        $option_data = array();
        foreach ($store_option_data as $store_data) {
            if (!isset($option_data[$attr_codes[$store_data['attribute_id']]]))
                $option_data[$attr_codes[$store_data['attribute_id']]] = array();
            $option_data[$attr_codes[$store_data['attribute_id']]][$store_data['option_id']] = $store_data['value'];
        }
        return $option_data;
    }

    /**
     * add option to attribute incoming_channel
     * @param type $brand_name
     */
    public function addChannelOption($brand_name) {
        $arg_attribute = 'channel_priority';
        $arg_value = $brand_name;

        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
        $attr = $attr_model->loadByCode('catalog_product', $arg_attribute);
        $attr_id = $attr->getAttributeId();

        $option['attribute_id'] = $attr_id;
        $option['value']['any_option_name'][0] = $arg_value;

        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttributeOption($option);
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $option_sql = "SELECT option.option_id FROM eav_attribute_option
 AS `option`
 INNER JOIN eav_attribute_option_value AS `option_value` ON option.option_id = option_value.option_id"
                . " WHERE option_value.value='{$brand_name}' AND option.attribute_id=$attr_id order by option.option_id desc";
        $result = $readConnection->fetchRow($option_sql);
        return $result['option_id'];
    }

    /**
     * add option to attribute incoming_channel
     * @param type $brand_name
     */
    public function getIncomingChannels($id = 0) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $inc_ids = '';
        if ($id)
            $inc_ids = $readConnection->fetchRow("SELECT GROUP_CONCAT(channel_id) as inc_ids FROM bridge_groups WHERE id!=$id");
        else
            $inc_ids = $readConnection->fetchRow("SELECT GROUP_CONCAT(channel_id) as inc_ids FROM bridge_groups");
        if (!@$inc_ids['inc_ids'])
            $inc_ids['inc_ids'] = 0;
        $inc_id_array = explode(',', @$inc_ids['inc_ids']);
        $inc_ids = implode(',', $inc_id_array);
        $option_sql = "SELECT id value,brand_name name FROM bridge_incomingchannels WHERE id not in($inc_ids) order by brand_name";
        $result = $readConnection->fetchAll($option_sql);
        if (!in_array('my_store', $inc_id_array)) {
            $result[] = array('value' => 'my_store', 'name' => 'My Store');
        }
        return $result;
    }

    /**
     * add option to attribute incoming_channel
     * @param type $brand_name
     */
    public function getIncomingChannels1D($id = 0) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $inc_ids = '';
        if ($id)
            $inc_ids = $readConnection->fetchRow("SELECT GROUP_CONCAT(channel_id) as inc_ids FROM bridge_groups WHERE id!=$id");
        else
            $inc_ids = $readConnection->fetchRow("SELECT GROUP_CONCAT(channel_id) as inc_ids FROM bridge_groups");
        if (!@$inc_ids['inc_ids'])
            $inc_ids['inc_ids'] = 0;
        $inc_id_array = explode(',', @$inc_ids['inc_ids']);
        $inc_ids = implode(',', @$inc_id_array);
        $option_sql = "SELECT id value,brand_name label FROM bridge_incomingchannels WHERE id not in($inc_ids) order by brand_name";
        $result = $readConnection->fetchAll($option_sql);
        if (!in_array('my_store', $inc_id_array))
            $result[] = array('value' => 'my_store', 'label' => 'My Store');
        return $result;
    }

    /**
     * create new option for a ttribute at the time of inventory sync if option not exist
     * @param type $attr_id
     * @param type $option_name
     * @return type option_id
     */
    public function add_new_option($attr_code, $option_name) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $attr_id = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', $attr_code);
        $option['attribute_id'] = $attr_id;
        $option['value']['any_option_name'][0] = $option_name;

        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttributeOption($option);
        $option_sql = "SELECT option.option_id FROM eav_attribute_option
 AS `option`
 INNER JOIN eav_attribute_option_value AS `option_value` ON option.option_id = option_value.option_id"
                . " WHERE option_value.value='{$option_name}' AND option.attribute_id=$attr_id order by option.option_id desc limit 0,1";
        $result = $readConnection->fetchRow($option_sql);
        return $result['option_id'];
    }

    public function checkUniqueWeightage($id, $weightage) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $sql = "SELECT id FROM bridge_incomingchannels WHERE weightage=$weightage";
        if ($id) {
            $sql = $sql . " AND id!=$id";
        }
        $sql = $sql . " LIMIT 0,1";
        $result = $readConnection->fetchRow($sql);
        return $result['id'] ? 1 : 0;
    }

    /* Fetch all attributes of an attributeset */

    public function getAttributeSetList() {
        $attribute_arr = array();
        $entityTypeId = Mage::getModel('eav/entity')
                ->setType('catalog_product')
                ->getTypeId();
        $attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection')
                ->setEntityTypeFilter($entityTypeId)
                ->load();
        foreach ($attributeSetCollection as $id => $attributeSet) {
            $name = $attributeSet->getAttributeSetName();
            $attribute_arr[$id] = $name;
        }
        return $attribute_arr;
    }

    /* Fetch all options of an attribute */

    public function getAttributeOpt($attr_code) {
        $attr_opt = array();
        $attribute_opt = Mage::getSingleton('eav/config')
                ->getAttribute(Mage_Catalog_Model_Product::ENTITY, $attr_code);

        if ($attribute_opt->usesSource()) {
            $options = array();
            $options_nw = array();
            $options = $attribute_opt->getSource()->getAllOptions(false);
            foreach ($options as $k => $v2) {
                if (!empty($v2['value']))
                    $attr_opt[$v2['value']] = $v2['label'];
            }
        }
        return $attr_opt;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    static public function getOptionArray() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $sql = "SELECT id,brand_name FROM bridge_incomingchannels";
        $result = $readConnection->fetchAll($sql);
        return array_column($result, 'brand_name', 'id');
    }

    public function getActiveChannelsForEDI() {
        $incomingchannel_ids = array(0);
        //get active incoming channel ids
        $incomingchannels = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('authorized', 1)
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('product_pricenquantity', 1)
                ->addFieldToFilter('is_hidden', 0)
                ->addFieldToSelect('id')
                ->getData();

        if ($incomingchannels) {
            $incomingchannel_ids = array_column($incomingchannels, 'id');
        }
        return $incomingchannel_ids;
    }

    static public function getAllChannelOptionArray() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $sql = "SELECT CONCAT('incoming',id) id,brand_name name FROM bridge_incomingchannels WHERE is_hidden=0;";
        $inc_result = $readConnection->fetchAll($sql);
        array_unshift($inc_result, array('id' => 'incoming', 'name' => 'Incoming Channel'));
        $ownstore = Mage::getStoreConfig('general/store_information/name');
        $ownstore = ($ownstore) ? $ownstore : "Own Store";
        array_unshift($inc_result, array('id' => '_0', 'name' => $ownstore));
        array_unshift($inc_result, array('id' => 'my_store', 'name' => 'My Store'));
        $sql = "SELECT CONCAT('outgoing',id) id,name FROM bridge_outgoingchannels";
        $ogc_result = $readConnection->fetchAll($sql);
        array_unshift($ogc_result, array('id' => 'outgoing', 'name' => 'Outgoing Channel'));
        $result = array_merge($inc_result, $ogc_result);
        return array_column($result, 'name', 'id');
    }
    static public function getSpPartnerChannelOptionArray($partner) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $sql = "SELECT CONCAT('incoming',id) id,brand_name name FROM bridge_incomingchannels WHERE is_hidden=0 AND channel_supplier=$partner;";
        $inc_result = $readConnection->fetchAll($sql);
//        array_unshift($inc_result, array('id' => 'incoming', 'name' => 'Incoming Channel'));
//        $ownstore = Mage::getStoreConfig('general/store_information/name');
//        $ownstore = ($ownstore) ? $ownstore : "Own Store";
//        array_unshift($inc_result, array('id' => '_0', 'name' => $ownstore));
//        array_unshift($inc_result, array('id' => 'my_store', 'name' => 'My Store'));
//        $sql = "SELECT CONCAT('outgoing',id) id,name FROM bridge_outgoingchannels";
//        $ogc_result = $readConnection->fetchAll($sql);
//        array_unshift($ogc_result, array('id' => 'outgoing', 'name' => 'Outgoing Channel'));
//        $result = array_merge($inc_result, $ogc_result);
        return array_column($inc_result, 'name', 'id');
    }

    public function savelog($values) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_incominglog');
        //echo $table;exit;
        $sql_inest = array(
            'status' => 'status=VALUES(status)',
            'status_code' => 'status_code=VALUES(status_code)',
            'updated_at' => 'updated_at= NOW()',
        );
        if (is_array($values)) {
            $values_str = '("' . implode('","', $values) . '",NOW(),NOW())';
            $columns_k = array_keys($values);
            $columns = '(' . implode(',', $columns_k) . ',created_at,updated_at)';
        } else {
            $columns = '(sku,bridge_channel_id,channel_type,status,status_code,created_at,updated_at)';
            $values_str = $values;
        }
        $sql = "INSERT INTO  $table " . $columns . " VALUES " . $values_str . ' ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id),'
                . implode(',', $sql_inest);

        $writeConnection->query($sql);
        return true;
    }
     public static function getBrandChannelOptionArray($partner){
        $brand_attr_code = (Mage::helper('core')->isModuleEnabled('Fedobe_Tribebrand')) ? Mage::helper('tribebrand')->getAttributeCode() :'manufacturer';
        $productCollection = Mage::getResourceModel('catalog/product_collection')
                        ->addAttributeToSelect(array('entity_id', 'sku'));
        $productCollection->addFieldToFilter("$brand_attr_code", array('eq'=>$partner));
        //$productCollection->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));
        //$productCollection->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        //Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($productCollection);
        $aliastbl = 'bridgeinfo';
        $producttbl = '`e`';
        $iDefaultStoreId = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();
        $productCollection->getSelect()->joinLeft(array("$aliastbl" => 'bridge_incomingchannel_product_info'), "($producttbl.entity_id=$aliastbl.product_entity_id OR $producttbl.sku=$aliastbl.sku) AND $producttbl.entity_type_id=4 ", array("$aliastbl.channel_id","$aliastbl.channel_type"));
        $productCollection->getSelect()->where("$aliastbl.channel_type <> 'outgoing'");
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $sql = $productCollection->getSelect()->__toString();
        $channelids = $foundchannel_ids = array();
        $result = $readConnection->fetchAll($sql);
        foreach($result as $k => $v){
            $channelids[$v['channel_id']] = $v['channel_id'];
        }
        if($channelids){
            $ids = implode(',',$channelids);
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $sql = "SELECT CONCAT('incoming',id) id,brand_name name,channel_supplier FROM bridge_incomingchannels WHERE is_hidden=0 AND FIND_IN_SET(id,'$ids');";
            $foundchannel_ids = $readConnection->fetchAll($sql);
        }
        $data = array('channels'=>array_column($foundchannel_ids, 'name', 'id'),'suppliers'=>array_column($foundchannel_ids, 'channel_supplier', 'channel_supplier'));
        return $data;
    }

}