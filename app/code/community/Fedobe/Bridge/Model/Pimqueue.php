<?php

class Fedobe_Bridge_Model_Pimqueue extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('bridge/pimqueue');
    }

    public function insert($sku_list, $incomingchannel_id, $option_id, $weightage, $relation = null) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');
        $attributeId = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'channel_priority');
        $bridge_channel_AttrId = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'bridge_channel_id');
        $mapping_sql = "SELECT data FROM bridge_incomingchannel_mapping WHERE brand_id=$incomingchannel_id";
        $mapping_data = $readConnection->fetchRow($mapping_sql);
        $store_mapping = unserialize($mapping_data['data'])['store'];
        if (!$store_mapping) {
            return false;
        }
        $stores = implode(",", $store_mapping);
        $insertcommend_inpim = "";
//        $
//        echo "<pre>";
//        print_r($sku_list);
//        exit;
//get the sku for which channel priority is set
        $sku_list_str = "'" . implode("','", $sku_list) . "'";
        $sku_priorities = array();
        $query = "SELECT attribute.value,attribute.store_id,product.sku FROM catalog_product_entity as product JOIN catalog_product_entity_text as attribute  ON attribute.entity_id=product.entity_id WHERE product.sku IN ($sku_list_str)  AND attribute.attribute_id=$attributeId AND attribute.store_id IN ($stores) AND attribute.value";
        $priority_channels = $readConnection->fetchAll($query);
        if ($priority_channels) {
            foreach ($priority_channels as $priority_channel) {
                $sku_priorities[$priority_channel['sku']][$priority_channel['store_id']] = $priority_channel['value'];
            }
        }
        $last_updated_skus = array();
        $query = "SELECT attribute.value,attribute.store_id,product.sku FROM catalog_product_entity as product JOIN catalog_product_entity_int as attribute  ON attribute.entity_id=product.entity_id WHERE product.sku IN ($sku_list_str)  AND attribute.attribute_id=$bridge_channel_AttrId AND attribute.store_id IN ($stores) AND attribute.value AND attribute.value!=$incomingchannel_id;";
        $updated_channels = $readConnection->fetchAll($query);
        $channel_id_arr = array();
        if ($updated_channels) {
            foreach ($updated_channels as $updated_channel) {
                $last_updated_skus[$updated_channel['sku']][$updated_channel['store_id']] = $updated_channel['value'];
                $channel_id_arr[] = $updated_channel['value'];
            }
            $channel_id_arr = array_unique($channel_id_arr);

            $channel_ids = Mage::getmodel('bridge/incomingchannels')
                    ->getCollection()
                    ->addFieldToFilter('id', $channel_id_arr)
                    ->addFieldToSelect(array('id', 'weightage'))
                    ->getData();
            $channel_ids = array_column($channel_ids, 'weightage', 'id');
            //get the channel who has greater weightage and availble skus
            $channel_ids_str = implode(',', $channel_id_arr);
            $availabe_sku = $readConnection->fetchAll("SELECT sku,brand_id FROM bridge_incomingchannel_skus WHERE brand_id IN ($channel_ids_str) AND sku IN ($sku_list_str) AND current_flag!='not_available' AND weightage < $weightage;");
            $availabe_sku = array_column($availabe_sku, 'sku', 'brand_id');
        }

//        foreach
//        echo "<pre>";
//        print_r($availabe_sku);
//        exit;
//end
        //end
        foreach ($sku_list as $sku) {
            $stores_for_current_sku = $store_mapping;
//            $query = "SELECT attribute.value,attribute.store_id FROM catalog_product_entity as product JOIN catalog_product_entity_text as attribute  ON attribute.entity_id=product.entity_id WHERE product.sku='{$sku}'  AND attribute.attribute_id=$attributeId AND attribute.store_id IN ($stores) AND attribute.value";
//            $priority_channels = $readConnection->fetchAll($query);
            $stores_set_for_this_sku = array();
            if (isset($sku_priorities[$sku]))
                foreach ($sku_priorities[$sku] as $store_id => $sku_option_id) {
                    if ($sku_option_id != $option_id) {
                        if (($key = array_search($store_id, $stores_for_current_sku)) !== false) {
                            unset($stores_for_current_sku[$key]);
                        }
                    } else {
                        $stores_set_for_this_sku[] = $store_id;
                    }
                }
            if (isset($last_updated_skus[$sku])) {
                $last_updated_skus[$sku] = array_diff_key($last_updated_skus[$sku], array_flip($stores_set_for_this_sku));

                if ($last_updated_skus[$sku])
                    foreach ($last_updated_skus[$sku] as $store_id => $updated_channel_id) {
                        if ($channel_ids[$updated_channel_id] < $weightage && @$availabe_sku[$updated_channel_id][$sku]) {
                            unset($stores_for_current_sku[$store_id]);
                        }
                    }
            }

            if ($stores_for_current_sku) {
                $store_views = implode(',', $stores_for_current_sku);
                //check the skus table for channel priority
                $query2 = "SELECT brand_id FROM bridge_incomingchannel_skus WHERE sku='{$sku}' AND weightage > $weightage AND brand_id!=$incomingchannel_id";
                $otherchannel_lists = $readConnection->fetchAll($query2);
                if ($otherchannel_lists)
                    $otherchannel_lists = array_map('current', $otherchannel_lists);
                if ($otherchannel_lists) {
                    $otherchannel_lists = implode(',', $otherchannel_lists);
                    $delete_query = "DELETE FROM bridge_pim_processing_queue WHERE store IN ($store_views) AND channel_id IN ($otherchannel_lists) AND sku='{$sku}' AND is_processed=0;";
                    $writeConnection->raw_query($delete_query);
                }
                $fetch_query = "SELECT store FROM bridge_pim_processing_queue WHERE sku='{$sku}' AND is_processed=0;";
                $prior_channel_stores = $readConnection->fetchAll($fetch_query);
                if ($prior_channel_stores)
                    $prior_channel_stores = array_map('current', $prior_channel_stores);
                if ($prior_channel_stores)
                    $stores_for_current_sku = array_diff($stores_for_current_sku, $prior_channel_stores);
                if ($stores_for_current_sku)
                    foreach ($stores_for_current_sku as $store_id) {
                        $channel_store_id = array_search($store_id, $store_mapping);
                        $insertcommend_inpim.="(NULL,$incomingchannel_id,'{$sku}',$channel_store_id,0),";
                    }
            }
        }
        //exit;
        if (@$insertcommend_inpim) {
            $insertcommend_inpim = trim($insertcommend_inpim, ',');
            $insertcommend_inpim = "INSERT INTO bridge_pim_processing_queue (id,channel_id,sku,store,is_processed) VALUES $insertcommend_inpim";
            $writeConnection->raw_query($insertcommend_inpim);
        }
        //get unique records
        $unique_list = $readConnection->fetchAll("SELECT MIN(id) id FROM bridge_pim_processing_queue GROUP BY sku,store,is_processed,channel_id HAVING channel_id=$incomingchannel_id AND is_processed=0");
        $unique_list = implode(',', array_map('current', $unique_list));
        //delete duplicate unprocessed records from the queue
        $delete_query = "DELETE FROM bridge_pim_processing_queue WHERE id NOT IN ($unique_list) AND is_processed=0 AND channel_id=$incomingchannel_id;";
        $writeConnection->raw_query($delete_query);
    }

    public function changeProcessStatus($sku, $is_processed, $product_status, $storeview) {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        if (is_null($product_status))
            $sql = "UPDATE bridge_pim_processing_queue SET is_processed=$is_processed WHERE sku='{$sku}' AND store IN ($storeview) AND is_processed=0;";
        else
            $sql = "UPDATE bridge_pim_processing_queue SET product_status=$product_status,is_processed=$is_processed WHERE sku='{$sku}' AND store IN ($storeview) AND is_processed=0;";
        $writeConnection->raw_query($sql);
    }

    public function changeRelationStatus($sku, $channel_id) {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $sql = "UPDATE bridge_pim_processing_queue SET relation_updated=1 WHERE sku='{$sku}' AND channel_id=$channel_id AND  relation_updated!=1;";
        $writeConnection->raw_query($sql);
    }

    public function getUnmappedSkus() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        //get the sku list for which have child skus
//         $sku_productstatus = array_column($sku_productstatus, 'product_status', 'sku');
        $sql = "SELECT GROUP_CONCAT(DISTINCT(CONCAT(sku,':',IFNULL(product_status, 'null')))) as sku_dtls,channel_id FROM bridge_pim_processing_queue WHERE relation_updated=0 GROUP BY  channel_id HAVING sku_dtls!=''";
//        echo $sql;exit;
        $sku_list = $readConnection->fetchAll($sql);
        return $sku_list;
    }

}
