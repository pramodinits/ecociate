<?php

class Fedobe_Bridge_Model_Outgoingchannels_Skus extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('bridge/outgoingchannels_skus');
    }

    public function insert($sku_list, $ogc_id, $relation = null) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');
        if (is_null($relation)) {
            $str_sku = "'" . implode("','", $sku_list) . "'";
            //delete the records which are not present in current matched sku
            $delete_query = "DELETE FROM bridge_outgoingchannel_skus WHERE channel_id=$ogc_id AND (sku NOT IN ($str_sku) AND parent_sku IS NULL) OR (parent_sku IS NOT NULL AND parent_sku NOT IN ($str_sku))";
            $writeConnection->raw_query($delete_query);
            //mark the pim queue as processed
            if ($sku_list) {
                //get the sku list exist for this channel
                $select_query = "SELECT sku FROM bridge_outgoingchannel_skus WHERE channel_id=$ogc_id AND relation IS NULL";
                $batchsku_list = $readConnection->fetchAll($select_query);
                if ($batchsku_list) {
                    $batchsku_list = array_column($batchsku_list, 'sku');
                    $sku_list = array_diff($sku_list, $batchsku_list);
                }
                $insert_query = null;
                foreach ($sku_list as $sku) {
                    $insert_query.="($ogc_id,'{$sku}'),";
                }
                if ($insert_query) {
                    $insert_query = "INSERT INTO bridge_outgoingchannel_skus (channel_id,sku) VALUES " . trim($insert_query, ',');
                    $writeConnection->raw_query($insert_query);
                }
            }
            //check if there is no records available to process then update is process to 0;
            $select_query = "SELECT count(id) edi_count FROM bridge_outgoingchannel_skus WHERE channel_id=$ogc_id AND is_processed=0;";
            $edi_count = $readConnection->fetchRow($select_query);
            if (!array_filter($edi_count)) {
                $writeConnection->raw_query("UPDATE bridge_outgoingchannel_skus SET is_processed=0 WHERE channel_id=$ogc_id;");
            }
        } else {
            $delete_query = null;
            $insert_query = null;
            foreach ($sku_list as $parent_sku => $relationinfo) {
                foreach ($relationinfo as $relation => $sku_info) {
                    $str_sku = "'" . implode("','", $sku_info) . "'";
                    //delete the records which are not present in current matched sku
                    $delete_query = "DELETE FROM bridge_outgoingchannel_skus WHERE channel_id=$ogc_id AND sku NOT IN ($str_sku) AND parent_sku='{$parent_sku}' AND relation='{$relation}';";
                    $writeConnection->raw_query($delete_query);
                    //get the sku list exist for this channel
                    $select_query = "SELECT sku FROM bridge_outgoingchannel_skus WHERE channel_id=$ogc_id AND parent_sku='{$parent_sku}' AND relation='{$relation}';";
                    $batchsku_list = $readConnection->fetchAll($select_query);
                    if ($batchsku_list) {
                        $batchsku_list = array_column($batchsku_list, 'sku');
                        $sku_info = array_diff($sku_info, $batchsku_list);
                    }
                    foreach ($sku_info as $sku) {
                        $insert_query.="($ogc_id,'{$sku}','{$relation}','{$parent_sku}'),";
                    }
                }
            }
            if ($insert_query) {
                $insert_query = "INSERT INTO bridge_outgoingchannel_skus (channel_id,sku,relation,parent_sku) VALUES " . trim($insert_query, ',');
                $writeConnection->raw_query($insert_query);
            }
        }
    }

    /**
     * get the sku and updated price
     * @param type $channel_id
     * @param type $limit
     * @return type
     */
    public function read_sku($channel_id, $limit) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        //get the sku list exist for this channel
        $select_query = "SELECT ogc.id,ogc.sku,ogc.parent_sku,ogc.relation,inc.converted_price,inc.deleted FROM bridge_outgoingchannel_skus ogc  LEFT JOIN bridge_incomingchannel_product_info inc ON ogc.sku=inc.sku AND inc.channel_id=$channel_id AND inc.channel_type='outgoing' WHERE ogc.channel_id=$channel_id AND ogc.is_processed=0 LIMIT 0,$limit";
        $batchsku_list = $readConnection->fetchAll($select_query);
        return $batchsku_list;
    }

    public function deleteNotMatchedProductById($Ids) {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $str_Ids = implode(',', $Ids);
        $writeConnection->raw_query("DELETE FROM  bridge_outgoingchannel_skus WHERE id IN ($str_Ids);");
    }

    public function updateProcessStatus($channel_id, $skulist, $is_processed = 1) {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $str_sku = "'" . implode("','", $skulist) . "'";
        $writeConnection->raw_query("UPDATE bridge_outgoingchannel_skus SET is_processed=$is_processed WHERE sku IN ($str_sku) AND channel_id=$channel_id;");
    }

}
