<?php
class Fedobe_Bridge_Model_Outgoingchannels_Action extends Mage_Core_Model_Abstract {
     public function _construct()
    {
          parent::_construct();
        $this->_init('bridge/outgoingchannels_action');
    }
   /**
     * Load incomingchannel action by brand_id
     *
     * @param intiger $brandId
     */
    public function loadAllByChannelId($channelID)
    {
        $this->setData($this->getResource()->loadAllByChannelId($channelID));
        return $this;
    }
   /**
     * Load incomingchannel action by id
     *
     * @param intiger $id
     */
    public function loadById($id)
    {
        $this->setData($this->getResource()->loadId($id));
        return $this;
    }

}
