<?php

class Fedobe_Bridge_Model_Api extends Mage_Api_Model_Resource_Abstract {
    /*
     * get the inventory details after filteration
     */

    public function inventory($filters) {
        $return_type = @$filters['return_type'];
        unset($filters['return_type']);
//        return $filters;
        $products = Mage::getModel("catalog/product")
                ->getCollection()
                ->addAttributeToSelect('sku')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('attribute_set_id')
                ->addAttributeToSelect('type_id');
        if (is_array($filters)) {
            try {
                foreach ($filters as $field => $value) {
                    if (is_array($value) && !empty($value))
                        $products->addAttributeToFilter($field, array('in' => $value));
                    else if ($field)
                        $products->addAttributeToFilter($field, $value);
                }
            } catch (Mage_Core_Exception $e) {
                $this->_fault('filters_invalid', $e->getMessage());
                // If we are adding filter on non-existent attribute
            }
        }

        $result = array();
        foreach ($products as $product) {
            $result[] = $product->toArray();
        }
        if (empty($result)) {
            $this->_fault('not_exists');
            return true;
        }
//        return $result;
//        if (strcasecmp($return_type, 'json') === 0)
        return json_encode($result);
//        else if (strcasecmp($return_type, 'xml') === 0)
//            return json_encode($result);
//        else
//            return $result;
    }

    /*
     * get the category details and dependent attribute for that category
     */

    public function category($filters) {
        //get category details
        $categories = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToSelect(array('entity_id', 'parent_id', 'category_id', 'name'))
                ->addIsActiveFilter();
        $result = array();
        $attribute_set = array();
        foreach ($categories as $category) {
            $result['category'][] = array('id' => $category->getId(), 'parent_id' => $category->getParentId(), 'attribute_set_id' => $category->getAttributeSetId(), 'name' => $category->getName());
            if ($category->getAttributeSetId()) {
                $attr = null;
                $attribute_set_dtls = Mage::getModel('catalog/product_attribute_api')->items($category->getAttributeSetId());
                if (!in_array($category->getAttributeSetId(), $result['attribute_set'])) {
                    foreach ($attribute_set_dtls as $attribute_set) {
                        $attr[] = $attribute_set['attribute_id'];
                    }
                    $result['attribute_set'][$category->getAttributeSetId()] = $attr;
                }
            }
        }

        //get attribute details

        $attributes = Mage::getResourceModel('catalog/product_attribute_collection')
                ->getItems();

        foreach ($attributes as $attribute) {
            $result['attribute'][] = array('id' => $attribute->getId(), 'code' => $attribute->getAttributecode(), 'name' => $attribute->getFrontendLabel());
        }
        return json_encode($result);
    }

    public function getCategories() {
        $category_list=array();
         $categories = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToSelect(array('entity_id', 'parent_id', 'category_id', 'name'))
                ->addIsActiveFilter();
        $result = array();
        $attribute_set = array();
        foreach ($categories as $category) {
            
        }
    }

    function showSubCategories($categoryId) {
        if ($categoryId) {
            $category = Mage::getModel('catalog/category')->load($categoryId);
            $subCats = $category->getChildrenCategories();
            if (count($subCats) > 0) {
                echo '<ul>';
                foreach ($subCats as $subCat) {
                    echo "<li><a href=\"{$subCat->getUrl()}\">{$subCat->getName()}</a>";
                    showSubCategories($subCat->getId());
                    echo '</li>';
                }
                echo '</ul>';
            }
        }
    }

    /*
     * get the category details and dependent attribute for that category
     */

    public function attribute($filters) {
        $attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection')->load();

        $result = array();
        foreach ($attributeSetCollection as $attribute) {
            $result[] = $attribute->toArray();
        }
        return json_encode($result);
    }

}
