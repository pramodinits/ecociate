<?php

class Fedobe_Bridge_Model_Storeview_Label extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('bridge/storeview_label');
    }

    /**
     * get the store view label of a specific brand of a type('categories,options,attribute)
     * @param type $type
     * @param type $brand_id
     * @return \Fedobe_Bridge_Model_Storeview_Label
     */
    public function getLabel($type, $brand_id, $store_id, $default_store = null) {
        $resource = Mage::getSingleton('core/resource');
        $result = array();
        $readConnection = $resource->getConnection('core_read');
        if ($type == 'options')
            $select = "entity_id id,value,attribute_id";
        else
            $select = "entity_id id,value";
        $label_sql1 = "SELECT $select  FROM `bridge_storeview_label` WHERE type='{$type}' AND brand_id=$brand_id AND store_id=$store_id";
        $result = $readConnection->fetchAll($label_sql1);
        $entity_id = implode(',', array_column($result, 'id'));
        if (!is_null($default_store) && $store_id != $default_store) {
            $entity_id=$entity_id?$entity_id:0;
            $label_sql2 = "SELECT $select FROM `bridge_storeview_label` WHERE entity_id not in ($entity_id) AND type='{$type}' AND brand_id=$brand_id AND store_id=$default_store";
            $result2 = $readConnection->fetchAll($label_sql2);
            $result = array_merge($result, $result2);
        }
        return $result;
    }

    /**
     * insert multiple rows at time in storeview label table
     */
    public function insertMutipleRows($rows, $brand_id, $type) {
        $sql = 'INSERT INTO bridge_storeview_label VALUES ';

        foreach ($rows as $row) {
            $value = addslashes($row['value']);
            $row['attribute_id'] = @$row['attribute_id'];
            $sql.="(NULL,$brand_id,'{$type}','{$row['store_id']}','{$row['attribute_id']}','{$row['id']}','{$value}'),";
        }
        $sql = trim($sql, ',');
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $writeConnection->raw_query($sql);
    }

    /**
     * delete all the label for a the given brand
     */
    public function deleteLabel($brand_id) {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $sql = "DELETE FROM bridge_storeview_label WHERE brand_id=$brand_id";
        $writeConnection->raw_query($sql);
    }

}
