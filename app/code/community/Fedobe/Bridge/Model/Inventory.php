<?php

class Fedobe_Bridge_Model_Inventory extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('bridge/inventory');
    }

    /**
     * get active catalog product's sku list
     */
    public function getEnableProductIds() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $status_attr_id = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'status');
        $enabled_ids = $readConnection->fetchAll("SELECT mainTable.entity_id FROM catalog_product_entity as mainTable JOIN  catalog_product_entity_int as status_attr ON mainTable.entity_id=status_attr.entity_id WHERE status_attr.attribute_id=$status_attr_id AND status_attr.value=1 AND mainTable.type_id='simple'  GROUP BY status_attr.entity_id;");
        $enabled_ids = $enabled_ids ? array_column($enabled_ids, 'entity_id') : array(0);
        return $enabled_ids;
    }

    /**
     * get active catalog product's sku list having no inventory
     */
    public function getEnableNonStockProductIds() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $status_attr_id = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'status');
        $entityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
//get products who have no invntory
        $enabled_entity_id = $readConnection->fetchAll("SELECT mainTable.entity_id FROM catalog_product_entity as mainTable JOIN  catalog_product_entity_int as status_attr ON mainTable.entity_id=status_attr.entity_id  JOIN cataloginventory_stock_item as inventory on inventory.product_id=mainTable.entity_id AND inventory.qty<=0 Where mainTable.type_id='simple' AND status_attr.attribute_id=$status_attr_id AND status_attr.entity_type_id=$entityTypeId AND status_attr.value=1 GROUP BY status_attr.entity_id;");
        $enabled_entity_id = $enabled_entity_id ? array_column($enabled_entity_id, 'entity_id') : array(0);
        return $enabled_entity_id;
    }

    /**
     * first edi cron process to process the delted live process
     */
    public function processDeletedSku() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');
        $incomingchannel_ids = Mage::getmodel('bridge/incomingchannels')->getActiveChannelsForEDI();
        if ($incomingchannel_ids):
            $incomingchannel_ids = implode(',', $incomingchannel_ids);
            $bridge_attr_id = Mage::getResourceModel('eav/entity_attribute')
                    ->getIdByCode('catalog_product', 'bridge_pricenqty_channel_id');
            $entityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
            $select_query = "SELECT bridge_products.product_entity_id FROM bridge_incomingchannel_product_info as bridge_products  JOIN catalog_product_entity_int as product ON bridge_products.product_entity_id=product.entity_id AND  product.entity_type_id=$entityTypeId AND product.attribute_id=$bridge_attr_id AND bridge_products.channel_id=product.value WHERE bridge_products.deleted AND bridge_products.is_processed IN (0,2)  AND channel_type='incoming' LIMIT 0,10;";

            $result = $readConnection->fetchAll($select_query);
            if ($result) {
                $product_ids = array_column($result, 'product_entity_id');
                $product_ids_str = implode(',', $product_ids);
                //update catalog stock inventory as out of stock
                $writeConnection->raw_query("UPDATE cataloginventory_stock_item SET is_in_stock=0 WHERE product_id IN ($product_ids_str);");
                $this->changeProcessStatusByEntityId($product_ids);
            }
        endif;
        return false;
    }

    /*     * .
     * second process the skus for which own magento store have no inventory
     */

    public function processNonStockSku() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $incomingchannel_ids = Mage::getmodel('bridge/incomingchannels')->getActiveChannelsForEDI();
        if ($incomingchannel_ids):
            $incomingchannel_ids = implode(',', $incomingchannel_ids);
//get products who have no invntory
            $enabled_ids = $this->getEnableNonStockProductIds();
            $enabled_ids_str = implode(',', $enabled_ids);
            $condition = 1;
            $condition = trim($condition, 'AND');

//get products who have no invntory
            $select_query = "SELECT bridge_products.product_entity_id FROM bridge_incomingchannel_product_info as bridge_products  WHERE  bridge_products.product_entity_id IN ($enabled_ids_str) AND bridge_products.deleted=0 AND bridge_products.channel_type='incoming' AND bridge_products.quantity>0 AND bridge_products.channel_id IN ($incomingchannel_ids) AND $condition GROUP BY bridge_products.product_entity_id ORDER BY last_updated_time LIMIT 0,10;";
            $result = $readConnection->fetchAll($select_query);

            if ($result) {
                $result = array_column($result, 'product_entity_id');
                return $this->updateInventory($result, $incomingchannel_ids);
            }
        endif;
        return false;
    }

    /**
     * Changed source channel for non stock skus
     */
    public function processNonstockChannel() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $incomingchannel_ids = Mage::getmodel('bridge/incomingchannels')->getActiveChannelsForEDI();
        //get products who have no invntory
        $enabled_ids = $this->getEnableNonStockProductIds();
        $enabled_ids_str = implode(',', $enabled_ids);
        $query = "SELECT bridge_products.product_entity_id FROM bridge_incomingchannel_product_info bridge_products WHERE bridge_products.product_entity_id in($enabled_ids_str) AND bridge_products.is_live=1 AND bridge_products.channel_type='incoming' LIMIT 0,10;";
        $result = $readConnection->fetchAll($query);
        $incomingchannel_ids = implode(',', $incomingchannel_ids);

        if ($result) {
            $result = array_column($result, 'product_entity_id');
            return $this->updateInventory($result, $incomingchannel_ids);
        }
        return false;
    }

    /**
     * $priority (2=>zero to non zero ,3=>non zero to zero)
     * update the skus which are updated to nonzro from zero stock 
     * and update the skus which are updated to zero from nonzero stock 
     */
    public function processPrioritiesSku($priority, $is_live = 0) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $incomingchannel_ids = Mage::getmodel('bridge/incomingchannels')->getActiveChannelsForEDI();
        if ($incomingchannel_ids):
            $incomingchannel_ids = implode(',', $incomingchannel_ids);
            $bridge_attr_id = Mage::getResourceModel('eav/entity_attribute')
                    ->getIdByCode('catalog_product', 'bridge_pricenqty_channel_id');
//get enable  products
            $enable_ids = $this->getEnableProductIds();
            $enable_ids_str = implode(',', $enable_ids);
            $select_query = "SELECT product_entity_id,id FROM bridge_incomingchannel_product_info  WHERE product_entity_id IN ($enable_ids_str) AND deleted=0 AND channel_type='incoming' AND is_processed IN (0,2) AND priority=$priority AND channel_id IN ($incomingchannel_ids) AND is_live=$is_live GROUP BY sku  ORDER BY last_updated_time LIMIT 0,20;";
            $priority_enities = $readConnection->fetchAll($select_query);


            if ($priority_enities) {
                $change_process_status = '';
                $priority_enities = array_column($priority_enities, 'id', 'product_entity_id');
                $ids_for_inventory = implode(',', array_keys($priority_enities));
                $product_nthaving_own_inv = $readConnection->fetchAll("SELECT entity_id FROM  catalog_product_entity_int ci WHERE ci.entity_id IN($ids_for_inventory) AND ci.attribute_id=$bridge_attr_id AND value IS NOT NULL;");

                if ($product_nthaving_own_inv) {
                    $product_nthaving_own_inv = array_column($product_nthaving_own_inv, 'entity_id', 'entity_id');
                    $change_process_status = array_diff_key($priority_enities, $product_nthaving_own_inv);
                    $priority_enities = array_intersect_key($priority_enities, $product_nthaving_own_inv);
                } else {
                    $change_process_status = $priority_enities;
                    $priority_enities = array();
                }
                if ($change_process_status) {
                    $this->changeProcessStatusByEntityId(array_keys($change_process_status));
                }
                if ($priority_enities) {
                    return $this->updateInventory(array_flip($priority_enities), $incomingchannel_ids);
                } else {
                    return $this->processPrioritiesSku($priority);
                }
            }
        endif;
        return false;
    }

    /**
     * process series skus
     */
    public function processSeriesSku() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $incomingchannel_ids = Mage::getmodel('bridge/incomingchannels')->getActiveChannelsForEDI();
        if ($incomingchannel_ids):
            $incomingchannel_ids = implode(',', $incomingchannel_ids);
            $enable_product_ids = $this->getEnableProductIds();
            $enable_product_ids_str = implode(',', $enable_product_ids);
            $select_query = "SELECT product_entity_id FROM bridge_incomingchannel_product_info  WHERE product_entity_id IN ($enable_product_ids_str) AND deleted=0 AND is_processed IN (0,2) AND channel_id IN ($incomingchannel_ids) AND channel_type='incoming' GROUP BY sku  ORDER BY last_updated_time LIMIT 0,10;";
            $result = $readConnection->fetchAll($select_query);
            if ($result) {

                $result = array_column($result, 'product_entity_id');
                return $this->updateInventory($result, $incomingchannel_ids);
            }
        endif;
        return false;
    }

    public function updateInventory($entity_ids, $incomingchannel_ids) {

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $entity_ids_str = implode(',', $entity_ids);
        $live = '';
        if (!file_exists($_SERVER["DOCUMENT_ROOT"] . '/bridgecron')) {
            mkdir($_SERVER["DOCUMENT_ROOT"] . '/bridgecron');
            chmod($_SERVER["DOCUMENT_ROOT"] . '/bridgecron', 0777);
        }
        $contents = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/bridgecron/edi_process.php');
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/bridgecron/edi_process.php', $contents . print_r($entity_ids, true));
        echo "<pre>";
        print_r($entity_ids);

//exit;
        foreach ($entity_ids as $entity_id) {
            $changed = false;
            $product = Mage::getModel('catalog/product')->load($entity_id);
            if (!$product)
                continue;
            //get the current product price and quantity before save
            $prev_price = $product->getPrice();
            $prev_quantity = $product->getStockItem()->getQty();
            $prev_Supplier = $product->getSuppliers();
            $exclude_channel = $product->getExcludeFromChannel();

            if ($exclude_channel) {
                $exclude_channel_arr = explode(',', $exclude_channel);
                $incomingchannel_ids_arr = explode(',', $incomingchannel_ids);
                $incomingchannel_ids_nw = array_diff($incomingchannel_ids_arr, $exclude_channel_arr);
                $incomingchannel_ids = implode(',', $incomingchannel_ids_nw);
            }
            if (intval($prev_quantity) > 0 && (!$product->getBridgePricenqtyChannelId())) {
                continue;
            }
            $change_inventory = true;
            //get the channel with sku having higher priority and quantity
            $select_query = "SELECT channel_id,price,quantity,converted_price,channel_supplier,is_processed FROM bridge_incomingchannel_product_info  WHERE channel_type='incoming' AND deleted=0 AND channel_id IN ($incomingchannel_ids) AND product_entity_id =$entity_id AND quantity>0 ORDER BY `slab`,`order`,`price` ASC ,`quantity` DESC LIMIT 0,1;";
            $result = $readConnection->fetchRow($select_query);
            if (!$result) {
                if ($product->getBridgePricenqtyChannelId()) {
//                    $current_incoming_channel = $readConnection->fetchRow("SELECT channel_id,price,quantity,converted_price,channel_supplier,is_processed FROM bridge_incomingchannel_product_info  WHERE channel_type='incoming' AND deleted=0 AND channel_id IN ($incomingchannel_ids) AND product_entity_id =$entity_id  ORDER BY `slab`,`order`,`price` ASC LIMIT 0,1;");
//                    if (!$current_incoming_channel)
//                        continue;
                    $own_store = Mage::getmodel('bridge/incomingchannels_product')
                            ->getCollection()
                            ->addFieldToFilter('product_entity_id', $entity_id)
                            ->addFieldToFilter('channel_id', 0)
                            ->getFirstItem();

                    $product->setBridgePricenqtyChannelId(NULL);
                    if (!$own_store || !$own_store->getId()) {
                        $product->setStockData(array('qty' => 0, 'is_in_stock' => 0));
                    } else {
                        $product->setStockData(array('qty' => $own_store->getQuantity(), 'is_in_stock' => $own_store->getQuantity() > 0 ? 1 : 0));
                        $product->setPrice($own_store->getPrice());
                        $product->setSuppliers($own_store->getChannelSupplier());
                        $product->setSpecialPrice($own_store->getSpecialPrice() ? $own_store->getSpecialPrice() : null);
                    }
//                    if ($current_incoming_channel['is_processed'] == 2 || !$current_incoming_channel['converted_price']) {
//                        $price_rule_query = "SELECT multi_price_rule,id FROM bridge_incomingchannels  WHERE id={$current_incoming_channel['channel_id']}";
//                        $price_rule_result = $readConnection->fetchRow($price_rule_query);
//                        $current_incoming_channel['converted_price'] = Mage::helper('bridge')->addPriceRule($price_rule_result, $product->getId(), $current_incoming_channel['price']);
//                    }
//                    $current_price = $current_incoming_channel['converted_price'];
//                    $product->setStockData(array('qty' => 0, 'is_in_stock' => 0));
//                    $product->setPrice($current_price);
//                    $product->setBridgePricenqtyChannelId($current_incoming_channel['channel_id']);
                    $result['quantity'] = $own_store ? $own_store->getQuantity() : 0;
                    $result['channel_id'] = 0;
                    $current_price = $own_store ? $own_store->getPrice() : $product->getPrice();
                    $changed = true;
                } else {
                    continue;
                }
            } else {
                if ($result['converted_price'] && $result['is_processed'] != 2)
                    $current_price = $result['converted_price'];
                else {
                    $price_rule_query = "SELECT multi_price_rule,id FROM bridge_incomingchannels  WHERE id={$result['channel_id']}";
                    $price_rule_result = $readConnection->fetchRow($price_rule_query);
                    $current_price = Mage::helper('bridge')->addPriceRule($price_rule_result, $product->getId(), $result['price']);
                }


                if ($product->getBridgePricenqtyChannelId() != $result['channel_id']) {
                    $product->setBridgePricenqtyChannelId($result['channel_id']);
                    if ($product->getBridgePricenqtyChannelId())
                        $product->setSpecialPrice(NULL);
                    $changed = true;
                }
                if (round($result['quantity']) != round($prev_quantity)) {
                    if ($result['quantity'])
                        $product->setStockData(array('qty' => $result['quantity'], 'is_in_stock' => 1));
                    else
                        $product->setStockData(array('qty' => $result['quantity'], 'is_in_stock' => 0));
                    $changed = true;
                }
                if (round($current_price, 2) != round($prev_price, 2)) {
                    $product->setPrice($current_price);
                    $changed = true;
                }
                if ($prev_Supplier != $result['channel_supplier'] && $result['channel_supplier']) {
                    $product->setSuppliers($result['channel_supplier']);
                    $changed = true;
                }
            }
            if ($changed) {
                $live[] = array('product_entity_id' => $entity_id, 'channel_id' => $result['channel_id'], 'converted_price' => $current_price);

                try {
                    $product->save();
                } catch (Exception $e) {
                    echo "<pre>";
                    print_r($result);
                    print_r($e->getMessage());
                    exit;
                }

//get the current product price and quantity after save
                $cur_price = $current_price;
                $cur_quantity = $result['quantity'];
//keep this product updation track in history table
                $history_data = array('brand_id' => $result['channel_id'], 'sku' => $product->getSku(), 'event' => 'price-updated', 'previous_price' => $prev_price, 'current_price' => $current_price, 'previous_quantity' => $prev_quantity, 'current_quantity' => $result['quantity']);
                $history_model = Mage::getmodel('bridge/incomingchannels_history');
                $history_model->setData($history_data);
                $history_model->save();
            }
        }
        //update the live status of the sku across channel
        if ($live)
            $this->changeLiveStatusByProductId($live);
        //update the sku as processed in edi batch
        $this->changeProcessStatusByEntityId($entity_ids);
        return true;
    }

    public function changeLiveStatusByProductId($live) {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $update_query = '';
        foreach ($live as $channel_ids) {
            $update_query .= "UPDATE  bridge_incomingchannel_product_info SET is_live=0 WHERE channel_id!={$channel_ids['channel_id']} AND product_entity_id='{$channel_ids['product_entity_id']}';";
            if ($channel_ids['converted_price'] && $channel_ids['channel_id'])
                $update_query .= "UPDATE  bridge_incomingchannel_product_info SET is_live=1,converted_price={$channel_ids['converted_price']} WHERE channel_id={$channel_ids['channel_id']} AND product_entity_id='{$channel_ids['product_entity_id']}' AND channel_type='incoming';";
            else
                $update_query .= "UPDATE  bridge_incomingchannel_product_info SET is_live=1 WHERE channel_id={$channel_ids['channel_id']} AND product_entity_id='{$channel_ids['product_entity_id']}' AND channel_type!='outgoing';";
        }
        if ($update_query)
            $writeConnection->raw_query($update_query);
        if ($print_parameter) {
            echo "<pre>";
            print_r($live);
        }
    }

    public function changeLiveStatus($live, $print_parameter = 0) {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $update_query = '';
        foreach ($live as $channel_ids) {
            $update_query .= "UPDATE  bridge_incomingchannel_product_info SET is_live=0 WHERE channel_id!={$channel_ids['channel_id']} AND sku='{$channel_ids['sku']}';";
            $update_query .= "UPDATE  bridge_incomingchannel_product_info SET is_live=1 WHERE channel_id={$channel_ids['channel_id']} AND sku='{$channel_ids['sku']}' AND channel_type='incoming';";
        }
        if ($update_query)
            $writeConnection->raw_query($update_query);
        if ($print_parameter) {
            echo "<pre>";
            print_r($live);
        }
    }

    public function changeProcessStatusByEntityId($entity_Ids) {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $entity_Ids_str = implode(',', $entity_Ids);
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $update_query = "UPDATE  bridge_incomingchannel_product_info SET is_processed=1 WHERE channel_type='incoming' AND product_entity_id IN($entity_Ids_str)";
        $writeConnection->raw_query($update_query);
    }

    public function changeProcessStatus($sku_list, $parameter = 'sku') {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        if ($parameter == "*") {
            $update_query = "UPDATE  bridge_incomingchannel_product_info SET is_processed=0 WHERE channel_type='incoming';";
        } else {
            $skulist = "'" . implode("','", $sku_list) . "'";
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');
            $update_query = "UPDATE  bridge_incomingchannel_product_info SET is_processed=1 WHERE channel_type='incoming' AND ";
            $update_query = $parameter == 'sku' ? "$update_query sku IN ($skulist);" : "$update_query id IN ($skulist);";
        }
        $writeConnection->raw_query($update_query);
    }

    public function updateEDITable($skulist, $channel_id) {
        $skulist = $skulist ? "'" . implode("','", $skulist) . "'" : 0;
        $attributeId = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'bridge_pricenqty_channel_id');
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $readConnection = $resource->getConnection('core_read');
        $select_query = "SELECT id,sku FROM bridge_incomingchannel_product_info WHERE channel_type='incoming' AND channel_id=$channel_id AND sku NOT IN ($skulist);";
        $result = $readConnection->fetchAll($select_query);

        if ($result) {
            $result = array_column($result, 'sku', 'id');
            $info_ids = implode(',', array_keys($result));
            $skulist = "'" . implode("','", $result) . "'";
            $writeConnection->raw_query("DELETE FROM bridge_incomingchannel_product_info WHERE id IN ($info_ids);");
            $writeConnection->raw_query("UPDATE  catalog_product_entity AS stock JOIN catalog_product_entity_int as attribute_table ON stock.entity_id=attribute_table.entity_id AND attribute_id=$attributeId AND value=$channel_id JOIN cataloginventory_stock_item AS inventory ON stock.entity_id=inventory.product_id SET inventory.qty=0,inventory.is_in_stock=0 WHERE stock.sku IN ($skulist);");
        }
    }

    /**
     * save the product details in bridge inventory table
     */
    public function saveStoreProductDetails($exclude_skus) {
        $exclude_skus = $exclude_skus ? "'" . implode("','", $exclude_skus) . "'" : "'0'";
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');
        $bridge_price_attributeId = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'bridge_pricenqty_channel_id');
        $sku_query = "SELECT p1.sku,p1.entity_id FROM catalog_product_entity p1  WHERE  sku NOT IN ($exclude_skus) AND entity_id NOT IN (SELECT entity_id FROM catalog_product_entity_int AS p2 WHERE p2.attribute_id=$bridge_price_attributeId AND p2.value IS NOT NULL);";
        $entity_sku = $readConnection->fetchAll($sku_query);
        if ($entity_sku) {
            $price_attributeId = Mage::getResourceModel('eav/entity_attribute')
                    ->getIdByCode('catalog_product', 'price');
            $spacialprice_attributeId = Mage::getResourceModel('eav/entity_attribute')
                    ->getIdByCode('catalog_product', 'special_price');

            $entity_sku = array_column($entity_sku, 'entity_id', 'sku');
            $entity_sku_list = implode(',', $entity_sku);
            $qty_query = "SELECT qty,product_id FROM cataloginventory_stock_item  WHERE  product_id IN ($entity_sku_list);";
            $qty_result = $readConnection->fetchAll($qty_query);
            $qty_result = array_column($qty_result, 'qty', 'product_id');
            $price_query = "SELECT entity_id,group_concat(concat(value,':',attribute_id)) as price FROM catalog_product_entity_decimal  WHERE  entity_id IN ($entity_sku_list) AND attribute_id IN ($price_attributeId,$spacialprice_attributeId) GROUP BY entity_id;";
            $price_result = $readConnection->fetchAll($price_query);
            $price_result = array_column($price_result, 'price', 'entity_id');
            $count = 0;
            $query_values = '';

            foreach ($entity_sku as $sku => $entity_id) {
                $price_for_sku = @explode(',', $price_result[$entity_id]);
                $entity_price = array();
                if ($price_for_sku) {
                    foreach ($price_for_sku as $price_sku) {
                        $price_special_price = explode(":", $price_sku);
                        $entity_price[@$price_special_price[1]] = @$price_special_price[0];
                    }
                }
                $entity_price[$price_attributeId] = @$entity_price[$price_attributeId] ? $entity_price[$price_attributeId] : 0;
                $entity_price[$spacialprice_attributeId] = @$entity_price[$spacialprice_attributeId] ? $entity_price[$spacialprice_attributeId] : NULL;
                $qty_result[$entity_id] = @$qty_result[$entity_id] ? $qty_result[$entity_id] : 0;
                $sku = addslashes($sku);
                $query_values .= "('{$sku}',{$qty_result[$entity_id]},{$entity_price[$price_attributeId]},{$entity_price[$spacialprice_attributeId]},1),";
                $count++;
                if ($count % 1000 === 0) {
                    $query_values = trim($query_values, ',');
                    $writeConnection->raw_query("INSERT INTO bridge_incomingchannel_product_info (sku,quantity,price,special_price,is_live) VALUES $query_values;");
                    $query_values = '';
                }
            }
            if ($query_values) {
                $query_values = trim($query_values, ',');
                $writeConnection->raw_query("INSERT INTO bridge_incomingchannel_product_info (sku,quantity,price,special_price,is_live) VALUES $query_values;");
            }
        }
    }

    public function deleteNonExistProduct($skulist, $channel_id) {
        $skulist = "'" . implode("','", $skulist) . "'";
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $writeConnection->raw_query("DELETE FROM bridge_incomingchannel_product_info WHERE channel_id=$channel_id AND channel_type='outgoing' AND sku IN ($skulist) AND deleted;");
    }

    /**
     * update the catalog stock quantity as 0 if it is out of stock
     * 
     */
    public function updateOutOfStockAsZeroQty() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');
        $out_of_stock_ids = $readConnection->fetchAll("SELECT product_id FROM  cataloginventory_stock_item stock WHERE stock.qty!=0 AND is_in_stock=0;");
        if ($out_of_stock_ids) {
            $out_of_stock_ids = array_column($out_of_stock_ids, 'product_id');
            $entityIdStr = implode(',', $out_of_stock_ids);
            $bridge_price_attributeId = Mage::getResourceModel('eav/entity_attribute')
                    ->getIdByCode('catalog_product', 'bridge_pricenqty_channel_id');
            $entityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
            $bridge_channel_ids = $readConnection->fetchAll("SELECT entity_id,value as channel_id FROM catalog_product_entity_int WHERE entity_id IN ($entityIdStr) AND entity_type_id=$entityTypeId AND attribute_id=$bridge_price_attributeId AND value IS NOT NULL;");
            $bridge_channel_ids = array_column($bridge_channel_ids, 'channel_id', 'entity_id');
            $update_query = '';
            foreach ($out_of_stock_ids as $entityId) {
                if (isset($bridge_channel_ids[$entityId])) {
                    $update_query .="UPDATE bridge_incomingchannel_product_info SET quantity=0  WHERE channel_id={$bridge_channel_ids[$entityId]} AND product_entity_id=$entityId AND channel_type='incoming';";
                } else {
                    $update_query .="UPDATE bridge_incomingchannel_product_info SET quantity=0  WHERE channel_id=0 AND product_entity_id=$entityId;";
                }
            }
            //update outgoing channel edi
            $writeConnection->raw_query("UPDATE bridge_incomingchannel_product_info SET quantity=0,is_processed=0 WHERE product_entity_id IN ($entityIdStr) AND channel_type='outgoing' AND is_processed NOT IN (-1,-2);");
            $writeConnection->raw_query("UPDATE bridge_incomingchannel_product_info SET quantity=0,is_processed=-2 WHERE product_entity_id IN ($entityIdStr) AND channel_type='outgoing' AND is_processed=-1;");
            //update incoming edi
            $writeConnection->raw_query($update_query);
            //update catalog stock inventory
            $writeConnection->raw_query("UPDATE cataloginventory_stock_item SET qty=0 WHERE product_id IN ($entityIdStr);");
        }
    }

}