<?php

class Fedobe_Bridge_Model_Incomingchannels_Product extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('bridge/incomingchannels_product');
    }

    public function saveSkuInfo($channel_id, $currency, $sku_data, $price = 0, $qty = 0, $slab = 0, $order = 0, $converted_price = 'NULL', $api_call = 1) {

        $current_currency = Mage::app()->getStore()->getBaseCurrencyCode();
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $readConnection = $resource->getConnection('core_read');
        $sql_inert_history = '';
        $history_arr = array();
        $history_arr_channel = array();
        $history_arr_channel['channel_type'] = 'incoming';
        $history_arr_channel['brand_id'] = $channel_id;
        $history_arr_channel['source'] = 'api';
        $history_table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_history');
        if (!is_array($sku_data))
            $sku_data = trim($sku_data);
        $channel_count = 1;
        if (is_array($sku_data) && $api_call) {
            $sku_array = array_keys($sku_data);
            $sku_list = "'" . implode("','", $sku_array) . "'";
            $existing_skus = $readConnection->fetchAll("SELECT sku FROM catalog_product_entity WHERE sku in($sku_list);");
            $existing_skus = array_column($existing_skus, 'sku', 'sku');
            $sku_data = array_intersect_key($sku_data, $existing_skus);
            $select_query = "SELECT id,sku,quantity,price ,is_live,deleted FROM bridge_incomingchannel_product_info WHERE channel_id=$channel_id AND sku in ($sku_list) AND channel_type='incoming'";
            $product_track_ids = $readConnection->fetchAll($select_query);
            $result = array();
            $product_track_skus = array_column($product_track_ids, 'sku', 'id');
            $product_track_ids = array_combine($product_track_skus, $product_track_ids); //array_column($product_track_ids, 'quantity', 'sku');
//            echo "<pre>";
//            print_r($product_track_ids);exit;

            $update_query = null;
            $insert_query = null;
            foreach ($sku_data as $sku => $skuinfo) {
                $skuinfo = unserialize($skuinfo);
                if (is_numeric($currency)) {
                    $skuinfo['price'] = $skuinfo['price'] * $currency;
                } else if ($currency) {
//                    $skuinfo['price'] = Mage::helper('directory')->currencyConvert($skuinfo['price'], $currency, $current_currency);
                    $skuinfo['price'] = $this->currency_conv($skuinfo['price'], $currency);
                }
                if (in_array($sku, $product_track_skus)) {
                    $updated_price_qty = '';
                    $update_priorty = '';
                    if ($product_track_ids[$sku]['price'] != $skuinfo['price'] || $product_track_ids[$sku]['quantity'] != $skuinfo['qty'] || @$skuinfo['deleted']) {
                        $updated_price_qty = "`price`={$skuinfo['price']},`quantity`={$skuinfo['qty']},`converted_price`=null,is_processed=0,`last_updated_time`=NOW() ";
                        if (!$skuinfo['deleted']) {
                            $updated_price_qty.=",deleted=0 ";
                        }
                    }
                    if (@$skuinfo['deleted']) {
                        $update_priorty = ",`priority`=1,`deleted`=1 ";
                        if (!$product_track_ids[$sku]['is_live'])
                            $update_priorty.=',is_processed=1';
                    } else if ($product_track_ids[$sku]['quantity'] == 0 && $skuinfo['qty'] > 0)
                        $update_priorty = ",`priority`=2 ";
                    else if ($product_track_ids[$sku]['quantity'] > 0 && $skuinfo['qty'] == 0)
                        $update_priorty = ",`priority`=3 ";
                    else if ($product_track_ids[$sku]['price'] != $skuinfo['price'])
                        $update_priorty = ",`priority`=4 ";
                    else
                        $update_priorty = ",`priority`=5";
                    if ($updated_price_qty) {
                        $id = array_search($sku, $product_track_skus);
                        $update_query.="UPDATE bridge_incomingchannel_product_info SET $updated_price_qty$update_priorty WHERE `id`=$id;";
                    }
                } else if (!@$skuinfo['deleted']) {
                    $insert_query .="('{$sku}',$channel_id,{$skuinfo['qty']},{$skuinfo['price']},$slab,$order,2,'incoming'),";
                }
            }
            if ($insert_query) {
                $insert_query = trim($insert_query, ',');
                $insert_query = "INSERT INTO  bridge_incomingchannel_product_info (`sku`,`channel_id`,`quantity`,`price`,`slab`,`order`,`priority`,`channel_type`) VALUES {$insert_query};";
            }
            $update_sql = $update_query . $insert_query;
//            echo $update_query;//exit;
        } else if (is_array($sku_data) && !$api_call) {
          
            echo "<pre>";
            print_r($sku_data);
//exit;
            $priceerror_track = '';
            //get all the channel information of provided skus
            $channel_ids = array_unique(array_column($sku_data, 'channel_id'));
            sort($channel_ids);

            $channel_infos = Mage::getmodel('bridge/incomingchannels')->getCollection()
                    ->addFieldToFilter('id', $channel_ids)
                    ->addFieldToSelect(array('id', 'currency', 'slab', 'order' => 'channel_order', 'channel_supplier'))
                    ->setOrder('id')
                    ->getData();

            $chanelId = array_column($channel_infos, 'id');
            $channel_infos = array_combine($chanelId, $channel_infos);
            $sku_list = array_column($sku_data, 'sku');
            $sku_list = array_map('trim', $sku_list);
            $sku_list = array_map('addslashes', $sku_list);
            $sku_list_str = "'" . implode("','", $sku_list) . "'";
            $entity_id_with_sku = $readConnection->fetchAll("SELECT entity_id,sku FROM catalog_product_entity WHERE sku in ($sku_list_str);");
            $entity_id_with_sku = array_column($entity_id_with_sku, 'entity_id', 'sku');

            foreach ($sku_data as $sku_edi_info) {
                $history_arr = array();
                $history_arr['sku'] = $sku_edi_info['sku'] = trim($sku_edi_info['sku']);
                $price = $before_convert = $sku_edi_info['price'];
                $qty = $sku_edi_info['quantity'];
//var_dump($price);exit;

                if (is_numeric($channel_infos[$sku_edi_info['channel_id']]['currency']) && time()) {
                    $price = $price * $channel_infos[$sku_edi_info['channel_id']]['currency'];
                } else if ($channel_infos[$sku_edi_info['channel_id']]['currency'] && time()) {
//                    $price = Mage::helper('directory')->currencyConvert($price, $channel_infos[$sku_edi_info['channel_id']]['currency'], $current_currency);
                    $price = $this->currency_conv($price, $channel_infos[$sku_edi_info['channel_id']]['currency']);
                }
                //var_dump($channel_infos);
                //var_dump($price);exit;
                $history_arr['current_price'] = $price;
                $history_arr['current_quantity'] = $qty;
                if ($sku_edi_info['bridge_id']) {
                    $updated_price_qty = '';
                    $update_priorty = '';
                    $updated_event = array();
                    if (round($sku_edi_info['bridge_price'], 2) != round($price, 2) || intval($sku_edi_info['bridge_quantity']) != intval($qty)) {
                        if (round($sku_edi_info['bridge_price'], 2) != round($price, 2))
                            $updated_event[] = 'price-updated';
                        if (intval($sku_edi_info['bridge_quantity']) != intval($qty))
                            $updated_event[] = 'quantity-updated';
                        $history_arr['previous_price'] = $sku_edi_info['bridge_price'];
                        $history_arr['previous_quantity'] = $sku_edi_info['bridge_quantity'];
                        $updated_price_qty = "`price`=$price,`quantity`=$qty,`converted_price`=null,`last_updated_time`=NOW(),is_processed=0 ";
                    }else {
                        continue;
                    }

                    if ($sku_edi_info['bridge_quantity'] == 0 && $qty > 0)
                        $update_priorty = ",`priority`=2";
                    else if ($sku_edi_info['bridge_quantity'] > 0 && $qty <= 0)
                        $update_priorty = ",`priority`=3";
                    else if ($sku_edi_info['bridge_price'] != $price)
                        $update_priorty = ",`priority`=4";
                    else if ($sku_edi_info['bridge_quantity'] != $qty)
                        $update_priorty = ",`priority`=5";
                    else
                        $update_priorty = ",`priority`=6";
                    if ($updated_price_qty) {
                        $update_sql.="UPDATE bridge_incomingchannel_product_info SET $updated_price_qty$update_priorty WHERE `id`={$sku_edi_info['bridge_id']};";
                        $history_arr['event'] = (count($updated_event) == 1) ? $updated_event[0] : 'price-qty-updated';
                        $history_arr = array_merge($history_arr, $history_arr_channel);
                        $sql_inert_history.= 'INSERT INTO ' . $history_table . '(' . implode(',', array_keys($history_arr)) . ') VALUES("' . implode('","', $history_arr) . '");';
                    }
                } else {
                    $history_arr['event'] = 'price-qty-updated'; //'created';
                    $history_arr = array_merge($history_arr, $history_arr_channel);
                    $sql_inert_history.= 'INSERT INTO ' . $history_table . '(' . implode(',', array_keys($history_arr)) . ') VALUES("' . implode('","', $history_arr) . '");';
                    //echo "<pre>";
                    $sku_case_lower = strtolower($sku_edi_info['sku']);
                    $entity_id_with_sku_match = array_change_key_case($entity_id_with_sku, CASE_LOWER);
                    // print_r($entity_id_with_sku_match);exit;
                    $update_sql .= "INSERT INTO bridge_incomingchannel_product_info (`sku`,`channel_id`,`quantity`,`price`,`slab`,`order`,`priority`,`channel_type`,`product_entity_id`,`channel_supplier`) VALUES ('{$sku_edi_info['sku']}',{$sku_edi_info['channel_id']},$qty,$price,{$channel_infos[$sku_edi_info['channel_id']]['slab']},{$channel_infos[$sku_edi_info['channel_id']]['order']},2,'incoming',{$entity_id_with_sku_match[$sku_case_lower]},{$channel_infos[$sku_edi_info['channel_id']]['channel_supplier']});";
                }
                $mix_channel_id[] = $sku_edi_info['channel_id'];
            }
//echo $update_sql;exit;
            $channel_count = 2;
        } else if (!is_null($sku_data) && $entity_id_with_sku = $readConnection->fetchRow("SELECT sku,entity_id FROM catalog_product_entity WHERE sku='{$sku_data}';")) {
            if (is_numeric($currency)) {
                $price = $price * $currency;
            } else if ($currency) {
//                $price = Mage::helper('directory')->currencyConvert($price, $currency, $current_currency);
                $price = $this->currency_conv($price, $currency);
            }
            $select_query = "SELECT id,quantity,price FROM bridge_incomingchannel_product_info WHERE channel_id=$channel_id AND sku='{$sku_data}' AND channel_type='incoming' LIMIT 0,1";
            $product_track_id = $readConnection->fetchRow($select_query); //print_r($product_track_id);exit;
            $updated_event = array();
            $history_arr = array();
            $history_arr['sku'] = $sku_data;
            //$history_arr = array_merge($history_arr,$history_arr_channel);
            if ($product_track_id) {
                $updated_price_qty = '';
                $update_priorty = '';

                if (round($product_track_id['price'], 2) != round($price, 2) || intval($product_track_id['quantity']) != intval($qty)) {
                    $updated_price_qty = "`price`=$price,`quantity`=$qty,`converted_price`=null,`last_updated_time`=NOW(),is_processed=0 ";
                    if (round($product_track_id['price'], 2) != round($price, 2))
                        $updated_event[] = 'price-updated';
                    if (intval($product_track_id['quantity']) != intval($qty))
                        $updated_event[] = 'quantity-updated';
                    $history_arr['previous_price'] = $product_track_id['price'];
                    $history_arr['previous_quantity'] = $product_track_id['quantity'];
                }
                if ($product_track_id['quantity'] == 0 && $qty > 0)
                    $update_priorty = ",`priority`=2";
                else if ($product_track_id['quantity'] > 0 && $qty <= 0)
                    $update_priorty = ",`priority`=3";
                else if ($product_track_id['price'] != $price)
                    $update_priorty = ",`priority`=4";
                else
                    $update_priorty = ",`priority`=5";
                if ($updated_price_qty) {
                    $update_sql.="UPDATE bridge_incomingchannel_product_info SET $updated_price_qty$update_priorty WHERE `id`={$product_track_id['id']};";
                    $history_arr['event'] = (count($updated_event) == 1) ? $updated_event[0] : 'price-qty-updated';
                    $history_arr['current_price'] = $price;
                    $history_arr['current_quantity'] = $qty;
                    $history_arr = array_merge($history_arr, $history_arr_channel);
                    $sql_inert_history = 'INSERT INTO ' . $history_table . '(' . implode(',', array_keys($history_arr)) . ') VALUES("' . implode('","', $history_arr) . '")';
                }
            } else {
                $channel_supplier = Mage::getmodel('bridge/incomingchannels')->getCollection()
                        ->addFieldToFilter('id', $channel_id)
                        ->addFieldToSelect('channel_supplier')
                        ->getFirstItem()
                        ->getData();
                $update_sql = "INSERT INTO bridge_incomingchannel_product_info (`sku`,`channel_id`,`quantity`,`price`,`slab`,`order`,`priority`,`channel_type`,`converted_price`,`product_entity_id`,`channel_supplier`) VALUES ('{$sku_data}',$channel_id,'{$qty}','{$price}',$slab,$order,2,'incoming',$converted_price,{$entity_id_with_sku['entity_id']},{$channel_supplier['channel_supplier']});";
                $history_arr['event'] = 'price-qty-updated'; //created
                $history_arr['current_price'] = $price;
                $history_arr['current_quantity'] = $qty;
                $history_arr = array_merge($history_arr, $history_arr_channel);
                $sql_inert_history = 'INSERT INTO ' . $history_table . '(' . implode(',', array_keys($history_arr)) . ') VALUES("' . implode('","', $history_arr) . '")';
            }
            $mix_channel_id[] = $sku_edi_info['channel_id'];
            //print_r($history_arr);exit;
        }
        if ($update_sql) {
            $writeConnection->raw_query($update_sql);
            if ($channel_count == 1)
                $writeConnection->raw_query("UPDATE bridge_edi_channelcron SET condition_changed=1 WHERE channel_type='incoming' AND channel_id={$channel_id}");
            else if ($mix_channel_id && $channel_count == 2) {
                $mix_channel_id = implode(',', $mix_channel_id);
                $writeConnection->raw_query("UPDATE bridge_edi_channelcron SET condition_changed=1 WHERE channel_type='incoming' AND channel_id IN ($mix_channel_id);");
            }
        }
        if ($sql_inert_history) {
            $writeConnection->query($sql_inert_history);
        }
    }

     public function currency_conv($price, $currentCurrencyCode) {
        // Base Currency
        $baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
        if ($currentCurrencyCode != $baseCurrencyCode) {
            // Allowed currencies
            $allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
            $rates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, array_values($allowedCurrencies));
            // the price converted
            $price = $price / $rates[$currentCurrencyCode];
        }
        return $price;
    }

    public function saveDeletedSkuInfo($inventory_ids) {
        if ($inventory_ids) {
            $inventory_ids = implode(',', $inventory_ids);
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');
            $writeConnection->raw_query("UPDATE bridge_incomingchannel_product_info SET deleted=1,is_processed=0,quantity=0,price=0 WHERE id IN($inventory_ids);");
        }
    }

}