<?php
class Fedobe_Bridge_Model_Incomingchannels_Mapping extends Mage_Core_Model_Abstract {
     public function _construct()
    {
          parent::_construct();
        $this->_init('bridge/incomingchannels_mapping');
    }
    
     public function loadAllByBrandId($brandId){
        $this->setData($this->getResource()->loadAllByBrandId($brandId));
        return $this;
    }

}
