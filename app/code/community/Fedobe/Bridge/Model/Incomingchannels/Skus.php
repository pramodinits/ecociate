<?php

class Fedobe_Bridge_Model_Incomingchannels_Skus extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('bridge/incomingchannels_skus');
    }

    public function insert($apiresult, $inc_info, $relation = null) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');
        if (is_null($relation))
            $sku_list = unserialize($apiresult['skus']);
        else
            $sku_list = unserialize($apiresult[$relation]);

//if no matching sku found then make the flag not available in sku table 
        //mark the pim queue as processed
        if (!$sku_list) {
            if (is_null($relation)) {
                $this->makeSkuNotAvailable($inc_info['id']);
            }
            return false;
        }
        $ogc_info = unserialize($apiresult['channel_info']);
        $create_new = false;
        $update_exist = false;
        if ($ogc_info['create_new'] && $inc_info['create_new']) {
            $create_new = true;
        }
        if ($ogc_info['update_exist'] && $inc_info['update_exist']) {
            $update_exist = true;
        }
        if ($create_new || $update_exist) {
            $curtime = date('Y-m-d H:i:s');

            if (!$create_new || !$update_exist) {
                $sku_list_str = "'" . implode("','", $sku_list) . "'";
                $sql_sku_exist = "SELECT sku FROM catalog_product_entity WHERE sku IN ($sku_list_str)";
                $list_sku_exist = $readConnection->fetchAll($sql_sku_exist);
                $list_sku_exist = array_map('current', $list_sku_exist);
                if (!$create_new)
                    $sku_list = $list_sku_exist;
                else if (!$update_exist)
                    $sku_list = array_diff($sku_list, $list_sku_exist);
            }
            if ($sku_list) {
                $insert_batchsku = "";
                if (is_null($relation))
                    $sql_batchsku = "SELECT id,sku FROM bridge_incomingchannel_skus WHERE brand_id={$inc_info['id']} AND relation IS NULL";
                else
                    $sql_batchsku = "SELECT id,sku FROM bridge_incomingchannel_skus WHERE brand_id={$inc_info['id']} AND relation='{$relation}' AND parent_sku='{$inc_info['sku']}'";
                $batchsku_list = $readConnection->fetchAll($sql_batchsku);
                $sku_exist = array();
                foreach ($batchsku_list as $sku) {
                    if (in_array($sku['sku'], $sku_list))
                        $insert_batchsku.="UPDATE bridge_incomingchannel_skus SET current_flag='update',updated='{$curtime}',weightage={$inc_info['weightage']} WHERE id={$sku['id']};";
                    else
                        $insert_batchsku.="UPDATE bridge_incomingchannel_skus SET current_flag='not_available',weightage={$inc_info['weightage']},updated='{$curtime}' WHERE id={$sku['id']};";
                    $sku_exist[] = $sku['sku'];
                }
                $new_skus = array_diff($sku_list, $sku_exist);
                if ($new_skus) {
                    if (is_null($relation))
                        $insert_batchsku .="INSERT INTO  bridge_incomingchannel_skus (id,brand_id,sku,current_flag,weightage,created,updated) VALUES";
                    else
                        $insert_batchsku .="INSERT INTO  bridge_incomingchannel_skus (id,brand_id,sku,relation,parent_sku,current_flag,weightage,created,updated) VALUES";
                    foreach ($new_skus as $new_sku) {
                        if (is_null($relation)) {
                            $insert_batchsku.=" (NULL,{$inc_info['id']},'{$new_sku}','new',{$inc_info['weightage']},'{$curtime}','{$curtime}'),";
                        } else if ($relation) {
                            $insert_batchsku.=" (NULL,{$inc_info['id']},'{$new_sku}','{$relation}','{$inc_info['sku']}','new',{$inc_info['weightage']},'{$curtime}','{$curtime}'),";
                        }
                    }
                    $insert_batchsku = trim($insert_batchsku, ',') . ";";
                }
                if ($insert_batchsku) {
                    $writeConnection->raw_query($insert_batchsku);
                }
                Mage::getModel('bridge/pimqueue')->insert($sku_list, $inc_info['id'], $inc_info['option_id'], $inc_info['weightage'], $relation);
            } else if (is_null($relation)) {
                $this->makeSkuNotAvailable($inc_info['id']);
            }
        }
    }

    public function makeSkuNotAvailable($channel_id) {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $update_sku_query = "UPDATE bridge_incomingchannel_skus SET current_flag='not_available' WHERE brand_id='{$channel_id}'";
        $update_pimqueue_query = "UPDATE bridge_pim_processing_queue SET is_processed=1,relation_updated=1 WHERE channel_id='{$channel_id}'";
        $writeConnection->raw_query($update_sku_query);
        $writeConnection->raw_query($update_pimqueue_query);
    }

    public function getsku($channel_id = null) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $sql = "SELECT queue.id queue_id,queue.sku,GROUP_CONCAT(queue.store) store,inc_table.id,inc_table.brand_name,inc_table.url,inc_table.key,inc_table.secret,"
                . "inc_table.permanent_token,inc_table.currency,inc_table.content_source,inc_table.permanent_secret,inc_table.salt,inc_table.relative_product,inc_table.create_new,inc_table.create_option,"
                . "inc_table.relative_product,inc_table.slab,inc_table.channel_order,inc_table.update_exist,inc_table.option_id,inc_table.weightage,sku_table.relation relation,sku_table.parent_sku parent_sku,"
                . "inc_table.product_pricenquantity,inc_table.multi_price_rule,inc_table.product_status,mapping_table.data mapping FROM bridge_pim_processing_queue queue JOIN "
                . "bridge_incomingchannel_skus as sku_table ON sku_table.brand_id=queue.channel_id AND sku_table.sku=queue.sku AND sku_table.current_flag!='not_available' "
                . "join bridge_incomingchannels inc_table ON "
                . "queue.channel_id=inc_table.id  JOIN bridge_incomingchannel_mapping mapping_table ON mapping_table.brand_id=inc_table.id"
                . " WHERE inc_table.product_info AND inc_table.status AND inc_table.authorized AND queue.is_processed=0 AND (inc_table.update_exist OR inc_table.create_new)";
        if (!is_null($channel_id)) {
            $sql.=" AND queue.channel_id=$channel_id";
        }
        $sql.=" GROUP BY queue.sku,queue.channel_id ORDER BY queue_id limit 0,1";
        $sku = $readConnection->fetchRow($sql);
//        echo "<pre>";
//        print_r($sku);exit;
        return $sku;
    }

    public function getSkuUpdatedTime($sku, $channel_id = null) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $sql = "SELECT updated FROM bridge_incomingchannel_skus WHERE brand_id=$channel_id AND sku='{$sku} AND updated IS NOT NULL' ORDER BY id DESC LIMIT 0,1";
        $updated = $readConnection->fetchRow($sql);
        return date('Y-m-d H:i:s', strtotime(@$updated['updated']));
    }

    public function changeStatus($sku, $status, $channel_id, $updated_at = null, $product_status = 1, $pkey = null) {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        if (!is_null($pkey)) {
            $sql = "UPDATE bridge_incomingchannel_skus SET status=$status WHERE id='{$pkey}'";
        } else {
            $set = "status=$status";
            if ($status == 3) {
                $set .=",updated='{$updated_at}',product_status=$product_status";
            }
            $sql = "UPDATE bridge_incomingchannel_skus SET $set WHERE sku='{$sku}' AND brand_id=$channel_id";
        }
        $writeConnection->raw_query($sql);
    }

    public function getSkuRelation($sku_list, $channel_id) {
        $sku_list = "'" . implode("','", $sku_list) . "'";
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $sql = "SELECT GROUP_CONCAT(sku) as sku,parent_sku,relation FROM bridge_incomingchannel_skus "
                . " WHERE current_flag!='not_available' AND brand_id=$channel_id and parent_sku IN ($sku_list) GROUP BY parent_sku,relation;";
        $sku_list = $readConnection->fetchAll($sql);
        return $sku_list;
    }

    public function matchSkuExist($sku, $incoming_channel) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $sql = "SELECT COUNT(id) present FROM bridge_incomingchannel_skus WHERE status in (1,2) AND (parent_sku IS NULL OR parent_sku='') AND sku='{$sku}' AND brand_id=$incoming_channel LIMIT 0,1";
        $exist = $readConnection->fetchRow($sql);
        return @$exist['present'];
    }

    /**
     * get single skus which have no relation(upsell,crosssell,related,configurable)
     * @return type array of sku
     */
//    public function getSingleSkus() {
//        $resource = Mage::getSingleton('core/resource');
//        $readConnection = $resource->getConnection('core_read');
//        $writeConnection = $resource->getConnection('core_write');
//        //get the list of sku which have parent sku and the prarent sku itself
//        $sql1 = "SELECT CONCAT(sku_table.sku,',',sku_table.parent_sku) as sku  FROM bridge_incomingchannel_skus sku_table JOIN bridge_pim_processing_queue as queue  ON sku_table.sku=queue.sku WHERE sku_table.parent_sku IS NOT NULL AND sku_table.current_flag!='not_available' AND queue.is_processed AND queue.relation_updated!=1 "
//                . "GROUP BY sku_table.parent_sku,sku_table.sku";
//        $paired_skus = $readConnection->fetchALL($sql1);
////        echo "<pre>";
////        print_r($paired_skus);exit;
//        $paired_sku_list = 0;
//        if ($paired_skus) {
//            $paired_sku_list = implode(",", array_column($paired_skus, 'sku'));
//            $paired_sku_list = "'" . implode("','", array_unique(explode(',', $paired_sku_list))) . "'";
//        }
//        //update the records who have no related skus and the product is exist previously so need to update the product status
//        $query_update = "UPDATE bridge_pim_processing_queue SET relation_updated=1 WHERE sku NOT IN ($paired_sku_list) AND is_processed=1 AND (product_status IS NULL || product_status=0);";
//        $writeConnection->raw_query($query_update);
//        //get the sku list excluding the above list of sku list
//        $sql = "SELECT sku,product_status,channel_id FROM bridge_pim_processing_queue WHERE"
//                . " sku NOT IN ($paired_sku_list) AND relation_updated!=1"
//                . " GROUP BY sku";
//        $sku_list = $readConnection->fetchALL($sql);
//        return $sku_list;
//    }
}
