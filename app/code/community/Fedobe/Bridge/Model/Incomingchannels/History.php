<?php

class Fedobe_Bridge_Model_Incomingchannels_History extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('bridge/incomingchannels_history');
    }

    /**
     * 
     * @param type $incoming_channel
     * @param type $sku
     * @param type $entityId
     * @return type
     */
    public function checkPIM($incoming_channel, $sku, $entityId = null) {
        $store_values = unserialize($incoming_channel['mapping'])['store'];
        if (!is_null($entityId) && $entityId != '') {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            //first check the store view level channel priority is set for this sku
            $attributeId = Mage::getResourceModel('eav/entity_attribute')
                    ->getIdByCode('catalog_product', 'channel_priority');
            $product_priority_sql = "SELECT store_id,value FROM catalog_product_entity_int WHERE entity_id=$entityId AND attribute_id=$attributeId AND (value IS NOT NULL OR value!='')";
            $product_priority_results = $readConnection->fetchAll($product_priority_sql);

            $confirm = array();
            foreach ($product_priority_results as $key => $product_priority_result) {
                if ($product_priority_result['value'] == $incoming_channel['option_id'] && in_array($product_priority_result['store_id'], $store_values) !== false) {
                    $confirm[] = $product_priority_result['store_id'];
                    unset($product_priority_results[$key]);
                }
            }
            $product_priority_results = array_column($product_priority_results, 'store_id');
            //get the store id for which chennel priority is not set
            $channel_priority_nset = array_diff($store_values, $product_priority_results);
            //if store id exist for which chennel priority is not set then check the history
            //from which channel the data is updated for this store view
            if ($channel_priority_nset) {
                $history_sql = "SELECT brand_id,store_view FROM bridge_incomingchannels_history  WHERE sku='{$incoming_channel['sku']}' AND event NOT IN('price-updated','conflict') GROUP BY brand_id,store_view ORDER BY id desc";
                $history_result = $readConnection->fetchAll($history_sql);
                $history_result = array_column($history_result, 'store_view', 'brand_id');
                $channel_ids = implode(',', array_keys($history_result));
                //check the channel having store view content for this product have greter weight in compare to current incoming channel?
                if ($channel_ids) {
                    $channel_w_higher_priority_sql = "SELECT id FROM bridge_incomingchannels WHERE id IN ($channel_ids) AND weightage>{$incoming_channel['weightage']}";
                    $channel_w_higher_priority_res = $readConnection->fetchAll($channel_w_higher_priority_sql);
                    foreach ($channel_w_higher_priority_res as $channel_id) {
                        if (array_key_exists($channel_id['id'], $history_result)) {
                            $store_views = explode(',', $history_result[$channel_id['id']]);
                            $channel_priority_nset = array_diff($channel_priority_nset, $store_views);
                        }
                    }
                }
                //merge both store ids(set in channel priority and for store the incoming channel is mapped and have greater weightage)
                $confirm = array_merge($confirm, $channel_priority_nset);
            }
        }
        if (isset($confirm))
            foreach ($confirm as $key => $value) {
                $result[] = array_keys($store_values, $value);
            }
        return isset($confirm) ? array_column($result,0) : array_keys($store_values);
    }

    /**
     * 
     */
    public function checkEDI($incoming_channel, $sku) {
        
    }

}
