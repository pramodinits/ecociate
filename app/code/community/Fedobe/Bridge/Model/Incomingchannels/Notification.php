<?php
class Fedobe_Bridge_Model_Incomingchannels_Notification extends Mage_Core_Model_Abstract {
     public function _construct()
    {
          parent::_construct();
        $this->_init('bridge/incomingchannels_notification');
    }
   /**
     * Load incomingchannel track by brand_id
     *
     * @param intiger $brandId
     */
    public function loadAllByBrandId($brandId)
    {
        $this->setData($this->getResource()->loadAllByBrandId($brandId));
        return $this;
    }
   /**
     * Load incomingchannel track by product sku
     *
     * @param string $sku
     */
    public function loadBySku($sku)
    {
        $this->setData($this->getResource()->loadBySku($sku));
        return $this;
    }

}
