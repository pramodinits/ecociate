<?php

class Fedobe_Bridge_Model_Channelpriority extends Mage_Core_Model_Abstract {

    public function getAllOptions($withEmpty = false) {
        $channels = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->getData();
        return $channels;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray() {
        $_options = array();
        foreach ($this->getAllOptions() as $option) {
            $_options[$option["id"]] = $option["brand_name"];
        }
        return $_options;
    }

}