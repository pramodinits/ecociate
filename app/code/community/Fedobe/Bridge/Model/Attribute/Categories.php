<?php

class Fedobe_Bridge_Model_Attribute_Categories extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('bridge/attribute_categories');
    }

    /**
     * Load attribute category set by brand id
     *
     * @param intiger $brand_id
     */
    public function loadByBrandId($brand_id) {
        $this->setData($this->getResource()->loadByBrandId($id));
        return $this;
    }

    /**
     * delete attribute category set by brand id
     *
     * @param intiger $brand_id
     */
    public function deleteByBrandId($brand_id) {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $condition = array($writeConnection->quoteInto('brand_id=?', $brand_id));
        try {
            $writeConnection->delete('bridge_attribute_categories', $condition);
        } catch (Exception $e) {
            print_r($e->getMessage());
            exit;
        }
    }

}
