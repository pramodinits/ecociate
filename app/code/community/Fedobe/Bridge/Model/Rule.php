<?php

class Fedobe_Bridge_Model_Rule extends Mage_Rule_Model_Abstract {

    protected $productid;
    /**
     * Limitation for products collection
     *
     * @var int|array|null
     */
    protected $_productsFilter = null;

    /**
     * Init resource model and id field
     */
    protected function _construct() {
        parent::_construct(array());
        if ($page = Mage::registry('channel_condition')) {
            $this->addData($page->getData());
            if($page->getProductId()){
                $this->_productsFilter=$page->getProductId();
            }
            // set entered data if was error when we do save
            $data = Mage::getSingleton('adminhtml/session')->getPageData(true);
            if (!empty($data)) {
                $this->addData($data);
            }
            $this->getConditions()->setJsFormObject('rule_conditions_fieldset');
        }
    }

    /**
     * Getter for rule conditions collection
     *
     * @return Mage_CatalogRule_Model_Rule_Condition_Combine
     */
    public function getConditionsInstance() {
        return Mage::getModel('catalogrule/rule_condition_combine');
    }

    /**
     * Getter for rule actions collection
     *
     * @return Mage_CatalogRule_Model_Rule_Action_Collection
     */
    public function getActionsInstance() {
        return Mage::getModel('catalogrule/rule_action_collection');
    }

    /**
     * Callback function for product matching
     *
     * @param $args
     * @return void
     */
    public function validateProduct($args) {
        $product = clone $args['product'];
        $product->setData($args['row']);
        $results = array();
        $results = (int) $this->getConditions()->validate($product);
        $this->productid[$product->getId()] = $results;
    }

    public function getMatchingProductIds($conditions_serialized=null) {
        if(!is_null($conditions_serialized)){
            $this->setConditionsSerialized($conditions_serialized);
        }
        if (is_null($this->productid)) {
        $this->productid = array();
        $this->setCollectedAttributes(array());
        /** @var $productCollection Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection */
        $productCollection = Mage::getResourceModel('catalog/product_collection');
            if ($this->_productsFilter) {
                $productCollection->addIdFilter($this->_productsFilter);
            }
        $this->getConditions()->collectValidatedAttributes($productCollection);        
        Mage::getSingleton('core/resource_iterator')->walk(
                $productCollection->getSelect(), array(array($this, 'validateProduct')), array(
            'attributes' => $this->getCollectedAttributes(),
            'product' => Mage::getModel('catalog/product'),
                )
        );
        }

        return array_keys(array_filter($this->productid));
    }

}
