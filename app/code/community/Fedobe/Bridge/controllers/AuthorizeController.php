<?php

/**
 *
 * @author     Darko Goleš <darko.goles@inchoo.net>
 * @package    Inchoo
 * @subpackage RestConnect
 * 
 * Url of controller is: http://magento.loc/restconnect/test/[action] 
 */
class Fedobe_Bridge_AuthorizeController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        $id = $this->getRequest()->getParam('id');
        $inc_channel_data = Mage::getmodel('bridge/incomingchannels')->load($id);
        if ($inc_channel_data->getId() && !$inc_channel_data['authorized']) {
            $params = array(
                'siteUrl' => $inc_channel_data['url'] . '/oauth',
                'requestTokenUrl' => $inc_channel_data['url'] . '/oauth/initiate',
                'accessTokenUrl' => $inc_channel_data['url'] . '/oauth/token',
                'authorizeUrl' => $inc_channel_data['url'] . '/admin/oauth_authorize', //This URL is used only if we authenticate as Admin user type
                'consumerKey' => $inc_channel_data['key'], //Consumer key registered in server administration
                'consumerSecret' => $inc_channel_data['secret'], //Consumer secret registered in server administration
                'callbackUrl' => Mage::getBaseUrl() . 'bridge/authorize/callback/id/' . $inc_channel_data->getId(), //Url of callback action below
            );

            // Initiate oAuth consumer with above parameters
            $consumer = new Zend_Oauth_Consumer($params);
            // Get request token
            $requestToken = $consumer->getRequestToken();
            // Get session
            $session = Mage::getSingleton('core/session');
            // Save serialized request token object in session for later use
            $session->setRequestToken(serialize($requestToken));
            // Redirect to authorize URL
            $consumer->redirect();
        } else {
            echo "Sorry Invalid Url";
            exit;
        }
    }

    public function callbackAction() {
        $id = $this->getRequest()->getParam('id');
        $inc_channel_data = Mage::getmodel('bridge/incomingchannels');
        $inc_channel_data->load($id);

        //oAuth parameters
        $params = array(
            'siteUrl' => $inc_channel_data['url'] . '/oauth',
            'requestTokenUrl' => $inc_channel_data['url'] . '/oauth/initiate',
            'accessTokenUrl' => $inc_channel_data['url'] . '/oauth/token',
            'consumerKey' => $inc_channel_data['key'],
            'consumerSecret' => $inc_channel_data['secret']
        );

        // Get session
        $session = Mage::getSingleton('core/session');
        // Read and unserialize request token from session
        $requestToken = unserialize($session->getRequestToken());
        // Initiate oAuth consumer
        $consumer = new Zend_Oauth_Consumer($params);
        // Using oAuth parameters and request Token we got, get access token
        $acessToken = $consumer->getAccessToken($_GET, $requestToken);
        $per_token['permanent_token'] = $acessToken->getToken();
        $per_token['permanent_secret'] = $acessToken->getTokenSecret();
        $per_token['authorized'] = 1;
        $inc_channel_data->addData($per_token);
        $inc_channel_data->save();
        echo "Successfully Authorized";
        exit;
    }

}
