<?php

class Fedobe_Bridge_RetailerController extends Mage_Core_Controller_Front_Action {

    public function signupAction() {
        $data = Mage::app()->getRequest()->getPost();
        Mage::getSingleton('customer/session')->setData('retailer', $data);

        $user_model = Mage::getModel('Admin/User');
        //check if username exist
        $error_msg = "";
        $user_model->setData(array('username' => $data['username']));
        $username_exist = $user_model->userExists();
        if ($username_exist) {
            $error_msg.='User Name,';
        }
        //check email id exist
        $user_model->setData(array('email' => $data['email'], 'username' => null));
        $email_exist = $user_model->userExists();
        if ($email_exist) {
            $error_msg.='Email ';
        }
        if ($error_msg) {
            $error_msg = trim($error_msg, ',') . ' already exist.';
            //set the error msg on session to flash
            Mage::getSingleton('customer/session')->addError($error_msg);
        } else {
            try {
                $user_model->setData($data);
                $api2_role_id = Mage::getmodel('bridge/outgoingchannels')->getRestRoleId();
                $user_model->setApi2Roles(array($api2_role_id));
                $user_model->save();
                $user_id = $user_model->getId();
                $role_id = Mage::getModel('Admin/Role')->getCollection()->addFieldToFilter('role_name', 'Bridge User')->addFieldToSelect('role_id')->getFirstItem()->getData();
                $user_model->setRoleIds($role_id)
                        ->setRoleUserId($user_model->getUserId())
                        ->saveRelations();
                //set the success msg on session to flash
                Mage::getSingleton('customer/session')->addSuccess($this->__('The account information has been saved.'));
                Mage::getSingleton("customer/session")->unsetData('retailer');
            } catch (Exception $e) {
                // return "failuer";
                Mage::getSingleton('customer/session')->addError($this->__('The account information has not been saved.Please try again.'));
            }
        }
        Mage::app()->getResponse()->setRedirect($_SERVER['HTTP_REFERER']);
    }

}
