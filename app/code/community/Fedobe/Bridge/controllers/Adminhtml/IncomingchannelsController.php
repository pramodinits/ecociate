<?php

class Fedobe_Bridge_Adminhtml_IncomingchannelsController extends Mage_Adminhtml_Controller_Action {

    public $isAdmin;

    protected function _construct() {
        parent::_construct();
/*$is_admin = Mage::helper('bridge')->isAdmin();
        if ($is_admin)
            $this->isAdmin = true;
        else
            $this->isAdmin = false;*/
    }

    public function _initAction() {
//        $this->loadLayout()->_setActiveMenu('fedobe/bridge');
        $this->_title(Mage::helper('bridge')->__('Fedobe Extension'));
    }

    public function indexAction() {
        $this->_initAction();
        $this->_title(Mage::helper('bridge')->__('Manage Incoming Channels'));
        $this->loadLayout();
        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function preDispatch() {
        $bypassaction = array('applycontentrule_cron');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }

    public function editAction() {
        $this->_initAction();
        $id = $this->getRequest()->getParam('id'); //        
        $model = Mage::getModel('bridge/incomingchannels');
        $session_security_salt = $id ? "{$id}_incoming_security_salt" : "incoming_security_salt";
        Mage::getSingleton('core/session')->unsetData("$session_security_salt");
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('bridge')->__('This Channel no longer exists.')
                );
                $this->_redirect('*/*');
                return;
            }
            $mapping_Data = Mage::getmodel('bridge/incomingchannels_mapping')->getCollection()->addFieldToFilter('brand_id', $id)->addFieldToSelect('data')->getFirstItem()->getData();
            $mapping_Data = @$mapping_Data['data'];
            $store_view = Mage::getmodel('bridge/storeview_label')->getCollection()
                            ->addFieldToSelect(array('value', 'type'))
                            ->addFieldToFilter('brand_id', $id)
                            ->addFieldToFilter('store_id', $this->getCurrentStore())
                            ->addFieldToFilter('type', array('in' => array('meta_title', 'meta_description')))->getData();
            foreach ($store_view as $view) {
                $$view['type'] = $view['value'];
            }
            $model->setMetaTitle(@$meta_title);
            $model->setMetaDescription(@$meta_description);

            $_SESSION['channel_type'] = $model->getChannelType();
            $_SESSION['brand_id'] = $id;
        }
        //set the security salt for this outgoing channel if paid module is installed
        Mage::getSingleton('core/session')->unsetData($session_security_salt);
        if (Mage::helper('core')->isModuleEnabled('Fedobe_Securitycheck')) {
            $security_salt = Mage::getModel('securitycheck/salt')->loadByIncomingChannel($model->getId())->getSalt();
            Mage::getSingleton('core/session')->setData("$session_security_salt", $security_salt);
        }

        $this->_title($model->getId() ? $model->getBrandName() : $this->__('New Incomingchannel'));
// set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }
        if (@$mapping_Data) {
            $model->setMappingData($mapping_Data);
        }

        Mage::register('incomingchannel_data', $model);
        $this->loadLayout();
        //remove store switcher for new add of a channel
        if (!$id) {
            $this->getLayout()->getBlock('left')->unsetChild('store_switcher');
        }
//        echo "<pre>";
//        print_r(Mage::getSingleton('core/session')->getIncomingSecuritySalt());exit;
        $this->renderLayout();
    }

    public function saveAction() {
        if ($this->getRequest()->getPost()) {
            $data = $this->getRequest()->getPost();
            try {
                Mage::getSingleton('adminhtml/session')->setPageData($data);
                $model = Mage::getModel('bridge/incomingchannels');
                $model->load(@$this->getRequest()->getParam('id'));
                $changed_slab_order = 0;

                if ($model->getSlab() != $data['slab'] || $model->getChannelOrder() != $data['channel_order']) {
                    $changed_slab_order = 1;
                }
                $session_security_salt = $model->getId() ? "{$model->getId()}_incoming_security_salt" : "incoming_security_salt";
                $prev_pim_cron = $model->getPimCron();
                $prev_edi_cron = $model->getEdiCron();
                $prev_authorization = $model->getAuthorized();
//validata the oauth token and secret
                $is_validated = $this->validateToken($model, $data);
                if (!$is_validated) {
                    $this->_getSession()->addError("Sorry Invalid Key or Secret");
                    echo Mage::app()->getResponse()->setRedirect($_SERVER['HTTP_REFERER']);
                    exit;
                } else if (@$data['weightage']) {
                    if (@$data['id']) {
                        $is_exist = $model->getCollection()->addFieldToFilter('weightage', $data['weightage'])
                                        ->addFieldToFilter('id', array('neq' => $data['id']))
                                        ->addFieldToSelect('id')->getFirstItem();
                    } else {
                        $is_exist = $model->getCollection()->addFieldToFilter('weightage', $data['weightage'])
                                        ->addFieldToSelect('id')->getFirstItem();
                    }
                    if ($is_exist->getId()) {
                        $this->_getSession()->addError("An Incoming Channel with this wightage is exist");
                        echo Mage::app()->getResponse()->setRedirect($_SERVER['HTTP_REFERER']);
                        exit;
                    }
                }
//end
                unset($data['rule']);
//make the authorization status to 0 if any secret has been changed
                if ($this->tokenChanged($model, $data))
                    $data['authorized'] = 0;
                $data['status'] = $data['channel_status'];
                $data['relative_product'] = isset($data['relative_product']) ? array_sum($data['relative_product']) : 0;
                ;
                //save security salt if paid module exist
                if (Mage::getSingleton('core/session')->getData($session_security_salt) && Mage::helper('core')->isModuleEnabled('Fedobe_Securitycheck')) {
                    $data['salt'] = "";
                }
                //add the incoming channel to attribute channel priority option
                if (!$this->getRequest()->getParam('id')) {
                    $option_id = Mage::getModel('bridge/incomingchannels')->addChannelOption($data['brand_name']);
                    $data['option_id'] = $option_id;
                }
//                echo "<pre>";
//                print_R($data);exit;
                $model->setData($data);
                $model->save();
                $cron_authorized = isset($data['authorized']) ? $data['authorized'] : $prev_authorization;
                $this->setCronSchedule($model, $prev_pim_cron, $prev_edi_cron, $cron_authorized);

                //save security salt if paid module exist
                if (Mage::helper('core')->isModuleEnabled('Fedobe_Securitycheck')) {
                    $security_salt_model = Mage::getModel('securitycheck/salt')->loadByIncomingChannel($model->getId());
                    if (Mage::getSingleton('core/session')->getData($session_security_salt)) {
                        $security_salt_model->addData(array('id' => $security_salt_model->getId(), 'salt' => Mage::getSingleton('core/session')->getData($session_security_salt), 'incomingchannel_id' => $model->getId(), 'updated' => date('Y-m-d H:i:s')));
                        $security_salt_model->save();
                    } else {
                        $security_salt_model->delete();
                    }
                }
                //end
                if (@$data['mapping']) {
                    if (is_array(@$data['mapping']['brandfieldset']))
                        $fieldset = array_combine($data['mapping']['brandfieldset'], $data['mapping']['storefieldset']);
                    $category = @$data['mapping']['category'];
                    $store = @$data['mapping']['store'];
                    unset($data['mapping']['store']);
                    unset($data['mapping']['brandfieldset']);
                    unset($data['mapping']['storefieldset']);
                    unset($data['mapping']['category']);
                    if (@$fieldset) {
                        $data['mapping']['fieldset'] = $fieldset;
                        foreach ($fieldset as $brandid => $storeid) {
                            if (!empty(@$data['mapping'][$brandid])) {
                                foreach ($data['mapping'][$brandid] as $key => $attributes) {
                                    $data['mapping'][$brandid] +=array($attributes['brand_attr_id'] => $attributes['store_attr_id']);
                                    unset($data['mapping'][$brandid][$key]);
                                }
                            }
                        }
                    }
                    $data['mapping']['category'] = array();
                    if (@$category)
                        foreach ($category as $cats) {
                            $data['mapping']['category'] +=array($cats['brand'] => $cats['store']);
                        }
                    if (@$data['mapping']['option_mapping']) {
                        $option_mapping = $data['mapping']['option_mapping'];
                        unset($data['mapping']['option_mapping']);
                        foreach ($option_mapping as $attribute_id => $options) {
                            $data['mapping']['option_mapping'][$attribute_id] = array();
                            if (is_array($options)) {
                                foreach ($options as $option) {
                                    $store_option_ids = is_array($option['store']) ? implode(',', $option['store']) : $option['store'];
                                    $data['mapping']['option_mapping'][$attribute_id] +=array($option['brand'] => $store_option_ids);
                                }
                            }
                        }
                    }


                    $data['mapping']['store'] = array();
                    if (@$store)
                        foreach ($store as $storeids) {
                            $data['mapping']['store'] +=array($storeids['brand'] => $storeids['store']);
                        }
                    $mapping['brand_id'] = $model->getId();
                    $mapping['data'] = serialize($data['mapping']);
                }
                $mapping_model = Mage::getmodel('bridge/incomingchannels_mapping');
                $mapping['id'] = $mapping_model->getCollection()->addFieldToFilter('brand_id', $model->getId())->addFieldToSelect('id')->getFirstItem()->getId();
                if ($mapping['id'] && !@$data['mapping']) {
                    $mapping_model->load($mapping['id']);
                    $mapping_model->delete();
                } else if (@$data['mapping']) {
                    $mapping_model->setData($mapping);
                    $mapping_model->save();
                }

//end
                //update edi cron table for update convert price

                if (!$data['id']) {
                    $edicrondata['channel_id'] = $model->getId();
                    $edicrondata['channel_type'] = "incoming"; //$data['channel_type'];
                    $edicrondata['status'] = $data['channel_status'];
//                    $edicrondata['is_processed'] = 1;
                    $cronmodel = Mage::getModel('bridge/bridgeapi_edichannelcron')->setData($edicrondata)->save();
                } else {
                    $edicrondata = array("status" => $data['channel_status']);
                    $resource = Mage::getSingleton('core/resource');
                    $write = $resource->getConnection('core_write');
                    $where = "channel_id = {$model->getId()} AND channel_type ='incoming'";
                    $write->update("bridge_edi_channelcron", $edicrondata, $where);
                }


                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('bridge')->__('The Channel has been saved.')
                );
                Mage::getSingleton('adminhtml/session')->setPageData(false);
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        //if slab or order is changed the ulpdate this in edi table if any inventory is exist for this channel
        if ($this->getRequest()->getParam('id') && $changed_slab_order) {
            $bridge_edi_crontable = $resource->getTableName('bridge/incomingchannels_product');
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection("core_write");
            $writeConnection->raw_query("UPDATE $bridge_edi_crontable SET `slab`={$data['slab']},`order`={$data['channel_order']} WHERE channel_type='incoming' AND channel_id={$model->getId()};");
        }
        //end
// The following line decides if it is a "save" or "save and continue"
        if ($this->getRequest()->getParam('back')) {
            $this->_redirect('*/*/edit', array('id' => $model->getId(), 'store' => @$this->getRequest()->getParam('store')));
        } else {
            $this->_redirect('*/*/');
        }
    }

    public function getCurrentStore() {
        $defaultstoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
        $store_id = Mage::app()->getRequest()->getParam('store') ? Mage::app()->getRequest()->getParam('store') : $defaultstoreId;
        return $store_id;
    }

    /**
     * validate oauth token and secret
     * @param type $id channel id
     * @param type $data token and secret
     * @return boolean 
     */
    public function validateToken($model, $data) {
        $callback_url = Mage::getBaseUrl();
        if ($this->tokenChanged($model, $data)) {//$model->getUrl() != $data['url'] || $model->getKey() != $data['key'] || $model->getSecret() != $data['secret']) {
            $params = array(
                'siteUrl' => $data['url'] . '/oauth',
                'requestTokenUrl' => $data['url'] . '/oauth/initiate',
                'accessTokenUrl' => $data['url'] . '/oauth/token',
                'authorizeUrl' => $data['url'] . '/admin/oauth_authorize', //This URL is used only if we authenticate as Admin user type
                'consumerKey' => $data['key'], //Consumer key registered in server administration
                'consumerSecret' => $data['secret'], //Consumer secret registered in server administration
                'callbackUrl' => $callback_url, //Url of callback action below
            );

// Initiate oAuth consumer with above parameters
            $consumer = new Zend_Oauth_Consumer($params);
            try {
// Get request token
                $requestToken = $consumer->getRequestToken();
            } catch (Exception $e) {
//if invalid credentials are given
                return false;
            }
            return @$requestToken->getTokenSecret() ? true : false;
        }
        return true;
    }

    public function tokenChanged($pre, $post) {
        if ($pre->getUrl() != $post['url'] || $pre->getKey() != $post['key'] || $pre->getSecret() != $post['secret']) {
            return true;
        }
        return false;
    }

    public function deleteAction() {
        $response = Mage::getModel('bridge/incomingchannels')->delete($this->getRequest()->getParam('id'));
        if ($response['status'] == 'error')
            Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('bridge')->__($response['msg'])
            );
        else
            Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('bridge')->__($response['msg'])
            );
        $this->_redirect('*/*/');
    }

    public function ajax_addruleAction() {
        if ($this->getRequest()->getPost()) {
            $serdata = $this->getRequest()->getPost();
            parse_str($serdata['rule'], $unserdata);
            $data = $unserdata;
            unset($data['rule']);
            $data = $unserdata;
            $data['brand_id'] = $serdata['brand_id'];
            $data['conditions_html'] = $serdata['conditions_html'];
            $arr = array();
            $unserdata['rule']['actions'][1] = $unserdata['rule']['conditions'][1];
            foreach ($unserdata['rule']['actions'] as $id => $cdata) {
                $path = explode('--', $id);
                $node = & $arr;
                for ($i = 0, $l = sizeof($path); $i < $l; $i++) {
                    if (!isset($node['conditions'][$path[$i]])) {
                        $node['conditions'][$path[$i]] = array();
                    }
                    $node = & $node['conditions'][$path[$i]];
                }
                foreach ($cdata as $k => $v) {
//check for gender attribute array issue
                    if (is_array($v)) {
                        $v = implode(',', $v);
                    }
                    $node[$k] = $v;
                }
            }
            $data['conditions_serialized'] = serialize($arr['conditions'][1]);
            $data['apply_on'] = serialize($data['apply_on']);
            $data['created'] = date('Y-m-d H:i:s');
            $model = Mage::getModel('bridge/incomingchannels_action');
            $model->setData($data);
            $model->save();
            Mage::helper('bridge')->change_edicron_flag('is_processed', 0, $this->getRequest()->getPost('brand_id'), 'incoming');
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection("core_write");
            $writeConnection->raw_query("UPDATE bridge_incomingchannel_product_info SET is_processed=2 WHERE channel_id={$model->getBrandId()} AND channel_type='incoming';");
            echo $model->getId();
            exit;
        }
    }

    public function ajax_deleteruleAction() {
        if ($this->getRequest()->getPost('id')) {
            $model = Mage::getModel('bridge/incomingchannels_action');
            $model->load($this->getRequest()->getPost('id'));
            Mage::helper('bridge')->change_edicron_flag('is_processed', 0, $model->getBrandId(), 'incoming');
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection("core_write");
            $writeConnection->raw_query("UPDATE bridge_incomingchannel_product_info SET is_processed=2 WHERE channel_id={$model->getBrandId()} AND channel_type='incoming';");
            $model->delete();
            exit;
        }
    }

    /**
     * get the values of mapping array of the keys provided in keys array
     * @param type $mapping
     * @param type $keys
     * @return type array
     */
    public function weightageunique_validationAction() {
        $exist = Mage::getmodel('bridge/incomingchannels')->checkUniqueWeightage($this->getRequest()->getParam('id'), $this->getRequest()->getParam('weightage'));
        echo $exist;
        exit;
    }

    public function gridAction() {
        $this->loadLayout();
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('bridge/adminhtml_incomingchannels_edit_tab_listing_grid')->toHtml()
        );
    }

    public function checkaddonAction() {
        $channel_type = $this->getRequest()->getParam('channel_type');
        if ($channel_type != 'magento') {
            $helperchannel = Mage::helper('bridge')->ModuleHelper($channel_type);
            $module = 'Fedobe_' . ucfirst($helperchannel);
            $addon_enabled = Mage::helper('core')->isModuleEnabled($module);
            if ($addon_enabled) {
                $this->_redirect('adminhtml/' . $channel_type . '/edit/channel_type/' . $channel_type);
            } else {
                //Here to check the decsription of the corresponding sales description
                Mage::register('addon', $channel_type);
                $this->loadLayout();
                $this->renderLayout();
            }
        } else {
            $this->_redirect('*/*/edit/channel_type/magento');
        }
    }

    public function checkcalculationAction() {
        $params = $this->getRequest()->getParam('price_rule');
        $input = $params;
        try {
            $pattern = '/{{([a-z_A-z]*)}}/i';
            $replacement = '$' . '$1';
            $finalstr = (String) preg_replace($pattern, $replacement, $input);
            $nolines = explode(';', $finalstr);
            foreach ($nolines as $key => $value) {
                eval('return ' . $value . ';');
            }
            echo 'Your store product price({{price}}) will be  = ' . round($price, 2);
        } catch (Exception $e) {
            echo "Invalid input ! we could not parse.";
        }
    }

    public function setCronSchedule($incoming_channel, $prev_pim_cron, $prev_edi_cron, $authorized) {
        if (!file_exists($_SERVER["DOCUMENT_ROOT"] . '/bridgecron')) {
            mkdir($_SERVER["DOCUMENT_ROOT"] . '/bridgecron');
        }
        chmod($_SERVER["DOCUMENT_ROOT"] . '/bridgecron', 0777);
        $pim_cron_link = $this->getUrl('adminhtml/sync/sync_batched_skus_for_pim');
        $edi_cron_link = $this->getUrl('adminhtml/sync/sync_batch_edi');
        $edi_sku_refresh_cron_link = $this->getUrl('adminhtml/sync/sync_batched_skus_for_edi');
        $pim_cron_link = strstr($pim_cron_link, '/key/', true) . "/id/{$incoming_channel['id']}";
        $edi_cron_link = strstr($edi_cron_link, '/key/', true) . "/id/{$incoming_channel['id']}";
        $edi_sku_refresh_cron_link = strstr($edi_cron_link, '/key/', true) . "/id/{$incoming_channel['id']}";
        $crons = shell_exec('crontab -l');

        //remove previous pim cron
        if ($prev_pim_cron && $prev_pim_cron != 'never') {
            $pim_cron_remove = "$prev_pim_cron  wget --quiet -O /dev/null $pim_cron_link" . PHP_EOL;
            $crons = str_replace($pim_cron_remove, "", $crons);
        }

        //remove previous edi cron
        if ($prev_edi_cron && $prev_edi_cron != 'never') {
            $pim_edi_remove = "$prev_edi_cron  wget --quiet -O /dev/null $edi_cron_link" . PHP_EOL;
            $crons = str_replace($pim_edi_remove, "", $crons);
            $pim_edi_remove = "0 */2 * * *  wget --quiet -O /dev/null $edi_sku_refresh_cron_link" . PHP_EOL;
            $crons = str_replace($pim_edi_remove, "", $crons);
        }
        //echo $crons;exit;
        //set the cron job for new update
        if ($incoming_channel['channel_status'] && $authorized) {

            //pim cron
            if ($incoming_channel['pim_cron'] != 'never' && $incoming_channel['product_info'])
                $crons .= "{$incoming_channel['pim_cron']}  wget --quiet -O /dev/null $pim_cron_link" . PHP_EOL;
            //edi cron
            if ($incoming_channel['edi_cron'] != 'never' && $incoming_channel['product_pricenquantity']) {
                $crons .= "{$incoming_channel['edi_cron']}  wget --quiet -O /dev/null $edi_cron_link" . PHP_EOL;
                $crons .= "0 */2 * * *  wget --quiet -O /dev/null $edi_sku_refresh_cron_link" . PHP_EOL;
            }
        }


        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/bridgecron/crontab.txt', $crons);
        exec("crontab {$_SERVER['DOCUMENT_ROOT']}/bridgecron/crontab.txt");
    }

    /**
     * Update product(s) status action
     *
     */
    public function massStatusAction() {
        $productIds = (array) $this->getRequest()->getParam('product');
        $channel_id = $this->getRequest()->getParam('id');
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        $status = (int) $this->getRequest()->getParam('status');
        $model = Mage::getModel('bridge/incomingchannels');
        $model->load($channel_id);
        $channel_type = $model->getChannelType();
        if ($channel_type == 'magento') {
            $channel_type = 'incomingchannels';
        }
        // print_r(Mage::registry('incomingchannel_data'));exit;
        try {
            $this->_validateMassStatus($productIds, $status);
            Mage::getSingleton('catalog/product_action')
                    ->updateAttributes($productIds, array('status' => $status), $storeId);

            $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been updated.', count($productIds))
            );
        } catch (Mage_Core_Model_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()
                    ->addException($e, $this->__('An error occurred while updating the product(s) status.'));
        }

        $this->_redirect('*/' . $channel_type . '/edit', array('id' => $channel_id, 'store' => $storeId, 'activeTab' => 'listing_section'));
    }

    public function masslogAction() {
        $productIds = (array) $this->getRequest()->getParam('product');
        $channel_id = $this->getRequest()->getParam('id');
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        $model = Mage::getModel('bridge/incomingchannels');
        $model->load($channel_id);
        $channel_type = $model->getChannelType();
        $ids = implode(',', $productIds);
        try {
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection("core_write");
            $table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_incominglog');
            $sql = "DELETE FROM $table WHERE `channel_type`='$channel_type' AND `bridge_channel_id`=$channel_id AND FIND_IN_SET(id,'$ids') ";
            $writeConnection->query($sql);
            $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been deleted.', count($productIds))
            );
        } catch (Mage_Core_Model_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()
                    ->addException($e, $this->__('An error occurred while deleting the product(s) logs.'));
        }

        $this->_redirect('*/' . $channel_type . '/edit', array('id' => $channel_id, 'store' => $storeId, 'activeTab' => 'notification_section'));
    }

    public function massreprocesslogAction() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection("core_read");
        $table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_incominglog');
        $productIds = (array) $this->getRequest()->getParam('product');
        $channel_id = $this->getRequest()->getParam('id');
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        $model = Mage::getModel('bridge/incomingchannels');
        $model->load($channel_id);
        $channel_type = $model->getChannelType();
        $ids = implode(',', $productIds);
        $sku_list = $readConnection->fetchAll("SELECT sku FROM $table WHERE id IN($ids);");
        $sku_list = $sku_list ? array_column($sku_list, 'sku') : array();
        try {
            $helper = Mage::helper($channel_type);
            if (is_object($helper) && method_exists($helper, 'reProcessLogs')) {
                $helper->reProcessLogs($bridge_channel_id, $sku_list);
                $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) have been reprocessed.', count($productIds))
                );
            } else {
                $this->_getSession()->addError(
                        $this->__('`reProcessLogs` method not defined in %s helper to reprocess skipped data.', $channel_type)
                );
            }
        } catch (Mage_Core_Model_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()
                    ->addException($e, $this->__('An error occurred while reprocessing the product(s) logs.'));
        }

        $this->_redirect('*/' . $channel_type . '/edit', array('id' => $channel_id, 'store' => $storeId, 'activeTab' => 'notification_section'));
    }

    /**
     * Validate batch of products before theirs status will be set
     *
     * @throws Mage_Core_Exception
     * @param  array $productIds
     * @param  int $status
     * @return void
     */
    public function _validateMassStatus(array $productIds, $status) {
        if ($status == Mage_Catalog_Model_Product_Status::STATUS_ENABLED) {
            if (!Mage::getModel('catalog/product')->isProductsHasSku($productIds)) {
                throw new Mage_Core_Exception(
                $this->__('Some of the processed products have no SKU value defined. Please fill it prior to performing operations on these products.')
                );
            }
        }
    }

    public function gridnotificationAction() {
        $this->loadLayout();
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('bridge/adminhtml_incomingchannels_edit_tab_notification_grid')->toHtml()
        );
    }

    public function exportCsvAction($product) {
        ob_start();
        // echo "<pre>";
        $fileName = 'Icc.csv';
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $grid = $this->getLayout()->createBlock('fedobe_bridge_block_adminhtml_incomingchannels_edit_tab_listing_grid');
        $grid->_prepareGrid();
        $collection = $grid->getCollection();
        $limitcount = $collection->getSize();
        //echo $limitcount;exit;
        $select = $collection->getSelect()->limit($limitcount);
        $query = $readConnection->fetchAll($select);
        $attribute_set = array();
        $attribute_header = array();

        foreach ($query as $product_1) {
            $product = Mage::getModel('catalog/product')->load($product_1['entity_id']);
            $product_data = $product->getData();
            $stock_data = $product->getStockItem()->getData();
            $product_data['qty'] = $stock_data['qty'];
            $product_data['is_in_stock'] = $stock_data['is_in_stock'];
            unset($product_data['stock_item']);
            $product_attr_set = $product_data['attribute_set_id'];
            if (!in_array($product_attr_set, array_keys($attribute_set))) {
                $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
                $attributeSetModel->load($product_attr_set);
                $attributeSetName = $attributeSetModel->getAttributeSetName();
                $attribute_set[$product_attr_set] = $attributeSetName;
                $attribute_nm[] = $attributeSetName;
                if (!isset($attribute_header[$product_attr_set])) {
                    $i = 0;
                    $attributes = Mage::getModel('catalog/product_attribute_api')->items($product_attr_set);
                    $attrcode_type = array_combine(array_column($attributes, 'code'), array_column($attributes, 'type'));
                    $arr_colmn = array_column($attributes, 'code');
                    $diff_header = array_diff(array_keys($product_data), $arr_colmn); //exit;
                    $merge = array_merge($diff_header, $arr_colmn);
                    $attribute_header[$product_attr_set][] = $merge;
                    $str[$product_attr_set][$i] = implode(",", $merge) . "\n";
                    //$str.="\n";
                    $i++;
                }
            }
            $t_merge = $attribute_header[$product_attr_set][0];
            $merge_data = array_fill_keys($t_merge, '');
            $mdata = array_merge($merge_data, $product_data);


            //to find out those attributes having type select
            $comm = array_merge($mdata, $attrcode_type);
            $sele = array_filter($comm, function($var) {
                return ($var && $var == 'select');
            });
            foreach ($sele as $k8 => &$v8) {
                $v8 = $product->getAttributeText($k8);
            }
            $mdata = array_merge($mdata, $sele);
            $mdata = array_map(function($v) {
                $v = preg_replace("/\r|\n/", "", $v); //trim(strip_tags($v));
                $v = trim(strip_tags($v));
                $v = str_replace("\n", "", $v);
                $v = str_replace(",", "", $v);
                if (is_array($v))
                    $v = '';
                return $v;
            }, $mdata);
            //print_r($mdata);exit;
            $mdata['attribute_set_id'] = $attribute_set[$mdata['attribute_set_id']];
            $str[$product_attr_set][$i] = implode(",", $mdata) . "\n";
            //$str.="\n";
            // $attribute_header[$product_attr_set][$i] = $mdata;
            $i++;
        }
        foreach ($str as $v) {
            foreach ($v as $k1 => $v1) {
                echo $v1;
            }
        }
        //echo $str; 
        header("Content-type: application/csv");
        header("Content-Disposition: attachment;filename=" . $fileName);
        exit;
    }

    /**
     *  Export edi grid to Excel XML format
     */
    public function exportExcelAction() {
        ob_start();
        $fileName = 'icc.xls';
        $grid = $this->getLayout()->createBlock('fedobe_bridge_block_adminhtml_incomingchannels_edit_tab_listing_grid');
        $grid->_isExport = true;
        // $grid->$_defaultLimit = 300;
        $grid->_prepareGrid();
        $collection = $grid->getCollection();
        $attribute_set = array();
        $attribute_header = array();
        foreach ($collection as $product) {
            $product = Mage::getModel('catalog/product')->load($product->getId());
            $product_data = $product->getData(); //print_r($product_data);exit;
            $stock_data = $product->getStockItem()->getData();
            $product_data['qty'] = $stock_data['qty'];
            $product_data['is_in_stock'] = $stock_data['is_in_stock'];
            unset($product_data['stock_item']);
            $product_attr_set = $product_data['attribute_set_id'];
            // echo $product_attr_set."<br>";
            if (!in_array($product_attr_set, $attribute_set)) {
                $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
                $attributeSetModel->load($product_attr_set);
                $attributeSetName = $attributeSetModel->getAttributeSetName();
                $attribute_set[$product_attr_set] = $attributeSetName;
                $attribute_nm[] = $attributeSetName;
                if (!isset($attribute_header[$product_attr_set])) {
                    $attributes = Mage::getModel('catalog/product_attribute_api')->items($product_attr_set);
                    //print_r($attributes);exit;
                    $attrcode_type = array_combine(array_column($attributes, 'code'), array_column($attributes, 'type'));
                    $attribute_header[$product_attr_set] = array_column($attributes, 'code');
                    $diff_header = array_diff(array_keys($product_data), $attribute_header[$product_attr_set]); //exit;
                    $merge = array_merge($diff_header, $attribute_header[$product_attr_set]);
                    $attribute_header[$product_attr_set] = $merge;
                }
                // $flip_merge = array_flip($merge);
                $t_merge = $attribute_header[$product_attr_set];
                $merge_data = array_fill_keys($t_merge, '');
                $mdata = array_merge($merge_data, $product_data);

//                $rv = array_filter($mdata,'is_array');
//                $mdata = array_diff($mdata,$rv);
                $comm = array_merge($mdata, $attrcode_type);
                $sele = array_filter($comm, function($var) {
                    return ($var && $var == 'select');
                });
                foreach ($sele as $k8 => &$v8) {
                    if ($k8 == 'gender') {
                        $v8 = $product->getResource()->getAttribute('gender')->setStoreId(1)->getSource()->getOptionText($product->getGender());
                        if (count($v8) > 1) {
                            $v8 = 'Unisex';
                        }
                    } else {
                        $v8 = $product->getAttributeText($k8);
                    }
                }
                $mdata = array_merge($mdata, $sele);
                $mdata['attribute_set_id'] = $attribute_set[$mdata['attribute_set_id']];
                $attribute_set_data[$product_attr_set][] = $mdata;
                //print_r($mdata);
                // exit;
            }
        }
        $attribute_nm = array_unique($attribute_nm);
        $attribute_nm = array_values($attribute_nm);

        require_once Mage::getBaseDir('lib') . "/PHPExcel/Classes/PHPExcel.php";
        $objPHPExcel = new PHPExcel();

        $activesheet = 0;
//        print_r(count($attribute_set_data));
//        print_r($attribute_set_data);exit;
        foreach ($attribute_header as $k4 => $v4) {
            if ($activesheet == 0) {
                $objPHPExcel->setActiveSheetIndex($activesheet);
                $objPHPExcel->getActiveSheet()->setTitle($attribute_nm[$activesheet]);
                $sheetindexforattribute[$k4] = $activesheet;
            }
            if ($activesheet > 0) {
                $objPHPExcel->createSheet();
                $sheet = $objPHPExcel->setActiveSheetIndex($activesheet);
                $sheet->setTitle("$attribute_nm[$activesheet]");
                $sheetindexforattribute[$k4] = $activesheet;
            }
            foreach ($v4 as $k3 => $v3) {
                $v3 = (!is_array($v3) ? $v3 : '');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($k3, 1, $v3);
                //$i++;
            }
            $activesheet++;
        }

        foreach ($attribute_set_data as $k6 => $v6) {
            $row = 2;
            foreach ($v6 as $k1 => $v1) {
                $col = 0;
                foreach ($v1 as $key => $value) {
                    $objPHPExcel->setActiveSheetIndex($sheetindexforattribute[$k6]);
                    $value_1 = trim(strip_tags($value));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value_1);
                    $col++;
                }
                $row++;
            }
        }

        // $objPHPExcel->setActiveSheetIndex(0);
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }

    public function deletelogAction() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_incominglog');
            $sql = 'DELETE FROM ' . $table . ' WHERE id=' . $id;
            $writeConnection->query($sql);
        }
        Mage::app()->getResponse()->setRedirect($_SERVER['HTTP_REFERER']);
    }

    public function reprocessAction() {
        Mage::helper('status')->status_code();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $id = $this->getRequest()->getParam('id');
        $sku = $this->getRequest()->getParam('sku');
        $channel_id = $this->getRequest()->getParam('channel_id');
        if ($sku && $channel_id) {
            $table = Mage::getSingleton('core/resource')->getTableName('amazon/incomingchannels_amazon');
            $sql = 'UPDATE ' . $table . ' SET is_processed= 0 WHERE sku= "' . $sku . '" AND bridge_channel_id = ' . $channel_id;
            $writeConnection->query($sql);

            $log_table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_incominglog');

            $updsql = 'UPDATE ' . $log_table . ' SET status= "Ready",status_code="' . ATTR_IMG . '" WHERE id=' . $id;
            $writeConnection->query($updsql);
        }
        Mage::app()->getResponse()->setRedirect($_SERVER['HTTP_REFERER']);
    }

    public function ajax_applyruleAction() {
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $read = Mage::getSingleton('core/resource')->getConnection('core_read');
        if ($this->getRequest()->getPost('channel_id')) {
            $channel_id = $this->getRequest()->getPost('channel_id');

            $table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_contentrule');
            //echo $table;exit;
            $sel_query = 'SELECT COUNT(*) FROM ' . $table . ' WHERE bridge_channel_id=' . $channel_id;
            $res = $read->fetchAll($sel_query);
            $content = array();
            $i = 0;
            if (!$res[0]['COUNT(*)']) {
                $_productCollection = Mage::getModel('catalog/product')->getCollection()
                                ->addAttributeToFilter('bridge_channel_id', $channel_id)->getData();
                $product_ids = array_column($_productCollection, 'entity_id');
                $product_skus = array_column($_productCollection, 'sku');
                $product_id_sku = array_combine($product_ids, $product_skus);
                foreach ($product_id_sku as $k => $v) {
                    $content[$i]['entity_id'] = $k;
                    $content[$i]['sku'] = $v;
                    $content[$i]['bridge_channel_id'] = $channel_id;
                    $content[$i]['is_processed'] = 0;
                    $content[$i]['created_at'] = date('Y-m-d H:i:s');
                    $content[$i]['updated_at'] = date('Y-m-d H:i:s');
                    $i++;
                }
                if (!empty($content)) {
                    $write->insertMultiple($table, $content);
                }
            }
        }
        echo 1;
        exit;
    }

    public function applycontentrule_cronAction() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_contentrule');
        $res = $readConnection->fetchAll('SELECT * FROM ' . $table . ' WHERE is_processed=0 LIMIT 1');
        $product_ids = array();
        if (!empty($res)) {
            $channel_id = $res[0]['bridge_channel_id'];
            if ($channel_id) {
                $model = Mage::getModel('bridge/incomingchannels');
                $model->load($channel_id);
                $contentsource = $model->getContentSource();
            }
            $product_id = $res[0]['entity_id'];
            $product = Mage::getModel('catalog/product')->load($product_id);
            $attributeId = Mage::getResourceModel('eav/entity_attribute')
                    ->getIdByCode('catalog_product', 'bridge_channel_id');
            $store_query = 'SELECT * FROM catalog_product_entity_int WHERE entity_id = ' . $product_id . ' AND attribute_id = ' . $attributeId;
            $store_res = $readConnection->fetchAll($store_query);
            //print_r($store_res);exit;
            foreach ($store_res as $k => $v) {
                $contentruledata = Mage::helper('bridge')->getConvertedString(array('id' => $channel_id), $product_id, 'inc', $v['store_id'], 1);
                //print_r($contentruledata);//exit;
                if (!empty($contentruledata)) {
                    $apply_on = unserialize($contentruledata['apply_on']);
                    $data_content = array();
                    $data_content['entity_id'] = $product_id;
                    $data_content['store_id'] = $v['store_id'];
                    $data = array();
                    foreach ($apply_on as $k => $v) {
                        if ($v == 'Own store') {
                            if (strcasecmp(@$contentsource, 'rule_base_original') === 0) {
                                if ($contentruledata['description'])
                                    $data['description'] = $contentruledata['description'];
                                if ($contentruledata['short_description'])
                                    $data['short_description'] = $contentruledata['short_description'];
                                if ($contentruledata['name'])
                                    $data['name'] = $contentruledata['name'];
                            } else if (strcasecmp(@$contentsource, 'inc_rule_base_original') === 0) {
                                $data['name'] = $contentruledata['name'] ? $contentruledata['name'] : $product->getIncName();
                                $data['description'] = $contentruledata['description'] ? $contentruledata['description'] : $product->getIncDescription();
                                $data['short_description'] = $contentruledata['short_description'] ? $contentruledata['short_description'] : $product->getIncShortDescription();
                            }
                        }
                        if ($v == 'Outgoing Channel') {
                            if ($contentruledata['name'])
                                $data['ogc_name'] = $contentruledata['name'];
                            if ($contentruledata['description'])
                                $data['ogc_description'] = $contentruledata['description'];
                            if ($contentruledata['short_description'])
                                $data['ogc_short_description'] = $contentruledata['short_description'];
                        }
                    }
                    if (!empty($data)) {
                        $data_set = array_merge($data_content, $data);
                        $product->setData($data_set);
                        $product->save();
                        $sql_upd = 'UPDATE ' . $table . ' SET is_processed = 1 WHERE id=' . $res[0]['id'];
                        $writeConnection->query($sql_upd);
                    }
                }
            }
        } else {
            //flush the table
            $del = 'TRUNCATE TABLE ' . $table;
            $writeConnection->query($del);
            $cronurl = 'wget -o /dev/null -q -O -';
            $endcronurl = '> /dev/null';
            $contentrule_link = Mage::getBaseUrl() . "admin/incomingchannels/applycontentrule_cron";
            $contentrule_link_str = '*/2 * * * * ' . $cronurl . ' ' . $contentrule_link . ' ' . $endcronurl;
            Mage::helper('cron')->removecron($contentrule_link_str);
        }
        echo 'done';
        exit;
    }

    public function ajax_inlinenamesaveAction() {
        if ($this->getRequest()->getPost('entity_id')) {
            $id = $this->getRequest()->getPost('entity_id');
            $name = $this->getRequest()->getPost('name');
            $product = Mage::getModel('catalog/product')->load($id);
            $product->setName($name);
            $product->save();
        }
    }

    /**
     * Export order grid to CSV format
     */
    public function exportLogCsvAction() {
        $fileName = 'log.csv';
        $grid = $this->getLayout()->createBlock('bridge/adminhtml_incomingchannels_edit_tab_notification_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportLogExcelAction() {
        $fileName = 'log.xml';
        $grid = $this->getLayout()->createBlock('bridge/adminhtml_incomingchannels_edit_tab_notification_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

}