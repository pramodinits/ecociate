<?php

class Fedobe_Bridge_Adminhtml_OutgoingchannelsController extends Mage_Adminhtml_Controller_Action {

    public function _initAction() {
        $this->loadLayout()->_setActiveMenu('fedobe/bridge/outgoing');
        $this->_title(Mage::helper('bridge')->__('Fedobe Extension'));
    }

    public function indexAction() {
        $this->_initAction();
        $this->_title(Mage::helper('bridge')->__('Manage Outgoing Channels'));
//        $this->loadLayout();
        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction() {
        $this->_initAction();
        Mage::getSingleton('core/session')->unsetData('channel_condition_skus');
        $id = $this->getRequest()->getParam('id');
        $otgchannel = Mage::getModel('bridge/outgoingchannels');
        $consumer_model = Mage::getModel('oauth/consumer');
        $session_security_salt = $id ? "{$id}_outgoing_security_salt" : "outgoing_security_salt";
        Mage::getSingleton('core/session')->unsetData("$session_security_salt");
        if (@$id) {
            $otgchannel->load($id);
            if (!$otgchannel->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('bridge')->__('This Channel  no longer exists.')
                );
                $this->_redirect('*/*');
                return;
            }
            if ($otgchannel->getConsumerId()) {
                $consumer_model->load($otgchannel->getConsumerId());
                $otgchannel->setKey($consumer_model->getKey());
                $otgchannel->setSecret($consumer_model->getSecret());
                $otgchannel->setName($consumer_model->getName());
            }

            if ($otgchannel->getProductStatus()) {
                $otgchannel->setBridge(array('product_status' => $otgchannel->getProductStatus()));
            }
            //set the security salt for this outgoing channel if paid module is installed
            if ($consumer_model->getId() && !$otgchannel->getSalt()) {
                Mage::getSingleton('core/session')->unsetData($session_security_salt);
                if (Mage::helper('core')->isModuleEnabled('Fedobe_Securitycheck')) {
                    $security_salt = Mage::getModel('securitycheck/salt')->loadByOutgoingChannel($otgchannel->getId())->getSalt();
                    Mage::getSingleton('core/session')->setData("$session_security_salt", $security_salt);
                }
            }
        }
        if (Mage::getmodel('bridge/outgoingchannels')->isAdminstrator(Mage::getSingleton('admin/Session')->getUser()->getUserId())) {
            if ($otgchannel->getAdminUserId()) {
                $user_data = Mage::getModel("admin/user")->load($otgchannel->getAdminUserId());
                $otgchannel->setUsername($user_data->getUsername());
                $otgchannel->setFirstname($user_data->getFirstname());
                $otgchannel->setLastname($user_data->getLastname());
                $otgchannel->setEmail($user_data->getEmail());
            }
        }

        $this->_title($otgchannel->getId() ? $this->__('Edit Outgoingchannel') : $this->__('New Outgoingchannel'));
// set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getChannelData(true);
        if (!empty($data)) {
            $otgchannel->setData($data);
        }
        if ($otgchannel->getId()) {
            Mage::getSingleton('adminhtml/session')->setData("{$id}_listed_sku", explode(',', $otgchannel->getSkuList()));
        }

        if (!@$otgchannel['secret'] && ($this->getRequest()->getParam('channel_type') == 'magento' || $otgchannel['channel_type'] == 'magento')) {
            $formData = $this->_getFormData();
            if ($formData) {
                $this->_setFormData($formData);
                $otgchannel->setKey($formData['key']);
                $otgchannel->setSecret($formData['secret']);
            } else {
                /** @var $helper Mage_Oauth_Helper_Data */
                $helper = Mage::helper('oauth');
                $otgchannel->setKey($helper->generateConsumerKey());
                $otgchannel->setSecret($helper->generateConsumerSecret());
                $this->_setFormData($otgchannel->getData());
            }
        }
//        Mage::register('current_consumer', $model);
        $default_settings = Mage::helper('bridge')->getDefaultSettings();
        Mage::register('default_settings', $default_settings);

        Mage::register('channel_condition', $otgchannel);
        $this->renderLayout();
    }

    /**
     * Coupon codes grid
     */
    public function conditionsGridAction() {
        echo $this->getLayout()->createBlock('fedobe_bridge_block_adminhtml_outgoingchannels_edit_tab_conditions_grid')->setSkuList($this->getRequest()->getPost('pids', null))->toHtml();
    }

    public function deleteAction() {
        $response = Mage::getModel('bridge/outgoingchannels')->delete($this->getRequest()->getParam('id'));
        if ($response['status'] == 'error')
            Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('bridge')->__($response['msg'])
            );
        else
            Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('bridge')->__($response['msg'])
            );
        $this->_redirect('*/*/');
    }

    public function saveAction() {
        $id = $this->getRequest()->getParam('id');
        if (!$this->_validateFormKey()) {
            if ($id) {
                $this->_redirect('*/*/edit', array('id' => $id));
            } else {
                $this->_redirect('*/*/new', array('id' => $id));
            }
            return;
        }

        if ($this->getRequest()->getPost()) {
            //save outgoing channel mapping data
            $session_security_salt = $id ? "{$id}_outgoing_security_salt" : "outgoing_security_salt";
            $data = $this->getRequest()->getPost();
            $otgchannel = Mage::getModel('bridge/outgoingchannels');
            $otgchannel->load($id);
            $defaul_settings = Mage::helper('bridge')->getDefaultSettings();
            $allow_condition = false;
            $is_retailer = Mage::getmodel('bridge/outgoingchannels')->isRetailer(Mage::getSingleton('admin/Session')->getUser()->getUserId());
            $is_adminstrator = Mage::getmodel('bridge/outgoingchannels')->isAdminstrator(Mage::getSingleton('admin/Session')->getUser()->getUserId());
            if ($is_adminstrator || (!$id && $defaul_settings['ogc_allow_conditiontab'] == "allow_editing") || $otgchannel->getAllowCondition() == "allow_editing")
                $allow_condition = true;

            $data['name'] = @$data['consumer_name'];

            if (@$data['consumer_name']) {
                $consumer_model = Mage::getModel('oauth/consumer');
                $consumer_model->load($otgchannel->getConsumerId());
                //save the user data if loged in user is admin
                if ($is_adminstrator) {
                    //if user exist then update
                    $user_model = Mage::getModel('Admin/User')->load($this->getRequest()->getPost('admin_user_id'));
                    $user_data = array('username' => $data['username'], 'firstname' => $data['firstname'], 'lastname' => $data['lastname'], 'email' => $data['email'], 'user_id' => $user_model->getId());
                    if (isset($data['password']))
                        $user_data['password'] = $data['password'];
                    else if (isset($data['new_password']))
                        $user_data['new_password'] = $data['new_password'];
                    $user_data['password_confirmation'] = $data['password_confirmation'];
                    $user_model->setData($user_data);
                    /*
                     * Unsetting new password and password confirmation if they are blank
                     */
                    if ($user_model->hasNewPassword() && $user_model->getNewPassword() === '') {
                        $user_model->unsNewPassword();
                    }
                    if ($user_model->hasPasswordConfirmation() && $user_model->getPasswordConfirmation() === '') {
                        $user_model->unsPasswordConfirmation();
                    }

                    $user_validation = $user_model->validate();
                    if (is_array($user_validation)) {
                        Mage::getSingleton('adminhtml/session')->setChannelData($data);
                        foreach ($user_validation as $message) {
                            Mage::getSingleton('adminhtml/session')->addError($message);
                        }
                        $this->_redirect('*/*/edit', array('_current' => true));
                        return $this;
                    }
                    try {

                        //if user is not created before then create the user and assign bridge user role and  bridge api api role
                        if (!$user_model->getId()) {
                            $api2_role_id = Mage::getmodel('bridge/outgoingchannels')->getRestRoleId();
                            $user_model->setApi2Roles(array($api2_role_id));
                            $user_model->save();
                            $user_id = $user_model->getId();
                            $role_id = Mage::getModel('Admin/Role')->getCollection()->addFieldToFilter('role_name', 'Bridge User')->addFieldToSelect('role_id')->getFirstItem()->getData();
                            $user_model->setRoleIds($role_id)
                                    ->setRoleUserId($user_model->getUserId())
                                    ->saveRelations();
                        } else {
                            //otherwise update the user
                            $user_model->save();
                        }
                    } catch (Mage_Core_Exception $e) {

                        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                        Mage::getSingleton('adminhtml/session')->setChannelData($data);
                        $this->_redirect('*/*/edit', array('id' => $otgchannel->getUserId()));
                        return;
                    }
                    $data['admin_user_id'] = $user_model->getId();
                }
                //save
                //save api inforamtion
                if (!$id) {
                    $dataForm = $this->_getFormData();
                    if ($dataForm) {
                        $data['key'] = $dataForm['key'];
                        $data['secret'] = $dataForm['secret'];
                    } else {
                        // If an admin was started create a new consumer and at this moment he has been edited an existing
                        // consumer, we save the new consumer with a new key-secret pair
                        /** @var $helper Mage_Oauth_Helper_Data */
                        $helper = Mage::helper('oauth');

                        $data['key'] = $helper->generateConsumerKey();
                        $data['secret'] = $helper->generateConsumerSecret();
                    }
                }
                //save oauth consumer data
                $consumer_model->addData($data);
                $consumer_model->save();
                $data['consumer_id'] = $consumer_model->getId();
            }

////Here let add the rules data
            if ($is_adminstrator) {
                $data['relative_product'] = @array_sum($data['relative_product']);
                $data['others'] = isset($data['others']) ? serialize($data['others']) : '';
            }
            $product_ids = '';
            if ($allow_condition) {
                if ($selectedids = $this->getRequest()->getPost('product_id')) {
                    $product_ids = Mage::helper('adminhtml/js')->decodeGridSerializedInput($selectedids);
                    $product_ids = implode(',', array_keys($product_ids));
                }

                $data['sku_list'] = $product_ids;
                $condition_rules = @$data['rule'];
                $arr = array();
                foreach ($condition_rules['conditions'] as $id => $cdata) {
                    $path = explode('--', $id);
                    $node = & $arr;
                    for ($i = 0, $l = sizeof($path); $i < $l; $i++) {
                        if (!isset($node['conditions'][$path[$i]])) {
                            $node['conditions'][$path[$i]] = array();
                        }
                        $node = & $node['conditions'][$path[$i]];
                    }
                    foreach ($cdata as $k => $v) {
//check for gender attribute array issue
                        if (is_array($v)) {
                            $v = implode(',', $v);
                        }
                        $node[$k] = $v;
                    }
                }
                $data['conditions_serialized'] = serialize($arr['conditions'][1]);
                $data['product_status'] = @$data['bridge']['product_status'];
            } else {
                unset($data['sku_list']);
            }
            unset($data['rule']);
            try {
                if (!@$data['admin_user_id'])
                    $data['admin_user_id'] = Mage::getSingleton('admin/Session')->getUser()->getUserId();
                //save security salt if paid module exist
                if (Mage::getSingleton('core/session')->getData($session_security_salt) && Mage::helper('core')->isModuleEnabled('Fedobe_Securitycheck')) {
                    $data['salt'] = "";
                }


//                if (@$data['admin_user_id']) {
//                    if (@$data['consumer_name']) {
//                        $otgchannel->loadByConsumerId($oauthmodel->getId());
//                        $data['consumer_id'] = $oauthmodel->getId();
//                    } else {
//                        $otgchannel->load($id);
//                    }
//                    $data['id'] = $otgchannel->getId();
//                } else {
//                    $data['id'] = $this->getRequest()->getParam('id');
//                }
                //if retiler logged in and creating a new channel then set the dafaultsettings
                if ($is_retailer && !$otgchannel->getId()) {
                    $data['allow_condition'] = $defaul_settings['ogc_allow_conditiontab'];
                    $data['product_info'] = $defaul_settings['ogc_sync_pim'];
                    $data['relative_product'][] = $defaul_settings['ogc_sync_related'] ? 1 : 0;
                    $data['relative_product'][] = $defaul_settings['ogc_sync_upsell'] ? 2 : 0;
                    $data['relative_product'][] = $defaul_settings['ogc_sync_cross_sell'] ? 4 : 0;
                    $data['content_source'] = $defaul_settings['ogc_content_source'];
                    $data['create_new'] = $defaul_settings['ogc_create_new'];
                    $data['cond_apply_on_relative_products'] = $defaul_settings['ogc_apply_cond_on_related_product'];
                    $data['filter_by'] = $defaul_settings['ogc_filter_by'];
                    $data['product_pricenquantity'] = $defaul_settings['ogc_sync_pricenquantity'];
                    $data['sync_order'] = $defaul_settings['ogc_sync_order'];
                    $data['price_source'] = $defaul_settings['ogc_price_source'];
                    $data['relative_product'] = array_sum($data['relative_product']);
                    $data['multiprice_rule'] = $data['ogc_multiprice_rule'];
                    $data['update_exist'] = $data['ogc_update_exist'];
                }
                $prev_condition = $otgchannel->getConditionsSerialized();
                $otgchannel->setData($data);
                $otgchannel->save();

                //update edi cron table for update convert price

                if (!$data['id']) {
                    $edicrondata['channel_id'] = $otgchannel->getId();
                    $edicrondata['channel_type'] = "outgoing"; //$data['channel_type'];
                    $edicrondata['status'] = $data['channel_status'];
                    $cronmodel = Mage::getModel('bridge/bridgeapi_edichannelcron')->setData($edicrondata)->save();
                } else {
                    $edicrondata = array("status" => $data['channel_status']);
                    if (($data['filter_by'] == 'condition' && unserialize($prev_condition) !== $arr['conditions'][1]) || ($otgchannel->getFilterBy() != $data['filter_by']) || ($data['filter_by'] == 'sku' && array_filter(explode(',', $data['sku_list'])) != array_filter(explode(',', $otgchannel->getSkuList())))) {
                        $edicrondata['condition_changed'] = 1;
                    }
                    $resource = Mage::getSingleton('core/resource');
                    $write = $resource->getConnection('core_write');
                    $where = "channel_id = {$otgchannel->getId()} AND channel_type ='outgoing'";
                    $write->update("bridge_edi_channelcron", $edicrondata, $where);
                }
                //end
                //save security salt if paid module exist
                if (@$data['consumer_name']) {
                    if (Mage::helper('core')->isModuleEnabled('Fedobe_Securitycheck')) {
                        $security_salt_model = Mage::getModel('securitycheck/salt')->loadByOutgoingChannel($otgchannel->getId());
                        if (Mage::getSingleton('core/session')->getData($session_security_salt)) {
                            $security_salt_model->addData(array('id' => $security_salt_model->getId(), 'salt' => Mage::getSingleton('core/session')->getData($session_security_salt), 'outgoingchannel_id' => $otgchannel->getId(), 'updated' => date('Y-m-d H:i:s')));
                            $security_salt_model->save();
                        } else {
                            $security_salt_model->delete();
                        }
                    }
                }
                //end
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('bridge')->__('The Channel has been saved.')
                );
                Mage::getSingleton('adminhtml/session')->setChannelData(false);
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->setChannelData($data);
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/');
                return;
            }
        }
// The following line decides if it is a "save" or "save and continue"
        if ($this->getRequest()->getParam('back')) {
            $this->_redirect('*/*/edit', array('id' => $otgchannel->getId()));
        } else {
            $this->_redirect('*/*/');
        }
    }

    public function generateAction() {
        if (!$this->getRequest()->isAjax()) {
            $this->_forward('noRoute');
            return;
        }
        if ($this->getRequest()->getPost()) {
            $data = $this->getRequest()->getPost();
//Here let add the rules data
            $condition_rules = $data['rule'];
            foreach ($condition_rules['conditions'] as $id => $cdata) {
                $path = explode('--', $id);
                $node = & $arr;
                for ($i = 0, $l = sizeof($path); $i < $l; $i++) {
                    if (!isset($node['conditions'][$path[$i]])) {
                        $node['conditions'][$path[$i]] = array();
                    }
                    $node = & $node['conditions'][$path[$i]];
                }
                foreach ($cdata as $k => $v) {
//check for gender attribute array issue
                    if (is_array($v)) {
                        $v = implode(',', $v);
                    }
                    $node[$k] = $v;
                }
            }
            $conditions_serialized = serialize($arr['conditions'][1]);
        }
        if ($this->getRequest()->getParam('id'))
            $skus = Mage::getmodel('bridge/rule')->getMatchingProductIds($conditions_serialized);
        else {
            $otgchannel = Mage::getModel('bridge/outgoingchannels');
            $otgchannel->setConditionsSerialized($conditions_serialized);
            Mage::register('channel_condition', $otgchannel);
            $skus = Mage::getmodel('bridge/rule')->getMatchingProductIds();
        }
        $product_status = @$data['bridge']['product_status'];
        if ($product_status) {
            $skus = Mage::getmodel('bridge/outgoingchannels')->getFilteredSkus($skus, $product_status);
        }
        Mage::getSingleton('core/session')->setData('channel_condition_skus', $skus);
    }

    /**
     * Get form data
     *
     * @return array
     */
    protected function _getFormData() {
        return $this->_getSession()->getData('consumer_data', true);
    }

    /**
     * Set form data
     *
     * @param $data
     * @return Mage_Oauth_Adminhtml_Oauth_ConsumerController
     */
    protected function _setFormData($data) {
        $this->_getSession()->setData('consumer_data', $data);
        return $this;
    }

    /**
     * return user information by ajax call
     */
    public function userinfoAction() {
        $user_id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('admin/user')->load($user_id)->getData();
        print_r(json_encode($model));
        exit;
    }

    /**
     * check url is exist?
     */
    public function urlunique_validationAction() {
        $exist = Mage::getmodel('bridge/outgoingchannels')->checkUniqueUrl($this->getRequest()->getParam('id'), $this->getRequest()->getParam('url'));
        echo $exist;
        exit;
    }

    /**
     * check unique user
     */
    public function uniqueuser_validationAction() {
        $user_model = Mage::getModel('Admin/User');
        $user_model->setData($this->getRequest()->getParams());
        echo $user_model->userExists();
        exit;
    }

    public function ajax_addruleAction() {
        if ($this->getRequest()->getPost()) {
            $serdata = $this->getRequest()->getPost();
            parse_str($serdata['rule'], $unserdata);
            $data = $unserdata;
            $data['channel_id'] = $serdata['brand_id'];
            $ogc_controller = $serdata['ogc_controller'];
            $pim_cron = $serdata['pim_cron'];
            $data['conditions_html'] = $serdata['conditions_html'];
            unset($data['rule']);
            $arr = array();
            $unserdata['rule']['actions'][1] = $unserdata['rule']['conditions'][1];
            foreach ($unserdata['rule']['actions'] as $id => $cdata) {
                $path = explode('--', $id);
                $node = & $arr;
                for ($i = 0, $l = sizeof($path); $i < $l; $i++) {
                    if (!isset($node['conditions'][$path[$i]])) {
                        $node['conditions'][$path[$i]] = array();
                    }
                    $node = & $node['conditions'][$path[$i]];
                }
                foreach ($cdata as $k => $v) {
//check for gender attribute array issue
                    if (is_array($v)) {
                        $v = implode(',', $v);
                    }
                    $node[$k] = $v;
                }
            }
            $data['conditions_serialized'] = serialize($arr['conditions'][1]);
            $data['created'] = date('Y-m-d H:i:s');
            $model = Mage::getModel('bridge/outgoingchannels_action');
            $model->setData($data);
            $model->save();
            Mage::helper('bridge')->change_edicron_flag('is_processed', 0, $data['channel_id'], 'outgoing');
            if($ogc_controller=='bridgecsv'){
                 if($pim_cron=='0 * * * *')//hourly
                    $hr = 1;
                 else if($pim_cron=='0 0 * * *')//daily
                    $hr = 24;
                 else if($pim_cron=='0 0 * * 0')//weekly
                    $hr = 168;
                 else if($pim_cron=='0 0 1 * *')//monthly
                    $hr = 720;
                 $resource = Mage::getSingleton('core/resource');
                 $readConnection = $resource->getConnection('core_read');
                 $writeConnection = $resource->getConnection("core_write");
                 $ogctable = Mage::getSingleton('core/resource')->getTableName('bridgecsv/csvogcdata');
                 $writeConnection->query('UPDATE ' . $ogctable . ' SET is_condition_changed=1,updated_at = DATE_SUB(updated_at, INTERVAL '.$hr.' HOUR), is_processed=0 WHERE bridge_channel_id=' . $data['channel_id']);
                 //Mage::helper('bridgecsv')->addcsvcron($data['id']);
            }
            echo $model->getId();
            exit;
        }
    }

    public function ajax_deleteruleAction() {
        if ($this->getRequest()->getPost('id')) {
            $ogc_controller = $this->getRequest()->getPost('ogc_controller');
            $pim_cron = $this->getRequest()->getPost('pim_cron');// echo $ogc_channel;exit;
            $model = Mage::getModel('bridge/outgoingchannels_action');
            $model->load($this->getRequest()->getPost('id'));
            $model->delete();
            Mage::helper('bridge')->change_edicron_flag('is_processed', 0, $model->getChannelId(), 'outgoing');
            if($ogc_controller=='bridgecsv'){
                if($pim_cron=='0 * * * *')//hourly
                    $hr = 1;
                 else if($pim_cron=='0 0 * * *')//daily
                    $hr = 24;
                 else if($pim_cron=='0 0 * * 0')//weekly
                    $hr = 168;
                 else if($pim_cron=='0 0 1 * *')//monthly
                    $hr = 720;
                 $resource = Mage::getSingleton('core/resource');
                 $readConnection = $resource->getConnection('core_read');
                 $writeConnection = $resource->getConnection("core_write");
                 $ogctable = Mage::getSingleton('core/resource')->getTableName('bridgecsv/csvogcdata');
                 $writeConnection->query('UPDATE ' . $ogctable . ' SET is_condition_changed=1,updated_at = DATE_SUB(updated_at, INTERVAL '.$hr.' HOUR), is_processed=0 WHERE bridge_channel_id=' . $this->getRequest()->getPost('channel_id'));
             }
            exit;
        }
    }

    public function checkaddonAction() {
        $channel_type = $this->getRequest()->getParam('channel_type');
        if ($channel_type != 'magento') {
            $module = 'Fedobe_' . ucfirst($channel_type);
            $addon_enabled = Mage::helper('core')->isModuleEnabled($module);
            if ($addon_enabled) {
                $this->_redirect('adminhtml/' . $channel_type . '/edit/channel_type/' . $channel_type);
            } else {
                //Here to check the decsription of the corresponding sales description
                Mage::register('addon', $channel_type);
                $this->loadLayout();
                $this->renderLayout();
            }
        } else {
            $this->_redirect('*/*/edit/channel_type/magento');
        }
    }

    public function exportCsvAction($product) {
        ob_start();
        // echo "<pre>";
        $fileName = 'Ogc.csv';
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $grid = $this->getLayout()->createBlock('fedobe_bridge_block_adminhtml_outgoingchannels_edit_tab_conditions_grid');
        $grid->_prepareGrid();
        $collection = $grid->getCollection();
        $limitcount = $collection->getSize();
        //echo $limitcount;exit;
        $select = $collection->getSelect()->limit($limitcount);
        $query = $readConnection->fetchAll($select);
        $attribute_set = array();
        $attribute_header = array();

        foreach ($query as $product_1) {
            $product = Mage::getModel('catalog/product')->load($product_1['entity_id']);
            $product_data = $product->getData();
            $stock_data = $product->getStockItem()->getData();
            $product_data['qty'] = $stock_data['qty'];
            $product_data['is_in_stock'] = $stock_data['is_in_stock'];
            unset($product_data['stock_item']);
            $product_attr_set = $product_data['attribute_set_id'];
            if (!in_array($product_attr_set, array_keys($attribute_set))) {
                $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
                $attributeSetModel->load($product_attr_set);
                $attributeSetName = $attributeSetModel->getAttributeSetName();
                $attribute_set[$product_attr_set] = $attributeSetName;
                $attribute_nm[] = $attributeSetName;
                if (!isset($attribute_header[$product_attr_set])) {
                    $i = 0;
                    $attributes = Mage::getModel('catalog/product_attribute_api')->items($product_attr_set);
                    $attrcode_type = array_combine(array_column($attributes, 'code'), array_column($attributes, 'type'));
                    $arr_colmn = array_column($attributes, 'code');
                    $diff_header = array_diff(array_keys($product_data), $arr_colmn); //exit;
                    $merge = array_merge($diff_header, $arr_colmn);
                    $attribute_header[$product_attr_set][] = $merge;
                    $str[$product_attr_set][$i] = implode(",", $merge) . "\n";
                    //$str.="\n";
                    $i++;
                }
            }
            $t_merge = $attribute_header[$product_attr_set][0];
            $merge_data = array_fill_keys($t_merge, '');
            $mdata = array_merge($merge_data, $product_data);


            //to find out those attributes having type select
            $comm = array_merge($mdata, $attrcode_type);
            $sele = array_filter($comm, function($var) {
                return ($var && $var == 'select');
            });
            foreach ($sele as $k8 => &$v8) {
                $v8 = $product->getAttributeText($k8);
            }
            $mdata = array_merge($mdata, $sele);
            $mdata = array_map(function($v) {
                $v = preg_replace("/\r|\n/", "", $v); //trim(strip_tags($v));
                $v = trim(strip_tags($v));
                $v = str_replace("\n", "", $v);
                $v = str_replace(",", "", $v);
                if (is_array($v))
                    $v = '';
                return $v;
            }, $mdata);
            //print_r($mdata);
            $mdata['attribute_set_id'] = $attribute_set[$mdata['attribute_set_id']];
            $str[$product_attr_set][$i] = implode(",", $mdata) . "\n";
            //$str.="\n";
            // $attribute_header[$product_attr_set][$i] = $mdata;
            $i++;
        }//exit;
        foreach ($str as $v) {
            foreach ($v as $k1 => $v1) {
                echo $v1;
            }
        }
        //echo $str; 
        header("Content-type: application/csv");
        header("Content-Disposition: attachment;filename=" . $fileName);
        exit;
    }

    /**
     *  Export edi grid to Excel XML format
     */
    public function exportExcelAction() {
        ob_start();
        // echo "<pre>";
        $fileName = 'Ogc.xls';
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $grid = $this->getLayout()->createBlock('fedobe_bridge_block_adminhtml_outgoingchannels_edit_tab_conditions_grid');
        $grid->_prepareGrid();
        $collection = $grid->getCollection();
        $limitcount = $collection->getSize();
        //echo $limitcount;exit;
        $select = $collection->getSelect()->limit($limitcount);
        $query = $readConnection->fetchAll($select);
        $attribute_set = array();
        $attribute_header = array();
        //print_r(count($collection->getData()));
        //print_r($collection->getData());exit;

        foreach ($query as $product_1) {
            $product = Mage::getModel('catalog/product')->load($product_1['entity_id']);
            $product_data = $product->getData(); //print_r($product_data);exit;
            $stock_data = $product->getStockItem()->getData();
            $product_data['qty'] = $stock_data['qty'];
            $product_data['is_in_stock'] = $stock_data['is_in_stock'];
            unset($product_data['stock_item']);
            $product_attr_set = $product_data['attribute_set_id'];
            // echo $product_attr_set."<br>";
            if (!in_array($product_attr_set, array_keys($attribute_set))) {
                $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
                $attributeSetModel->load($product_attr_set);
                $attributeSetName = $attributeSetModel->getAttributeSetName();
                $attribute_set[$product_attr_set] = $attributeSetName;
                $attribute_nm[] = $attributeSetName;
                if (!isset($attribute_header[$product_attr_set])) {
                    $attributes = Mage::getModel('catalog/product_attribute_api')->items($product_attr_set);
                    //print_r($attributes);exit;
                    $attrcode_type = array_combine(array_column($attributes, 'code'), array_column($attributes, 'type'));
                    $attribute_header[$product_attr_set] = array_column($attributes, 'code');
                    $diff_header = array_diff(array_keys($product_data), $attribute_header[$product_attr_set]); //exit;
                    $merge = array_merge($diff_header, $attribute_header[$product_attr_set]);
                    $attribute_header[$product_attr_set] = $merge;
                }
            }
            // $flip_merge = array_flip($merge);
            $t_merge = $attribute_header[$product_attr_set];
            $merge_data = array_fill_keys($t_merge, '');
            $mdata = array_merge($merge_data, $product_data);

            $comm = array_merge($mdata, $attrcode_type); //print_r($comm);exit;
            $sele = array_filter($comm, function($var) {
                return ($var && in_array($var, array('select', 'multiselect')));
            });

            foreach ($sele as $k8 => &$v8) {
                if ($k8 == 'gender') {
                    $v8 = $product->getResource()->getAttribute('gender')->setStoreId(1)->getSource()->getOptionText($product->getGender());
                    if (count($v8) > 1) {
                        $v8 = 'Unisex';
                    }
                } else {
                    $v8 = $product->getAttributeText($k8);
                }
            }
            $mdata = array_merge($mdata, $sele); //print_r($mdata);
            $mdata['attribute_set_id'] = $attribute_set[$mdata['attribute_set_id']];
            $attribute_set_data[$product_attr_set][] = $mdata;
        }
        //exit;
        $attribute_nm = array_unique($attribute_nm);
        $attribute_nm = array_values($attribute_nm);

        require_once Mage::getBaseDir('lib') . "/PHPExcel/Classes/PHPExcel.php";
        $objPHPExcel = new PHPExcel();

        $activesheet = 0;
//        print_r(count($attribute_set_data));
        //print_r($attribute_set_data);exit;
        foreach ($attribute_header as $k4 => $v4) {
            if ($activesheet == 0) {
                $objPHPExcel->setActiveSheetIndex($activesheet);
                $objPHPExcel->getActiveSheet()->setTitle($attribute_nm[$activesheet]);
                $sheetindexforattribute[$k4] = $activesheet;
            }
            if ($activesheet > 0) {
                $objPHPExcel->createSheet();
                $sheet = $objPHPExcel->setActiveSheetIndex($activesheet);
                $sheet->setTitle("$attribute_nm[$activesheet]");
                $sheetindexforattribute[$k4] = $activesheet;
            }
            foreach ($v4 as $k3 => $v3) {
                $v3 = (!is_array($v3) ? $v3 : '');
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($k3, 1, $v3);
            }
            $activesheet++;
        }
        // print_r($attribute_set_data);exit;
        foreach ($attribute_set_data as $k6 => $v6) {
            $row = 2;
            foreach ($v6 as $k1 => $v1) {
                $col = 0;
                foreach ($v1 as $key => $value) {
                    $objPHPExcel->setActiveSheetIndex($sheetindexforattribute[$k6]);
                    $value_1 = trim(strip_tags($value));
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value_1);
                    $col++;
                }
                $row++;
            }
        }

        // $objPHPExcel->setActiveSheetIndex(0);
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        $objWriter->save('php://output');
        exit;
    }

}