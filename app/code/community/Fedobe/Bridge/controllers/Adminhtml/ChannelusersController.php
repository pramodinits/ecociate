<?php

class Fedobe_Bridge_Adminhtml_ChannelusersController extends Mage_Adminhtml_Controller_Action {

    public function _initAction() {
        $this->loadLayout()->_setActiveMenu('fesdobe/bridge');
        $this->_title(Mage::helper('bridge')->__('Fedobe Extension'));
    }

    public function indexAction() {
        $this->_initAction();
        $this->_title(Mage::helper('bridge')->__('Manage Channel Users'));
        $this->loadLayout();
        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction() {
        $this->_initAction();
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('admin/user');
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('bridge')->__('This Retailer no longer exists.')
                );
                $this->_redirect('*/*');
                return;
            }
        }

        $this->_title($model->getId() ? $model->getUsername() : $this->__('New Channel Retailer'));
        // Restore previously entered form data from session
        $data = Mage::getSingleton('adminhtml/session')->getUserData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        Mage::register('channeluser_data', $model);
        $this->loadLayout();
        $this->renderLayout();
    }

    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {
            $id = $this->getRequest()->getParam('user_id');
            $model = Mage::getModel('admin/user')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This Retailer no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }


            $model->setData($data);

            /*
             * Unsetting new password and password confirmation if they are blank
             */
            if ($model->hasNewPassword() && $model->getNewPassword() === '') {
                $model->unsNewPassword();
            }
            if ($model->hasPasswordConfirmation() && $model->getPasswordConfirmation() === '') {
                $model->unsPasswordConfirmation();
            }
            $result = $model->validate();
            try {
                if (!$id) {
                    $api2_role_id = Mage::getmodel('bridge/outgoingchannels')->getRestRoleId();
                    $model->setApi2Roles(array($api2_role_id));
                    $model->save();
                    $role_id = Mage::getModel('Admin/Role')->getCollection()->addFieldToFilter('role_name', 'Bridge User')->addFieldToSelect('role_id')->getFirstItem()->getId();
                    $model->setRoleIds(array($role_id))
                            ->setRoleUserId($model->getUserId())
                            ->saveRelations();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The Channel user has been saved.'));
                Mage::getSingleton('adminhtml/session')->setUserData(false);
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setUserData($data);
                $this->_redirect('*/*/edit', array('id' => $model->getUserId()));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction() {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('bridge/incomingchannels');
                $model->load($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('bridge')->__('The Incomingchannel has been deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                        Mage::helper('bridge')->__('An error occurred while deleting the Incomingchannel. Please review the log and try again.')
                );
                Mage::logException($e);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('bridge')->__('Unable to find a incomingchannel to delete.')
        );
        $this->_redirect('*/*/');
    }

    public function ajax_addruleAction() {
        if ($this->getRequest()->getPost()) {
            $data = $this->getRequest()->getPost();
            $data['created'] = date('Y-m-d H:i:s');
            $model = Mage::getModel('bridge/incomingchannels_action');
            unset($data['form_key']);
            $model->setData($data);
            $model->save();
            echo $model->getId();
            exit;
        }
    }

    public function ajax_deleteruleAction() {
        if ($this->getRequest()->getPost('id')) {
            $model = Mage::getModel('bridge/incomingchannels_action');
            $model->load($this->getRequest()->getPost('id'));
            $model->delete();
            exit;
        }
    }

}
