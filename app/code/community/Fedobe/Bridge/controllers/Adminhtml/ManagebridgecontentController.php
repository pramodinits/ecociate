<?php

class Fedobe_Bridge_Adminhtml_ManagebridgecontentController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function saveAction() {
        $id = $this->getRequest()->getParam('product');
        $store_id = $this->getRequest()->getParam('store')?$this->getRequest()->getParam('store'):Mage_Core_Model_App::ADMIN_STORE_ID; 
        $data = $this->getRequest()->getPost();
        if ($id && $data) {
            /** @var $session Mage_Admin_Model_Session */
            $session = Mage::getSingleton('adminhtml/session');
            $redirectBack = $this->getRequest()->getParam('back', false);
            Mage::app()->setCurrentStore($store_id);
            $product = Mage::getModel('catalog/product')->load($id);
            //validate Product

            if (!$product->getId()) {
                $session->addError(Mage::helper('catalog')->__('The Product no longer exists'));
            }
            //validate Own Store data as these values cannot be
            if (!isset($data['bridgename']['own']) || !isset($data['bridgeshortdescription']['own']) || !isset($data['bridgedescription']['own'])) {
                $session->addError(Mage::helper('catalog')->__('Product cannot be saved'));
            }
            $entityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
            //Here let's set the product data
            $datatobesaved = array(
                'name' => $data['bridgename']['own'],
                'short_description' => $data['bridgeshortdescription']['own'],
                'description' => $data['bridgedescription']['own'],
                'inc_name' => $data['bridgename']['incoming'],
                'inc_short_description' => $data['bridgeshortdescription']['incoming'],
                'inc_description' => $data['bridgedescription']['incoming'],
                'ogc_name' => $data['bridgename']['outgoing'],
                'ogc_short_description' => $data['bridgeshortdescription']['outgoing'],
                'ogc_description' => $data['bridgedescription']['outgoing'],
                'entity_id' =>$product->getId(),
                'bridge_content_edited' =>'1'
            );
            $product->addData($datatobesaved);
            try {
                $product->save(); 
                $session->addSuccess(Mage::helper('catalog')->__('The product bridge content has been saved.'));
            } catch (Exception $e) {
                $session->addError($e->getMessage());
            }
        }
    }
}
