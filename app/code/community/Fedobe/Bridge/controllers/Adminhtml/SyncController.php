<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SyncingController
 *
 * @author Fedobe1
 */
class Fedobe_Bridge_Adminhtml_SyncController extends Mage_Adminhtml_Controller_Action {

    public function preDispatch() {
        $bypassaction = array('sync_inventory', 'sync_batched_skus_for_pim', 'sync_batched_skus_for_edi', 'sync_batch_edi', 'edi_process', 'import_prodct_to_edi', 'create_conveted_price_cron');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }

    /**
     * sync the category and attribute for a incoming channel
     */
    public function sync_category_n_attributeAction() {
        $api_id = $this->getRequest()->getParam('id');
        if ($api_id) {
            $incoming_channel = Mage::getmodel('bridge/incomingchannels')->load($api_id)->getData();
            if (@$incoming_channel['id'] && Mage::helper('bridge')->getSalt($incoming_channel)) {
                $attr_cat_result = $this->getApiResult($incoming_channel, 'mappingdata');

                $unser_categories = unserialize($attr_cat_result['categories']);
                $unser_attributes = unserialize($attr_cat_result['attributes']);
                $options = $unser_attributes['options'];
                unset($unser_attributes['options']);
                $attr_cat_result['categories'] = serialize(array_unique(array_column($unser_categories, 'id')));
                $attr_cat_result['attributes'] = serialize($unser_attributes);
                foreach ($unser_attributes as $attribute) {
                    unset($attribute['name']);
                    $attribute_labels = array_map(function($atrlabel) {
                        if ($atrlabel['value'])
                            return array('id' => $atrlabel['attribute_id'], 'value' => $atrlabel['value'], 'store_id' => $atrlabel['store_id']);
                    }, $attribute);
                }
                $attribute_labels = array_filter($attribute_labels);
//            echo "<pre>";
//            print_r($attribute_labels);exit;
                Mage::getmodel('bridge/storeview_label')->deleteLabel($api_id);
                if (@$unser_categories)
                    Mage::getmodel('bridge/storeview_label')->insertMutipleRows($unser_categories, $api_id, 'category');
                if (@$attribute_labels)
                    Mage::getmodel('bridge/storeview_label')->insertMutipleRows($attribute_labels, $api_id, 'attribute');
                if (@$options)
                    Mage::getmodel('bridge/storeview_label')->insertMutipleRows($options, $api_id, 'options');
                $attr_cat_result['brand_id'] = $api_id;
                $att_cat_model = Mage::getmodel('bridge/attribute_categories');
                $att_cat_model->deleteByBrandId($api_id);
                $att_cat_model->setData($attr_cat_result);
                $att_cat_model->save();
            }
        }
        Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('bridge')->__('Attribute and categories are synced sucessfully.')
        );
        $this->_redirect('*/incomingchannels/edit', array('id' => $api_id, 'activeTab' => 'mapping_section'));
    }

    public function manualAction() {
        $api_id = $this->getRequest()->getParam('id');
        $this->sync_batched_skus_for_pimAction();
        $this->sync_batched_skus_for_ediAction();
        $this->sync_batch_ediAction();
        Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('bridge')->__('Manual Syncing Completed Sucessfully')
        );

        $this->_redirect('*/incomingchannels/edit', array('id' => $api_id));
    }

    /**
     * sync matched product skus from the specified inc
     * oming channel
     * @param type $brand_id
     */
    public function sync_batched_skus_for_pimAction() {
        $channel_id = @$this->getRequest()->getParam('id');
        $incomingchannels = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('id', $channel_id)
                ->addFieldToFilter('authorized', 1)
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('product_info', 1)
                ->addFieldToFilter(array(
                    'create_new', //attribute_1 with key 0
                    'update_exist', //attribute_2 with key 1
                        ), array(
                    array('eq' => 1), //condition for attribute_1 with key 0
                    array('eq' => 1), //condition for attribute_2
                        )
                )
                ->getFirstItem()
                ->getData();
//get salt
        if (@$incomingchannels['id'] && Mage::helper('bridge')->getSalt($incomingchannels)) {
            $matched_skus = $this->getApiResult($incomingchannels, 'batchsku');
            if (!@$matched_skus['skus']) {
                Mage::getmodel('bridge/incomingchannels_skus')->makeSkuNotAvailable($incomingchannels['id']);
                exit;
            }
            Mage::getmodel('bridge/incomingchannels_skus')->insert($matched_skus, $incomingchannels);
        }
        $tab_name = ($this->getRequest()->getParam('activeTab')) ? $this->getRequest()->getParam('activeTab') : 'main_section';
        if (isset($_SERVER['argv']) && !empty($_SERVER['argv'])) {
            exit;
        }
//        else {//echo $msg;exit;
//            $this->_redirect('*/incomingchannels/edit', array('id' => $channel_id));
//        }
    }

    /**
     * get matched sku for edi action
     */
    public function sync_batched_skus_for_ediAction() {
        $channel_id = @$this->getRequest()->getParam('id');
        $incomingchannels = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('id', $channel_id)
                ->addFieldToFilter('authorized', 1)
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('product_pricenquantity', 1)
                ->getFirstItem()
                ->getData();
//get salt
//            echo "<pre>";
//            print_r($incomingchannels);exit;
        if (@$incomingchannels['id'] && Mage::helper('bridge')->getSalt($incomingchannels)) {
            $sku_list = $this->getApiResult($incomingchannels, 'edisku');
            Mage::getmodel('bridge/inventory')->updateEDITable($sku_list, $channel_id);
        }
        if (isset($_SERVER['argv']) && !empty($_SERVER['argv'])) {
            exit;
        }
    }

    /**
     * get price n qunatity of batch sku
     */
    public function sync_batch_ediAction() {
        $channel_id = @$this->getRequest()->getParam('id');
        $incomingchannels = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('id', $channel_id)
                ->addFieldToFilter('authorized', 1)
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('product_pricenquantity', 1)
                ->getFirstItem()
                ->getData();
//get salt
        if (@$incomingchannels['id'] && Mage::helper('bridge')->getSalt($incomingchannels)) {
            while (1) {
                $api_result = $this->getApiResult($incomingchannels, 'edi');
//                echo "<pre>";
//                print_r($api_result);exit;
                if (count($api_result) && isset($api_result[0])) {
                    $this->trackError(@$incomingchannels, $api_result[0], 'edi');
                    break;
                }
                if ($api_result)
                    Mage::getmodel('bridge/incomingchannels_product')->saveSkuInfo($incomingchannels['id'], $incomingchannels['currency'], $api_result, null, null, $incomingchannels['slab'], $incomingchannels['channel_order']); //, $edi_info['price'], $edi_info['qty'], $slab, $order);
            }
            echo "<pre>";
            print_r($api_result);
            exit;
        }
        if (isset($_SERVER['argv']) && !empty($_SERVER['argv'])) {
            exit;
        }
    }

    //new edi processing priorities
    /**
     * update price and quantity
     */
     public function edi_processAction() {
        //clean the edi table 
        $this->cleanTrashFromEdi();
        //make the out of stock products as 0 quantity
        Mage::getmodel('bridge/inventory')->updateOutOfStockAsZeroQty();
        if (Mage::getmodel('bridge/inventory')->processDeletedSku()) {
            $message = "Process deleted skus";
            //exit;
        } else if (Mage::getmodel('bridge/inventory')->processPrioritiesSku(3, 1)) {//next check if any edi inventory changed from non zero to zero with live products
            $message = "Processed non zero to zero with live skus";
            // exit;
        } else if (Mage::getmodel('bridge/inventory')->processPrioritiesSku(4, 1)) {//next check if any price of any inventory is changed with live products
            $message = "Processed price changed  with live skus";
            //exit;
        } else if (Mage::getmodel('bridge/inventory')->processNonStockSku()) {//next check if any own store product having empty inventory
            $message = "Processed nonstock skus";
            // exit;
        } else if (Mage::getmodel('bridge/inventory')->processPrioritiesSku(2, 1)) {//next check if any edi inventory changed from  zero to non zero with live products
            $message = "Processed zero to non zero with live skus";
            // exit;
        } else if (Mage::getmodel('bridge/inventory')->processPrioritiesSku(2)) {//next check if any edi inventory changed from  zero to non zero which are not live products
            $message = "zero to non zero with  not live skus";
            //exit;
        } else if (Mage::getmodel('bridge/inventory')->processPrioritiesSku(4)) {//next check if any price of any inventory is changed which are not live
            $message = "Processed price changed  with non live";
            //exit;
        } else if (Mage::getmodel('bridge/inventory')->processPrioritiesSku(5, 1)) {//next check if any price of any inventory is changed which are not live
            $message = "Processed quantity changed with live skus";
            //exit;
        } else if (Mage::getmodel('bridge/inventory')->processPrioritiesSku(5)) {//next check if any price of any inventory is changed which are not live
            $message = "Processed quantity changed with non live skus";
            //exit;
        } else if (Mage::getmodel('bridge/inventory')->processSeriesSku()) { //next check all the remaining scenario (if quantity changed for edi inventory)
            $message = "Processed the remaining skus which were not processed";
            // exit;
        } else if (Mage::getmodel('bridge/inventory')->processNonstockChannel()) { //next check all the remaining scenario (if quantity changed for edi inventory)
            $message = "Changed source channel for non stock skus";
            // exit;
        } else {
            $message = "You have no inventory to process";
        }
        if (!file_exists($_SERVER["DOCUMENT_ROOT"] . '/bridgecron')) {
            mkdir($_SERVER["DOCUMENT_ROOT"] . '/bridgecron');
        }
        chmod($_SERVER["DOCUMENT_ROOT"] . '/bridgecron', 0777);
        $contents = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/bridgecron/edi_process.php');
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/bridgecron/edi_process.php', $contents . "\n" . $message);
        echo $message;
        exit;
    }
    /**
     * clean edi table from thrashed outgoing which are 30 days earlier record
     */
    public function cleanTrashFromEdi(){
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection("core_write");
        $bridge_edi_table = $resource->getTableName('bridge/incomingchannels_product');
        $writeConnection->raw_query("DELETE FROM $bridge_edi_table WHERE channel_type='outgoing' AND is_processed=4 AND  UNIX_TIMESTAMP(last_updated_time) < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 30 DAY));");
   }

//if any error occuer in syncing then track the error in error log table
    public function trackError($inc_channel, $message, $sync_type = 'pim') {
        $errorlog = Mage::getmodel('bridge/errorlog');
        if ($sync_type == 'edi')
            $errorlog->setData(array('channel_id' => $inc_channel['id'], 'message' => $message));
        else {
            $errorlog->setData(array('channel_id' => $inc_channel['id'], 'sku' => $inc_channel['sku'], 'message' => $message));
            $this->changeQueueProcessStatus($inc_channel['sku'], 1, NULL, $inc_channel['store']);
            $this->changeQueueRelationStatus($inc_channel['sku'], $inc_channel['id']);
        }
        $errorlog->save();
        if ($sync_type != "edi")
            exit;
    }

    /**
     * sync inventory for a incoming channel
     */
    public function sync_inventoryAction() {
        $api_id = @$this->getRequest()->getParam('id') ? $this->getRequest()->getParam('id') : null;
//get a sku from matchinng sku list to sync it details
        $inc_channel = Mage::getmodel('bridge/incomingchannels_skus')->getsku($api_id);
//        echo "<pre>";
//        print_r($inc_channel);exit;


        if ($inc_channel['id']) {
            //salt doesnot match for any channel then track the error and marked as processed
            if (!Mage::helper('bridge')->getSalt($inc_channel)) {
                $this->trackError($inc_channel, "Error in Security Salt");
            }
//check if sku exist
            $sku_exist_in_catalog = Mage::getSingleton('catalog/product')->loadByAttribute('sku', $inc_channel['sku']);
//check the channel setup is it allowing to create and update product
            if (($sku_exist_in_catalog && !$inc_channel['update_exist']) || (!$sku_exist_in_catalog && !$inc_channel['create_new'])) {
                $this->trackError($inc_channel, 'Now SKU is not matched with current channel condition');
            }
            $api_result = $this->getApiResult($inc_channel, 'pim');
            if (count($api_result) == 1) {
                $this->trackError($inc_channel, $api_result[0]);
            }
            if ($inc_channel['relation']) {
                $inc_channel['sku'] = explode(',', $inc_channel['sku'])[0];
            }
            $product_data = unserialize($api_result['pim']);
//current attribute set id and status  and product type of synced product
            $synced_product_data = array_keys($product_data['store']);
            $current_attributeset = $product_data['store'][$synced_product_data[0]]['attribute_set_id'];
            $current_status = $product_data['store'][$synced_product_data[0]]['status'];
            $product_type_id = $product_data['store'][$synced_product_data[0]]['type_id'];


//if product exist and type id is differeent then track the error message
            if ($sku_exist_in_catalog && $sku_exist_in_catalog->getTypeId() != $product_type_id) {
                $this->trackError($inc_channel, "Changing the existing product type is not prossible");
            }
            $entityId = $sku_exist_in_catalog ? $sku_exist_in_catalog->getId() : '';
            $event = $entityId ? 'content-updated' : 'created';
            $status = $sku_exist_in_catalog ? $sku_exist_in_catalog->getStatus() : 2; //$this->getProductStatus($entityId, $current_status, $inc_channel);
//            echo "<pre>";
//            print_r($product_data);
//            exit;

            $store_attr = Mage::getModel('eav/entity_attribute')->getCollection()
                    ->addFieldToFilter('is_user_defined', 1)
                    ->addFieldToSelect(array('attribute_id', 'attribute_code'))
                    ->getData();
            $store_attr = array_column($store_attr, 'attribute_code', 'attribute_id');
//end
//get mapping data
            $mapping_data = unserialize($inc_channel['mapping']);
            $store_mapping = $mapping_data['store'];
//get attribute code for mapping with attribute id
            $attr_cat = Mage::getmodel('bridge/attribute_categories')->getCollection()->addFieldToFilter('brand_id', $inc_channel['id'])->addFieldToSelect(array('attributes', 'categories'))->getFirstItem()->getData();
            $attr_codes = array();
            $brand_mapping_attributes = array();
            $store_mapping_attributes = array();

            if (isset($attr_cat['attributes']))
                foreach (unserialize($attr_cat['attributes']) as $attr_Set) {
                    if (isset($attr_Set['name']))
                        unset($attr_Set['name']);
                    $attr_codes +=array_column($attr_Set, 'attribute_code', 'attribute_id');
                }

//            $updated_at = $product_data['store'][array_keys($product_data['store'])[0]]['updated_at'];
            if ($mapping_data) {
                if (@$mapping_data[$current_attributeset]) {
                    $brand_mapping_attributes = array_keys($mapping_data[$current_attributeset]);
                    $store_mapping_attributes = $mapping_data[$current_attributeset];
                    $bnd_atr_code = $this->get_values_for_keys($attr_codes, $brand_mapping_attributes);
                    $str_atr_code = $this->get_values_for_keys($store_attr, $store_mapping_attributes);
                    $mapping_data[$current_attributeset] = array_combine($bnd_atr_code, $str_atr_code);
                }
                if (isset($mapping_data['option_mapping']))
                    foreach ($mapping_data['option_mapping'] as $attr_id => $opt) {
                        unset($mapping_data['option_mapping'][$attr_id]);
                        $mapping_data['option_mapping'][$attr_codes[$attr_id]] = $opt;
                    }
//get all options for attributes for brand
                if (!empty($brand_mapping_attributes)) {
                    $brand_options_collection = Mage::getmodel('bridge/storeview_label')->getCollection();
                    $brand_option_data = $brand_options_collection->addFieldToSelect(array('attribute_id', 'entity_id', 'value'))
                            ->addFieldToFilter('brand_id', $inc_channel['id'])
                            ->addFieldToFilter('type', array('eq' => 'options'))
//                        ->addFieldToFilter('attribute_id', array('in' => $brand_mapping_attributes))
                            ->addGroupByEntityId()
                            ->getData();
                    $brand_options = array();
                    foreach ($brand_option_data as $bnd_data) {
                        $brand_options[$attr_codes[$bnd_data['attribute_id']]][$bnd_data['entity_id']] = $bnd_data['value'];
                    }
                }
//end
                if (!empty($store_mapping_attributes))
//get all options for attributes which are mapped for own store
                    $store_options = Mage::getmodel('bridge/incomingchannels')->getoptions($store_mapping_attributes, $store_attr);
            }



//save product content store wise
            $storeview = array();
//            echo "<pre>";
//            print_r($mapping_data);
            foreach ($product_data['store'] as $storeId => $product) {
//set the default value for this product
                $product['entity_id'] = $entityId;
                $product['status'] = $status;
                $product['bridge_channel_id'] = $inc_channel['id'];
//replace the attribute set in attribute map
                $get_attr_codes = @$mapping_data[$current_attributeset];
                $replace_attribute = $this->get_values_for_keys($product, @array_keys($get_attr_codes)); //@array_intersect_key($get_attr_codes, $product);

                $replaced_values = @array_combine($get_attr_codes, $replace_attribute);
//map the attribute options if it is mapped
                if (@$replaced_values)
                    $product = @array_merge($product, $replaced_values);
                if (@$mapping_data[$current_attributeset]) {
                    $option_attributes = array_intersect_key($product, $mapping_data[$current_attributeset]);
                    $option_attributes = array_intersect_key($option_attributes, $brand_options);

                    if (array_filter($option_attributes))
                        foreach ($option_attributes as $bnd_attr_code => $product_value) {
                            if (isset($mapping_data['option_mapping'][$bnd_attr_code][$product_value])) {
                                if (strpos($mapping_data['option_mapping'][$bnd_attr_code][$product_value], ',')) {
                                    $mapping_data['option_mapping'][$bnd_attr_code][$product_value] = explode(',', $mapping_data['option_mapping'][$bnd_attr_code][$product_value]);
                                }
                                $product[$mapping_data[$current_attributeset][$bnd_attr_code]] = $mapping_data['option_mapping'][$bnd_attr_code][$product_value];
                            } else if ($store_opt_id = @array_search($brand_options[$bnd_attr_code][$product[$bnd_attr_code]], $store_options[$mapping_data[$current_attributeset][$bnd_attr_code]])) {
                                $product[$mapping_data[$current_attributeset][$bnd_attr_code]] = $store_opt_id;
                            } else if ($inc_channel['create_option']) {//then create the option for this attribute
                                $product[$mapping_data[$current_attributeset][$bnd_attr_code]] = Mage::getmodel('bridge/incomingchannels')->add_new_option($mapping_data[$current_attributeset][$bnd_attr_code], $brand_options[$bnd_attr_code][$product[$bnd_attr_code]]);
                            }
                            if ($mapping_data[$current_attributeset][$bnd_attr_code] != $bnd_attr_code)
                                unset($product[$bnd_attr_code]);
                        }
                }

                if (@$get_attr_codes)
                    $product['attribute_set_id'] = $mapping_data['fieldset'][$current_attributeset];
                //take the source content in separate copy
                $product['inc_description'] = @$product['description'];
                $product['inc_short_description'] = @$product['short_description'];
                $product['inc_name'] = @$product['name'];
                if ($entityId) {
                    unset($product['description']);
                    unset($product['short_description']);
                    unset($product['name']);
                }

//get product model for storeid
                $product_model = '';
                $product_model = Mage::getModel('catalog/product')->setData($product);
                $product_model->setStoreId($store_mapping[$storeId]);
                try {
                    if (!$product['entity_id']) {
//if the synced produt is configurable
                        if (isset($product_data['ConfigAttr'])) {
//                        $product_model->setCanSaveCustomOptions(true);
                            $product_model->getTypeInstance()->setUsedProductAttributeIds($product_data['ConfigAttr']); //attribute ID of attribute 'color' in my store
                            $configurableAttributesData = $product_model->getTypeInstance()->getConfigurableAttributesAsArray();
                            $product_model->setConfigurableAttributesData($configurableAttributesData);
                            $product_model->setCanSaveConfigurableAttributes(true);
                            $product_model->setConfigurableProductsData(array());
                        }
                    }
//save the product 
//                    echo "<pre>";
//                    print_r($product_model);exit;

                    $product_model->save();
                    $entityId = $product_model->getId();
                    $storeview[] = $store_mapping[$storeId];
                } catch (Exception $e) {
                    echo "<pre>";
                    print_r($product);
                    print_r($e->getMessage());
                    exit;
                }
            }
            //check the condition for content source
            foreach ($storeview as $ownstoreId) {
                $data = array();
                $product = Mage::getmodel('catalog/product')->setStoreId($ownstoreId)->load($entityId);
                if (strcasecmp(@$inc_channel['content_source'], 'rule_base_original') === 0) {
                    $contentruledata = Mage::helper('bridge')->getConvertedString(array('id' => $inc_channel['id']), $entityId, 'inc', $ownstoreId);
                    if ($contentruledata['description'])
                        $data['description'] = $contentruledata['description'];
                    if ($contentruledata['short_description'])
                        $data['short_description'] = $contentruledata['short_description'];
                    if ($contentruledata['name'])
                        $data['name'] = $contentruledata['name'];
                } else if (strcasecmp(@$inc_channel['content_source'], 'original') === 0) {
                    $contentruledata = Mage::helper('bridge')->getConvertedString(array('id' => $inc_channel['id']), $entityId, 'inc', $storeId);
                    $data['name'] = $contentruledata['name'] ? $contentruledata['name'] : $product->getIncName();
                    $data['description'] = $contentruledata['description'] ? $contentruledata['description'] : $product->getIncDescription();
                    $data['short_description'] = $contentruledata['short_description'] ? $contentruledata['short_description'] : $product->getIncShortDescription();
                }
                if ($data) {
                    $product->addData($data);
                    $product->save();
                }
            }


            if (!$sku_exist_in_catalog) {
//save product image
                if ($product_data['image']) {
                    $image = $product_data['image'];
                    $this->addProductGalleryImages($image, $inc_channel['sku'], $product_model);
                }
//if product not exist then set the default channel id
                $defaultstoreId = $this->getDefaultStoreId();
                $product_model = Mage::getModel('catalog/product')->load($entityId);
                $product_model->setStoreId($defaultstoreId);
                $product_model->setBridgeChannelId($inc_channel['id']);
                $product_model->save();
            }
//save product category
            if (isset($product_data['categories']) && $product_data['categories']) {
                $categories = $mapping_data['category'] ? $this->get_values_for_keys($mapping_data['category'], $product_data['categories']) : $product_data['categories'];
                $exist_categories = $product_model->getCategoryIds();

                if ($exist_categories) {
                    $categories = array_merge($categories, $exist_categories);
                    $categories = array_unique($categories);
                }
                $product_model->setCategoryIds($categories);
                $product_model->save();
            }
//end 
//keep the synced sku price and quantity in edi table
            if (isset($api_result['edi'])) {
                if ($inc_channel['product_pricenquantity']) {
                    $edi = unserialize($api_result['edi']);
                    $quantity = $edi['qty'];
                    Mage::getmodel('bridge/incomingchannels_product')->saveSkuInfo($inc_channel['id'], $inc_channel['currency'], $inc_channel['sku'], $edi['price'], $quantity, $inc_channel['slab'], $inc_channel['channel_order']);
//save the product price and quantity if product isnot prvioulsy exist
                    if (!$sku_exist_in_catalog) {
                        if (is_numeric($inc_channel['currency'])) {
                            $edi['price'] = $edi['price'] * $inc_channel['currency'];
                        } else {
                            $current_currency = Mage::app()->getStore()->getBaseCurrencyCode();
                            $edi['price'] = Mage::helper('directory')->currencyConvert($edi['price'], $inc_channel['currency'], $current_currency);
                        }
                        $price = Mage::helper('bridge')->addPriceRule($inc_channel, $entityId, $edi['price']);
//change the product quantity if synced quantity is not 0
//                        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($entityId);
//                        if (intval($quantity)) {
                        $product_model->addData(array('price' => $price, 'msrp' => $edi['msrp'], 'bridge_pricenqty_channel_id' => $inc_channel['id'], 'stock_data' => array('qty' => $quantity)));
                        $product_model->save();
                        Mage::getmodel('bridge/inventory')->changeLiveStatus(array(0 => array('channel_id' => $inc_channel['id'], 'sku' => $product_model->getSku())));
//                        }
                    }
                }
            }
//keep this product updation track in history table
            $storeview = implode(',', $storeview);
            $history_data = array('brand_id' => $inc_channel['id'], 'sku' => $inc_channel['sku'], 'event' => $event, 'store_view' => "$storeview", 'created' => date('Y-m-d H:i:s'));
            $history_model = Mage::getmodel('bridge/incomingchannels_history');
            $history_model->setData($history_data);
            $history_model->save();
            if (@$api_result['related']) {
                Mage::getmodel('bridge/incomingchannels_skus')->insert($api_result, $inc_channel, 'related');
            }
            if (@$api_result['upsell']) {
                Mage::getmodel('bridge/incomingchannels_skus')->insert($api_result, $inc_channel, 'upsell');
            }
            if (@$api_result['crosssell']) {
                Mage::getmodel('bridge/incomingchannels_skus')->insert($api_result, $inc_channel, 'crosssell');
            }
            if (@$api_result['associated']) {
                Mage::getmodel('bridge/incomingchannels_skus')->insert($api_result, $inc_channel, 'associated');
            }
//change the queue staus as processed sku for this channel
            $product_status = $sku_exist_in_catalog ? null : ($inc_channel['product_status'] == 3 ? $current_status : $inc_channel['product_status']);
            $this->changeQueueProcessStatus($inc_channel['sku'], 1, $product_status, $storeview);
            if (!@$api_result['upsell'] && !@$api_result['crosssell'] && !@$api_result['related'] && !@$api_result['associated']) {
                if (!is_null($product_status))
                    $this->changeProductStatus($inc_channel['sku'], $product_status);
                $this->changeQueueRelationStatus($inc_channel['sku'], $inc_channel['id']);
            }
//change the status of the sku from in process to waiting for related product process
//            $status = $sku_exist_in_catalog ? null : ($inc_channel['product_status'] === 0 ? 2 : ($inc_channel['product_status'] === 1 ? 1 : $current_status));
        } else {
// if no sku found for syncing then build relation
            $this->buildRelation();
        }
        exit;
    }

    public function getDefaultStoreId() {
        return Mage::app()->getWebsite()
                        ->getDefaultGroup()
                        ->getDefaultStoreId();
    }

    /**
     * update the product price and quantity
     * @param type $data
     * @param type $inc_channel
     */
    public function updateEDI($data, $inc_channel) {
        $product = Mage::getmodel('catalog/product')->loadByAttribute('sku', $inc_channel['sku']);
        $change_price = false;
        if ($product->getId()) {
            $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
//get the current product price and quantity before save
            $prev_price = $product->getPrice();
            $prev_quantity = $stock->getQty();

            if (!intval($stock->getQty())) {
                $change_price = true;
            } else {
                $prev_updated_cahnnel_id = Mage::getmodel('bridge/incomingchannels_history')->getCollection()
                        ->addFieldToFilter('event', 'price-updated')
                        ->addFieldToFilter('sku', $product->getSku())
                        ->setOrder('id', 'DESC')
                        ->getFirstItem()
                        ->getBrandId();
                if ($prev_updated_cahnnel_id == $inc_channel['id']) {
                    $change_price = true;
                } else if ($prev_updated_cahnnel_id) {
                    $prev_slab = Mage::getmodel('bridge/channels')->load($prev_slab)->getSlab();
                    if ($prev_slab > $inc_channel['slab']) {
                        $change_price = true;
                    } else {
                        if ($product->getPrice() > $data['price']) {
                            $change_price = true;
                        }
                    }
                }
            }
//            echo "change price" . $change_price;
//            exit;
//keep the track of price and quantity of this sku for this incoming channel
            $track_id = Mage::getmodel('bridge/incomingchannels_product')->getCollection()
                    ->addFieldToFilter('channel_id', $inc_channel['id'])
                    ->addFieldToFilter('sku', $inc_channel['sku'])
                    ->getFirstItem()
                    ->getId();
            $track_data = array('id' => $track_id, 'sku' => "{$inc_channel['sku']}", 'channel_id' => $inc_channel['id'], 'quantity' => $data['stock']['qty'], 'price' => $data['price'], 'created' => date('Y-m-d H:i:s'));
            $track_model = Mage::getmodel('bridge/incomingchannels_product');
            $track_model->setData($track_data);
            $track_model->save();
//if product price change is true
            if ($change_price) {
                $stockData = $data['stock'];
                unset($stockData['item_id']);
                $stockData['stock_id'] = $stock->getStockId();
                $stockData['product_id'] = $product->getId();
                $stockData['item_id'] = $stock->getItemId();
// Create the initial stock item object
                $stock->setData($stockData);
                $stock->save();

//change the product price
                $product->setPrice($data['price']);
                $product->save();
//get the current product price and quantity after save
                $cur_price = $product->getPrice();
                $cur_quantity = $stock->getQty();
//keep this product updation track in history table
                $history_data = array('brand_id' => $inc_channel['id'], 'sku' => $inc_channel['sku'], 'event' => 'price-updated', 'previous_price' => $prev_price, 'current_price' => $cur_price, 'previous_quantity' => $prev_quantity, 'current_quantity' => $cur_quantity, 'created' => date('Y-m-d H:i:s'));
                $history_model = Mage::getmodel('bridge/incomingchannels_history');
                $history_model->setData($history_data);
                $history_model->save();
                return true;
            }
        }
        return false;
    }

    /**
     * add image gallery to a product
     * @param type $mediaArray
     * @param type $sku
     * @param type $product
     */
    function addProductGalleryImages($mediaArray, $sku, $product) {
        $importDir = Mage::getBaseDir('media') . DS . 'Fedobebridgeimport';
        $importmediaarray = array();
        if (!file_exists($importDir)) {
            mkdir($importDir, 0777, true);
        }
        $mediaimagetype = array('image', 'small_image', 'thumbnail');
        $flagarr = array();
//Here let's copy from remote server to current store
        foreach ($mediaArray as $k => $imageinfo) {
            $imageinfo = (array) $imageinfo;
            $k++;
            $source = $imageinfo['url'];
            $imginf = pathinfo($source);
            $imagefile = $this->imageExists($importDir, $sku, $imginf['extension'], $k);
            $dest = "$importDir/$imagefile";
            if (!empty($imageinfo['types'])) {
                $types = $imageinfo['types'];
                $flag = 1;
            } else {
                $types = array();
                $flag = 0;
            }
            if (copy($source, $dest)) {
                $importmediaarray[] = array(
                    "filename" => $imagefile,
                    "types" => $types
                );
                $flagarr[] = $flag;
            }
        }
//Here to to check if not image types found then set the first image as base image, small iamge and thumnail
        if (array_sum($flagarr) == 0) {
            $importmediaarray[0]['types'] = $mediaimagetype;
        }

        foreach ($importmediaarray as $pos => $fileinfo) {
            $filePath = $importDir . '/' . $fileinfo['filename'];
            if (file_exists($filePath)) {
                try {
                    $product->addImageToMediaGallery($filePath, $fileinfo['types'], false, false);
                    unlink($filePath);
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            } else {
                echo "Product does not have an image or the path is incorrect. Path was: {$filePath}<br/>";
            }
        }
        $product->save();
    }

    function imageExists($importDir, $sku, $ext, $k) {
        if (!file_exists("$importDir/$sku" . "_$k.$ext")) {
            return "$sku" . "_$k.$ext";
        } else {
            $k = $k . "_" . $k;
            return imageExists($importDir, $sku, $ext, $k);
        }
    }

    public function buildRelation() {
        $sku_list = Mage::getmodel('bridge/pimqueue')->getUnmappedSkus();
//        echo "<pre>";
//        print_r($sku_list);exit;
        foreach ($sku_list as $sku_dtls) {
            $parent_skulist = $sku_dtls['sku_dtls'];
            $parent_skulist_array = explode(',', $parent_skulist);
            foreach ($parent_skulist_array as $single_sku) {
                $single_sku = explode(':', $single_sku);
                $product_status[$single_sku[0]] = $single_sku[1];
            }
            $channel_wise_sku_list = Mage::getmodel('bridge/incomingchannels_skus')->getSkuRelation(array_keys($product_status), $sku_dtls['channel_id']);
//            echo "<pre>";
//            print_r($channel_wise_sku_list);
//            exit;
            if (!$channel_wise_sku_list)
                continue;
            $current_parent_sku = $channel_wise_sku_list[0]['parent_sku'];
            $count = count($channel_wise_sku_list) - 1;
            foreach ($channel_wise_sku_list as $key => $sku_relation) {
                if (($sku_relation['parent_sku'] != $current_parent_sku) || $count == $key) {
                    if ($product_status[$current_parent_sku] !== 0 && !is_null($product_status[$current_parent_sku]) && $product_status[$current_parent_sku] != 'null')
                        $this->changeProductStatus($current_parent_sku, $product_status[$current_parent_sku]);
                    $this->changeQueueRelationStatus($current_parent_sku, $sku_dtls['channel_id']);
                    $current_parent_sku = $sku_relation['parent_sku'];
                }
                $sku_relation['sku'] = explode(',', $sku_relation['sku']);
                if ($sku_relation['relation'] == 'upsell')
                    $this->saveUpSellProduct($sku_relation['parent_sku'], $sku_relation['sku']);
                else if ($sku_relation['relation'] == 'crosssell')
                    $this->saveCrossSellProduct($sku_relation['parent_sku'], $sku_relation['sku']);
                else if ($sku_relation['relation'] == 'related')
                    $this->saveRelatedProduct($sku_relation['parent_sku'], $sku_relation['sku']);
                else if ($sku_relation['relation'] == 'associated')
                    $this->saveConfigurableProduct($sku_relation['parent_sku'], $sku_relation['sku']);
            }
        }
    }

    /**
     * create cross sell relation
     * 
     */
    public function saveCrossSellProduct($parent_sku, $sku_list) {
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $parent_sku);
        $crosssells = $product->getCrossSellProducts();
        $param = array();
        foreach ($crosssells as $item) {
            $param[$item->getId()] = array('position' => $item->getPosition());
        }
        $counter = count($param);
        $entity_ids = $this->getIdBySku($sku_list);
        foreach ($entity_ids as $eid) {
            if (!isset($param[$eid])) { //prevent elements from beeing overwritten
                $param[$eid] = array(
                    'position' => ++$counter
                );
            }
        }

        $product->setCrossSellLinkData($param);
        $product->save();
    }

    /**
     * create up sell relation
     * 
     */
    public function saveUpSellProduct($parent_sku, $sku_list) {
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $parent_sku);
        $upsells = $product->getUpSellProducts();
        $param = array();
        foreach ($upsells as $item) {
            $param[$item->getId()] = array('position' => $item->getPosition());
        }
        $counter = count($param);
        $entity_ids = $this->getIdBySku($sku_list);
        foreach ($entity_ids as $eid) {
            if (!isset($param[$eid])) { //prevent elements from beeing overwritten
                $param[$eid] = array(
                    'position' => ++$counter
                );
            }
        }
        $product->setUpSellLinkData($param);
        try {
            $product->save();
        } catch (Exception $e) {

            print_r($e);
            exit;
        }
    }

    /**
     * create related product relation
     * 
     */
    public function saveRelatedProduct($parent_sku, $sku_list) {
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $parent_sku);
        $related = $product->getRelatedProducts();
        $param = array();
        foreach ($related as $item) {
            $param[$item->getId()] = array('position' => $item->getPosition());
        }
        $counter = count($param);
        $entity_ids = $this->getIdBySku($sku_list);
        foreach ($entity_ids as $eid) {
            if (!isset($param[$eid])) { //prevent elements from beeing overwritten
                $param[$eid] = array(
                    'position' => ++$counter
                );
            }
        }
        $product->setRelatedLinkData($param);
        $product->save();
    }

    /**
     * assign simple product to configurable product
     * 
     */
    public function saveConfigurableProduct($parent_sku, $sku_list) {
        $mainConfigrableProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $parent_sku);
        $entity_ids = $this->getIdBySku($sku_list);
////add simple product to configurable product
        Mage::getResourceSingleton('catalog/product_type_configurable')->saveProducts($mainConfigrableProduct, $entity_ids);
    }

    /**
     */
    public function getProductStatus($exist, $current_status, $incoming_channel) {
        $status = $exist ? ($incoming_channel['product_status'] === 0 ? 2 : ($incoming_channel['product_status'] === 1 ? 1 : $current_status)) : 2;
        return $status;
    }

    /**
     * get product id by product sku
     * @param type $sku_list
     * @return type
     */
    public function getIdBySku($sku_list) {
        $product_skus = Mage::getmodel('catalog/product')->getCollection()
                        ->addAttributeToFilter('sku', array('in' => $sku_list))
                        ->addAttributeToSelect('entity_id')->getData();
        return array_column($product_skus, 'entity_id', 'sku');
    }

    /**
     * change the status process of incoming channel sku
     * @param type $id
     */
    public function changeQueueProcessStatus($sku, $is_processed, $product_status, $storeview) {
        Mage::getModel('bridge/pimqueue')->changeProcessStatus($sku, $is_processed, $product_status, $storeview);
    }

    /**
     * change the status process of incoming channel sku
     * @param type $id
     */
    public function changeQueueRelationStatus($sku, $channel_id) {
        Mage::getModel('bridge/pimqueue')->changeRelationStatus($sku, $channel_id);
    }

    /**
     * change the product stataus
     * @param type $sku
     * @param type $product_status
     */
    public function changeProductStatus($sku, $product_status) {
//        $defaultstoreId = $this->getDefaultStoreId();
        $product_id = Mage::getModel("catalog/product")->getIdBySku($sku);
        try {
            $product_status = (int) $product_status;
            Mage::getModel('catalog/product_status')->updateProductStatus($product_id, 0, $product_status);
        } catch (Exception $e) {
//            echo "<pre>";
//            print_r($e->getMessage());
        }
    }

    /**
     * replace the place holder by the variable
     */
    public function varReplace($string, $product) {
        $place_holders = array('{name}', '{sku}');
        $replace = array(@$product['name'], @$product['sku']);
        return str_replace($place_holders, $replace, $string);
    }

    /**
     * return the channel priority
     */
    public function checkChannelPriority($channel1, $channel2) {
        $group_id1 = Mage::getmodel('bridge/groups')->getCollection()
                ->addFieldToFilter("channel_id", array('finset' => "$channel1"))
                ->getFirstItem()
                ->getId();
        $group_id2 = Mage::getmodel('bridge/groups')->getCollection()
                ->addFieldToFilter("channel_id", array('finset' => "$channel2"))
                ->getFirstItem()
                ->getId();
        $rtrn = @$group_id1 ? ((!$group_id2 || $group_id1 < $group_id2) ? true : ($group_id1 == $group_id2 ? "same" : false)) : false;
        return $rtrn;
    }

    /**
     * get the result of the api call
     * @param type $api_id 
     * @param type $action method that need to call
     * @return type array of data
     */
    public function getApiResult($incoming_channel, $method = '') {
//oAuth parameters
        $params = array(
            'siteUrl' => $incoming_channel['url'] . '/oauth',
            'requestTokenUrl' => $incoming_channel['url'] . '/oauth/initiate',
            'accessTokenUrl' => $incoming_channel['url'] . '/oauth/token',
            'consumerKey' => $incoming_channel['key'],
            'consumerSecret' => $incoming_channel['secret']
        );

// Get session
        $session = Mage ::getSingleton('core/session');
// Read and unserialize request token from session
//        $requestToken = unserialize($session->getRequestToken());
// Initiate oAuth consumer
        $consumer = new Zend_Oauth_Consumer($params);
        $acessToken = new Zend_Oauth_Token_Access();
//set the permanent token and secret
        $acessToken->setToken($incoming_channel['permanent_token']);
        $acessToken->setTokenSecret($incoming_channel['permanent_secret']);
// Get HTTP client from access token object
        $restClient = $acessToken->getHttpClient($params);

// Set REST resource URL
        $salt = $incoming_channel['salt'] ? base64_encode($incoming_channel['salt']) : 0;
        $parameterForValidation = "salt/{$salt}/salt_type/{$incoming_channel['salt_type']}";

        if (strcasecmp($method, 'mappingdata') === 0) {//url for get category and attribute set
            $restClient->setUri($incoming_channel['url'] . '/api/rest/mappingdata/fetch/mappingdata/' . $parameterForValidation);
        } else if (strcasecmp($method, 'batchsku') === 0) {//url for batch sku for a channel
            $restClient->setUri($incoming_channel['url']
                    . "/api/rest/products/fetch/batchsku/$parameterForValidation");
        } else if (strcasecmp($method, 'pim') === 0) {//url for syncing a sku content
            $relation = $incoming_channel['relation'] ? $incoming_channel['relation'] : 0;
            if ($relation) {
                $incoming_channel['sku'] = $incoming_channel['sku'] . "," . $incoming_channel['parent_sku'];
            }
            $incoming_channel['sku'] = urlencode($incoming_channel['sku']);
            $restClient->setUri($incoming_channel['url']
                    . "/api/rest/products/fetch/pim/sku/{$incoming_channel['sku']}/updated/0/store/{$incoming_channel['store']}/relation/{$relation}/relative_product/{$incoming_channel['relative_product']}/$parameterForValidation");
        } else if (strcasecmp($method, 'edi') === 0) {//url for syncing edi
            $restClient->setUri($incoming_channel['url']
                    . "/api/rest/products/fetch/edi/relative_product/{$incoming_channel['relative_product']}/$parameterForValidation");
        } else if (strcasecmp($method, 'edisku') === 0) {//url for syncing edi
            $restClient->setUri($incoming_channel['url']
                    . "/api/rest/products/fetch/edisku/$parameterForValidation");
        }

// In Magento it is neccesary to set json or xml headers in order to work
        $restClient->setHeaders('Accept', 'application/json');
        $restClient->setMethod(Zend_Http_Client::GET);
// Get method
//Make REST request
        $response = $restClient->request();
        return (array) json_decode($response->getBody());
    }

    /**
     * get the values of mapping array of the keys provided in keys array
     * @param type $mapping
     * @param type $keys
     * @return type array
     */
    public function get_values_for_keys($mapping, $keys) {
        if ($keys)
            foreach ($keys as $key) {
                $output_arr[] = @$mapping[$key];
            }
        return @$output_arr;
    }

    /**
     * import the product of own store in bridge inventory table
     */
    public function import_prodct_to_ediAction() {
        $skus_present = Mage::getmodel('bridge/incomingchannels_product')->getCollection()
                ->addFieldToSelect('sku')
                ->addFieldToFilter('channel_id', 0)
                ->getData();
        $sku_list = array_column($skus_present, 'sku');
        Mage::getmodel('bridge/inventory')->saveStoreProductDetails($sku_list);
        exit;
    }

    public function create_conveted_price_cronAction() {
        $channel_id = $this->getRequest()->getParam('id');
        $channel_type = $this->getRequest()->getParam('channel_type');
        Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $bridge_edi_crontable = $resource->getTableName('bridge/bridgeapi_edichannelcron');
        $status_msg = array(-1 => 'new', -2 => 'new-outofstock');
//Get channelid from EdiCron table
        if ($channel_id)
            $sql = "SELECT * FROM " . $bridge_edi_crontable . " WHERE (is_processed = 0  OR condition_changed OR new_added) AND status = 1 AND channel_id=$channel_id AND channel_type='{$channel_type}' limit 1";
        else
            $sql = "SELECT * FROM " . $bridge_edi_crontable . " WHERE (is_processed = 0  OR condition_changed OR new_added) AND status = 1 ORDER BY FIELD(channel_type,'outgoing','incoming'),id limit 1";
        $edicron_detail = $readConnection->fetchRow($sql);
        $productinfo_table_name = $resource->getTableName('bridge/incomingchannels_product');
        echo "<pre>";
        print_r($edicron_detail);

        if ($edicron_detail) {
            //get the server current time
            $now_time = $readConnection->fetchRow("SELECT NOW() FROM $bridge_edi_crontable WHERE 1 limit 0,1");
            $now_time = $now_time['NOW()'];

            $channel_type = $edicron_detail['channel_type'];
            $channel_id = $edicron_detail['channel_id'];
            if ($edicron_detail['channel_type'] === 'outgoing') {
//Get Outgoing channel details
                $otgchannel = Mage::getModel('bridge/outgoingchannels')->load($channel_id);

//get the product id using filter by condition or sku
                if (strcasecmp($otgchannel->getFilterBy(), 'sku') === 0) {
                    $product_ids = explode(',', $otgchannel->getSkuList());
                } else {
                    Mage::register('channel_condition', $otgchannel);
                    $product_ids = Mage::getmodel('bridge/rule')->getMatchingProductIds();
                    $product_status = $otgchannel->getProductStatus();
                    if ($product_status)
                        $product_ids = Mage::getmodel('bridge/outgoingchannels')->getFilteredSkus($product_ids, $product_status);
                }//print_r($product_ids);//exit;
                $history = array();
                if ($edicron_detail['is_processed'] && !$edicron_detail['condition_changed']) { //echo 345;exit;
                    $exist_ids = Mage::getmodel('bridge/incomingchannels_product')
                                    ->getCollection()
                                    ->addFieldToFilter('channel_id', $channel_id)
                                    ->addFieldToFilter('channel_type', 'outgoing')
                                    ->addFieldToSelect('product_entity_id')->getData();
                    if ($exist_ids) {
                        $exist_ids = array_column($exist_ids, 'product_entity_id');
                    }


                    $new_added = $exist_ids ? array_diff($product_ids, $exist_ids) : $product_ids;
                    if ($new_added) {
                        //Get the product skulist 
                        $products = Mage::getmodel('catalog/product')->getCollection()
                                ->addAttributeToFilter('entity_id', array('in' => $new_added))
                                ->addAttributeToFilter('type_id', array('in' => array('simple', 'configurable')))
                                ->addAttributeToSelect(array('price', 'special_price', 'msrp', 'ogc_price'));
                        $i = 0;
                        foreach ($products as $product_insert) {
                            $product_insert = $product_insert->getData();
                            $quantity = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product_insert['entity_id'])->getQty();
                            //update price by price rule if exist
                            $base_price = $otgchannel->getPriceSource() != "ogc_price" ? @$product_insert[$otgchannel->getPriceSource()] : (@$product_insert['ogc_price'] ? @$product_insert['ogc_price'] : (@$product_insert['price'] ? @$product_insert['price'] : @$product_insert['msrp']));
                            $base_price = $base_price ? $base_price : 0;
                            $converted_price = Mage::helper('bridge')->addPriceRule($otgchannel, $product_insert['entity_id'], $base_price, 'ogc');
                            $history[$i]['sku'] = $product_insert_detail[$i]['sku'] = addslashes($product_insert['sku']);
                            $history[$i]['brand_id'] = $product_insert_detail[$i]['channel_id'] = $channel_id;
                            $history[$i]['current_quantity'] = $product_insert_detail[$i]['quantity'] = $quantity;
                            $history[$i]['current_price'] = $product_insert_detail[$i]['price'] = @$product_insert['price'];
                            $history[$i]['channel_type'] = $product_insert_detail[$i]['channel_type'] = "outgoing";
                            $product_insert_detail[$i]['id'] = NULL;
                            $product_insert_detail[$i]['converted_price'] = $converted_price;
                            $product_insert_detail[$i]['is_processed'] = $quantity > 0 ? -1 : -2;
                            $product_insert_detail[$i]['product_entity_id'] = $product_insert['entity_id'];
                            $history[$i]['status'] = $status_msg[-1];
                            $event_msg = 'price-qty-updated';
                            $history[$i]['event'] = $event_msg;
                            $history[$i]['source'] = 'converted-price';
                            $i = $i + 1;
                            if ($i % 1000 == 0) {
                                $writeConnection->insertMultiple($productinfo_table_name, $product_insert_detail);
                                $product_insert_detail = array();
                            }
                        }//print_r($history);exit;
                        if ($product_insert_detail)
                            $writeConnection->insertMultiple($productinfo_table_name, $product_insert_detail);

                        if (!empty($history)) {
                            $history_table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_history');
                            $writeConnection->insertMultiple($history_table, $history);
                        }
                    }
                    echo "new added for outgoing channel";
                } else {

                    //Get the product collections 
                    $product_ids_str = $product_ids ? implode(',', $product_ids) : 0;
                    print_r($product_ids_str); //exit;
                    $products = Mage::getmodel('catalog/product')->getCollection()
                            ->addAttributeToFilter('entity_id', array('in' => $product_ids))
                            ->addAttributeToFilter('type_id', array('in' => array('simple', 'configurable')))
                            ->addAttributeToSelect(array('entity_id', 'price', 'special_price', 'msrp', 'ogc_price'));

                    $product_details = $products->getData();
//update the outgoing channel product status as no access status if the product is out of matchd sku list
                    $writeConnection->raw_query("UPDATE $productinfo_table_name SET is_processed=3 WHERE channel_id =$channel_id  AND channel_type ='outgoing' AND product_entity_id NOT IN ($product_ids_str) AND deleted=0 AND is_processed NOT IN (4,5);");



                    $remaining_products = Mage::getmodel('bridge/incomingchannels_product')
                                    ->getCollection()
                                    ->addFieldToFilter('channel_id', $channel_id)
                                    ->addFieldToFilter('channel_type', 'outgoing')
                                    ->addFieldToSelect(array('sku', 'is_processed', 'id', 'quantity', 'converted_price', 'price', 'product_entity_id', 'channel_id'))->getData();

                    if ($remaining_products) {
                        $remaining_products_ids = array_column($remaining_products, 'product_entity_id');
                        $remaining_products = array_combine($remaining_products_ids, $remaining_products);
                    }

                    $count_Totalproduct = $products->getSize();
                    //print_r($count_Totalproduct);exit;
                    if ($count_Totalproduct) {
                        //Data to be insert in the table bridge_incomingchannel_product_info
                        $product_insert_detail = array();
                        $i = 0;
                        $update_query = '';
                        foreach ($products as $product_insert) {
                            $product_insert = $product_insert->getData(); //print_r($product_insert);exit;
                            if (!isset($remaining_products[$product_insert['entity_id']]) || !in_array($remaining_products[$product_insert['entity_id']]['is_processed'], array(0, 1)) || (!$edicron_detail['is_processed'])) {
                                $price_changed_product_ids[] = $product_insert['entity_id'];
                                $quantity = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product_insert['entity_id'])->getQty();
                                //update price by price rule if exist
                                $base_price = $otgchannel->getPriceSource() != "ogc_price" ? @$product_insert[$otgchannel->getPriceSource()] : (@$product_insert['ogc_price'] ? @$product_insert['ogc_price'] : (@$product_insert['price'] ? @$product_insert['price'] : @$product_insert['msrp']));
                                $base_price = $base_price ? $base_price : 0;
                                $converted_price = Mage::helper('bridge')->addPriceRule($otgchannel, $product_insert['entity_id'], $base_price, 'ogc');

                                if (isset($remaining_products[$product_insert['entity_id']])) {
                                    if ($remaining_products[$product_insert['entity_id']]['price'] == $product_insert['price'] && $quantity == $remaining_products[$product_insert['entity_id']]['quantity'] && $converted_price == $remaining_products[$product_insert['entity_id']]['converted_price']) {
                                        if (in_array($remaining_products[$product_insert['entity_id']]['is_processed'],array(2,3,4)))
                                            $update_query.="UPDATE $productinfo_table_name SET is_processed=0,`last_updated_time`='{$now_time}' WHERE id={$remaining_products[$product_insert['entity_id']]['id']};";
                                        continue;
                                    }
                                    if ($remaining_products[$product_insert['entity_id']]['is_processed'] == -2 && $quantity > 0) {
                                        $status = $status_msg[-1];
                                        $update_query.="UPDATE $productinfo_table_name SET is_processed=-1,price={$product_insert['price']},quantity=$quantity,converted_price=$converted_price,`last_updated_time`='{$now_time}' WHERE id={$remaining_products[$product_insert['entity_id']]['id']};";
                                    } else if ($remaining_products[$product_insert['entity_id']]['is_processed'] == -1 && $quantity <= 0) {
                                        $status = $status_msg[-2];
                                        $update_query.="UPDATE $productinfo_table_name SET is_processed=-2,price={$product_insert['price']},quantity=$quantity,converted_price=$converted_price,`last_updated_time`='{$now_time}' WHERE id={$remaining_products[$product_insert['entity_id']]['id']};";
                                    } else if ($remaining_products[$product_insert['entity_id']]['is_processed'] == -1 || $remaining_products[$product_insert['entity_id']]['is_processed'] == -2) {
                                        $status = $status_msg[$remaining_products[$product_insert['entity_id']]['is_processed']];
                                        $update_query.="UPDATE $productinfo_table_name SET price={$product_insert['price']},quantity=$quantity,converted_price=$converted_price,`last_updated_time`='{$now_time}' WHERE id={$remaining_products[$product_insert['entity_id']]['id']};";
                                    } else
                                        $update_query.="UPDATE $productinfo_table_name SET is_processed=0,price={$product_insert['price']},quantity=$quantity,converted_price=$converted_price,`last_updated_time`='{$now_time}' WHERE id={$remaining_products[$product_insert['entity_id']]['id']};";
                                    $history[$i]['sku'] = $product_insert['sku'];
                                    $history[$i]['brand_id'] = $otgchannel->getId(); //$remaining_products[$product_insert['entity_id']]['channel_id'];
                                    $history[$i]['previous_quantity'] = $remaining_products[$product_insert['entity_id']]['quantity'];
                                    $history[$i]['current_quantity'] = $quantity;
                                    $history[$i]['previous_price'] = $remaining_products[$product_insert['entity_id']]['price'];
                                    $history[$i]['current_price'] = $converted_price;
                                    $history[$i]['channel_type'] = 'outgoing';
                                    $history[$i]['status'] = $status;
                                    //echo $update_query;

                                    $event = array();
                                    if (intval($history[$i]['previous_quantity']) != intval($history[$i]['current_quantity']))
                                        $event[] = 'quantity-updtaed';
                                    if (intval($history[$i]['previous_price']) != intval($history[$i]['current_price']))
                                        $event[] = 'price-updated';
                                    if (count($event) == 1) {
                                        $event_msg = $event[0];
                                    } else if (count($event) == 2) {
                                        $event_msg = 'price-qty-updated';
                                    } else {
                                        $event_msg = 'nochange';
                                    }
                                    $history[$i]['event'] = $event_msg;
                                    $history[$i]['source'] = 'converted-price';
//                                        if($event_msg=='no'){
//                                            unset($history[$i]);
//                                        }
                                } else {
                                    $history[$i]['sku'] = $product_insert_detail[$i]['sku'] = addslashes($product_insert['sku']);
                                    $history[$i]['brand_id'] = $product_insert_detail[$i]['channel_id'] = $otgchannel->getId();
                                    $history[$i]['current_quantity'] = $product_insert_detail[$i]['quantity'] = $quantity;
                                    $history[$i]['current_price'] = $product_insert_detail[$i]['price'] = @$product_insert['price'];
                                    $history[$i]['channel_type'] = $product_insert_detail[$i]['channel_type'] = "outgoing";
                                    //update price by price rule if exist
                                    $product_insert_detail[$i]['converted_price'] = $converted_price;
                                    $product_insert_detail[$i]['id'] = NULL;
                                    $product_insert_detail[$i]['is_processed'] = $quantity > 0 ? -1 : -2;
                                    $product_insert_detail[$i]['product_entity_id'] = $product_insert['entity_id'];
                                    $history[$i]['status'] = $status_msg[-1];
                                    $history[$i]['event'] = 'price-qty-updated';
                                    $history[$i]['source'] = 'converted-price';
                                    $i = $i + 1;
                                    if ($i % 1000 == 0) {
                                        $writeConnection->insertMultiple($productinfo_table_name, $product_insert_detail);
                                        $product_insert_detail = array();
                                    }
                                }
                            }
                        }//print_r($history);//exit;
                        if ($update_query)
                            $writeConnection->raw_query($update_query);
                        //Insert all record of the channel_id
                        if ($product_insert_detail)
                            $writeConnection->insertMultiple($productinfo_table_name, $product_insert_detail);

                        if (!empty($history)) {
                            $history_table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_history');
                            $writeConnection->insertMultiple($history_table, $history);
                        }

                        echo "Successfully saved the products";
                    } else {
                        echo "No products found";
                    }
                }
            } else {

//Get Incoming channel details
                $incchannel = Mage::getModel('bridge/incomingchannels')->load($channel_id);

                $inc_collection = Mage::getmodel('bridge/incomingchannels_product')->getCollection()
                        ->addFieldToFilter('channel_id', $channel_id)
                        ->addFieldToFilter('channel_type', $channel_type);


                if ($edicron_detail['is_processed']) {
                    $inc_collection->addFieldToFilter(
                            array('is_processed', 'converted_price'), // columns
                            array(// conditions
                        array('eq' => 2), // condition for field 1
                        array('null' => true), // conditions for field_2
                    ));
                }
                $inc_products = $inc_collection->addFieldToSelect('*')->getData();
                $entity_id_list = array_column($inc_products, 'product_entity_id');

                //Get the product skulist 
                if ($entity_id_list)
                    $product_details = Mage::getmodel('catalog/product')->getCollection()
                                    ->addAttributeToFilter('entity_id', $entity_id_list)->getData();


                $productinfo_table_name = $resource->getTableName('bridge/incomingchannels_product');
                if ($product_details) {
                    $product_entity_list = array_column($product_details, 'entity_id');
                    $count = 0;
                    $query = '';

                    foreach ($inc_products as $inc_product) {
                        if (!in_array($inc_product['product_entity_id'], $product_entity_list)) {
                            continue;
                        }
                        $converted_price = Mage::helper('bridge')->addPriceRule($incchannel, $inc_product['product_entity_id'], $inc_product['price'], 'inc');
                        if ($inc_products['converted_price'] == $converted_price && $inc_products['is_processed'] == 1)
                            continue;
                        $query.="UPDATE $productinfo_table_name SET converted_price=$converted_price,`last_updated_time`='{$now_time}',is_processed=0 Where id={$inc_product['id']};";
                        $count++;
                        if ($count % 1000 === 0) {
                            $writeConnection->raw_query($query);
                            $query = '';
                        }
                    }

                    if ($query) {
                        $writeConnection->raw_query($query);
                    }
                    echo "Successfully saved the products";
                } else {
                    echo "No products found";
                }
            }
            //Update the table bridge_edi_channelcron  after all products of channelid inseted to the table bridge_incomingchannel_product_info
            try {
                $writeConnection->raw_query("UPDATE $bridge_edi_crontable SET is_processed=1,condition_changed=0,new_added=0 WHERE channel_id = $channel_id AND channel_type ='{$channel_type}';");
                if($channel_type == 'outgoing'){
                    $ogcmodel = Mage::getModel('bridge/outgoingchannels');
                    $ogcmodel->load($channel_id);
                    $ogc_data = $ogcmodel->getData();
                    $class_type  = Mage::helper($ogc_data['channel_type'])->sortids($channel_id,$product_ids_str);
                }
            } catch (Exception $ex) {
                echo "<pre>";
                print_r($ex->getMessage());
                exit;
            }
        } else {
            echo "No Channel Found";
        }
        exit;
    }
   
}