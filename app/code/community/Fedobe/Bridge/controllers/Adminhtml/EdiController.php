<?php

class Fedobe_Bridge_Adminhtml_EdiController extends Mage_Adminhtml_Controller_Action {

    public function _initAction() {
        $this->loadLayout()->_setActiveMenu('fesdobe/bridge');
        $this->_title(Mage::helper('bridge')->__('Fedobe Extension'));
    }

    public function indexAction() {
        $this->_initAction();
        $this->_title(Mage::helper('bridge')->__('Manage EDI'));
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * ajax delete
     */
    public function deleteAction() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $write_connection = $resource->getConnection('core_write');
        $data = $this->getRequest()->getPost();
        $inventoryId = $data['id'];
        $data_sql = "SELECT sku,channel_id,product_entity_id,is_live FROM bridge_incomingchannel_product_info WHERE id=$inventoryId;";
        $data_res = $readConnection->fetchRow($data_sql);
        if ($data_res) {
            try {
                $channelId = $data_res['channel_id'];
                $channel_sql = "SELECT channel_type FROM bridge_incomingchannels WHERE id=$channelId;";
                $channel_res = $readConnection->fetchRow($channel_sql);
                $channelType = $channel_res['channel_type'];
                $helperobj = Mage::helper($channelType);
                $method = "deleteInventory";
                $write_connection->beginTransaction();
                if (method_exists($helperobj, $method)) {
                    //delete the inventory from temp table
                    $helperobj->deleteInventory($channelId,$data_res['sku']);

                    //make the product out of stock if its a live product
                    if ($data_res['is_live']) {
                        $productId = $data_res['product_entity_id'];
                        $outOfStockQuery = "UPDATE cataloginventory_stock_item SET is_in_stock=0 WHERE product_id=$productId;";
                        $write_connection->query($outOfStockQuery);
                    }
                    //delete from edi inventory
                    $deleteInvQUery = "DELETE FROM bridge_incomingchannel_product_info WHERE  id=$inventoryId;";
                    $write_connection->query($deleteInvQUery);
                    $write_connection->commit();
                }
            } catch (Exception $ex) {
                $write_connection->rollback();
            }
        }
    }

    /**
     * ajax inline update of edi
     */
    public function inlineUpdateAction() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection('core_write');
        $date_sql = "SELECT NOW() FROM catalog_product_entity WHERE 1 limit 0,1";
        $date_res = $readConnection->fetchRow($date_sql);
        $data = $this->getRequest()->getPost();
        $data['converted_price'] = null;
        $data['last_updated_time'] = $date_res['NOW()'];
        $model = Mage::getmodel('bridge/incomingchannels_product')->load($data['id']);
        $model_data = $model->getData();
        $channel_type = $model_data['channel_type'] == 'incoming' ? 'inc' : 'ogc';
        $own = 0;
        $product = Mage::getmodel('catalog/product')->load($model_data['product_entity_id']);
        $product_data = $product->getData();
        if (isset($data['channel_supplier'])) {
            $model->setData($data);
            $model->save();
            if (!$product_data['bridge_pricenqty_channel_id']) {
                $product->setSuppliers($data['channel_supplier']);
                $product->save();
            }
            exit;
        }
        if ($product_data && $model_data['channel_id']) {
            $edi_price = isset($data['price']) ? $data['price'] : $model_data['price'];
            if ($channel_type == 'inc') {
                $channel_info = Mage::getmodel('bridge/incomingchannels')->getCollection()
                        ->addFieldToFilter('id', $model_data['channel_id'])
                        ->addFieldToSelect(array('multi_price_rule', 'id'))
                        ->getFirstItem()
                        ->getData();
            } else {
                $channel_info = Mage::getmodel('bridge/outgoingchannels')->getCollection()
                        ->addFieldToFilter('id', $model_data['channel_id'])
                        ->addFieldToSelect(array('multi_price_rule' => 'multiprice_rule', 'id'))
                        ->getFirstItem()
                        ->getData();
            }
            $new_price = Mage::helper('bridge')->addPriceRule($channel_info, $product_data['entity_id'], $edi_price, $channel_type);
            $data['converted_price'] = $new_price;
        }
        $data['special_price'] = isset($data['special_price']) ? (trim($data['special_price'], "'") == 'null' ? null : trim($data['special_price'], "'")) : $model_data['special_price'];
        $model->setData($data);
        $model->save();
        if ($product_data) {
            $stockItem = $product->getStockItem(); //Mage::getModel('cataloginventory/stock_item')->loadByProduct($product['entity_id']);
            $previous_qty = $stockItem->getQty();
            $product_data['bridge_pricenqty_channel_id'] = intval(@$product_data['bridge_pricenqty_channel_id']);
//check it is a own store price or quantity is updated
            if ($model_data['channel_id'] == 0) { //print_r($model_data);exit;
                if (!$product_data['bridge_pricenqty_channel_id'] || ((isset($data['quantity']) && $data['quantity']) || (!isset($data['quantity']) && $model_data['quantity'] > 0))) {
                    $product->setPrice(isset($data['price']) ? $data['price'] : $model_data['price']);
                    $product->setBridgePricenqtyChannelId(null);
                    $product->setSpecialPrice($data['special_price']);
                    $product->setSuppliers(isset($data['channel_supplier']) ? $data['channel_supplier'] : $model_data['channel_supplier']);
                    $own_qty = isset($data['quantity']) ? $data['quantity'] : $model_data['quantity'];
                    $product->setStockData(array('qty' => $own_qty, 'is_in_stock' => $own_qty > 0 ? 1 : 0));
                    $product->save();
                    //save the data in history table
                    if ($product_data['bridge_pricenqty_channel_id']) {
                        $writeConnection->raw_query("UPDATE bridge_incomingchannel_product_info SET is_live=0 where sku='{$product_data['sku']}' AND channel_type='incoming' AND is_live=1;");
                        $writeConnection->raw_query("UPDATE bridge_incomingchannel_product_info SET is_live=1 where sku='{$product_data['sku']}' AND channel_id=0;");
//                        $history_model = Mage::getmodel('bridge/incomingchannels_history');
//                        $history_model->setData(array('sku' => $product_data['sku'], 'event' => 'price-updated', 'previous_price' => $product_data['price'], 'current_price' => $model_data['price'], 'previous_quantity' => $previous_qty, 'current_quantity' => $model_data['quantity']));
//                        $history_model->save();
                    }
                    Mage::getmodel('bridge/inventory')->changeLiveStatus(array(0 => array('channel_id' => 0, 'sku' => $product_data['sku'])));
                }
                $own = 1;
                echo "false";
                // exit;
            } else if ($model_data['channel_type'] == 'incoming') {//if any channel quantity or price changed
                //if current maganto store is change channel           
                if ($product_data['bridge_pricenqty_channel_id'] == $model_data['channel_id']) {
                    $product->setSuppliers($model_data['channel_supplier']);
                    if (isset($data['price'])) {
                        $product->setPrice($new_price);
                        $product->save();
                    } else if (isset($data['quantity'])) {
                        $product->setStockData(array('qty' => $data['quantity'], 'is_in_stock' => $data['quantity'] > 0 ? 1 : 0));
                        $product->save();
                    }
                }
            }
            //added by sital
            $current_price = (isset($data['price'])) ? $data['price'] : $model_data['price'];
            $current_qty = (isset($data['quantity'])) ? $data['quantity'] : $model_data['quantity'];

            if ($model_data['channel_id']) {
                $previous_price = $model_data['price'];
                $previous_qty_h = $model_data['quantity'];
            } else {
                $previous_price = $product_data['price'];
                $previous_qty_h = $previous_qty;
            }

            if (round($current_price, 2) != round($previous_price, 2)) {
                $event[] = 'price-updated';
            }
            if (intval($previous_qty_h) != intval($current_qty)) {
                $event[] = 'quantity-updated';
            }
            //print_r($event);exit;
            //$event_msg= (count($event)==1)?$event[0]:'price-qty-updated';
            if (count($event) == 1) {
                $event_msg = $event[0];
            } else if (count($event) == 2) {
                $event_msg = 'price-qty-updated';
            } else {
                $event_msg = 'no';
            }//echo $event_msg;exit;
            $history_model = Mage::getmodel('bridge/incomingchannels_history');
            if ($model_data['channel_id']) {

                $history = array('brand_id' => $model_data['channel_id'], 'sku' => $product_data['sku'], 'channel_type' => $model_data['channel_type'], 'event' => $event_msg, 'source' => 'inline-edit', 'previous_price' => $previous_price, 'current_price' => $current_price, 'previous_quantity' => $previous_qty_h, 'current_quantity' => $current_qty);
            } else {
                $history = array('sku' => $product_data['sku'], 'event' => $event_msg, 'source' => 'inline-edit', 'previous_price' => $previous_price, 'current_price' => $current_price, 'previous_quantity' => $previous_qty_h, 'current_quantity' => $current_qty);
            }   //print_r($history);exit; 
            $history_model->setData($history);
            $history_model->save();
            if ($own)
                exit;
            //added by sital end
        }
        $symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getBaseCurrencyCode())->getSymbol();
        $symbol = $symbol ? $symbol : Mage::app()->getStore()->getBaseCurrencyCode();
        echo "$symbol&nbsp" . number_format(Mage::helper('directory')->currencyConvert($data['converted_price'], Mage::app()->getStore()->getBaseCurrencyCode(), $data['conv_currency']), 2);
        exit;
    }

    /**
     * Export edid grid to CSV format
     */
    public function exportCsvAction() {
        $fileName = 'edi_inventory.csv';
        $grid = $this->getLayout()->createBlock('bridge/adminhtml_edi_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export edi grid to Excel XML format
     */
    public function exportExcelAction() {
        $fileName = 'edi_inventory.xml';
        $grid = $this->getLayout()->createBlock('bridge/adminhtml_edi_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    public function historygridAction() {
        if ($this->getRequest()->getParam('sku')) {

            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $writeConnection = $resource->getConnection("core_write");
            $sku = $this->getRequest()->getParam('sku'); // echo $sku;exit;
            $channel_type = strtolower($this->getRequest()->getParam('channel_type'));  //echo $channel_type;exit;
            $channel_id = $this->getRequest()->getParam('channel_id');
            if ($channel_type == 'ownstore')
                $channel_type_str = 'channel_type IS NULL';
            else if ($channel_type == 'incoming' || $channel_type == 'outgoing')
                $channel_type_str = "channel_type = '" . $channel_type . "' AND brand_id=$channel_id";
            else {
                $channel_type = $channel_type == 'allincoming' ? 'incoming' : 'outgoing';
                $channel_type_str = "channel_type = '" . $channel_type . "'";
            }

            // $channel_type_str = (strtolower($channel_type)=='ownstore')?'channel_type IS NULL':"channel_type = '".$channel_type."'";
            //print_r($channel_type_str);exit;
            $product_info_table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_history');
            $history_query = 'SELECT * FROM ' . $product_info_table . ' WHERE sku="' . $sku . '" AND ' . $channel_type_str . ' order by created desc limit 20';
            //echo $history_query;exit;
            $res = $readConnection->fetchAll($history_query);
            //print_r($res);exit;
            $block = $this->getLayout()->createBlock('core/template')
                            ->setProduct($res)
                            ->setTemplate('fedobe/bridge/history.phtml')->toHtml();
            echo $block;
            exit;
        }
    }

    public function ediGridAction() {
        $this->loadLayout();
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('Fedobe_Bridge_Block_Adminhtml_Edi_Grid')->toHtml()
        );
//        echo $this->getLayout()->createBlock('fedobe_bridge_block_adminhtml_edi_grid')->toHtml();
    }

    public function generateAction() {
        if (!$this->getRequest()->isAjax()) {
            $this->_forward('noRoute');
            return;
        }
        if ($this->getRequest()->getPost()) {
            $data = $this->getRequest()->getPost();
//Here let add the rules data
            $condition_rules = $data['rule'];
            foreach ($condition_rules['conditions'] as $id => $cdata) {
                $path = explode('--', $id);
                $node = & $arr;
                for ($i = 0, $l = sizeof($path); $i < $l; $i++) {
                    if (!isset($node['conditions'][$path[$i]])) {
                        $node['conditions'][$path[$i]] = array();
                    }
                    $node = & $node['conditions'][$path[$i]];
                }
                foreach ($cdata as $k => $v) {
//check for gender attribute array issue
                    if (is_array($v)) {
                        $v = implode(',', $v);
                    }
                    $node[$k] = $v;
                }
            }
            $conditions_serialized = serialize($arr['conditions'][1]);
        }
        $product_ids = Mage::getmodel('bridge/rule')->getMatchingProductIds($conditions_serialized);
        print_r($product_ids);
        $product_ids = $product_ids ? $product_ids : array();
        Mage::getSingleton('core/session')->setData('edi_product_ids', $product_ids);
        exit;
    }

    public function history_tabAction() {
        if ($this->getRequest()->getParam('sku')) {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $writeConnection = $resource->getConnection("core_write");
            $sku = $this->getRequest()->getParam('sku'); // echo $sku;exit;
            $channel_type = $this->getRequest()->getParam('channel_type');  //echo $channel_type;exit;
            $channel_id = $this->getRequest()->getParam('channel_id');
            $product_info_table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_history');
            $history_query = 'SELECT * FROM ' . $product_info_table . ' WHERE sku="' . $sku . '" order by created desc';
            //echo $history_query;exit;
            $history_link = 'adminhtml/edi/historygrid/sku/' . $sku . '/channel_type/' . $channel_type;
            $history_link = Mage::helper('bridge')->getUrl($history_link);
            $res = $readConnection->fetchAll($history_query);
            $block = $this->getLayout()->createBlock('core/template')
                            ->setProduct($res)
                            ->setChanneltype($channel_type)
                            ->setChannelId($channel_id)
                            ->setSku($sku)
                            ->setGridurl($history_link)
                            ->setTemplate('fedobe/bridge/history_tab.phtml')->toHtml();
            echo $block;
            exit;
        }
    }

}