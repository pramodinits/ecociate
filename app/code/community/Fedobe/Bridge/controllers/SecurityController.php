<?php

class Fedobe_Bridge_SecurityController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        $data = $this->getRequest()->getParams();
        //checking the paid module exist or not
        if (Mage::helper('core')->isModuleEnabled('Fedobe_Securitycheck'))
            return true;
        else {
            //check the salt for validation
            if ($data['salt'] == $data['post_salt']) {
                return true;
            }
        }
        return false;
    }
}
