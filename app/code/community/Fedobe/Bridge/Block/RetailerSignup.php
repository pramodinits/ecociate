<?php
class Fedobe_Bridge_Block_Retailersignup extends Mage_Core_Block_Template {
//check a customer is logged in or not  if not then redirect to logged in page
    public function check_login() {
        if (!$this->helper('customer')->isLoggedIn()):
            Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::helper('core/url')->getCurrentUrl());
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl('customer/account/login'));
        endif;
    }
//check api config is exist for the logged in customer if exist then return the config data
    public function getapiuser() {
        $fedobe_apiuser = Mage::getModel('exportconnector/apiuser');
        $fedobe_apiuser->loadByCustomerId(Mage::getSingleton('customer/session')->getId());
        return $fedobe_apiuser;
    }

}
