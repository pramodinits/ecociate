<?php
class Fedobe_Bridge_Block_Adminhtml_Outgoingchannels extends Mage_Adminhtml_Block_Widget_Grid_Container
{
   protected $_addButtonLabel = 'Add New Outgoing Channel';
 
    public function __construct()
    {
        parent::__construct();
        $this->_controller = 'adminhtml_outgoingchannels';
        $this->_blockGroup = 'bridge';
        $this->_headerText = Mage::helper('bridge')->__('Manage Outgoing Channels');
    }
    protected function _prepareLayout()
   {
       $this->setChild( 'grid',
           $this->getLayout()->createBlock( $this->_blockGroup.'/' . $this->_controller . '_grid',
           $this->_controller . '.grid')->setSaveParametersInSession(true) );
       return parent::_prepareLayout();
   }
}