<?php

/*
 * My Render Extension
 */

class Fedobe_Bridge_Block_Adminhtml_Render_StatusType extends
Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $channel_id = $row->getData('bridge_channel_id');
        $model = Mage::getModel('bridge/incomingchannels');
        $model->load($channel_id);
        $channel_type = $model->getChannelType();
        $helperobj = Mage::helper($channel_type);
        $method = "getStatusType";
        if (method_exists($helperobj, $method)) {
            $errorcodes = $helperobj->getStatusType();
        }
        $status = $row->getData('status_code');
        return $errorcodes[$status];
    }
}