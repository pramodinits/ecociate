<?php

/*
 * My Render Extension
 */

class Fedobe_Bridge_Block_Adminhtml_Render_Skustatus extends
Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $status = '';
        $channel_id = $row->getData('bridge_channel_id');
        $model = Mage::getModel('bridge/incomingchannels');
        $model->load($channel_id);
        $channel_type = $model->getChannelType();
        $status = $row->getData('status_code');
        $status_code = Mage::helper('status')->getChannelStatusCode($channel_type);
        if (!is_null($status))
            $status_str = "<div title='" . $status_code[$status] . "' style='cursor:pointer;'>" . $status . "</div>";
        else
            $status_str = $status;
        return $status_str;
    }
}