<?php
/*
 * My Render Extension
 */
class Fedobe_Bridge_Block_Adminhtml_Render_Status extends
    Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        if(!($row->getData($this->getColumn()->getIndex()))) {
            $data = 'No';
        } else {
            $data = 'Yes';
        }
        return $data;
    }
}