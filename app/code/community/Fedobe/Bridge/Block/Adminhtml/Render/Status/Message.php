<?php

/*
 * My Render Extension
 */

class Fedobe_Bridge_Block_Adminhtml_Render_Status_Message extends
Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $channel_id = Mage::app()->getRequest()->getParam('id');
        $channel_type = Mage::getmodel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('id', $channel_id)
                ->addFieldToSelect('channel_type')
                ->getFirstItem()
                ->getChannelType();
        $helper_class = Mage::getConfig()->getHelperClassName($channel_type);
        if (class_exists($helper_class)) {
            $helper = Mage::helper($channel_type);
            if (is_object($helper) && method_exists($helper, 'getErrorCodeList')) {
                $current_status_code = $row->getData('status_code');
                $status_code = $helper->getErrorCodeList();
                return $status_code[$current_status_code];
            }
        }
        return '';
    }

}
