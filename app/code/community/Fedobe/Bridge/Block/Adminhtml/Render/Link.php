<?php
class Fedobe_Bridge_Block_Adminhtml_Render_Link extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $obj = new Fedobe_Bridge_Block_Adminhtml_Outgoingchannels_Grid();
        $data = $obj->getRowUrl($row);
        return "<a href='$data'>Edit</a>";
    }

}
