<?php
/*
 * My Render Extension
 */
class Fedobe_Bridge_Block_Adminhtml_Render_Productcount extends
    Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $channel_id = $row->getData('id');
        $channel_type = $row->getData('channel_type');
        $_productCollection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter('bridge_channel_id', $channel_id)->load();
        $_productCollection_content = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter('bridge_channel_id', $channel_id)
                ->addAttributeToFilter('bridge_content_edited', 0)
                ->load();
        $amazon_m = $_productCollection->getSize();
        $content_edit = $_productCollection_content->getSize();
        if($content_edit)
            $count_str = $amazon_m."<div style='color:green;cursor:pointer;float:right' title='To be content edited' >+".$content_edit."</div>";
        else
            $count_str = $amazon_m;

        return $amazon_m?$count_str:'0';
       
    }
}