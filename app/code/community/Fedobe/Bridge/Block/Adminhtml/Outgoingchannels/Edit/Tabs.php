<?php
/**
 * @category    AM
 * @package     AM_RevSlider
 * @copyright   Copyright (C) 2008-2014 ArexMage.com. All Rights Reserved.
 * @license     GNU General Public License version 2 or later
 * @author      ArexMage.com
 * @email       support@arexmage.com
 */

class Fedobe_Bridge_Block_Adminhtml_Outgoingchannels_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs{
    public function __construct(){
        parent::__construct();
        $this->setDestElementId('edit_form');
        $this->setId('outgoingchannels');
        if ($tab = $this->getRequest()->getParam('activeTab')){
            $this->_activeTab = $tab;
        }else if(!@$this->getRequest()->getParam('id') && !@$this->getRequest()->getParam('channel_type')){
            $this->_activeTab = 'settings_section';
        }else{
            $this->_activeTab = 'main_section';
        }
        $this->setTitle(Mage::helper('bridge')->__('Outgoing Channel'));
    }

    public function _prepareLayout(){
        if (!@$this->getRequest()->getParam('id') && !@$this->getRequest()->getParam('channel_type')){
            
        }
        return parent::_prepareLayout();
    }

    /**
     * Add new tab after another
     *
     * @param   string $tabId new tab Id
     * @param   array|Varien_Object $tab
     * @param   string $afterTabId
     * @return  Mage_Adminhtml_Block_Widget_Tabs
     */
    public function addTabAfter($tabId, $tab, $afterTabId){
        $this->addTab($tabId, $tab);
        $this->_tabs[$tabId]->setAfter($afterTabId);
    }
}
