<?php

class Fedobe_Bridge_Block_Adminhtml_Outgoingchannels_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'bridge';
        $this->_controller = 'adminhtml_outgoingchannels';
        $this->_mode = 'edit';
        $this->_addButton('save_and_continue', array(
            'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick' => 'saveAndContinueEdit();',
            'class' => 'save',
                ), -100);

        $this->_updateButton('save', 'label', Mage::helper('bridge')->__('Save Channel'));


        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
 public function getDeleteUrl()
    {
        return $this->getUrl('*/outgoingchannels/delete', array($this->_objectId => $this->getRequest()->getParam($this->_objectId)));
    }
    public function _prepareLayout() {
        if (!@$this->getRequest()->getParam('id') && !@$this->getRequest()->getParam('channel_type')) {
            $this->_removeButton('save_and_continue');
        }
        return parent::_prepareLayout();
    }

    public function getHeaderText() {
        $channel_name = Mage::helper('bridge')->getOutgoingAddons();
        $currentchannel = $this->getRequest()->getParam('channel_type');
        $current_channel_name = @$channel_name[$currentchannel];
        $current_channel_name = ($current_channel_name) ? "($current_channel_name)" : "";
        if (Mage::registry('channel_condition') && Mage::registry('channel_condition')->getId()) {
            return Mage::helper('bridge')->__('Edit Outgoingchannel for "%s %s"', $this->htmlEscape(Mage::registry('channel_condition')->getName()), $current_channel_name);
        } else {
            return Mage::helper('bridge')->__("New Outgoingchannel $current_channel_name");
        }
    }

}
