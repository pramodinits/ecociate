<?php

class Fedobe_Bridge_Block_Adminhtml_Outgoingchannels_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('outgoingchannels_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('bridge/outgoingchannels')->getCollection();
        $collection->addFieldToFilter('is_hidden',0);
        if (Mage::getmodel('bridge/outgoingchannels')->isRetailer(Mage::getSingleton('admin/Session')->getUser()->getUserId())) {
            $collection->addFieldToFilter('main_table.admin_user_id', Mage::getSingleton('admin/session')->getUser()->getUserId());
        }
        $qust = $collection->getSelect()->joinLeft('admin_user', 'main_table.admin_user_id = admin_user.user_id', array('admin_user.firstname'));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('id', array(
            'header' => Mage::helper('bridge')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'id'
        ));
        $this->addColumn('name', array(
            'header' => Mage::helper('bridge')->__('Channel Name'),
            'align' => 'left',
            'index' => 'name'
        ));
        $this->addColumn('url', array(
            'header' => Mage::helper('bridge')->__('URL'),
            'align' => 'left',
            'index' => 'url'
        ));
        $this->addColumn('firstname', array(
            'header' => Mage::helper('bridge')->__('Channel User'),
            'align' => 'left',
            'index' => 'firstname'
        ));
//        $this->addColumn('secret', array(
//            'header' => Mage::helper('bridge')->__('Secret'),
//            'align' => 'left',
//            'index' => 'secret'
//        ));
        $this->addColumn('channel_type', array(
            'header' => Mage::helper('bridge')->__('Channel Type'),
            'align' => 'left',
            'width' => '50px',
            'index' => 'channel_type'
        ));
        if ($this->isAdminstrator()) {
            $this->addColumn('channel_status', array(
                'header' => Mage::helper('bridge')->__('Status'),
                'align' => 'left',
                'index' => 'channel_status',
                'renderer' => 'Fedobe_Bridge_Block_Adminhtml_Render_Status'
            ));
        }
        //$link changed
        //$link = $this->getChanneleditUrl($row);//Mage::helper('bridge')->getUrl("adminhtml/$channel_type/edit/") . 'id/$id';
        $this->addColumn('action_edit', array(
            'header' => $this->helper('bridge')->__('Action'),
            'width' => 15,
            'sortable' => false,
            'filter' => false,
            'type' => 'action',
            'actions' => array(
                array(
                    'caption' => $this->helper('bridge')->__('Edit'),
                ),
            ),
            'renderer'  => 'Fedobe_Bridge_Block_Adminhtml_Render_Link'
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        $channel_type = $row->getChannelType();
        $channel_type = ($channel_type =='magento') ?'outgoingchannels' :  $channel_type;
        return Mage::helper('bridge')->getUrl("adminhtml/$channel_type/edit/") . 'id/'.$row->getId();
//        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    public function getChanneleditUrl($row){
        return Mage::helper('bridge')->getUrl("adminhtml/".$row->getChannelType()."/edit/") . 'id/'.$row->getId();
    }
    /**
     * check the logged in user is retailer or not
     */
    public function isAdminstrator() {
        return Mage::getmodel('bridge/outgoingchannels')->isAdminstrator(Mage::getSingleton('admin/session')->getUser()->getId());
    }

}
