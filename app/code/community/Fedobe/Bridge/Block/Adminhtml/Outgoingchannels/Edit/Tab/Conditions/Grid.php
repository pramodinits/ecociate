<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Coupon codes grid
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Fedobe_Bridge_Block_Adminhtml_Outgoingchannels_Edit_Tab_Conditions_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    //public $_defaultLimit = 300000;
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->setId('condProductListGrid');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);
        $this->setDefaultFilter(array('sku_list1' => ''));
    }

    /**
     * Add filter
     *
     * @param object $column
     * @return Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Upsell
     */
    protected function _addColumnFilterToCollection($column) {
        // Set custom filter for in product flag
        if ($column->getId() == 'sku_list1') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in' => $productIds));
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', array('nin' => $productIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * Prepare collection for grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    public function _prepareCollection() {
        $skus = array(0);
        $collection = Mage::getModel('catalog/product')->getCollection();
        $coresession = Mage::getSingleton('core/session')->getData();
        if (isset($coresession['channel_condition_skus'])) {
            $skus = Mage::getSingleton('core/session')->getChannelConditionSkus();
            $collection->addAttributeToFilter('entity_id', array('in' => $skus));
        } else if (@$this->getRequest()->getParam('id')) {
            $skus = Mage::getmodel('bridge/rule')->getMatchingProductIds();
            if ($skus && Mage::registry('channel_condition') && Mage::registry('channel_condition')->getProductStatus()) {
                $skus = Mage::getmodel('bridge/outgoingchannels')->getFilteredSkus($skus, Mage::registry('channel_condition')->getProductStatus());
            }
            Mage::getSingleton('core/session')->setData('channel_condition_skus', $skus);
            $collection->addAttributeToFilter('entity_id', array('in' => $skus));
        }


        $collection->addAttributeToSelect('sku')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('attribute_set_id')
                ->addAttributeToSelect('type_id')
                ->addAttributeToSelect('price');

        if (Mage::helper('bridge')->isModuleEnabled('Mage_CatalogInventory')) {
            $collection->joinField('qty', 'cataloginventory/stock_item', 'qty', 'product_id=entity_id', '{{table}}.stock_id=1', 'left');
        }
        $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Define grid columns
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns() {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        $store = Mage::app()->getStore($storeId);
        $bridge_channel_id = $ogc_id = $this->getRequest()->getParam('id');
        $data = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter('bridge_channel_id', $bridge_channel_id)
                ->getData();
        $attribute_set_id = array_column($data, 'attribute_set_id');
        $attributes = Mage::getModel('catalog/product_attribute_api')->items($attribute_set_id[0]);

        if (!$this->_isExport) {
            //$this->$_defaultLimit = 10000;
            $this->addColumn('sku_list1', array(
                'header_css_class' => 'a-center',
                'type' => 'checkbox',
                'name' => 'sku_list',
                'values' => $this->_getSelectedProducts(),
                'align' => 'center',
                'index' => 'entity_id'
            ));
        }
        $this->addColumn('name', array(
            'header' => Mage::helper('bridge')->__('Name'),
            'index' => 'name',
            'width' => '200px',
        ));
        $this->addColumn('type', array(
            'header' => Mage::helper('bridge')->__('Type'),
            'width' => '60px',
            'index' => 'type_id',
            'type' => 'options',
            'options' => Mage::getSingleton('catalog/product_type')->getOptionArray(),
        ));

        $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
                ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
                ->load()
                ->toOptionHash();

        $this->addColumn('set_name', array(
            'header' => Mage::helper('bridge')->__('Attrib. Set Name'),
            'width' => '100px',
            'index' => 'attribute_set_id',
            'type' => 'options',
            'options' => $sets,
        ));
        $this->addColumn('sku', array(
            'header' => Mage::helper('bridge')->__('SKU'),
            'width' => '80px',
            'index' => 'sku',
        ));
        if (Mage::getmodel('bridge/outgoingchannels')->isAdminstrator(Mage::getSingleton('admin/Session')->getUser()->getUserId())) {
            $this->addColumn('price', array(
                'header' => Mage::helper('bridge')->__('Price'),
                'index' => 'price',
                'type' => 'price',
                'currency_code' => $store->getBaseCurrency()->getCode(),
            ));
        }

        if (Mage::helper('bridge')->isModuleEnabled('Mage_CatalogInventory')) {
            $this->addColumn('qty', array(
                'header' => Mage::helper('bridge')->__('Qty'),
                'width' => '100px',
                'type' => 'number',
                'index' => 'qty',
            ));
        }
        $this->addColumn('status', array(
            'header' => Mage::helper('bridge')->__('Status'),
            'width' => '70px',
            'index' => 'status',
            'type' => 'options',
            'options' => Mage::getSingleton('catalog/product_status')->getOptionArray(),
        ));
        if (!$this->_isExport) {
            $this->addColumn('position', array(
                'header' => Mage::helper('catalog')->__('Position'),
                'name' => 'position',
                'width' => 60,
                'type' => 'number',
                'validate_class' => 'validate-number',
                'index' => 'position',
                'editable' => true,
                'edit_only' => true
            ));

            $this->addColumn('action_edit', array(
                'header' => $this->helper('bridge')->__('Action'),
                'width' => '100px',
                'sortable' => false,
                'filter' => false,
                'type' => 'action',
                'renderer' => 'Fedobe_Bridge_Block_Adminhtml_Outgoingchannels_Managebridgecontent',
            ));
        }

        $this->addExportType('*/outgoingchannels/exportCsv', Mage::helper('bridge')->__('CSV'));
        $this->addExportType('*/outgoingchannels/exportExcel', Mage::helper('bridge')->__('Excel XML'));
        return parent::_prepareColumns();
    }

    /**
     * Get grid url
     *
     * @return string
     */
    public function getGridUrl() {
        return $this->getUrl('*/*/conditionsGrid', array('_current' => true));
    }

    public function getRowUrl($row) {
        return "javascript:void(0);";
    }

    protected function _getSelectedProducts() {
        $productids = $this->getSkuList();
        if (!is_array($productids)) {
            $productids = array_keys($this->getSelectedProductIds());
        }
        return $productids;
    }

    public function getSelectedProductIds() {
        $id = $this->getRequest()->getParam('id');
        $encodedIds = array();
        if ($id) {
            $otgchannel = Mage::getModel('bridge/outgoingchannels');
            $otgchannel->load($id);
            $product_ids = $otgchannel->getSkuList();
            $product_ids = explode(',', $product_ids);
            foreach ($product_ids as $product_id) {
                $encodedIds[$product_id]['position'] = "0";
            }
        }
        return $encodedIds;
    }

    public function getCollection() {
        return parent::getCollection();
    }

    public function _afterLoadCollection() {
        return parent::_afterLoadCollection();
    }

    public function _prepareGrid() {
        return parent::_prepareGrid();
    }

    public function callback_attribute($value, $row, $column, $isExport) {
        $entity_id = $row->getData('entity_id');
        $attribute = $column->getIndex();
        $product = Mage::getModel('catalog/product')->load($entity_id);
        return $product->getAttributeText($attribute);
    }

}