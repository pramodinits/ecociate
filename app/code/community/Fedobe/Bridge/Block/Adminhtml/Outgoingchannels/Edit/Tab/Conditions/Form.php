<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Coupons generation parameters form
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Fedobe_Bridge_Block_Adminhtml_Outgoingchannels_Edit_Tab_Conditions_Form extends Mage_Adminhtml_Block_Widget_Form {

    /**
     * Prepare coupon codes generation parameters form
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm() {
        $model = Mage::getModel('bridge/rule');
        $channel_condition = @Mage::registry('channel_condition') ? Mage::registry('channel_condition') : Mage::getmodel('bridge/outgoingchannels');
        $default_settings = Mage::registry('default_settings');
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('rule_');
        $form->setFieldNameSuffix('bridge');
        $renderer = Mage::getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
                ->setTemplate('promo/fieldset.phtml')
                ->setNewChildUrl($this->getUrl('*/promo_quote/newConditionHtml/form/rule_conditions_fieldset'));

        $fieldset = $form->addFieldset('conditions_fieldset', array(
                    'legend' => Mage::helper('bridge')->__('Apply the rule only if the following conditions are met (leave blank for all products)')
                ))->setRenderer($renderer);

        $fieldset->addField('conditions', 'text', array(
            'name' => 'conditions',
            'label' => Mage::helper('catalogrule')->__('Conditions'),
            'title' => Mage::helper('catalogrule')->__('Conditions'),
        ))->setRule($model)->setRenderer(Mage::getBlockSingleton('rule/conditions'));

        $fieldset->addField('product_status', 'select', array(
            'label' => Mage::helper('bridge')->__('Product Status'),
            'name' => 'product_status',
            'title' => Mage::helper('bridge')->__('Product Status'),
            'options' => array(0 => Mage::helper('bridge')->__('Select'), 1 => Mage::helper('bridge')->__('Active'), 2 => Mage::helper('bridge')->__('Inactive')),
        ));

        $gridBlock = Mage::getBlockSingleton('fedobe_bridge_block_adminhtml_outgoingchannels_edit_tab_conditions_grid');
        $gridBlockJsObject = '';
        if ($gridBlock)
            $gridBlockJsObject = $gridBlock->getJsObjectName();
        $idPrefix = $form->getHtmlIdPrefix();
        $generateUrl = $this->getGenerateUrl();
        $fieldset->addField('generate_button', 'note', array(
            'text' => $this->getButtonHtml(
                    Mage::helper('bridge')->__('Generate'), "getProductList('{$idPrefix}' ,'{$generateUrl}','{$gridBlockJsObject}')", 'generate conditionbtn'
            ),
                            'after_element_html' => "<div>".count(explode(',',trim($channel_condition->getSkuList(),'on,')))." Skus selected</div>",
        ));
        
        //Add a text field for the current and saved skulist show on 4th March 2016
        $fieldset->addField('sku_list_hidden', 'hidden', array(
            'name' => 'sku_list_hidden',
            'label' => Mage::helper('bridge')->__('Skulist'),
            'title' => Mage::helper('bridge')->__('Skulist'),
            'value' => $channel_condition->getSkuList(),
        ));
                    
        $form->setValues($this->getValues());

        $this->setForm($form);

        return $this;
    }

    /**
     * Retrieve URL to Generate Action
     *
     * @return string
     */
    public function getGenerateUrl() {
        return $this->getUrl('adminhtml/outgoingchannels/generate');
    }

    public function getValues() {
        if (Mage::registry('channel_condition')) {
            $outgoing_conditions = Mage::registry('channel_condition');
            $outgoing_conditions_data = $outgoing_conditions->getData();
            $outgoing_conditions_data['sku_list_hidden'] = $outgoing_conditions_data['sku_list'];
            return $outgoing_conditions_data;
        }
        return array();
    }

}
