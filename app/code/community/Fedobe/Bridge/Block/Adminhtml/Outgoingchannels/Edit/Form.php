<?php

class Fedobe_Bridge_Block_Adminhtml_Outgoingchannels_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    public function __construct() {
        parent::__construct();
        $this->setId('bridge_outgoingchannel_form');
//        $this->setTitle(Mage::helper('bridge')->__('Rule Information'));
    }

    protected function _prepareForm() {
//        echo  $this->getData('action');exit;
        $form = new Varien_Data_Form(array('id' => 'edit_form', 'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))), 'method' => 'post'));
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }

}
