<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * description
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Fedobe_Bridge_Block_Adminhtml_Outgoingchannels_Edit_Tab_Settings extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel() {
        return Mage::helper('bridge')->__('Settings');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle() {
        return Mage::helper('bridge')->__('Settings');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab() {
        if (!@$this->getRequest()->getParam('id') && !@$this->getRequest()->getParam('channel_type'))
            return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden() {
        if (@$this->getRequest()->getParam('id') || @$this->getRequest()->getParam('channel_type'))
            return false;
    }

    protected function _prepareLayout() {
        return parent::_prepareLayout();
    }

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('settings', array('legend' => Mage::helper('bridge')->__('Create Channel Settings')));
        $child = Mage::helper('bridge')->getOutgoingAddons();
        $fieldset->addField('channel_type', 'select', array(
            'label' => Mage::helper('bridge')->__('Channel Type'),
            'title' => Mage::helper('bridge')->__('Channel Type'),
            'name' => 'channel_type',
            'value' => '',
            'values' => $child
        ));
        $fieldset->addField('continue_button', 'note', array(
            'text' => $this->getButtonHtml(
                    Mage::helper('bridge')->__('Continue'), "setSettings('" . $this->getContinueUrl() . "')", 'generate'
            )
        ));

        $this->setForm($form);
    }

    public function getContinueUrl() {
        return $this->getUrl('*/*/checkaddon', array(
                    '_current' => true
        ));
    }

}
