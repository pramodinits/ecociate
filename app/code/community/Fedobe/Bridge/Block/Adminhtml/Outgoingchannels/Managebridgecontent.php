<?php

class Fedobe_Bridge_Block_Adminhtml_Outgoingchannels_Managebridgecontent extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public $_product;

    public function render(Varien_Object $row) {
        $data = $row->getData();
        $link = $this->getUrl(
                'adminhtml/managebridgecontent/index', array(
            'product' => $data['entity_id'],
            'channel_type' => 'outgoing',
            'set' => $data['attribute_set_id'],
            'type' => $data['type_id'],
            'popup' => 1
                )
        );
        $id = $data['entity_id'];
        $sku = $data['sku'];
        $product = Mage::getModel('catalog/product')->setStoreId(0)->load($data['entity_id']);
        $visible = $product->isVisibleInCatalog() && $product->isVisibleInSiteVisibility();
        //$color = ($visible)?'':'#A9A9A9';
        $view_link = $this->__getFrontendProductUrl($product);
        $edit_link = 'adminhtml/catalog_product/edit/id/' . $data['entity_id'];
        $edit_link = Mage::helper('bridge')->getUrl($edit_link);
        $arr = '  <a href="' . $link . '" target="' . $id . $sku . '">Manage</a>';
        $arr .= '  <a href="' . $edit_link . '" target="_blank">Edit</a>';
        if ($visible)
            $arr .= '  <a href="' . $view_link . '" target="' . $id . '">View</a>' . "&nbsp;";
        return $arr;
    }

    function __getFrontendProductUrl($product) {
        $this->_product = $product;
        $websites = Mage::app()->getWebsites();
        $stores = array();
        foreach ($websites as $wk => $wv) {
            $website_id = $wv->getId();
            break;
        }
        $website_id = ($this->getRequest()->getParam('website')) ? $this->getRequest()->getParam('website') : $website_id;
        $websites = Mage::getModel("core/website")->load($website_id);
        foreach ($websites->getStoreIds() as $sk => $sv) {
            $store_id = $sv;
            break;
        }
        $storeid = ($this->getRequest()->getParam('store')) ? $this->getRequest()->getParam('store') : $store_id;
        $use_store_code_in_url = Mage::getStoreConfig('web/url/use_store', $storeid);
        if ($use_store_code_in_url) {
            $store_code = Mage::getModel('core/store')->load($storeid)->getCode();
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $store_code . '/' . $this->_product->getUrlPath();
        } else {
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $this->_product->getUrlPath();
        }
    }

}
?>