<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * description
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Fedobe_Bridge_Block_Adminhtml_Outgoingchannels_Edit_Tab_Conditions extends Mage_Adminhtml_Block_Text_List implements Mage_Adminhtml_Block_Widget_Tab_Interface {
//class Fedobe_Bridge_Block_Adminhtml_Outgoingchannels_Edit_Tab_Conditions extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel() {
        return Mage::helper('bridge')->__('Conditions');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle() {
        return Mage::helper('bridge')->__('Conditions');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab() {
        if (($this->getRequest()->getParam('id') || @$this->getRequest()->getParam('channel_type')) && $this->conditionTabRestriction())
            return true;
    }

    public function conditionTabRestriction() {
        if (Mage::getmodel('bridge/outgoingchannels')->isAdminstrator(Mage::getSingleton('admin/Session')->getUser()->getUserId())) {
            return true;
        } else {
            $channel_condition = @Mage::registry('channel_condition') ? Mage::registry('channel_condition') : Mage::getmodel('bridge/outgoingchannels');
            $default_settings = @Mage::registry('default_settings') ? Mage::registry('default_settings') : array();
            if ($channel_condition->getId()) {
                if ($channel_condition->getAllowCondition() != "not_visible")
                    return true;
                else
                    return false;
            }else {
                if ($default_settings['ogc_allow_conditiontab'] != "not_visible")
                    return true;
                else
                    return false;
            }
        }
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden() {
        if ((!@$this->getRequest()->getParam('id') && !@$this->getRequest()->getParam('channel_type')) || !$this->conditionTabRestriction())
            return true;
    }

}
