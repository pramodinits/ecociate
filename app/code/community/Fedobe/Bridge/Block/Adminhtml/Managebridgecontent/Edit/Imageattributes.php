<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Product attribute add/edit form block
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Fedobe_Bridge_Block_Adminhtml_Managebridgecontent_Edit_Imageattributes extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        //Here let's fetch the product base image
        $product_id = $this->getRequest()->getParam('product');
        $store_id = $this->getRequest()->getParam('store');
        $product = Mage::getModel('catalog/product')->setStoreId($store_id)->load($product_id);
        $small_width = 150;
        $small_height = 150;
        $product_small_image_url = (String) Mage::helper('catalog/image')->init($product, 'image')->resize($small_width, $small_height);
        $org_imageUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage();

        $form = new Varien_Data_Form(array('id' => 'manage_bridge_form', 'action' => $this->getData('action'), 'method' => 'post'));
        $form->setUseContainer(false);
        $form->setFieldsetRenderer($this->getLayout()->createBlock('bridge/adminhtml_managebridgecontent_form_renderer_fieldset'));
        $fieldset = $form->addFieldset('bridge_product_image', array(
            'legend' => $this->__('Image'),
            'fieldset_container_id' => 'bridge_product_image'
        ));
        $bridgejs = Mage::helper('adminhtml/js')->getScript(
                "function viewproductimage(){
                var win = window.open('" . $org_imageUrl . "', 'view_bridge_product_iamge','width=800,height=600,resizable=1,scrollbars=1');
                win.focus();
        }\n"
        );
        $imagedata = "<img src='$product_small_image_url' width='$small_width' height='$small_height'/><br/><a href='javascript:void(0);' onclick='viewproductimage();'>View Original</a>" . $bridgejs;
        $fieldset->addField('bridge_name_own', 'note', array(
            'text' => $imagedata
        ));

        $fieldset_shortdesc = $form->addFieldset('bridge_product_attribute_details', array(
            'legend' => $this->__('Attributes'),
            'fieldset_container_id' => 'bridge_product_attribute_details'
        ));



        //Here let's collect all product attributes
        $attributedat_table = "<table style='width:100%;'><thead><tr><th>Label</th><th>Value</th></tr></thead><tbody>";
        $attributes = $product->getAttributes();
        foreach ($attributes as $attribute) {
            $attributeLabel = $attribute->getFrontendLabel();
            $value = $attribute->getFrontend()->getValue($product);
            if ($attribute->getIsVisibleOnFront()) {
                $attributedat_table .= "<tr>"
                        . "<td>$attributeLabel</td>"
                        . "<td>" . (($value) ? $value : 'N/A') . "</td>"
                        . "</tr>";
            }
        }
        $attributedat_table .= "</tbody></table>";
        $fieldset_shortdesc->addField('bridge_shortdescription_own', 'note', array(
            'text' => $attributedat_table
        ));


        if ($product->getSku() && $product->getBridgeChannelId()) {
            $attributes_am = Mage::helper('bridge')->getChannelTypeAttributes($product->getSku(),$product->getBridgeChannelId());

            if (!empty($attributes_am)) {
                $fieldset_shortdesc_am = $form->addFieldset('bridge_product_amazon_attribute_details', array(
                    'legend' => $this->__('Amazon Attributes'),
                    'fieldset_container_id' => 'bridge_product_amazon_attribute_details'
                ));
                $attributedat_table_am = "<table style='width:100%;'><thead><tr><th>Label</th><th>Value</th></tr></thead><tbody>";
                foreach ($attributes_am as $attribute) {
                    $attributeLabel = $attribute['attribute_id'];
                    $value = $attribute['value'];
                    $attributedat_table_am .= "<tr>"
                            . "<td>$attributeLabel</td>"
                            . "<td>" . (($value) ? $value : 'N/A') . "</td>"
                            . "</tr>";
                }
                $attributedat_table_am .= "</tbody></table>";
                $fieldset_shortdesc_am->addField('bridge_shortdescription_am', 'note', array(
                    'text' => $attributedat_table_am
                ));
            }
        }
        $this->setForm($form);
        return parent::_prepareForm();
    }

}