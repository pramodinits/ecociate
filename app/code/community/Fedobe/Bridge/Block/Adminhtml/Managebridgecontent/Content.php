<?php

class Fedobe_Bridge_Block_Adminhtml_Managebridgecontent_Content extends Mage_Adminhtml_Block_Widget_Form_Container  {

    public function __construct() {
        $this->_objectId = 'product_id';
        $this->_controller = 'managebridgecontent';
        parent::__construct();
        $entity_id = $this->getRequest()->getParam('product');
        $this->_product = Mage::getModel('catalog/product')->setStoreId(0)->load($entity_id);
        
        if($this->getRequest()->getParam('popup')) {
            $this->_removeButton('back');
            $this->_addButton(
                'view',
                array(
                    'onclick'   => 'window.open(\''.$this->__getFrontendProductUrl().'\','.$entity_id.')',
                    'disabled'  => !$this->_isVisible(),
                    'label' => (!$this->_isVisible())?
                    Mage::helper('catalog')->__('Product is not visible on frontend'):
                    Mage::helper('catalog')->__('View Product Page'),
                    'level'     => 0,
                )
            );
            $this->_addButton(
                'close',
                array(
                    'label'     => Mage::helper('catalog')->__('Close Window'),
                    'class'     => 'cancel',
                    'onclick'   => 'window.close()',
                    'level'     => -1
                )
            );
            $this->_addButton('save', array(
            'label'     => Mage::helper('adminhtml')->__('Save'),
            'onclick'   => 'manageBridgeForm.submit();',
            'class'     => 'save',
        ), 1);
        }
        $this->setTemplate('fedobe/bridge/managebridgecontent/content.phtml');
    }
    
    public function getHeaderText()
    {
            return Mage::helper('catalog')->__('Manage Bridge Content');
    }
    
    protected function _prepareLayout()
    {
        $this->setChild('form', $this->getLayout()->createBlock('bridge/adminhtml_managebridgecontent_edit_form'));
        return parent::_prepareLayout();
    }
    

    public function getSaveUrl()
    {
        return $this->getUrl('*/'.$this->_controller.'/save', array('_current'=>true, 'back'=>null));
    }
    private function _isVisible()
    {
        return $this->_product->isVisibleInCatalog() && $this->_product->isVisibleInSiteVisibility();
    }
    
    private function __getFrontendProductUrl(){
        $websites = Mage::app()->getWebsites();
        $stores = array();
        foreach ($websites as $wk => $wv) {
                $website_id = $wv->getId();
                break;
        }
        $website_id = ($this->getRequest()->getParam('website')) ? $this->getRequest()->getParam('website') : $website_id;
        $websites = Mage::getModel( "core/website" )->load($website_id);
        foreach ($websites->getStoreIds() as $sk => $sv) {
                $store_id = $sv;
                break;
        }
        $storeid = ($this->getRequest()->getParam('store')) ? $this->getRequest()->getParam('store') :$store_id;
        $use_store_code_in_url = Mage::getStoreConfig('web/url/use_store', $storeid);
        if($use_store_code_in_url){
            $store_code = Mage::getModel('core/store')->load($storeid)->getCode();
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).$store_code.'/'.$this->_product->getUrlPath();
        }else{
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).$this->_product->getUrlPath();
        }
    }
}
