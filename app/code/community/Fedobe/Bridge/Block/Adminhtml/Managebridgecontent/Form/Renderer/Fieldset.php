<?php
class Fedobe_Bridge_Block_Adminhtml_Managebridgecontent_Form_Renderer_Fieldset extends Mage_Adminhtml_Block_Widget_Form_Renderer_Fieldset implements Varien_Data_Form_Element_Renderer_Interface {

    protected function _construct() {
        $this->setTemplate('fedobe/bridge/managebridgecontent/fieldset.phtml');
    }

}
