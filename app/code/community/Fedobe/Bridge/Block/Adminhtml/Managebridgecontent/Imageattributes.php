<?php
class Fedobe_Bridge_Block_Adminhtml_Managebridgecontent_Imageattributes extends Mage_Adminhtml_Block_Widget_Form_Container  {
    public function __construct() {
        parent::__construct();
        $this->setTemplate('fedobe/bridge/managebridgecontent/image-attributes.phtml');
    }
    public function getHeaderText()
    {
            return Mage::helper('catalog')->__("Image and Attributes");
    }
    protected function _prepareLayout()
    {
        $this->setChild('form', $this->getLayout()->createBlock('bridge/adminhtml_managebridgecontent_edit_imageattributes'));
        return parent::_prepareLayout();
    }
}