<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Product attribute add/edit form block
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class Fedobe_Bridge_Block_Adminhtml_Managebridgecontent_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array('id' => 'manage_bridge_form', 'action' => $this->getData('action'), 'method' => 'post'));
        $form->setUseContainer(true);
        $form->setFieldsetRenderer($this->getLayout()->createBlock('bridge/adminhtml_managebridgecontent_form_renderer_fieldset'));
        $data = $this->getBridgeProductData();
        $fieldset = $form->addFieldset('bridge_name_information', array(
				'legend'=> $this->__('Product Name : '.$data['bridge_name_own']),
                                'fieldset_container_id'     => 'bridge_name_information'
			));
        $fieldset->addField('bridge_name_own', 'text', array(
                'name' 		=> "bridgename[own]",
                'label' 	=> $this->__('Own Store'),
                'title' 	=> $this->__('Own Store'),
                'required'	=> true,
                'class'		=> 'required-entry',
        ));
        $fieldset->addField('bridge_name_incoming', 'text', array(
                'name' 		=> "bridgename[incoming]",
                'label' 	=> $this->__('Incoming'),
                'title' 	=> $this->__('Incoming')
        ));
        $fieldset->addField('bridge_name_outgoing', 'text', array(
                'name' 		=> "bridgename[outgoing]",
                'label' 	=> $this->__('Outgoing'),
                'title' 	=> $this->__('Outgoing')
        ));
        $fieldset_shortdesc = $form->addFieldset('bridge_shortdescription_information', array(
				'legend'=> $this->__('Product Short Description'),
                                'fieldset_container_id'     => 'bridge_shortdescription_information'
			));
        $fieldset_shortdesc->addField('bridge_shortdescription_own', 'textarea', array(
                'name' 		=> "bridgeshortdescription[own]",
                'label' 	=> $this->__('Own Store'),
                'title' 	=> $this->__('Own Store'),
                'required'	=> true,
                'class'		=> 'required-entry',
        ));
        $fieldset_shortdesc->addField('bridge_shortdescription_incoming', 'textarea', array(
                'name' 		=> "bridgeshortdescription[incoming]",
                'label' 	=> $this->__('Incoming'),
                'title' 	=> $this->__('Incoming')
        ));
        $fieldset_shortdesc->addField('bridge_shortdescription_outgoing', 'textarea', array(
                'name' 		=> "bridgeshortdescription[outgoing]",
                'label' 	=> $this->__('Outgoing'),
                'title' 	=> $this->__('Outgoing')
        ));
        $fieldset_desc = $form->addFieldset('bridge_description_information', array(
				'legend'=> $this->__('Product Description'),
                                'fieldset_container_id'     => 'bridge_description_information'
			));
        $fieldset_desc->addField('bridge_description_own', 'textarea', array(
                'name' 		=> "bridgedescription[own]",
                'label' 	=> $this->__('Own Store'),
                'title' 	=> $this->__('Own Store'),
                'required'	=> true,
                'class'		=> 'required-entry',
        ));
        $fieldset_desc->addField('bridge_description_incoming', 'textarea', array(
                'name' 		=> "bridgedescription[incoming]",
                'label' 	=> $this->__('Incoming'),
                'title' 	=> $this->__('Incoming')
        ));
        $fieldset_desc->addField('bridge_description_outgoing', 'textarea', array(
                'name' 		=> "bridgedescription[outgoing]",
                'label' 	=> $this->__('Outgoing'),
                'title' 	=> $this->__('Outgoing')
        ));
        $this->setForm($form);
        $this->getForm()->setValues($this->getBridgeProductData());
        return parent::_prepareForm();
    }
    
    public function getBridgeProductData(){
        $product_id = $this->getRequest()->getParam('product');
        $store_id = $this->getRequest()->getParam('store');
        $product = Mage::getModel('catalog/product')->setStoreId($store_id)->load($product_id);
        $customformdata = array(
            'bridge_name_own' =>$product->getName(),
            'bridge_name_incoming' =>$product->getData('inc_name'),
            'bridge_name_outgoing' =>$product->getData('ogc_name'),
            'bridge_shortdescription_own' =>$product->getShortDescription(),
            'bridge_shortdescription_incoming' =>$product->getData('inc_short_description'),
            'bridge_shortdescription_outgoing' =>$product->getData('ogc_short_description'),
            'bridge_description_own' =>$product->getDescription(),
            'bridge_description_incoming' =>$product->getData('inc_description'),
            'bridge_description_outgoing' =>$product->getData('ogc_description'),
        );
        return $customformdata;
    }

}
