<?php

class Fedobe_Bridge_Block_Adminhtml_Managebridgecontent_Widget_Button extends Mage_Adminhtml_Block_Widget_Button
{
    /**
     * @var Mage_Catalog_Model_Product Product instance
     */
    private $_product;

    /**
     * Block construct, setting data for button, getting current product
     */
    protected function _construct()
    {
        $this->_product = Mage::registry('current_product');
        parent::_construct();
        $entity_id = $this->getRequest()->getParam('id');
        $this->setData(array(
            'label'     => Mage::helper('catalog')->__('View Product Page'),
            'onclick'   => 'window.open(\''.$this->__getFrontendProductUrl().'\','.$entity_id.')',
            'disabled'  => !$this->_isVisible(),
            'title' => (!$this->_isVisible())?
                Mage::helper('catalog')->__('Product is not visible on frontend'):
                Mage::helper('catalog')->__('View Product Page')
        ));
    }

    /**
     * Checking product visibility
     *
     * @return bool
     */
    private function _isVisible()
    {
        return $this->_product->isVisibleInCatalog() && $this->_product->isVisibleInSiteVisibility();
    }
    
    private function __getFrontendProductUrl(){
        $websites = Mage::app()->getWebsites();
        $stores = array();
        foreach ($websites as $wk => $wv) {
                $website_id = $wv->getId();
                break;
        }
        $website_id = ($this->getRequest()->getParam('website')) ? $this->getRequest()->getParam('website') : $website_id;
        $websites = Mage::getModel( "core/website" )->load($website_id);
        foreach ($websites->getStoreIds() as $sk => $sv) {
                $store_id = $sv;
                break;
        }
        $storeid = ($this->getRequest()->getParam('store')) ? $this->getRequest()->getParam('store') :$store_id;
        $use_store_code_in_url = Mage::getStoreConfig('web/url/use_store', $storeid);
        if($use_store_code_in_url){
            $store_code = Mage::getModel('core/store')->load($storeid)->getCode();
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).$store_code.'/'.$this->_product->getUrlPath();
        }else{
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).$this->_product->getUrlPath();
        }
    }
}