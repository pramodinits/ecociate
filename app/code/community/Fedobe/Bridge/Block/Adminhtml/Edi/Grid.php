<?php

class Fedobe_Bridge_Block_Adminhtml_Edi_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    protected $supplierList;
    protected $partner;
    protected $isAdmin;
    protected $productImage;
    protected $attrSets;
    protected $statusAttr;
    protected $nameAttr;
    protected $manufacturereAttr;
    protected $defaultStore;

    public function __construct() {
        parent::__construct();
        $this->setDefaultSort('sku');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
        $this->setVarNameFilter('edi_filter');
        $this->setId('ediGrid');
        $this->setUseAjax(true);
        $this->supplierList = Mage::helper('bridge')->getSupplierList();
        $this->partner = null;
        $this->manufacturereAttr = null;
        $this->isAdmin = true;

        $is_admin = Mage::helper('bridge')->isAdmin();
        if (!$is_admin) {
            $sellerid = Mage::helper('tribeseller')->getSellerIdFromadminUser();
            if ($sellerid) {
                //if logged in user is not adminstrator then he/she can only she own invneotries
                $sellerCollection = Mage::getModel('tribeseller/seller')->getCollection();
                $seller = $sellerCollection
                        ->addAttributeToFilter('entity_id', $sellerid)
                        ->addAttributeToSelect('seller_partner')
                        ->getFirstItem()
                        ->getSellerPartner();
                $this->partner = $seller;
            } else if ($this->isBrand()) {
                $brand_attr_code = Mage::helper('tribebrand')->getAttributeCode();
                $this->partner = Mage::helper('tribebrand')->getBrandOptionId();
            }
            $this->isAdmin = false;
        }
        $this->attrSets = Mage::getResourceModel('eav/entity_attribute_set_collection')
                ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
                ->load()
                ->toOptionHash();
        $this->statusAttr = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'status');
        $this->nameAttr = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'name');
        
        if (Mage::helper('core')->isModuleEnabled('Fedobe_Tribebrand')) {
            $this->manufacturereAttr = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', Mage::helper('tribebrand')->getAttributeCode());
        }else{
            $this->manufacturereAttr = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'manufacturer');
        }

        $this->defaultStore = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
    }

    protected function _prepareCollection() {

        $active_incomingchannel_ids = $this->getActiveChannelsForEDI();
        $active_incomingchannel_ids = $active_incomingchannel_ids ? implode(',', $active_incomingchannel_ids) : 0;
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection("core_write");
        $coresession = Mage::getSingleton('core/session')->getData();
        $productinfo_table_name = $resource->getTableName('bridge/incomingchannels_product');
        $status_AttrId = $this->statusAttr;
        $manufacturereAttr = $this->manufacturereAttr;
        $nameAttr = $this->nameAttr;
        $iDefaultStoreId = $this->defaultStore;
        $where = "sku IS NULL OR sku=''";
        $writeConnection->delete($productinfo_table_name, $where);
        $collection = Mage::getModel('bridge/incomingchannels_product')->getCollection()
                ->addFieldToFilter('deleted', 0)
                ->addFieldToFilter('is_processed', array('neq' => 4))
                ->addFieldToFilter('product_entity_id', array('notnull' => true));
        
        if (isset($coresession['edi_product_ids']))
            $collection->addFieldToFilter('product_entity_id', array('in' => $coresession['edi_product_ids']));
        $collection->getSelect()
                ->where("main_table.channel_type ='outgoing' OR main_table.channel_type IS NULL OR (main_table.channel_type='incoming' AND  main_table.channel_id IN({$active_incomingchannel_ids}))")
                ->columns(array('channel_name' => 'CONCAT(IFNULL(main_table.channel_type,"_"),main_table.channel_id)', 'main_table.channel_type' => 'IFNULL(main_table.channel_type,"ownstore")'))
                ->columns(array('channel_supplier' => 'IF(main_table.channel_type="outgoing","",main_table.channel_supplier)'))
                ->joinLeft('catalog_product_entity', 'main_table.product_entity_id=catalog_product_entity.entity_id', array('catalog_product_entity.attribute_set_id as attribute_set_id'))
                ->joinLeft('catalog_product_entity_int', "catalog_product_entity.entity_id=catalog_product_entity_int.entity_id AND catalog_product_entity_int.entity_type_id=4 AND catalog_product_entity_int.store_id=$iDefaultStoreId AND catalog_product_entity_int.attribute_id=$status_AttrId", array('catalog_product_entity_int.value as is_enabled'))
                ->joinLeft('catalog_product_entity_varchar', "catalog_product_entity.entity_id=catalog_product_entity_varchar.entity_id AND catalog_product_entity_varchar.entity_type_id=4 AND catalog_product_entity_varchar.store_id=$iDefaultStoreId AND catalog_product_entity_varchar.attribute_id=$nameAttr", array('catalog_product_entity_varchar.value as name'))
                ->joinLeft('bridge_incomingchannels', 'main_table.channel_id=bridge_incomingchannels.id AND main_table.channel_type="incoming"', array('bridge_incomingchannels.brand_name as channel'));
        
        //Here to show the manufaturer column in the grid        
        if($manufacturereAttr){
            $brand_attr_code = (Mage::helper('core')->isModuleEnabled('Fedobe_Tribebrand')) ? Mage::helper('tribebrand')->getAttributeCode() :'manufacturer';
            if ($this->isBrand()) {
                $own_channel_ids = ',1_,'.implode(',',Mage::getSingleton('bridge/incomingchannels')->getBrandChannels(Mage::helper('tribebrand')->getBrandSellerOptionId()));
            }  else {
                $own_channel_ids = ',0_,';
            }
            $collection->getSelect()->joinLeft(array("$brand_attr_code" => 'catalog_product_entity_int'), "catalog_product_entity.entity_id=$brand_attr_code.entity_id AND $brand_attr_code.entity_type_id=4 AND $brand_attr_code.store_id=$iDefaultStoreId AND $brand_attr_code.attribute_id=$manufacturereAttr", array("$brand_attr_code.value as $brand_attr_code","CONCAT(main_table.product_entity_id,'$own_channel_ids') as own_channel_ids"));
        }  
        
        if (!is_null($this->partner)) {
            $partner = $this->partner ? $this->partner : 0;
            if($this->isBrand()){
                $brand_attr_code = (Mage::helper('core')->isModuleEnabled('Fedobe_Tribebrand')) ? Mage::helper('tribebrand')->getAttributeCode() :'manufacturer';
                $collection->addFieldToFilter("$brand_attr_code.value", $partner);
                $collection->addFieldToFilter("main_table.channel_type", array('neq' => 'outgoing'));
            }else{
                $collection->addFieldToFilter('main_table.channel_supplier', $partner)
                    ->addFieldToFilter('channel_id', array('neq' => 0));
            }
        }
        
//        echo $collection->getSelect()->__toString();//exit;
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    public function isBrand() {
        $is_brand = false;
        if (Mage::helper('core')->isModuleEnabled('Fedobe_Tribebrand')) {
            $is_brand = Mage::helper('tribebrand')->getBrandSellerIdFromadminUser();
        }
        return $is_brand;
    }

    protected function _prepareColumns() {
//        $this->addColumn('id', array(
//            'header' => Mage::helper('bridge')->__('id'),
//            'align' => 'left',
//            'index' => 'id',
//            'width' => '200px',
//            'filter_index' => "main_table.id"
//        ));
        $this->addColumn('image', array(
            'header' => Mage::helper('bridge')->__('Image'),
            'index' => 'image',
            'width' => '50px',
            'frame_callback' => array($this, 'callback_image')
        ));
        $this->addColumn('sku', array(
            'header' => Mage::helper('bridge')->__('SKU'),
            'align' => 'left',
            'index' => 'sku',
            'width' => '200px',
            'filter_index' => "main_table.sku"
        ));
        $sets = $this->attrSets;

        $this->addColumn('set_name', array(
            'header' => Mage::helper('bridge')->__('Attrib. Set Name'),
            'width' => '100px',
            'index' => 'attribute_set_id',
            'type' => 'options',
            'options' => $sets,
        ));

        if ($this->isAdmin) {
            $brand_attr_code = Mage::helper('tribebrand')->getAttributeCode();
            $brand_attr_id = $this->manufacturereAttr;
            if ($brand_attr_id) {
                $manulist = Mage::helper('tribebrand')->getManufacturerList();
                $this->addColumn($brand_attr_code, array(
                    'header' => Mage::helper('bridge')->__('Manufacturer'),
                    'align' => 'left',
                    'index' => $brand_attr_code,
                    'type' => 'options',
                    'width' => '200px',
                    'options' => $manulist,
                    'filter_index' => "$brand_attr_code.value",
                ));
            }
        }

        $this->addColumn('name', array(
            'header' => Mage::helper('bridge')->__('Name'),
            'index' => 'name',
            'width' => '200px',
            'filter_index' => "catalog_product_entity_varchar.value"
                //'column_css_class' => 'nameinline',
//            'renderer'=>'Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Listingedit'
        ));
        if ($this->isAdmin) {
            $channel_ids = Mage::getSingleton('bridge/incomingchannels')->getAllChannelOptionArray();
            $get_supplier_list = Mage::helper('bridge')->getSupplierList();
        } else if($this->isBrand()){
            $info = Mage::getSingleton('bridge/incomingchannels')->getBrandChannelOptionArray($this->partner);
            $channel_ids = $info['channels'];
            $suppliers = $info['suppliers'];
            $get_supplier_list = Mage::helper('bridge')->getSupplierList();
            foreach ($get_supplier_list as $k => $v) {
                if(!in_array($k, $suppliers))
                        unset($get_supplier_list[$k]);
            }
        }else{
            $channel_ids = Mage::getSingleton('bridge/incomingchannels')->getSpPartnerChannelOptionArray($this->partner);
        }
        
        $this->addColumn('channel_name', array(
            'header' => Mage::helper('bridge')->__('Channel'),
            'align' => 'left',
            'index' => 'channel_name',
            'type' => 'options',
            'width' => '200px',
            'options' => $channel_ids,
            'filter_condition_callback' => array($this, '_processFilter_channel'),
        ));
        if ($this->isAdmin || $this->isBrand()) {
            $this->addColumn('channel_supplier', array(
                'header' => Mage::helper('bridge')->__('Partner'),
                'align' => 'left',
                'index' => 'channel_supplier',
                'type' => 'options',
                'width' => '200px',
                'options' => $get_supplier_list,
                'filter_index' => "main_table.channel_supplier",
                'renderer' => 'bridge/adminhtml_widget_grid_column_renderer_inline_supplier',
            ));
        }
        if (!$this->_isExport) {
            $this->addColumn('quantity', array(
                'header' => Mage::helper('bridge')->__('Quantity'),
                'index' => 'quantity',
                'align' => 'left',
                'type' => 'number',
                'width' => '150px',
                'renderer' => 'bridge/adminhtml_widget_grid_column_renderer_inline_quantity',
            ));
        } else {
            $this->addColumn('quantity', array(
                'header' => Mage::helper('bridge')->__('Quantity'),
                'index' => 'quantity',
                'align' => 'left',
                'type' => 'number',
                'width' => '150px'
            ));
        }
        $store = $this->_getStore();
        if ((!(@$this->helper('adminhtml')->prepareFilterString($this->getRequest()->getParam('filter'))['price']['currency']) || Mage::app()->getStore()->getBaseCurrencyCode() == $this->helper('adminhtml')->prepareFilterString($this->getRequest()->getParam('filter'))['price']['currency']) && !$this->_isExport) {
            $this->addColumn('price', array(
                'header' => Mage::helper('bridge')->__('Price'),
                'align' => 'left',
                'index' => 'price',
                'type' => 'price',
                'width' => '170px',
                'currency_code' => $store->getBaseCurrency()->getCode(),
                'renderer' => 'bridge/adminhtml_widget_grid_column_renderer_inline_price',
            ));
        } else {
            $this->addColumn('price', array(
                'header' => Mage::helper('bridge')->__('Price'),
                'align' => 'left',
                'index' => 'price',
                'type' => 'price',
                'width' => '170px',
                'currency_code' => $store->getBaseCurrency()->getCode(),
            ));
        }

        $this->addColumn('converted_price', array(
            'header' => Mage::helper('bridge')->__('Converted Price'),
            'align' => 'left',
            'index' => 'converted_price',
            'type' => 'price',
            'width' => '150px',
            'column_css_class' => 'converted_price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
        ));
        if ($this->isAdmin) {
            if ((!(@$this->helper('adminhtml')->prepareFilterString($this->getRequest()->getParam('filter'))['special_price']['currency']) || Mage::app()->getStore()->getBaseCurrencyCode() == $this->helper('adminhtml')->prepareFilterString($this->getRequest()->getParam('filter'))['special_price']['currency']) && !$this->_isExport) {
                $this->addColumn('special_price', array(
                    'header' => Mage::helper('bridge')->__('Special Price'),
                    'align' => 'left',
                    'index' => 'special_price',
                    'width' => '170px',
                    'type' => 'price',
                    'currency_code' => $store->getBaseCurrency()->getCode(),
                    'renderer' => 'bridge/adminhtml_widget_grid_column_renderer_inline_specialprice',
                ));
            } else {
                $this->addColumn('special_price', array(
                    'header' => Mage::helper('bridge')->__('Special Price'),
                    'align' => 'left',
                    'index' => 'special_price',
                    'width' => '170px',
                    'type' => 'price',
                    'currency_code' => $store->getBaseCurrency()->getCode(),
                ));
            }
        }
        $this->addColumn('is_processed', array(
            'header' => Mage::helper('bridge')->__('Status'),
            'align' => 'left',
            'index' => 'is_processed',
            'type' => 'options',
            'width' => '50px',
            'options' => array(-2 => 'New Out of stock', -1 => 'New', 1 => 'Synced', 0 => 'Changed', 2 => 'Processing', 3 => 'No Access'), //,4=>'Trashed'),
            'filter_condition_callback' => array($this, '_processFilter'),
            'renderer' => 'bridge/adminhtml_widget_grid_column_renderer_status',
        ));
        if ($this->isAdmin) {
            $this->addColumn('is_live', array(
                'header' => Mage::helper('bridge')->__('Live'),
                'align' => 'left',
                'index' => 'is_live',
                'type' => 'options',
                'width' => '50px',
                'options' => array(1 => 'Yes', 0 => 'No'),
            ));
            $this->addColumn('is_enabled', array(
                'header' => Mage::helper('bridge')->__('Enabled'),
                'align' => 'left',
                'index' => 'is_enabled',
                'type' => 'options',
                'width' => '50px',
                'options' => array(1 => 'Yes', 2 => 'No'),
                'filter_index' => "catalog_product_entity_int.value"
            ));
        }
        $this->addColumn('last_updated_time', array(
            'header' => Mage::helper('bridge')->__('Last Updated'),
            'align' => 'left',
            'width' => '120px',
            'type' => 'date',
            'default' => '--',
            'index' => 'last_updated_time',
            'renderer' => 'bridge/adminhtml_widget_grid_column_renderer_editime'
        ));
        if (!$this->_isExport) {
            $this->addColumn('action', array(
                'header' => $this->helper('bridge')->__('Action'),
                'width' => '100px',
                'sortable' => false,
                'filter' => false,
                'type' => 'action',
                'width' => '450px',
                'renderer' => $this->isAdmin ? 'Fedobe_Bridge_Block_Adminhtml_Incomingchannels_ManagebridgecontentBySku' : 'Fedobe_Bridge_Block_Adminhtml_Incomingchannels_ManagebridgecontentBySkuForSeller',
            ));
        }
        $this->addExportType('*/*/exportCsv', Mage::helper('bridge')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('bridge')->__('Excel XML'));
        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/ediGrid', array('_current' => true));
    }

    public function getRowUrl($row) {
        return "javascript:void(0);";
    }

    protected function _getStore() {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    protected function _processFilter($collection, $column) {
        $filterprocess = $column->getFilter()->getValue();
        if ($filterprocess == '') {
            return $this;
        }
        $this->getCollection()->addFieldToFilter('main_table.channel_type', array('notnull' => true));
        $this->getCollection()->addFieldToFilter('main_table.is_processed', $filterprocess);
        return;
    }

    /**
     * Retrieve a file container array by grid data as CSV
     *
     * Return array with keys type and value
     *
     * @return array
     */
    public function getCsvFile() {
        $this->_isExport = true;
        $this->_prepareGrid();

        $io = new Varien_Io_File();

        $path = Mage::getBaseDir('var') . DS . 'export' . DS;
        $name = md5(microtime());
        $file = $path . DS . $name . '.csv';

        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $path));
        $io->streamOpen($file, 'w+');
        $io->streamLock(true);
        $io->streamWriteCsv($this->_getExportHeaders());

        $this->_exportIterateCollection('_exportCsvItem', array($io));
//        echo $this->_getExportTotals();exit;

        if ($this->getCountTotals()) {
            $io->streamWriteCsv($this->_getExportTotals());
        }

        $io->streamUnlock();
        $io->streamClose();

        return array(
            'type' => 'filename',
            'value' => $file,
            'rm' => true // can delete file after use
        );
    }

    protected function _processFilter_channel($collection, $column) {
        $filterprocess = $column->getFilter()->getValue();
        if ($filterprocess == '') {
            return $this;
        }
        if (in_array($filterprocess, array('my_store', 'incoming', 'outgoing', '_0'))) {
            if (in_array($filterprocess, array('my_store', '_0'))) {
                $this->getCollection()->addFieldToFilter('main_table.channel_type', array('null' => true));
            } else {
                $this->getCollection()->addFieldToFilter('main_table.channel_type', $filterprocess);
            }
        } else {
            $id = preg_replace("/[^0-9]/", "", $filterprocess);
            $channel_type = preg_replace('/[0-9]+/', '', $filterprocess);
            $this->getCollection()->addFieldToFilter('main_table.channel_id', $id);
            $this->getCollection()->addFieldToFilter('main_table.channel_type', $channel_type);
        }
        return;
    }

    public function getActiveChannelsForEDI() {
        $incomingchannel_ids = array(0);
        //get active incoming channel ids
        $incomingchannels = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('is_hidden', 0)
                ->addFieldToSelect('id')
                ->getData();

        if ($incomingchannels) {
            $incomingchannel_ids = array_column($incomingchannels, 'id');
        }
        return $incomingchannel_ids;
    }

    protected function _getImageTypesAssignedToProduct($product, $imageFile) {
        $types = array();
        foreach ($product->getMediaAttributes() as $attribute) {
            if ($product->getData($attribute->getAttributeCode()) == $imageFile) {
                $types[] = $attribute->getAttributeCode();
            }
        }
        return $types;
    }

    public function callback_image($value, $row, $column, $isExport) {
        // echo "<pre>";
        $entity_id = $row->getData('product_entity_id');
        if (!$this->productImage[$entity_id]) {
            $store_product = Mage::getModel('catalog/product')->load($entity_id);
            $media_gallery = $store_product->getData('media_gallery');
            foreach ($media_gallery['images'] as $image) {
                $image_types = $this->_getImageTypesAssignedToProduct($store_product, $image['file']);
                if ($image_types) {
                    $this->productImage[$entity_id] = Mage::getSingleton('catalog/product_media_config')->getMediaUrl($image['file']);
                }
            }
        }

        $out = "<img src=" . $this->productImage[$entity_id] . " width='60px'/>";
        return $out;
    }

}