<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Coupons generation parameters form
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Fedobe_Bridge_Block_Adminhtml_Edi_Form extends Mage_Adminhtml_Block_Widget_Form {

    /**
     * Prepare coupon codes generation parameters form
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm() {
        $user = Mage::getModel('admin/user')->load(Mage::getSingleton('admin/Session')->getUser()->getUserId());
        if (strcasecmp($user->getRole()->getRoleName(), 'Administrators') === 0) {
            $model = Mage::getModel('bridge/rule');
            $form = new Varien_Data_Form();
            $form->setHtmlIdPrefix('rule_');
            $form->setFieldNameSuffix('bridge');
            $renderer = Mage::getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
                    ->setTemplate('promo/fieldset.phtml')
                    ->setNewChildUrl($this->getUrl('*/promo_quote/newConditionHtml/form/rule_conditions_fieldset'));

            $fieldset = $form->addFieldset('conditions_fieldset', array(
                        'legend' => Mage::helper('bridge')->__('Apply the rule only if the following conditions are met (leave blank for all products)' . "<span style='margin-left:721px;'><a id='fldst' href='javascript:void(0);' onclick='showgrid();'><img src='" . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN) . 'adminhtml/default/default/images/entry_edit_head_arrow_down.gif' . "'/></a></span>")
                    ))->setRenderer($renderer);

            $fieldset->addField('conditions', 'text', array(
                'name' => 'conditions',
                'label' => Mage::helper('catalogrule')->__('Conditions'),
                'title' => Mage::helper('catalogrule')->__('Conditions'),
            ))->setRule($model)->setRenderer(Mage::getBlockSingleton('rule/conditions'));

            $gridBlock = Mage::getBlockSingleton('fedobe_bridge_block_adminhtml_edi_grid');
            $gridBlockJsObject = '';
            if ($gridBlock)
                $gridBlockJsObject = $gridBlock->getJsObjectName();
            $idPrefix = $form->getHtmlIdPrefix();
            $generateUrl = $this->getGenerateUrl();
            $fieldset->addField('generate_button', 'note', array(
                'text' => $this->getButtonHtml(
                        Mage::helper('bridge')->__('Generate'), "getProductList('{$idPrefix}' ,'{$generateUrl}','{$gridBlockJsObject}')", 'generate conditionbtn'
                ),
            ));




            $this->setForm($form);
        }
        return $this;
    }

    /**
     * Retrieve URL to Generate Action
     *
     * @return string
     */
    public function getGenerateUrl() {
        return $this->getUrl('adminhtml/edi/generate');
    }

}