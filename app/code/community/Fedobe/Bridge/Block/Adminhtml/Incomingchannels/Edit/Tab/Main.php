<?php

class Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel() {
        return Mage::helper('bridge')->__('Settings');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle() {
        return Mage::helper('bridge')->__('Settings');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab() {
        if ($this->getRequest()->getParam('id') || @$this->getRequest()->getParam('channel_type'))
            return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden() {
        if (!@$this->getRequest()->getParam('id') && !@$this->getRequest()->getParam('channel_type'))
            return false;
    }

    protected function _prepareForm() {
        $is_admin = Mage::helper('bridge')->isAdmin();
        $model = Mage::registry('incomingchannel_data');
        $session_security_salt = $model->getId() ? "{$model->getId()}_incoming_security_salt" : "incoming_security_salt";
        $afterstatushtml = '';
        if ($model->getId() && $model->getUrl() && $model->getKey() && $model->getSecret()) {
            if (!$model->getAuthorized()) {
                $authorize_url = Mage::getBaseUrl() . "/bridge/authorize/index/id/" . $model->getId();
                $afterstatushtml = "<tr><td></td><td><a href='{$authorize_url}' target='_blank'>Click to Authorize the Channel</a></td></tr>";
            } else if ($model->getStatus()) {
                $sync_url = $this->getUrl('adminhtml/sync/manual', array('id' => $model->getId())) . 'acvtiveTab/main_section';
                $afterstatushtml = '<tr><td></td><td><input type="button" class="scalable form-button" value="Manual Sync" onclick="setLocation(\'' . $sync_url . '\')"/></td></tr>';
            }
        }
        if (@$this->getRequest()->getParam('channel_type')) {
            $model->setChannelType($this->getRequest()->getParam('channel_type'));
        }
        $relative_products = $model->getRelativeProduct();

        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('incomingchannel_');

        $fieldset1 = $form->addFieldset('main_fieldset1', array(
            'legend' => Mage::helper('bridge')->__('Channel Settings')
                )
        );
        $fieldset1->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
        $fieldset1->addField('general_informations', 'hbar', array(
            'id' => 'General Informations'
        ));
        if ($model->getId())
            $fieldset1->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        if ($model->getChannelType()) {
            $fieldset1->addField('channel_type', 'hidden', array('name' => 'channel_type'));
        }


        $fieldset1->addField('brand_name', 'text', array(
            'name' => 'brand_name',
            'label' => Mage::helper('bridge')->__('Name'),
            'title' => Mage::helper('bridge')->__('Name'),
            'required' => true,
        ));
        $fieldset1->addField('url', 'text', array(
            'name' => 'url',
            'label' => Mage::helper('bridge')->__('URL'),
            'title' => Mage::helper('bridge')->__('URL'),
            'class' => 'validate-url'
        ));
        $fieldset1->addField('mode', 'select', array(
            'name' => 'mode',
            'label' => Mage::helper('bridge')->__('Mode'),
            'title' => Mage::helper('bridge')->__('Mode'),
            'required' => true,
            'options' => array('demo' => Mage::helper('bridge')->__('Demo'), 'live' => Mage::helper('bridge')->__('Live')),
        ));
        $fieldset1->addField('status', 'select', array(
            'name' => 'channel_status',
            'label' => Mage::helper('bridge')->__('Status'),
            'title' => Mage::helper('bridge')->__('Status'),
            'options' => array(1 => Mage::helper('bridge')->__('Active'), 0 => Mage::helper('bridge')->__('Inactive')),
            'after_element_html' => $afterstatushtml,
            'required' => true,
        ));
        $fieldset1->addField('channel_informations', 'hbar', array(
            'id' => 'Channel Informations'
        ));
        $fieldset1->addField('', 'select', array(
            'name' => '',
            'label' => Mage::helper('bridge')->__('Channel Type'),
            'title' => Mage::helper('bridge')->__('Channel Type'),
            'disabled' => true,
            'options' => array(uc_words($model->getChannelType())),
        ));
        $fieldset1->addField('key', 'text', array(
            'name' => 'key',
            'label' => Mage::helper('bridge')->__('Key'),
            'title' => Mage::helper('bridge')->__('Key')
        ));
        $fieldset1->addField('secret', 'text', array(
            'name' => 'secret',
            'label' => Mage::helper('bridge')->__('Secret'),
            'title' => Mage::helper('bridge')->__('Secret')
        ));
        $fieldset1->addField('salt', 'text', array(
            'name' => 'salt',
            'label' => Mage::helper('bridge')->__('Security Salt'),
            'title' => Mage::helper('bridge')->__('Security Salt')
        ));


        if ($is_admin) {
            $fieldset2 = $form->addFieldset('main_fieldset2', array(
                'legend' => Mage::helper('bridge')->__('PIM Settings')
                    )
            );
            $fieldset2->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
            $fieldset2->addField('sync_settings', 'hbar', array(
                'id' => 'Sync Settings'
            ));
            $product_info = $fieldset2->addField('product_info', 'select', array(
                'name' => 'product_info',
                'label' => Mage::helper('bridge')->__('Sync Product Information'),
                'title' => Mage::helper('bridge')->__('Sync Product Information'),
                'options' => array(1 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'required' => true,
            ));
            $relative_product = $fieldset2->addField('relative_product', 'select', array(
                'name' => 'relative_product[1]',
                'label' => Mage::helper('bridge')->__('Sync Related Products'),
                'title' => Mage::helper('bridge')->__('Sync Related Products'),
                'values' => array(1 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'onchange' => 'this.value ? 1 : 0;',
                'required' => true,
            ));
            $up_sell = $fieldset2->addField('up_sell', 'select', array(
                'name' => 'relative_product[2]',
                'label' => Mage::helper('bridge')->__('Sync Upsell Products'),
                'title' => Mage::helper('bridge')->__('Sync Upsell Products'),
                'values' => array(2 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'onchange' => 'this.value ? 2 : 0;',
                'required' => true,
            ));
            $cross_sell = $fieldset2->addField('cross_sell', 'select', array(
                'name' => 'relative_product[3]',
                'label' => Mage::helper('bridge')->__('Sync Cross Sell Product'),
                'title' => Mage::helper('bridge')->__('Sync Cross Sell Product'),
                'values' => array(4 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'onchange' => 'this.value ? 4 : 0;',
                'required' => true,
            ));
            $fieldset2->addField('basic_settings', 'hbar', array(
                'id' => 'Basic Settings'
            ));
            $fieldset2->addField('weightage', 'text', array(
                'name' => 'weightage',
                'label' => Mage::helper('bridge')->__('Weightage'),
                'title' => Mage::helper('bridge')->__('Weightage'),
                'class' => 'validate-number validate-greater-than-zero unique-weightage',
                'required' => true,
            ));
            $fieldset2->addField('content_source', 'select', array(
                'name' => 'content_source',
                'label' => Mage::helper('bridge')->__('Product Content Destination'),
                'title' => Mage::helper('bridge')->__('Product Content Destination'),
                'options' => array('copy' => Mage::helper('bridge')->__('Keep Separate Incoming Content Only '), 'rule_base_original' => Mage::helper('bridge')->__('Replace with Rule Based else Leave Original Content'), 'original' => Mage::helper('bridge')->__('Replace with Rule Based else Replace With Incoming Content')),
                'required' => true,
            ));
            $fieldset2->addField('product_status', 'select', array(
                'name' => 'product_status',
                'label' => Mage::helper('bridge')->__('Default Status Of Listed Product'),
                'title' => Mage::helper('bridge')->__('Default Status Of Listed Product'),
                'options' => array(1 => Mage::helper('bridge')->__('Enabled'), 2 => Mage::helper('bridge')->__('Disabled'), 3 => Mage::helper('bridge')->__('As it in Source')),
                'required' => true
            ));
            $fieldset2->addField('create_new', 'select', array(
                'name' => 'create_new',
                'label' => Mage::helper('bridge')->__('Add New Product Automatically'),
                'title' => Mage::helper('bridge')->__('Add New Product Automatically'),
                'options' => array(1 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'required' => true,
            ));
            $fieldset2->addField('update_exist', 'select', array(
                'name' => 'update_exist',
                'label' => Mage::helper('bridge')->__('Update Existing Product Automatically'),
                'title' => Mage::helper('bridge')->__('Update Existing Product Automatically'),
                'options' => array(1 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'required' => true,
            ));
            $fieldset2->addField('create_option', 'select', array(
                'name' => 'create_option',
                'label' => Mage::helper('bridge')->__('Create New Attribute Options Automatically'),
                'title' => Mage::helper('bridge')->__('Create New Attribute Options Automatically'),
                'options' => array(1 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'required' => true,
            ));
            $fieldset2->addField('pim_cron', 'select', array(
                'name' => 'pim_cron',
                'label' => Mage::helper('bridge')->__('Cron Frequency'),
                'title' => Mage::helper('bridge')->__('Cron Frequency'),
                'options' => array('never' => Mage::helper('bridge')->__('Never'), '0 0 * * *' => Mage::helper('bridge')->__('Daily'), '0 0 * * 0' => Mage::helper('bridge')->__('Weekly'), '0 0 1 * *' => Mage::helper('bridge')->__('Monthly')),
                'required' => true,
            ));


            $fieldset3 = $form->addFieldset('main_fieldset3', array(
                'legend' => Mage::helper('bridge')->__('EDI Settings')
                    )
            );
            $fieldset3->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');

            $fieldset3->addField('sync_settings_', 'hbar', array(
                'id' => 'Sync Settings'
            ));
            $fieldset3->addField('product_pricenquantity', 'select', array(
                'name' => 'product_pricenquantity',
                'label' => Mage::helper('bridge')->__('Sync Prices & Quantity'),
                'title' => Mage::helper('bridge')->__('Sync Prices & Quantity'),
                'options' => array(1 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'required' => true,
            ));
            $fieldset3->addField('sync_order', 'select', array(
                'name' => 'sync_order',
                'label' => Mage::helper('bridge')->__('Sync Orders'),
                'title' => Mage::helper('bridge')->__('Sync Orders'),
                'options' => array(1 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'required' => true,
            ));
            $fieldset3->addField('basic_settins_', 'hbar', array(
                'id' => 'Basic Settings'
            ));
            $multiplepricerule = $fieldset3->addField('multi_price_rule', 'select', array(
                'name' => 'multi_price_rule',
                'label' => Mage::helper('bridge')->__('Multiple Pricing Rules'),
                'title' => Mage::helper('bridge')->__('Multiple Pricing Rules'),
                'options' => array('max_price' => Mage::helper('bridge')->__('Take Max Price Only')
                    , 'min_price' => Mage::helper('bridge')->__('Take Min Price Only')
                    , 'base_price' => Mage::helper('bridge')->__('Apply Multiple on Base Price')
                    , 'cumulative_price' => Mage::helper('bridge')->__('Apply Multiple on Cumulative Price')
                )
            ));
            $fieldset3->addField('slab', 'text', array(
                'name' => 'slab',
                'label' => Mage::helper('bridge')->__('Slab'),
                'title' => Mage::helper('bridge')->__('Slab'),
                'required' => true,
            ));
            $fieldset3->addField('channel_order', 'text', array(
                'name' => 'channel_order',
                'label' => Mage::helper('bridge')->__('Order'),
                'title' => Mage::helper('bridge')->__('Order'),
                'required' => true,
            ));
            $field_nm = $fieldset3->addField('edi_cron', 'select', array(
                'name' => 'edi_cron',
                'label' => Mage::helper('bridge')->__('Cron Frequency'),
                'title' => Mage::helper('bridge')->__('Cron Frequency'),
                'options' => array('never' => Mage::helper('bridge')->__('Never'), '0 * * * *' => Mage::helper('bridge')->__('Hourly'), '0 0 * * *' => Mage::helper('bridge')->__('Daily'), '0 0 * * 0' => Mage::helper('bridge')->__('Weekly'), '0 0 1 * *' => Mage::helper('bridge')->__('Monthly')),
                'required' => true,
            ));
        }
        $unique_wightage_url = $this->getUrl('adminhtml/incomingchannels/weightageunique_validation') . "?isAjax=true";
        $form_key = Mage::getSingleton('core/session')->getFormKey();
        $field_nm->setAfterElementHtml('
<script>
    $j(function () {
    $j("#incomingchannel_status").change();
    $j("#incomingchannel_mode").change();
    });
    //change the sync mode for this channel
     $j("#incomingchannel_mode").change(function () {
            if ($j("#incomingchannel_mode").val() == "demo") {
                $j("#incomingchannel_product_status").val(2);
                $j("#incomingchannel_product_status").prop("disabled", "disabled");
                $j("#incomingchannel_product_status").addClass(\'required-entry\');
            } else {
                $j("#incomingchannel_product_status").prop("disabled", "");
                $j("#incomingchannel_product_status").addClass(\'required-entry\');
            }

        });
     //dependancy validation
        $j("#incomingchannel_status").change(function(){
         if($j("#incomingchannel_status").val()==0){
            $j("#incomingchannel_status").closest("td").find("span").remove();
            $j("#incomingchannel_url").removeClass("required-entry");
            $j("#incomingchannel_key").removeClass("required-entry");
            $j("#incomingchannel_secret").removeClass("required-entry");
            $j("label[for=\'incomingchannel_url\']").find(".required").remove();
            $j("label[for=\'incomingchannel_key\']").find(".required").remove();
            $j("label[for=\'incomingchannel_secret\']").find(".required").remove();
            }else{
            $j("#incomingchannel_status").closest("td").find("span").remove();
            $j("#incomingchannel_status").closest("td").append("<span style=\'color:black\'>[Channel Setting is required]</span>");
            $j("#incomingchannel_url").addClass("required-entry");
            $j("#incomingchannel_key").addClass("required-entry");
            $j("#incomingchannel_secret").addClass("required-entry");
            $j("label[for=\'incomingchannel_url\']").append("<span class=\'required\'>*</span>");
            $j("label[for=\'incomingchannel_key\']").append("<span class=\'required\'>*</span>");
            $j("label[for=\'incomingchannel_secret\']").append("<span class=\'required\'>*</span>");
            }
        });
        
    Validation.add("unique-weightage", "Please enter a unique Weightage.", function (v) {
        ret = false;
        url = "' . $unique_wightage_url . '";
        id = $j("#incomingchannel_id").val();
        data = {id: id, weightage: $j("#incomingchannel_weightage").val(), form_key: "' . $form_key . '"};
        $j.ajax({
            type: "POST",
            async: false,
            url: url,
            data: data,
            success: function (res) {
                if (res == 1) {
                    ret = false;
                } else {
                    ret = true;
                }
            }
        });
        return ret;
    });
</script>
    
       ');

        if (!$model->getWeightage()) {
            $model->setWeightage(10);
        }
        $model->setRelativeProduct($relative_products & 1);
        $model->setUpSell($relative_products & 2);
        $model->setCrossSell($relative_products & 4);
        $security_salt = Mage::getSingleton('core/session')->getData($session_security_salt) ? Mage::getSingleton('core/session')->getData($session_security_salt) : $model->getSalt();
        $model->setSalt($security_salt);
        $form->setValues($model->getData());
        $this->setForm($form);
        $this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
                        ->addFieldMap($product_info->getHtmlId(), $product_info->getName())
                        ->addFieldMap($relative_product->getHtmlId(), $relative_product->getName())
                        ->addFieldMap($up_sell->getHtmlId(), $up_sell->getName())
                        ->addFieldMap($cross_sell->getHtmlId(), $cross_sell->getName())
                        ->addFieldDependence(
                                $relative_product->getName(), $product_info->getName(), '1'
                        )
                        ->addFieldDependence(
                                $up_sell->getName(), $product_info->getName(), '1'
                        )
                        ->addFieldDependence(
                                $cross_sell->getName(), $product_info->getName(), '1'
        ));

        Mage::dispatchEvent('fedobe_bridge_adminhtml_incomingchannels_edit_tab_main_prepare_form', array('form' => $form));

        return parent::_prepareForm();
    }

}