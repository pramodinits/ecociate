<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * description
 *
 * @category    Mage
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Edit_Tab_Mapping extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel() {
        return Mage::helper('bridge')->__('Mapping');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle() {
        return Mage::helper('bridge')->__('Mapping');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab() {
        return $this->_isEditingnAuthorized();
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden() {
        return !$this->_isEditingnAuthorized();
    }

    /**
     * Check whether we edit existing rule or adding new one
     *
     * @return bool
     */
    protected function _isEditingnAuthorized() {
        return !is_null($this->getRequest()->getParam('id')) && @Mage::registry('incomingchannel_data')->getAuthorized();
    }

    protected function _prepareForm() {
        $form = new Varien_Data_Form();
        $renderer = Mage::getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
                ->setTemplate('fedobe/bridge/mapping.phtml');
        $fieldset = $form->addFieldset('mapping_fieldset', array(
                    'legend' => Mage::helper('bridge')->__('Mapping')
                ))->setRenderer($renderer);
        $this->setForm($form);

        return $this;
    }

    public function getBrandAttributeSet() {
        $brand_id = $this->getRequest()->getParam('id');
        $brand_attribute_set = Mage::getmodel('bridge/attribute_categories')->getCollection()
                ->addFieldToFilter('brand_id', $brand_id)
                ->addFieldToSelect(array('attributes', 'categories', 'store', 'default_store'))
                ->getFirstItem()
                ->getData();
        return $brand_attribute_set;
    }

    public function getRetailerAttributeSet($systm_attr=0) {
        $brand_attribute_set = Mage::getmodel('bridge/incomingchannels')->getAttributes($systm_attr);
        return $brand_attribute_set;
    }
    public function getAllRetailerAttributeSet() {
        $brand_attribute_set = Mage::getmodel('bridge/incomingchannels')->getAllAttributes();
        return $brand_attribute_set;
    }

    public function getRetailerCategories() {
        $defaultstoreId=Mage::app()
    ->getWebsite()
    ->getDefaultGroup()
    ->getDefaultStoreId();
        $store_id=Mage::app()->getRequest()->getParam('store')?Mage::app()->getRequest()->getParam('store'):$defaultstoreId;
       $category_collection = Mage::getModel('catalog/category')
                ->getCollection()
                ->setStoreId($store_id)
                ->addAttributeToSelect(array('entity_id', 'parent_id', 'category_id', 'name'))
                ->addIsActiveFilter();
        return $category_collection;
    }

    /**
     * get all stores
     */
    public function getRetailerStores() {
        $store_result = array('0'=>'Default Values');
        $stores = Mage::app()->getStores();
        foreach ($stores as $store) {
            $store_result[$store->getId()] = $store->getName();
        }
        return $store_result;
    }

}
