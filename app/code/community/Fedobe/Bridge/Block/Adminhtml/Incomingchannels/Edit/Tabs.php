<?php

/**
 * @category    AM
 * @package     AM_RevSlider
 * @copyright   Copyright (C) 2008-2014 ArexMage.com. All Rights Reserved.
 * @license     GNU General Public License version 2 or later
 * @author      ArexMage.com
 * @email       support@arexmage.com
 */
class Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setDestElementId('edit_form');
        $this->setId('incomingchannels');
        if ($tab = $this->getRequest()->getParam('activeTab')) {
            $this->_activeTab = $tab;
        } else if (!@$this->getRequest()->getParam('id') && !@$this->getRequest()->getParam('channel_type')) {
            $this->_activeTab = 'settings_section';
        } else {
            $this->_activeTab = 'main_section';
        }
        $this->setTitle(Mage::helper('bridge')->__('Incoming Channel'));
    }

    public function _prepareLayout() {
        return parent::_prepareLayout();
    }

    /**
     * Add new tab after another
     *
     * @param   string $tabId new tab Id
     * @param   array|Varien_Object $tab
     * @param   string $afterTabId
     * @return  Mage_Adminhtml_Block_Widget_Tabs
     */
    public function addTabAfter($tabId, $tab, $afterTabId) {
        $this->addTab($tabId, $tab);
        $this->_tabs[$tabId]->setAfter($afterTabId);
    }

    protected function _beforeToHtml() {
        $param = Mage::app()->getRequest()->get('activeTab');
        if (array_key_exists($param, $this->_tabs)) {
            $this->_tabs[$param]->setActive();
        }
        parent::_beforeToHtml();
        if (!Mage::helper('bridge')->isAdmin()) {
            $this->removeTab('mapping_section');
            $this->removeTab('listing_section');
            $this->removeTab('notification_section');
            $this->removeTab('actions_section');
        }
        return Mage_Adminhtml_Block_Widget_Tabs::_beforeToHtml();
    }

}