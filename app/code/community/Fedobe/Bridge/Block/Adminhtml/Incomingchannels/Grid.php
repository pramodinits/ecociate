<?php

class Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Grid extends Mage_Adminhtml_Block_Widget_Grid {

//    protected $supplierList;
    protected $partner;
    protected $isAdmin;

    public function __construct() {
        parent::__construct();
        $this->setId('incomingchannels_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
//        $this->supplierList = Mage::helper('bridge')->getSupplierList();
        $this->partner = null;
        $this->isAdmin = true;
        //if logged in user is not adminstrator then he/she can only she own invneotries
       
        $is_admin = Mage::helper('bridge')->isAdmin();
        if (!$is_admin) {
            $sellerid = Mage::helper('tribeseller')->getSellerIdFromadminUser();
        //if logged in user is not adminstrator then he/she can only she own invneotries
            $sellerCollection = Mage::getModel('tribeseller/seller')->getCollection();
            $seller = $sellerCollection
                    ->addAttributeToFilter('entity_id', $sellerid)
                    ->addAttributeToSelect('seller_partner')
                    ->getFirstItem()
                    ->getSellerPartner();
            $this->partner = $seller;
            $this->isAdmin = false;
        }
        if(Mage::helper('core')->isModuleEnabled('Fedobe_Tribebrand')){
            $brand_seller_id = Mage::helper('tribebrand')->getBrandSellerIdFromadminUser();
            $this->partner = $brand_seller_id;
            $this->isAdmin = false;
        } 
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('bridge/incomingchannels')->getCollection();
        $collection->addFieldToFilter('is_hidden', 0);
        if (!is_null($this->partner)) {
            $partner = $this->partner ? $this->partner : 0;
            $collection->addFieldToFilter('channel_supplier', $partner);
        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('id', array(
            'header' => Mage::helper('bridge')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'id'
        ));

        $this->addColumn('brand_name', array(
            'header' => Mage::helper('bridge')->__('Channel Name'),
            'align' => 'left',
            'index' => 'brand_name'
        ));
        $this->addColumn('product_count', array(
            'header' => Mage::helper('bridge')->__('Product Count'),
            'align' => 'left',
            'index' => 'product_count',
            'renderer' => 'Fedobe_Bridge_Block_Adminhtml_Render_Productcount'
        ));
        $this->addColumn('channel_type', array(
            'header' => Mage::helper('bridge')->__('Channel Type'),
            'align' => 'left',
            'index' => 'channel_type'
        ));
        $this->addColumn('weightage', array(
            'header' => Mage::helper('bridge')->__('Weightage'),
            'align' => 'left',
            'index' => 'weightage',
        ));
        $this->addColumn('slab', array(
            'header' => Mage::helper('bridge')->__('Slab'),
            'align' => 'left',
            'index' => 'slab',
        ));
        $this->addColumn('enabled', array(
            'header' => Mage::helper('bridge')->__('Enabled'),
            'align' => 'left',
            'index' => 'status',
            'renderer' => 'Fedobe_Bridge_Block_Adminhtml_Render_Status'
        ));
        $status = array('In Queue', 'Condition Required', 'Condition Inactive', 'Searching product', 'Search for product Complete', 'Collecting image', 'Mapping Required', 'Migration Goingon', 'Migration Complete', 'Edi Update', 'Status Unknown');
        $status_code = array_combine($status, $status);
        $this->addColumn('cron_status', array(
            'header' => Mage::helper('bridge')->__('Status'),
            'align' => 'left',
            'index' => 'cron_status',
            'type' => 'options',
            'options' => $status_code,
                //'frame_callback'=>array($this, 'callback_skus'),
                // 'filter_condition_callback'=> array($this, 'filter_skus')
        ));
        //$link changed
        $link = Mage::helper('bridge')->getUrl('adminhtml/incomingchannels/edit/') . 'id/$id';
        $this->addColumn('action_edit', array(
            'header' => $this->helper('bridge')->__('Action'),
            'width' => 15,
            'sortable' => false,
            'filter' => false,
            'type' => 'action',
            'renderer' => 'Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Gridrender',
        ));
        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
//        $channel_type = $row->getChannelType();
//        if($channel_type == 'magento')
//            $channel_type = 'incomingchannels';
//        $link = 'adminhtml/' . $channel_type . '/edit/';
//        $id = $row->getId();
//        $link = Mage::helper('bridge')->getUrl($link) . 'id/' . $id;
//        return $link;
    }

    public function callback_skus($value, $row, $column, $isExport) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $channel_id = $row->getData('id');
        $collection = Mage::getModel('bridge/incomingchannels_incominglog')->getCollection();
        $data = $collection->addFieldToFilter('bridge_channel_id', $channel_id)->getData();
        $incmodel = Mage::getModel('bridge/incomingchannels');
        $incmodel->load($channel_id);
        $model = Mage::getModel('bridge/incomingchannels');
        $model->load($channel_id);
        $channel_type = $incmodel->getChannelType();
        $old_cron_status = $incmodel->getCronStatus();
        $cron_status = '';
        if ($channel_type == 'amazon') {
            $amazon_condition = $model->getAmazonCondition();
            $product_info = $model->getProductInfo();
            $product_pricenquantity = $model->getProductPricenquantity();
            if (empty($data)) {
                if (empty($amazon_condition)) {
                    $code[] = 'Condition Required';
                } else {
                    if (!$product_info && !$product_pricenquantity) {
                        $code[] = 'Condition Inactive';
                    } else {
                        $code[] = 'Status Unknown';
                    }
                }
            } else {
                $cron_status = array('In Queue', 'Searching product', 'Search for product Complete', 'Collecting image', 'Mapping Required', 'Migration Goingon', 'Migration Complete', 'Edi Update', 'Status Unknown');
                $total_product = count($data);
                $status_occurance = array_column($data, 'status');
                $ready_Count = array_column($status, 'Ready');
                $status = array_unique($status_occurance);
                $staus_count = array_count_values($status_occurance);
                if (in_array('Ediupdate', $status)) {
                    $Ediupdate_count = (isset($staus_count['Ediupdate'])) ? $staus_count['Ediupdate'] : 0;
                    //if($total_product==$Ediupdate_count)
                    $code[] = 'Edi Update';
                }

                if (in_array('Created', $status)) {
                    if ($total_product == $staus_count['Created'] || !$ready_Count)
                        $code[] = 'Migration Complete';
                    else {
                        $code[] = 'Migration Goingon';
                    }
                }
                if (in_array('Ready', $status)) {
                    $ready_count = (isset($staus_count['Ready'])) ? $staus_count['Ready'] : 0;
                    $missed_count = (isset($staus_count['Missed'])) ? $staus_count['Missed'] : 0;
                    if (($ready_count + $missed_count) == $total_product)
                        $code[] = 'Mapping Required';
                    else {
                        if (in_array('New', $status))
                            $code[] = 'Collecting image';
                    }
                }

                if (in_array('New', $status) || in_array('Missed', $status)) {
                    $new_count = (isset($staus_count['New'])) ? $staus_count['New'] : 0;
                    $missed_count = (isset($staus_count['Missed'])) ? $staus_count['Missed'] : 0;
                    if (($new_count + $missed_count) == $total_product)
                        $code[] = 'Search for product Complete';
                    else
                        $code[] = 'Searching product';
                }

                if (in_array('PIM', $status)) {
                    $PIM_count = (isset($staus_count['PIM'])) ? $staus_count['PIM'] : 0;
                    if ($total_product == $PIM_count)
                        $code[] = 'In Queue';
                    else
                        $code[] = 'Searching product';
                }
            }
            $cron_status = (!empty($code)) ? $code[0] : 'Status Unknown';
            $incomingchannel_table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels');
            if ($old_cron_status != $cron_status) {
                $query = 'UPDATE ' . $incomingchannel_table . ' SET cron_status= "' . $cron_status . '" WHERE id=' . $channel_id;
                $writeConnection->query($query);
            }
        }
        return $cron_status;
    }

}