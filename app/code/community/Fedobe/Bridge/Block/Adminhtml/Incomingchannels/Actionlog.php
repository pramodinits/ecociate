<?php
class Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Actionlog extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{
    public $_product;
    public function render(Varien_Object $row) {
       $data = $row->getData();
        $id = $data['id'];
        $sku = $data['sku'];
        $channelid = $data['bridge_channel_id'];
        $model = Mage::getModel('bridge/incomingchannels');
        $model->load($channelid);
        $channel_type = $model->getChannelType();
        $link = 'adminhtml/incomingchannels/deletelog/id/' .$id;
        $reprocess_link = 'adminhtml/incomingchannels/reprocess/id/'.$id.'/sku/' .$sku.'/channel_id/'.$channelid;
        $link = Mage::helper('bridge')->getUrl($link);
        $reprocess_link = Mage::helper('bridge')->getUrl($reprocess_link);
        $arr = '  <a href="'.$link.'">Delete</a>'."&nbsp;"."&nbsp;";
        if($channel_type=='amazon')
        $arr .= '  <a href="'.$reprocess_link.'">Reprocess</a>';
        return $arr;
    }
}
?>

