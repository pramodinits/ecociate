<?php

class Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Log_Managebridgecontent extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $data = $row->getData();
        $product = Mage::getModel('catalog/product')->setStoreId(0)->loadByAttribute('sku', $data['sku']);
        if (!$product)
            return '';
        if (@$product->getId()) {
            $link = $this->getUrl(
                    'adminhtml/managebridgecontent/index', array(
                'product' => $product->getId(),
                'set' => $product->getAttibuteSetId(),
                'type' => $product->getTypeId(),
                'popup' => 1
                    )
            );
            $id = $product->getId();
            $sku = $data['sku'];
            $url = $product->getUrlPath();
            $visible = $product->isVisibleInCatalog() && $product->isVisibleInSiteVisibility();
            $view_link = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $url;
            $edit_link = 'adminhtml/catalog_product/edit/id/' . $id;
            $edit_link = Mage::helper('bridge')->getUrl($edit_link);
            $arr = '  <a href="' . $link . '" target="' . $id . $sku . '" title="Manage Product Content"><img src="' . $this->getSkinUrl('images/fedobe/Content.png') . '"></a>';
            $arr .= '  <a href="' . $edit_link . '" target="_blank" title="Edit Product"><img src="' . $this->getSkinUrl('images/fedobe/Edit.png') . '"></a>';
            
            if ($visible)
                $arr .= '  <a href="' . $view_link . '" target="' . $id . '" title="View Product Page"><img src="' . $this->getSkinUrl('images/fedobe/View.png') . '"></a>' . "&nbsp;";
            return $arr;
        }
    }

}

?>