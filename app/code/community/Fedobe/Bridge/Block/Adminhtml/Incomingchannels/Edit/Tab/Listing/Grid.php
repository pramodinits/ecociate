<?php

class Fedobe_Bridge_Block_Adminhtml_incomingchannels_Edit_Tab_Listing_Grid extends Mage_Adminhtml_Block_Widget_Grid {
    //public $_defaultLimit = 300000;
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->setId('condProductListGrid');
        $this->setUseAjax(true);
        $this->setVarNameFilter('condproduct_filter');
    }

    /**
     * Prepare collection for grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    public function _prepareCollection() {
        $id = $this->getRequest()->getParam('id');
        $pim_attr_id = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'bridge_channel_id');
        $edi_attr_id = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'bridge_pricenqty_channel_id');
        $collection = Mage::getModel('catalog/product')->getCollection();
        if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
            $collection->joinField('qty', 'cataloginventory/stock_item', 'qty', 'product_id=entity_id', '{{table}}.stock_id=1', 'left');
        }

        if (@$this->getRequest()->getParam('store')) {
            $collection->addAttributeToSelect('*')->getSelect()
                    ->join('catalog_product_entity_int', "e.entity_id = catalog_product_entity_int.entity_id AND ((catalog_product_entity_int.attribute_id=$pim_attr_id AND catalog_product_entity_int.store_id={$this->getRequest()->getParam('store')}) OR catalog_product_entity_int.attribute_id=$edi_attr_id) AND catalog_product_entity_int.value=$id", array('catalog_product_entity_int.value'));
        } else {
            $collection->addAttributeToSelect('*')->getSelect()
                    ->join('catalog_product_entity_int', "e.entity_id = catalog_product_entity_int.entity_id AND (catalog_product_entity_int.attribute_id=$pim_attr_id OR catalog_product_entity_int.attribute_id=$edi_attr_id) AND catalog_product_entity_int.value=$id", array('catalog_product_entity_int.value'))
                    ->group('catalog_product_entity_int.entity_id');
        }

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Define grid columns
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns() {
        $attributeCodeConfig = "bridge_channel_id";
        $bridgecontentedited = "bridge_content_edited";
        $bridge_channel_id = $this->getRequest()->getParam('id');
        $data = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToFilter('bridge_channel_id', $bridge_channel_id)
                ->getData();
        $attribute_set_id = array_column($data, 'attribute_set_id');
        $attributes = Mage::getModel('catalog/product_attribute_api')->items($attribute_set_id[0]);
        $this->addColumn('entity_id', array(
            'header' => Mage::helper('bridge')->__('Id'),
            'index' => 'entity_id',
            'width' => '70px',
            'column_css_class' => 'entity_id',
        ));
         $this->addColumn('image', array(
            'header' => Mage::helper('bridge')->__('Image'),
            'index' => 'image',
            'width' => '50px',
            'frame_callback'=>array($this, 'callback_image')
        ));
        $this->addColumn('name', array(
            'header' => Mage::helper('bridge')->__('Name'),
            'index' => 'name',
            'width' => '200px',
            //'column_css_class' => 'nameinline',
            'renderer'=>'Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Listingedit'
        ));
        $this->addColumn('type', array(
            'header' => Mage::helper('bridge')->__('Type'),
            'width' => '60px',
            'index' => 'type_id',
            'type' => 'options',
            'options' => Mage::getSingleton('catalog/product_type')->getOptionArray(),
        ));

        $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
                ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
                ->load()
                ->toOptionHash();

        $this->addColumn('set_name', array(
            'header' => Mage::helper('bridge')->__('Attrib. Set Name'),
            'width' => '100px',
            'index' => 'attribute_set_id',
            'type' => 'options',
            'options' => $sets,
        ));
        $this->addColumn('sku', array(
            'header' => Mage::helper('bridge')->__('SKU'),
            'width' => '80px',
            'index' => 'sku',
        ));
        if (Mage::helper('bridge')->isModuleEnabled('Mage_CatalogInventory')) {
            $this->addColumn('qty', array(
                'header' => Mage::helper('bridge')->__('Qty'),
                'width' => '20px',
                'type' => 'number',
                'index' => 'qty',
            ));
        }
        $this->addColumn('status', array(
            'header' => Mage::helper('bridge')->__('Status'),
            'width' => '70px',
            'index' => 'status',
            'type' => 'options',
            'options' => Mage::getSingleton('catalog/product_status')->getOptionArray(),
        ));
        if (!$this->_isExport) {
            $this->addColumnAfter($bridgecontentedited, array(
                'header' => Mage::helper('catalog')->__('Bridge Content Edited'),
                'width' => '100px',
                'type' => 'options',
                'column_css_class' => 'bridge-content-edited',
                'index' => $bridgecontentedited,
                'options' => array('1' => "Yes", '0' => 'No')
                    ), $attributeCodeConfig);

            $this->addColumn('action_edit', array(
                'header' => $this->helper('bridge')->__('Action'),
                'width' => '100px',
                'sortable' => false,
                'filter' => false,
                'type' => 'action',
                'renderer' => 'Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Managebridgecontent',
            ));
        }
        $this->addExportType('*/incomingchannels/exportCsv', Mage::helper('bridge')->__('CSV'));
        $this->addExportType('*/incomingchannels/exportExcel', Mage::helper('bridge')->__('Excel XML'));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('product');

        $statuses = Mage::getSingleton('catalog/product_status')->getOptionArray(); //
        array_unshift($statuses, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('catalog')->__('Change status'),
            'url' => $this->getUrl('*/incomingchannels/massStatus', array('_current' => true)),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    // 'class' => 'required-entry',
                    'label' => Mage::helper('catalog')->__('Status'),
                    'values' => $statuses
                )
            )
        ));

        Mage::dispatchEvent('adminhtml_catalog_product_grid_prepare_massaction', array('block' => $this));
        return $this;
    }

    /**
     * Get grid url
     *
     * @return string
     */
    public function getGridUrl() {
        return $this->getUrl('*/incomingchannels/grid', array('_current' => true));
    }

    public function getRowUrl($row) { //echo $row->getId();
        //        $id = $row->getEntityId();
        //        $link = 'adminhtml/catalog_product/edit/';
        //        $id = $row->getId();
        //        $link = Mage::helper('bridge')->getUrl($link) . 'id/' . $id;
        //        return $link;
    }

    public function callback_attribute($value, $row, $column, $isExport) {
        $entity_id = $row->getData('entity_id');
        $attribute = $column->getIndex();
        $product = Mage::getModel('catalog/product')->load($entity_id);
        return $product->getAttributeText($attribute);
    }

    public function getCollection() {
        return parent::getCollection();
    }

    public function _afterLoadCollection() {
        return parent::_afterLoadCollection();
    }

    public function _prepareGrid() {
        return parent::_prepareGrid();
    }
    protected function _getImageTypesAssignedToProduct($product, $imageFile) {
        $types = array();
        foreach ($product->getMediaAttributes() as $attribute) {
            if ($product->getData($attribute->getAttributeCode()) == $imageFile) {
                $types[] = $attribute->getAttributeCode();
            }
        }
        return $types;
}
 public function callback_image($value, $row, $column, $isExport){
    // echo "<pre>";
    $entity_id = $row->getData('entity_id');
    $store_product = Mage::getModel('catalog/product')->load($entity_id);
    $media_gallery = $store_product->getData('media_gallery');
   // print_r($media_gallery);
    foreach ($media_gallery['images'] as $image) {
                $image_types = $this->_getImageTypesAssignedToProduct($store_product, $image['file']);
//print_r($image_types);
                if ($image_types) {
                    $result = array(
                        'id' => $image['value_id'],
                        'label' => $image['label'],
                        'position' => $image['position'],
                        'exclude' => $image['disabled'],
                        'url' => Mage::getSingleton('catalog/product_media_config')->getMediaUrl($image['file']),
                        'types' => $image_types
                    );
                }
            }
        $out = "<img src=". $result['url'] ." width='60px'/>";
        return $out;
}

}
