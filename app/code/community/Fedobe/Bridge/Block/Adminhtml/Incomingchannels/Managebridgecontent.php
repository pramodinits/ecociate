<?php

class Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Managebridgecontent extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    private $_productcommonpath;
    private $_productchanneltype;

    public function __construct() {
        parent::__construct();
        $this->_productcommonpath = $this->__getCommonfrontentpath();
        $this->_productchanneltype = $this->getChanneltype();
    }

    public function render(Varien_Object $row) {
        $data = $row->getData();
        $link = $this->getUrl(
                'adminhtml/managebridgecontent/index', array(
            'product' => $data['entity_id'],
            'channel_type' => 'incoming',
            'set' => $data['attribute_set_id'],
            'type' => $data['type_id'],
            'popup' => 1
                )
        );
        $id = $data['entity_id'];
        $sku = $data['sku'];
        $product = Mage::getModel('catalog/product')->setStoreId(0)->load($data['entity_id']);
        $visible = $product->isVisibleInCatalog() && $product->isVisibleInSiteVisibility();
        //$color = ($visible)?'':'#A9A9A9';

        $incoming_view_link = $this->getIncomingPageUrl($product);
        $view_link = $this->__getFrontendProductUrl($product);
        $edit_link = 'adminhtml/catalog_product/edit/id/' . $data['entity_id'];
        $edit_link = Mage::helper('bridge')->getUrl($edit_link);
        $arr = '  <a href="' . $link . '" target="' . $id . $sku . '"><img src="' . $this->getSkinUrl('images/fedobe/Content.png') . '" title="Manage Content" alt="Manage Content"></a>';
        $arr .= '  <a href="' . $edit_link . '" target="_blank"><img src="' . $this->getSkinUrl('images/fedobe/Edit.png') . '" title="Edit" alt="Edit"></a>';
        if ($visible)
            $arr .= '  <a href="' . $view_link . '" target="' . $id . '"><img src="' . $this->getSkinUrl('images/fedobe/View.png') . '" title="View" alt="View"></a>' . "&nbsp;";
        if ($incoming_view_link)
            $arr .= '  <a href="' . $incoming_view_link . '" target="' . $id . '"><img src="' . $this->getSkinUrl('images/fedobe/View.png') . '" title="View '.  ucfirst($this->_productchanneltype).' Product page" alt="View '.  ucfirst($this->_productchanneltype).' Product page"></a>' . "&nbsp;";
        return $arr;
    }

    private function __getCommonfrontentpath() {
        $websites = Mage::app()->getWebsites();
        $stores = array();
        foreach ($websites as $wk => $wv) {
            $website_id = $wv->getId();
            break;
        }
        $website_id = ($this->getRequest()->getParam('website')) ? $this->getRequest()->getParam('website') : $website_id;
        $websites = Mage::getModel("core/website")->load($website_id);
        foreach ($websites->getStoreIds() as $sk => $sv) {
            $store_id = $sv;
            break;
        }
        $storeid = ($this->getRequest()->getParam('store')) ? $this->getRequest()->getParam('store') : $store_id;
        $use_store_code_in_url = Mage::getStoreConfig('web/url/use_store', $storeid);
        if ($use_store_code_in_url) {
            $store_code = Mage::getModel('core/store')->load($storeid)->getCode();
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $store_code . '/';
        } else {
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        }
    }

    function __getFrontendProductUrl($product) {
        return $this->_productcommonpath.$product->getUrlPath();
    }

    private function getChanneltype(){
        $channel_type = "";
        $channel_id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('bridge/incomingchannels');
        $model->load($channel_id);
        $channel_type = $model->getChannelType();
        return $channel_type;
    }

    public function getIncomingPageUrl($product) {
        $channel_type = $this->_productchanneltype;
        $url = "";
        switch ($channel_type) {
            case 'souq':
                $url = $this->__getSouqFrontendProductUrl($product);
                break;
            case 'souq':
                $url = $this->__getSouqFrontendProductUrl($product);
                break;
            default:
                break;
        }
        return $url;
    }

    private function __getSouqFrontendProductUrl($product) {
        $channel_id = $this->getRequest()->getParam('id');
        $entity_id = $product->getId();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $souqdatatable = $resource->getTableName('souq/incomingchannels_souq');
        $sql = "SELECT detail_url FROM $souqdatatable WHERE bridge_channel_id = $channel_id AND id_product_magento = $entity_id LIMIT 1";
        $sqlQuery = $readConnection->query($sql);
        $souq_url = "";
        while ($row = $sqlQuery->fetch()) {
            $souq_url = $row['detail_url'];
        }
        return $souq_url;
    }
    private function __getAmazonFrontendProductUrl($product) {
        $channel_id = $this->getRequest()->getParam('id');
        $entity_id = $product->getId();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $amazondatatable = $resource->getTableName('amazon/incomingchannels_amazon');
        $sql = "SELECT detail_url FROM $amazondatatable WHERE bridge_channel_id = $channel_id AND id_product = $entity_id LIMIT 1";
        $sqlQuery = $readConnection->query($sql);
        $amazon_url = "";
        while ($row = $sqlQuery->fetch()) {
            $amazon_url = $row['detail_url'];
        }
        return $amazon_url;
    }

}

?>