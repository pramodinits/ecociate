<?php

class Fedobe_Bridge_Block_Adminhtml_Incomingchannels_ManagebridgecontentBySku extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {
private $_productcommonpath;
    private $_productchanneltype;

    public function __construct() {
        parent::__construct();
        $this->_productcommonpath = $this->__getCommonfrontentpath();
        $this->_productchanneltype = $this->getChanneltype();
    }
    public function render(Varien_Object $row) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $data = $row->getData();
        $product = Mage::getModel('catalog/product')->setStoreId(0)->loadByAttribute('sku', $data['sku']);
        if (@$product->getId()) {
            $link = $this->getUrl(
                    'adminhtml/managebridgecontent/index', array(
                'product' => $product->getId(),
                'set' => $product->getAttibuteSetId(),
                'type' => $product->getTypeId(),
                'popup' => 1
                    )
            );
            //echo "<pre>";
            $id = $product->getId();//print_r($data);exit;
            $sku = $data['sku'];
            $url = $product->getUrlPath();
            $visible = $product->isVisibleInCatalog() && $product->isVisibleInSiteVisibility();
            $view_link =$this->__getFrontendProductUrl($product);// Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $url;
            $edit_link = 'adminhtml/catalog_product/edit/id/' . $id;
            $edit_link = Mage::helper('bridge')->getUrl($edit_link);

            $arr = '  <a href="' . $link . '" target="' . $id . $sku . '" title="Manage Product Content"><img src="' . $this->getSkinUrl('images/fedobe/Content.png') . '"></a>';
            $arr .= '  <a href="' . $edit_link . '" target="_blank" title="Edit Product"><img src="' . $this->getSkinUrl('images/fedobe/Edit.png') . '"></a>';
            $product_info_table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_history');
            $history_query = 'SELECT * FROM ' . $product_info_table . ' WHERE sku="' . $sku . '" order by created limit 10';
            $res = $readConnection->fetchAll($history_query);
            if (!empty($res)) {
                $history_link = 'adminhtml/edi/history_tab';
                $history_link = Mage::helper('bridge')->getUrl($history_link,array('sku'=>urlencode($sku),'channel_type'=>$data['main_table.channel_type'],'channel_id'=>$data['channel_id']));
                $arr .= '  <a class="anc fancybox.ajax" href="' . $history_link . '" onclick="callfancy();" title="Product Changelog & Information"><img src="' . $this->getSkinUrl('images/fedobe/Information.png') . '"></a>';
            }

            if ($visible)
                $arr .= '  <a href="' . $view_link . '" target="' . $id . '" title="View Product Page"><img src="' . $this->getSkinUrl('images/fedobe/View.png') . '"></a>' . "&nbsp;";
            return $arr;
        }
    }
    private function __getCommonfrontentpath() {
        $websites = Mage::app()->getWebsites();
        $stores = array();
        foreach ($websites as $wk => $wv) {
            $website_id = $wv->getId();
            break;
        }
        $website_id = ($this->getRequest()->getParam('website')) ? $this->getRequest()->getParam('website') : $website_id;
        $websites = Mage::getModel("core/website")->load($website_id);
        foreach ($websites->getStoreIds() as $sk => $sv) {
            $store_id = $sv;
            break;
        }
        $storeid = ($this->getRequest()->getParam('store')) ? $this->getRequest()->getParam('store') : $store_id;
        $use_store_code_in_url = Mage::getStoreConfig('web/url/use_store', $storeid);
        if ($use_store_code_in_url) {
            $store_code = Mage::getModel('core/store')->load($storeid)->getCode();
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $store_code . '/';
        } else {
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        }
    }

    function __getFrontendProductUrl($product) {
        return $this->_productcommonpath.$product->getUrlPath();
    }

    private function getChanneltype(){
        $channel_type = "";
        $channel_id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('bridge/incomingchannels');
        $model->load($channel_id);
        $channel_type = $model->getChannelType();
        return $channel_type;
    }

    public function getIncomingPageUrl($product) {
        $channel_type = $this->_productchanneltype;
        $url = "";
        switch ($channel_type) {
            case 'souq':
                $url = $this->__getSouqFrontendProductUrl($product);
                break;
            default:
                break;
        }
        return $url;
    }

    private function __getSouqFrontendProductUrl($product) {
        $channel_id = $this->getRequest()->getParam('id');
        $entity_id = $product->getId();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $souqdatatable = $resource->getTableName('souq/incomingchannels_souq');
        $sql = "SELECT detail_url FROM $souqdatatable WHERE bridge_channel_id = $channel_id AND id_product_magento = $entity_id LIMIT 1";
        $sqlQuery = $readConnection->query($sql);
        $souq_url = "";
        while ($row = $sqlQuery->fetch()) {
            $souq_url = $row['detail_url'];
        }
        return $souq_url;
    }
    

}
