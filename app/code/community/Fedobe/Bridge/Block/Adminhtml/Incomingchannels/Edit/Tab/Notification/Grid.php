<?php

class Fedobe_Bridge_Block_Adminhtml_incomingchannels_Edit_Tab_Notification_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    /**
     * Constructor
     */
    private $_channeltype;
    public function __construct() {
        parent::__construct();
        $this->setId('importProductListGrid');
        $this->setUseAjax(true);
        $this->setVarNameFilter('importcondproduct_filter');
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('bridge/incomingchannels');
        $model->load($id);
        $channel_type = $model->getChannelType();
        $this->_channeltype = $channel_type;
    }

    /**
     * Prepare collection for grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
  protected function _prepareCollection() {
        $id = $this->getRequest()->getParam('id');
        $collection = Mage::getModel('bridge/incomingchannels_incominglog')->getCollection()->addFieldToFilter('bridge_channel_id', $id);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }


    /**
     * Define grid columns
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns() {
         $attributeCodeConfig = "bridge_channel_id";
        $bridgecontentedited = "bridge_content_edited";
        $this->addColumn('bridge_channel_id', array(
            'header' => Mage::helper('bridge')->__('bridge_channel_id'),
            'index' => 'bridge_channel_id',
            'column_css_class'=>'no-display',
            'header_css_class'=>'no-display'
        ));
        
        
        $this->addColumn('sku', array(
            'header' => Mage::helper('bridge')->__('SKU'),
            'index' => 'sku',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('bridge')->__('Status'),
            'index' => 'status',
            'type' => 'options',
            'options' => Mage::helper('bridge')->getStatus(),
        ));
        
//        $arr_k = array_keys(Mage::helper('status')->status_code());
        $arr_k = array_keys(Mage::helper('status')->getChannelStatusCode($this->_channeltype));
        $status_code=  array_combine($arr_k, $arr_k);

        if (!$this->_isExport) {
           $this->addColumn('status_code', array(
            'header' => Mage::helper('bridge')->__('Status Code'),
            'index' => 'status_code',
            'type' => 'options',
            'options' => $status_code,
            'renderer' => 'Fedobe_Bridge_Block_Adminhtml_Render_Skustatus'
        ));
        } else {
           $this->addColumn('status_code', array(
            'header' => Mage::helper('bridge')->__('Status Code'),
            'index' => 'status_code',
            'type' => 'options',
            'options' => $status_code,
        ));
            $this->addColumn('status_message', array(
                'header' => Mage::helper('bridge')->__('Status Message'),
                'index' => 'status_message',
                'width' => '200px',
                'align' => 'right',
                'renderer' => 'Fedobe_Bridge_Block_Adminhtml_Render_Status_Message'
            ));
        }
        
        
        if (!$this->_isExport) {

            $this->addColumn('action_edit', array(
                'header' => $this->helper('bridge')->__('Action'),
                'sortable' => false,
                'filter' => false,
                'type' => 'action',
                'renderer' => 'Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Actionlog',
            ));
        }
        $this->addExportType('*/incomingchannels/exportLogCsv', Mage::helper('bridge')->__('CSV'));
        $this->addExportType('*/incomingchannels/exportLogExcel', Mage::helper('bridge')->__('Excel XML'));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('product');
        $this->getMassactionBlock()->addItem('removelogs', array(
            'label' => Mage::helper('catalog')->__('Delete Logs'),
            'url' => $this->getUrl('*/incomingchannels/masslog', array('_current' => true)),
        ));
        $this->getMassactionBlock()->addItem('reprocesslogs', array(
            'label' => Mage::helper('catalog')->__('Reprocess All Skiped Data'),
            'url' => $this->getUrl('*/incomingchannels/massreprocesslog', array('_current' => true)),
        ));
        return $this;
    }

    /**
     * Get grid url
     *
     * @return string
     */
    public function getGridUrl() {
        return $this->getUrl('*/incomingchannels/gridnotification', array('_current' => true));
    }
    public function getRowUrl($row) { //echo $row->getId();
//        $id = $row->getEntityId();
//        $link = 'adminhtml/catalog_product/edit/';
//        $id = $row->getId();
//        $link = Mage::helper('bridge')->getUrl($link) . 'id/' . $id;
//        return $link;
    }

}
