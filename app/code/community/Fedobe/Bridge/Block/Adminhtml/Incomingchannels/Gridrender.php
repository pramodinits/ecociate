<?php

class Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Gridrender extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $data = $row->getData();
        $channel_type = $data['channel_type'];
        $id = $data['id'];
        if ($channel_type == 'magento')
            $channel_type = 'incomingchannels';
        $link = 'adminhtml/' . $channel_type . '/edit/';
        $link = Mage::helper('bridge')->getUrl($link) . 'id/' . $id;
        $arr = '<a href="' . $link . '">Edit</a>';
        return $arr;
    }

}
?>

