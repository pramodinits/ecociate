<?php

class Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {
protected $isAdmin;
    public function __construct() {
        parent::__construct();
        $this->isAdmin = Mage::helper('bridge')->isAdmin();
        $this->_objectId = 'id';
        $this->_blockGroup = 'bridge';
        $this->_controller = 'adminhtml_incomingchannels';
        $this->_mode = 'edit';
        if (!$this->isAdmin) {
            $this->_addButton('save_and_continue', array(
                'label' => Mage::helper('adminhtml')->__('Save'),
                'onclick' => 'saveAndContinueEdit();',
                'class' => 'save'
                    ), -101);
        } else {
            $this->_addButton('save_and_continue', array(
                'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit();',
                'class' => 'save'
                    ), -101);
        }

        $store_id = ($this->getRequest()->getParam('store')) ? $this->getRequest()->getParam('store') : Mage_Core_Model_App::ADMIN_STORE_ID;
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/store/{$store_id}/');
            }
        ";
    }

    public function getDeleteUrl() {
        return $this->getUrl('*/incomingchannels/delete', array($this->_objectId => $this->getRequest()->getParam($this->_objectId)));
    }

    public function _prepareLayout() {
        if (!@$this->getRequest()->getParam('id') && !@$this->getRequest()->getParam('channel_type')) {
            $this->_removeButton('save_and_continue');
        }
        if (!$this->isAdmin) {
            $this->_removeButton('save');
            $this->_removeButton('delete');
            $this->_removeButton('back');
        }
        return parent::_prepareLayout();
    }

    public function getHeaderText() {
        if (Mage::registry('incomingchannel_data') && Mage::registry('incomingchannel_data')->getId()) {
            $channelname = $this->getChannelName(Mage::registry('incomingchannel_data')->getChannelType());
            return Mage::helper('bridge')->__('Edit Incomingchannel for "%s" %s', $this->htmlEscape(Mage::registry('incomingchannel_data')->getBrandName()), $channelname);
        } else {
            $channelname = $this->getChannelName($this->getRequest()->getParam('channel_type'));
            return Mage::helper('bridge')->__('New Incomingchannel' . $channelname);
        }
    }

    private function getChannelName($channeltype) {
        if ($channeltype) {
            $incomingchannels = Mage::helper('bridge')->getIncomingAddons();
            $channelname = " (" . $incomingchannels[$channeltype] . ")";
        } else {
            $channelname = '';
        }
        return $channelname;
    }

}