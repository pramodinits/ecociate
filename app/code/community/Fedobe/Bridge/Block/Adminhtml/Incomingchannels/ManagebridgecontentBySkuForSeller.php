<?php

class Fedobe_Bridge_Block_Adminhtml_Incomingchannels_ManagebridgecontentBySkuForSeller extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    private $_productcommonpath;

    public function __construct() {
        parent::__construct();
        $this->_productcommonpath = $this->__getCommonfrontentpath();
    }

    public function render(Varien_Object $row) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $data = $row->getData();
        $inventoryId = $data['id'];
        $arr='';
        $product = Mage::getModel('catalog/product')->setStoreId(0)->loadByAttribute('sku', $data['sku']);
        if (@$product->getId()) {
            $visible = $product->isVisibleInCatalog() && $product->isVisibleInSiteVisibility();
            $view_link = $this->__getFrontendProductUrl($product); // Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $url;


            if ($visible)
                $arr = '  <a href="' . $view_link . '" target="_blank" title="View Product Page"><img src="' . $this->getSkinUrl('images/fedobe/View.png') . '"></a>' . "&nbsp;";
            $arr .= '  <a href="javascript:void(0);" title="Delete Inventory" onclick="deleteInventory(this,'.$inventoryId.')"><img src="' . $this->getSkinUrl('images/fedobe/cancel_icon.gif') . '"></a>' . "&nbsp;";
            return $arr;
        }
    }

    private function __getCommonfrontentpath() {
        $websites = Mage::app()->getWebsites();
        $stores = array();
        foreach ($websites as $wk => $wv) {
            $website_id = $wv->getId();
            break;
        }
        $website_id = ($this->getRequest()->getParam('website')) ? $this->getRequest()->getParam('website') : $website_id;
        $websites = Mage::getModel("core/website")->load($website_id);
        foreach ($websites->getStoreIds() as $sk => $sv) {
            $store_id = $sv;
            break;
        }
        $storeid = ($this->getRequest()->getParam('store')) ? $this->getRequest()->getParam('store') : $store_id;
        $use_store_code_in_url = Mage::getStoreConfig('web/url/use_store', $storeid);
        if ($use_store_code_in_url) {
            $store_code = Mage::getModel('core/store')->load($storeid)->getCode();
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $store_code . '/';
        } else {
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        }
    }

    function __getFrontendProductUrl($product) {
        return $this->_productcommonpath . $product->getUrlPath();
    }

}