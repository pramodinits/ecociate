<?php

class Fedobe_Bridge_Block_Adminhtml_Catalog_Product_Ownprice extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Attributes {

    protected function _prepareForm() {
//        echo $this->getGroup()->getAttributeGroupName() . "<br />";
        parent::_prepareForm();
        if (Mage::registry('product')->getBridgePricenqtyChannelId())
            if (strcasecmp($this->getGroup()->getAttributeGroupName(), 'prices') === 0) {
                $form = $this->getForm();
                $form->addType('script', 'Fedobe_Bridge_Block_Adminhtml_Catalog_Product_Script');
                $sku = Mage::registry('product')->getSku();
                $channel_id = Mage::registry('product')->getBridgePricenqtyChannelId();
                $group_id = $this->getGroup()->getAttributeGroupId();
                $form->addField("$channel_id", 'script', array(
                    'class' => "$sku",
                    'name' => "$channel_id"
                ));
                $this->setForm($form);
            } else if (strcasecmp($this->getGroup()->getAttributeGroupName(), 'General') === 0) {
                $form = $this->getForm();
                $form->addType('supplier_script', 'Fedobe_Bridge_Block_Adminhtml_Catalog_Product_Supplierscript');
                $sku = Mage::registry('product')->getSku();
                $channel_id = Mage::registry('product')->getBridgePricenqtyChannelId();
                $form->addField("$channel_id", 'supplier_script', array(
                    'class' => "$sku",
                    'name' => "$channel_id"
                ));
                $this->setForm($form);
            }
    }

}
