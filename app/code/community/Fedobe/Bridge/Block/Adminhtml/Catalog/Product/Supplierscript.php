<?php

class Fedobe_Bridge_Block_Adminhtml_Catalog_Product_Supplierscript extends Varien_Data_Form_Element_Abstract {

    protected $_element;

    public function getElementHtml() {
        $id = $this->getData('name');
        $sku = $this->getData('class');
        $bridge_inventory = Mage::getmodel('bridge/incomingchannels_product')->getCollection()
                ->addFieldToFilter('channel_id', 0)
                ->addFieldToFilter('sku', $sku)
                ->addFieldToSelect(array('channel_supplier'))
                ->getFirstItem();
        $incoming_channel = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('id', $id)
                ->addFieldToSelect('brand_name')
                ->getFirstItem()
                ->getData();
        $js_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS);
        $jq_url = $js_url . 'fedobe/jquery-1.9.1.min.js';
        $noconflict_url = $js_url . 'lib/jquery/noconflict.js';
        $html = '<script type="text/javascript" src="' . $jq_url . '"></script><script type="text/javascript"  src="' . $noconflict_url . '"></script>'
                . '<script>'
                . 'var channel_suppliers=$j("#suppliers").val();'
                . '$j("#suppliers").prop("disabled",true);'
                . '$j("#suppliers").closest("td").append("<div><input id=\'use_channel_suppliers\' class=\'checkbox\' type=\'checkbox\' checked=\'checked\' value=\'1\'  name=\'product[channel][use_channel_suppliers]\' /><label class=\'normal\' for=\'use_channel_suppliers\'>Use Channel Supplier(Own Store Supplier Was ' . $bridge_inventory['channel_supplier'] . ')</label></div>");'
                . '$j("#use_channel_suppliers").click(function(){'
                . 'if($j(this).is(\':checked\')){'
                . '$j("#suppliers").val(channel_suppliers);'
                . '$j("#suppliers").prop("disabled",true);'
                . '}else{'
                . '$j("#suppliers").prop("disabled",false);'
                . '}'
                . '});'
                . '</script>';
        return $html;
    }

}
?>

