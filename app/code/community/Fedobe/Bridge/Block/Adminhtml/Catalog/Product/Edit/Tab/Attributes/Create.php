<?php

class Fedobe_Bridge_Block_Adminhtml_Catalog_Product_Edit_Tab_Attributes_Create extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Attributes_Create {

    protected function _toHtml() {
        $html = parent::_toHtml();
        $html.= $this->getBridgeManageButtontoHtml();
        return $html;
    }

    public function getBridgeManageButtontoHtml() {
        $html = '&emsp;<button '
                . ($this->getBridgeManageButtonId() ? ' id="' . $this->getBridgeManageButtonId() . '"' : '')
                . ' title="'
                . Mage::helper('core')->quoteEscape("Manage Bridge Content")
                . '"'
                . ' type="button"'
                . ' class="scalable add' . '"'
                . ' onclick="fedobemanagebridgecontent();"'
                . '><span><span><span>' . "Manage Bridge Content" . '</span></span></span></button>' . $this->getAfterHtml();
        $html.= $this->getManageBridgeJscontent();
        return $html;
    }

    public function getBridgeManageButtonId() {
        return 'create_bridge_manage_content_' . $this->getConfig()->getGroupId();
    }

    public function getManageBridgeJscontent() {
        $bridgejs = Mage::helper('adminhtml/js')->getScript(
                "function fedobemanagebridgecontent(){
                var win = window.open('" . $this->getManageBridgeContentUrl() . "', 'manage_bridge_content','width=1000,height=600,resizable=1,scrollbars=1');
                win.focus();
        }\n"
        );
        return $bridgejs;
    }

    public function getManageBridgeContentUrl() {
        return $this->getUrl(
                        'adminhtml/managebridgecontent/index', array(
                    'group' => $this->getConfig()->getGroupId(),
                    'tab' => $this->getConfig()->getTabId(),
                    'store' => $this->getConfig()->getStoreId(),
                    'product' => $this->getConfig()->getProductId(),
                    'set' => $this->getConfig()->getAttributeSetId(),
                    'type' => $this->getConfig()->getTypeId(),
                    'popup' => 1
                        )
        );
    }
}
