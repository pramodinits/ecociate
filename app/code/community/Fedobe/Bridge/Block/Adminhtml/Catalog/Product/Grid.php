<?php

class Fedobe_Bridge_Block_Adminhtml_Catalog_Product_Grid extends Mage_Adminhtml_Block_Catalog_Product_Grid {

    public function setCollection($collection) {
        $store = $this->_getStore();
        $attributeCodeConfig = "bridge_channel_id";
        if ($store->getId() && !isset($this->_joinAttributes[$attributeCodeConfig])) {
            $collection->joinAttribute(
                    $attributeCodeConfig, "catalog_product/$attributeCodeConfig", 'entity_id', null, 'left', $store->getId()
            );
        } else {
            $collection->addAttributeToSelect($attributeCodeConfig);
        }
        $bridgecontentedited = "bridge_content_edited";
        if ($store->getId() && !isset($this->_joinAttributes[$bridgecontentedited])) {
            $collection->joinAttribute(
                    $bridgecontentedited, "catalog_product/$bridgecontentedited", 'entity_id', null, 'left', $store->getId()
            );
        } else {
            $collection->addAttributeToSelect($bridgecontentedited);
        }

        parent::setCollection($collection);
    }

    protected function _prepareColumns() {
        $attributeCodeConfig = "bridge_channel_id";
        $bridgecontentedited = "bridge_content_edited";
        $incomingchannels = Mage::getModel('bridge/incomingchannels')->getCollection();
        $incomingchannels->addFieldToSelect(array('id', 'brand_name'));
        $incomingchanneldata['own_store'] = Mage::getStoreConfig('general/store_information/name');
        ;
        foreach ($incomingchannels as $data) {
            $incomingchanneldata[$data->getId()] = $data->getBrandName();
        }
        $this->addColumnAfter($attributeCodeConfig, array(
            'header' => Mage::helper('catalog')->__('Bridge Channel'),
            'width' => '100px',
            'type' => 'options',
            'index' => $attributeCodeConfig,
            'options' => $incomingchanneldata,
            'filter_condition_callback' => array($this, '_processFilter_channel'),
                ), 'sku');
        $this->addColumnAfter($bridgecontentedited, array(
            'header' => Mage::helper('catalog')->__('Bridge Content Edited'),
            'width' => '100px',
            'type' => 'options',
            'column_css_class' => 'bridge-content-edited',
            'index' => $bridgecontentedited,
            'options' => array('1' => "Yes", '0' => 'No')
                ), $attributeCodeConfig);
        return parent::_prepareColumns();
    }

    protected function _processFilter_channel($collection, $column) {
        $filterprocess = $column->getFilter()->getValue();
        if ($filterprocess == '') {
            return $this;
        }
        if ($filterprocess == 'own_store') {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $bridge_pim_attr = Mage::getResourceModel('eav/entity_attribute')
                    ->getIdByCode('catalog_product', 'bridge_channel_id');
            $store_id = $this->_getStore()->getId();
            $entityIds = $readConnection->fetchAll("SELECT entity_id FROM catalog_product_entity_int WHERE attribute_id=$bridge_pim_attr AND value IS NOT NULL AND store_id=$store_id;");
            $entityIds = $entityIds ? array_column($entityIds, 'entity_id') : array(0);
            $this->getCollection()->addAttributeToFilter('entity_id', array('nin' => $entityIds));
        } else {
            $this->getCollection()->addAttributeToFilter('bridge_channel_id', $filterprocess);
        }
        return;
    }

}
