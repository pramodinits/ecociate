<?php

class Fedobe_Bridge_Block_Adminhtml_Catalog_Product_Script extends Varien_Data_Form_Element_Abstract {

    protected $_element;

    public function getElementHtml() {
        $id = $this->getData('name');
        $sku = $this->getData('class');
        $bridge_inventory = Mage::getmodel('bridge/incomingchannels_product')->getCollection()
                ->addFieldToFilter('channel_id', 0)
                ->addFieldToFilter('sku', $sku)
                ->addFieldToSelect(array('price', 'special_price'))
                ->getFirstItem();
        $incoming_channel = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('id', $id)
                ->addFieldToSelect('brand_name')
                ->getFirstItem()
                ->getData();
        $js_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_JS);
        $jq_url = $js_url . 'fedobe/jquery-1.9.1.min.js';
        $noconflict_url = $js_url . 'lib/jquery/noconflict.js';
        $html = '<script type="text/javascript" src="' . $jq_url . '"></script><script type="text/javascript"  src="' . $noconflict_url . '"></script>'
                . '<script>'
                . 'var channel_price=$j("#price").val();'
                . '$j("#price").prop("disabled","disabled");'
                . '$j("#special_price").prop("disabled","disabled");'
                . '$j("#price").closest("td").append("<div><input id=\'use_channel_price\' class=\'checkbox\' type=\'checkbox\' checked=\'checked\' value=\'1\'  name=\'product[channel][use_channel_price]\' /><label class=\'normal\' for=\'use_channel_price\'>Use Channel price(Own Store Price Was ' . $bridge_inventory['price'] . ')</label></div>");'
                . '$j("#special_price").closest("td").append("<div><input id=\'use_channel_special_price\' class=\'checkbox\' type=\'checkbox\' checked=\'checked\' value=\'1\'  name=\'product[channel][use_channel_special_price]\' /><label class=\'normal\' for=\'use_channel_special_price\'>Use Channel Special price(Own Store Special Price Was ' . $bridge_inventory['special_price'] . ')</label></div>");'
                . '$j("#price").closest("tbody").append("<tr><td class=\'label\'><label>Active Channel</label></td><td class=\'value\'>'
                . '<select class=\'select\' disabled=\'disabled\'><option>' . $incoming_channel['brand_name'] . '</option></select></td>'
                . '<td class=\'scope-label\'><span class=\'nobr\'>[GLOBAL]</span></td></tr>");'
                . '$j("#use_channel_price").click(function(){'
                . 'if($j(this).is(\':checked\')){'
                . '$j("#price").val(channel_price);'
                . '$j("#price").prop("disabled","disabled");'
                . '}else{'
                . '$j("#price").prop("disabled","");'
                . '}'
                . '});'
                . '$j("#use_channel_special_price").click(function(){'
                . 'if($j(this).is(\':checked\')){'
                . '$j("#special_price").val(0);'
                . '$j("#special_price").prop("disabled","disabled");'
                . '}else{'
                . '$j("#special_price").prop("disabled","");'
                . '}'
                . '});'
                . '</script>';
        return $html;
    }

}
?>

