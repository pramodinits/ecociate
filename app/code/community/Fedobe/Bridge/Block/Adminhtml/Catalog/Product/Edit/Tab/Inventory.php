<?php

class Fedobe_Bridge_Block_Adminhtml_Catalog_Product_Edit_Tab_Inventory extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Inventory {

    public function __construct() {
        parent::__construct();
        if (Mage::registry('product')->getBridgePricenqtyChannelId()) {
            $this->setTemplate('fedobe/bridge/catalog/product/tab/inventory.phtml');
        }
    }

}
