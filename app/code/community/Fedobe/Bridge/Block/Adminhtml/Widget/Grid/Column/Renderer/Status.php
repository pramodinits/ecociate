<?php

class Fedobe_Bridge_Block_Adminhtml_Widget_Grid_Column_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        if ($row->getData('channel_type') == '') {
            return '';
        } else {
            $status_array=array(-2=>'New Out of stock',-1=>'New',1 => 'Synced', 0 => 'Changed',2=>'Processing',3=>'No Access',4=>'Trashed');
            return $status_array[parent::render($row)];
        }
    }

}
