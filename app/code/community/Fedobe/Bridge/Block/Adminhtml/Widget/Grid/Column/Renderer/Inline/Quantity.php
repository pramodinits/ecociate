<?php

class Fedobe_Bridge_Block_Adminhtml_Widget_Grid_Column_Renderer_Inline_Quantity extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $html =  "<span class='edit_val'>".parent::render($row)."</span>";

        $html .= '<span class="inlinespan">&nbsp;&nbsp;&nbsp;<a onclick="edit(this,\'qty\');" href="javascript:void(0);" class="editlink">' . Mage::helper('bridge')->__('Edit') . '</a>';
        $html .= '&nbsp;&nbsp;&nbsp;<a onclick="updateQuantity(this, ' . $row->getId() . ');" style="display:none;" href="javascript:void(0);" class="savelink">' . Mage::helper('bridge')->__('Save') . '</a>';
        $html .= '&nbsp;&nbsp;&nbsp;<a onclick="cancelQty(this);" style="display:none;" href="javascript:void(0);" class="cancellink">' . Mage::helper('bridge')->__('Cancel') . '</a></span>';

        return $html;
    }

}
