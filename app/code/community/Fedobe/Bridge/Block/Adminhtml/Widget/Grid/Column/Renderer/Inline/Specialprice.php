<?php

class Fedobe_Bridge_Block_Adminhtml_Widget_Grid_Column_Renderer_Inline_Specialprice extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        if(parent::render($row)!=''){
        $symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
        $symbol=$symbol?$symbol:Mage::app()->getStore()->getCurrentCurrencyCode();
        $html = "<span id='special_price_currency'>$symbol&nbsp;</span>" . "<span class='edit_val'>" . number_format(parent::render($row), 2) . "</span>";
        }else{
            $html = "<span id='special_price_currency'></span>" . "<span class='edit_val'></span>";
        }

        if (!$row->getChannelId()) {
            $html .= '<span class="inlinespan">&nbsp;&nbsp;&nbsp;<a onclick="edit(this);" href="javascript:void(0);" class="editlink">' . Mage::helper('bridge')->__('Edit') . '</a>';
            $html .= '&nbsp;&nbsp;&nbsp;<a onclick="updateSpecialPrice(this, ' . $row->getId() . ');" style="display:none;" href="javascript:void(0);" class="savelink">' . Mage::helper('bridge')->__('Save') . '</a>';
            $html .= '&nbsp;&nbsp;&nbsp;<a onclick="cancelPrice(this);" style="display:none;" href="javascript:void(0);" class="cancellink">' . Mage::helper('bridge')->__('Cancel') . '</a></span>';
        }
        return $html;
    }

}
