<?php

class Fedobe_Bridge_Block_Adminhtml_Widget_Grid_Column_Renderer_Inline_Supplier extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $get_supplier_list = Mage::helper('bridge')->getSupplierList();
        $selected_id = parent::render($row);
        if ($row->getChannelType()=='outgoing') {
            return '';
        }else if (!$row->getChannelId()) {
            $html = "<span class='edit_val'><select class='supplier_inline_edit' disabled='true'>";
            foreach ($get_supplier_list as $supplier_id => $supplier_label) {
                if ($supplier_id == $selected_id)
                    $html.="<option value=" . $supplier_id . " selected='selected'>$supplier_label</option>";
                else
                    $html.="<option value=" . $supplier_id . ">$supplier_label</option>";
            }
            $html.="</select></span>";
            $html .= '<span class="inlinespan">&nbsp;&nbsp;&nbsp;<a onclick="editSupplier(this);" href="javascript:void(0);" class="editlink">' . Mage::helper('bridge')->__('Edit') . '</a>';
            $html .= '&nbsp;&nbsp;&nbsp;<a onclick="updateSupplier(this, ' . $row->getId() . ');" style="display:none;" href="javascript:void(0);" class="savelink">' . Mage::helper('bridge')->__('Save') . '</a>';
            $html .= '&nbsp;&nbsp;&nbsp;<a onclick="cancelSupplier(this);" style="display:none;" href="javascript:void(0);" class="cancellink">' . Mage::helper('bridge')->__('Cancel') . '</a></span>';
            return $html;
        } else {
            return $get_supplier_list[$selected_id];
        }
    }

}
