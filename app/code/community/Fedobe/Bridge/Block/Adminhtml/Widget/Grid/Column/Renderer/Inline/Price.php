<?php

class Fedobe_Bridge_Block_Adminhtml_Widget_Grid_Column_Renderer_Inline_Price extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
        $symbol=$symbol?$symbol:Mage::app()->getStore()->getCurrentCurrencyCode();
        $html = "<span id='price_currency'>$symbol&nbsp;</span>" . "<span class='edit_val'>".number_format($row->getPrice(),2)."</span>";

        $html .= '<span class="inlinespan">&nbsp;&nbsp;&nbsp;<a onclick="edit(this);" href="javascript:void(0);" class="editlink">' . Mage::helper('bridge')->__('Edit') . '</a>';
        $html .= '&nbsp;&nbsp;&nbsp;<a onclick="updatePrice(this, ' . $row->getId() . ');" style="display:none;" href="javascript:void(0);" class="savelink">' . Mage::helper('bridge')->__('Save') . '</a>';
        $html .= '&nbsp;&nbsp;&nbsp;<a onclick="cancelPrice(this);" style="display:none;" href="javascript:void(0);" class="cancellink">' . Mage::helper('bridge')->__('Cancel') . '</a></span>';

        return $html;
    }

}
