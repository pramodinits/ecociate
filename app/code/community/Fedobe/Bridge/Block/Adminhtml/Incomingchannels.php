<?php

class Fedobe_Bridge_Block_Adminhtml_Incomingchannels extends Mage_Adminhtml_Block_Widget_Grid_Container {

    protected $_addButtonLabel = 'Add New Incoming Channel';

    public function __construct() {
        parent::__construct();
        $this->_controller = 'adminhtml_incomingchannels';
        $this->_blockGroup = 'bridge';
        $this->_headerText = Mage::helper('bridge')->__('Manage Incoming Channels');
        $this->isAdmin = true;
        $is_admin = Mage::helper('bridge')->isAdmin();
        if (!$is_admin) {
            $this->isAdmin = false;
        }
    }

    protected function _prepareLayout() {
        $this->setChild('grid', $this->getLayout()->createBlock($this->_blockGroup . '/' . $this->_controller . '_grid', $this->_controller . '.grid')->setSaveParametersInSession(true));
        parent::_prepareLayout();
        if(!$this->isAdmin){
            $this->_removeButton('add');
        }
        return $this;
    }

}