<?php

class Fedobe_Bridge_Helper_Data extends Mage_Core_Helper_Abstract {

    public static function getUrl($route = '', $params = array()) {
        return Mage::getModel('adminhtml/url')->getUrl($route, $params);
    }

    public function getOgcGridUrl() {
        return $this->getUrl('adminhtml/outgoingchannels/index');
    }

    public function getDefaultSettings() {
        $default_settings = Mage::getStoreConfig('bridge/magento_settings');
        return $default_settings;
    }

    public function getModuleInstallationDirectory() {
        return Mage::getModuleDir('', 'Fedobe_Bridge') . "/Instructionguide";
    }

    public function getOutgoingAddons() {
        return array('magento' => 'Magento', 'bridgecsv' => 'CSV', 'bridgeapi' => 'API');
    }

    public function getIncomingAddons() {
        return array('magento' => 'Magento', 'amazon' => 'Amazon', 'incomingbridgecsv' => 'CSV');//, 'souq' => 'Souq'
    }

    public function getOauthConsumerKey($authHeaderValue) {
        if ($authHeaderValue && 'oauth' === strtolower(substr($authHeaderValue, 0, 5))) {
            $authHeaderValue = substr($authHeaderValue, 6); // ignore 'OAuth ' at the beginning

            foreach (explode(',', $authHeaderValue) as $paramStr) {
                $nameAndValue = explode('=', trim($paramStr), 2);

                if (count($nameAndValue) < 2) {
                    continue;
                }
                if ((bool) preg_match('/oauth_[a-z_-]+/', $nameAndValue[0])) {
                    $protocolParams[rawurldecode($nameAndValue[0])] = rawurldecode(trim($nameAndValue[1], '"'));
                }
            }
        }
        return $protocolParams['oauth_consumer_key'];
    }

    public function ModuleHelper($channel_type) {
        $channel_type = ($channel_type == 'incomingbridgecsv') ? 'bridgecsv' : $channel_type;
        return $channel_type;
    }

    public function customPricecalculation($params, $price) {
        $input = $params;
        try {
            $pattern = '/{{([a-z_A-z]*)}}/i';
            $replacement = '$' . '$1';
            $finalstr = (String) preg_replace($pattern, $replacement, $input);
            $nolines = explode(';', $finalstr);
            foreach ($nolines as $key => $value) {
                eval('return ' . $value . ';');
            }
            return round($price, 2);
        } catch (Exception $e) {
            return 0;
        }
    }

    /**
     * update the price rule for the product with the specified incoming channel if any price rule is set
     * @param type $productID
     * @param type $channelId
     */
    public function addPriceRule($channel_info, $entity_id, $edi_price, $type = "inc") {
        $price_rule = isset($channel_info['multi_price_rule']) ? $channel_info['multi_price_rule'] : $channel_info['multiprice_rule'];
        $use_base_price = $price_rule == 'cumulative_price' ? 0 : 1;
        $all_rule = $price_rule == 'cumulative_price' || $price_rule == 'base_price' ? 1 : 0;
        if ($type == "inc") {
            $actions_model = Mage::getmodel('bridge/incomingchannels_action');
            $actions = $actions_model->getCollection()
                    ->addFieldToFilter('brand_id', $channel_info['id'])
                    ->addFieldToFilter('content_type', 1)
                    ->addFieldToSelect(array('type', 'amount', 'conditions_serialized', 'apply', 'custom_price'))
                    ->getData();
        } else {
            $actions_model = Mage::getmodel('bridge/outgoingchannels_action');
            $actions = $actions_model->getCollection()
                    ->addFieldToFilter('channel_id', $channel_info['id'])
                    ->addFieldToFilter('content_type', 1)
                    ->addFieldToSelect(array('type', 'amount', 'conditions_serialized', 'apply', 'custom_price'))
                    ->getData();
        }
        $base_price = $last_price = $edi_price;
        $price = array();
//        return $actions;
        foreach ($actions as $action) {
            Mage::unregister('channel_condition');
            $actions_model->setData(array('product_id' => $entity_id, 'conditions_serialized' => $action['conditions_serialized']));
            Mage::register('channel_condition', $actions_model);
            if (Mage::getmodel('bridge/rule')->getMatchingProductIds()) {
                if ($action['apply'] == 'custom_price') {
                    $custom_price_rule = $action['custom_price'];
                    if ($all_rule) {
                        if ($use_base_price) {
                            $last_price +=$this->customPricecalculation($custom_price_rule, $base_price) - $base_price;
                        } else {
                            $last_price = $this->customPricecalculation($custom_price_rule, $last_price);
                        }
                    } else {
                        $price[] = $this->customPricecalculation($custom_price_rule, $base_price);
                    }
                } else if ($action['type'] == 'increase' && $action['apply'] == 'by_percent') {
                    if ($all_rule) {
                        if ($use_base_price)
                            $last_price += $base_price * ($action['amount'] / 100);
                        else
                            $last_price +=$last_price * $action['amount'] / 100;
                    }else {
                        $price[] = $base_price + ($base_price * $action['amount'] / 100);
                    }
                } else if ($action['type'] == 'decrease' && $action['apply'] == 'by_percent') {
                    if ($all_rule) {
                        if ($use_base_price)
                            $last_price -= $base_price * ($action['amount'] / 100);
                        else
                            $last_price -=$last_price * $action['amount'] / 100;
                    }else {
                        $price[] = $base_price - ($base_price * $action['amount'] / 100);
                    }
                } else if ($action['type'] == 'increase' && $action['apply'] == 'by_fixed') {
                    if ($all_rule) {
                        $last_price += $action['amount'];
                    } else {
                        $price[] = $base_price + $action['amount'];
                    }
                } else if ($action['type'] == 'decrease' && $action['apply'] == 'by_fixed') {
                    if ($all_rule) {
                        $last_price -= $action['amount'];
                    } else {
                        $price[] = $base_price - $action['amount'];
                    }
                }
            }
        }
        $ret_price = $all_rule ? $last_price : (empty($price) ? $base_price : ($price_rule == 'min_price' ? min($price) : max($price)));
        return $ret_price;
    }

    public function getConvertedString($channel_info, $product_id, $type = "inc", $store_id = null, $contentrule = NULL) {
        if ($type == "inc") {
            $actions_model = Mage::getmodel('bridge/incomingchannels_action');
            $actions = $actions_model->getCollection()
                    ->addFieldToFilter('brand_id', $channel_info['id'])
                    ->addFieldToFilter('content_type', 2)
                    ->addFieldToSelect(array('name_rule', 'short_desc_rule', 'desc_rule', 'conditions_serialized', 'apply_on'))
                    ->getData();
        } else {
            $actions_model = Mage::getmodel('bridge/outgoingchannels_action');
            $actions = $actions_model->getCollection()
                    ->addFieldToFilter('channel_id', $channel_info['id'])
                    ->addFieldToFilter('content_type', 2)
                    ->addFieldToSelect(array('name_rule', 'short_desc_rule', 'desc_rule', 'conditions_serialized'))
                    ->getData();
        }
        $result = array();
        $store_id = !is_null($store_id) ? $store_id : Mage::app()
                        ->getWebsite()
                        ->getDefaultGroup()
                        ->getDefaultStoreId();

        $product = Mage::getmodel('catalog/product')->setStoreId($store_id)->load($product_id);
        foreach ($actions as $action) {
            Mage::unregister('channel_condition');
            $actions_model->setData(array('product_id' => $product_id, 'conditions_serialized' => $action['conditions_serialized']));
            Mage::register('channel_condition', $actions_model);
            if (Mage::getmodel('bridge/rule')->getMatchingProductIds()) {
                $result['name'] = $this->replaceString($product, $action['name_rule']);
                $result['description'] = $this->replaceString($product, $action['desc_rule']);
                $result['short_description'] = $this->replaceString($product, $action['short_desc_rule']);
                if (!is_null($contentrule) && $contentrule == 1) {
                    $result['apply_on'] = $action['apply_on'];
                }
                return $result;
            }
        }
    }

    private function replaceString($product, $inpstring) {
        $patternstr = "/\{([a-zA-Z0-9_-])*\}/s";
        $matches = $replaced = array();
        preg_match_all($patternstr, $inpstring, $matches);
        if (isset($matches[0]) && !empty($matches[0])) {
            foreach ($matches[0] as $k => $v) {
                $cleankey = trim(str_replace(array('{', '}'), array('', ''), $v));
                if ($product->getResource()->getAttribute($cleankey))
                    $replaced['{' . $cleankey . '}'] = $product->getResource()->getAttribute($cleankey)->getFrontend()->getValue($product);
            }
        }
        $inpstring = str_replace(array_keys($replaced), array_values($replaced), $inpstring);
        return $inpstring;
    }

    public function getConvertedPrice($channel_id, $sku) {
        $converted_price = Mage::getmodel('bridge/incomingchannels_product')
                ->getCollection()
                ->addFieldToFilter('channel_id', $channel_id)
                ->addFieldToFilter('sku', $sku)
                ->addFieldToFilter('converted_price', 'notnull')
                ->getFirstItem()
                ->getConvertedPrice();
        return $converted_price;
    }

    /**
     * post data through rest api
     * @param type $channel_info
     * @param type $data
     * @return type
     */
    public function postApiRequest($incoming_channel, $data) {
        //oAuth parameters
        $params = array(
            'siteUrl' => $incoming_channel['url'] . '/oauth',
            'requestTokenUrl' => $incoming_channel['url'] . '/oauth/initiate',
            'accessTokenUrl' => $incoming_channel['url'] . '/oauth/token',
            'consumerKey' => $incoming_channel['key'],
            'consumerSecret' => $incoming_channel['secret']
        );

// Get session
        $session = Mage ::getSingleton('core/session');
// Read and unserialize request token from session
//        $requestToken = unserialize($session->getRequestToken());
// Initiate oAuth consumer
        $consumer = new Zend_Oauth_Consumer($params);
        $acessToken = new Zend_Oauth_Token_Access();
//set the permanent token and secret
        $acessToken->setToken($incoming_channel['permanent_token']);
        $acessToken->setTokenSecret($incoming_channel['permanent_secret']);
// Get HTTP client from access token object
        $restClient = $acessToken->getHttpClient($params);

// Set REST resource URL
        $salt = $incoming_channel['salt'] ? base64_encode($incoming_channel['salt']) : 0;
        $parameterForValidation = "salt/{$salt}/salt_type/{$incoming_channel['salt_type']}";


        $restClient->setUri($incoming_channel['url']
                . "/api/rest/postorder");
        $restClient->resetParameters(true);
        $restClient->setRawData(json_encode(array(
            'firstname' => 'jyoti',
            'lastname' => 'sahoo',
            'email' => array('simple@fedobe.com'),
            'password' => 'p455w0rd'
        )));
        // In Magento it is neccesary to set json or xml headers in order to work
        $restClient->setHeaders('Accept', 'application/json');
        $restClient->setHeaders('Content-Type', 'application/json');
        $restClient->setMethod(Zend_Http_Client::POST);
//Make REST request
        $response = $restClient->request();
//        return (array) json_decode($response->getBody());
        echo "<pre>";
        print_r($response->getBody());
        exit;
    }

    public function getSalt(&$incomingchannels) {
        if (empty($incomingchannels['salt'])) {
//save security salt if paid module exist
            if (Mage::helper('core')->isModuleEnabled('Fedobe_Securitycheck')) {
                $securitysalt = Mage::getModel('securitycheck/salt')->loadByIncomingChannel($incomingchannels['id'])->getSalt();
                if ($securitysalt) {
                    $incomingchannels['salt'] = $securitysalt;
                    $incomingchannels['salt_type'] = 'paid';
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            $incomingchannels['salt_type'] = 'free';
        }
        return true;
    }

    function change_edicron_flag($field = "is_processed", $value = 0, $current_otg_channel_id, $channel_type) {
        $resource = Mage::getSingleton('core/resource');
        $write = $resource->getConnection('core_write');
        try {
            $write->raw_query("UPDATE bridge_edi_channelcron SET $field=$value WHERE channel_id=$current_otg_channel_id AND channel_type ='{$channel_type}';");
        } catch (Exception $ex) {
            print_r($ex->getMessage());
            exit;
        }
    }

    function getStatus() {
        /*
          1xx Informational
          2xx Success
          3xx Redirection
          4xx Client Error
          5xx Server Error
          6xxx Unofficial codes
         */
        return array('PIM' => 'PIM', 'New' => 'New', 'Ready' => 'Ready', 'Created' => 'Created', 'Missed' => 'Missed', 'Ediupdtae' => 'Ediupdate');
    }

    public function addProductGalleryImages($mediaArray, $sku, $product) {
        $importDir = Mage::getBaseDir('media') . DS . 'Fedobeamazonimport';
        $importmediaarray = array();
        if (!file_exists('path/to/directory')) {
            mkdir($importDir, 0777, true);
        }
        $mediaimagetype = array('image', 'small_image', 'thumbnail');
        $flagarr = array();
        //Here let's copy from remote server to current store
        foreach ($mediaArray as $k => $imageinfo) {
            $imageinfo = (array) $imageinfo;
            $k++;
            $source = $imageinfo['url'];
            $imginf = pathinfo($source);
            $imagefile = $this->imageExists($importDir, $sku, $imginf['extension'], $k); //echo $imagefile;exit;
            $dest = "$importDir/$imagefile";
            if (!empty($imageinfo['types'])) {
                $types = $imageinfo['types'];
                $flag = 1;
            } else {
                $types = array();
                $flag = 0;
            }
            if (copy($source, $dest)) {
                $importmediaarray[] = array(
                    "filename" => $imagefile,
                    "types" => $types
                );
                $flagarr[] = $flag;
            }
            //exit;
        }
        //Here to to check if not image types found then set the first image as base image, small iamge and thumnail
        if (array_sum($flagarr) == 0) { //echo 34;exit;
            $importmediaarray[0]['types'] = $mediaimagetype;
        }

        foreach ($importmediaarray as $pos => $fileinfo) {
            $filePath = $importDir . '/' . $fileinfo['filename'];
            if (file_exists($filePath)) {
                try {
                    $product->addImageToMediaGallery($filePath, $fileinfo['types'], false, false);
                    unlink($filePath);
                } catch (Exception $e) {
                    echo $e->getMessage();
                }//echo 13;exit;
            } else {
                echo "Product does not have an image or the path is incorrect. Path was: {$filePath}<br/>";
            }
        }
        //echo $product->getSku();exit;
        $product->save();
    }

    public function imageExists($importDir, $sku, $ext, $k) {
        if (!file_exists("$importDir/$sku" . "_$k.$ext")) {
            return "$sku" . "_$k.$ext";
        } else {
            $k = $k . "_" . $k;
            return imageExists($importDir, $sku, $ext, $k);
        }
    }

    function getPIMChannels() {
        $channel_ids = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('product_info', 1)
                ->addFieldToFilter(array(
                    'create_new', //attribute_1 with key 0
                    'update_exist', //attribute_2 with key 1
                        ), array(
                    array('eq' => 1), //condition for attribute_1 with key 0
                    array('eq' => 1), //condition for attribute_2
                        )
                )
                ->addFieldToSelect(array('id', 'weightage'))
                ->getData();
        if ($channel_ids)
            $channel_ids = array_column($channel_ids, 'weightage', 'id');
        return $channel_ids;
    }

    function getBridgeAttributes() {
        return array('bridge_channel_id', 'bridge_content_edited', 'bridge_pricenqty_channel_id', 'channel_priority', 'inc_description', 'inc_name', 'inc_short_description', 'ogc_description', 'ogc_name', 'ogc_price', 'ogc_short_description');
    }

    function getFormattedSku($sku) {
        return strtoupper(trim($sku));
    }

    function getSupplierList() {
        $storeId_for_supplier_attr = 0;
        $config = Mage::getModel('eav/config');
        $supplierattr_code = $this->getSupplierCode();
        $attribute = $config->getAttribute(Mage_Catalog_Model_Product::ENTITY, $supplierattr_code);
        //$values = $attribute->setStoreId($storeId)->getSource()->getAllOptions(); //here is another method to get the attribute value
        $supplier_options = Mage::getResourceModel('eav/entity_attribute_option_collection');
        $Supplier_values = $supplier_options->setAttributeFilter($attribute->getId())->setStoreFilter($storeId_for_supplier_attr)->toOptionArray();
        $supplier_opt_value = array_column($Supplier_values, 'label', 'value');
        return $supplier_opt_value;
    }

    public function getChannelTypeAttributes($sku, $channel_id) {
        $attributes = "";
        if ($channel_id) {
            $model = Mage::getModel('bridge/incomingchannels')->load($channel_id);
            $channel_type = trim($model->getChannelType());
            if ($channel_type) {
                $helperobj = Mage::helper($channel_type);
                $method = "getIncomingAttributeBysku";
                if (method_exists($helperobj, $method)) {
                    $attributes = $helperobj->getIncomingAttributeBysku($sku, $channel_id);
                }
            }
        }
        return $attributes;
    }

    public function getStoreCategoryTreeHtml($list = 0) {
        // Get category collection
        $categories = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToSelect('name')
                ->addAttributeToSort('path', 'asc')
                ->addFieldToFilter('is_active', array('eq' => '1'))
                ->load()
                ->toArray();

        // Arrange categories in required array
        $categoryList = array();
        foreach ($categories as $catId => $category) {
            if (isset($category['name'])) {
                $categoryList[] = array(
                    'label' => htmlspecialchars($category['name']),
                    'level' => htmlspecialchars($category['level']),
                    'value' => $catId
                );
            }
        }
        if ($list) {
            return $categoryList;
        } else {
            $catsring = "";
            foreach ($categoryList as $value) {
                $catName = $value['label'];
                $catId = $value['value'];
                $catLevel = $value['level'];

                $space = '---';
                for ($i = 1; $i < $catLevel; $i++) {
                    $space = $space . "---";
                }
                $catName = $space . $catName;
                $catsring .='<option value="' . $catId . '">' . strtolower($catName) . '</option>';
            }
            return $catsring;
        }
    }

    public function getSupplierCode() {
        $suppliercode = Mage::getStoreConfig('bridge/bridge_supplier_settings/bridge_supplier_attrcode');
        $suppliercode = ($suppliercode) ? trim($suppliercode) : 'suppliers';
        return $suppliercode;
    }

    public function changeEdiProductQty($order_details, $order_status, $order_id) {
        try {
            $entityIdArray = array_keys($order_details);
            $entityId = implode(',', $entityIdArray);
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection("core_read");
            $writeConnection = $resource->getConnection("core_write");
            $productinfo_table_name = $resource->getTableName('bridge/incomingchannels_product');
            $ordersynctable = $resource->getTableName('bridge/ordersync');
            //get the server current time
            $now_time = $readConnection->fetchRow("SELECT NOW() FROM $productinfo_table_name WHERE 1 limit 0,1");
            $now_time = $now_time['NOW()'];

            $entityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
            $bridge_channel_AttrId = Mage::getResourceModel('eav/entity_attribute')
                    ->getIdByCode('catalog_product', 'bridge_pricenqty_channel_id ');
            $bridge_query = "SELECT entity_id,value FROM catalog_product_entity_int WHERE  entity_type_id=$entityTypeId AND entity_id IN ($entityId) AND attribute_id=$bridge_channel_AttrId AND value IS NOT NULL;";
            $bridge_result = $readConnection->fetchAll($bridge_query);
            if ($bridge_result) {
                $bridge_result = array_column($bridge_result, 'value', 'entity_id');
            }
            if ($order_status == Mage_Sales_Model_Order::STATE_CANCELED) {
                $prev_order_details = $readConnection->fetchAll("SELECT channel_id,item_id FROM $ordersynctable WHERE order_id=$order_id;");
                $prev_order_details = $prev_order_details ? array_column($prev_order_details, 'channel_id', 'item_id') : array();
                $cancel_order_query = '';
                $updated_product = array();
                foreach ($order_details as $entityid => $item_details) {
                    if ($prev_order_details[$entityid]) {
                        $channelid = $prev_order_details[$entityid];
                        $cancel_quantity = $order_details[$entityid]['quantity'];
                        $cancel_order_query .="UPDATE $productinfo_table_name SET quantity=(quantity+$cancel_quantity) WHERE product_entity_id=$entityid AND channel_id=$channelid AND channel_type='incoming';";
                        if (isset($bridge_result[$entityid])) {
                            $cancel_order_query .="UPDATE $productinfo_table_name SET is_processed=0 WHERE product_entity_id=$entityid AND channel_id=$bridge_result[$entityid] AND channel_type='incoming';";
                            $updated_product[] = $entityid;
                        }
                    } else {
                        $product = Mage::getModel('catalog/product')->load($entityid);
                        $product->setBridgePricenqtyChannelId(NULL);
                        $product->save();
                    }
                }
                if ($updated_product)
                    $this->updateOutgoingProductsAfterOrder($updated_product, $now_time);
                if ($cancel_order_query) {
                    $writeConnection->raw_query($cancel_order_query);
                }
            } else if ($order_status == Mage_Sales_Model_Order::STATE_NEW) {

                $quantity_query = "SELECT product_id,qty FROM cataloginventory_stock_item WHERE product_id IN ($entityId);";
                $quantity_result = $readConnection->fetchAll($quantity_query);
                $quantity_result = array_column($quantity_result, 'qty', 'product_id');
                $query = '';

                foreach ($entityIdArray as $entity_id) {
                    $qty = intval($quantity_result[$entity_id]);
                    if (isset($bridge_result[$entity_id]))
                        $query.="UPDATE $productinfo_table_name SET quantity={$qty},last_updated_time='{$now_time}' WHERE channel_id=$bridge_result[$entity_id] AND product_entity_id=$entity_id AND channel_type='incoming';";
                    else
                        $query.="UPDATE $productinfo_table_name SET quantity={$qty},last_updated_time='{$now_time}' WHERE channel_id=0 AND product_entity_id=$entity_id;";
                }
                if ($query) {
                    $writeConnection->raw_query($query);
                    //update outgoing channels for which currently ordered products is source
                    $this->updateOutgoingProductsAfterOrder($entityIdArray, $now_time);
                }
            }
        } catch (Exception $ex) {
            echo "<pre>";
            print_r($ex->getMessage());
            print_r($ex->getLine());
            exit;
        }
    }

    public function updateOutgoingProductsAfterOrder($entityIdArray, $now_time = null) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection("core_read");
        $writeConnection = $resource->getConnection("core_write");
        $productinfo_table_name = $resource->getTableName('bridge/incomingchannels_product');
        $bridge_edi_crontable = $resource->getTableName('bridge/bridgeapi_edichannelcron');
        if (is_null($now_time)) {
            $now_time = $readConnection->fetchRow("SELECT NOW() FROM $productinfo_table_name WHERE 1 limit 0,1");
            $now_time = $now_time['NOW()'];
        }
        $inventory_model = Mage::getmodel('bridge/incomingchannels_product');
        $channel_ids_arr = $inventory_model->getCollection()
                        ->addFieldToFilter('product_entity_id', array('in' => $entityIdArray))
                        ->addFieldToFilter('channel_type', 'outgoing')
                        ->addFieldToSelect('channel_id')->getData();
        if ($channel_ids_arr) {
            $channel_ids = array_column($channel_ids_arr, 'channel_id');
            $channel_ids_str = implode(',', $channel_ids);
            $entityId = implode(',', $entityIdArray);
            $writeConnection->raw_query("UPDATE $productinfo_table_name SET quantity=0,is_processed=2,`last_updated_time`='{$now_time}' WHERE  product_entity_id IN ($entityId) AND channel_type='outgoing' AND deleted=0 AND is_processed NOT IN (-1,-2);");
            //update the edi cron table as condition changeds
            $writeConnection->raw_query("UPDATE " . $bridge_edi_crontable . " SET condition_changed=1 WHERE channel_type='outgoing' AND channel_id IN($channel_ids_str);");
        }
    }

    public function isAdmin() {
        $is_seller = Mage::helper('tribeseller')->isSellerLoggedin();
        $is_brand = false;
        if (Mage::helper('core')->isModuleEnabled('Fedobe_Tribebrand')) {
            $is_brand = Mage::helper('tribebrand')->getBrandSellerIdFromadminUser();
        }
        return $is_seller ? false : ($is_brand ? false: true);
        /* $admin_user_session = Mage::getSingleton('admin/session');
          if ($admin_user_session->getUser()) {
          $adminuserId = $admin_user_session->getUser()->getUserId();
          $role_data = Mage::getModel('admin/user')->load($adminuserId)->getRole()->getData();
          if (!empty($role_data) && $role_data['role_name'] == 'Administrators')
          return TRUE;
          else
          return FALSE;
          }else {
          return FALSE;
          } */
    }

}