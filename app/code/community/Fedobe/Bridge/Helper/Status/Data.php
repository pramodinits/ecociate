<?php
class Fedobe_Bridge_Helper_Status_Data extends Mage_Core_Helper_Abstract {
    public function status_code($opt=NULL){
        
        define('QUEUE',"AM200");
        
        define('SKUFOUND',"AM120");
        define('SKUNOTFOUND',"AM121" );
        
        define('ASINFOUND',"AM130" );
        define('ASINNOTFOUND',"AM131" );
        
        define('BRANDFOUND',"AM140" );
        define('BRANDNOTFOUND',"AM141" );
        
        define('ATTR',"AM150" );
        define('IGNORE',"AM153" );//as lookup attribute is same as attribute
        define('NOATTR',"AM151" );
        
        define('IMG',"AM160" );
        define('NOIMG',"AM161" );
        
        define('ATTR_IMG',"AM170" );
        define('IGNORE_IMG',"AM171");
        define('NOATTR_IMG',"AM181");
        
        define('NOATTR_NOIMG',"AM182");
        define('ATTR_NOIMG',"AM183");
        define('IGNORE_NOIMG',"AM184");
        
        define('ASINNOTUPDATE',"AM501");
        define('PRIMEANDPRICECHANGED',"AM510");
        define('PRIMEANDPRICENOTCHANGED',"AM511");
        
        define('NONPRIMEANDPRICECHANGED',"AM520");
        define('NONPRIMEANDPRICENOTCHANGED',"AM521");
        
        define('PRODUCTCREATE',"AM600");
        define('NOTPRODUCTCREATE',"AM601");
        define('NOATTRMATCH',"AM603");
        
        define('UPDATEEXIST',"AM610");
        define('FAILUPDATEEXIST',"AM611");
        define('NOUPDATEEXIST',"AM620");
        if(is_null($opt)){
        $ret= array(
            "AM200"=>'Product is in Queue'
            ,"AM120"=>'Sku found from amazon'
            ,"AM121"=>'Sku not found from amazon'
            ,"AM130"=>'Asin found from amazon'
            ,"AM131"=>'Asin not found from amazon'
            ,"AM140"=>'Brand found from amazon'
            ,"AM141"=>'Brand not found from amazon'
            ,"AM150"=>'Attribute found from amazon'
            ,"AM153"=>'Attribute found from amazon but ignore to process it.'
            ,"AM151"=>'Attribute not found from amazon'
            ,"AM160"=>'Image only found from amazon'
            ,"AM161"=>'Image not found from amazon'
            ,"AM170"=>'Attribute and image found from amazon'
            ,"AM171"=>'Attribute and image found from amazon but ignore to process'
            ,"AM181"=>'No Attribute but image found from amazon'
            ,"AM182"=>'No Attribute No image found from amazon'
            ,"AM183"=>'Attribute found but no image found from amazon'
            ,"AM184"=>'Attribute and image found from amazon'
            ,"AM501"=>'Attribute Found but image not found from amazon,ignore to process'
            ,"AM510"=>'Prime product and price changed'
            ,"AM511"=>'Prime product but price not changed'
            ,"AM520"=>'Non Prime product and price changed'
            ,"AM521"=>'Non Prime product and price not changed'
            ,"AM600"=>'Product created successfully in magento store'
            ,"AM601"=>'Product creation failed in magento store'
            ,"AM603"=>'No attribute matched from mapping for this product'
            ,"AM610"=>'Product updated'
            ,"AM611"=>'Product updation failed'
            ,"AM620"=>'Product is ready to update'
        );
        }else{
            $ret = array(
                0 => array('label' => 'PIM',
                    'value' => array(
                       array('label'=>"AM200",'value'=>"AM200")
                        ,array('label'=>"AM120",'value'=>"AM120")
                        ,array('label'=>"AM121",'value' => 'AM121')
                        ,array('label'=>"AM130",'value'=>"AM130")
                        ,array('label'=>'AM131','value'=>"AM131")
                        ,array('label'=>'AM140','value'=>"AM140")
                        ,array('label'=>'AM141','value'=>"AM141")
                    )
                ),
                1 => array('label' => 'EDIUPDATE', 
                    'value' => array(
                        array('label'=>'AM150','value'=>"AM150")
                        ,array('label'=>'AM153','value'=>"AM153")
                        ,array('label'=>'AM151','value'=>"AM151")
                        ,array('label'=>'AM160','value'=>"AM160")
                        ,array('label'=>'AM161','value'=>"AM161")
                        ,array('label'=>'AM170','value'=>"AM170")
                        ,array('label'=>'AM171','value'=>"AM171")
                        ,array('label'=>'AM181','value'=>"AM181")
                        ,array('label'=>'AM182','value'=>"AM182")
                        ,array('label'=>'AM183','value'=>"AM183")
                        ,array('label'=>'AM184','value'=>"AM184")
                        ,array('label'=>'AM501','value'=>"AM501")
                        ,array('label'=>'AM510','value'=>"AM510")
                        ,array('label'=>'AM511','value'=>"AM511")
                        ,array('label'=>'AM520','value'=>"AM520")
                        ,array('label'=>'AM521','value'=>"AM521")
                    )
                ),
                2 => array('label' => 'Created',
                    'value' => array(
                        array('label'=>'AM600','value'=>"AM600")
                        ,array('label'=>'AM601','value'=>"AM601")
                        ,array('label'=>'AM603','value'=>"AM603")
                        ,array('label'=>'AM610','value'=>"AM610")
                        ,array('label'=>'AM611','value'=>"AM611")
                        ,array('label'=>'AM620','value'=>"AM620")
                    )
                )
            );
        }
            return $ret;
   }
   public function getProductsHtmlSelect()
    {       
        $tmp = Mage::app()->getLayout()->createBlock('core/html_select')
            ->setName('status_code')
            ->setId('status_code')
            ->setClass('validate-select')          
            ->setExtraParams('onchange="getOptions()"'); 
        $select =  $tmp->setOptions($this->status_code(1));      
         //echo $select->getHtml();exit;
        return $select->getHtml();
    }
    
    public function getChannelStatusCode($channel_type){
        $errorcodes = array();
        $helperobj = Mage::helper($channel_type);
        $method = "getErrorCodeList";
        if (method_exists($helperobj, $method)) {
            $errorcodes = $helperobj->getErrorCodeList();
        }
        return $errorcodes;
    }
    public function getChannelStatusCodeWithType($channel_type){
        $errorcodes = array();
        $helperobj = Mage::helper($channel_type);
        $method = "getStatusMsg";
        $status_array=array();
        if (method_exists($helperobj, $method)) {
            $errorcodes = $helperobj->getStatusMsg();
            $errorcode_types=  array_unique(array_values($errorcodes));
            foreach ($errorcode_types as $key => $value) {
                $status_array=array_merge($status_array,array($value=>$value));
                $errorcode=  array_keys($errorcodes, $value);
                $errorcode=  array_combine($errorcode, $errorcode);
                $status_array=array_merge($status_array,$errorcode);
            }
        }
        return $status_array;
    }
}
?>

