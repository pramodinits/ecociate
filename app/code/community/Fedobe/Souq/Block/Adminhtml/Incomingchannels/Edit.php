<?php

class Fedobe_Souq_Block_Adminhtml_Incomingchannels_Edit extends Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Edit {

    public function __construct() {
        parent::__construct();
        $this->_mode = 'edit';
        $channel_data = Mage::registry('incomingchannel_data');
        $am_data = $channel_data->getOrigdata();
        if ($am_data['channel_type'] == 'souq') {
            $this->_removeButton('save');
            $this->_removeButton('back');
            $this->_removeButton('reset');
            $this->_removeButton('sync_inventory');
            $this->_removeButton('save_and_continue');
            $this->_removeButton('savebtn');
            $this->_removeButton('sync_skus');
            $back_url = $this->getUrl('adminhtml/incomingchannels/index');
            $this->_addButton('back', array(
                'label' => Mage::helper('adminhtml')->__('Back'),
               'onclick' => 'setLocation(\'' . $back_url . '\')',
                'class' => 'back',
                    ), 0);
            $this->_addButton('reset', array(
                'label' => Mage::helper('adminhtml')->__('Reset'),
               'onclick' => 'setLocation(window.location.href)',
                'class' => 'reset',
                    ), 1);
            $this->_addButton('savebtn', array(
                'label' => Mage::helper('adminhtml')->__('Save Channel'),
                'onclick' => 'if (confirm(\'Are you sure to save?\')) {validatForm(0)}',
                'class' => 'save',
                    ), -102);
            $this->_addButton('save_and_continue', array(
                'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
                'onclick' => 'if (confirm(\'Are you sure to save?\')) {validatForm(1)}',
                'class' => 'save',
                    ), -103);
            $sync_url = $this->getUrl('adminhtml/souq/product_create', array($this->_objectId => $this->getRequest()->getParam($this->_objectId)));
            //echo 'hello';
            $mapping_data = @Mage::registry('souqmapping_data')->getData();
            $channel_status = @Mage::registry('incomingchannel_data')->getStatus();
//            if (!is_null($this->getRequest()->getParam('id')) && @Mage::registry('incomingchannel_data')->getAuthorized() && $mapping_data && $channel_status) {
//                $this->_addButton('sync_inventory', array(
//                    'label' => Mage::helper('adminhtml')->__('Sync Inventory'),
//                    'onclick' => 'var tab = $j(\'#incomingchannel_tab_name\').val();var url = \''.$sync_url.'\';setLocation(url+\'activeTab/\'+tab);'
//                        ), -101);
//            }
        }
    }

}
