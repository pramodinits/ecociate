<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright  Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * description
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Fedobe_Souq_Block_Adminhtml_Incomingchannels_Edit_Tab_Condition extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel() {
        return Mage::helper('souq')->__('Condition');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle() {
        return Mage::helper('souq')->__('Condition');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab() {
        $channel_data = Mage::registry('incomingchannel_data');
        if ($this->getRequest()->getParam('id') && $channel_data->getChannelType() == 'souq')
            return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden() {
        $channel_data = Mage::registry('incomingchannel_data');
        if (!$this->getRequest()->getParam('id') && $channel_data->getChannelType() != 'souq')
            return false;
    }

    protected function _prepareForm() {
        //Get the category details of producttype details of souq
        $souq_category_opt = Mage::getModel('souq/incomingchannels_souqbrandlist')->getSouqCategoryList();
        //$souq_category_opt = asort($souq_category_opt);
//        print "<pre>";
//        print_r($souq_category_opt);
//        print "</pre>";
        //$souq_brand_opt = Mage::getModel('souq/incomingchannels_souqbrandlist')->getSouqBrandList(258);
        
        $channel_data = Mage::registry('incomingchannel_data');
        $model = Mage::registry('incomingchannel_data');
        $data = $model->getData();
        $souq_condition = unserialize($data['souq_condition']);
        $form = new Varien_Data_Form();
        $condtion_changed =  0 ;
        $fieldset = $form->addFieldset('actions_fieldset', array(
            'legend' => Mage::helper('souq')->__('Souq Condition')
        ));
       
        $fieldset->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
        $basic_cond = $fieldset->addField('basic_condition', 'hbar', array(
            'id' => 'Basic Conditions'
        ));
        
        /* Souq condition form fields */
        $searchtype = $fieldset->addField('searchtype', 'select', array(
            'name' => 'souq_condition[1][searchtype]',
            'label' => Mage::helper('souq')->__('Search Type'),
            'title' => Mage::helper('souq')->__('Search Type'),
            'value' => (isset($souq_condition[1]['searchtype']) && !empty($souq_condition[1]['searchtype']))?$souq_condition[1]['searchtype']:'',
            'options' => array('searchquery'=>'Search Query','skulist'=>'Sku List','brand'=>'Brand'),
            //'note' => 'This setting only have impact while migration starts from souq.Here while seeing the test data and count,prime filter doesnothave any effect .',
            'class' => 'souq-condition-params'
        ));
        $searchquery = $fieldset->addField('q', 'text', array(
            'name' => 'souq_condition[1][q]',
            'label' => Mage::helper('souq')->__('Search query'),
            'title' => Mage::helper('souq')->__('Search query'),
            'value' => (isset($souq_condition[1]['q']) && !empty($souq_condition[1]['q']))?$souq_condition[1]['q']:'',
            'class' => 'souq-condition-params',
            
        ));
        $searchsku = $fieldset->addField('sku_list', 'textarea', array(
            'name' => 'souq_condition[1][sku_list]',
            'label' => Mage::helper('souq')->__('Sku List'),
            'title' => Mage::helper('souq')->__('Sku List'),
            'value' => (isset($souq_condition[1]['sku_list']) && !empty($souq_condition[1]['sku_list']))?$souq_condition[1]['sku_list']:'',
            'class' => 'souq-condition-params',
            
        ));
        $fieldset->addField('condition_changed', 'hidden', array(
            'name' => 'souq_condition[1][condition_changed]',
            'label' => Mage::helper('souq')->__('Condition changed'),
            'title' => Mage::helper('souq')->__('Condition changed'),
            'value' => $condtion_changed,
            'class' => 'souq-condition-changed-param',
            
        ));
        $fieldset->addField('seller', 'text', array(
            'name' => 'souq_condition[1][seller]',
            'label' => Mage::helper('souq')->__('Seller'),
            'title' => Mage::helper('souq')->__('Seller'),
            'value' => (isset($souq_condition[1]['seller']) && !empty($souq_condition[1]['seller']))?$souq_condition[1]['seller']:'',
            'note' => 'Seller Username .',
            'class' => 'souq-condition-params',
            
        ));
        /*
        $fieldset->addField('product_types', 'text', array(
            'name' => 'souq_condition[1][product_types]',
            'label' => Mage::helper('souq')->__('Category'),
            'title' => Mage::helper('souq')->__('Product Types'),
            'value' => (isset($souq_condition[1]['product_types']) && !empty($souq_condition[1]['product_types']))?$souq_condition[1]['product_types']:'',
            'note' => 'Multiple product_type_id (comma separated) ',
            'class' => 'souq-condition-params',
            
        ));
         */
        $fieldset->addField('product_types', 'select', array(
            'name' => 'souq_condition[1][product_types]',
            'label' => Mage::helper('souq')->__('Category'),
            'title' => Mage::helper('souq')->__('Product Types'),
            'value' => (isset($souq_condition[1]['product_types']) && !empty($souq_condition[1]['product_types']))?$souq_condition[1]['product_types']:'',
            'options' => $souq_category_opt,
            //'note' => 'Multiple product_type_id (comma separated) ',
            'class' => 'souq-condition-params',
            
        ));
        $fieldset->addField('advanced_condtion', 'hbar', array(
            'id' => 'Advanced Conditions'
        ));
        
        $fieldset->addField('deals_tags', 'text', array(
            'name' => 'souq_condition[1][deals_tags]',
            'label' => Mage::helper('souq')->__('Deals Tags'),
            'title' => Mage::helper('souq')->__('Deals Tags'),
            'value' => (isset($souq_condition[1]['deals_tags']) && !empty($souq_condition[1]['deals_tags']))?$souq_condition[1]['deals_tags']:'',
            'note' => 'Multiple deals_tags (comma separated). For example all deals tag returns all available deals related to the search query',
            'class' => 'souq-condition-params',
            
        ));
        $fieldset->addField('price_from', 'text', array(
            'name' => 'souq_condition[1][price_from]',
            'label' => Mage::helper('souq')->__('Price From'),
            'title' => Mage::helper('souq')->__('Price From'),
            'value' => (isset($souq_condition[1]['price_from']) && !empty($souq_condition[1]['price_from']))?$souq_condition[1]['price_from']:'',
            'note' => 'Min price search filter',
            'class' => 'souq-condition-params',
            
        ));
       
        $fieldset->addField('price_to', 'text', array(
            'name' => 'souq_condition[1][price_to]',
            'label' => Mage::helper('souq')->__('Price To'),
            'title' => Mage::helper('souq')->__('Price TO'),
            'value' => (isset($souq_condition[1]['price_to']) && !empty($souq_condition[1]['price_to']))?$souq_condition[1]['price_to']:'',
            'note' => 'Max price search filter',
            'class' => 'souq-condition-params',
            
        ));
        
        $fieldset->addField('condition', 'select', array(
            'name' => 'souq_condition[1][condition]',
            'label' => Mage::helper('souq')->__('Condition'),
            'title' => Mage::helper('souq')->__('Condition'),
            'value' => (isset($souq_condition[1]['condition']) && !empty($souq_condition[1]['condition']))?$souq_condition[1]['condition']:'',
            'options' => array('new'=>'new','used'=>'used','refurbished'=>'refurbished','open box new'=>'open box new'),
            //'note' => 'This setting only have impact while migration starts from souq.Here while seeing the test data and count,prime filter doesnothave any effect .',
            'class' => 'souq-condition-params'
        ));
        
        $fieldset->addField('affiliate_id', 'text', array(
            'name' => 'souq_condition[1][affiliate_id]',
            'label' => Mage::helper('souq')->__('Affiliate Id'),
            'title' => Mage::helper('souq')->__('Affiliate Id'),
            'value' => (isset($souq_condition[1]['affiliate_id']) && !empty($souq_condition[1]['affiliate_id']))?$souq_condition[1]['affiliate_id']:'',
            'class' => 'souq-condition-params',
            
        ));
        $fieldset->addField('page', 'text', array(
            'name' => 'souq_condition[1][page]',
            'label' => Mage::helper('souq')->__('Page'),
            'title' => Mage::helper('souq')->__('Page'),
            'value' => (isset($souq_condition[1]['page']) && !empty($souq_condition[1]['page']))?$souq_condition[1]['page']:'',
            'note' => 'Use page to define what page of product results to return .',
            'class' => 'souq-condition-params',
            
        ));
        $fieldset->addField('show', 'text', array(
            'name' => 'souq_condition[1][show]',
            'label' => Mage::helper('souq')->__('Show'),
            'title' => Mage::helper('souq')->__('Show'),
            'value' => (isset($souq_condition[1]['show']) && !empty($souq_condition[1]['show']))?$souq_condition[1]['show']:'',
            'note' => 'How many products you want to show.',
            'class' => 'souq-condition-params',
            
        ));
        $fieldset->addField('sort_by', 'select', array(
            'name' => 'souq_condition[1][sort_by]',
            'label' => Mage::helper('souq')->__('Sort by'),
            'title' => Mage::helper('souq')->__('Sort by'),
            'value' => (isset($souq_condition[1]['sort_by']) && !empty($souq_condition[1]['sort_by']))?$souq_condition[1]['sort_by']:'',
            'options' => array('price_desc'=>'price_desc','price_asc'=>'price_asc','rank_desc'=>'rank_desc','rank_asc'=>'rank_asc'),
            'class' => 'souq-condition-params'
        ));
        $fieldset->addField('show_attributes', 'select', array(
            'name' => 'souq_condition[1][show_attributes]',
            'label' => Mage::helper('souq')->__('Show Attributes'),
            'title' => Mage::helper('souq')->__('Show Attributes'),
            'value' => (isset($souq_condition[1]['show_attributes']) && !empty($souq_condition[1]['show_attributes']))?$souq_condition[1]['show_attributes']:'',
            'options' => array('0'=>'0','1'=>'1'),
            'note' => 'Show/Hide products attributes.',
            'class' => 'souq-condition-params'
        ));
        $fieldset->addField('country', 'select', array(
            'name' => 'souq_condition[1][country]',
            'label' => Mage::helper('souq')->__('Country'),
            'title' => Mage::helper('souq')->__('Country'),
            'value' => (isset($souq_condition[1]['country']) && !empty($souq_condition[1]['country']))?$souq_condition[1]['country']:'',
            'options' => array('ae'=>'ae','eg'=>'eg','sa'=>'sa','kw'=>'kw'),
            'class' => 'souq-condition-params'
        ));
        $fieldset->addField('ship_to', 'select', array(
            'name' => 'souq_condition[1][ship_to]',
            'label' => Mage::helper('souq')->__('Ship to country'),
            'title' => Mage::helper('souq')->__('Ship to country'),
            'value' => (isset($souq_condition[1]['ship_to']) && !empty($souq_condition[1]['ship_to']))?$souq_condition[1]['ship_to']:'',
            'options' => array('qa'=>'qa','bh'=>'bh','om'=>'om'),
            'class' => 'souq-condition-params'
        ));
        $language_fld = $fieldset->addField('language', 'select', array(
            'name' => 'souq_condition[1][language]',
            'label' => Mage::helper('souq')->__('Language'),
            'title' => Mage::helper('souq')->__('Language'),
            'value' => (isset($souq_condition[1]['language']) && !empty($souq_condition[1]['language']))?$souq_condition[1]['language']:'',
            'options' => array('en'=>'en'),
            'class' => 'souq-condition-params'
        ));
        
       $language_fld->setAfterElementHtml($this->getLayout()->createBlock('core/template')->setTemplate('fedobe/souq/incomingchannels/condition_addmore.phtml')->toHtml());
       /*$renderer = Mage::getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
                ->setTemplate('fedobe/souq/incomingchannels/condition_addmore.phtml');
        $fieldset = $form->addFieldset('attributes_filters', array(
                    'legend' => Mage::helper('souq')->__('Attributes Filters')
                ))->setRenderer($renderer);*/ 
       
        $field_nm = $fieldset->addField('format', 'select', array(
            'name' => 'souq_condition[1][format]',
            //'label' => Mage::helper('souq')->__('Format'),
            'title' => Mage::helper('souq')->__('Format'),
            'value' => (isset($souq_condition[1]['format']) && !empty($souq_condition[1]['format']))?$souq_condition[1]['format']:'',
            'options' => array('json'=>'json'),
            'class' => 'souq-condition-params no-display'
        ));
        
        
        /* End of souq condition form fields */
        
        $field_nm->setAfterElementHtml($this->getLayout()->createBlock('core/template')->setTemplate('fedobe/souq/incomingchannels/souq-products.phtml')->toHtml());
        $this->setForm($form);
        $this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
                        ->addFieldMap($searchtype->getHtmlId(), $searchtype->getName())
                        ->addFieldMap($searchquery->getHtmlId(), $searchquery->getName())
                        ->addFieldMap($searchsku->getHtmlId(), $searchsku->getName())
                        ->addFieldDependence( $searchquery->getName(), $searchtype->getName(), 'searchquery')
                        ->addFieldDependence($searchsku->getName(), $searchtype->getName(), 'skulist')
                );
        //unset($hd_searchindexoptions['All']);
       // $hd_searchindexoptions = array_keys($hd_searchindexoptions);
        return $this;
    }

}
