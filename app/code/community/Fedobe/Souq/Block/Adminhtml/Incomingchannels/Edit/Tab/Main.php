<?php

class Fedobe_Souq_Block_Adminhtml_Incomingchannels_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel() {
        return Mage::helper('souq')->__('Settings');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle() {
        return Mage::helper('souq')->__('Settings');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab() {
        if ($this->getRequest()->getParam('id') || @$this->getRequest()->getParam('channel_type'))
            return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden() {
        if (!@$this->getRequest()->getParam('id') && !@$this->getRequest()->getParam('channel_type'))
            return false;
    }

    protected function _prepareForm() {
        $model = Mage::registry('incomingchannel_data');
        if ($model->getId() && !$model->getAuthorized()) {
            $authorize_link = Mage::getBaseUrl() . "/souq/authorize/index/id/" . $model->getId();
        }

        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('incomingchannel_');

        $fieldset = $form->addFieldset('channel_settings', array(
            'legend' => Mage::helper('souq')->__('Channel Settings')
                )
        );

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        }
        $fieldset->addField('tab_name', 'hidden', array(
                'name' => 'tab_name',
            ));
       $fieldset->addField('authorized', 'hidden', array(
        'name'=> 'authorized'
        ));
        if ($this->getRequest()->getParam('channel_type') || $model->getChannelType()) {
            $fieldset->addField('channel_type', 'hidden', array('name' => 'channel_type'));
        }
        /*$fieldset->addType('customtype', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
        $fieldset->addField('general_information', 'customtype', array(
            'name' => 'general_settings'
        ));*/
        $fieldset->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
        $fieldset->addField('general_informations', 'hbar', array(
            'id' => 'General Informations'
        ));
        $field_nm = $fieldset->addField('brand_name', 'text', array(
            'name' => 'brand_name',
            'label' => Mage::helper('souq')->__('Name'),
            'title' => Mage::helper('souq')->__('Name'),
            'required' => true
        ));
        
        $field_nm->setAfterElementHtml($this->getLayout()->createBlock('core/template')->setTemplate('fedobe/souq/incomingchannels/mainjs.phtml')->toHtml());
        $fieldset->addField('mode', 'select', array(
            'name' => 'mode',
            'label' => Mage::helper('souq')->__('Mode'),
            'title' => Mage::helper('souq')->__('Mode'),
            //'options' => array('demo' => Mage::helper('souq')->__('Demo'), 'live' => Mage::helper('souq')->__('Live')),
            'options' => array('demo' => Mage::helper('souq')->__('Dev'), 'live' => Mage::helper('souq')->__('Live')),
            'required' => true,
        ));

         $mapping_data = @Mage::registry('souqmapping_data')->getData();
         $channel_status = $model->getStatus();
         $authorize = $model->getAuthorized();
         $id = $model->getId();
         $sync_url = $this->getUrl('adminhtml/souq/product_create', array('id' => $id)).'acvtiveTab/main_section';
         if($id &&$authorize&&$channel_status&&$mapping_data)
            $btn = '<tr><td></td><td><input type="button" class="scalable form-button" value="Manual Sync" onclick="setLocation(\''.$sync_url.'\')"/></td></tr>';//setLocation('.$sync_url.')
         else
            $btn = '<tr><td></td><td><input type="button" class="scalable" value="Manual Sync" disabled="disabled" onclick=""/></td></tr>';
       $status = $fieldset->addField('status', 'select', array(
            'name' => 'channel_status',
            'label' => Mage::helper('souq')->__('Status'),
            'title' => Mage::helper('souq')->__('Status'),
            'options' => array(1 => Mage::helper('souq')->__('Active'), 0 => Mage::helper('souq')->__('Inactive')),
            'required' => true,
           'after_element_html' => $btn
        ));
       //Add supplier dropdown on 13thApril 2016
        $storeId_for_supplier_attr = 0;
        $config = Mage::getModel('eav/config');
        $attribute = $config->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'suppliers');
        //$values = $attribute->setStoreId($storeId)->getSource()->getAllOptions(); //here is another method to get the attribute value
        $supplier_options = Mage::getResourceModel('eav/entity_attribute_option_collection');
        $Supplier_values = $supplier_options->setAttributeFilter($attribute->getId())->setStoreFilter($storeId_for_supplier_attr)->toOptionArray();
        $supplier_label = array_column($Supplier_values, 'label');
        $supplier_value = array_column($Supplier_values, 'value');
        $supplier_opt_value = array_combine($supplier_value , $supplier_label);
        //fortesting in local set $supplier_opt_value below value
        //$supplier_opt_value = array(403 => "24Diamonds",155 => "Amazon",164 => "Amazon Prime",423 => "Area Trend",612 => "Arush.ae",419 => "Ashford");
        $fieldset->addField('channel_supplier', 'select', array(
            'name' => 'channel_supplier',
            'label' => Mage::helper('souq')->__('Supplier'),
            'title' => Mage::helper('souq')->__('Supplier'),
            'options' => $supplier_opt_value,
            //'value' => $model->getData('channel_supplier') ? $model->getData('channel_supplier') : ""
        ));
        //end
       
       $fieldset->addField('channel_informations', 'hbar', array(
            'id' => 'Channel Informations'
        ));
         $fieldset->addField('label_channeltype', 'select', array(
            'name' => 'label_channeltype',
            'label' => Mage::helper('bridge')->__('Channel Type'),
            'title' => Mage::helper('bridge')->__('Channel Type'),
            'disabled' => true,
            'options' => array(uc_words($model->getChannelType())),
        ));

        $key = $fieldset->addField('key', 'text', array(
            'name' => 'key',
            'label' => Mage::helper('souq')->__('Key'),
            'title' => Mage::helper('souq')->__('Key')
        ));
        $secret = $fieldset->addField('secret', 'text', array(
            'name' => 'secret',
            'label' => Mage::helper('souq')->__('Secret'),
            'title' => Mage::helper('souq')->__('Secret')
        ));
        
        $fieldset_pim = $form->addFieldset('pim_settings', array(
            'legend' => Mage::helper('souq')->__('Catalog Settings')
                )
        );
        
        $fieldset_pim->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
        $fieldset_pim->addField('sync_settings', 'hbar', array(
            'id' => 'Sync Settings'
        ));
        $product_info = $fieldset_pim->addField('product_info', 'select', array(
            'name' => 'product_info',
            'label' => Mage::helper('souq')->__('Sync Product Information'),
            'title' => Mage::helper('souq')->__('Sync Product Information'),
            'options' => array(1 => Mage::helper('souq')->__('Yes'), 0 => Mage::helper('souq')->__('No')),
            //'disabled'=>'disabled;'
        ));
        
        $relative_product = $fieldset_pim->addField('sync_related_product', 'select', array(
            'name' => 'relative_product[1]',
            'label' => Mage::helper('souq')->__('Sync Related Products'),
            'title' => Mage::helper('souq')->__('Sync Related Products'),
            'options' => array(1 => Mage::helper('souq')->__('Yes'), 0 => Mage::helper('souq')->__('No')),
            'disabled'=>'disabled;',
            'class'=>'label-hide'
        ));

        $up_sell = $fieldset_pim->addField('sync_upsell_product', 'select', array(
            'name' => 'relative_product[2]',
            'label' => Mage::helper('souq')->__('Sync Upsell Products'),
            'title' => Mage::helper('souq')->__('Sync Upsell Products'),
            'options' => array(1 => Mage::helper('souq')->__('Yes'), 0 => Mage::helper('souq')->__('No')),
            'disabled'=>'disabled;'
        ));

        $cross_sell = $fieldset_pim->addField('sync_crossell_product', 'select', array(
            'name' => 'relative_product[3]',
            'label' => Mage::helper('souq')->__('Sync Cross Sell Product'),
            'title' => Mage::helper('souq')->__('Sync Cross Sell Product'),
            'options' => array(1 => Mage::helper('souq')->__('Yes'), 0 => Mage::helper('souq')->__('No')),
            'disabled'=>'disabled;'
        ));
        
        $fieldset_pim->addField('basic_settings', 'hbar', array(
            'id' => 'Basic Settings'
        ));
        $fieldset_pim->addField('weightage', 'text', array(
            'name' => 'weightage',
            'label' => Mage::helper('souq')->__('Weightage'),
            'title' => Mage::helper('souq')->__('Weightage'),
            'class' => 'validate-number validate-greater-than-zero unique-weightage',
            'required' => true,
            /*'disabled'=>'disabled;',*/
        ));

        $fieldset_pim->addField('content_source', 'select', array(
            'name' => 'content_source',
            'label' => Mage::helper('souq')->__('Product Content Destination'),
            'title' => Mage::helper('souq')->__('Product Content Destination'),
            //'options' => array('original' => Mage::helper('souq')->__('Replace With original content'), 'copy' => Mage::helper('souq')->__('Keep Separate Copy')),
            'options' => array('copy' => Mage::helper('souq')->__('Keep Separate Incoming Content Only '),'rule_base_original' => Mage::helper('souq')->__('Replace with Rule Based else Leave Original Content'), 'original' => Mage::helper('souq')->__('Replace with Rule Based else Replace With Incoming Content')),
            'required' => true,
            /*'disabled'=>'disabled;',*/
        ));
        $fieldset_pim->addField('product_status', 'select', array(
            'name' => 'product_status',
            'label' => Mage::helper('souq')->__('Default Status of Listed Products'),
            'title' => Mage::helper('souq')->__('Default Status of Listed Products'),
            'options' => array(1 => Mage::helper('souq')->__('Enabled'), 2=> Mage::helper('souq')->__('Disabled')),
            //'required' => true,
            
        ));
        $fieldset_pim->addField('create_new', 'select', array(
            'name' => 'create_new',
            'label' => Mage::helper('souq')->__('Add New Product Automatically'),
            'title' => Mage::helper('souq')->__('Add New Product Automatically'),
            'options' => array(1 => Mage::helper('souq')->__('Yes'), 0 => Mage::helper('souq')->__('No')),
            'required' => true,
            //'disabled'=>'disabled;',
        ));
        $fieldset_pim->addField('update_exist', 'select', array(
            'name' => 'update_exist',
            'label' => Mage::helper('souq')->__('Update Existing Product Automatically'),
            'title' => Mage::helper('souq')->__('Update Existing Product Automatically'),
            'options' => array(1 => Mage::helper('souq')->__('Yes'), 0 => Mage::helper('souq')->__('No')),
            'required' => true,
            //'disabled'=>'disabled;',
        ));
        
        $fieldset_pim->addField('pim_cron', 'select', array(
            'name' => 'pim_cron',
            'label' => Mage::helper('souq')->__('Update Frequency'),
            'title' => Mage::helper('souq')->__('Update Frequency'),
            //'options' => array('never' => Mage::helper('souq')->__('Never'),'0 * * * *'=>Mage::helper('souq')->__('Hourly'), '0 0 * * *'=> Mage::helper('souq')->__('Daily'), '0 0 * * 0' => Mage::helper('souq')->__('Weekly'), '0 0 1 * *' => Mage::helper('souq')->__('Monthly')),
            'options' => array('never' => Mage::helper('souq')->__('Never'), '0 0 * * *'=> Mage::helper('souq')->__('Daily'), '0 0 * * 0' => Mage::helper('souq')->__('Weekly'), '0 0 1 * *' => Mage::helper('souq')->__('Monthly')),
            'required' => true,
        ));
        $fieldset_edi = $form->addFieldset('edi_ettings', array(
            'legend' => Mage::helper('souq')->__('Inventory Settings')
                )
        );
        
        $fieldset_edi->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
        $fieldset_edi->addField('sync_settings_', 'hbar', array(
            'id' => 'Sync Settings'
        ));
        $fieldset_edi->addField('product_pricenquantity', 'select', array(
            'name' => 'product_pricenquantity',
            'label' => Mage::helper('souq')->__('Sync Prices & Quantity'),
            'title' => Mage::helper('souq')->__('Sync Prices & Quantity'),
            'options' => array(1 => Mage::helper('souq')->__('Yes'), 0 => Mage::helper('souq')->__('No')),
            //'disabled'=>'disabled;'
        ));
       
        
        $fieldset_edi->addField('basic_settins_', 'hbar', array(
            'id' => 'Basic Settings'
        ));
         $fieldset_edi->addField('multi_price_rule', 'select', array(
            'name' => 'multi_price_rule',
            'label' => Mage::helper('souq')->__('Multiple Pricing Rules'),
            'title' => Mage::helper('souq')->__('Multiple Pricing Rules'),
            'options' => array('max_price' => Mage::helper('bridge')->__('Take Max Price Only'), 'min_price' => Mage::helper('bridge')->__('Take Min Price Only'), 'base_price' => Mage::helper('bridge')->__('Apply Multiple on Base Price'), 'cumulative_price' => Mage::helper('bridge')->__('Apply Multiple on Cumulative Price')
                             )
        ));
        
         $fieldset_edi->addField('channel_order', 'text', array(
            'name' => 'channel_order',
            'label' => Mage::helper('souq')->__('Order'),
            'title' => Mage::helper('souq')->__('Order'),
            'required' => true,
        ));
        $fieldset_edi->addField('slab', 'text', array(
            'name' => 'slab',
            'label' => Mage::helper('souq')->__('Slab'),
            'title' => Mage::helper('souq')->__('Slab'),
            /*'disabled'=>'disabled;',*/
            'required' => true,
        ));
        $fieldset_edi->addField('edi_cron', 'select', array(
            'name' => 'edi_cron',
            'label' => Mage::helper('souq')->__('Update Frequency'),
            'title' => Mage::helper('souq')->__('Update Frequency'),
            'required' => true,
            'options' => array('never' => Mage::helper('souq')->__('Never'),'0 * * * *'=>Mage::helper('souq')->__('Hourly'), '0 0 * * *'=> Mage::helper('souq')->__('Daily'), '0 0 * * 0' => Mage::helper('souq')->__('Weekly'), '0 0 1 * *' => Mage::helper('souq')->__('Monthly'))
        ));
         $renderer = Mage::getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
                ->setTemplate('fedobe/souq/incomingchannels/settings.phtml');

        $fieldset = $form->addFieldset('mains_fieldset', array(
                    'legend' => Mage::helper('souq')->__('Settings')
                ))->setRenderer($renderer);
        if (@$this->getRequest()->getParam('channel_type')) {
            $model->setChannelType($this->getRequest()->getParam('channel_type'));
        }
        if (!$model->getWeightage()) {
            $model->setWeightage(10);
        }
        if (!$model->getMultiPriceRule()) {
            $model->setMultiPriceRule('sum_on_base');
        }
        $form->setValues($model->getData());
        $this->setForm($form);
        $this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
                        ->addFieldMap($product_info->getHtmlId(), $product_info->getName())
                        ->addFieldMap($relative_product->getHtmlId(), $relative_product->getName())
                        ->addFieldMap($up_sell->getHtmlId(), $up_sell->getName())
                        ->addFieldMap($cross_sell->getHtmlId(), $cross_sell->getName())
                        ->addFieldDependence(
                                $relative_product->getName(), $product_info->getName(), '1'
                        )
                        ->addFieldDependence(
                                $up_sell->getName(), $product_info->getName(), '1'
                        )
                        ->addFieldDependence(
                                $cross_sell->getName(), $product_info->getName(), '1'
        ));
        Mage::dispatchEvent('fedobe_souq_adminhtml_incomingchannels_edit_tab_main_prepare_form', array('form' => $form));

        return parent::_prepareForm();
    }

}
