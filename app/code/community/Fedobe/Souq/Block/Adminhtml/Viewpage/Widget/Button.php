<?php

class Fedobe_Souq_Block_Adminhtml_Viewpage_Widget_Button extends Mage_Adminhtml_Block_Widget_Button {

    /**
     * @var Mage_Catalog_Model_Product Product instance
     */
    private $_product;

    /**
     * Block construct, setting data for button, getting current product
     */
    protected function _construct() {
        $this->_product = Mage::registry('current_product');
        parent::_construct();
        $entity_id = $this->getRequest()->getParam('id')."123";
        $souq_url = $this->__getSouqFrontendProductUrl();
        if ($souq_url) {
            $this->setData(array(
                'label' => Mage::helper('catalog')->__('View Souq Product Page'),
                'onclick' => 'window.open(\'' . $souq_url . '\',' . $entity_id . ')',
                'disabled' => !$this->_isVisible(),
                'title' => (!$this->_isVisible()) ?
                        Mage::helper('catalog')->__('Product is not visible on frontend') :
                        Mage::helper('catalog')->__('View Product Page')
            ));
        }
    }

    /**
     * Checking product visibility
     *
     * @return bool
     */
    private function _isVisible() {
        return $this->_product->isVisibleInCatalog() && $this->_product->isVisibleInSiteVisibility();
    }

    private function __getSouqFrontendProductUrl() {
        $channel_id = $this->_product->getData('bridge_channel_id');
        $entity_id = $this->_product->getId();
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $souqdatatable = $resource->getTableName('souq/incomingchannels_souq');
        $sql = "SELECT detail_url FROM $souqdatatable WHERE bridge_channel_id = $channel_id AND id_product_magento = $entity_id LIMIT 1";
        $sqlQuery = $readConnection->query($sql);
        $souq_url = "";
        while ($row = $sqlQuery->fetch()) {
            $souq_url = $row['detail_url'];
        }
        return $souq_url;
    }
}