<?php
class Fedobe_Souq_Helper_Attribute_Data extends Mage_Core_Helper_Abstract {
    
    public function attributeValueExists($attribute_set_id, $value) {
        $attribute_model = Mage::getModel('eav/entity_attribute');
        $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');
        $ret = false;
        if ($attribute_set_id) {
            $attribute = $attribute_model->load($attribute_set_id);
            $attribute_options_model->setAttribute($attribute);
            $options = $attribute_options_model->getAllOptions(false); //print_r($options);exit;

            foreach ($options as $option) {
                if (strtolower($option['label']) == strtolower($value)) {
                    $ret = $option['value'];
                }
            }
        }//echo "<pre>";print_r($ret);echo "</pre>";exit;
        return $ret;
    }

    public function addProductGalleryImages($mediaArray, $sku, $product) {
        $importDir = Mage::getBaseDir('media') . DS . 'Fedobesouqimport';
        $importmediaarray = array();
        if (!file_exists('path/to/directory')) {
            mkdir($importDir, 0777, true);
        }
        $mediaimagetype = array('image', 'small_image', 'thumbnail');
        $flagarr = array();
        //Here let's copy from remote server to current store
        foreach ($mediaArray as $k => $imageinfo) {
            $imageinfo = (array) $imageinfo;
            $k++;
            $source = $imageinfo['url'];
            $imginf = pathinfo($source);
            $imagefile = $this->imageExists($importDir, $sku, $imginf['extension'], $k); //echo $imagefile;exit;
            $dest = "$importDir/$imagefile";
            if (!empty($imageinfo['types'])) {
                $types = $imageinfo['types'];
                $flag = 1;
            } else {
                $types = array();
                $flag = 0;
            }
            if (copy($source, $dest)) {
                $importmediaarray[] = array(
                    "filename" => $imagefile,
                    "types" => $types
                );
                $flagarr[] = $flag;
            }
            //exit;
        }
        //Here to to check if not image types found then set the first image as base image, small iamge and thumnail
        if (array_sum($flagarr) == 0) { //echo 34;exit;
            $importmediaarray[0]['types'] = $mediaimagetype;
        }

        foreach ($importmediaarray as $pos => $fileinfo) {
            $filePath = $importDir . '/' . $fileinfo['filename'];
            if (file_exists($filePath)) {
                try {
                    $product->addImageToMediaGallery($filePath, $fileinfo['types'], false, false);
                    unlink($filePath);
                } catch (Exception $e) {
                    echo $e->getMessage();
                }//echo 13;exit;
            } else {
                echo "Product does not have an image or the path is incorrect. Path was: {$filePath}<br/>";
            }
        }
        //echo $product->getSku();exit;
        $product->save();
    }

    public function imageExists($importDir, $sku, $ext, $k) {
        if (!file_exists("$importDir/$sku" . "_$k.$ext")) {
            return "$sku" . "_$k.$ext";
        } else {
            $k = $k . "_" . $k;
            return $this->imageExists($importDir, $sku, $ext, $k);
        }
    }
     function attributecode($id) {
        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
        $attr = $attr_model->load($id);
        return $attr->getAttributeCode();
    }
}

