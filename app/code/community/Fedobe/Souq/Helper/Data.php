<?php

class Fedobe_Souq_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getcurrentpriceconversiorate() {
        $currentpricerates = $this->getcurrentConvertionrate();
        return $currentpricerates;
    }

    function clean($string) {
        return preg_replace('/[^A-Za-z0-9\-\s\,]/', '', $string); // Removes special chars.
    }

    function getcurrentConvertionrate() { //echo 234;exit;
        $dom = new DomDocument();
        $products_page_url = "https://www.google.com/finance/converter?a=1&from=USD&to=AED";
        $content = file_get_contents($products_page_url);
        $dom->loadHTML($content);
        $dom->preserveWhiteSpace = false;
        $xpath = new DOMXpath($dom);
        $product_specification = $this->simpleparseToArray($xpath, 'bld');
        return trim($product_specification);
    }

    public function getSouqDataForMappingForm($id) {
        if ($id) {
            //As the SOUQ data been processed let's read and format the data for mapping
            $souqdata = array();
            $resource = Mage::getSingleton('core/resource');
            $souqdatatable = $resource->getTableName('souq/incomingchannels_souq');
            $readConnection = $resource->getConnection('core_read');
            //$sql = $readConnection->select()->from($souqdatatable, array('id_product_souq', 'product_type_id', 'ean', 'name', 'short_description', 'description', 'currency', 'souq_price', 'msrp', 'attributes'))->where("bridge_channel_id=$id");
            $sql = $readConnection->select()->from($souqdatatable, array('id_product_souq', 'product_type_id', 'ean', 'description', 'currency', 'souq_price', 'msrp', 'attributes'))->where("bridge_channel_id=$id");
            $processdata = $readConnection->fetchAll($sql);
            if ($processdata) {
                foreach ($processdata as $k => $v) {
                    foreach ($v as $kk => $vv) {
                        if ($kk == "attributes") {
                            $attributes_unser = unserialize(stripslashes($vv));
                            foreach ($attributes_unser as $attrv) {
                                $label = $this->cleankey(trim($attrv['label']));
                                $value = $this->cleankey(trim($attrv['value']));
                                //$value = $attrv['value'];
                                if ($value != '')
                                    $souqdata[$label][$value] = $value;
                            }
                        }elseif ($kk == "description") {
                            $pattern = '/<li>(.*?)<\/li>/';
                            preg_match_all($pattern, $vv, $matches);
                            $all_desc_attrs = $matches[1];
                            foreach($all_desc_attrs as $all_desc_attr){
                                $des_option_val[] = explode(':',$all_desc_attr);
                            }
                            foreach($des_option_val as $des_key_val){
                                if((count($des_key_val) != 1) && (count($des_key_val) == 2)){
                                    $label = $this->cleankey(trim($des_key_val[0]));
                                    $value = $this->cleankey(trim($des_key_val[1]));
                                    $souqdata[$label][$value] = $value;
                                }
                                if(count($des_key_val) > 2){
                                    $label = $this->cleankey(trim($des_key_val[0]));
                                    $value = "";
                                    $i = 1;
                                    foreach($des_key_val as $des_val_morethan2){
                                        if($i > 1){
                                            $value .= $des_val_morethan2 .":";
                                        }
                                        $i = $i + 1;
                                    }
                                    $value = $this->cleankey(trim($value));
                                    $souqdata[$label][$value] = $value;
                                }
                            }
                        } else {
                            $kk = $this->cleankey(trim($kk));
                            $vv = $this->cleankey(trim($vv));
                            if ($vv != '')
                                $souqdata[$kk][$vv] = $vv;
                        }
                    }
                }
            }
            return $souqdata;
            //} 
        }
    }

    function cleankey($string) {
        $string = str_replace(' ', '-', strtolower($string));
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }

    function getFormattedKey($product_info) {
        $keys = array_keys($product_info);
        $keys = array_map(array($this, 'clean'), $keys);
        $product_info = array_combine($keys, $product_info);
        return $product_info;
    }

    public function getAttributeOptions_bk($store_id) {
        $defaultstoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
        if (!$store_id)
            $store_id = $defaultstoreId;
        $resource = Mage::getSingleton('core/resource');
        $result = array();
        $readConnection = $resource->getConnection('core_read');
        $label_sql1 = "SELECT eav_attribute.attribute_code attribute_id,optvalues.option_id,optvalues.value FROM
        eav_attribute_option AS options  JOIN eav_attribute ON options.attribute_id=eav_attribute.attribute_id JOIN eav_attribute_option_value  AS optvalues ON options.option_id=optvalues.option_id WHERE store_id=$store_id AND eav_attribute.entity_type_id = 4";
        $result = $readConnection->fetchAll($label_sql1);
        return $result;
    }
public function getAttributeOptions($store_id = "") {
        $defaultstoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
        
        //if (!$store_id){
        if(!is_numeric($store_id)){
            $store_id = $defaultstoreId;
        }
        $resource = Mage::getSingleton('core/resource');
        $result = array();
        $readConnection = $resource->getConnection('core_read');
        $label_sql1 = "SELECT eav_attribute.attribute_code attribute_id,optvalues.option_id,optvalues.value FROM
        eav_attribute_option AS options  JOIN eav_attribute ON options.attribute_id=eav_attribute.attribute_id JOIN eav_attribute_option_value  AS optvalues ON options.option_id=optvalues.option_id WHERE store_id=$store_id AND eav_attribute.entity_type_id = 4";
        $result = $readConnection->fetchAll($label_sql1);
        return $result;
    }
    public function getImageDirectory() {
        return Mage::getBaseDir('media') . '/IncomingSouqimages/';
    }

//    public function addProductGalleryImages($mediaArray, $product) {
//        $sku = $product->getSku();
//        $importDir = Mage::helper('souq')->getImageDirectory() . DS . $sku;
//        $importmediaarray = array();
//        if (!file_exists($importDir)) {
//            mkdir($importDir, 0777, true);
//        }
//        $mediaimagetype = array('image', 'small_image', 'thumbnail');
//        $flagarr = array();
//
//        //Here let's copy from remote server to current store
//        foreach ($mediaArray as $k => $source) {
//            //Here to to check if not image types found then set the first image as base image, small iamge and thumnail
//
//            if ($k == 0) { //echo 34;exit;
//                $types = $mediaimagetype;
//            } else {
//                $types = array();
//            }
//            $k++;
//            $source = $importDir . DS . $source;
//            $imagefile = $this->imageExists($importDir, $sku, 'jpg', $k); //echo $imagefile;exit;
//            $dest = "$importDir/$imagefile";
//
//            if (file_exists($source) && file_put_contents($dest, file_get_contents($source))) {
//                $importmediaarray[] = array(
//                    "filename" => $imagefile,
//                    "types" => $types
//                );
//            }
//        }
//
//        if (!$importmediaarray)
//            return false;
//
//        foreach ($importmediaarray as $pos => $fileinfo) {
//            $filePath = $importDir . '/' . $fileinfo['filename'];
//            if (file_exists($filePath)) {
//                try {
//                    $product->addImageToMediaGallery($filePath, $fileinfo['types'], false, false);
//                    unlink($filePath);
//                } catch (Exception $e) {
//                    echo $e->getMessage();
//                }
//            } else {
//                echo "Product does not have an image or the path is incorrect. Path was: {$filePath}<br/>";
//            }
//        }
//        $product->save();
//        return true;
//    }
//
//    public function imageExists($importDir, $sku, $ext, $k) {
//        if (!file_exists("$importDir/$sku" . "_$k.$ext")) {
//            return "$sku" . "_$k.$ext";
//        } else {
//            $k = $k . "_" . $k;
//            return $this->imageExists($importDir, $sku, $ext, $k);
//        }
//    }

}
