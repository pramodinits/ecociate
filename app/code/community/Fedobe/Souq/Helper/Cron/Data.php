<?php

class Fedobe_Souq_Helper_Cron_Data extends Mage_Core_Helper_Abstract {

    //Find exact product according searched sku
    private function getSouqProductSku($products, $sku) {
        if (count($products) > 0) {
            $find = 0;
            foreach ($products as $product) {
                $attributes = $product->attributes;
                foreach ($attributes as $attribute) {
                    if (($attribute->value == $sku) && ($attribute->label == "Model Number")) {
                        $find = 1;
                        break;
                    }
                }
                if ($product->label && !$find) {
                    if (preg_match('/' . $sku . '/', $product->label)) {
                        $find = 1;
                    }
                }
                if ($find) {
                    return $product;
                    break;
                }
            }
        }
    }

    //Insert souq product_id and channel_id  to pretemperary table(bridge_presouqdata) which is fetched by searchquery type or search brand type
    public function insertSouqdataToPreTempTable($channel_id) {
        $souqchannel_object = Mage::getModel('bridge/incomingchannels')->loadById($channel_id);
        $souq_channel_data = $souqchannel_object->getData();
        $edi_update = ($souq_channel_data['product_pricenquantity']) ? 1 : 0;
        $pim_update = ($souq_channel_data['product_pricenquantity'] && (($souq_channel_data['update_exist']) || ($souq_channel_data['create_new']))) ? 1 : 0;
        $souq_channel_condtion_serialize = $souq_channel_data['souq_condition'];
        $souq_channel_condtion = unserialize($souq_channel_condtion_serialize);
        $return_value = 0;
        if ($souq_channel_data['key'] && $souq_channel_data['secret']) {
            if (trim($souq_channel_condtion[1]['q']) || trim($souq_channel_condtion[1]['seller']) || trim($souq_channel_condtion[1]['product_types'])) {
                $souqparams = 1;
            }
            $url = "https://api.souq.com/v1/products";
            if (trim($souq_channel_condtion[1]['q']))
                $url .= "?q=" . trim($souq_channel_condtion[1]['q']);
            if (trim($souq_channel_condtion[1]['seller']))
                $url .= "&seller=" . trim($souq_channel_condtion[1]['seller']);
            if (trim($souq_channel_condtion[1]['product_types']))
                $url .= "&product_types=" . str_replace(',', '%2C', trim($souq_channel_condtion[1]['product_types']));
            if (trim($souq_channel_condtion[1]['deals_tags']))
                $url .= "&deals_tags=" . str_replace(',', '%2C', trim($souq_channel_condtion[1]['deals_tags']));
            if (trim($souq_channel_condtion[1]['price_from']))
                $url .= "&price_from=" . trim($souq_channel_condtion[1]['price_from']);
            if (trim($souq_channel_condtion[1]['price_to']))
                $url .= "&price_to=" . trim($souq_channel_condtion[1]['price_to']);
            if (trim($souq_channel_condtion[1]['condition']))
                $url .= "&condition=" . trim($souq_channel_condtion[1]['condition']);
            if (trim($souq_channel_condtion[1]['souq_condition_attrfilter'][0]['attr_id']) && trim($souq_channel_condtion[1]['souq_condition_attrfilter'][0]['attr_value'])) {
                foreach ($souq_channel_condtion[1]['souq_condition_attrfilter'] as $val) {
                    $url .="&attribute_filter_" . $val['attr_id'] . "=" . urlencode($val['attr_value']);
                }
            }
            if (trim($souq_channel_condtion[1]['affiliate_id']))
                $url .= "&affiliate_id=" . trim($souq_channel_condtion[1]['affiliate_id']);

            if ($souq_channel_condtion[1]['sort_by'])
                $url .= "&sort_by=" . $souq_channel_condtion[1]['sort_by'];

            $url .= "&show_attributes=1"; //for always show attribute in getting souq product on 9thMarch 2016
            if ($souq_channel_condtion[1]['country'])
                $url .= "&country=" . $souq_channel_condtion[1]['country'];
            if ($souq_channel_condtion[1]['ship_to'])
                $url .= "&ship_to=" . $souq_channel_condtion[1]['ship_to'];
            if ($souq_channel_condtion[1]['language'])
                $url .= "&language=" . $souq_channel_condtion[1]['language'];
            $url .= "&format=json"; //alway send data in json format
            $url .= "&app_id=" . $souq_channel_data['key'];
            $url .= "&app_secret=" . $souq_channel_data['secret'];
            //Get Json formated data from the souq api
            if ($souqparams) {
                $jsonData = file_get_contents($url); //old jsondata without quotamanagement
//                $apicall_response_header = $this->get_contents($url);
//                $total_remaining_apicall = $apicall_response_header['total_remaining_call'];
//                $total_remaining_apicall_per_minute = $apicall_response_header['remaining_call_per_minute'];
//                $jsondata = $apicall_response_header['response'];
                $total_product_info = json_decode($jsonData)->meta;
                $showing = $total_product_info->showing ? $total_product_info->showing : 0;
                $total = $total_product_info->total ? $total_product_info->total : 0;
                $totalpage = ceil($total / 24);
                $souq_productsid_str = "";
                for ($i = 1; $i <= $totalpage; $i++) {
                    $newurl = $url . "&page=" . $i;
                    $newurl = $newurl . "&show=" . 24;
                    $jsonData = file_get_contents($newurl);

                    $products = (array) json_decode($jsonData)->data->products;
                    foreach ($products as $product) {
                        $souq_productsid_str .= $product->id . ",";
                    }
                }
                $souq_productsid_new = explode(",", trim($souq_productsid_str, ","));
                $souq_productsid_new_arr = array();
                foreach ($souq_productsid_new as $souq_productid) {
                    $souq_productsid_new_arr[trim($souq_productid)] = trim($souq_productid);
                }

                //get the old productid from the table bridge_souqpredata
                Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
                $resource = Mage::getSingleton('core/resource');
                $readConnection = $resource->getConnection('core_read');
                $writeConnection = $resource->getConnection("core_write");
                $bridge_souqpredata_table = $resource->getTableName('souq/incomingchannels_souqpredata');

                //Get all souqproductid for only the searchtype = query
                $sql = "SELECT id_product_souq  FROM " . $bridge_souqpredata_table . " WHERE sku IS NULL AND channel_id = " . $channel_id;
                $souq_productsid_old = $readConnection->fetchAll($sql);
                $souq_productsid_old_arr = array();
                foreach ($souq_productsid_old as $souq_productid) {
                    $souq_productsid_old_arr[trim($souq_productid['id_product_souq'])] = trim($souq_productid['id_product_souq']);
                }

                $deleted_productid = array_diff($souq_productsid_old_arr, $souq_productsid_new_arr); //flag=removed
                $added_productid_arr = array_diff($souq_productsid_new_arr, $souq_productsid_old_arr); //flag=new
                $updated_productid_arr = array_intersect($souq_productsid_old_arr, $souq_productsid_new_arr); //flag=updated
                //Mark Remove productid which is not in new productid list
                if (count($deleted_productid)) {
                    $deleted_productid_str = implode(",", $deleted_productid);
                    $where = " WHERE id_product_souq IN ($deleted_productid_str) AND channel_id =$channel_id ";
                    $updata_query = "UPDATE bridge_presouqdata SET flag=2,is_processed=0,update_pim={$pim_update},update_edi={$edi_update} " . $where;
                    $writeConnection->raw_query($updata_query);
                    //$writeConnection->delete($bridge_souqpredata_table, $where);
                }
                //Mark update productid which is already in the productid list
                if (count($updated_productid_arr)) {
                    $updated_productid_str = implode(",", $updated_productid_arr);
                    $where = " WHERE id_product_souq IN ($updated_productid_str) AND channel_id =$channel_id ";
                    $updata_query = "UPDATE bridge_presouqdata SET flag=3,is_processed=0,update_pim={$pim_update},update_edi={$edi_update} " . $where;
                    $writeConnection->raw_query($updata_query);
                }
                //added product which new in the new productid list
                foreach ($added_productid_arr as $productid) {
                    $presouq_productid_data = array();
                    $presouq_productid_data['channel_id'] = $channel_id;
                    $presouq_productid_data['id_product_souq'] = $productid;
                    $presouq_productid_data['is_processed'] = 0;
                    $presouq_productid_data['update_pim'] = $pim_update;
                    $presouq_productid_data['update_edi'] = $edi_update;
                    $presouq_productid_data['flag'] = 1;
                    $model_presouqdata = Mage::getModel('souq/incomingchannels_souqpredata');
                    if ($productid) {
                        $model_presouqdata->setData($presouq_productid_data);
                        $model_presouqdata->save();
                    }
                }
                //Change is_processed=1 when all product_id of channel according to the condtion is inserted to bridge_presouqdata
                $updata_query = "UPDATE bridge_souqactivechannels SET is_processed=1,processed_at=NOW() WHERE channel_id = " . $channel_id;
                $writeConnection->raw_query($updata_query);
                //end
                $return_value = 1;
                return $return_value;
            } else {
                return $return_value;
            }
        } else {
            return $return_value;
        }
    }

    //Create cron related function
    public function setXmlCron($crontiming, $cronurl) {
        $cronurl = $this->removeKeyFromUrl($cronurl);
        if (!file_exists($_SERVER["DOCUMENT_ROOT"] . '/bridgecron')) {
            mkdir($_SERVER["DOCUMENT_ROOT"] . '/bridgecron');
            chmod($_SERVER["DOCUMENT_ROOT"] . '/bridgecron', 0777);
        }
        $crons = shell_exec('crontab -l');
        $cron_ready_str = $this->getCronUrl($crontiming, $cronurl);
        $findcron = str_replace('/', '\/', preg_quote($cron_ready_str));
        $pattern = "/$findcron/i";
        $match = preg_match($pattern, $crons);
        if (!$match) {
            $crons .= $cron_ready_str . "\n";
        }
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/bridgecron/crontab.txt', $crons);
        exec("crontab {$_SERVER['DOCUMENT_ROOT']}/bridgecron/crontab.txt");
    }

    public function removeXmlCron($crontiming, $cronurl) {
        $cronurl = $this->removeKeyFromUrl($cronurl);
        $crons = shell_exec('crontab -l');
        $cronurl = $this->getCronUrl($crontiming, $cronurl);
        $crons = str_replace($cronurl . "\n", "", $crons);
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/bridgecron/crontab.txt', $crons);
        exec("crontab {$_SERVER['DOCUMENT_ROOT']}/bridgecron/crontab.txt");
    }

    public function getCronUrl($crontiming, $cronurl) {
        $cronstarturl = 'wget -o /dev/null -q -O -';
        $endcronurl = '> /dev/null';
        $cron_ready_str = "$crontiming " . $cronstarturl . ' ' . $cronurl . ' ' . $endcronurl;
        return $cron_ready_str;
    }

    public function removeKeyFromUrl($url) {
        $test = explode("/", trim($url, "/"));
        $total = count($test);
        unset($test[$total - 1]);
        unset($test[$total - 2]);
        return implode("/", $test);
    }

    //for quota mangent functions below
    public function get_contents($url) {
        $response = file_get_contents($url);
        $headers = $http_response_header;
        foreach ($headers as $k => $v) {
            $value = explode(':', $v);
            if ($value[0] == 'X-Rate-Limit-Limit-Scope-discovery') {
                $call_per_minute = $value[1];
                $headercalltime['call_per_minute'] = $call_per_minute;
            }
            if ($value[0] == 'X-Rate-Limit-Remaining-Quota') {
                $total_remaining_call = $value[1];
                $headercalltime['total_remaining_call'] = $total_remaining_call;
            }
            if ($value[0] == 'X-Rate-Limit-Remaining-Scope-discovery') {
                $remaining_call_per_minute = $value[1];
                $headercalltime['remaining_call_per_minute'] = $remaining_call_per_minute;
            }
        }
        $headercalltime['response'] = $response;
        return $headercalltime;
    }

}
