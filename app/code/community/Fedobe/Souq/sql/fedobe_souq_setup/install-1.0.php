<?php
$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$installer->startSetup();
$installer->run("
CREATE TABLE IF NOT EXISTS `bridge_souqdata` (
  `id` int(11) NOT NULL,
  `id_product_souq` int(11) DEFAULT NULL,
  `id_product_magento` int(11) DEFAULT NULL,
  `product_type_id` int(11) DEFAULT NULL,
  `product_type_label_plural` varchar(255) DEFAULT NULL,
  `bridge_channel_id` int(50) DEFAULT NULL,
  `ean` varchar(255) DEFAULT NULL,
  `name` tinytext DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `souq_price` float(14,2) NULL DEFAULT '0.00',
  `msrp` float(14,2) NULL DEFAULT '0.00',
 
  `attributes` text DEFAULT NULL,
  `lookup_attribute` text DEFAULT NULL,
  `images` text DEFAULT NULL,
  `detail_url` text DEFAULT NULL,
  `attribute_set_id` int(11) DEFAULT NULL,
  `is_updated` int(1) NULL DEFAULT '0',
  `is_price_changed` int(1) NULL DEFAULT '0',
  `is_img_updated` int(1) NULL DEFAULT '0',
  `souq_exclude` int(1) NULL DEFAULT '0',
  `is_processed` int(1) NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);
ALTER TABLE `bridge_souqdata` ADD PRIMARY KEY (`id`);
ALTER TABLE `bridge_souqdata` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;  
ALTER TABLE `bridge_souqdata` ADD UNIQUE(`id_product_souq`);
ALTER TABLE `bridge_souqdata` ADD `short_description` TEXT NULL DEFAULT NULL AFTER `name`;
ALTER TABLE `bridge_souqdata` ADD `description` TEXT NULL DEFAULT NULL AFTER `short_description`;

ALTER TABLE `bridge_incomingchannels` ADD `souq_condition` text NULL AFTER `product_pricenquantity`;
ALTER TABLE `bridge_incomingchannels` ADD `souq_maxmin_price` TEXT NULL AFTER `sync_order`;
ALTER TABLE `bridge_incomingchannels` ADD `souq_pricerange_calculated` INT( 1 ) NULL DEFAULT '0' AFTER `souq_maxmin_price` ;

ALTER TABLE `bridge_channelcron` ADD `souq_condition` varchar(255) NULL AFTER `souq_maxmin_price` ;

CREATE TABLE IF NOT EXISTS `bridge_souqcategory` (
`id` int(11) NOT NULL,
  `category_name` varchar(255)  DEFAULT NULL,
  `category_id` text ,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ;
ALTER TABLE `bridge_souqcategory` ADD PRIMARY KEY (`id`);
ALTER TABLE `bridge_souqcategory` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE IF NOT EXISTS `bridge_souqbrand` (
`id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `brand_attr_id` int(11) DEFAULT NULL,
  `brand_attr_name` varchar(255) DEFAULT NULL,
  `brand_id_value` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE `bridge_souqbrand` ADD PRIMARY KEY (`id`);
ALTER TABLE `bridge_souqbrand` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE IF NOT EXISTS `bridge_presouqdata` (
`id` int(11) NOT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `id_product_souq` int(11) DEFAULT NULL,
  `sku` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_processed` int(1) DEFAULT '0'
);

ALTER TABLE `bridge_presouqdata` ADD PRIMARY KEY (`id`);
ALTER TABLE `bridge_presouqdata` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `bridge_presouqdata` ADD `update_pim` TINYINT(1) NULL DEFAULT '0' AFTER `is_processed`;
ALTER TABLE `bridge_presouqdata` ADD `update_edi` TINYINT(1) NULL DEFAULT '0' AFTER `update_pim`;
ALTER TABLE `bridge_presouqdata` ADD `flag` TINYINT(1) NULL DEFAULT '1' COMMENT '1->Newqualified,2->Removed,3->Updated' AFTER `update_edi`;
ALTER TABLE `bridge_presouqdata` ADD `pim_updatedat` TIMESTAMP NULL DEFAULT NULL AFTER `flag`;
ALTER TABLE `bridge_presouqdata` ADD `edi_updatedat` TIMESTAMP NULL DEFAULT NULL AFTER `pim_updatedat`;

CREATE TABLE IF NOT EXISTS `bridge_souqactivechannels` (
`id` int(11) NOT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `update_frequency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_processed` tinyint(1) DEFAULT '1',
  `pim` tinyint(1) NOT NULL DEFAULT '0',
  `edi` tinyint(1) NOT NULL DEFAULT '0',
  `processed_at` timestamp NULL DEFAULT NULL
);

ALTER TABLE `bridge_souqactivechannels` ADD PRIMARY KEY (`id`);
ALTER TABLE `bridge_souqactivechannels` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `bridge_souqdata` ADD `skipped` TINYINT(1) NULL DEFAULT '0' AFTER `is_processed`;
ALTER TABLE `bridge_souqdata` ADD `flag` TINYINT(1) NULL DEFAULT '1' COMMENT '1->Newqualified,2->Removed,3->Updated' AFTER `skipped`;
");

$installer->endSetup();