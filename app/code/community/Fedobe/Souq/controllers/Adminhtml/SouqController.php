<?php

//require_once Mage::getBaseDir('lib') . "/Souq.php";

class Fedobe_Souq_Adminhtml_SouqController extends Mage_Adminhtml_Controller_Action {

    public $_perminutequota = 60;

    public function _initAction() {
//        $this->loadLayout()->_setActiveMenu('fedobe/souq');
        $this->_title(Mage::helper('souq')->__('Fedobe Extension'));
    }

    public function indexAction() {
        $this->_initAction();
        $this->_title(Mage::helper('bridge')->__('Manage Incoming Channels'));
        $this->loadLayout();
        $this->renderLayout();
    }

    public function preDispatch() {
        $bypassaction = array('souqEdiupdateCron', 'product_create', 'insertSouqProductdetailsToSouqdataTable', 'getSouqActiveChannels', 'saveSouqActiveChannels', 'insertSouqProductdetailsToTempTable', 'getSouqProductidFromSku', 'getSouqBrandName', 'getSouqProductTypeid', 'test', 'skulist_cron', 'importbrand', 'pricerange', 'pricerangeloop', 'product_update', 'massupdate', 'page_crawl', 'cronready', 'cronroute');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }

    //Save souq products to magento store from the temporary table (bridge_souqdata)
    //public function product_create() {//comment this line
    public function product_createAction() {
        //get the souqproduct from the temporary table
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $table = Mage::getSingleton('core/resource')->getTableName('souq/incomingchannels_souq');
        //$sql = "SELECT * FROM " . $table . " WHERE is_processed = 0 AND bridge_channel_id =40 order by id asc LIMIT 1";
        $sql = "SELECT * FROM " . $table . " WHERE is_processed = 0 AND skipped=0 order by id asc LIMIT 1";
        $results = $readConnection->fetchAll($sql); //This souqproduct details    
        $i = 0;
        $product_skipped = 0;
        //check if no product to add in to magento store destory cron product_create
        if (!count($results)) {
            $crontiming = '*/3 * * * *';
            //$cronurl = Mage::getBaseUrl() . "admin/souq/product_create";
            $cronurl = Mage::helper("adminhtml")->getUrl("*/souq/product_create");
            Mage::helper('souqcron')->removeXmlCron($crontiming, $cronurl);
            echo "There is no product to insert from bridge_souqdata table to magento store";
            exit;
        }
        //end
        foreach ($results as $product_data) {
            $souq_product_id = $product_data['id_product_souq'];
            $id_product_magento = $product_data['id_product_magento'];
            $processed_before = ($product_data['id_product_magento']) ? 1 : 0;//product already processed before means magento_store_id is present not null 
            $product_data = $this->getFormattedKey($product_data);
            $product_data['bridge_channel_id'] = $product_data['bridgechannelid'];
            unset($product_data['bridgechannelid']);
            $channel_id = $product_data['bridge_channel_id'];

            if ($channel_id) {
                //Get the channel Info
                $channel_infos = Mage::getModel('bridge/incomingchannels');
                $channel_infos->load($channel_id);
                $channel_data = $channel_infos->getData();
                $temp_product_id = $product_data['id'];
                if (!$channel_infos->getId()) {
                    Mage::getSingleton('adminhtml/session')->addError(
                            Mage::helper('souq')->__('This Channel no longer exists.')
                    );
                    $this->_redirect('*/*');
                    return;
                }
                //check the product wheather present is present in bridge_presouqdata or not ,ifnot then product $quantity=0,bydoing flag=2
                $sql_pretemp = "SELECT * FROM bridge_presouqdata WHERE id_product_souq = {$souq_product_id} AND channel_id={$channel_id} order by id asc LIMIT 1";
                $pretemp_result = $readConnection->fetchAll($sql_pretemp);
                if (!count($pretemp_result)) {
                    $product_data['flag'] = 2;
                }
                //end
                if (($channel_data['product_info'] && ($channel_data['create_new'] || $channel_data['update_exist'])) || $channel_data['product_pricenquantity']) {
                    //Add extra attribute name and value to the produt_data array 
                    $extra_attributes = unserialize(stripslashes($product_data['attributes']));
                    $souqattrdata = array();
                    foreach ($extra_attributes as $attrv) {
                        $label = $this->clean(trim($attrv['label']));
//                    $value = $this->clean(trim($attrv['value']));
//                    $value = $attrv['value'];
//                    if ($value != '') {
                        $souqattrdata[$label] = trim($attrv['value']);
//                    }
                    }
                    $product_data = array_merge($product_data, $souqattrdata);
                    unset($product_data['lookupattribute']);
                    unset($product_data['attributes']);

                    unset($product_data['id']);
                    unset($product_data['idproductmagento']);
                    //end
                    //Add Description attribute to Product_data
                    $description_attr = $product_data['description'];
                    $pattern = '/<li>(.*?)<\/li>/';
                    preg_match_all($pattern, $description_attr, $matches);
                    $all_desc_attrs = $matches[1];

                    foreach ($all_desc_attrs as $all_desc_attr) {
                        $des_option_val[] = explode(':', $all_desc_attr);
                    }
                    foreach ($des_option_val as $des_key_val) {
                        if ((count($des_key_val) != 1) && (count($des_key_val) == 2)) {
                            $label = $this->clean(trim($des_key_val[0]));
//                        $value = $this->clean(trim($des_key_val[1]));
                            //$souqdata[$label][$value] = $value;
                            $souq_desc_attr_data[$label] = trim($des_key_val[1]);
                        }
                        if (count($des_key_val) > 2) {
                            $label = $this->clean(trim($des_key_val[0]));
                            $value = "";
                            $i = 1;
                            foreach ($des_key_val as $des_val_morethan2) {
                                if ($i > 1) {
                                    $value .= $des_val_morethan2 . ":";
                                }
                                $i = $i + 1;
                            }
//                        $value = $this->clean(trim($value));
                            //$souqdata[$label][$value] = $value;
                            $souq_desc_attr_data[$label] = trim($value);
                        }
                    }

                    if ($souq_desc_attr_data)
                        $product_data = array_merge($product_data, $souq_desc_attr_data);
                    //end
                    //get mapping information
                    $mapp_details = Mage::getModel('bridge/incomingchannels_mapping')->loadAllByBrandId($channel_id)->getData();
                    $mapping_info = unserialize($mapp_details[0]['data'])['import'];
                    //check for category mapping
                    $category_mapping_set = 0;
                    $attribute_mapping_set = 0;
                    if ($mapping_info['categorymaptype'] == 1) {
                        $category_mapp_value = $mapping_info['allcategoryintomap'][0];
                        if ($category_mapp_value) {
                            $category_mapping_set = 1;
                        }
                    } else {
                        $category_mapp_value = array_values($mapping_info['category_mapping']);
                        if (is_array($category_mapp_value[0])) {
                            $category_mapping_set = 1;
                        }
                    }
                    //end
                    //check for attribute set mapping
                    if ($mapping_info['attributesettype'] == 1) {
                        $attribute_set_id = $mapping_info['default_attribute_set'];
                        $attribute_mapp_value = $mapping_info['attributes'][$attribute_set_id];
                        if (is_array($attribute_mapp_value)) {
                            if (($attribute_mapp_value['model-number'] == 'sku') || ($attribute_mapp_value['ean'] == 'sku')) {
                                $attribute_mapping_set = 1;
                            }
                        }
                    }
                    //end
                    //mark the record as processed since the mapping of the channel is not done properly
                    if (!$category_mapping_set || !$attribute_mapping_set) {
                        $updata_query = "UPDATE bridge_souqdata SET is_processed=1,skipped=1 WHERE id='{$temp_product_id}'";
                        $writeConnection->raw_query($updata_query); //uncomment
                        $product_skipped = 1;
                        break;
                    }
                    $bridge_attributes = array_flip(Mage::helper('bridge')->getBridgeAttributes());
                    $current_webite_id = Mage::app()->getWebsite()->getId();

                    $mapped_option = array();
                    //attribute set mapping
                    if ($mapping_info['attributesettype'] == 1) {
                        $product_data['attribute_set_id'] = $mapping_info['default_attribute_set'];
                        $attrset_id = $mapping_info['default_attribute_set'];
                        $mapped_option = @$mapping_info['option_mapping']['all'] ? $mapping_info['option_mapping']['all'] : array();
                        $mapped_Attribute = $mapping_info['attributes'][$attrset_id];
                        $custom_maping = $mapping_info['custom_maping'][$attrset_id];
                    } else {
                        if (isset($mapping_info['attributeset'][strtolower($product_data[$mapping_info['csvattrsetcol']])])) {
                            $attrset_id = $mapping_info['attributeset'][strtolower($product_data[$mapping_info['csvattrsetcol']])];
                            $mapped_option = @$mapping_info['option_mapping'][strtolower($product_data[$mapping_info['csvattrsetcol']])] ? $mapping_info['option_mapping'][strtolower($product_data[$mapping_info['csvattrsetcol']])] : array();
                            $mapped_Attribute = $mapping_info['attributes'][strtolower($product_data[$mapping_info['csvattrsetcol']])];
                            $custom_maping = $mapping_info['custom_maping'][strtolower($product_data[$mapping_info['csvattrsetcol']])];
                        } /* else {
                          Mage::getmodel('bridgecsv/data')->setProcessStatus($product_data['id']);
                          exit;
                          } */
                    }
                    //option mapping
                    $raw_ret_options = Mage::helper('souq')->getAttributeOptions(0);
                    $ret_options = array();
                    foreach ($raw_ret_options as $optk => $optv) {
                        $ret_options[$optv['attribute_id']][$optv['option_id']] = htmlspecialchars(strtolower($optv['value']), ENT_QUOTES);
                    }

//              if ($mapped_option) {
                    $product_data = $this->map_option($product_data, $mapped_option, $mapped_Attribute, $ret_options, 1);
//              }
                    //end
                    //cateory mapping
                    if ($mapping_info['categorymaptype'] == 1) {
                        $product_data['category_ids'] = $mapping_info['allcategoryintomap'];
                    } else {
                        if (isset($mapping_info['category_mapping'][strtolower($product_data[$mapping_info['csvcategorycolumnname']])])) {
                            $product_data['category_ids'] = $mapping_info['category_mapping'][strtolower($product_data[$mapping_info['csvcategorycolumnname']])];
                        }
                    }
                    //end
                    //Storeview Mapping
                    if ($mapping_info['storeviewtype'] == 1) {
                        $product_data['store_view'] = $mapping_info['allstoreviewmap'];
                    } else {
                        if (isset($mapping_info['storeview_mapping'][strtolower($product_data[$mapping_info['storeviewmappingcolname']])])) {
                            $product_data['store_view'] = $mapping_info['storeview_mapping'][strtolower($product_data[$mapping_info['storeviewmappingcolname']])];
                        }
                    }
                    //end
                    //attribute map  
                    $mapped_product = $this->change_array_key($product_data, $mapped_Attribute);
                    //end
                    //custom mapping     
                    $custom_maping = array_diff_key($custom_maping, array_flip($mapped_Attribute));
                    $custom_maping = $custom_maping ? $custom_maping : array();
                    $mapped_product = array_merge($mapped_product, $custom_maping);
                    //end
                    //clean(format) sku and here assing sku = model_number if exist else find sku from title
                    $mapped_product['sku'] = $this->clean(trim($mapped_product['sku']));
                    $product_title = $product_data['name'];
                    $split = explode(" ", $product_title);
                    $temp = '';
                    $generateSku = '';
                    foreach ($split as $value) {
                        if($value && ($value != "-"))
                            $generateSku .= $value[0];
                        if($value && ($value != "-") && ($value == strtoupper($value))) {
                            $temp .= "-".trim($value,",");
                        }
                    }
                    $product_sku = trim(trim($temp,","),"-") ? trim(trim($temp,","),"-") : $generateSku;
                    //$this->clean(trim($product_data['ean']));
                    $mapped_product['sku'] = ($mapped_product['sku']) ? $mapped_product['sku'] : $product_sku;
                    //check if product with this sku already exist or not, if exist set new geterated sku 
                    $check_sku = $mapped_product['sku'];
                    $skusuffix = 1;
                    while (Mage::getModel('catalog/product')->getIdBySku($check_sku)) {
                        if($skusuffix==1)
                            $check_sku = $check_sku."-".$skusuffix;
                        $check_sku = substr($check_sku, 0, -1).$skusuffix;
                        $skusuffix++;
                    }
                    $mapped_product['sku'] = $check_sku;

                    //Check wheather the product is exist in magento store or not
                    $pim_update = true;
                    $edi_update = true;
                    $image_update = true;
                    $same_modelnumber_exist = false;
                    if($processed_before){
                        $processed_product =  Mage::getModel('catalog/product')->load($id_product_magento);
                        $processed_sku = $processed_product->getSku();
                        $mapped_product['sku'] = ($processed_sku) ? trim($processed_sku) : $mapped_product['sku'];
                    }
                    $product = Mage::getmodel('catalog/product')->loadByAttribute('sku', $mapped_product['sku']);
                    if ($product) {
                        $exist_entity_id = $product->getId();
                        $table = Mage::getSingleton('core/resource')->getTableName('souq/incomingchannels_souq');
                        $sql = "SELECT * FROM " . $table . " WHERE id_product_magento = " . $exist_entity_id . " AND is_processed = 1 order by id asc LIMIT 1";
                        $exist_productdetails = $readConnection->fetchAll($sql);
                        if (count($exist_productdetails)) {
                            $mapped_product['sku'] = $this->clean(trim($product_data['ean']));
                            $same_modelnumber_exist = true;
                        } else {
                            //Check for edi_update or pim_update if the product is already exist 
                            if ($channel_data['update_exist'] || $channel_data['product_pricenquantity']) {
                                $image_update = false;
                                if (!$channel_data['product_pricenquantity']) {
                                    $edi_update = false;
                                }
                                if (!$channel_data['update_exist']) {
                                    $pim_update = false;
                                }
                            } else {
                                //mark the record as processed and skipped since no eid_update and pim_update in bridge_souqdata table
                                $updata_query = "UPDATE bridge_souqdata SET is_processed=1,skipped=1 WHERE id='{$temp_product_id}'";
                                $writeConnection->raw_query($updata_query); //uncomment
                                $product_skipped = 1;
                                break;
                            }
                        }
                    }

                    $price = $mapped_product['price'] ? $mapped_product['price'] : 100000;
                    //if the product is outofstock then quantity = 0

                    if ($mapped_product['flag'] == 2) {
                        $quantity = 0;
                        unset($mapped_product['flag']);
                    } else {
                        $quantity = isset($mapped_product['fedobe_availability']) && !$mapped_product['fedobe_availability'] ? 0 : (isset($mapped_product['fedobe_quantity']) ? $mapped_product['fedobe_quantity'] : 0);
                    }
                    //end
                    //remove the bridge attribute if any set in the mapped product
                    $mapped_product = array_diff_key($mapped_product, $bridge_attributes);
                    $mapped_product['bridge_channel_id'] = $channel_id;

                    $event = ($product && !$same_modelnumber_exist) ? 'content-updated' : 'created';
                    //For creating new product check wheather addnew product setting is on or off
                    if ($event == 'created' && !$channel_data['create_new']) {
                        $pim_update = false;
                        //mark the record as skipped in bridge_souqdata table since no productinfo,update_exist,createnew,ediupdate
                        $updata_query = "UPDATE bridge_souqdata SET is_processed=1,skipped=1 WHERE id='{$temp_product_id}'";
                        $writeConnection->raw_query($updata_query);
                        $product_skipped = 1;
                        echo "syn product info,create_new,updateexist,product_pricenquantity  are disabled";
                        break;
                    }
                    $mapped_product['inc_description'] = $mapped_product['description'];
                    $mapped_product['inc_name'] = $mapped_product['name'];
                    $mapped_product['inc_short_description'] = $mapped_product['shortdescription'];

                    if (strcasecmp($channel_data['content_source'], 'original') !== 0 && $event != 'created') {
                        unset($mapped_product['description']);
                        unset($mapped_product['name']);
                        unset($mapped_product['shortdescription']);
                    }

                    if ($product && !$same_modelnumber_exist) {
                        unset($mapped_product['status']);
                        //unset($mapped_product['price']);
                        unset($mapped_product['souqprice']);
                        $mapped_product['entity_id'] = $product->getId();
                    } else {
                        if ($channel_data['mode'] == 'demo')
                            $mapped_product['status'] = 2;
                        else {
                            $mapped_product['status'] = $channel_data['product_status'] == 3 ? $mapped_product['status'] : $channel_data['product_status'];
                        }
                        $product = Mage::getModel('catalog/product');
                        $mapped_product['bridge_pricenqty_channel_id'] = $channel_id;
                    }
//                $mapped_product['sku'] = trim($mapped_product['sku']);
//                $mapped_product['sku'] = ($mapped_product['sku']) ? trim($mapped_product['sku']) : trim($product_data['ean']);
                    $mapped_product['website_ids'] = array(1);
                    $store_view_map = $mapped_product['store_view'];
                    $mapped_product['store_id'] = $store_view_map;
                    $mapped_product['type_id'] = 'simple';
                    $mapped_product['tax_class_id'] = 0;
                    $mapped_product['weight'] = ($mapped_product['weight']) ? $mapped_product['weight'] : 0.5;

                    if ($pim_update) {
                        $product->setData($mapped_product);
                    }
                    try {
                        if ($pim_update) {
                            //save the product finally to magento store
                            $product->save(); //uncomment
                            //save the default sotre bridge channel id if store map is not default and product was not exist
                            if ($event == 'created' && $store_view_map != 0) {
                                //$product->setStoreId(0)->addData(array('bridge_channel_id' => $channel_id, 'entity_id' => $product->getId()));
                                $product->setStoreId(0)->addData(array('bridge_channel_id' => $channel_id));
                                $product->save(); //Attribute($product,'bridge_channel_id');//uncomment
                            }


                            //keep this product updation track in history table
                            //$storeview = implode(',', $storeview);
                            $history_data = array('brand_id' => $channel_id, 'sku' => $product->getSku(), 'event' => $event, 'store_view' => $mapped_product['store_view']);
                            $history_model = Mage::getmodel('bridge/incomingchannels_history');
                            $history_model->setData($history_data);
                            $history_model->save(); //uncomment
                            //update the image for this product if its new

                            if ($image_update && $mapped_product['images']) {
                                $img = unserialize(stripslashes($mapped_product['images']));
                                $imge = $img['XL'];
                                foreach ($imge as $k => $v) {
                                    $images[] = array('url' => $v);
                                    //$images[] = array($k => $v);//for specifying image type (small,large,xl,m,xs)
                                }

                                if ($images) {
                                    if (Mage::helper('souqattribute')->addProductGalleryImages($images, $product->getSku(), $product)) {
                                        $image_update = false;
                                    }
                                    /* if (Mage::helper('souq')->addProductGalleryImages($images, $product))
                                      $image_update = false; */
                                }
                            }
                        }
                        //save the data in edi
                        //keep the synced sku price and quantity in edi table
                        $edi_pim_update_time = "";
                        if ($edi_update) {
                            //for price,quantity update in inventory table(bridge_incoming_product_info table)
                            if ($event == 'content-updated') {
                                Mage::getmodel('bridge/incomingchannels_product')->saveSkuInfo($channel_id, $channel_data['currency'], $product->getSku(), $price, $quantity, $channel_data['slab'], $channel_data['channel_order']);
                                $edi_pim_update_time = "edi_updatedat=NOW()";
                            } else {
                                //save the product price and quantity if product isnot prvioulsy exist
                                Mage::getmodel('bridge/incomingchannels_product')->saveSkuInfo($channel_id, $channel_data['currency'], $product->getSku(), $price, $quantity, $channel_data['slab'], $channel_data['channel_order']);
                                if (is_numeric($channel_data['currency'])) {
                                    $converted_price = $price * $channel_data['currency'];
                                } else if ($channel_data['currency']) {
                                    $converted_price = Mage::getmodel('bridge/incomingchannels_product')->currency_conv($price, $channel_data['currency']);
                                } else {
                                    $converted_price = $price;
                                }

                                $converted_price = Mage::helper('bridge')->addPriceRule($channel_data, $product->getId(), $converted_price);
                                //change the product quantity if synced quantity is not 0
                                $is_in_stock = $quantity ? 1 : 0;
                                $product = Mage::getmodel('catalog/product')->load($product->getId());
                                $product->addData(array('price' => $converted_price, 'bridge_pricenqty_channel_id' => $channel_id, 'stock_data' => array('qty' => $quantity, 'is_in_stock' => $is_in_stock), 'entity_id' => $product->getId()));
                                $product->save();
                                //$edi_update = false;
                                Mage::getmodel('bridge/inventory')->changeLiveStatus(array(0 => array('channel_id' => $channel_id, 'sku' => $product->getSku())));
                                //keep this product price updation track in history table
                                $history_data = array('brand_id' => $channel_id, 'sku' => $product->getSku(), 'event' => 'price-updated', 'current_price' => $converted_price, 'current_quantity' => $quantity);
                                $history_model = Mage::getmodel('bridge/incomingchannels_history');
                                $history_model->setData($history_data);
                                $history_model->save();
                                $edi_pim_update_time = "edi_updatedat=NOW()";
                            }
                        }
                    } catch (Exception $ex) {
                        echo "<pre>";
                        print_r($ex->getMessage());
                        exit;
                    }
                    //check the condition for content source
                    if ($pim_update) {
                        //foreach ($product_stviews as $product_data) {
                        $data = array();
                        $product = Mage::getmodel('catalog/product')->setStoreId($store_view_map)->load($product->getId());
                        if (strcasecmp($channel_data['content_source'], 'rule_base_original') === 0) {
                            $contentruledata = Mage::helper('bridge')->getConvertedString(array('id' => $channel_id), $product->getId(), 'inc', $store_view_map);
                            if ($contentruledata['description'])
                                $data['description'] = $contentruledata['description'];
                            if ($contentruledata['short_description'])
                                $data['short_description'] = $contentruledata['short_description'];
                            if ($contentruledata['name'])
                                $data['name'] = $contentruledata['name'];
                        } else if (strcasecmp($channel_data['content_source'], 'original') === 0) {
                            $contentruledata = Mage::helper('bridge')->getConvertedString(array('id' => $channel_id), $product->getId(), 'inc', $store_view_map);
                            $data['name'] = $contentruledata['name'] ? $contentruledata['name'] : $product->getIncName();
                            $data['description'] = $contentruledata['description'] ? $contentruledata['description'] : $product->getIncDescription();
                            $data['short_description'] = $contentruledata['short_description'] ? $contentruledata['short_description'] : $product->getIncShortDescription();
                        }
                        if ($data) {
                            $product->addData($data);
                            $product->save();
                        }
                        if ($edi_pim_update_time != "") {
                            $edi_pim_update_time .= ",pim_updatedat=NOW()";
                        } else {
                            $edi_pim_update_time = "pim_updatedat=NOW()";
                        }
                    }
                    //}
                    //mark the created or updated time of the product in the table bridge_presouqdata
                    if ($edi_pim_update_time != "") {
                        $update_query = "UPDATE bridge_presouqdata SET {$edi_pim_update_time} WHERE id_product_souq={$souq_product_id} AND channel_id ={$channel_id}";
                        $writeConnection->raw_query($update_query); //uncomment
                        echo 'pim edi update==' . $update_query;
                    }
                    //end 
                    //mark the record as processed in bridge_souqdata table
                    $updata_query = "UPDATE bridge_souqdata SET is_processed=1,id_product_magento='{$product->getId()}' WHERE id='{$temp_product_id}'";
                    $writeConnection->raw_query($updata_query); //uncomment
                    echo "<br/>Product having sku " . $product->getSku() . " has inserted<br/>";
                    echo $updata_query;
                    exit;
                } else {
                    //mark the record as skipped in bridge_souqdata table since no productinfo,update_exist,createnew,ediupdate
                    $updata_query = "UPDATE bridge_souqdata SET is_processed=1,skipped=1 WHERE id='{$temp_product_id}'";
                    $writeConnection->raw_query($updata_query);
                    $product_skipped = 1;
                    echo "syn product info,create_new,updateexist,product_pricenquantity  are disabled";
                    break;
                }
            }
            break; //end of loop after adding the product 
        }
        if ($product_skipped) {
            echo "product skipped having id =" . $temp_product_id;
        }
    }

    function map_option($array, $option, $mapped_attribute, $ret_options, $option_create) {
        //map the mpped options
        if ($option):
            foreach ($option as $option_key => $option_value) {
                $array_opt_val = $this->clean($array[$option_key]);
                if (isset($array[$option_key])) {
                    if (isset($option[$option_key][$array_opt_val])) {
                        $array[$option_key] = $option[$option_key][$array_opt_val];
                        unset($ret_options[$mapped_attribute[$option_key]]);
                    }
                }
            }
        endif;
        if (@$ret_options)
            $attribute_intersect = array_intersect_key(array_flip($mapped_attribute), $ret_options);

        if (@$attribute_intersect)
            $attribute_mapped_intersect = array_intersect_key($array, array_flip($attribute_intersect));

        //map the unmapped options for hich only attribute is map
        if (@$attribute_mapped_intersect) {
            foreach ($attribute_mapped_intersect as $csv_attr => $option_val) {

                $option_val = htmlspecialchars(strtolower($option_val), ENT_QUOTES);

                if (in_array($option_val, $ret_options[$mapped_attribute[$csv_attr]])) {
                    $array[$csv_attr] = array_search($option_val, $ret_options[$mapped_attribute[$csv_attr]]);
                } else if ($option_create) {
                    //create new option if not exist
                    $array[$csv_attr] = Mage::getmodel('bridge/incomingchannels')->add_new_option($mapped_attribute[$csv_attr], $option_val);
                }
            }
        }
        return $array;
    }

    function clean($string) {
        $string = str_replace(' ', '-', strtolower($string));
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }

    function getFormattedKey($product_info) {
        $keys = array_keys($product_info);
        $keys = array_map(array($this, 'clean'), $keys);
        $product_info = array_combine($keys, $product_info);
        return $product_info;
    }

    function change_array_key($array, $mapped_Attribute) {
        //        $intersect_keys=  array_intersect_key($array, $mapped_Attribute);
        //        $replace_attribute_key=  array_combine($mapped_Attribute, $intersect_keys);
        foreach ($mapped_Attribute as $bndattr => $storeattr) {
            if (isset($array[$bndattr])) {
                $array[$storeattr] = $array[$bndattr];
                if ($storeattr != $bndattr)
                    unset($array[$bndattr]);
            }
        }
        return $array;
    }

    function editAction() { //echo 'souq temp';exit;
        $this->_initAction();
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('bridge/incomingchannels');
        $souq_model = Mage::getModel('bridge/incomingchannels_mapping');
        if ($id) {
            $model->load($id); //echo "<pre>";print_r($model);echo "</pre>";exit;
            $souq_model->loadAllByBrandId($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('bridge')->__('This Channel no longer exists.')
                );
                $this->_redirect('*/*');
                return;
            }
            $mapping_Data = Mage::getmodel('bridge/incomingchannels_mapping')->getCollection()->addFieldToFilter('brand_id', $id)->addFieldToSelect('data')->getFirstItem()->getData();
            $mapping_Data = @$mapping_Data['data'];
            $store_view = Mage::getmodel('bridge/storeview_label')->getCollection()
                            ->addFieldToSelect(array('value', 'type'))
                            ->addFieldToFilter('brand_id', $id)
                            ->addFieldToFilter('store_id', $this->getCurrentStore())
                            ->addFieldToFilter('type', array('in' => array('meta_title', 'meta_description')))->getData();
            foreach ($store_view as $view) {
                $$view['type'] = $view['value'];
            }
            $model->setMetaTitle(@$meta_title);
            $model->setMetaDescription(@$meta_description);
            $_SESSION['channel_type'] = $model->getChannelType();
            $_SESSION['brand_id'] = $id;
            // $model->setAuthorized(1);
            // $model->setSouqChannelInfoEditbasicCronfrequency('weekly');//echo "<pre>";print_r($model);echo "</pre>";//exit;
        }

        $this->_title($model->getId() ? $model->getBrandName() : $this->__('New Incomingchannel'));
// set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getPageData(true);
        if (!empty($data)) {
            $model->addData($data);
        }
        if (@$mapping_Data) {
            $model->setMappingData($mapping_Data);
        }//echo "<pre>";print_r($model);echo "</pre>";//exit;
        //echo "<pre>";print_r($souq_model);echo "</pre>";
        Mage::register('incomingchannel_data', $model);
        Mage::register('souqmapping_data', $souq_model);
        $this->loadLayout();
        //remove store switcher for new add of a channel
        if (!$id) {
            $this->getLayout()->getBlock('left')->unsetChild('store_switcher');
        }
        $this->renderLayout();
    }

    public function saveAction() {
        if ($this->getRequest()->getPost()) {
            $write = Mage::getSingleton('core/resource')->getConnection('core_write');
            $read = Mage::getSingleton('core/resource')->getConnection('core_read');
            $data = $this->getRequest()->getPost();
            //Get the changed condtion variable and unset it from the data array
            $changed_condition = $data['souq_condition'][1]['condition_changed'];
            unset($data['souq_condition'][1]['condition_changed']);

            //Below code is for the attribute filter with unsetting the blank filter on 5th March 2016
            $filter_with_no_blankvalue = array_map('array_filter', $data['souq_condition_attrfilter']);
            //for unseting key which have blank array
            foreach ($filter_with_no_blankvalue as $k => $v) {
                if (!count($v)) {
                    unset($data['souq_condition_attrfilter'][$k]);
                }
            }
            //this is for reindexing the array
            $data['souq_condition_attrfilter'] = array_values($data['souq_condition_attrfilter']);
            //Below code is for the attribute filter with unsetting the blank filter on 5th March 2016
            //check wheather attr_id and attr_value is set or not
            $attr_id_and_value_set = 0;
            if ($data['souq_condition_attrfilter'][0]['attr_id'] && $data['souq_condition_attrfilter'][0]['attr_value']) {
                $attr_id_and_value_set = 1;
            }
            //end

            $data['souq_condition'][1]['souq_condition_attrfilter'] = $data['souq_condition_attrfilter'];
            unset($data['souq_condition_attrfilter']);

            try {
                $sku_list = array();
                Mage::getSingleton('adminhtml/session')->setPageData($data);
                $model = Mage::getModel('bridge/incomingchannels');
                $model->load(@$this->getRequest()->getParam('id'));
                //$prev_pim_cron = $model->getPimCron();
                //$prev_edi_cron = $model->getEdiCron();
                if (@$data['weightage']) {
                    if (@$data['id']) {
                        $is_exist = $model->getCollection()->addFieldToFilter('weightage', $data['weightage'])
                                        ->addFieldToFilter('id', array('neq' => $data['id']))
                                        ->addFieldToSelect('id')->getFirstItem();
                    } else {
                        $is_exist = $model->getCollection()->addFieldToFilter('weightage', $data['weightage'])
                                        ->addFieldToSelect('id')->getFirstItem();
                    }
                    if ($is_exist->getId()) {
                        $this->_getSession()->addError("An Incoming Channel with this wightage is exist");
                        echo Mage::app()->getResponse()->setRedirect($_SERVER['HTTP_REFERER']);
                        exit;
                    }
                }
//end
                unset($data['rule']);
                $data['status'] = $data['channel_status'];
                if (!empty($data['souq_condition'])) {
                    foreach ($data['souq_condition'] as $k => &$v) {
                        $v = array_filter($v, function($var) {
                            return ($var != '') ? $var : '';
                        });
                        if (empty($data['souq_condition'][$k]))
                            unset($data['souq_condition'][$k]);
                    }
                    if (!empty($data['souq_condition'])) {
                        if (!empty($data['souq_condition'][1]['sku_list'])) {
                            $souq_condition = $data['souq_condition'][1];
                            $sku_list = explode(',', $data['souq_condition'][1]['sku_list']);
                            $data['souq_condition'][1]['sku_list'] = implode(',', array_unique($sku_list));
                        }
                        $data['souq_condition'] = serialize($data['souq_condition']);
                    } else {
                        $data['souq_condition'] = '';
                    }
                }

                $data['authorized'] = 1;
                $model->setData($data);
                $model->save();
                //code for order slab changes on 30thApril 2016
                if ($this->getRequest()->getParam('id')) {
                    $resource = Mage::getSingleton('core/resource');
                    $writeConnection = $resource->getConnection("core_write");
                    //if slab or order is changed the update this in edi table if any inventory is exist for this channel

                    $bridge_edi_crontable = $resource->getTableName('bridge/incomingchannels_product');
                    //uncomment below line 
                    $writeConnection->raw_query("UPDATE $bridge_edi_crontable SET `slab`={$data['slab']},`order`={$data['channel_order']},channel_supplier={$data['channel_supplier']} WHERE channel_type='incoming' AND channel_id={$model->getId()};");
                }
                //End of code for order slab changes on 30thApril 2016
                /* saving sku to pre tempsouqdata table */

                $request_data = $this->getRequest()->getPost();
                $searchtype = $request_data['souq_condition'][1]['searchtype'];
                $channel_id = $request_data['id'];
                if ($searchtype == 'skulist') {
                    Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
                    $resource = Mage::getSingleton('core/resource');
                    $readConnection = $resource->getConnection('core_read');
                    $writeConnection = $resource->getConnection("core_write");
                    $bridge_souqpredata_table = $resource->getTableName('souq/incomingchannels_souqpredata');

                    $sql = "SELECT sku FROM " . $bridge_souqpredata_table . " WHERE channel_id = " . $request_data['id'];
                    $old_skulists = $readConnection->fetchAll($sql);
                    $old_skulist_arr = array();
                    if (count($old_skulists)) {
                        foreach ($old_skulists as $old_skulist) {
                            $old_skulist_arr[trim($old_skulist['sku'])] = trim($old_skulist['sku']);
                        }
                    }
                    $new_skulist_arr = array();
                    if (count($sku_list)) {
                        foreach ($sku_list as $sku) {
                            $new_skulist_arr[trim($sku)] = trim($sku);
                        }
                    }

                    $deleted_sku = array_diff($old_skulist_arr, $new_skulist_arr);
                    $deleted_sku_str = "'" . implode("','", $deleted_sku) . "'";
                    $added_sku = array_diff($new_skulist_arr, $old_skulist_arr);
                    //Delete sku which is not in new sku list

                    if (count($deleted_sku)) {
                        $where = "sku IN ($deleted_sku_str) AND channel_id =$channel_id ";
                        $writeConnection->delete($bridge_souqpredata_table, $where);
                    }
                    //added sku which new in the new sku list
                    foreach ($added_sku as $sku) {
                        $presouqskudata = array();
                        $presouqskudata['channel_id'] = $request_data['id'];
                        $presouqskudata['sku'] = $sku;
                        $model_presouqdata = Mage::getModel('souq/incomingchannels_souqpredata');
                        $model_presouqdata->setData($presouqskudata);
                        $model_presouqdata->save(); //commented for not saving skus to pretemp table
                    }
                } else {
                    if ($changed_condition && $attr_id_and_value_set) {
                        //Insert souqproductids to bridge_presouqdata table
                        //Mage::helper('souqcron')->insertSouqdataToPreTempTable($channel_id); //commented for not saving souqproducts to pretemp table
                    }
                }

                /* End of saving sku to pre tempsouqdata table */

                //Setup cron for adding active channel to bridge_souqactivechannel
                if ($channel_id) {
                    $crontiming = '*/2 * * * *';
                    //$cronurl = Mage::getBaseUrl() . "admin/souq/saveSouqActiveChannels";
                    $cronurl = Mage::helper("adminhtml")->getUrl("*/souq/saveSouqActiveChannels");
                    Mage::helper('souqcron')->setXmlCron($crontiming, $cronurl); //uncomment these line for create cron
                    //$this->saveSouqActiveChannels(); //comment this line when cron is created
                }
                //end

                try {

                    if ($model->getId() && $new_upload):
                        $csvmapping = Mage::getmodel('bridge/incomingchannels_mapping')->getCollection()
                                        ->addFieldToFilter('brand_id', $id)
                                        ->getFirstItem()->getData();
                        if ($csvmapping) {
                            $csvmapping = unserialize($csvmapping['data'])['import'];
                            if (!$this->autoProcesscsvAction($csvmapping, $new_upload_name)) {
                                $this->_getSession()->addError("Cannot upload the csv file,it does not match with the mapping headers");
                                echo Mage::app()->getResponse()->setRedirect($_SERVER['HTTP_REFERER']);
                                exit;
                            }
                        }
                    endif;
                    $model->save();
                } catch (Exception $e) {
                    echo "<pre>";
                    print_r($data);
                    print_r($e->getMessage());
                    exit;
                }


                //Here to store the mapping information
                $mappingdata = array();
                $mappingkeys = array('storeviewtype', 'storeviewmappingcolname', 'storeviewmappingcol', 'categorymaptype', 'csvcategorycolumnname', 'csvcategorycolumn', 'attributesettype', 'mapping', 'csvattrsetcol', 'allcategoryintomap', 'allstoreviewmap');
                foreach ($mappingkeys as $k => $v) {
                    if (isset($data[$v])) {
                        $mappingdata[$v] = $data[$v];
                        unset($data[$v]);
                    }
                }

                if ($mappingdata) {
                    $mapping['brand_id'] = $model->getId();
                    //make the data format in the way will efficient for at time of product mapping
                    $import_mapping = $mappingdata;
                    $only_mapping = $import_mapping['mapping'];
                    if ($mappingdata['csvcategorycolumn']) {
                        unset($import_mapping['csvcategorycolumn']);
                        foreach ($mappingdata['csvcategorycolumn']['csvcategory'] as $csv_map_key => $csv_map_value) {
                            $import_mapping['category_mapping'][$csv_map_value[0]] = is_array($mappingdata['csvcategorycolumn']['storecategory'][$csv_map_key]) ? $mappingdata['csvcategorycolumn']['storecategory'][$csv_map_key] : $mappingdata['csvcategorycolumn']['storecategory'][$csv_map_key][0];
                        }
                    }
                    if ($mappingdata['storeviewmappingcol']) {
                        unset($import_mapping['storeviewmappingcol']);
                        foreach ($mappingdata['storeviewmappingcol'] as $storeview_map_value) {
                            $import_mapping['storeview_mapping'][$storeview_map_value['brand']] = $storeview_map_value['store'];
                        }
                    }
                    if ($mappingdata['attributesettype'] == 1) {
                        $import_mapping['default_attribute_set'] = $mappingdata['mapping']['storefieldset'][0];
                    }


                    $allowed_field_mappings = array('storefieldset', 'attributes', 'custommappingattr', 'brandfieldset', 'attributeoptions');
                    unset($import_mapping['mapping']);
                    foreach ($only_mapping as $mapping_name => $mapping_value) {
                        foreach ($mapping_value as $mapping_name_key => $mapping_name_value) {
                            if ($mapping_name == 'brandfieldset') {
                                $import_mapping['attributeset'][$mapping_name_value] = $only_mapping['storefieldset'][$mapping_name_key];
                            }
                            foreach ($mapping_name_value as $map_key => $map_value) {
                                if ($mapping_name == 'attributes') {
                                    $attribute_id = $mapping_name_key == 'all' ? $only_mapping['storefieldset'][0] : $mapping_name_key;
                                    $import_mapping['attributes'][$attribute_id][$map_value['brand']] = $map_value['store'];
                                } else if ($mapping_name == 'custommappingattr') {
                                    if ($only_mapping['custommappingvalue'][$mapping_name_key][$map_key] != '')
                                        $import_mapping['custom_maping'][$mapping_name_key][$map_value] = $only_mapping['custommappingvalue'][$mapping_name_key][$map_key];
                                } else if ($mapping_name == 'attributeoptions') {
                                    foreach ($map_value as $opt_key => $opt_val)
                                        if ($opt_val != '')
                                            $import_mapping['option_mapping'][$mapping_name_key][$map_key][$opt_val['brand']] = $opt_val['store'];
                                }
                            }
                        }
                    }


                    $mapping['data'] = serialize(array('interface' => $mappingdata, 'import' => $import_mapping));
                }

                $mapping_model = Mage::getmodel('bridge/incomingchannels_mapping');
                $mapping['id'] = $mapping_model->getCollection()->addFieldToFilter('brand_id', $model->getId())->addFieldToSelect('id')->getFirstItem()->getId();
                if ($mapping['id'] && !$mapping['data']) {
                    $mapping_model->load($mapping['id']);
                    $mapping_model->delete();
                } else if ($mapping['data']) {
                    $mapping_model->setData($mapping);
                    $mapping_model->save();
                }
                //END of maping save
                //save meta title and description rule to corresponding stroe             
                $store_id = $this->getCurrentStore();
                $store_view = Mage::getmodel('bridge/storeview_label');
//stor meta description
                $meta_des_store_id = Mage::getmodel('bridge/storeview_label')->getCollection()
                        ->addFieldToFilter('brand_id', $model->getId())
                        ->addFieldToFilter('type', 'meta_description')
                        ->addFieldToFilter('store_id', $store_id)
                        ->getFirstItem()
                        ->getId();
                $meta_description = array('id' => @$meta_des_store_id, 'brand_id' => $model->getId(), 'type' => 'meta_description', 'value' => $data['meta_description'], 'store_id' => $store_id);
                $store_view->setData($meta_description);
                $store_view->save();
//store meta title
                $meta_des_store_id = Mage::getmodel('bridge/storeview_label')->getCollection()
                        ->addFieldToFilter('brand_id', $model->getId())
                        ->addFieldToFilter('type', 'meta_title')
                        ->addFieldToFilter('store_id', $store_id)
                        ->getFirstItem()
                        ->getId();
                $meta_title = array('id' => @$meta_des_store_id, 'brand_id' => $model->getId(), 'type' => 'meta_title', 'value' => $data['meta_title'], 'store_id' => $store_id);
                $store_view->setData($meta_title);
                $store_view->save();
//add the incoming channel to attribute channel priority option
                if (!$this->getRequest()->getParam('id')) {
                    $option_id = Mage::getModel('bridge/incomingchannels')->addChannelOption($model->getBrandName());
                    $inc_model = Mage::getmodel('bridge/incomingchannels');
                    $inc_model->setData(array('id' => $model->getId(), 'option_id' => $option_id));
                    $inc_model->save();
                }
//end           

                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('bridge')->__('The Channel has been saved.')
                );
                Mage::getSingleton('adminhtml/session')->setPageData(false);
                $tab_name = ($data['tab_name']) ? $data['tab_name'] : 'main_section';
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
// The following line decides if it is a "save" or "save and continue"
        if ($this->getRequest()->getParam('back')) {
            $this->_redirect('*/souq/edit', array('activeTab' => $tab_name, 'id' => $model->getId(), 'store' => @$this->getRequest()->getParam('store')));
        } else {
            $this->_redirect('*/incomingchannels');
        }
    }

    public function getCurrentStore() {
        $defaultstoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
        $store_id = Mage::app()->getRequest()->getParam('store') ? Mage::app()->getRequest()->getParam('store') : $defaultstoreId;
        return $store_id;
    }

    public function gridAction() {
        $this->loadLayout();
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('bridge/adminhtml_incomingchannels_edit_tab_listing_grid')->toHtml()
        );
    }

    public function fetchsouqproductsAction() {
        try {
            $params = $this->getRequest()->getParams();
            echo "<div id='souq-products-list'>";

            //Create Dynamic request url for souq incoming
            if ($params['key'] && $params['secret']) {
                if (trim($params['souq_condition'][1]['q']) || trim($params['souq_condition'][1]['seller']) || trim($params['souq_condition'][1]['product_types'])) {
                    $souqparams = 1;
                }
                $url = "https://api.souq.com/v1/products";
                if ($params['souq_condition'][1]['searchtype'] == "searchquery") {
                    if (trim($params['souq_condition'][1]['q']))
                        $url .= "?q=" . trim($params['souq_condition'][1]['q']);
                }
                if ($params['souq_condition'][1]['searchtype'] == "skulist") {
                    if (trim($params['souq_condition'][1]['sku_list'])) {
                        $sku_list = explode(',', $params['souq_condition'][1]['sku_list']);
                        $url .= "?q=" . trim($sku_list[0]);
                    }
                }
                if (trim($params['souq_condition'][1]['seller']))
                    $url .= "&seller=" . trim($params['souq_condition'][1]['seller']);
                if (trim($params['souq_condition'][1]['product_types']))
                    $url .= "&product_types=" . str_replace(',', '%2C', trim($params['souq_condition'][1]['product_types']));
                if (trim($params['souq_condition'][1]['deals_tags']))
                    $url .= "&deals_tags=" . str_replace(',', '%2C', trim($params['souq_condition'][1]['deals_tags']));
                if (trim($params['souq_condition'][1]['price_from']))
                    $url .= "&price_from=" . trim($params['souq_condition'][1]['price_from']);
                if (trim($params['souq_condition'][1]['price_to']))
                    $url .= "&price_to=" . trim($params['souq_condition'][1]['price_to']);
                if (trim($params['souq_condition'][1]['condition']))
                    $url .= "&condition=" . trim($params['souq_condition'][1]['condition']);
                if (trim($params['souq_condition_attrfilter'][0]['attr_id']) && trim($params['souq_condition_attrfilter'][0]['attr_value'])) {
                    foreach ($params['souq_condition_attrfilter'] as $val) {
                        $url .="&attribute_filter_" . $val['attr_id'] . "=" . urlencode($val['attr_value']);
                    }
                }
                if (trim($params['souq_condition'][1]['affiliate_id']))
                    $url .= "&affiliate_id=" . trim($params['souq_condition'][1]['affiliate_id']);
                if (trim($params['souq_condition'][1]['page']))
                    $url .= "&page=" . trim($params['souq_condition'][1]['page']);
                if (trim($params['souq_condition'][1]['show']))
                    $url .= "&show=" . trim($params['souq_condition'][1]['show']);
                if ($params['souq_condition'][1]['sort_by'])
                    $url .= "&sort_by=" . $params['souq_condition'][1]['sort_by'];
                if (isset($params['souq_condition'][1]['show_attributes'])) {
                    if ($params['souq_condition'][1]['searchtype'] == "skulist") {
                        $url .= "&show_attributes=1";
                    } else {
                        $url .= "&show_attributes=" . $params['souq_condition'][1]['show_attributes'];
                    }
                }
                if ($params['souq_condition'][1]['country'])
                    $url .= "&country=" . $params['souq_condition'][1]['country'];
                if ($params['souq_condition'][1]['ship_to'])
                    $url .= "&ship_to=" . $params['souq_condition'][1]['ship_to'];
                if ($params['souq_condition'][1]['language'])
                    $url .= "&language=" . $params['souq_condition'][1]['language'];

                $url .= "&format=json"; //alway send data in json format
                $url .= "&app_id=" . $params['key'];
                $url .= "&app_secret=" . $params['secret'];
                //end or Dynamic url for souq incoming
                //Get Json formated data from the souq api
                if ($souqparams) {
                    $jsonData = file_get_contents($url);
                    $total_product_info = json_decode($jsonData)->meta;
                    $showing = $total_product_info->showing ? $total_product_info->showing : 0;
                    $total = $total_product_info->total ? $total_product_info->total : 0;
                    $products = array();
                    $products = (array) json_decode($jsonData)->data->products;
                    //Get exact product according to sku list if search type is sku_list
                    if ($params['souq_condition'][1]['searchtype'] == "skulist") {
                        $product = $this->getSouqProductSku($products, $sku_list[0]);
                        $imgcontent = $this->formatSouqSkuDataForDisplay($product);
                        echo "<div>Currently showing 1 of Total 1 Records found</div><div>" . $imgcontent . "</div>";
                    } else {
                        $imgcontent = $this->formatSouqDatatForDispaly($products);
                        echo "<div>Currently showing " . $showing . " of Total " . $total . " Records found</div><div>" . $imgcontent . "</div>";
                    }
                    //echo "<div>Currently showing " . $showing . " of Total " . $total . " Records found</div><div>" . $imgcontent . "</div>";
                } else {
                    echo $this->__("One of the following params is mandatory (Product Types,Search query,Seller)!");
                }
            } else {
                echo $this->__("Colud not connect to Souq, Credentials used(Key,SecretTag Key) needed.");
            }
            echo "</div>";
        } catch (Exception $e) {
            echo $this->__("<div id='souq-products-list'>Colud not connect to Souq, Credentials used(Key,SecretTag Key) are not correct.</div>");
        }
    }

    private function formatSouqDatatForDispaly($products) {
        $content = "<div>";
        if (count($products) > 0) {
            foreach ($products as $product) {
                $content .="<div style='float:left;'>"
                        . "<div><img src='" . $product->images->S[0] . "' alt='" . $product->label . "' /></div>"
                        . "<div style='width:100px;'><a target='_blank' href='" . $product->link . "'>" . $product->label . "</a></div>"
                        . "</div>";
            }
        }
        $content .= "<div style='clear:both;'></div></div>";
        return $content;
    }

    //For showing product fetched by sku selection
    private function formatSouqSkuDataForDisplay($product) {
        $content = "<div>";
        if (count($product) > 0) {
            $content .="<div style='float:left;'>"
                    . "<div><img src='" . $product->images->S[0] . "' alt='" . $product->label . "' /></div>"
                    . "<div style='width:100px;'><a target='_blank' href='" . $product->link . "'>" . $product->label . "</a></div>"
                    . "</div>";
        }
        $content .= "<div style='clear:both;'></div></div>";
        return $content;
    }

    private function getSouqProductSku($products, $sku) {
        if (count($products) > 0) {
            $find = 0;
            foreach ($products as $product) {
                $attributes = $product->attributes;
                foreach ($attributes as $attribute) {
                    if (($attribute->value == $sku) && ($attribute->label == "Model Number")) {
                        $find = 1;
                        break;
                    }
                }
                if ($product->label && !$find) {
                    if (preg_match('/' . $sku . '/', $product->label)) {
                        $find = 1;
                    }
                }
                if ($find) {
                    return $product;
                    break;
                }
            }
        }
    }

    //Get product_id from the sku which is in the bridge_presouqdata table
    public function getSouqProductidFromSkuAction() {
        $channel_id = $this->getRequest()->getParam('channel_id');
        //get the sku from the table bridge_souqpredata which is not having souq_product_id
        Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $bridge_souqpredata_table = $resource->getTableName('souq/incomingchannels_souqpredata');
        //As There is a limitiation of making API request to souq per minute
        //Here we are taking care of that
        //Here let's use 50 as limit for
        $limit = 50;

        $sql = "SELECT sku FROM " . $bridge_souqpredata_table . " WHERE id_product_souq IS NULL AND is_processed = 0  AND channel_id = " . $channel_id . " LIMIT $limit";
        $sqlattrQuery = $readConnection->query($sql);
        $souq_sku_arr = array();
        while ($row = $sqlattrQuery->fetch()) {
            $souq_sku_arr[$row['sku']] = $row['sku'];
        }
        //get the souq_productid of the indivisual sku
        if (count($souq_sku_arr)) {
            $count = 0;
            foreach ($souq_sku_arr as $sku) {
                $url = "https://api.souq.com/v1/products?q=$sku&show_attributes=1&country=ae&language=en&format=json&app_id=6042584&app_secret=co6M6NR0yESzNfB0qKDu";
                $jsonData = file_get_contents($url);
                $products = (array) json_decode($jsonData)->data->products;
                //Get the exact souq product having exact sku = $sku
                $product = $this->getSouqProductSku($products, $sku);
                //Get the exact souq_product_id of the $sku
                $souq_product_id = $product->id;
                //Update the souq_product_id field of $sku in the table bridge_presouqdata
                $pre_souq_productdata = array("id_product_souq" => $souq_product_id);
                $where = "channel_id = {$channel_id} AND sku ='{$sku}'";
                $writeConnection->update($bridge_souqpredata_table, $pre_souq_productdata, $where);
                if ($souq_product_id)
                    $count = $count + 1;
            }
            echo "Souq_product_id of total $count skus in the SkuList has Updated";
        }else {
            echo "There no skus left for updating their product_id";
        }
    }

//save souq product details to temporary table(bridge_souqdata) by getting souq_product_id from the bridge_presouqdata table
    public function insertSouqProductdetailsToTempTableAction() {
        $channel_id = $this->getRequest()->getParam('channel_id');
        //get the souq_product_id from the table bridge_souqpredata which is not processed
        Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $bridge_souqpredata_table = $resource->getTableName('souq/incomingchannels_souqpredata');

        //As There is a limitiation of making API request to souq per minute
        //Here we are taking care of that
        //Here let's use 50 as limit for
        $limit = 50;
        $sql = "SELECT id_product_souq FROM " . $bridge_souqpredata_table . " WHERE is_processed = 0  AND channel_id = " . $channel_id . " LIMIT $limit";
        $sqlattrQuery = $readConnection->query($sql);
        $souq_productids_arr = array();
        while ($row = $sqlattrQuery->fetch()) {
            $souq_productids_arr[$row['id_product_souq']] = $row['id_product_souq'];
        }

        //For deleting record if current fetched record in array $souq_productids_arr already exist in the bridge_souqdata for bridge_channel_id = $channel_id
        if (count($souq_productids_arr)) {
            $bridge_souqTempdata_table = $resource->getTableName('souq/incomingchannels_souq');
            $souq_productids_str = implode(",", $souq_productids_arr);
            $where = "bridge_channel_id = $channel_id AND FIND_IN_SET(id_product_souq,'$souq_productids_str')";
            $writeConnection->delete($bridge_souqTempdata_table, $where);
        }
        //End of delete
        if (count($souq_productids_arr)) {
            foreach ($souq_productids_arr as $souq_productid) {
                $url = "https://api.souq.com/v1/products/$souq_productid?country=ae&language=en&show_offers=1&show_attributes=1&show_variations=1&format=json&app_id=6042584&app_secret=co6M6NR0yESzNfB0qKDu";
                $jsonData = file_get_contents($url);
                $souq_product = (array) json_decode($jsonData)->data;

                //set souqproductdata in array to insert to bridge_souqdata
                $souqproductdata = array();
                $souqproductdata['id_product_souq'] = $souq_product['id'];
                $souqproductdata['souq_price'] = $souq_product['offer_price'];
                $souqproductdata['msrp'] = $souq_product['msrp'];
                $souqproductdata['currency'] = $souq_product['currency'];
                $souqproductdata['name'] = addslashes($souq_product['label']);
                $souqproductdata['detail_url'] = $souq_product['link'];
                $souqproductdata['bridge_channel_id'] = $channel_id;
                $souqproductdata['images'] = addslashes(serialize((array) $souq_product['images']));
                $souqproductdata['ean'] = $souq_product['ean'][0];
                $souqproductdata['product_type_id'] = $souq_product['product_type_id'];
                $souqproductdata['product_type_label_plural'] = $souq_product['product_type_label_plural'];
                $souqproductdata['short_description'] = $souq_product['snippet_of_description'];
                //get Attributes related to the products from attribute
                $k = 0;
//                foreach ($souq_product['attributes'] as $val) {
//                    $souqproductdata['attributes'][$k] = (array) $val;
//                    $k++;
//                }
                //get Attributes related to the products from attribute group
                foreach ($souq_product['attributes_groups'][0]->attributes as $attribute) {
                    if ($attribute->label != "Description") {
                        $souqproductdata['attributes'][$k] = (array) $attribute;
                        $k++;
                    }
                }
                $souqproductdata['attributes'] = addslashes(serialize($souqproductdata['attributes']));
                //get dsescription related to the products
                foreach ($souq_product['attributes_groups'][0]->attributes as $attribute) {
                    if ($attribute->label == "Description") {
                        $souqproductdata['description'] = $attribute->value;
                    }
                }
                //Insert product to the temporary table bridge_souqdata 
                $model_souqdata = Mage::getModel('souq/incomingchannels_souq');
                $model_souqdata->setData($souqproductdata);
                $model_souqdata->save();
            }
            //Here let's update the flag in pre souq table
            $updsql = "UPDATE $bridge_souqpredata_table SET is_processed = 1 WHERE channel_id = $channel_id AND FIND_IN_SET(id_product_souq,'$souq_productids_str')";
            $writeConnection->query($updsql);
            echo "Successfully data inserted to souqproduct temporary table";
        } else {
            echo "There is no product_id for insert in to the bridge_souqdata table";
        }
    }

    //pick one one produtId from the presouqdata table which has is_processed=0 irrespective of the channel_id
    public function insertSouqProductdetailsToSouqdataTableAction() {//Uncomment this line for cron
        //public function insertSouqProductdetailsToSouqdataTable() {//comment this line for cron setup
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $bridge_souqpredata_table = $resource->getTableName('souq/incomingchannels_souqpredata');
        $sql = "SELECT * FROM " . $bridge_souqpredata_table . " WHERE is_processed = 0 ORDER BY id asc  LIMIT 1";
        $added_product = $readConnection->fetchAll($sql);

        //if product present in the table bridge_presouqdata which has is_processed = 0
        if ($added_product[0]['id']) {
            $bridge_souqdata_table = $resource->getTableName('souq/incomingchannels_souq');
            $sql = "SELECT * FROM " . $bridge_souqdata_table . " WHERE id_product_souq = " . $added_product[0]['id_product_souq'] . " AND bridge_channel_id=" . $added_product[0]['channel_id'];
            $productdetail_souqdata = $readConnection->fetchAll($sql);
            $product_id_souqdata = ($productdetail_souqdata[0]['id']) ? $productdetail_souqdata[0]['id'] : "";

            if (count($productdetail_souqdata)) {
                $product_exist_in_souqdata_table = true;
            } else {
                $product_exist_in_souqdata_table = false;
            }
            //for outofstock products
            if ($added_product[0]['flag'] == 2 && $product_exist_in_souqdata_table) {
//                $where = "bridge_channel_id = {$added_product[0]['channel_id']} AND id_product_souq ={$added_product[0]['id_product_souq']}";
//                $writeConnection->delete($bridge_souqdata_table, $where);
                $updsql_outofstock = "UPDATE {$bridge_souqdata_table} SET skipped=0,is_processed = 0,flag=2,updated_at=NOW() WHERE id = {$product_id_souqdata} ";
                $writeConnection->query($updsql_outofstock);
            }
            //For updating the souq product in the bridge_souqdata table if flag =3(update) and product is in souqdata
            if ($added_product[0]['flag'] == 3 || $added_product[0]['flag'] == 1) {
                $souq_productid = $added_product[0]['id_product_souq'];
                $url = "https://api.souq.com/v1/products/$souq_productid?country=ae&language=en&show_offers=1&show_attributes=1&show_variations=1&format=json&app_id=6042584&app_secret=co6M6NR0yESzNfB0qKDu";
                $jsonData = file_get_contents($url);
                $souq_product = (array) json_decode($jsonData)->data;

                //set souqproductdata in array to insert to bridge_souqdata
                $souqproductdata = array();
                $souqproductdata['id_product_souq'] = $souq_product['id'];
                $souqproductdata['souq_price'] = $souq_product['offer_price'];
                $souqproductdata['msrp'] = $souq_product['msrp'];
                $souqproductdata['currency'] = $souq_product['currency'];
                $souqproductdata['name'] = addslashes($souq_product['label']);
                $souqproductdata['detail_url'] = $souq_product['link'];
                $souqproductdata['bridge_channel_id'] = $channel_id;
                $souqproductdata['images'] = addslashes(serialize((array) $souq_product['images']));
                $souqproductdata['ean'] = $souq_product['ean'][0];
                $souqproductdata['product_type_id'] = $souq_product['product_type_id'];
                $souqproductdata['product_type_label_plural'] = $souq_product['product_type_label_plural'];
                $souqproductdata['short_description'] = addslashes($souq_product['snippet_of_description']);
                //get Attributes related to the products from attribute
                $k = 0;
                //get Attributes related to the products from attribute group
                foreach ($souq_product['attributes_groups'][0]->attributes as $attribute) {
                    if ($attribute->label != "Description") {
                        $souqproductdata['attributes'][$k] = (array) $attribute;
                        $k++;
                    }
                }
                $souqproductdata['attributes'] = addslashes(serialize($souqproductdata['attributes']));
                //get dsescription related to the products
                foreach ($souq_product['attributes_groups'][0]->attributes as $attribute) {
                    if ($attribute->label == "Description") {
                        $souqproductdata['description'] = addslashes($attribute->value);
                    }
                }

                //for product not present in bridge_souqdata and flag in presouqdata is 3
                if ((($added_product[0]['flag'] == 1) || ($added_product[0]['flag'] == 3)) && $product_exist_in_souqdata_table) {
                    $updsql = "UPDATE {$bridge_souqdata_table} SET skipped=0,is_processed = 0,flag=3,product_type_id={$souqproductdata['product_type_id']},"
                            . "product_type_label_plural='{$souqproductdata['product_type_label_plural']}',ean='{$souqproductdata['ean']}',"
                            . "name='{$souqproductdata['name']}',short_description='{$souqproductdata['short_description']}',description='{$souqproductdata['description']}',"
                            . "currency='{$souqproductdata['currency']}',souq_price='{$souqproductdata['souq_price']}',msrp='{$souqproductdata['msrp']}',"
                            . "attributes='{$souqproductdata['attributes']}',images='{$souqproductdata['images']}',detail_url='{$souqproductdata['detail_url']}',"
                            . "updated_at=NOW()"
                            . " WHERE id = {$product_id_souqdata} ";
                    $writeConnection->query($updsql);
                }
                //For product not present in bridge_souqdata and flag in presouqdata is 3(update) or 1(new)
                if ((($added_product[0]['flag'] == 1) || ($added_product[0]['flag'] == 3)) && !$product_exist_in_souqdata_table) {
                    //Insert product to the temporary table bridge_souqdata 
                    $souqproductdata['flag'] = 1;
                    $souqproductdata['bridge_channel_id'] = $added_product[0]['channel_id'];
                    $model_souqdata = Mage::getModel('souq/incomingchannels_souq');
                    $model_souqdata->setData($souqproductdata);
                    $model_souqdata->save();
                }
            }
            //Mark is_process=1 product is inserted to bridge_souqdata from bridge_presouqdata table
            $updsql = "UPDATE {$bridge_souqpredata_table} SET is_processed = 1 WHERE id = {$added_product[0]['id']} ";
            $writeConnection->query($updsql);
        } else {
            //Remove cron for adding active channel to bridge_souqactivechannel
            $crontiming = '*/1 * * * *';
            //$cronurl = Mage::getBaseUrl() . "admin/souq/insertSouqProductdetailsToSouqdataTable";
            $cronurl = Mage::helper("adminhtml")->getUrl("*/souq/insertSouqProductdetailsToSouqdataTable");
            Mage::helper('souqcron')->removeXmlCron($crontiming, $cronurl);
            //end 
        }
        //end
        //Setup cron for pickup one record(product) from bridge_souqdata insert in to magento store
        $crontiming = '*/3 * * * *';
        //$cronurl = Mage::getBaseUrl() . "admin/souq/product_create";
        $cronurl = Mage::helper("adminhtml")->getUrl("*/souq/product_create");
        Mage::helper('souqcron')->setXmlCron($crontiming, $cronurl); //uncomment this line
        //end
    }

    //Add active channels to bridge_souqactivechannels table
    public function saveSouqActiveChannelsAction() {//uncomment this line for cron call
        //public function saveSouqActiveChannels() {//comment this line for cron call
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");

        //get the active channels from bridge_incomingschannel
        $table = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels');
        $sql = "SELECT * FROM " . $table . " WHERE status=1 AND channel_type='souq' AND ((product_info =1 AND (create_new=1 OR update_exist =1)) OR (product_pricenquantity =1))";
        $active_channels = $readConnection->fetchAll($sql);

        //get new active channel_id from the bridge_incomingchannels table according to above active condtion
        $souq_activechannel_new_arr = array();
        foreach ($active_channels as $active_channel) {
            $souq_activechannel_new_arr[$active_channel['id']] = $active_channel['id'];
        }

        //Get all old active channel from the bridge_souqactivechannels table
        $souqactivechanneltable = Mage::getSingleton('core/resource')->getTableName('souq/incomingchannels_souqactivechannel');
        $sql = "SELECT * FROM " . $souqactivechanneltable . " WHERE 1";
        $old_active_channels = $readConnection->fetchAll($sql);
        $souq_activechannel_old_arr = array();
        foreach ($old_active_channels as $old_active_channel) {
            $souq_activechannel_old_arr[$old_active_channel['channel_id']] = $old_active_channel['channel_id'];
        }

        $deleted_channelid_arr = array_diff($souq_activechannel_old_arr, $souq_activechannel_new_arr); //flag=removed
        $added_channelid_arr = array_diff($souq_activechannel_new_arr, $souq_activechannel_old_arr); //flag=new
        $updated_channelid_arr = array_intersect($souq_activechannel_old_arr, $souq_activechannel_new_arr); //flag=updated
        //Mark Remove channelid which is not in new active channelid list
        if (count($deleted_channelid_arr)) {
            $deleted_channelid_str = implode(",", $deleted_channelid_arr);
            $where = " channel_id IN ({$deleted_channelid_str}) ";
            $writeConnection->delete($souqactivechanneltable, $where);
        }
        //Mark update channelid which is already in the active channelid list
        $cronvalue = array('0 * * * *' => 1, '0 0 * * *' => 2, '0 0 * * 0' => 3, '0 0 1 * *' => 4);
        $cronvalue_time = array(0 => 'Never', 1 => '1 Hour', 2 => '1 Day', 3 => '1 Week', 4 => '1 Month');
        if (count($updated_channelid_arr)) {
            foreach ($active_channels as $active_channel) {
                if (in_array($active_channel['id'], $updated_channelid_arr)) {
                    $pimcron = ($cronvalue[$active_channel['pim_cron']]) ? $cronvalue[$active_channel['pim_cron']] : 0;
                    $edicron = ($cronvalue[$active_channel['edi_cron']]) ? $cronvalue[$active_channel['edi_cron']] : 0;
                    $mincron_value = ($pimcron && $edicron) ? min($pimcron, $edicron) : max($pimcron, $edicron);
                    $where = " WHERE channel_id={$active_channel['id']} ";
                    $updata_query = "UPDATE {$souqactivechanneltable} SET update_frequency='{$cronvalue_time[$mincron_value]}',pim={$active_channel['product_info']},edi={$active_channel['product_pricenquantity']} {$where} ";
                    $writeConnection->raw_query($updata_query);
                }
            }
        }
        //Add the new active channel which is not in old active channelid list
        if (count($added_channelid_arr)) {
            $active_channel_details = array();
            $i = 0;
            foreach ($active_channels as $active_channel) {
                if (in_array($active_channel['id'], $added_channelid_arr)) {
                    $pimcron = ($cronvalue[$active_channel['pim_cron']]) ? $cronvalue[$active_channel['pim_cron']] : 0;
                    $edicron = ($cronvalue[$active_channel['edi_cron']]) ? $cronvalue[$active_channel['edi_cron']] : 0;
                    $mincron_value = ($pimcron && $edicron) ? min($pimcron, $edicron) : max($pimcron, $edicron);
                    $active_channel_details[$i]['channel_id'] = $active_channel['id'];
                    $active_channel_details[$i]['update_frequency'] = $cronvalue_time[$mincron_value];
                    $active_channel_details[$i]['pim'] = $active_channel['product_info'];
                    $active_channel_details[$i]['edi'] = $active_channel['product_pricenquantity'];
                    $active_channel_details[$i]['is_processed'] = 0;
                    $i = $i + 1;
                }
            }
            $writeConnection->insertMultiple($souqactivechanneltable, $active_channel_details);
        }

        //Remove cron for adding active channel to bridge_souqactivechannel
        $crontiming = '*/2 * * * *';
        //$cronurl = Mage::getBaseUrl() . "admin/souq/saveSouqActiveChannels";
        $cronurl = Mage::helper("adminhtml")->getUrl("*/souq/saveSouqActiveChannels");
        Mage::helper('souqcron')->removeXmlCron($crontiming, $cronurl);
        //end
        //Setup cron for pickup active channel from bridge_souqactivechannel and get souqproducts related to the channel_condition
        $crontiming = '*/10 * * * *';
        //$cronurl = Mage::getBaseUrl() . "admin/souq/getSouqActiveChannels";
        $cronurl = Mage::helper("adminhtml")->getUrl("*/souq/getSouqActiveChannels");
        Mage::helper('souqcron')->setXmlCron($crontiming, $cronurl); //uncomment this line
        //$this->getSouqActiveChannels();//comment this line
        //end
    }

    //Get active channels from table bridge_souqactivechannel
    public function getSouqActiveChannelsAction() {//uncomment this line for cron setup
        //public function getSouqActiveChannels() {//Comment this line for cron setup
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        //get the active channels from bridge_souqactivechannel table where is_processed =1 and current_time > updatetime+update frequency
        $table = Mage::getSingleton('core/resource')->getTableName('souq/incomingchannels_souqactivechannel');
        $sql = "SELECT * FROM " . $table . " WHERE is_processed=1 ";
        $processed_channels = $readConnection->fetchAll($sql);

        if (count($processed_channels)) {
            foreach ($processed_channels as $processed_channel) {
                $last_process_time = $processed_channel['processed_at'];
                $update_frequency = $processed_channel['update_frequency'];
                if ($update_frequency != 'Never') {
                    //date_default_timezone_set('Asia/Kolkata'); //comment this line for cron setup
                    $update_timestamp_str = $last_process_time . " + " . $update_frequency;
                    $update_timestamp = strtotime($update_timestamp_str);
                    $now_timestamp = time();
                    if ($now_timestamp >= $update_timestamp) {
                        //Change is_processed=0 when current time >= updatetimestap 
                        $updata_query = "UPDATE bridge_souqactivechannels SET is_processed=0 WHERE channel_id = " . $processed_channel['channel_id'];
                        $writeConnection->raw_query($updata_query);
                        //end
                    }
                }
            }
        }
        //For edicron start set up when is_processed=0 and processed_at is not Null(means processed before but now to edi update) in table bridge_souqactivechannels
        $sql_active_processed = "SELECT * FROM " . $table . " WHERE is_processed=0 AND edi=1 AND processed_at IS NOT NULL ORDER BY id asc limit 1"; //desc
        $active_channels_processed = $readConnection->fetchAll($sql_active_processed);
        
        if (count($active_channels_processed)) {
            $crontiming = '0 * * * *';
            $active_channel_id_processed = $active_channels_processed[0]['channel_id'];
            //$cronurl = Mage::getBaseUrl() . "admin/souq/souqEdiupdateCron?channel_id=" . $active_channels_processed[0]['channel_id'];
            $cronurl = Mage::helper("adminhtml")->getUrl("*/souq/souqEdiupdateCron?channel_id=".$active_channel_id_processed);
            Mage::helper('souqcron')->setXmlCron($crontiming, $cronurl);
        }
        //end
        //get the active channels from bridge_souqactivechannel table which have is_processed=0
        $sql = "SELECT * FROM " . $table . " WHERE is_processed=0 ORDER BY id asc limit 1"; //desc
        $active_channels = $readConnection->fetchAll($sql);

        if (count($active_channels)) {

            $current_active_channel_id = $active_channels[0]['channel_id'];

            //Insert souqproductids to bridge_presouqdata table
            $return_status = Mage::helper('souqcron')->insertSouqdataToPreTempTable($current_active_channel_id);

            if ($return_status == 1) {
                //Setup cron for pickup one record from bridge_presouqdata insert in to the bridge_souqdata table
                $crontiming = '*/1 * * * *';
                //$cronurl = Mage::getBaseUrl() . "admin/souq/insertSouqProductdetailsToSouqdataTable";
                $cronurl = Mage::helper("adminhtml")->getUrl("*/souq/insertSouqProductdetailsToSouqdataTable");
                Mage::helper('souqcron')->setXmlCron($crontiming, $cronurl); //uncomment this line
                //end
            }
        }
        
    }

    //code for SouqEdiupdateCron funtion
    public function souqEdiupdateCronAction() {
        if (isset($_SERVER['argv']) && !empty($_SERVER['argv'])) {
            parse_str($_SERVER['argv'][0], $arr);
            $debug = var_export($arr, true);
            $channel_id = $arr['channel_id'];
        } else {
            $channel_id = $this->getRequest()->getParam('channel_id');
        }
        Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        //get channel_info of channel_id
        $channel_infos = Mage::getModel('bridge/incomingchannels');
        $channel_infos->load($channel_id);
        $channel_data = $channel_infos->getData();
        $souq_channel_condtion_serialize = $channel_data['souq_condition'];
        $souq_channel_condtion = unserialize($souq_channel_condtion_serialize);
        $seller = ($souq_channel_condtion[1]['seller']) ? $souq_channel_condtion[1]['seller'] : '';
        
        //end
        $table = 'bridge_souqdata';

        $channel_ids = array($channel_id);
        
        //$channel_ids = $channel_id;
        $channel_ids_str = implode(',', $channel_ids);
        $sql = "SELECT * FROM `bridge_incomingchannel_product_info` WHERE FIND_IN_SET(channel_id,'$channel_ids_str')";
        $sqlattrQuery = $readConnection->query($sql);
        $product_ids = $prodcutindo = array();
        while ($row = $sqlattrQuery->fetch()) {
            $product_ids[$row['product_entity_id']] = $row['product_entity_id'];
            $prodcutindo[$row['product_entity_id']] = array(
                'channel_id' => $row['channel_id'],
                'sku' => $row['sku'],
                'slab' => $row['slab'],
                'order' => $row['order'],
                'price' => $row['price'],
            );
        }
        
        $allids = implode(',', $product_ids);
        $finalsql = "SELECT `id_product_souq`, `id_product_magento` FROM `bridge_souqdata` WHERE FIND_IN_SET(`id_product_magento`,'$allids')";
        $sqlattrQuery = $readConnection->query($finalsql);
        $finalproductids = array();
        while ($row = $sqlattrQuery->fetch()) {
            if (!isset($finalproductids[$row['id_product_magento']])) {
                $finalproductids[$row['id_product_magento']] = $row['id_product_souq'];
            }
        }

        foreach ($finalproductids as $key => $value) {
            $apiurl = "https://api.souq.com/v1/products/$value?country=ae&language=en&show_offers=1&show_attributes=1&show_variations=1&format=json&app_id=6042584&app_secret=co6M6NR0yESzNfB0qKDu";
            
            //$data = get_contents($apiurl);
            //$jsondata = (array) json_decode($data['response']);
            $responseData = file_get_contents($apiurl);
            $jsondata = (array) json_decode($responseData)->data;
            $offers = $jsondata['offers'];
            
            $channel_id = $prodcutindo[$key]['channel_id'];
            $sku = $prodcutindo[$key]['sku'];
            $slab = $prodcutindo[$key]['slab'];
            $order = $prodcutindo[$key]['order'];
            $price = $prodcutindo[$key]['price'];
            $mapped_product = "AED";
            if (!empty($offers)) {
                foreach ($offers as $k => $v) {
                    if ($v->seller->name == $seller) {
                        $price = $v->price;
                        $quantity = ($v->in_stock >= 1) ? 10 : 0;
                    }
                }
            } else {
                $quantity = 0;
            }
            //print "{$channel_id} {$mapped_product} {$sku} {$price} {$quantity} {$slab} {$order}";exit;
            Mage::getmodel('bridge/incomingchannels_product')->saveSkuInfo($channel_id, $mapped_product, $sku, $price, $quantity, $slab, $order);
        }
    }

    //end
    //Get category_id(or product_type_id from souq)
    public function getSouqProductTypeidAction() {
        //Below html code of souq produt_type name or category name from the http:uae.souq.com
        //first div for category name of produt_type name list from souq site
        $string = '<div class="large-4 columns"><h3 class="shop-all-title">Appliances</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/air-conditioner/l/">Air Conditioners</a></li><li><a href="http://uae.souq.com/ae-en/air-treatment/l/">Air Treatment</a></li><li><a href="http://uae.souq.com/ae-en/blenders_mixers/l/">Blenders &amp; Mixers</a></li><li><a href="http://uae.souq.com/ae-en/hot-beverage-maker/l/">Coffee &amp; Espresso Makers</a></li><li><a href="http://uae.souq.com/ae-en/deep-fryer/l/">Deep Fryers</a></li><li><a href="http://uae.souq.com/ae-en/dishwashers/l/">Dishwashers</a></li><li><a href="http://uae.souq.com/ae-en/dryers/l/">Dryers</a></li><li><a href="http://uae.souq.com/ae-en/electric-slicer/l/">Electric Slicers</a></li><li><a href="http://uae.souq.com/ae-en/fans/l/">Fans</a></li><li class="parent"><a class="sub-shop-title">Food Preparation Center</a></li><li><a href="http://uae.souq.com/ae-en/food_processor/l/">Food Processors</a></li><li><a href="http://uae.souq.com/ae-en/heater/l/">Heaters</a></li><li><a href="http://uae.souq.com/ae-en/ironing-accessories/l/">Ironing Accessories</a></li><li><a href="http://uae.souq.com/ae-en/irons/l/">Irons</a></li><li><a href="http://uae.souq.com/ae-en/juice-extractor/l/">Juicers &amp; Presses</a></li><li><a href="http://uae.souq.com/ae-en/kettle/l/">Kettles</a></li><li><a href="http://uae.souq.com/ae-en/kitchen-scale/l/">Kitchen Scales</a></li><li><a href="http://uae.souq.com/ae-en/large-appliance/l/">Large Appliances</a></li><li><a href="http://uae.souq.com/ae-en/microwave/l/">Microwaves</a></li><li><a href="http://uae.souq.com/ae-en/ovens-ranges/l/">Ovens &amp; Ranges</a></li><li><a href="http://uae.souq.com/ae-en/rangehood/l/">Range Hoods</a></li><li><a href="http://uae.souq.com/ae-en/refrigerators-freezers/l/">Refrigerators &amp; Freezers</a></li><li><a href="http://uae.souq.com/ae-en/rice-cooker/l/">Rice Cooker</a></li><li><a href="http://uae.souq.com/ae-en/sandwich-waffle-makers-grill/l/">Sandwich &amp; Waffle Makers</a></li><li><a href="http://uae.souq.com/ae-en/sewing-accessories/l/">Sewing &amp; Accessories</a></li><li class="parent"><a class="sub-shop-title">Small Appliances Center</a></li><li><a href="http://uae.souq.com/ae-en/steam-cleaner/l/">Steam Cleaners</a></li><li><a href="http://uae.souq.com/ae-en/lcd-led-dlp-tv/l/">Televisions</a></li><li><a href="http://uae.souq.com/ae-en/toaster/l/">Toasters</a></li><li><a href="http://uae.souq.com/ae-en/vacuum-cleaner/l/">Vacuum Cleaners</a></li><li><a href="http://uae.souq.com/ae-en/vacuums-floor-care/l/">Vacuum Floor Care Accessories</a></li><li><a href="http://uae.souq.com/ae-en/washers-dryers/l/">Washers Dryers 2 in 1</a></li><li><a href="http://uae.souq.com/ae-en/washing-machines/l/">Washing Machines</a></li></ul></div><h3 class="shop-all-title">Bags &amp; Wallets</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/backpacks/l/">Backpacks</a></li><li><a href="http://uae.souq.com/ae-en/Bag-carrying-Case/l/">Bags &amp; Carry Cases</a></li><li><a href="http://uae.souq.com/ae-en/handbags/l/">Handbags</a></li><li><a href="http://uae.souq.com/ae-en/luggage/l/">Luggage &amp; Accessories</a></li><li><a href="http://uae.souq.com/ae-en/school-bags/l/">School Bags</a></li><li><a href="http://uae.souq.com/ae-en/wallets/l/">Wallets</a></li></ul></div><h3 class="shop-all-title">Books</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/business-trade-books/l/">Business &amp; Trade Books</a></li><li><a href="http://uae.souq.com/ae-en/kids-book/l/">Children\'s Books</a></li><li><a href="http://uae.souq.com/ae-en/comic-graphic-novel/l/">Comics &amp; Graphic Novels</a></li><li><a href="http://uae.souq.com/ae-en/educational-book/l/">Education, Learning &amp; Self Help Books</a></li><li><a href="http://uae.souq.com/ae-en/books/l/">Lifestyle Books</a></li><li><a href="http://uae.souq.com/ae-en/fiction-literature/l/">Literature &amp; Fiction</a></li></ul></div><h3 class="shop-all-title">Coins, Stamps &amp; Paper money</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/coins/l/">Coins</a></li><li><a href="http://uae.souq.com/ae-en/paper-money/l/">Paper Money</a></li><li><a href="http://uae.souq.com/ae-en/stamps/l/">Stamps</a></li></ul></div><h3 class="shop-all-title">Footwear</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/athletic-shoes/l/">Athletic Shoes</a></li><li><a href="http://uae.souq.com/ae-en/boots/l/">Boots</a></li><li><a href="http://uae.souq.com/ae-en/shoes/l/">Casual &amp; Formal Shoes</a></li><li><a href="http://uae.souq.com/ae-en/sandals/l/">Sandals</a></li><li><a href="http://uae.souq.com/ae-en/shoes-accessories/l/">Shoes Accessories</a></li><li><a href="http://uae.souq.com/ae-en/slippers/l/">Slippers</a></li></ul></div><h3 class="shop-all-title">Health &amp; Personal Care</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/bath-body/l/">Bath &amp; Body</a></li><li><a href="http://uae.souq.com/ae-en/massagers/l/">Body Massagers</a></li><li><a href="http://uae.souq.com/ae-en/dental-care/l/">Dental Care</a></li><li><a href="http://uae.souq.com/ae-en/digital-fever-thermometer/l/">Digital Fever Thermometers</a></li><li><a href="http://uae.souq.com/ae-en/electric-shavers/l/">Electric Shavers &amp; Removal</a></li><li><a href="http://uae.souq.com/ae-en/electrical-personal-machine/l/">Electrical Personal Care</a></li><li><a href="http://uae.souq.com/ae-en/food-supplement/l/">Food Supplements &amp; Nutrition</a></li><li><a href="http://uae.souq.com/ae-en/men-grooming/l/">Men\'s Grooming</a></li><li><a href="http://uae.souq.com/ae-en/natural-nutrition-products/l/">Natural Nutrition Products</a></li><li><a href="http://uae.souq.com/ae-en/health-personal-care/l/">Personal Care</a></li><li><a href="http://uae.souq.com/ae-en/personal-scale/l/">Personal Scales</a></li><li><a href="http://uae.souq.com/ae-en/small-medical-equipment/l/">Small Medical Equipment</a></li><li><a href="http://uae.souq.com/ae-en/sport-nutrition/l/">Sports Nutrition</a></li><li><a href="http://uae.souq.com/ae-en/vitamin-mineral/l/">Vitamins &amp; Minerals</a></li></ul></div><h3 class="shop-all-title">Kitchen &amp; Home Supplies</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/baking-tools-accessories/l/">Baking Tools &amp; Accessories</a></li><li><a href="http://uae.souq.com/ae-en/utensils/l/">Cooking Utensils</a></li><li><a href="http://uae.souq.com/ae-en/cooking-set/l/">Cookware &amp; Bakeware</a></li><li><a href="http://uae.souq.com/ae-en/cutlery-sets/l/">Cutlery &amp; Flatware Set</a></li><li><a href="http://uae.souq.com/ae-en/dinnerware/l/">Dinnerware &amp; Serveware</a></li><li><a href="http://uae.souq.com/ae-en/drinkware/l/">Drinkware</a></li><li><a href="http://uae.souq.com/ae-en/fruit-vegetable-tools/l/">Fruit &amp; Vegetable Tools</a></li><li><a href="http://uae.souq.com/ae-en/home-supplies/l/">Home Supplies</a></li><li><a href="http://uae.souq.com/ae-en/kitchen-dining/l/">Kitchen &amp; Dining Tools</a></li><li><a href="http://uae.souq.com/ae-en/kitchen-measuring-tools/l/">Kitchen Measuring Tools</a></li><li><a href="http://uae.souq.com/ae-en//kitchen-dining/kitchen-storage/a-6346/l/">Kitchen Storage</a></li><li><a href="http://uae.souq.com/ae-en/table-linens/l/">Table Linens</a></li></ul></div><h3 class="shop-all-title">Office Products &amp; Supplies</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/fax-machine-shredder/l/">Fax Machines</a></li><li><a href="http://uae.souq.com/ae-en/multifunction-devices/l/">Multifunctional Printers</a></li><li><a href="http://uae.souq.com/ae-en/office-equipment/l/">Office Equipment</a></li><li><a href="http://uae.souq.com/ae-en/office-furniture/l/">Office Furniture</a></li><li><a href="http://uae.souq.com/ae-en/office-supplies/l/">Office Supplies</a></li><li><a href="http://uae.souq.com/ae-en/printer/l/">Printers</a></li><li class="parent"><a class="sub-shop-title">Printers, Scanners, Hardware &amp; Accessories</a></li><li><a href="http://uae.souq.com/ae-en/stationary/l/">Stationery</a></li></ul></div><h3 class="shop-all-title">Sports &amp; Fitness</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/athletic-shoes/l/">Athletic Shoes</a></li><li><a href="http://uae.souq.com/ae-en/Bag-carrying-Case/l/">Bags &amp; Carry Cases</a></li><li><a href="http://uae.souq.com/ae-en/bikes-scooters/l/">Bikes, Scooters &amp; Ride-Ons</a></li><li><a href="http://uae.souq.com/ae-en/camping-goods/l/">Camping , Hiking &amp; Climbing  Goods</a></li><li class="parent"><a class="sub-shop-title">Fitness Technology Center</a></li><li><a href="http://uae.souq.com/ae-en/gps-receiver/l/">GPS Receiver</a></li><li><a href="http://uae.souq.com/ae-en/outdoor_play/l/">Outdoor Play</a></li><li><a href="http://uae.souq.com/ae-en/sporting-goods/l/">Sporting Goods</a></li><li><a href="http://uae.souq.com/ae-en/sport-equipment/l/">Sports Equipments</a></li></ul></div><h3 class="shop-all-title">Vouchers &amp; Tickets</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/gift-vouchers/l/">Gift Vouchers</a></li></ul></div></div>';
        //second div for category name of produt_type name list from souq site
        $string .= '<div class="large-4 columns"><h3 class="shop-all-title">Art, Crafts &amp; Collectibles</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/antique/l/">Antiques</a></li><li><a href="http://uae.souq.com/ae-en/drawing-painting/l/">Drawings &amp; Paintings</a></li><li><a href="http://uae.souq.com/ae-en/handcraft-sculpture-carving/l/">Handcrafts, Sculpture &amp; Carvings</a></li><li><a href="http://uae.souq.com/ae-en/islamic-ethnic-digital-art/l/">Islamic</a></li><li><a href="http://uae.souq.com/ae-en/map-atlas-globe/l/">Maps, Atlases &amp; Globes</a></li><li><a href="http://uae.souq.com/ae-en/photograph/l/">Photographs</a></li><li><a href="http://uae.souq.com/ae-en/Prints and Posters/l/">Prints &amp; Posters</a></li></ul></div><h3 class="shop-all-title">Beauty</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/beauty-gift-set/l/">Beauty Gifts Sets</a></li><li><a href="http://uae.souq.com/ae-en/beauty-tools-accessories/l/">Beauty Tools &amp; Accessories</a></li><li><a href="http://uae.souq.com/ae-en/hair-care/l/">Hair Care</a></li><li><a href="http://uae.souq.com/ae-en/hair_electronics/l/">Hair Electronics</a></li><li><a href="http://uae.souq.com/ae-en/hair-accessories/l/">Hair Tools &amp; Accessories</a></li><li><a href="http://uae.souq.com/ae-en/makeup/l/">Makeup</a></li><li><a href="http://uae.souq.com/ae-en/skin-care/l/">Skin Care</a></li><li><a href="http://uae.souq.com/ae-en/wigs/l/">Wigs</a></li></ul></div><h3 class="shop-all-title">Car Electronics &amp; Accessories</h3><div class="grouped-list"><ul class="side-nav"><li class="parent"><a class="sub-shop-title">Accessories</a></li><li><a href="http://uae.souq.com/ae-en/car-audio/l/">Car Audio</a></li><li><a href="http://uae.souq.com/ae-en/car-navigation/l/">Car Navigation</a></li><li><a href="http://uae.souq.com/ae-en/car-speaker-subwoofers-amplifier/l/">Car Speakers, Subwoofers &amp; Amplifiers</a></li><li><a href="http://uae.souq.com/ae-en/car-video/l/">Car Video</a></li><li><a href="http://uae.souq.com/ae-en/gps-navigator/l/">GPS Navigators</a></li><li><a href="http://uae.souq.com/ae-en/gps-receiver/l/">GPS Receiver</a></li></ul></div><h3 class="shop-all-title">Computers, IT &amp; Networking</h3><div class="grouped-list"><ul class="side-nav"><li class="parent"><a class="sub-shop-title">Computer &amp; Laptop Accessories</a><</li><li class="parent"><a class="sub-shop-title">Computer Parts &amp; Components</a><</li><li class="parent"><a class="sub-shop-title">Computers &amp; Servers</a></li><li class="parent"><a class="sub-shop-title">Networking &amp; Accessories</a></li><li class="parent"><a class="sub-shop-title">Printers, Scanners, Hardware &amp; Accessories</a></li><li><a href="http://uae.souq.com/ae-en/software/l/">Software</a></li><li><a href="http://uae.souq.com/ae-en/laptop-notebook/l/">Laptops &amp; Netbooks</a></li></ul></div><h3 class="shop-all-title">Gaming</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/games-console/l/">Game Consoles</a></li><li><a href="http://uae.souq.com/ae-en/games-console-accessories/l/">Game Gadgets &amp; Accessories</a></li><li><a href="http://uae.souq.com/ae-en/screen-protectors/l/">Screen Protectors</a></li><li><a href="http://uae.souq.com/ae-en/skins-decals/l/">Skins &amp; Decals</a></li><li><a href="http://uae.souq.com/ae-en/games/l/">Video Games</a></li></ul></div><h3 class="shop-all-title">Home Decor &amp; Furniture</h3><div class="grouped-list"><ul class="side-nav"><li class="parent"><a class="sub-shop-title">Home Decor Center</a></li><li class="parent"><a class="sub-shop-title">Lamps &amp; Lighting</a></li><li class="parent"><a class="sub-shop-title">Furniture</a></li></ul></div><h3 class="shop-all-title">Mobile Phones, Tablets &amp; Accessories</h3><div class="grouped-list"><ul class="side-nav"><li class="parent"><a class="sub-shop-title">Accessories</a></li><li><a href="http://uae.souq.com/ae-en/phone-number-line/l/">Fancy Numbers</a></li><li><a href="http://uae.souq.com/ae-en/mobile-phone/l/">Mobile Phones</a></li><li><a href="http://uae.souq.com/ae-en/screen-protectors/l/">Screen Protectors</a></li><li><a href="http://uae.souq.com/ae-en/skins-decals/l/">Skins &amp; Decals</a></li><li><a href="http://uae.souq.com/ae-en/smart-watches/l/">Smart Watches</a></li><li><a href="http://uae.souq.com/ae-en/tablet/l/">Tablets</a></li></ul></div><h3 class="shop-all-title">Perfumes &amp; Fragrances</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/perfumes-fragrances/l/">Perfumes &amp; Fragrances</a></li></ul></div><h3 class="shop-all-title">Tools &amp; Home Improvements</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/hand-tool/l/">Hand Tools</a></li><li><a href="http://uae.souq.com/ae-en/measuring-layout-tools/l/">Measuring &amp; Layout Tools</a></li><li><a href="http://uae.souq.com/ae-en/nails_screws_fixings/l/">Nails, Screws &amp; Fixings</a></li><li><a href="http://uae.souq.com/ae-en/paint_tools_supplies/l/">Paint &amp; Supplies</a></li><li><a href="http://uae.souq.com/ae-en/tools-accessories/l/">Power &amp; Hand Tools Accessories</a></li><li><a href="http://uae.souq.com/ae-en/power-tool/l/">Power Tools</a></li><li><a href="http://uae.souq.com/ae-en/safety-work-wear/l/">Safety &amp; Work Wear</a></li></ul></div><h3 class="shop-all-title">Watches &amp; Accessories</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/smart-watches/l/">Smart Watches</a></li><li><a href="http://uae.souq.com/ae-en/watch-accessories/l/">Watch Accessories</a></li><li><a href="http://uae.souq.com/ae-en/watches/l/">Watches</a></li></ul></div></div>';
        //third div for category name of produt_type name list from souq site
        $string .= '<div class="large-4 columns"><h3 class="shop-all-title">Baby</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/baby-accessories/l/">Baby Accessories</a></li><li><a href="http://uae.souq.com/ae-en/baby-bag/l/">Baby Bags</a></li><li><a href="http://uae.souq.com/ae-en/baby-bath-skin-care/l/">Baby Bath &amp; Skin Care</a></li><li><a href="http://uae.souq.com/ae-en/baby-clothes/l/">Baby Clothing &amp; Shoes</a></li><li><a href="http://uae.souq.com/ae-en/baby-food/l/">Baby Food</a></li><li><a href="http://uae.souq.com/ae-en/baby-gear/l/">Baby Gear</a></li><li><a href="http://uae.souq.com/ae-en/baby-gift-set/l/">Baby Gift Sets</a></li><li><a href="http://uae.souq.com/ae-en/baby-safety-health/l/">Baby Safety &amp; Health</a></li><li><a href="http://uae.souq.com/ae-en/baby-toy-accessories/l/">Baby Toys &amp; Accessories</a></li><li><a href="http://uae.souq.com/ae-en/diapers/l/">Diapers</a></li><li><a href="http://uae.souq.com/ae-en/feeding-diapering-bathing/l/">Feeding</a></li><li><a href="http://uae.souq.com/ae-en/nursery-furniture-decor/l/">Nursery Furniture</a></li></ul></div><h3 class="shop-all-title">Bed &amp; Bath</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/bathroom-equipment/l/">Bathroom Equipment</a></li><li><a href="http://uae.souq.com/ae-en/bedding/l/">Bedding</a></li><li><a href="http://uae.souq.com/ae-en/pillows/l/">Bed Pillows</a></li><li><a href="http://uae.souq.com/ae-en/blankets-throws/l/">Blankets &amp; Throws</a></li><li><a href="http://uae.souq.com/ae-en/curtains/l/">Curtains</a></li><li><a href="http://uae.souq.com/ae-en/mattresses/l/">Mattresses</a></li><li><a href="http://uae.souq.com/ae-en/showers-showerheads/l/">Showers &amp; Showerheads</a></li><li><a href="http://uae.souq.com/ae-en/cabinet/l/">Storage &amp; Organization</a></li><li><a href="http://uae.souq.com/ae-en/towels/l/">Towels</a></li></ul></div><h3 class="shop-all-title">Clothing &amp; Accessories</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/accessories/l/">Accessories</a></li><li><a href="http://uae.souq.com/ae-en/athletic-wear/l/">Athletic Wear</a></li><li><a href="http://uae.souq.com/ae-en/costumes/l/">Costumes</a></li><li><a href="http://uae.souq.com/ae-en/dresses/l/">Dresses</a></li><li><a href="http://uae.souq.com/ae-en/ethnic-traditional-wear/l/">Ethnic &amp; Traditional Wear</a></li><li><a href="http://uae.souq.com/ae-en/eyewear/l/">Eyewear</a></li><li><a href="http://uae.souq.com/ae-en/jacket-coats/l/">Jackets &amp; Coats</a></li><li><a href="http://uae.souq.com/ae-en/maternity-wear/l/">Maternity Wear</a></li><li><a href="http://uae.souq.com/ae-en/pants/l/">Pants</a></li><li><a href="http://uae.souq.com/ae-en/shorts/l/">Shorts</a></li><li><a href="http://uae.souq.com/ae-en/skirts/l/">Skirts</a></li><li><a href="http://uae.souq.com/ae-en/sleepwear/l/">Sleepwear</a></li><li><a href="http://uae.souq.com/ae-en/suits/l/">Suits</a></li><li><a href="http://uae.souq.com/ae-en/swimwears/l/">Swimwear</a></li><li><a href="http://uae.souq.com/ae-en/tops/l/">Tops</a></li><li><a href="http://uae.souq.com/ae-en/underwears/l/">Underwear</a></li><li><a href="http://uae.souq.com/ae-en/uniforms/l/">Uniforms</a></li><li><a href="http://uae.souq.com/ae-en/women-lingerie/l/">Women\'s Lingerie</a></li></ul></div><h3 class="shop-all-title">Electronics</h3><div class="grouped-list"><ul class="side-nav"><li class="parent"><a class="sub-shop-title"> Audio &amp; Accessories</a></li><li><a href="http://uae.souq.com/ae-en/batteries/l/">Batteries</a></li><li><a href="http://uae.souq.com/ae-en/cables/l/">Cables</a></li><li class="parent"><a class="sub-shop-title"> Cameras &amp; Photos</a></li><li><a href="http://uae.souq.com/ae-en/cd-recording-media/l/">CD Recording Media</a></li><li><a href="http://uae.souq.com/ae-en/laptop-charger/l/">Chargers</a></li><li><a href="http://uae.souq.com/ae-en/clock-radio/l/">Clock Radios</a></li><li><a href="http://uae.souq.com/ae-en/ebook-reader/l/">E-Book Readers</a></li><li class="parent"><a class="sub-shop-title"> Home &amp; Office Electronics</a></li><li><a href="http://uae.souq.com/ae-en/home-theatre-system/l/">Home Theater Systems</a></li><li><a href="http://uae.souq.com/ae-en/video-accessories/l/">Home Video Accessories</a></li><li class="parent"><a class="sub-shop-title"> MP3, MP4 Player  &amp; Accessories</a></li><li class="parent"><a class="sub-shop-title"> Projectors &amp; Accessories</a></li><li><a href="http://uae.souq.com/ae-en/recording-studio-equipment/l/">Recording &amp; Studio Equipment</a></li><li><a href="http://uae.souq.com/ae-en/security-surveillance-system/l/">Security &amp; Surveillance Systems</a></li><li><a href="http://uae.souq.com/ae-en/smart-watches/l/">Smart Watches</a></li><li><a href="http://uae.souq.com/ae-en/stereo-system-equalizer/l/">Stereo Systems &amp; Equalizers</a></li><li class="parent"><a class="sub-shop-title"> TVs, Satellites &amp; Accessories</a></li><li><a href="http://uae.souq.com/ae-en/vcd-vcp-vcr-player/l/">VCD, VCP and VCR Players</a></li><li class="parent"><a class="sub-shop-title"> Video, Home Theater &amp; Accessories</a></li></ul></div><h3 class="shop-all-title">Garden &amp; Outdoor</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/barbecue-tool-grill-accessories/l/">Barbecue Tools &amp; Grill Accessories</a></li><li><a href="http://uae.souq.com/ae-en/garden-decoration/l/">Garden Decoration</a></li><li><a href="http://uae.souq.com/ae-en/garden-furniture/l/">Garden Furniture</a></li><li><a href="http://uae.souq.com/ae-en/garden-equipment-watering/l/">Gardening &amp; Watering Supplies</a></li><li><a href="http://uae.souq.com/ae-en/garden-light/l/">Garden Lighting</a></li><li><a href="http://uae.souq.com/ae-en/grill-smoker/l/">Grills &amp; Smokers</a></li><li><a href="http://uae.souq.com/ae-en/pest-control/l/">Pest Control</a></li><li><a href="http://uae.souq.com/ae-en/smoking-accessories/l/">Smoking Accessories</a></li></ul></div><h3 class="shop-all-title">Jewelry &amp; Accessories</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/bracelets/l/">Bracelets</a></li><li><a href="http://uae.souq.com/ae-en/earrings/l/">Earrings</a></li><li><a href="http://uae.souq.com/ae-en/jewelry-accessories/l/">Jewelry Accessories</a></li><li><a href="http://uae.souq.com/ae-en/jewelry-set/l/">Jewelry Sets</a></li><li><a href="http://uae.souq.com/ae-en/loose-gemstones-diamond/l/">Loose Gemstones &amp; Diamonds</a></li><li><a href="http://uae.souq.com/ae-en/men-jewleries/l/">Men\'s Jewelry</a></li><li><a href="http://uae.souq.com/ae-en/necklace-pendant/l/">Necklaces, Pendants &amp; Charms</a></li><li><a href="http://uae.souq.com/ae-en/rings/l/">Rings</a></li></ul></div><h3 class="shop-all-title">Music &amp; Movies</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/movies-plays-series/l/">Movies, Plays &amp; Series</a></li><li><a href="http://uae.souq.com/ae-en/music-cd/l/">Music CDs</a></li><li><a href="http://uae.souq.com/ae-en/musical-instrument/l/">Musical Instruments</a></li><li><a href="http://uae.souq.com/ae-en/musical-instrument-parts/l/">Musical Instruments Parts &amp; Accessories</a></li><li><a href="http://uae.souq.com/ae-en/tuner/l/">Tuners</a></li></ul></div><h3 class="shop-all-title">Pet Supplies</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/pet_food/l/">Pet Food</a></li><li><a href="http://uae.souq.com/ae-en/pet-supplies/l/">Pet Supplies</a></li></ul></div><h3 class="shop-all-title">Toys</h3><div class="grouped-list"><ul class="side-nav"><li><a href="http://uae.souq.com/ae-en/bikes-scooters/l/">Bikes, Scooters &amp; Ride-Ons</a></li><li><a href="http://uae.souq.com/ae-en/toys/l/">Toys</a></li></ul></div></div>';
        $pattern = '/<a .*?>(.*?)<\/a>/'; //Search hyperlink content as pattern
        preg_match_all($pattern, $string, $matches);
        $producttypelist = array_unique($matches[1]); //all category_name or product_type name
        //$producttypelist = array(0 => 'Air Conditioners', 1 => 'Air Treatment', 2 => 'Blenders & Mixers');//for testing array
        //get souqcategory id from souq by api call and keep them in jsondata array
        $jsondata_category = array();
        $i = 0;
        foreach ($producttypelist as $catgoryname) {
            $category_name = urlencode($catgoryname);
            $url = "https://api.souq.com/v1/products/types?q=" . $category_name . "&page=1&show=10&language=en&format=json&app_id=6042584&app_secret=co6M6NR0yESzNfB0qKDu";
            $jsondata_category[$i]['category_name'] = $catgoryname;
            $jsondata_category[$i]['category_id'] = file_get_contents($url);
            $i = $i + 1;
        }
        //Insert the category name and id to table bridge_souqcategory

        Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $souqcategory_table_name = $resource->getTableName('souq/incomingchannels_souqcategorylist');

        $writeConnection->insertMultiple($souqcategory_table_name, $jsondata_category);
        echo "Souq Categoryname and categoryid inserted successfully to bridge_souqcategory table ";
        exit;
    }

    //get brand name of all categoryid or producttypeid from souq
    public function getSouqBrandNameAction() {
        //Get categoryname and category_id from table bridge_souqcategory
        Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $souqcategory_table_name = $resource->getTableName('souq/incomingchannels_souqcategorylist');
        $sql = "SELECT * FROM " . $souqcategory_table_name . " WHERE 1";

        $souqcategory_lists = $readConnection->fetchAll($sql);
        //create array for brand(id,value) list by category wise  
        $brandname_insert_details = array();
        //$i = 0;
        foreach ($souqcategory_lists as $category) {
            $i = 0;
            $brandname_insert_details = array();
            //Get the exact categoryid from the category name on 14th may 2016
            $category_name = $category['category_name'];
            $category_details = json_decode($category['category_id']);
            $category_ids = $category_details->data;
            foreach($category_ids as $v){
                if(!strcmp(trim(html_entity_decode($v->label_singular)), trim(html_entity_decode($category_name))) || !strcmp(trim(html_entity_decode($v->label_plural)), trim(html_entity_decode($category_name)))) {
                    $categoryid = $v->id;
                }
            }
            //end of categoryid on 14th may 2016
//            $categoryid = $category_ids->data[0]->id;//old code for getting the category id
            //souq Api url for getting brand id and value from the corresponding categoryid or product_type
            $url = "https://api.souq.com/v1/products/types/" . $categoryid . "?show_attributes_values=1&language=en&format=json&app_id=6042584&app_secret=co6M6NR0yESzNfB0qKDu";
            //get souq api call data as json format
            $souqbrandlist = file_get_contents($url);
            $souqbrandnames = json_decode($souqbrandlist)->data->attributes;
            //create array for brand(id,value) list by category wise for inserting to  the bridge_souqbrand table
            foreach ($souqbrandnames as $brandname) {
                //if (($brandname->name == "manufacturer") && ($brandname->label == "Brand") && ($brandname->id == '7')) {
                if (count($brandname->values)) {
                    $brandname_insert_details[$i]['category_name'] = $category['category_name'];
                    $brandname_insert_details[$i]['category_id'] = $categoryid;
                    $brandname_insert_details[$i]['brand_attr_id'] = $brandname->id;
                    $brandname_insert_details[$i]['brand_attr_name'] = $brandname->name;
                    $brandname_insert_details[$i]['brand_id_value'] = json_encode($brandname->values);
                }
                //}
                $i = $i + 1;
            }
            //Insert array for brand(id,value) list by category wise in the table bridge_souqbrand
            if (count($brandname_insert_details)) {
                $souqbrand_table_name = $resource->getTableName('souq/incomingchannels_souqbrandlist');
                $writeConnection->insertMultiple($souqbrand_table_name, $brandname_insert_details);
            }
        }

        echo "Souq brand id and value inserted successfully to bridge_souqbrand table ";
    }

    //Get brand attribute id from the bridge_souqbrand  table
    public function getBrandAttrIdAction() {
        $category_id = $this->getRequest()->getParam('categoryid');
        $category_name = htmlentities($this->getRequest()->getParam('category_name'));
        $attr_id = $this->getRequest()->getParam('attr_id');
        $souq_attr_id_list = Mage::getModel('souq/incomingchannels_souqbrandlist')->getSouqAttrIdList($category_id, $category_name);

        $Attr_id_opt = '<select class="addmore_attr_id_cls souq-condition-params-addmore" title="Attribute id" name="souq_condition_attrfilter[0][attr_id]" ><option value="0">Select</option>';
        foreach ($souq_attr_id_list as $val) {
            if ($attr_id == $val['brand_attr_id']) {
                $Attr_id_opt .= '<option selected="selected" value="' . $val['brand_attr_id'] . '">' . $val['brand_attr_name'] . '</option>';
            } else {
                $Attr_id_opt .= '<option value="' . $val['brand_attr_id'] . '">' . $val['brand_attr_name'] . '</option>';
            }
        }
        $Attr_id_opt .= '</select>';
        echo $Attr_id_opt;
    }

    //Get brand attribute id and value from the bridge_souqbrand  table
    public function getBrandAttrIdValueAction() {
        $category_id = $this->getRequest()->getParam('categoryid');
        $attr_id = $this->getRequest()->getParam('attr_id');
        $attr_value = $this->getRequest()->getParam('attr_value'); //First brand attr_value for selecting the attr_value
        $category_name = htmlentities($this->getRequest()->getParam('category_name'));
        $position_dropdown = $this->getRequest()->getParam('position_dropdown');
        $souq_attr_id_value_list = Mage::getModel('souq/incomingchannels_souqbrandlist')->getSouqAttrIdValueList($category_id, $attr_id, $category_name);

        $souq_attr_id_value_opt = '<select class="addmore_attr_value_cls souq-condition-params-addmore" title="Attribute value" name="souq_condition_attrfilter[' . $position_dropdown . '][attr_value]" ><option value="0">Select</option>';
        foreach ($souq_attr_id_value_list as $id => $val) {
            if ($val == $attr_value) {
                $souq_attr_id_value_opt .= '<option selected="selected" value="' . $val . '">' . $val . '</option>';
            } else {
                $souq_attr_id_value_opt .= '<option value="' . $val . '">' . $val . '</option>';
            }
        }
        $souq_attr_id_value_opt .= '</select>';
        echo $souq_attr_id_value_opt;
    }

    public function getRestBrandAttrIdValueAction() {
        $category_id = $this->getRequest()->getParam('categoryid');
        $category_name = htmlentities($this->getRequest()->getParam('category_name'));
        $selected_attr_id = explode(',', $this->getRequest()->getParam('selected_attr_id')); //selected brand in array format
        $no_of_attrid_dropdown = $this->getRequest()->getParam('no_of_attrid_dropdown');

        //$souq_brand_opt = Mage::getModel('souq/incomingchannels_souqbrandlist')->getSouqBrandList($category_id);
        $souq_attr_id_list = Mage::getModel('souq/incomingchannels_souqbrandlist')->getSouqAttrIdList($category_id, $category_name);
        $souq_attr_id_opt = '<span class="addmore_span" id="attr_id_' . $no_of_attrid_dropdown . '"><select class="addmore_attr_id_cls souq-condition-params-addmore" title="Attribute id" name="souq_condition_attrfilter[' . $no_of_attrid_dropdown . '][attr_id]" ><option value="0">Select</option>';
        foreach ($souq_attr_id_list as $id => $val) {
            if (!in_array($val['brand_attr_id'], $selected_attr_id)) {
                $souq_attr_id_opt .= '<option value="' . $val['brand_attr_id'] . '">' . $val['brand_attr_name'] . '</option>';
            }
        }
        $souq_attr_id_opt .= '</select></span>';

        $str_1 = $souq_attr_id_opt;
        $str_2 = '<span class="addmore_span" id="attr_val_' . $no_of_attrid_dropdown . '"><select class="addmore_attr_value_cls souq-condition-params-addmore" title="Attribute value" name="souq_condition_attrfilter[' . $no_of_attrid_dropdown . '][attr_value]" ><option value="0">Select</option></select></span>';
        $str_3 = '<span class="addmore_btn_span"><input type="button" value="-" class="form-button addmore addmore_cls removebutton_cls" onclick="$j(this).closest(\'.addmore_div\').remove();"/></span>';
        $htm_div = '<div class="addmore_div" style="margin-bottom:10px;">' . $str_1 . $str_2 . $str_3 . '</div>';
        echo $htm_div;
        exit;
    }

}
