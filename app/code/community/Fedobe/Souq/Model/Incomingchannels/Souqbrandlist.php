<?php

class Fedobe_Souq_Model_Incomingchannels_Souqbrandlist extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('souq/incomingchannels_souqbrandlist');
    }
    
    //Get All souq category list 
    public function getSouqCategoryList(){
        Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $souqbrand_table_name = $resource->getTableName('souq/incomingchannels_souqbrandlist');
        $sql = "SELECT * FROM " . $souqbrand_table_name . " WHERE 1";
        $souq_categorydetails = $readConnection->fetchAll($sql);
        $souq_categorylist = array(0 => "Select");//$value["Select"];
        foreach ($souq_categorydetails as $key => $value) {
            $souq_categorylist[$value["category_id"]] = html_entity_decode($value["category_name"]);
        }
        return $souq_categorylist;
    }
    
    //Get All souq brand list of the given souq categoryId
    public function getSouqAttrIdValueList($category_id = "" , $attr_id = "" , $category_name = ""){
        Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $souqbrand_table_name = $resource->getTableName('souq/incomingchannels_souqbrandlist');
        $sql = "SELECT * FROM " . $souqbrand_table_name . " WHERE category_id =".$category_id ." AND brand_attr_id = ".$attr_id." AND category_name = '".$category_name."' lIMIT 1";
        $souq_Attr_details = $readConnection->fetchAll($sql);
        $souq_Attr_id_value_details = json_decode($souq_Attr_details[0]["brand_id_value"]);
        //$souq_brandlist = array(0 => "Select");//$value["Select"];
        foreach ($souq_Attr_id_value_details as $key => $value) {
            $souq_attr_id_value_list[$value->id] = $value->value;
        }
        return $souq_attr_id_value_list;
    }
    
    //Get All souq Attribute name and id of a souq categoryid given
    public function getSouqAttrIdList($category_id = "" , $category_name = ""){
        Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $souqbrand_table_name = $resource->getTableName('souq/incomingchannels_souqbrandlist');
        $sql = "SELECT * FROM " . $souqbrand_table_name . " WHERE category_id =".$category_id ." AND category_name = '".$category_name."'" ;
        $souq_attr_id_list = $readConnection->fetchAll($sql);
        return $souq_attr_id_list;
    }
    
}