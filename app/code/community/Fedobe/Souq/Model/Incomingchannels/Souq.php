<?php

class Fedobe_Souq_Model_Incomingchannels_Souq extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('souq/incomingchannels_souq');
    }

    public function getSouqattributeopt($searchindex, $channel_id) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $table = Mage::getSingleton('core/resource')->getTableName('souq/incomingchannels_souq'); //echo $table;
        $arr_list = array(); //echo $channel_id;
        if ($channel_id) {
            //$sql = 'SELECT lookup_attribute,attributes FROM ' . $table . ' WHERE bridge_channel_id=' . $channel_id;
            $sql = 'SELECT attributes FROM ' . $table . ' WHERE bridge_channel_id=' . $channel_id;
            $data = $readConnection->fetchAll($sql); //return $data;
            $attribute_arr_master = array();
            $arr_u = array();
            $arr_list = array();
            $m = 0;
            if (!empty($data)) {
                $arr_head = array();
                $arr_child= array();
                foreach ($data as $k3 => $v3) {
                    $attr_arr = array();
                    $attr_arr = (array) unserialize($v3['attributes']); 
                     $attr_arr_label = array_column($attr_arr, 'label');
                    $attr_arr_value = array_column($attr_arr, 'value');
                    $arr_k = array_combine($attr_arr_label, $attr_arr_value);
                    $arr_child = array_merge($arr_child,array_flip($arr_k));
                    
                }
                //print "<pre>";print_r($arr_child);print "</pre>";
                foreach($arr_child as $k1=>$v1){
                    $master_list[$k1]['id'] = $k1;
                    $master_list[$k1]['value'] = $k1;
                    $master_list[$k1]['attribute_id'] = $v1;
                }
            }
        }
        return $master_list;
    }

    public function getSouqattributes($searchindex, $channel_id) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read'); //echo $channel_id;
        $table = Mage::getSingleton('core/resource')->getTableName('souq/incomingchannels_souq');
        $arr_u = array();
        if ($channel_id) {
            //$sql = 'SELECT lookup_attribute,attributes FROM ' . $table . ' WHERE bridge_channel_id=' . $channel_id;
            $sql = 'SELECT attributes FROM ' . $table . ' WHERE bridge_channel_id=' . $channel_id;
            $data = $readConnection->fetchAll($sql); //return $data[0];
            $attribute_arr_master = array();
            $arr_u = array();
            $arr_list = array();
            $channel_data = Mage::registry('incomingchannel_data');
            $am_data = $channel_data->getOrigdata();
            $attr_set_list = Mage::getModel('bridge/incomingchannels')->getAttributeSetList();
            $am_data_arr = unserialize($am_data['souq_condition']);
            $searchindex = $am_data_arr[1]['q'];
            $chossen_attribute_set = $searchindex; //$attr_set_list[$searchindex];
            if (!empty($data)) {
                $arr_u = array();
                $arr_k = array();
                foreach ($data as $k3 => $v3) {
                    $attr_arr = array();
                    $attr_arr = (array) unserialize($v3['attributes']); //$attr_arr = (array) json_decode($v3['attributes']);
                    $attr_arr_label = array_column($attr_arr, 'label');
                    $arr_k = array_merge($arr_k,$attr_arr_label);
                }
                $arr_k = array_unique($arr_k);
                $arr_cnt_key = array_count_values($arr_k); //echo $arr_cnt_key;
                //print "<pre>";print_r($arr_cnt_key);print "</pre>";
                $m = 0;
                foreach ($arr_cnt_key as $k1 => $v1) {
                    $arr_u[$searchindex]['name'] = $chossen_attribute_set;
                    $arr_u[$searchindex][$m]['attribute_id'] = $k1;
                    $arr_u[$searchindex][$m]['attribute_code'] = $k1;
                    $arr_u[$searchindex][$m]['frontend_label'] = $k1;
                    $arr_u[$searchindex][$m]['store_id'] = 1;
                    $arr_u[$searchindex][$m]['value'] = $k1;
                    $m++;
                }//echo "<pre>"; print_r($arr_u);echo "</pre>";
            }
        }
        return $arr_u;
    }

    function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens. return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars. }
        return $string;
    }

    public function souqsave($columns, $values, $channel_id, $upd = 0) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $table = Mage::getSingleton('core/resource')->getTableName('souq/incomingchannels_souq');
        $sql = '';
        $channel_str = "bridge_channel_id=$channel_id";
        $sql_inest = array(
            'ean' => 'ean=VALUES(ean)',
            'bridge_channel_id' => $channel_str,
            'name' => 'name=VALUES(name)',
            'souq_price' => 'souq_price=VALUES(souq_price)',
            'msrp' => 'msrp=VALUES(msrp)',
            'detail_url' => 'detail_url=VALUES(detail_url)',
            'attributes' => 'attributes= if(VALUES(attributes)!="",VALUES(attributes),attributes)',
            'updated_at' => 'updated_at= NOW()'
        );
        if ($upd == 0) { //for insert new record
            $sql = "INSERT INTO  $table " . $columns . " VALUES " . $values . ' ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id),'
                    . implode(',', $sql_inest);
            //echo $sql;exit;
        } else if ($upd == 1) { //for asin update
            $sql = "INSERT INTO $table " . $columns . " VALUES " . $values . ' ON DUPLICATE KEY UPDATE '
                    . 'souq_price=VALUES(souq_price),'
                    . 'price=VALUES(price),'
                    . 'supplier=VALUES(supplier),'
                    . 'is_updated=1,'
                    . 'is_available=VALUES(is_available),'
                    . 'updated_at= NOW()';
        } else if ($upd = 2) {
            $sql = "INSERT INTO $table " . $columns . " VALUES " . $values . ' ON DUPLICATE KEY UPDATE '
                    . 'asin=VALUES(asin) ,'
                    //. 'sku=VALUES(sku),'
                    . 'manufacturer=if(VALUES(manufacturer)!="",VALUES(manufacturer),manufacturer),'
                    . 'is_updated=1,'
                    . 'attribute_set_id=if(VALUES(attribute_set_id)!="",VALUES(attribute_set_id),attribute_set_id),'
                    . 'updated_at= NOW()';
        }
        //echo $sql;exit;
        $writeConnection->query($sql);
        if ($upd == 0)
            $ret = 'success';
        else
            $ret = $sql;
        return $ret;
    }


    function getActivesouqchannels() {
        $incomingchannel_ids = 0;
        //get active incoming channel ids
        $incomingchannels = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('channel_type', 'souq')
                ->addFieldToFilter('mode', 'live')
                ->getData();

        if ($incomingchannels) {
            $incomingchannel_ids = array_column($incomingchannels, 'id');
        }
        return count($incomingchannel_ids);
    }

    function magento_pricesave($channel_info, $price_change) {
        if (!empty($channel_info) && !empty($price_change)) {
            foreach ($price_change as $k => $v) {
                $data = Mage::getModel('catalog/product')->getCollection()
                                ->addAttributeToFilter('entity_id', $v['entity_id'])->getFirstItem();
                if (!empty($data->getData())) {
                    $_product_m = Mage::getModel('catalog/product');
                    $product = $_product_m->load($data->getData('entity_id'));
                    $product->setPrice($v['converted_price']);
                    $product->save();
                    Mage::getModel('bridge/incomingchannels_product')->saveSkuInfo($channel_info['id'], $v['sku'], $v['souq_price'], $v['qty'], $slab = 0, $order = 0, $v['converted_price']);
                    //$m++;
                }
            }//echo 'done';exit;
        }
        return 1;
    }

    function magento_msrpsave($msrp_change) {
        if (!empty($msrp_change)) {
            foreach ($msrp_change as $k => $v) {
                $data = Mage::getModel('catalog/product')->getCollection()
                                ->addAttributeToFilter('entity_id', $v['entity_id'])->getFirstItem();
                if (!empty($data->getData())) {
                    $_product_m = Mage::getModel('catalog/product');
                    $product = $_product_m->load($data->getData('entity_id'));
                    $product->setMsrp($v['msrp']);
                    $product->save();
                }
            }
        }
        return 1;
    }

    function checkforimage($channel_id) {
        $ret = 0;
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $table = Mage::getSingleton('core/resource')->getTableName('souq/incomingchannels_souq');
        $sql = "SELECT COUNT(*) FROM $table WHERE bridge_channel_id=$channel_id AND is_img_updated=0";
        $data = $readConnection->fetchAll($sql);
        if (!$data[0]['COUNT(*)']) {
            $ret = 1;
        }
        return $ret;
    }

}
