<?php
class Fedobe_Souq_Model_Incomingchannels_Importskulist extends Mage_Core_Model_Abstract {
     public function _construct()
    {
         parent::_construct();
        $this->_init('souq/incomingchannels_importskulist', 'id');
    }
    public function skuexist($sku,$bridge_channel_id)
    {
        $this->setData($this->getResource()->skuexist($sku,$bridge_channel_id));
        return $this;
    }

}
