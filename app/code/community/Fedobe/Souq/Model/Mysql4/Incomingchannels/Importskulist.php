<?php
class Fedobe_Souq_Model_Mysql4_Incomingchannels_Importskulist extends Mage_Core_Model_Mysql4_Abstract {
     public function _construct()
    {
        $this->_init('souq/incomingchannels_importskulist', 'id');
    }
    public function skuexist($sku,$bridge_channel_id)
    {
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('souq/incomingchannels_importskulist'))
            ->where('sku=:sku')
            ->where('bridge_channel_id=:bridge_channel_id');
        return $adapter->fetchAll($select, array('sku'=>$sku,'bridge_channel_id'=>$bridge_channel_id));
    }
   
}
