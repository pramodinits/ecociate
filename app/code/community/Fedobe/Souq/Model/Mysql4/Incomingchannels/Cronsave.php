<?php
class Fedobe_Souq_Model_Mysql4_Incomingchannels_Cronsave extends Mage_Core_Model_Mysql4_Abstract {
     public function _construct()
    {
        $this->_init('souq/incomingchannels_cronsave', 'id');
    }
    public function checkif_cronexist($bridge_channe_id){
        $adapter = $this->_getReadAdapter();
        $select = $adapter->select()->from($this->getTable('souq/incomingchannels_cronsave'))
            ->where('bridge_channel_id=:bridge_channe_id');
        return $adapter->fetchRow($select, array('bridge_channe_id'=>$bridge_channe_id));
    
    }
}
