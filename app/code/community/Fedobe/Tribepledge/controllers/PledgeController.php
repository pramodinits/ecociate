<?php require_once Mage::getBaseDir('code') . DS . 'community/Fedobe/Arccore/controllers/Adminhtml/ProfilesController.php';

class Fedobe_Tribepledge_PledgeController extends Fedobe_Arccore_Adminhtml_ProfilesController {

    protected $_entityTypeId;

    public function preDispatch() {
        //parent::preDispatch();
        $this->_entityTypeId = Mage::helper('arccore')->getEntityTypeId('arccore');
        $this->_attributesetId = Mage::helper('tribepledge')->getAttributeSetId();
        $bypassaction = array('save');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }
    //we add this method this method previously go to parent page
    public function saveAction() {
        $datas = $this->getRequest()->getPost('arc');
        if ($datas['trpl_email']) {
            $content = 'Dear admin, <br/><br/>'
                    . 'Following are the pledge details:<br /><br/>'
                    . ' Contact Person Name : ' . $datas["trcr_first_name"] .
                    ' Contact Person Email : ' . $datas["trpl_email"] .
                    '<br/> Contact No : ' . $datas["trpl_contact_no"] .
                    '<br/> Comment : ' . $datas['trpl_comment'] . '<br/><br/>'
                    . 'Kind Regards,<br/>EarthynGreen Team (<a href="mailto:info@earthyngreen.com">info@earthyngreen.com</a>)<br/><br/><a href="http://www.earthyngreen.com/">www.earthyngreen.com</a>';

            $contentUser = 'Dear ' . $datas["trcr_first_name"] . ', <br/><br/>'
                    . 'Welcome to EarthynGreen. Thank you for taking a step towards being a responsible consumer.<br /><br />' .
                    ' Kind Regards,<br/>EarthynGreen Team (<a href="mailto:info@earthyngreen.com">info@earthyngreen.com</a>)<br/><br/><a href="http://www.earthyngreen.com/">www.earthyngreen.com</a>';
            /* core email */
            require('email/class.phpmailer.php');
            $mail = new PHPMailer();
            $subject = "Pledge for Green";
            $mail->IsSMTP();
            $mail->SMTPDebug = 0;
            $mail->SMTPAuth = TRUE;
            $mail->SMTPSecure = "tls";
            $mail->Port = SMTPPORT;
            $mail->Username = SMTPUSERNAME;
            $mail->Password = SMTPPASSWORD;
            $mail->Host = SMTPHOST;
            $mail->Mailer = "smtp";
            $mail->From = SMTPFROMEMAIL;
            $mail->FromName = SMTPFROMNAME;
            $mail->AddAddress(SMTPTOEMAIL);
            $mail->Subject = $subject;
            $mail->WordWrap = 80;
            $mail->MsgHTML($content);
            $mail->IsHTML(true);
            
            $usermail = new PHPMailer();
            $subject = "Pledge for Green";
            $usermail->IsSMTP();
            $usermail->SMTPDebug = 0;
            $usermail->SMTPAuth = TRUE;
            $usermail->SMTPSecure = "tls";
            $usermail->Port = SMTPPORT;
            $usermail->Username = SMTPUSERNAME;
            $usermail->Password = SMTPPASSWORD;
            $usermail->Host = SMTPHOST;
            $usermail->Mailer = "smtp";
            $usermail->From = SMTPFROMEMAIL;
            $usermail->FromName = SMTPFROMNAME;
            $usermail->AddAddress($datas['trpl_email']);
            //$mail->AddAddress(SMTPTOEMAIL);
//            $mail->SetFrom("pramodini.das@thoughtspheres.com", "Pramodini Das");
//            $mail->AddReplyTo($datas['email'], $datas['name']);
            $usermail->Subject = $subject;
            $usermail->WordWrap = 80;
            $usermail->MsgHTML($contentUser);
            $usermail->IsHTML(true);
            $usermail->Send();
            /* core email */
            if (!$mail->Send()) {
                $res['haserror'] = true;
                $res['resultmail'] = $mail->ErrorInfo;
            } else {
                $res['haserror'] = false;
            }
        } else {
            $res['haserror'] = true;
        }
        echo json_encode($res);
        exit();
    }
}
?>