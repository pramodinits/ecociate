<?php

class Fedobe_Tribepledge_Helper_Data extends Mage_Core_Helper_Abstract {

    const ATTRIBUTESET_NAME = 'Tribe Pledge';

    public function getAttributeSetId() {
        $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                ->load(SELF::ATTRIBUTESET_NAME, 'attribute_set_name')
                ->getAttributeSetId();
        return $attributeSetId;
    }

    public function getProfileType() {
        return 'Pledge';
    }

//   public function getInfo($label,$info){
//           $collection = $this->getAllPhotos($info['entity_id']);
//           $count = $collection->getSize();
//      // $collection = $this->getAllReview($info['entity_id']);
//       return array('label' => $label, 'count' => $count,'url' => $this->getPhotoUrl($info['trcr_username']));
//   } 
    public function getAllPledge() {
        $attribute = Mage::getModel('eav/config')->getAttribute('fedobe_arccore_arc', 'trcr_name');
        $trcr_name_attr_id = $attribute->getId();
        $collection = Mage::getModel('arccore/arc')->getCollection()->addAttributeToSelect('*');
        $collection->addAttributeTofilter('attribute_set_id', Mage::helper('tribepledge')->getAttributeSetId());
        $collection->getSelect()->order('created_at ASC')->limit(5);
        return $collection;
    }

//   public function getPhotoUrl($parenturl_key){
//        $identifier = Mage::getStoreConfig('tribe_photos/edit/identifier') ?  Mage::getStoreConfig('tribe_photos/edit/identifier') : 'photos';
//        return trim(Mage::getUrl("$parenturl_key/$identifier"), "/");
//    }
//     public function isActionallowed($info) {
//        $allow = false;
//        if ($info['user_log_id'] || (Mage::app()->isInstalled() && Mage::getSingleton('customer/session')->isLoggedIn())) {
//            if ($info['trcr_account_type'] == 2) {
//                $id = $info['user_log_id']  ? $info['user_log_id'] : Mage::getSingleton('customer/session')->getCustomer()->getId();
//                if($info['trcr_customer_list'] == $id){
//                    $allow = true;
//                }
//            } else {
//                $email = $info['logged_in_email']  ? $info['logged_in_email'] : Mage::getSingleton('customer/session')->getCustomer()->getEmail();
//                if(trim($info['trcr_email']) == $email){
//                    $allow = true;
//                }
//            }
//        }
//        return $allow;
//    }
//     public function getcustomerdata($id){
//       $customerData = Mage::getModel('customer/customer')->load($id)->getData();
//       return $customerData;
//   }
//   public function getLoggedincustomerdata(){
//       return Mage::getSingleton('customer/session')->getCustomer()->getData();
//   }
//    public function editInfoPhotourl() {
//        return Mage::getUrl("tribepledge/photos/editinfo");
//    }
//
//    public function photodeleteUrl() {
//        return Mage::getUrl("tribepledge/photos/delete");
//    }
//    public function loadmorephotoUrl() {
//        return Mage::getUrl("tribepledge/photos/loadmorephotoUrl");
//    }
//     public function decrypt($data) {
//        $encpre = $this->getencprefix();
//        $encsuffix = $this->getencsuffix();
//        $data = str_replace($encsuffix, '', str_replace($encpre, '', $data));
//        return Mage::helper('core')->jsonDecode(base64_decode($data));
//    }
//
//    public function encrypt($data) {
//        $enc_data = base64_encode(Mage::helper('core')->jsonEncode($data));
//        $encpre = $this->getencprefix();
//        $encsuffix = $this->getencsuffix();
//        return $encpre . $enc_data . $encsuffix;
//    }
//     private function getencprefix() {
//        $prefix = "Photos for profiles start";
//        return base64_encode($prefix);
//    }
//
//    private function getencsuffix() {
//        $sufix = "Photos for profiles end";
//        return base64_encode($sufix);
//    }
}
