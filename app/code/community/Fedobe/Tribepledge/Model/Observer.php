<?php

class Fedobe_Tribepledge_Model_Observer {
    public function addButtonPledge($observer){
    $container = $observer->getBlock();
    $attributeSetId = Mage::getModel('eav/entity_attribute_set')
                        ->load('Tribe Pledge', 'attribute_set_name')
                        ->getAttributeSetId();
   // echo $attributeSetId;exit;
    if(null !== $container && $container->getType() == 'tribepledge/adminhtml_pledge') {
        $data_2 = array(
            'label'     => 'Add Pledge',
            'class'=>'add',
            'onclick'   => 'setLocation(\' '  . Mage::helper('adminhtml')->getUrl('adminhtml/tribe_pledge/new/set/'.$attributeSetId, array('_secure' => true)) . '\')',
        );
        $container->addButton('addpledge', $data_2);
    }
    return $this;
}

}
