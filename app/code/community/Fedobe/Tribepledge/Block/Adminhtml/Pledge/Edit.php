<?php

/**
 * Customer attribute edit block
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribepledge
 * @author      Fedobe Magento Team
 */
class Fedobe_Tribepledge_Block_Adminhtml_Pledge_Edit extends Fedobe_Arccore_Block_Adminhtml_Arc_Edit {

    public function __construct() {
        parent::__construct();
        $this->_controller = 'tribe_pledge';
        $this->_blockGroup = 'tribepledge';
    }
}
