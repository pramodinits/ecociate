<?php

/**
 * Manage Customer Attribute grid block
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribepledge
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Tribepledge_Block_Adminhtml_Pledge_Grid extends Fedobe_Arccore_Block_Adminhtml_Arccore_Grid {

    public function __construct() {
        parent::__construct();
        $this->setAttributeSetId(Mage::helper('tribepledge')->getAttributeSetId());
    }

    /**
     * Prepare customer attributes grid collection object
     *
     * @return Fedobe_Tribepledge_Block_Adminhtml_Photo_Grid
     */
    protected function _prepareCollection() {
        $collection = Mage::getModel('arccore/arc')->getCollection()->addAttributeToSelect('*');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('entity_id', array(
            'header' => Mage::helper('tribepledge')->__('ID'),
            'width' => '50px',
            'type' => 'number',
            'index' => 'entity_id',
        ));
        //$finalopt = array('Organic Brand','Store','Organic Farmer','Producer Organization');
        $this->addColumn('trcr_first_name', array(
            'header' => Mage::helper('tribepledge')->__('First Name'),
            'index' => 'trcr_first_name',
            'width' => '100px'
        ));
         $this->addColumn('trcr_last_name', array(
            'header' => Mage::helper('tribepledge')->__('Last Name'),
            'width' => '50px',
            'index' => 'trcr_last_name',
        ));
         $this->addColumn('trpl_email', array(
            'header' => Mage::helper('tribepledge')->__('Email ID'),
            'width' => '50px',
            'index' => 'trpl_email',
        ));
         $this->addColumn('trpl_contact_no', array(
            'header' => Mage::helper('tribepledge')->__('Contact No'),
            'width' => '50px',
            'index' => 'trpl_contact_no',
        ));
         $this->addColumn('trpl_comment', array(
            'header' => Mage::helper('tribepledge')->__('Comment'),
            'width' => '50px',
            'index' => 'trpl_comment',
        ));

        $attributeSetId = Mage::helper('tribepledge')->getAttributeSetId();
        $this->addColumn('action', array(
            'header' => Mage::helper('tribepledge')->__('Action'),
            'width' => '50px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('tribepledge')->__('Edit'),
                    'url' => array(
                        'base' => '*/*/edit/set/' . $attributeSetId,
                        'params' => array('store' => $this->getRequest()->getParam('store'))
                    ),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
        ));


        return parent::_prepareColumns();
    }

}