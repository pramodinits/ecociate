<?php
/**
 * Grid container file
 * 
 * @category    Fedobe
 * @package     Fedobe_Tribepledge
 * @author      Fedobe Magento Team
 * 
 */
class Fedobe_Tribepledge_Block_Adminhtml_Pledge extends Mage_Adminhtml_Block_Widget_Grid_Container
{       
  public function __construct()
  {
    /*both these variables tell magento the location of our Grid.php(grid block) file.
     * $this->_blockGroup.'/' . $this->_controller . '_grid'
     * i.e clarion_Customerattribute/adminhtml_customerattribute_grid
     * $_blockGroup - is your module's name.
     * $_controller - is the path to your grid block. 
     */
    
    $this->_controller = 'adminhtml_pledge';
    $this->_blockGroup = 'tribepledge';
    
    $this->_headerText = Mage::helper('tribepledge')->__('Manage Pledge');
    $this->_addButtonLabel = Mage::helper('tribepledge')->__('Add New Pledge');
    parent::__construct();
    $this->_removeButton('add');
  }
}