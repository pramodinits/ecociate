<?php
class Fedobe_Tribepledge_Block_Pledge extends Mage_Core_Block_Template {
    
    private $_photoscollection;
    private $_profileinfo;
    
     protected function _prepareLayout() {
        parent::_prepareLayout();
        return $this;
    }
     public function getCurrentProfileInfo() {
        return Mage::registry('tribeprofileinfo');
    }
    
    public function getAllPhotos(){
        $info = $this->getCurrentProfileInfo();
        $photoscollection = Mage::helper('tribepledge')->getAllPhotos($info['entity_id']);
        return $photoscollection;
    }
     public function getFormdata(){
        $formdata =array();
         $profile_data = Mage::helper('tribecore/page')->getInfo(); 
           $customer = Mage::helper('tribepledge')->getLoggedincustomerdata();//loggedin customer data
           $profile_data = Mage::helper('tribecore/page')->getInfo();//current profile type regisrty
            $email_option_id = $customer['entity_id'];
            //echo $email_option_id;exit;
            $hidden_value=array();
            $posttype = Mage::helper('arccore')->getattributeoptionsid('trcr_post_type','Register');
            $hidden_value ['trcr_post_type'] = $posttype;
            $hidden_value['trcr_customer_list'] = $email_option_id;
          
            $hidden_value['trcr_profile_list'] = $profile_data['trcr_profile_type'];//'Brand';
            $hidden_value['trcr_username_list'] = $profile_data['entity_id'];//'test';
            $hidden_value['trcr_status'] = 2;
            
            $exclude = array_keys($hidden_value);
            $attribute_set_id = Mage::helper('tribepledge')->getAttributeSetId();
            $attributes = Mage::helper('arccore')->getFormAttributes($attribute_set_id,$exclude);
            $formdata['attributes'] = $attributes;
            $formdata['hidden_attributes'] = $hidden_value;
            return $formdata;
    }
    public function setPhotosCollectionInfo($photoscollection,$info){
        $this->_photoscollection = $photoscollection;
        $this->_profileinfo = $info;
        return $this;
    }
     public function getPhotosCollection(){
        return $this->_photoscollection;
    }
    public function getPhotosProfileInfo(){
        return $this->_profileinfo;
    }
}
?>