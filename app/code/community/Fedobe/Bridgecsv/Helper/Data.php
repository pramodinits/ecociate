<?php

class Fedobe_Bridgecsv_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getUploadDirectory() {
        return Mage::getBaseDir('media') . '/Incomingcsvfiles/';
    }

    public function getImageDirectory() {
        $image_directory = Mage::getBaseDir('media') . '/IncomingCSVimages/';
        if (!file_exists($image_directory)) {
            mkdir($image_directory);
            chmod($image_directory, 0777);
        }
        return $image_directory;
    }

    public function getDownloadDirectory() {
        return Mage::getBaseUrl('media') . 'Incomingcsvfiles/';
    }

    public function getCsvData($id) {
        if ($id) {
            $csvprocess = Mage::getModel('bridgecsv/process');
            $processinfo = $csvprocess->loadByChannelId($id);
            if ($processinfo->getId()) {
                //As the CSV data been processed let's read and format the data for mapping
                $csvdata = array();
                $resource = Mage::getSingleton('core/resource');
                $csvdatatable = $resource->getTableName('bridgecsv/data');
                $readConnection = $resource->getConnection('core_read');
                $sql = $readConnection->select()->from($csvdatatable, array('is_header', 'data'))->where("channel_id=$id");
                $processdata = $readConnection->fetchAll($sql);
                if ($processdata) {
                    foreach ($processdata as $k => $v) {
                        if ($v['is_header'] != '1') {
                            //Here let's unserialize the data and format it
                            $unserdata = unserialize($v['data']);
//                            $unserdata=array_map('utf8_decode',$unserdata);
                            foreach ($unserdata as $kk => $vv) {
                                $kk = $this->clean(utf8_decode($kk));
                                $vv = $this->clean(utf8_decode($vv));
                                if ($vv != '')
                                    $csvdata[$kk][$vv] = $vv;
                            }
                        }
                    }
                }
                return $csvdata;
            } else {
                return 1;
            }
        } else {
            return 0;
        }
    }

    function clean($string) {
        $string = str_replace(' ', '-', strtolower($string));
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
    }

    function getFormattedKey($product_info) {
        $keys = array_map('utf8_decode',array_keys($product_info));
        $keys = array_map(array($this, 'clean'), $keys);
        $product_info = array_combine($keys, array_map('utf8_decode',$product_info));
        return $product_info;
    }

    function getActiveIncomingChannels() {
        $channel_ids = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('channel_type', 'incomingbridgecsv')
                ->addFieldToFilter(array(
                    'product_info', //attribute_1 with key 0
                    'product_pricenquantity', //attribute_2 with key 1
                        ), array(
                    array('eq' => 1), //condition for attribute_1 with key 0
                    array('eq' => 1), //condition for attribute_2
                ))
                ->addFieldToFilter(array(
                    'create_new', //attribute_1 with key 0
                    'update_exist', //attribute_2 with key 1
                        ), array(
                    array('eq' => 1), //condition for attribute_1 with key 0
                    array('eq' => 1), //condition for attribute_2
                        )
                )
                ->addFieldToSelect(array('id'))
                ->getData();
        if ($channel_ids)
            $channel_ids = array_column($channel_ids, 'id');
        return $channel_ids;
    }

    function getPIMChannels() {
        $channel_ids = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('channel_type', 'incomingbridgecsv')
                ->addFieldToFilter('product_info', 1)
                ->addFieldToFilter(array(
                    'create_new', //attribute_1 with key 0
                    'update_exist', //attribute_2 with key 1
                        ), array(
                    array('eq' => 1), //condition for attribute_1 with key 0
                    array('eq' => 1), //condition for attribute_2
                        )
                )
                ->addFieldToSelect(array('id'))
                ->getData();
        if ($channel_ids)
            $channel_ids = array_column($channel_ids, 'id');
        return $channel_ids;
    }

    public function getAttributeOptions($store_id) {
        $defaultstoreId = Mage::app()
                ->getWebsite()
                ->getDefaultGroup()
                ->getDefaultStoreId();
        if (!$store_id)
            $store_id = $defaultstoreId;
        $resource = Mage::getSingleton('core/resource');
        $result = array();
        $readConnection = $resource->getConnection('core_read');
        $label_sql1 = "SELECT eav_attribute.attribute_code attribute_id,optvalues.option_id,optvalues.value FROM
        eav_attribute_option AS options  JOIN eav_attribute ON options.attribute_id=eav_attribute.attribute_id JOIN eav_attribute_option_value  AS optvalues ON options.option_id=optvalues.option_id WHERE store_id=$store_id AND eav_attribute.entity_type_id=4";
        $result = $readConnection->fetchAll($label_sql1);
        return $result;
    }

    public function addProductGalleryImages($mediaArray, $entity_id, $incoming_settings) {
        $product = Mage::getModel('catalog/product')->load($entity_id);
        $sku = $product->getSku();
        $importDir = Mage::helper('bridgecsv')->getImageDirectory();
        $importmediaarray = array();
        if (!file_exists($importDir)) {
            mkdir($importDir, 0777, true);
        }
        $mediaimagetype = array('image', 'small_image', 'thumbnail');
        $flagarr = array();
        $img_count = 0;

        //Here let's copy from remote server to current store
        foreach ($mediaArray as $k => $source) {
            if ($img_count == 5)
                break;

            $k++;
            if ($incoming_settings['include_img_url'] == 2) {
                $source = $importDir . DS . trim($incoming_settings['upload_csv_file'], '.csv') . DS . $source;
            }
            $imagefile = $this->imageExists($importDir, $sku, 'jpg', $k); //echo $imagefile;exit;
            $dest = "$importDir/$imagefile";

            if (file_get_contents($source) && file_put_contents($dest, file_get_contents($source))) {
                $importmediaarray[] = $imagefile;
                $img_count++;
            }
        }

        if (!$importmediaarray)
            return false;
        $image_count = 0;

        foreach ($importmediaarray as $pos => $fileinfo) {
            $filePath = $importDir . '/' . $fileinfo;
            if (file_exists($filePath) && filesize($filePath) / 1048 <= 300) {
                try {
                    if ($image_count == 0)
                        $product->addImageToMediaGallery($filePath, $mediaimagetype, false, false);
                    else
                        $product->addImageToMediaGallery($filePath, array(), false, false);

                    unlink($filePath);
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            } else {
                echo "Product does not have an image or the path is incorrect. Path was: {$filePath}<br/>";
            }
            $image_count++;
        }
        $product->save();
        return true;
    }

    public function imageExists($importDir, $sku, $ext, $k) {
        if (!file_exists("$importDir/$sku" . "_$k.$ext")) {
            return "$sku" . "_$k.$ext";
        } else {
            $k = $k . "_" . $k;
            return $this->imageExists($importDir, $sku, $ext, $k);
        }
    }

    public function getDefaultStoreId() {
        return Mage::app()
                        ->getWebsite()
                        ->getDefaultGroup()
                        ->getDefaultStoreId();
    }
    
    public function currency_conv($price, $currentCurrencyCode) {
        // Base Currency
        $baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
        if($currentCurrencyCode != $baseCurrencyCode){
        // Allowed currencies
        $allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
        $rates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, array_values($allowedCurrencies));
        // the price converted
        $price = $price / $rates[$currentCurrencyCode];
        }
        return $price;
    }
    
    public function getstores(){
        $availablestores = array();
         foreach (Mage::app()->getWebsites() as $website) {
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    $availablestores[$store->getStoreId()] =$store->getData();
                }
            }
        }
        return $availablestores;
    }
    public function addcsvcron($channel_id){
            if($channel_id){
                 if (!file_exists($_SERVER["DOCUMENT_ROOT"] . '/bridgecron')) {
                mkdir($_SERVER["DOCUMENT_ROOT"] . '/bridgecron');
                chmod($_SERVER["DOCUMENT_ROOT"] . '/bridgecron', 0777);
            }
            $crons = shell_exec('crontab -l'); //echo $crons;exit;
            $cronurl = 'wget -o /dev/null -q -O -';
            $endcronurl = '> /dev/null';
            $ogc_link = Mage::getBaseUrl() . "admin/bridgecsv/csvcron?id=".$channel_id;
            $ogc_link_str = '*/2 * * * * '.$cronurl.' '.$ogc_link.' '.$endcronurl;
            str_replace($ogc_link_str . "\n", "", $crons, $ogc_link_count);
            if (!$ogc_link_count)
               $crons .= $ogc_link_str . PHP_EOL;
            file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/bridgecron/crontab.txt', $crons);
            exec("crontab {$_SERVER['DOCUMENT_ROOT']}/bridgecron/crontab.txt");
        }
        
    }
    public function removecron($channel_id) {
        if (!file_exists($_SERVER["DOCUMENT_ROOT"] . '/bridgecron')) {
            mkdir($_SERVER["DOCUMENT_ROOT"] . '/bridgecron');
            chmod($_SERVER["DOCUMENT_ROOT"] . '/bridgecron', 0777);
        }
        if ($channel_id) {
            $cronurl = 'wget -o /dev/null -q -O -';
                $endcronurl = '> /dev/null';
                $product_create_link = Mage::getBaseUrl() . "admin/bridgecsv/csvcron?id=".$channel_id;
                $product_create_str = '*/2 * * * * '.$cronurl.' '.$product_create_link.' '.$endcronurl;
                //echo $product_create_str;exit;
            $crons = shell_exec('crontab -l'); //print_r($crons);exit;
            str_replace($product_create_str . "\n", "", $crons, $count);
            if ($count) {
                $crons = str_replace($product_create_str . "\n", "", $crons, $count);
            }print_r($crons);
            if ($crons != '') {
                file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/bridgecron/crontab.txt', $crons);
                exec("crontab {$_SERVER['DOCUMENT_ROOT']}/bridgecron/crontab.txt");
            }
        }
        return 1;
    }
    public function setCronSchedule() {

        if (!file_exists($_SERVER["DOCUMENT_ROOT"] . '/bridgecron')) {
            mkdir($_SERVER["DOCUMENT_ROOT"] . '/bridgecron');
            chmod($_SERVER["DOCUMENT_ROOT"] . '/bridgecron', 0777);
        }

        $cron_ready_link = Mage::getBaseUrl() . "admin/bridgecsv/cronroute?name=cronstart";
        //exec('crontab -l', $crons);
        $crons = shell_exec('crontab -l');
        //wget -o /dev/null -q -O - "http://kanarygifts.com/index.php/admin/admin/amazon/cronroute?name=productupdate&channel_id=3" > /dev/null
        $cronurl = 'wget -o /dev/null -q -O -';
        $endcronurl = '> /dev/null';

        $cron_ready_str = '*/5 * * * * ' . $cronurl . ' ' . $cron_ready_link . ' ' . $endcronurl;
        
        str_replace($cron_ready_str . "\n", "", $crons, $cronready_count);
        
        if (!$cronready_count) {
            $crons.=$cron_ready_str . PHP_EOL;
        }
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/bridgecron/crontab.txt', $crons);
        exec("crontab {$_SERVER['DOCUMENT_ROOT']}/bridgecron/crontab.txt");
    }
    public function cronready() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");

        $ogctemptable = Mage::getSingleton('core/resource')->getTableName('bridgecsv/csvogcdata');
        $ogctable = Mage::getSingleton('core/resource')->getTableName('bridge/outgoingchannels');
        
        //for updating products
        $sql = "SELECT * FROM (SELECT temp.bridge_channel_id,temp.created_at,temp.updated_at,ogc.others FROM ".$ogctemptable.' as temp JOIN '.$ogctable.' as ogc ON temp.bridge_channel_id=ogc.id WHERE ogc.channel_status=1 AND temp.is_cron_run=0 order by temp.updated_at DESC) AS x GROUP BY x.bridge_channel_id ';//AND ogc.channel_mode=1
        //echo $sql;exit;
        $result = $readConnection->fetchAll($sql);
        foreach($result as $k=>$v){
             $channel_id = $v['bridge_channel_id'];
             $others = unserialize($v['others']);
             $pim_cron = $others['pim_cron_job']; //print_r($pim_cron);exit;
                $priority = array();
                $brandpriority = $this->calculatecrontime($pim_cron, $v['updated_at'], $v1['created_at'],1);
                if ($brandpriority['status'] != -1) {
                    if ($brandpriority['status'] == 1) {
                        echo "csv data cron activated:<br>";
                        $writeConnection->query('UPDATE '.$ogctemptable.' SET is_cron_run=1 WHERE bridge_channel_id='.$channel_id);
                        $this->addcsvcron($channel_id);
                    } else {
                        echo "csv data cron deactivated:<br>";
                    }
                    echo 'channel_id- '.$channel_id . "<br>";
                    print_r($brandpriority);
                    echo "<br>";
                }
        }
        
        //for ftp file cron
        $sql_ftp = "SELECT * FROM (SELECT temp.bridge_channel_id,temp.last_ftp_upload_time,ogc.others,ogc.salt FROM ".$ogctemptable.' as temp JOIN '.$ogctable.' as ogc ON temp.bridge_channel_id=ogc.id WHERE ogc.channel_status=1 AND ogc.channel_type = "bridgecsv" AND ogc.is_ftp_drive_enabled=1 order by temp.last_ftp_upload_time ASC limit 5) AS x GROUP BY x.bridge_channel_id ';//AND ogc.channel_mode=1
        $result_ftp = $readConnection->fetchAll($sql_ftp);
         foreach($result_ftp as $k1=>$v1){
             $channel_id = $v1['bridge_channel_id'];
             $others = unserialize($v1['others']);
             $export_type = $others['export_automatically'];
             $ftp_cron = $others['ftp_frequency']; //print_r($result_ftp);exit;
                $priority = array();
                if((in_array($export_type, array('ftp','drive','email'))) && $ftp_cron!='never'){
                    $ftppriority = $this->calculatecrontime($ftp_cron, $v1['last_ftp_upload_time']);
                    if ($ftppriority['status'] != -1) {
                        if ($ftppriority['status'] == 1) {
                            echo "ftp url run for channel_id:$channel_id<br>";
                            if($export_type=='ftp'){
                                $ftp_url = Mage::getBaseUrl() . "admin/bridgecsv/file_cron?type=ftp&format=".$others['ftp_format']."&salt=".$v1['salt'];
                                $msg = file_get_contents($ftp_url);
                            }else if($export_type=='drive'){
                                $ftp_url = Mage::getBaseUrl() . "admin/bridgecsv/googledrive_upload?channel_id=".$channel_id;
                                //echo $ftp_url;exit;
                                $msg = file_get_contents($ftp_url);
                            }else if($export_type=='email'){
                                $ftp_url = Mage::getBaseUrl() . "admin/bridgecsv/email_send?channel_id=".$channel_id;
                                //echo $ftp_url;exit;
                                $msg = file_get_contents($ftp_url);
                            }
                            echo "url :$ftp_url<br>";
                            str_replace('file uploaded', '', $msg,$cnt);
                            if($cnt)
                            $writeConnection->query('UPDATE '.$ogctemptable.' SET last_ftp_upload_time=NOW() WHERE bridge_channel_id='.$channel_id);
                            sleep(2);
                        }
                        echo $msg.' channel_id- '.$channel_id . "<br>";
                        print_r($ftppriority);
                        echo "<br>";
                    }
                }
        }
        exit;
     }
    function calculatecrontime($cron, $updated_at, $created_at=NULL,$start_im=0) {
        $priority = array('status' => -1);
        // $timezone = date_default_timezone_get();echo $timezone;exit;
        if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
            date_default_timezone_set('Asia/Kolkata');
            //        echo date_default_timezone_get();
            $now = new DateTime();
            $now_time = $now->getTimestamp(); //exit;
        } else {
            $bridgeamazon_table = Mage::getSingleton('core/resource')->getTableName('bridgecsv/csvogcdata');
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $writeConnection = $resource->getConnection("core_write");
            $date_sql = "SELECT NOW() FROM $bridgeamazon_table WHERE 1";
            $result = $readConnection->fetchAll($date_sql);
            $now_time = strtotime($result[0]['NOW()']);
        }
        if ($cron == '0 * * * *') {
            $hr = ($now_time - strtotime($updated_at)) / 3600; //echo $hr;exit;
            if ($hr >= 1) {
                $priority['status'] = 1;
            } else {
                $priority['status'] = 0;
            }
        } elseif ($cron == '0 0 * * *') { // daily
            $hr = ($now_time - strtotime($updated_at)) / 3600;
            if ($hr >= 24 || ($start_im && !IS_NULL($created_at) && ($updated_at==$created_at))) {
                $priority['status'] = 1;
            } else {
                $priority['status'] = 0;
            }
        } elseif ($cron == '0 0 * * 0') { // weekly
            $hr = ($now_time - strtotime($updated_at)) / 3600;
            if ($hr >= 168) {
                $priority['status'] = 1;
            } else {
                $priority['status'] = 0;
            }
        } elseif ($cron == '0 0 1 * *') { // monthly
            $hr = ($now_time - strtotime($updated_at)) / 3600;
            if ($hr >= 720) {
                $priority['status'] = 1;
            } else {
                $priority['status'] = 0;
            }
        }
        return $priority;
    }
    public function sortids($channel_id,$product_ids_str){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        //$sort_product = $readConnection->fetchAll('SELECT product_entity_id FROM `bridge_incomingchannel_product_info` WHERE product_entity_id IN ('.$product_ids_str.') AND is_processed IN ("3","4","-1","-2","1","0") AND channel_id= '.$channel_id.' AND channel_type="outgoing" ORDER BY FIELD(is_processed,"3","4","-1","-2","1","0"),last_updated_time DESC');//echo "<pre>";
        //$id_status_2 = array_column($sort_product,'product_entity_id');
        $table = Mage::getSingleton('core/resource')->getTableName('bridgecsv/csvogcdata');
        $sql_select = $readConnection->fetchAll('SELECT entity_id FROM ' . $table . ' WHERE bridge_channel_id=' . $channel_id);
        $sql_select_id = array_column($sql_select, 'entity_id');
        $id_status = explode(',',$product_ids_str);
        $ids_new = array_diff($id_status, $sql_select_id);
        $entity_id_arr = array();
        if (!empty($ids_new)) {
            foreach ($ids_new as $k => $v) {
                $entity_id_arr[$i]['entity_id'] = $v;
                $entity_id_arr[$i]['bridge_channel_id'] =$channel_id;
                //$entity_id_arr[$i]['ediorder'] = $k;
                $i++;
             }
           }//print_r($entity_id_arr);exit;
         if(!empty($entity_id_arr))
             $writeConnection->insertMultiple($table, $entity_id_arr);
         $ignore_ids = array_diff($sql_select_id, $id_status); //print_r($ignore_ids);exit;
         if (!empty($ignore_ids)) {
           $writeConnection->query('UPDATE ' . $table . ' SET is_ignored=1 WHERE entity_id IN (' . implode(',', $ignore_ids) . ') AND bridge_channel_id=' . $channel_id);
          }
          $common_ids = array();
          $common_ids = array_intersect($sql_select_id, $id_status);
          if (!empty($common_ids)) {
              $writeConnection->query('UPDATE ' . $table . ' SET is_ignored=0 WHERE entity_id IN (' . implode(',', $common_ids) . ') AND bridge_channel_id=' .$channel_id);
          }
        return 1;
    }
    
    public function deleteChannel($id){ //echo $id;exit;
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $table = Mage::getSingleton('core/resource')->getTableName('bridgecsv/csvogcdata');
        $del = 'DELETE FROM '.$table.' WHERE bridge_channel_id='.$id;
        $writeConnection->query($del);
    }
    
}