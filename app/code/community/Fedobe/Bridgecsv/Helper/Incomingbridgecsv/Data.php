<?php
class Fedobe_Bridgecsv_Helper_Incomingbridgecsv_Data extends Mage_Core_Helper_Abstract {

    public function getMapped($product_info, $mapping_info, $channel_info, $ret_options, $product_exist) {
        //cateory mapping
        $categories = array();
        if ($mapping_info['categorymaptype'] == 1) {
            $categories = $mapping_info['allcategoryintomap'];
        } else {
            $category_value = Mage::helper('bridgecsv')->clean($product_info[$mapping_info['csvcategorycolumnname']]);

            if ($mapping_info['csvcategorycolumn'][$category_value]) {
                $categories = $mapping_info['csvcategorycolumn'][$category_value];
            } else if (!in_array($category_value, $mapping_info['ignored']['categories']) && !$channel_info['others']['skip_unmapped']) {
                $this->saveCsvInLog($product_info['sku'], $channel_info['id'], 'UnMapped Category');
                return false;
            }
        }

        //end
        //attribute set mapping
        if ($mapping_info['attributesettype'] == 1) {
            $attrset_id = $mapping_info['storefieldset'];
            $mapped_option = @$mapping_info['option_mapping']['all'] ? $mapping_info['option_mapping']['all'] : array();
            $mapped_Attribute = $mapping_info['attributes']['all'];
            $custom_maping = $mapping_info['custom_maping']['all'];
            $ignore_attributes = $mapping_info['ignored']['attribute']['all'];
            $ignore_options = $mapping_info['ignored']['attribute']['option']['all'];
        } else {
            $attrset_val = Mage::helper('bridgecsv')->clean($product_info[$mapping_info['csvattrsetcol']]);
            if (isset($mapping_info['attributeset'][$attrset_val])) {
                $attrset_id = $mapping_info['attributeset'][$attrset_val];
                $mapped_option = @$mapping_info['option_mapping'][$attrset_val] ? $mapping_info['option_mapping'][$attrset_val] : array();
                $mapped_Attribute = $mapping_info['attributes'][$attrset_val];
                $custom_maping = $mapping_info['custom_maping'][$attrset_val];
                $ignore_attributes = $mapping_info['ignored']['attribute'][$attrset_val];
                $ignore_options = $mapping_info['ignored']['attribute']['option'][$attrset_val];
            } else {
                $this->saveCsvInLog($product_info['sku'], $channel_info['id'], 'UnMapped Attribute Set');
                return false;
            }
        }
        //end
        //if the product array has any unmapped attribute then
        $rem_attribute = array_diff_key($product_info, $mapped_Attribute);
        $rem_attribute = array_diff_key($rem_attribute, array_flip($ignore_attributes));

        if ($rem_attribute && $channel_info['others']['skip_unmapped']) {
            $this->saveCsvInLog($product_info['sku'], $channel_info['id'], 'UnMapped Attribute');
            return false;
        }

        //option mapping
        if (!($product_info = $this->map_option($product_info, $mapped_option, $mapped_Attribute, $ret_options, $channel_info, $mapping_info['global']['option'], $ignore_options)))
            return false;
        //end
        //attribute map  
        $mapped_product = $this->map_attribute($product_info, $mapped_Attribute, $mapping_info['global']['attribute'], $ignore_attributes);

        //end
        //custom mapping     
        $custom_maping = array_diff_key($custom_maping, array_flip($mapped_Attribute));
        $custom_maping = $custom_maping ? $custom_maping : array();


        $mapped_product = array_merge($mapped_product, $custom_maping);

        //end           
        //remove the bridge attribute if any set in the m apped product
        $bridge_attributes = array_flip(Mage::helper('bridge')->getBridgeAttributes());
        $mapped_product = array_diff_key($mapped_product, $bridge_attributes);
        $mapped_product['inc_description'] = $mapped_product['description'];
        $mapped_product['inc_name'] = $mapped_product['name'];
        $mapped_product['inc_short_description'] = $mapped_product['short_description'];


        if ($product_exist) {
            if (strcasecmp($channel_info['content_source'], 'inc_rule_base_original') !== 0) {
                unset($mapped_product['description']);
                unset($mapped_product['name']);
                unset($mapped_product['short_description']);
            }
            unset($mapped_product['status']);
            unset($mapped_product['price']);
        } else {
            if ($channel_info['mode'] == 'demo')
                $mapped_product['status'] = 2;
            else
                $mapped_product['status'] = $channel_info['product_status'] == 3 ? $mapped_product['status'] : $channel_info['product_status'];

            $mapped_product['bridge_pricenqty_channel_id'] = $channel_info['id'];
            $mapped_product['attribute_set_id'] = $attrset_id;
            $mapped_product['type_id'] = Mage_Catalog_Model_Product_Type::TYPE_SIMPLE;
            $supplier_code = Mage::helper('bridge')->getSupplierCode();
            $mapped_product[$supplier_code] = $channel_info['channel_supplier'];
        }

        if ($categories)
            $mapped_product['category_ids'] = $categories;
        return $mapped_product;
    }

    function map_option($array, $option, $mapped_attribute, $ret_options, $channel_info, $global_option, $ignored_options) {
        $mapped_attribute = $mapped_attribute ? $mapped_attribute : array();
        $option = $option ? $option : array();
        $global_option = $global_option ? $global_option : array();
        $ignored_options = $ignored_options ? $ignored_options : array();
        //map the mapped options
        if ($option):
            foreach ($option as $option_key => $option_value) {
                if (isset($array[$option_key]) && isset($option[$option_key][$array[$option_key]])) {
                    $array[$option_key] = implode(',', $option[$option_key][$array[$option_key]]);
                    unset($ret_options[$mapped_attribute[$option_key]]);
                }
            }
        endif;
        //map the global options
        if ($global_option):
            foreach ($global_option as $goption_key => $goption_value) {
                if (isset($array[$goption_key]) && isset($global_option[$goption_key][$array[$goption_key]])) {
                    $array[$goption_key] = implode(',', $global_option[$goption_key][$array[$goption_key]]);
                    unset($ret_options[$mapped_attribute[$goption_key]]);
                }
            }
        endif;
        if (@$ret_options)
            $attribute_intersect = array_intersect_key(array_flip($mapped_attribute), $ret_options);

        if (@$attribute_intersect)
            $attribute_mapped_intersect = array_intersect_key($array, array_flip($attribute_intersect));
        //map the unmapped options for which only attribute is map
        if (@$attribute_mapped_intersect) {
            foreach ($attribute_mapped_intersect as $csv_attr => $option_val) {
                if ($channel_info['create_option']) {
                    //create new option if not exist
                    $array[$csv_attr] = Mage::getmodel('bridge/incomingchannels')->add_new_option($mapped_attribute[$csv_attr], $option_val);
                } else if ($channel_info['others']['skip_unmapped'] && !in_array($option_val, $global_option[$csv_attr])) {
                    $this->saveCsvInLog($array['sku'], $channel_info['id'], 'UnMapped Option');
                    return false;
                }
            }
        }
        return $array;
    }

    function map_attribute($array, $mapped_Attribute, $global_Attribute, $ignored_Attribute) {
        $mapped_Attribute = $mapped_Attribute ? $mapped_Attribute : array();
        $global_Attribute = $global_Attribute ? $global_Attribute : array();
        //mapped attribute for attribute set
        foreach ($mapped_Attribute as $bndattr => $storeattr) {
            if (isset($array[$bndattr])) {
                $array[$storeattr] = $array[$bndattr];
                if ($storeattr != $bndattr)
                    unset($array[$bndattr]);
            }
        }
        //map attribute for global mapping
        $global_Attribute = array_diff_key($global_Attribute, $mapped_Attribute);
        $global_Attribute = array_intersect_key($global_Attribute, $array);
        foreach ($global_Attribute as $bndattr => $storeattr) {
            if (isset($array[$bndattr])) {
                $array[$storeattr] = $array[$bndattr];
                if ($storeattr != $bndattr)
                    unset($array[$bndattr]);
            }
        }
        return $array;
    }

    public function getErrorCodeList() {
        return array(
            'CSVIN100' => "UnMapped Attribute Set",
            'CSVIN600' => "UnMapped Category",
            'CSVIN601' => "UnMapped Attribute",
            'CSVIN602' => "UnMapped Option",
            'CSVIN102' => "Price and Quantity Not Mapped",
            'CSVIN105' => "Product Skipped",
            'CSVIN603' => "Product Updated",
            'CSVIN604' => "New Product Created",
            'CSVIN605' => "Product Image Not Found",
            'CSVIN103' => "Store View Not Mapped",
            'CSVIN104' => "Product Ready To Update",
        );
    }

    public function getStatusMsg() {
        return array(
            'CSVIN100' => "Bridge",
            'CSVIN102' => "Bridge",
            'CSVIN105' => "Bridge",
            'CSVIN103' => "Bridge",
            'CSVIN104' => "Bridge",
            'CSVIN600' => "Magento Store",
            'CSVIN601' => "Magento Store",
            'CSVIN602' => "Magento Store",
            'CSVIN603' => "Magento Store",
            'CSVIN604' => "Magento Store",
            'CSVIN605' => "Magento Store"
        );
    }

    public function getStatusType() {
        return array(
            'CSVIN100' => "Failure",
            'CSVIN600' => "Failure",
            'CSVIN601' => "Failure",
            'CSVIN602' => "Failure",
            'CSVIN102' => "Failure",
            'CSVIN105' => "Failure",
            'CSVIN603' => "Success",
            'CSVIN604' => "Success",
            'CSVIN605' => "Failure",
            'CSVIN103' => "Failure",
            'CSVIN104' => "Success",
        );
    }

    public function saveCsvInLog($sku, $channel_id, $status_type) {
        $logarr['sku'] = $sku;
        $logarr['status_code'] = array_search($status_type, $this->getErrorCodeList());
        $logarr['status'] = $this->getStatusMsg()[$logarr['status_code']];
        $logarr['bridge_channel_id'] = $channel_id;
        $logarr['channel_type'] = 'incoming';
        Mage::getModel('bridge/incomingchannels')->savelog($logarr);
    }

    public function reProcessLogs($bridge_channel_id, $sku_list) {
        if ($sku_list) {
            $resource = Mage::getSingleton('core/resource');
            $write = $resource->getConnection('core_write');
            $csvstable = $resource->getTableName('bridgecsv/data');
            $sku_list = "'" . implode("','", $sku_list) . "'";
            $sql = "UPDATE $csvstable SET `pim_skipped` = 0 WHERE sku IN($sku_list) AND `bridge_channel_id` = $channel_id  AND `pim_skipped`= 1;";
            $write->raw_query($sql);
        }
    }

    public function deleteChannel($channel_id) {
        $resource = Mage::getSingleton('core/resource');
        $write = $resource->getConnection('core_write');
        $csvdata = $resource->getTableName('bridgecsv/data');
        $csvprocess = $resource->getTableName('bridgecsv/process');
        $query1 = "DELETE FROM $csvdata where channel_id=$channel_id;";
        $query2 = "DELETE FROM $csvprocess where channel_id=$channel_id;";
        $write->raw_query($query1);
        $write->raw_query($query2);
    }
  public function deleteInventory($channelId,$sku){ //echo $id;exit;
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection("core_write");
        $table = Mage::getSingleton('core/resource')->getTableName('bridgecsv/csvogcdata');
        $updateQuery = "UPDATE bridgecsv_data SET quantity=0,edi_processed=1,is_deleted=1 WHERE channel_id={$channelId} AND sku='{$sku}';";
        $writeConnection->query($updateQuery);
    }

}
