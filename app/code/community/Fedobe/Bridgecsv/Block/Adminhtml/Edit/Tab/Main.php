<?php

class Fedobe_Bridgecsv_Block_Adminhtml_Edit_Tab_Main extends Fedobe_Bridge_Block_Adminhtml_Outgoingchannels_Edit_Tab_Main {

    protected function _prepareForm() {
        parent::_prepareForm();
        $model = Mage::registry('channel_condition') ? Mage::registry('channel_condition') : Mage::getmodel('bridge/outgoingchannels');
        $data = $model->getData();
        $otherarrdata = unserialize($data['others']);
        if($otherarrdata['refreshtoken']){
            $otherarrdata['drive_verification_status'] = 'verified';
        }else{
            $otherarrdata['drive_verification_status'] = 'not verified';
        }
        foreach ($otherarrdata as $k => $v) {
            $data[$k]= $v;
        }
        
        $form = new Varien_Data_Form();
        $form->setFieldsetRenderer($this->getLayout()->createBlock('bridge/adminhtml_managebridgecontent_form_renderer_fieldset'));
        $fieldset = $form->addFieldset('channel_settings', array(
            'legend' => Mage::helper('bridge')->__('Channel Settings'),
            'fieldset_container_id' => 'channel_settings'
                )
        );
        if ($model->getId())
            $fieldset->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        
        if ($this->getRequest()->getParam('channel_type') || $model->getChannelType()) {
            $fieldset->addField('channel_type', 'hidden', array('name' => 'channel_type', 'value' => $this->getRequest()->getParam('channel_type')));
        }

        $fieldset->addField('general_info', 'label', array(
            'label' => Mage::helper('bridge')->__('General Informations'),
            'title' => Mage::helper('bridge')->__('General Informations'),
        ));

        $fieldset->addField('name', 'text', array(
            'name' => 'channel_name',
            'label' => Mage::helper('bridge')->__('Name'),
            'title' => Mage::helper('bridge')->__('Name'),
            'required' => true,
        ));
        $fieldset->addField('channel_mode', 'select', array(
            'name' => 'channel_mode',
            'label' => Mage::helper('bridge')->__('Mode'),
            'title' => Mage::helper('bridge')->__('Mode'),
            'options' => array(0 => Mage::helper('bridge')->__('Dev'), 1 => Mage::helper('bridge')->__('Live')),
        ));
        $admin = Mage::getModel('admin/user')->load(Mage::getSingleton('admin/session')->getUser()->getId())->getRole()->getData();
        if (!empty($admin)) {
            $fieldset->addField('channel_status', 'select', array(
                'name' => 'channel_status',
                'label' => Mage::helper('bridge')->__('Status'),
                'title' => Mage::helper('bridge')->__('Status'),
                'options' => array(1 => Mage::helper('bridge')->__('Active'), 0 => Mage::helper('bridge')->__('Inactive')),
                'required' => true,
            ));
        }
        $fieldset->addField('channel_info', 'label', array(
            'label' => Mage::helper('bridge')->__('Channel Informations'),
            'title' => Mage::helper('bridge')->__('Channel Informations'),
        ));


        $fieldset->addField('url', 'text', array(
            'name' => 'url',
            'label' => Mage::helper('bridge')->__('IP Address'),
            'title' => Mage::helper('bridge')->__('IP Address'),
            'note' => $this->__('comma separated list of IP Addresses restriction of getting data'),
        ));
        $securitysalt = $fieldset->addField('salt', 'text', array(
            'name' => 'salt',
            'label' => Mage::helper('bridge')->__('Security Salt'),
            'title' => Mage::helper('bridge')->__('Security Salt'),
            'readonly' => true,
            'value' => ($model->getSalt()) ? $model->getSalt() : md5(uniqid())
        ))->setRenderer($this->getLayout()->createBlock('bridgecsv/adminhtml_form_field_keygenerator'));
       // echo Mage::getBaseUrl() . "admin/bridgecsv/csvcron?salt=".$model->getSalt();
       
        
        
        $field_nm_2= $fieldset->addField('view_csv_url', 'label', array(
            'label' => Mage::helper('bridge')->__('Csv View Url'),
            'title' => Mage::helper('bridge')->__('Csv View Url')
        ));
        $field_nm_2->setAfterElementHtml(Mage::getBaseUrl() . "admin/bridgecsv/file_cron?type=view&format=csv&salt=".$model->getSalt());
        
        $field_nm_3= $fieldset->addField('xml_view_url', 'label', array(
            'label' => Mage::helper('bridge')->__('Xml View Url'),
            'title' => Mage::helper('bridge')->__('Xml View Url')
        ));
        $field_nm_3->setAfterElementHtml(Mage::getBaseUrl() . "admin/bridgecsv/file_cron?type=view&format=xml&salt=".$model->getSalt());
        
        $field_nm_4= $fieldset->addField('xls_view_url', 'label', array(
            'label' => Mage::helper('bridge')->__('Xls View Url'),
            'title' => Mage::helper('bridge')->__('Xls View Url')
        ));
        $field_nm_4->setAfterElementHtml(Mage::getBaseUrl() . "admin/bridgecsv/file_cron?type=view&format=xls&salt=".$model->getSalt());
       
        $field_nm= $fieldset->addField('download_csv_url', 'label', array(
            'label' => Mage::helper('bridge')->__('Csv Download Url'),
            'title' => Mage::helper('bridge')->__('Csv Download Url')
        ));
        $field_nm->setAfterElementHtml(Mage::getBaseUrl() . "admin/bridgecsv/file_cron?type=dwnld&format=csv&salt=".$model->getSalt());
        
        
        $field_nm_1= $fieldset->addField('download_xml_url', 'label', array(
            'label' => Mage::helper('bridge')->__('Xml Download Url'),
            'title' => Mage::helper('bridge')->__('Xml Download Url')
        ));
        $field_nm_1->setAfterElementHtml(Mage::getBaseUrl() . "admin/bridgecsv/file_cron?type=dwnld&format=xml&salt=".$model->getSalt());
        
        $field_nm_2= $fieldset->addField('download_xls_url', 'label', array(
            'label' => Mage::helper('bridge')->__('Xls Download Url'),
            'title' => Mage::helper('bridge')->__('Xls Download Url')
        ));
        $field_nm_2->setAfterElementHtml(Mage::getBaseUrl() . "admin/bridgecsv/file_cron?type=dwnld&format=xls&salt=".$model->getSalt());
        
         
         $uploadremote_ogc = $fieldset->addField('export_automatically', 'select', array(
             'name' => 'others[export_automatically]',
            'label' => Mage::helper('bridge')->__('Export Automatically'),
            'title' => Mage::helper('bridge')->__('Export Automatically'),
            'options' => array(0=>'select','ftp'=>'FTP','drive'=>'Google Drive','email'=>'Email Attachment')
        ));
        $ftpusername_ogc = $fieldset->addField('ftp_username_ogc', 'text', array(
            'name' => 'others[ftp_username_ogc]',
            'label' => Mage::helper('bridge')->__('FTP Username'),
            'title' => Mage::helper('bridge')->__('FTP Username'),
            'required' => true,
        ));
        $ftppassword_ogc = $fieldset->addField('ftp_password_ogc', 'password', array(
            'name' => 'others[ftp_password_ogc]',
            'label' => Mage::helper('bridge')->__('FTP Password'),
            'title' => Mage::helper('bridge')->__('FTP Password'),
            'required' => true,
        ));
        $ftpurl_ogc = $fieldset->addField('ftp_url_ogc', 'text', array(
            'name' => 'others[ftp_url_ogc]',
            'label' => Mage::helper('bridge')->__('FTP Url/Ip'),
            'title' => Mage::helper('bridge')->__('FTP Url/Ip'),
            'required' => true,
        ));
        $email_ogc = $fieldset->addField('to_email', 'text', array(
            'name' => 'others[to_email]',
            'label' => Mage::helper('bridge')->__('Send to'),
            'title' => Mage::helper('bridge')->__('Send to'),
            'required' => true,
        ));
        $email_from = $fieldset->addField('from_email', 'text', array(
            'name' => 'others[from_email]',
            'label' => Mage::helper('bridge')->__('Send from'),
            'title' => Mage::helper('bridge')->__('Send from'),
            'required' => true,
        ));
        $email_bcc_label = $fieldset->addField('bcc_email_label', 'label', array(
            'name' => 'others[bcc_email_label]',
            'label' => Mage::helper('bridge')->__('Bcc'),
            'title' => Mage::helper('bridge')->__('Bcc'),
        ));
         $email_bcc =  $email_bcc_label->setAfterElementHtml($this->getLayout()->createBlock('core/template')->setTemplate('fedobe/bridgecsv/bccaddmore.phtml')->toHtml());
        
        $email_body = $fieldset->addField('body_email', 'textarea', array(
            'name' => 'others[body_email]',
            'label' => Mage::helper('bridge')->__('Email Body'),
            'title' => Mage::helper('bridge')->__('Email Body'),
            'required' => true,
        ));
        
//         $ftptestbtn1 = $fieldset->addField('test_ftp', 'button', array(
//            'title' => Mage::helper('bridge')->__('test_ftp'),
//            'value' => "test_ftp",
//            'class' => 'form-button'
//        ));
        
        
        $ftpfilepath_ogc = $fieldset->addField('ftp_file_path_ogc', 'text', array(
            'name' => 'others[ftp_file_path_ogc]',
            'label' => Mage::helper('bridge')->__('FTP File Path'),
            'title' => Mage::helper('bridge')->__('FTP File Path'),
            'required' => true,
        ));
        
        
        $cronjobfrequency = array(
            "0 0 * * *" => 'Daily',
            "0 0 * * 0" => 'Weekly',
            "0 0 1 * *" => 'Monthly',
            'never'=>'Never'
        );
        $ftpfrequency_ogc = $fieldset->addField('ftp_frequency', 'select', array(
            'name' => 'others[ftp_frequency]',
            'label' => Mage::helper('bridge')->__('Cron Frequency'),
            'title' => Mage::helper('bridge')->__('Cron Frequency'),
            'options' => $cronjobfrequency,
        ));
        $ftp_file_format_ogc = $fieldset->addField('ftp_format', 'select', array(
            'name' => 'others[ftp_format]',
            'label' => Mage::helper('bridge')->__('Cron Format'),
            'title' => Mage::helper('bridge')->__('Cron Format'),
            'options' => array('xml'=>'xml','csv'=>'csv','xls'=>'xls'),
        ));
        $drive_ogc = $fieldset->addField('google_drive_verfication', 'hidden', array(
             'name' => 'others[google_drive_verfication]'
        ));
         $drive_refreshtokenogc = $fieldset->addField('refreshtoken', 'hidden', array(
             'name' => 'others[refreshtoken]'
        ));
           $drive_verification = $fieldset->addField('drive_verification_status', 'text', array(
            'name' => 'others[drive_verification_status]',
            'label' => Mage::helper('bridge')->__('Drive Status'),
            'title' => Mage::helper('bridge')->__('Drive Status'),
            'readonly' => true,
        ));
         $drive_ogc_btn=  $drive_verification->setAfterElementHtml('<a class ="form-button" target="_blank" href='.Mage::getBaseUrl() .'admin/bridgecsv/store_refreshtoken?salt='.$model->getSalt().'>Verify Googledrive</a>');
//        $drive_ogc_btn = $fieldset->addField('allow_googledrive_connection', 'button', array(
//            'name' => 'others[allow_googledrive_connection]',
//            'title' => Mage::helper('bridge')->__('Verify'),
//            'value' => "Verify Googledrive",
//            'class' => 'form-button',
//            'onclick' => "return googledriveconnection();"
//        ));
        
       
        $fieldset2 = $form->addFieldset('pim_settings', array(
            'legend' => Mage::helper('bridge')->__('PIM Settings'),
            'fieldset_container_id' => 'pim_settings'
                )
        );
       $fieldset2->addField('pim_sync_settings', 'label', array(
            'label' => Mage::helper('bridge')->__('Sync Settings'),
            'title' => Mage::helper('bridge')->__('Sync Settings'),
        ));
        //$field_nm->setAfterElementHtml(Mage::getBaseUrl() . "admin/bridgecsv/csvcron?id=".$model->getId()."<br>".Mage::getBaseUrl() . "admin/bridgecsv/file_cron?type=view&salt=".$model->getSalt());
        $fieldset2->addField('product_info', 'select', array(
            'name' => 'product_info',
            'label' => Mage::helper('bridge')->__('Sync Product Information'),
            'title' => Mage::helper('bridge')->__('Sync Product Information'),
            'options' => Mage::getModel('adminhtml/system_config_source_yesno')->toArray(),
            'after_element_html' => '<p class="note"><span><span class="notice"><b><u>Note</u>:</b></span>'
            . 'If set to <b>No</b>,then no informations(Related,Cross-Sell,Up-Sell) will be available in CSV</span></p>',
            ));
        $content_source = array('contentrule' => Mage::helper('bridge')->__('Rule Based Content Only'),'copy' => Mage::helper('bridge')->__('Outgoing Content Only'), 'original' => Mage::helper('bridge')->__('Original Content Only'), "contentrule_or_copy" => Mage::helper('bridge')->__('Rule Based else Outgoing Content'), "copy_or_contentrule" => Mage::helper('bridge')->__('Outgoing else Rule Based Content'));
        $fieldset2->addField('edi_product_content_source', 'select', array(
            'name' => 'content_source',
            'label' => Mage::helper('bridge')->__('Select Product Content Source'),
            'title' => Mage::helper('bridge')->__('Select Product Content Source'),
            'options' => $content_source,
        ));
        $fieldset2->addField('pim_image_settings', 'label', array(
            'label' => Mage::helper('bridge')->__('Image Settings'),
            'title' => Mage::helper('bridge')->__('Image Settings'),
        ));
        $fieldset2->addField('include_img_url', 'select', array(
            'name' => 'others[include_img_url]',
            'label' => Mage::helper('bridge')->__('Include Image Url In CSV File'),
            'title' => Mage::helper('bridge')->__('Include Image Url In CSV File'),
            'options' => Mage::getModel('adminhtml/system_config_source_yesno')->toArray(),
        ));
        $remoteftpupload = $fieldset2->addField('upload_images_to_ftp', 'select', array(
            'name' => 'others[upload_images_to_ftp]',
            'label' => Mage::helper('bridge')->__('Upload Images To Remote FTP'),
            'title' => Mage::helper('bridge')->__('Upload Images To Remote FTP'),
            'options' => Mage::getModel('adminhtml/system_config_source_yesno')->toArray(),
        ));
        $ftpusername = $fieldset2->addField('ftp_username', 'text', array(
            'name' => 'others[ftp_username]',
            'label' => Mage::helper('bridge')->__('FTP Username'),
            'title' => Mage::helper('bridge')->__('FTP Username'),
        ));
        $ftppassword = $fieldset2->addField('ftp_password', 'password', array(
            'name' => 'others[ftp_password]',
            'label' => Mage::helper('bridge')->__('FTP Password'),
            'title' => Mage::helper('bridge')->__('FTP Password'),
        ));
        $ftpurl = $fieldset2->addField('ftp_url', 'text', array(
            'name' => 'others[ftp_url]',
            'label' => Mage::helper('bridge')->__('FTP Url'),
            'title' => Mage::helper('bridge')->__('FTP Url'),
        ));
        $ftppath = $fieldset2->addField('path_to_upload_images_ftp', 'text', array(
            'name' => 'others[path_to_upload_images_ftp]',
            'label' => Mage::helper('bridge')->__('Path To Upload Images'),
            'title' => Mage::helper('bridge')->__('Path To Upload Images'),
        ));
        $ftptestbtn = $fieldset2->addField('test_ftp_connection', 'button', array(
            'title' => Mage::helper('bridge')->__('Test Remote Connection'),
            'value' => "Test Remote Connection",
            'class' => 'form-button'
        ));
        
        
        $fieldset2->addField('pim_cron_settings', 'label', array(
            'label' => Mage::helper('bridge')->__('Cron Settings'),
            'title' => Mage::helper('bridge')->__('Cron Settings'),
        ));
        
        $cronjobfilterby = array('condition' => 'Condition', 'sku' => 'SKU');
        $fieldset2->addField('pim_cron_job', 'select', array(
            'name' => 'others[pim_cron_job]',
            'label' => Mage::helper('bridge')->__('Cron Frequency'),
            'title' => Mage::helper('bridge')->__('Cron Frequency'),
            'options' => $cronjobfrequency,
        ));
        $fieldset2->addField('filter_by', 'select', array(
            'name' => 'filter_by',
            'label' => Mage::helper('bridge')->__('Cron Request Filter by'),
            'title' => Mage::helper('bridge')->__('Cron Request Filter by'),
            'options' => $cronjobfilterby,
        ));

        $fieldset3 = $form->addFieldset('edi_settings', array(
            'legend' => Mage::helper('bridge')->__('EDI Settings'),
            'fieldset_container_id' => 'edi_settings'
                )
        );
        /*$fieldset3->addField('edit_price_quantity', 'select', array(
            'name' => 'product_pricenquantity',
            'label' => Mage::helper('bridge')->__('Sync Price and Quantity'),
            'title' => Mage::helper('bridge')->__('Sync Price and Quantity'),
            'options' => Mage::getModel('adminhtml/system_config_source_yesno')->toArray(),
        ));*/
        
        $ediprice = array('ogc_price' => 'Wholesale Price', 'price' => 'Selling Price', 'msrp' => 'MSRP');
        $fieldset3->addField('edi_best_price', 'select', array(
            'name' => 'price_source',
            'label' => Mage::helper('bridge')->__('Base Price'),
            'title' => Mage::helper('bridge')->__('Base Price'),
            'options' => $ediprice,
        ));
        
        $fieldset3->addField('multiprice_rule', 'select', array(
                'name' => 'multiprice_rule',
                'label' => Mage::helper('bridge')->__('Multiple Pricing Rules'),
                'title' => Mage::helper('bridge')->__('Multiple Pricing Rules'),
                'options' => array('max_price' => Mage::helper('bridge')->__('Take Max Price Only'), 'min_price' => Mage::helper('bridge')->__('Take Min Price Only'), 'base_price' => Mage::helper('bridge')->__('Apply Multiple on Base Price'), 'cumulative_price' => Mage::helper('bridge')->__('Apply Multiple on Cumulative Price')),
                'required' => true,
            ));
        $fieldset3->addField('include_deleted_products', 'select', array(
                'name' => 'others[include_deleted_products]',
                'label' => Mage::helper('bridge')->__('Include deleted Products'),
                'title' => Mage::helper('bridge')->__('Include deleted Products'),
                'options' => array('No','Yes')
            ));
        
        /*$fieldset3->addField('edi_cron_settings', 'label', array(
            'label' => Mage::helper('bridge')->__('Cron Settings'),
            'title' => Mage::helper('bridge')->__('Cron Settings'),
        ));
        $fieldset3->addField('edi_cron_job', 'select', array(
            'name' => 'others[edi_cron_job]',
            'label' => Mage::helper('bridge')->__('Cron Frequency'),
            'title' => Mage::helper('bridge')->__('Cron Frequency'),
            'options' => $cronjobfrequency,
        ));*/
        //Here to add our custom javascript via a phtml fiel
        $securitysalt->setAfterElementHtml($this->getLayout()->createBlock('core/template')->setTemplate('fedobe/bridgecsv/csvjs.phtml')->toHtml());
        $data['test_ftp_connection'] = "Test Remote Connection";
        $data['allow_googledrive_connection'] = "Verify Googledrive";
        //unset($data['others']['google_drive_verfication']);
       // echo "<pre>";print_r($data);echo "</pre>";
        if($model->getId())
            $form->setValues($data);
        
        $this->setForm($form);
        $this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
            ->addFieldMap($remoteftpupload->getHtmlId(), $remoteftpupload->getName())
            ->addFieldMap($ftpusername->getHtmlId(), $ftpusername->getName())
            ->addFieldMap($ftppassword->getHtmlId(), $ftppassword->getName())
            ->addFieldMap($ftpurl->getHtmlId(), $ftpurl->getName())
            ->addFieldMap($ftppath->getHtmlId(), $ftppath->getName())
            ->addFieldMap($ftptestbtn->getHtmlId(), $ftptestbtn->getName())
                
            ->addFieldMap($uploadremote_ogc->getHtmlId(), $uploadremote_ogc->getName())
            ->addFieldMap($ftpusername_ogc->getHtmlId(), $ftpusername_ogc->getName())
            ->addFieldMap($ftppassword_ogc->getHtmlId(), $ftppassword_ogc->getName())
            ->addFieldMap($ftpurl_ogc->getHtmlId(), $ftpurl_ogc->getName())
            ->addFieldMap($ftpfilepath_ogc->getHtmlId(), $ftpfilepath_ogc->getName())
            ->addFieldMap($ftp_file_format_ogc->getHtmlId(), $ftp_file_format_ogc->getName())
            ->addFieldMap($ftpfrequency_ogc->getHtmlId(), $ftpfrequency_ogc->getName())
                
            ->addFieldMap($drive_ogc_btn->getHtmlId(), $drive_ogc_btn->getName())
            ->addFieldMap($drive_verification->getHtmlId(), $drive_verification->getName())
            ->addFieldMap($drive_refreshtokenogc->getHtmlId(), $drive_refreshtokenogc->getName())
                
            ->addFieldMap($email_ogc->getHtmlId(), $email_ogc->getName())    
            ->addFieldMap($email_from->getHtmlId(), $email_from->getName())    
            ->addFieldMap($email_body->getHtmlId(), $email_body->getName())    
            ->addFieldMap($email_bcc->getHtmlId(), $email_bcc->getName())    
            ->addFieldMap($email_bcc_label->getHtmlId(), $email_bcc_label->getName())    
            ->addFieldDependence(
                $ftppath->getName(),
                $remoteftpupload->getName(),
                '1'
            )->addFieldDependence(
                $ftpurl->getName(),
                $remoteftpupload->getName(),
                '1'
            )->addFieldDependence(
                $ftppassword->getName(),
                $remoteftpupload->getName(),
                '1'
            )->addFieldDependence(
                $ftpusername->getName(),
                $remoteftpupload->getName(),
                '1'
            )->addFieldDependence(
                $ftptestbtn->getName(),
                $remoteftpupload->getName(),
                '1'
            )->addFieldDependence(
                $ftpfilepath_ogc->getName(),
                $uploadremote_ogc->getName(),
                'ftp'
            )->addFieldDependence(
                $ftpurl_ogc->getName(),
                $uploadremote_ogc->getName(),
                'ftp'
            )->addFieldDependence(
                $ftppassword_ogc->getName(),
                $uploadremote_ogc->getName(),
                'ftp'
            )->addFieldDependence(
                $ftpusername_ogc->getName(),
                $uploadremote_ogc->getName(),
                'ftp'
            )->addFieldDependence(
                $ftp_file_format_ogc->getName(),
                $uploadremote_ogc->getName(),
                array('ftp','drive','email')
            )->addFieldDependence(
                $ftpfrequency_ogc->getName(),
                $uploadremote_ogc->getName(),
                array('ftp','drive','email')
            )->addFieldDependence(
                $drive_ogc_btn->getName(),
                $uploadremote_ogc->getName(),
                'drive'
            )->addFieldDependence(
                $drive_verification->getName(),
                $uploadremote_ogc->getName(),
                'drive'
            )->addFieldDependence(
                $drive_refreshtokenogc->getName(),
                $uploadremote_ogc->getName(),
                'drive'
            )->addFieldDependence(
                $drive_refreshtokenogc->getName(),
                $uploadremote_ogc->getName(),
                'drive'
            )->addFieldDependence(
                $email_ogc->getName(),
                $uploadremote_ogc->getName(),
                'email'
            )->addFieldDependence(
                $email_body->getName(),
                $uploadremote_ogc->getName(),
                'email'
            )->addFieldDependence(
                $email_from->getName(),
                $uploadremote_ogc->getName(),
                'email'
            )->addFieldDependence(
                $email_bcc->getName(),
                $uploadremote_ogc->getName(),
                'email'
            )->addFieldDependence(
                $email_bcc_label->getName(),
                $uploadremote_ogc->getName(),
                'email'
            )
        );
        return $this;
    }

}