<?php

class Fedobe_Bridgecsv_Block_Adminhtml_Edit_Tab_Export extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel() {
        return Mage::helper('bridge')->__('Export');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle() {
        return Mage::helper('bridge')->__('Export');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab() {
        if ($this->getRequest()->getParam('id'))
            return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden() {
        if (!@$this->getRequest()->getParam('id') && !@$this->getRequest()->getParam('channel_type'))
            return true;
    }

    protected function _prepareForm() {
        parent::_prepareForm();
        $channel_condition = @Mage::registry('channel_condition') ? Mage::registry('channel_condition') : Mage::getmodel('bridge/outgoingchannels');
        $form = new Varien_Data_Form();
        $form->setFieldsetRenderer($this->getLayout()->createBlock('bridge/adminhtml_managebridgecontent_form_renderer_fieldset'));
        
        $fieldset = $form->addFieldset('export_setting', array(
            'legend' => Mage::helper('bridge')->__('Export Settings'),
            'fieldset_container_id' => 'export_setting'
                )
        );
                
        $exportformat = array('csv'=>"CSV",'xml'=>"XML");
        $fieldset->addField('export_format', 'select', array(
            'label' => Mage::helper('bridge')->__('Export Data Format'),
            'title' => Mage::helper('bridge')->__('Export Data Format'),
            'options' => $exportformat
        ));
        $fieldset->addField('export_data_btn', 'button', array(
            'title' => Mage::helper('bridge')->__('Download'),
            'value' => "Download",
            'class' => 'form-button',
            'onclick' => 'return downloaddata()'
        ));
        $this->setForm($form);
        return $this;
    }

}