<?php

class Fedobe_Bridgecsv_Block_Adminhtml_Edit_Tab_Incoming_Main extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel() {
        return Mage::helper('bridge')->__('Settings');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle() {
        return Mage::helper('bridge')->__('Settings');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab() {
        if ($this->getRequest()->getParam('id') || @$this->getRequest()->getParam('channel_type'))
            return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden() {
        if (!@$this->getRequest()->getParam('id') && !@$this->getRequest()->getParam('channel_type'))
            return false;
    }

    protected function _prepareForm() {
        $is_admin = Mage::helper('bridge')->isAdmin();
        $model = Mage::registry('incomingchannel_data');
        if (@$this->getRequest()->getParam('channel_type')) {
            $model->setChannelType($this->getRequest()->getParam('channel_type'));
        }
        $data = $model->getData();

        $otherarrdata = unserialize($data['others']);
        foreach ($otherarrdata as $k => $v) {
            $data[$k] = $v;
        }
        $form = new Varien_Data_Form();
        $form->setFieldsetRenderer($this->getLayout()->createBlock('bridge/adminhtml_managebridgecontent_form_renderer_fieldset'));

        $fieldset1 = $form->addFieldset('channel_settings', array(
            'legend' => Mage::helper('bridge')->__('Channel Settings'),
            'fieldset_container_id' => 'channel_settings'
                )
        );
        $fieldset1->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
        $fieldset1->addField('general_informations', 'hbar', array(
            'id' => 'General Informations'
        ));
        if ($model->getId())
            $fieldset1->addField('id', 'hidden', array(
                'name' => 'id',
            ));
        if ($model->getChannelType()) {
            $fieldset1->addField('channel_type', 'hidden', array('name' => 'channel_type', 'value' => $this->getRequest()->getParam('channel_type')));
        }


        $fieldset1->addField('brand_name', 'text', array(
            'name' => 'brand_name',
            'label' => Mage::helper('bridge')->__('Name'),
            'title' => Mage::helper('bridge')->__('Name'),
            'required' => true,
        ));
        //Add supplier dropdown
        $get_supplier_list = Mage::helper('bridge')->getSupplierList();
        $fieldset1->addField('channel_supplier', 'select', array(
            'name' => 'channel_supplier',
            'label' => Mage::helper('bridgecsv')->__('Seller'),
            'title' => Mage::helper('bridgecsv')->__('Seller'),
            'options' => $get_supplier_list,
            'required' => true,
        ));
        //end
        $fieldset1->addField('mode', 'select', array(
            'name' => 'mode',
            'label' => Mage::helper('bridge')->__('Mode'),
            'title' => Mage::helper('bridge')->__('Mode'),
            'required' => true,
            'options' => array('demo' => Mage::helper('bridge')->__('Demo'), 'live' => Mage::helper('bridge')->__('Live')),
        ));
        $fieldset1->addField('status', 'select', array(
            'name' => 'channel_status',
            'label' => Mage::helper('bridge')->__('Status'),
            'title' => Mage::helper('bridge')->__('Status'),
            'options' => array(1 => Mage::helper('bridge')->__('Active'), 0 => Mage::helper('bridge')->__('Inactive')),
            'after_element_html' => $afterstatushtml,
            'required' => true,
        ));

        $fieldset1->addField('csv_source', 'hbar', array(
            'id' => 'CSV Source'
        ));
        $csvsource = array('' => 'Select', '1' => 'Remote CSV File', '3' => 'Local CSV file');
//        $csvsource = array('' => 'Select', '1' => 'Remote CSV File', '2' => 'Remote CSV Folder', '3' => 'Local CSV file');
        $fieldset1->addField('csv_separator', 'text', array(
            'name' => 'others[csv_separator]',
            'label' => Mage::helper('bridgecsv')->__('CSV Separator'),
            'title' => Mage::helper('bridgecsv')->__('CSV Separator')
        ));
        $csvsource = $fieldset1->addField('csv_source_type', 'select', array(
            'name' => 'others[csv_source_type]',
            'label' => Mage::helper('bridge')->__('CSV Source Type'),
            'title' => Mage::helper('bridge')->__('CSV Source Type'),
            'options' => $csvsource,
            'required' => true,
        ));
        $remotecsvfile = $fieldset1->addField('path_to_download_csv_file', 'text', array(
            'name' => 'others[path_to_download_csv_file]',
            'label' => Mage::helper('bridge')->__('Remote CSV File Path'),
            'title' => Mage::helper('bridge')->__('Remote CSV File Path'),
            'required' => true,
        ));
        $fieldset1->addType('customfile', 'Fedobe_Bridgecsv_Block_Adminhtml_Customrenderer_Customfile');
        $localcsvfile = $fieldset1->addField('upload_csv_file', 'customfile', array(
            'name' => 'upload_csv_file',
            'label' => Mage::helper('bridge')->__('Upload CSV File'),
            'title' => Mage::helper('bridge')->__('Upload CSV File'),
            'required' => ($otherarrdata['upload_csv_file'] != '') ? FALSE : TRUE,
            'note' => "(*.csv) ",
        ));
        $remotecsvfolder = $ftpcsvfolderpath = $fieldset1->addField('folder_path_to_download_csv', 'text', array(
            'name' => 'others[folder_path_to_download_csv]',
            'label' => Mage::helper('bridge')->__('Remote Folder Path of CSV'),
            'title' => Mage::helper('bridge')->__('Remote Folder Path of CSV'),
            'required' => true,
        ));

        if ($is_admin) {
            $fieldset1->addField('ftp_credentials', 'hbar', array(
                'id' => 'FTP Credentials'
            ));
            $ftpusername = $fieldset1->addField('ftp_username', 'text', array(
                'name' => 'others[ftp_username]',
                'label' => Mage::helper('bridge')->__('FTP Username'),
                'title' => Mage::helper('bridge')->__('FTP Username'),
            ));
            $ftppassword = $fieldset1->addField('ftp_password', 'password', array(
                'name' => 'others[ftp_password]',
                'label' => Mage::helper('bridge')->__('FTP Password'),
                'title' => Mage::helper('bridge')->__('FTP Password'),
            ));
            $ftpurl = $fieldset1->addField('ftp_url', 'text', array(
                'name' => 'others[ftp_url]',
                'label' => Mage::helper('bridge')->__('FTP Url/Ip'),
                'title' => Mage::helper('bridge')->__('FTP Url/Ip'),
            ));
            $ftptestbtn = $fieldset1->addField('test_ftp_connection', 'button', array(
                'title' => Mage::helper('bridge')->__('Test Remote Connection'),
                'value' => "Test Remote Connection",
                'class' => 'form-button',
                'onclick' => "return testremotecsvftpconnection();"
            ));

            $fieldset2 = $form->addFieldset('pim_settings', array(
                'legend' => Mage::helper('bridge')->__('PIM Settings'),
                'fieldset_container_id' => 'pim_settings'
                    )
            );
            $fieldset2->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');
            $fieldset2->addField('sync_settings', 'hbar', array(
                'id' => 'Sync Settings'
            ));
            $fieldset2->addField('product_info', 'select', array(
                'name' => 'product_info',
                'label' => Mage::helper('bridge')->__('Sync Product Information'),
                'title' => Mage::helper('bridge')->__('Sync Product Information'),
                'options' => array(1 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'required' => true,
            ));

            $fieldset2->addField('csv_image_settings', 'hbar', array(
                'id' => 'CSV Image Settings'
            ));
            $csvimagesource = array('' => 'Select', '1' => "Included In CSV File", '2' => "Remote Folder");
            $csvimagesourcetype = $fieldset2->addField('include_img_url', 'select', array(
                'name' => 'others[include_img_url]',
                'label' => Mage::helper('bridge')->__('CSV Image Source'),
                'title' => Mage::helper('bridge')->__('CSV Image Source'),
                'options' => $csvimagesource,
                'required' => true
            ));

            $ftppath = $fieldset2->addField('path_to_download_images_ftp', 'text', array(
                'name' => 'others[path_to_download_images_ftp]',
                'label' => Mage::helper('bridge')->__('Remote Folder Path for Images'),
                'title' => Mage::helper('bridge')->__('Remote Folder Path for Images'),
                'required' => true,
            ));




            $fieldset2->addField('basic_settings', 'hbar', array(
                'id' => 'Basic Settings'
            ));
            $fieldset2->addField('weightage', 'text', array(
                'name' => 'weightage',
                'label' => Mage::helper('bridge')->__('Weightage'),
                'title' => Mage::helper('bridge')->__('Weightage'),
                'class' => 'validate-number validate-greater-than-zero unique-weightage',
                'required' => true,
            ));
            $fieldset2->addField('content_source', 'select', array(
                'name' => 'content_source',
                'label' => Mage::helper('bridge')->__('Product Content Destination'),
                'title' => Mage::helper('bridge')->__('Product Content Destination'),
//            'options' => array('original' => Mage::helper('bridge')->__('Replace With original content'), 'copy' => Mage::helper('bridge')->__('Keep Separate Copy')),
                'options' => array('copy' => Mage::helper('bridge')->__('Keep Separate Copy'), 'inc_rule_base_original' => Mage::helper('bridge')->__('Replace Original by Rule Based else by Incoming Content'), 'rule_base_original' => Mage::helper('bridge')->__('Replace Original by Rule Based Content')),
                'required' => true,
            ));
            $fieldset2->addField('product_status', 'select', array(
                'name' => 'product_status',
                'label' => Mage::helper('bridge')->__('Default Status Of Listed Product'),
                'title' => Mage::helper('bridge')->__('Default Status Of Listed Product'),
                'options' => array(1 => Mage::helper('bridge')->__('Enabled'), 2 => Mage::helper('bridge')->__('Disabled'), 3 => Mage::helper('bridge')->__('As it in Source')),
                'required' => true
            ));
            $fieldset2->addField('create_new', 'select', array(
                'name' => 'create_new',
                'label' => Mage::helper('bridge')->__('Add New Product Automatically'),
                'title' => Mage::helper('bridge')->__('Add New Product Automatically'),
                'options' => array(1 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'required' => true,
            ));
            $fieldset2->addField('update_exist', 'select', array(
                'name' => 'update_exist',
                'label' => Mage::helper('bridge')->__('Update Existing Product Automatically'),
                'title' => Mage::helper('bridge')->__('Update Existing Product Automatically'),
                'options' => array(1 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'required' => true,
            ));
            $fieldset2->addField('create_option', 'select', array(
                'name' => 'create_option',
                'label' => Mage::helper('bridge')->__('Create New Attribute Options Automatically'),
                'title' => Mage::helper('bridge')->__('Create New Attribute Options Automatically'),
                'options' => array(1 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'required' => true,
            ));
            $fieldset2->addField('skip_unmapped', 'select', array(
                'name' => 'others[skip_unmapped]',
                'label' => Mage::helper('bridge')->__('Skip when Unmapped Data is found'),
                'title' => Mage::helper('bridge')->__('Skip when Unmapped Data is found'),
                'options' => array(1 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'required' => true,
            ));
            $fieldset2->addField('pim_cron', 'select', array(
                'name' => 'pim_cron',
                'label' => Mage::helper('bridge')->__('Cron Frequency'),
                'title' => Mage::helper('bridge')->__('Cron Frequency'),
                'options' => array('never' => Mage::helper('bridge')->__('Never'), '0 0 * * *' => Mage::helper('bridge')->__('Daily'), '0 0 * * 0' => Mage::helper('bridge')->__('Weekly'), '0 0 1 * *' => Mage::helper('bridge')->__('Monthly')),
                'required' => true,
            ));


            $fieldset3 = $form->addFieldset('edi_settings', array(
                'legend' => Mage::helper('bridge')->__('EDI Settings'),
                'fieldset_container_id' => 'edi_settings'
                    )
            );
            $fieldset3->addType('hbar', 'Fedobe_Bridge_Block_Adminhtml_Mainrenderer_Hbar');

            $fieldset3->addField('sync_settings_', 'hbar', array(
                'id' => 'Sync Settings'
            ));
            $fieldset3->addField('product_pricenquantity', 'select', array(
                'name' => 'product_pricenquantity',
                'label' => Mage::helper('bridge')->__('Sync Prices & Quantity'),
                'title' => Mage::helper('bridge')->__('Sync Prices & Quantity'),
                'options' => array(1 => Mage::helper('bridge')->__('Yes'), 0 => Mage::helper('bridge')->__('No')),
                'required' => true,
            ));

            $fieldset3->addField('basic_settins_', 'hbar', array(
                'id' => 'Basic Settings'
            ));
            $multiplepricerule = $fieldset3->addField('multi_price_rule', 'select', array(
                'name' => 'multi_price_rule',
                'label' => Mage::helper('bridge')->__('Multiple Pricing Rules'),
                'title' => Mage::helper('bridge')->__('Multiple Pricing Rules'),
                'options' => array('max' => Mage::helper('bridge')->__('Take Max Price Only')
                    , 'min' => Mage::helper('bridge')->__('Take Min Price Only')
                    , 'sum_on_base' => Mage::helper('bridge')->__('Apply Multiple on Base Price')
                    , 'sum_on_cumulative' => Mage::helper('bridge')->__('Apply Multiple on Cumulative Price')
                )
            ));
            $fieldset3->addField('slab', 'text', array(
                'name' => 'slab',
                'label' => Mage::helper('bridge')->__('Slab'),
                'title' => Mage::helper('bridge')->__('Slab'),
                'required' => true,
            ));
            $fieldset3->addField('channel_order', 'text', array(
                'name' => 'channel_order',
                'label' => Mage::helper('bridge')->__('Order'),
                'title' => Mage::helper('bridge')->__('Order'),
                'required' => true,
            ));
            $fieldset3->addField('miss_sku_action', 'select', array(
                'name' => 'others[miss_sku_action]',
                'label' => Mage::helper('bridge')->__('Missing SKU while updating'),
                'options' => array('out_of_stock' => Mage::helper('bridge')->__('Treat as Out of Stock'), 'no_change' => Mage::helper('bridge')->__('Treat as Not Changed'),'deleted' => Mage::helper('bridge')->__('Treat as Deleted')),
                'required' => true,
            ));
            $field_nm = $fieldset3->addField('edi_cron', 'select', array(
                'name' => 'edi_cron',
                'label' => Mage::helper('bridge')->__('Cron Frequency'),
                'title' => Mage::helper('bridge')->__('Cron Frequency'),
                'options' => array('never' => Mage::helper('bridge')->__('Never'), '0 * * * *' => Mage::helper('bridge')->__('Hourly'), '0 0 * * *' => Mage::helper('bridge')->__('Daily'), '0 0 * * 0' => Mage::helper('bridge')->__('Weekly'), '0 0 1 * *' => Mage::helper('bridge')->__('Monthly')),
                'required' => true,
            ));

            $unique_wightage_url = $this->getUrl('adminhtml/incomingchannels/weightageunique_validation') . "?isAjax=true";
            if (!$model->getWeightage()) {
                $model->setWeightage(10);
            }
        }
        $form_key = Mage::getSingleton('core/session')->getFormKey();
        $form->setValues($data);
        if ($is_admin)
            $ftptestbtn->setValue('Test Remote Connection');
        $this->setForm($form);
        if ($is_admin) {
            $ftpurl->setAfterElementHtml($this->getLayout()->createBlock('core/template')->setTemplate('fedobe/bridgecsv/csvjs.phtml')->toHtml());
            $this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
                            ->addFieldMap($csvsource->getHtmlId(), $csvsource->getName())
                            ->addFieldMap($remotecsvfile->getHtmlId(), $remotecsvfile->getName())
                            ->addFieldMap($localcsvfile->getHtmlId(), $localcsvfile->getName())
                            ->addFieldMap($remotecsvfolder->getHtmlId(), $remotecsvfolder->getName())
                            ->addFieldMap($csvimagesourcetype->getHtmlId(), $csvimagesourcetype->getName())
                            ->addFieldMap($ftppath->getHtmlId(), $ftppath->getName())
                            ->addFieldDependence(
                                    $remotecsvfolder->getName(), $csvsource->getName(), '2'
                            )->addFieldDependence(
                            $localcsvfile->getName(), $csvsource->getName(), '3'
                    )->addFieldDependence(
                            $remotecsvfile->getName(), $csvsource->getName(), '1'
                    )->addFieldDependence(
                            $ftppath->getName(), $csvimagesourcetype->getName(), '2'
                    )
            );
        } else {
            $this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
                            ->addFieldMap($csvsource->getHtmlId(), $csvsource->getName())
                            ->addFieldMap($remotecsvfile->getHtmlId(), $remotecsvfile->getName())
                            ->addFieldMap($localcsvfile->getHtmlId(), $localcsvfile->getName())
                            ->addFieldMap($remotecsvfolder->getHtmlId(), $remotecsvfolder->getName())
                            ->addFieldDependence(
                                    $remotecsvfolder->getName(), $csvsource->getName(), '2'
                            )->addFieldDependence(
                            $localcsvfile->getName(), $csvsource->getName(), '3'
                    )->addFieldDependence(
                            $remotecsvfile->getName(), $csvsource->getName(), '1'
                    )
            );
        }
        Mage::dispatchEvent('fedobe_bridge_adminhtml_incomingchannels_edit_tab_main_prepare_form', array('form' => $form));

        return parent::_prepareForm();
    }

}