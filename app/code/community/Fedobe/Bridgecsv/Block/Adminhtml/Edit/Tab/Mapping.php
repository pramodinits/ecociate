<?php

class Fedobe_Bridgecsv_Block_Adminhtml_Edit_Tab_Mapping extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    /**
     * Prepare content for tab
     *
     * @return string
     */
    public function getTabLabel() {
        return Mage::helper('bridge')->__('Mapping');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle() {
        return Mage::helper('bridge')->__('Mapping');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab() {
        if ($this->getRequest()->getParam('id'))
            return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden() {
        if (!@$this->getRequest()->getParam('id') && !@$this->getRequest()->getParam('channel_type'))
            return true;
    }

    protected function _prepareForm() {
        parent::_prepareForm();
        $channel_condition = @Mage::registry('channel_condition') ? Mage::registry('channel_condition') : Mage::getmodel('bridge/outgoingchannels');
        $data = $channel_condition->getData();//echo "<pre>"; print_r($data);echo "</pre>";
        $otherarrdata = unserialize($data['others']);
//        echo "<pre>";print_r($otherarrdata);echo "</pre>";
        foreach ($otherarrdata as $k => $v) {
            $data[$k] = $v;
        }
        $form = new Varien_Data_Form();
        $form->setFieldsetRenderer($this->getLayout()->createBlock('bridge/adminhtml_managebridgecontent_form_renderer_fieldset'));
        
        
        $fieldset = $form->addFieldset('currency_setting', array(
            'legend' => Mage::helper('bridge')->__('Currency Mapping'),
            'fieldset_container_id' => 'currency_setting'
                )
        );
        
        $current_currency = Mage::app()->getStore()->getBaseCurrencyCode();
        $currencyModel = Mage::getModel('directory/currency');
        $Allcurrencies = $currencyModel->getConfigAllowCurrencies();
        $Allcurrencies['custom'] = "Custom";
        $Allcurrencies = array_combine(array_values($Allcurrencies),array_values($Allcurrencies));
        $fieldset->addField('noted', 'note', array(
            'label' => Mage::helper('bridge')->__('Current Store Base Currency'),
            'title' => Mage::helper('bridge')->__('Current Store Base Currency'),
            'text' => Mage::helper('bridgecsv')->__("<b>$current_currency</b>")
        ));
        $fieldset->addField('currency', 'select', array(
            'name' => 'others[currency]',
            'label' => Mage::helper('bridge')->__('Select Outgoing Currency'),
            'title' => Mage::helper('bridge')->__('Select Outgoing Currency'),
            'options' => $Allcurrencies,
            'onchange' => 'customcurrency(this);'
        ));
        
        
        
        
        $fieldset = $form->addFieldset('store_setting', array(
            'legend' => Mage::helper('bridge')->__('Store View Informations'),
            'fieldset_container_id' => 'store_setting'
                )
        );
        $fieldset->addType('store_field', 'Fedobe_Bridgecsv_Block_Adminhtml_Form_Edit_Tab_Field_Store');
       $availablestores = Mage::helper('bridgecsv')->getstores();
              //  echo "<pre>";
       $availablestores_id = array_combine(array_keys($availablestores),array_column($availablestores,'code'));
       $i=0;
       foreach($availablestores as $k=>$v){
           $arr[$i]['value']=$v['store_id'];
           $arr[$i]['label']=$v['code'];
                   $i++;
       }
//       print_r($arr);echo "</pre>";
        $fieldset->addField('note', 'note', array(
            'text' => Mage::helper('bridgecsv')->__('<h5>Choose your desired store view to have store specific product informations</h5>'),
        ));
//        $fieldset->addField('store_field', 'store_field', array(
//            'stores' => $availablestores,
//            'default_store' => Mage::app()->getWebsite(true)->getDefaultGroup()->getDefaultStoreId(),
//            'value' => $channel_condition->getStoreField()
//        ));
        $fieldset->addField('store_field', 'checkboxes', array('label' => 'Store View', 'name' => 'others[store][]',
    'values' => $arr,
    'checked' => explode(',',$otherarrdata['store'])
));
        if ($this->getRequest()->getParam('channel_type')) {
            $fieldset->addField('channel_type', 'hidden', array('name' => 'channel_type', 'value' => $this->getRequest()->getParam('channel_type')));
        }
        $fieldset2 = $form->addFieldset('category_settings', array(
            'legend' => Mage::helper('bridge')->__('Category Informations'),
            'fieldset_container_id' => 'category_settings'
                )
        );
        $fieldset2->addField('slno_in_csv', 'select', array(
            'name' => 'others[slno_in_csv]',
            'label' => Mage::helper('bridge')->__('Include Serial Number in CSV'),
            'title' => Mage::helper('bridge')->__('Include Serial Number in CSV'),
            'options' => Mage::getModel('adminhtml/system_config_source_yesno')->toArray(),
            'value' => ($channel_condition->getSlnoInCsv()) ? $channel_condition->getSlnoInCsv() : 1
        ));
        $fieldset2->addField('root_cat_in_csv', 'select', array(
            'name' => 'others[root_cat_in_csv]',
            'label' => Mage::helper('bridge')->__('Include Category in CSV'),
            'title' => Mage::helper('bridge')->__('Include Category in CSV'),
            'options' => Mage::getModel('adminhtml/system_config_source_yesno')->toArray(),
            'value' => ($channel_condition->getRootCatInCsv()) ? $channel_condition->getRootCatInCsv() : 1
        ));
        $fieldset2->addField('attribute_set_in_csv', 'select', array(
            'name' => 'others[attribute_set_in_csv]',
            'label' => Mage::helper('bridge')->__('Include Attribute Set in CSV'),
            'title' => Mage::helper('bridge')->__('Include Attribute Set in CSV'),
            'options' => Mage::getModel('adminhtml/system_config_source_yesno')->toArray(),
            'value' => ($channel_condition->getRootCatInCsv()) ? $channel_condition->getRootCatInCsv() : 1
        ));
//        $fieldset2->addField('sub_cat_in_csv', 'select', array(
//            'name' => 'others[sub_cat_in_csv]',
//            'label' => Mage::helper('bridge')->__('Include Sub Category in CSV'),
//            'title' => Mage::helper('bridge')->__('Include Sub Category in CSV'),
//            'options' => Mage::getModel('adminhtml/system_config_source_yesno')->toArray(),
//            'value' => ($channel_condition->getSubCatInCsv()) ? $channel_condition->getSubCatInCsv() : 1
//        ));
        
        
        $fieldset3 = $form->addFieldset('attribute_settings', array(
            'legend' => Mage::helper('bridge')->__('Attribute Informations'),
            'fieldset_container_id' => 'attribute_settings'
                )
        );
        $fieldset3->addField('attribute_note', 'note', array(
            'text' => Mage::helper('bridgecsv')->__('<h5>By Default <label class="highlight"><b>NAME,SKU,PRICE</b></label> included in CSV.Here you can add more attributes as per your requirement.</h5>'),
        ));
        $fieldset3->addType('attribute_field', 'Fedobe_Bridgecsv_Block_Adminhtml_Form_Edit_Tab_Field_Attributes');
        $fieldset3->addField('attribute_field', 'attribute_field', array(
            'stores' => $availablestores,
            'default_store' => Mage::app()->getWebsite(true)->getDefaultGroup()->getDefaultStoreId(),
            'value' => $channel_condition->getStoreField()
        ));
        if($channel_condition->getId())
          $form->setValues($data);
        $this->setForm($form);
        return $this;
    }

}
