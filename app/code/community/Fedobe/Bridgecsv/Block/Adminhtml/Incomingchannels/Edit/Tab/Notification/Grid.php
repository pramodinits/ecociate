<?php

//require_once(Mage::getModuleDir('Block', 'Fedobe_Bridge') . DS . 'Block' . DS . 'Adminhtml' . DS . 'Incomingchannels' . DS . 'Edit' . DS . 'Tab' . DS . 'Notification' . DS . 'Grid.php');

class Fedobe_Bridgecsv_Block_Adminhtml_incomingchannels_Edit_Tab_Notification_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    /**
     * Constructor
     */
    private $_channeltype;

    public function __construct() {
        parent::__construct();
        $this->setId('importProductListGrid');
        $this->setUseAjax(true);
        $this->setVarNameFilter('importcondproduct_filter');
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('bridge/incomingchannels');
        $model->load($id);
        $channel_type = $model->getChannelType();
        $this->_channeltype = $channel_type;
    }

    /**
     * Prepare collection for grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection() {
        $id = $this->getRequest()->getParam('id');
        $collection = Mage::getModel('bridge/incomingchannels_incominglog')->getCollection()->addFieldToFilter('bridge_channel_id', $id);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Define grid columns
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns() {
        $attributeCodeConfig = "bridge_channel_id";
        $bridgecontentedited = "bridge_content_edited";
        $this->addColumn('bridge_channel_id', array(
            'header' => Mage::helper('bridgecsv')->__('bridge_channel_id'),
            'index' => 'bridge_channel_id',
            'align' => 'right',
            'width' => '100px',
        ));


        $this->addColumn('sku', array(
            'header' => Mage::helper('bridgecsv')->__('SKU'),
            'index' => 'sku',
            'width' => '200px',
            'align' => 'right',
        ));
        $status_code = Mage::helper('status')->getChannelStatusCodeWithType('incomingbridgecsv');
        if (!$this->_isExport) {
            $this->addColumn('status_code', array(
                'header' => Mage::helper('bridgecsv')->__('Status Code'),
                'index' => 'status_code',
                'width' => '200px',
                'type' => 'options',
                'options' => $status_code,
                'filter_condition_callback' => array($this, '_codeFilter'),
                'align' => 'right',
                'renderer' => 'Fedobe_Bridge_Block_Adminhtml_Render_Skustatus'//<a href="javascript:void(0)">i</a>
            ));
        } else {
            $this->addColumn('status_code', array(
                'header' => Mage::helper('bridgecsv')->__('Status Code'),
                'index' => 'status_code',
                'width' => '200px',
                'type' => 'options',
                'options' => $status_code,
                'filter_condition_callback' => array($this, '_codeFilter'),
                'align' => 'right'
            ));
            $this->addColumn('status_message', array(
                'header' => Mage::helper('bridgecsv')->__('Status Message'),
                'index' => 'status_message',
                'width' => '200px',
                'align' => 'right',
                'renderer' => 'Fedobe_Bridge_Block_Adminhtml_Render_Status_Message'
            ));
        }

        $this->addColumn('status_type', array(
            'header' => Mage::helper('bridgecsv')->__('Status'),
            'index' => 'status_type',
            'type' => 'options',
            'width' => '200px',
            'options' => array('Success' => 'Success', 'Failure' => 'Failure'),
            'filter_condition_callback' => array($this, '_statusTypeFilter'),
            'renderer' => 'Fedobe_Bridge_Block_Adminhtml_Render_Statustype'
        ));
        $this->addColumn('updated_at', array(
            'header' => Mage::helper('bridgecsv')->__('Last Updated'),
            'align' => 'left',
            'width' => '200px',
            'type' => 'date',
            'default' => '--',
            'index' => 'updated_at',
            'renderer' => 'bridge/adminhtml_widget_grid_column_renderer_editime'
        ));

        if (!$this->_isExport) {
            $this->addColumn('action', array(
                'header' => $this->helper('bridgecsv')->__('Action'),
                'width' => '100px',
                'sortable' => false,
                'filter' => false,
                'type' => 'action',
                'width' => '450px',
                'renderer' => 'Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Log_Managebridgecontent',
                    )
            );
        }
        $this->addExportType('*/*/exportLogCsv', Mage::helper('bridge')->__('CSV'));
        $this->addExportType('*/*/exportLogExcel', Mage::helper('bridge')->__('Excel XML'));
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('product');
        $this->getMassactionBlock()->addItem('removelogs', array(
            'label' => Mage::helper('catalog')->__('Delete Logs'),
            'url' => $this->getUrl('*/incomingchannels/masslog', array('_current' => true)),
        ));
        $this->getMassactionBlock()->addItem('reprocesslogs', array(
            'label' => Mage::helper('catalog')->__('Reprocess All Skiped Data'),
            'url' => $this->getUrl('*/incomingchannels/massreprocesslog', array('_current' => true)),
        ));
        return $this;
    }

    /**
     * Get grid url
     *
     * @return string
     */
    public function getGridUrl() {
        return $this->getUrl('*/incomingbridgecsv/gridnotification', array('_current' => true));
    }

    protected function _codeFilter($collection, $column) {

        $filterprocess = $column->getFilter()->getValue();
        if ($filterprocess == '') {
            return $this;
        }
        $helperobj = Mage::helper($this->_channeltype);
        $status_msg = $helperobj->getStatusMsg();
        if (in_array($filterprocess, array_values($status_msg))) {
            $status_codes = array_keys($status_msg, $filterprocess);
            $this->getCollection()->addFieldToFilter('main_table.status_code', array('in' => $status_codes));
        } else {
            $this->getCollection()->addFieldToFilter('main_table.status_code', $filterprocess);
        }
        return;
    }

    protected function _statusTypeFilter($collection, $column) {

        $filterprocess = $column->getFilter()->getValue();
        if ($filterprocess == '') {
            return $this;
        }
        $helperobj = Mage::helper($this->_channeltype);
        $status_type = $helperobj->getStatusType();

        if (in_array($filterprocess, array_values($status_type))) {
            $status_codes = array_keys($status_type, $filterprocess);
            $this->getCollection()->addFieldToFilter('main_table.status_code', array('in' => $status_codes));
        }
        return;
    }

    public function getRowUrl($row) {
        return "javascript:void(0);";
    }

}

