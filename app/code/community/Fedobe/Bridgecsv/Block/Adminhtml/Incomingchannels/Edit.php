<?php

require_once(Mage::getModuleDir('Block', 'Fedobe_Bridge') . DS . 'Block' . DS . 'Adminhtml' . DS . 'Incomingchannels' . DS . 'Edit.php');

class Fedobe_Bridgecsv_Block_Adminhtml_Incomingchannels_Edit extends Fedobe_Bridge_Block_Adminhtml_Incomingchannels_Edit {

    public function __construct() {
        parent::__construct();
        if ($this->getRequest()->getParam('id') && @Mage::registry('incomingchannel_data')->getMappingData()) {
            $is_admin = Mage::helper('bridge')->isAdmin();
            if ($is_admin) {
                $this->_addButton('update_primary_field', array(
                    'label' => Mage::helper('adminhtml')->__('Resync'),
                    'onclick' => 'setLocation(\'' . $this->getUrl('adminhtml/incomingbridgecsv/update_csv_data', array($this->_objectId => $this->getRequest()->getParam($this->_objectId), 'process' => 'all')) . '\')'
                        ), -102);
            }
            $this->_addButton('update_primary_field_for_new_upload', array(
                'label' => Mage::helper('adminhtml')->__('Map the primary field'),
                'onclick' => 'setLocation(\'' . $this->getUrl('adminhtml/incomingbridgecsv/update_csv_data', array($this->_objectId => $this->getRequest()->getParam($this->_objectId))) . '\')'
                    ), -103);
        }
    }

}