<?php
class Fedobe_Bridgecsv_Block_Adminhtml_Form_Edit_Tab_Field_Store extends Varien_Data_Form_Element_Abstract{
    public function __construct($attributes=array())
    {
        parent::__construct($attributes);
    }
    public function getElementHtml()
    {
        $channel_condition = @Mage::registry('channel_condition') ? Mage::registry('channel_condition') : Mage::getmodel('bridge/outgoingchannels');
        $othersdata = $channel_condition->getData('others');
        $exportsetting = explode(',',unserialize($othersdata)['store']);
        $value = $this->getValue();
        $html = "<div id='{$this->getHtmlId()}' {$this->serialize($this->getHtmlAttributes())}>";
        $allstores = $this->getStores();
        $defaultstoreid = $this->getDefaultStore();
        foreach ($allstores as $store_id => $store) {
            $readonly = $checked = $default = "";
            if($defaultstoreid == $store_id){
                $readonly = "disabled='disabled'";
                $checked = "checked='checked'";
                $default = "(<label class='highlight'>Default Store</label>)";
            }
            if(in_array($store_id, $exportsetting))
                    $checked = "checked='checked'";
            $html .="&nbsp;<input type='checkbox' name='others[store][]' id='{$store['code']}' $readonly $checked />&nbsp;<span class='{$store['code']} bridge-custom-format'><label for='{$store['code']}'>{$store['name']} $default</label>&nbsp;</span>";
        }
        $html.= "<input type='hidden' name='others[store][]' value='$defaultstoreid' />";
        $html .= "</div>".$this->getAfterElementHtml();
        return $html;
    }
}