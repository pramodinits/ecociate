<?php

class Fedobe_Bridgecsv_Block_Adminhtml_Form_Edit_Tab_Field_Attributes extends Varien_Data_Form_Element_Abstract {

    public function __construct($attributes = array()) {
        parent::__construct($attributes);
    }

    public function getElementHtml() {
        $channel_condition = @Mage::registry('channel_condition') ? Mage::registry('channel_condition') : Mage::getmodel('bridge/outgoingchannels');
        $conditions_serialized = $channel_condition->getData('conditions_serialized');
        $othersdata = $channel_condition->getData('others');
        $exportsetting = unserialize($othersdata);
        $arrtfields = '';
        $arrtfields_store = '';
        $arrtfields_ar = array();
        if(isset($exportsetting['csvfields'])){
            $arrtfields .= $exportsetting['csvfields'];
            $arrtfields_ar = explode(',',$arrtfields);
            if(!in_array('name',$arrtfields_ar))
                $arrtfields_ar = array_merge(array('name'),$arrtfields_ar);
            if(!in_array('price',$arrtfields_ar))
                $arrtfields_ar = array_merge(array('price'),$arrtfields_ar);
            if(!in_array('sku',$arrtfields_ar))
                $arrtfields_ar = array_merge(array('sku'),$arrtfields_ar);
            if(!in_array('quantity',$arrtfields_ar))
                $arrtfields_ar = array_merge(array('quantity'),$arrtfields_ar);
            if(!in_array('last_updated_time',$arrtfields_ar))
                $arrtfields_ar = array_merge(array('last_updated_time'),$arrtfields_ar);
             $arrtfields = implode(',',$arrtfields_ar);
            
        }else{
            $arrtfields = 'name,sku,price,quantity,last_updated_time';
        }
        if(isset($exportsetting['store_fields'])){
            $arrtfields_store=$exportsetting['store_fields'];
        }
        //print_r($exportsetting);
        $ids = Mage::getmodel('bridge/rule')->getMatchingProductIds($conditions_serialized);
        $attrset = Mage::getModel('bridge/outgoingchannels')->getAttributeSetIds($ids);
        $groupnames = array('common' => "Common");
        //Here to get names of attribute set
        $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
        foreach ($attrset as $k => $attrset_id) {
            $attributeSetModel->load($attrset_id);
            $groupnames[$attrset_id] = $attributeSetModel->getAttributeSetName();
        }
        $attributes = $this->getFormattedAttributes($attrset);
        $selectablehtml = $this->getSelectableHtml($attributes, $groupnames, $arrtfields);
        $sortablehtml = $this->getSortableHtml($arrtfields);
        $sortablehtml_1 = $this->getSortable_storeHtml($arrtfields_store);
        $html = '<div>
<div class="entry-edit fltlft">
    <div class="entry-edit-head">
        <h4 class="icon-head head-edit-form fieldset-legend">Select Attributes</h4>
    </div>
    <div class="fieldset customselecatablediv">
        ' . $selectablehtml . '
    </div>
     <div class="entry-edit-head">
        <h4 class="icon-head head-edit-form fieldset-legend">Select Stock Information</h4>
    </div>
    <div class="fieldset customselecatablediv1">
       <h3>Stock Information</h3>
       <ol class="selectable1">
       <li id="edi_status" data-code1="edi_status">
        edi_status
        </li>
        </ol>
    </div>
</div>
<div class="entry-edit fltlft" style="margin-left:10px;">
    <div class="entry-edit-head">
        <h4 class="icon-head head-edit-form fieldset-legend">Sort Selected Attributes</h4>
    </div>
    <div class="fieldset customselecatablediv" id="customsortableattributes">
    '.$sortablehtml.'
    </div>
    <div class="fieldset customselecatablediv1" id="customsortableattributes1">
    '.$sortablehtml_1.'
    </div>
    <input type="hidden" name="others[csvfields]" id="finalcsvfields" value="' . $arrtfields . '"/>
    <input type="hidden" name="others[store_fields]" id="finalcsvfields_store" value="' . $arrtfields_store . '"/>
</div>
<div class="clearer"></div>
</div>';
        return $html;
    }

    public function getFormattedAttributes($attrset) {
        $finalattributes = $commonattrs = $attributes = $obsr = array();
        foreach ($attrset as $attr_set_id) {
            $obsr[] = $attr_set_id;
            $attrs = Mage::getModel('catalog/product_attribute_api')->items($attr_set_id);
            foreach ($attrs as $k => $attr) {
                $attributes[$attr_set_id][$attr['attribute_id']] = $attr['code'];
            }
        }
        //Here let format the attributes by grouping common attributes into one group
        if (count($obsr) > 1) {
            foreach ($obsr as $k => $v) {
                $next = $k + 1;
                $commonattrs = (isset($obsr[$next])) ? array_intersect($attributes[$v], $attributes[$obsr[$next]]) : $commonattrs;
            }
            if (!empty($commonattrs))
                $finalattributes['common'] = $commonattrs;
            foreach ($attributes as $attr_set_id => $attrs) {
                $unique = array_diff($attrs, $commonattrs);
                if (!empty($unique)) {
                    $finalattributes[$attr_set_id] = $unique;
                }
            }
        } else {
            $finalattributes['common'] = $commonattrs = $attributes[$obsr[0]];
        }
        return $finalattributes;
    }

    public function getSelectableHtml($attributes, $groupnames, $arrtfields) {
        $selecatablehtml = "";
        $arrtfieldsarr = explode(',', $arrtfields);
        foreach ($attributes as $attrset_id => $attrs) {
            $selecatablehtml .="<h3>{$groupnames[$attrset_id]} Attributes</h3><ol class='selectable'>";
            foreach ($attrs as $attr_id => $attr_code) {
                $uiselected = (in_array($attr_code, $arrtfieldsarr)) ? 'ui-selected' : '';
                $selecatablehtml .= '<li class="ui-widget-content ' . $uiselected . '" id="' . $attr_id . '" data-code="' . $attr_code . '">' . $attr_code . '</li>';
            }
            $selecatablehtml .="</ol>";
        }
        return $selecatablehtml;
    }

    public function getSortableHtml($arrtfields) {
        $cancl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'adminhtml/default/default/images/fedobe/cancel_icon.gif';
        //$cancl = $this->getSkinUrl('images/fedobe/cancel_icon.gif');
        $sortablehtml = '<ul class="sortable ui-sortable">';
        $arrtfieldsarr = explode(',', $arrtfields);
       // print_r($arrtfieldsarr);
        foreach ($arrtfieldsarr as $attr_id => $attr_code) {
            $sortablehtml .= '<li class="ui-state-default ui-sortable-handle" id="' .$attr_code. '"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>' .$attr_code;
            if(!in_array($attr_code,array('name','price','sku','quantity','last_updated_time')))
                $sortablehtml .= '<span style="float:right;color:black"><a href="javascript:void(0);" onclick="cancelli(this,\''.$attr_code.'\');"><img src="'.$cancl.'" /></a></span>';
            $sortablehtml .= '</li>';
        }
        $sortablehtml .="</ul>";
        return $sortablehtml;
    }
    
     public function getSortable_storeHtml($arrtfields) {
        $cancl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).'adminhtml/default/default/images/fedobe/cancel_icon.gif';
        $sortablehtml = '<ul class="sortable_store ui-sortable">';
        $arrtfieldsarr = explode(',', $arrtfields);
        foreach ($arrtfieldsarr as $attr_id => $attr_code) {
            $sortablehtml .= '<li class="ui-state-default ui-sortable-handle" id="' .$attr_code. '"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>' .$attr_code;
            $sortablehtml .= '<span style="float:right;color:black"><a href="javascript:void(0);" onclick="cancelli_store(this,\''.$attr_code.'\');"><img src="'.$cancl.'" /></a></span>';
            $sortablehtml .= '</li>';
        }
        $sortablehtml .="</ul>";
        return $sortablehtml;
    }
}
