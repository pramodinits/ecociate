<?php

class Fedobe_Bridgecsv_Model_Ediimport extends Mage_Core_Model_Abstract {

    var $cron_limit = 100;

    function getActiveChannelsForEDI() {
        $channel_ids = Mage::getModel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('channel_type', 'incomingbridgecsv')
                ->addFieldToFilter('product_pricenquantity', 1)
                ->addFieldToFilter('is_hidden', 0)
                ->addFieldToSelect(array('id'))
                ->getData();
        if ($channel_ids)
            $channel_ids = array_column($channel_ids, 'id');
        return $channel_ids;
    }

    /**
     * get active catalog product's sku list
     */
    public function getEnableSkus() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $status_attr_id = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'status');
        $enabled_sku = $readConnection->fetchAll("SELECT product.sku FROM catalog_product_entity as product JOIN catalog_product_entity_int as status_attr ON product.entity_id=status_attr.entity_id AND status_attr.attribute_id=$status_attr_id AND status_attr.value=1 GROUP BY status_attr.entity_id;");
        $enabled_sku = $enabled_sku ? array_column($enabled_sku, 'sku') : array(0);
        $enabled_sku = array_map('addslashes', $enabled_sku);
        return $enabled_sku;
    }

    /**
     * get catalog product's sku list
     */
    public function getExistSkus() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $status_attr_id = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'status');
        $enabled_sku = $readConnection->fetchAll("SELECT product.sku FROM catalog_product_entity as product"); // JOIN catalog_product_entity_int as status_attr ON product.entity_id=status_attr.entity_id AND status_attr.attribute_id=$status_attr_id AND status_attr.value=1 GROUP BY status_attr.entity_id;");
        $enabled_sku = $enabled_sku ? array_column($enabled_sku, 'sku') : array(0);
        $enabled_sku = array_map('addslashes', $enabled_sku);
        return $enabled_sku;
    }

    /**
     * get active catalog product's sku list having no inventory
     */
    public function getEnableNonStockSkus() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $status_attr_id = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'status');
//get products who have no invntory
        $enabled_sku_entity_id = $readConnection->fetchAll("SELECT product.sku,product.entity_id FROM catalog_product_entity as product JOIN catalog_product_entity_int as status_attr ON product.entity_id=status_attr.entity_id AND status_attr.attribute_id=$status_attr_id AND status_attr.value=1  JOIN cataloginventory_stock_item as inventory on inventory.product_id=product.entity_id AND inventory.qty<=0 GROUP BY status_attr.entity_id;");
        $enabled_sku_entity_id = $enabled_sku_entity_id ? array_column($enabled_sku_entity_id, 'sku', 'entity_id') : array(0);
        $enabled_sku = array_map('addslashes', $enabled_sku_entity_id);
        return $enabled_sku;
    }

    /**
     * get catalog product's sku list having no inventory
     */
    public function getNonStockSkus() {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $status_attr_id = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'status');
//get products who have no invntory
        $enabled_sku_entity_id = $readConnection->fetchAll("SELECT product.sku,product.entity_id FROM catalog_product_entity as product JOIN cataloginventory_stock_item as inventory on inventory.product_id=product.entity_id AND inventory.qty<=0 GROUP BY product.entity_id;");
        $enabled_sku_entity_id = $enabled_sku_entity_id ? array_column($enabled_sku_entity_id, 'sku', 'entity_id') : array(0);
        $enabled_sku = array_map('addslashes', $enabled_sku_entity_id);
        return $enabled_sku;
    }

    /**
     * $priority (1=>nonzero-to-zero,2=>price-change,3=>zero-to-nonzero,4-remaining)
     * update the skus which are updated to nonzro from zero stock 
     * and update the skus which are updated to zero from nonzero stock 
     */
    public function processPrioritiesSku($priority, $is_live = 0) {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $incomingchannel_ids = $this->getActiveChannelsForEDI();
        if ($incomingchannel_ids):
            $incomingchannel_ids = implode(',', $incomingchannel_ids);
            $enabled_sku = $this->getEnableSkus();
            $enabled_sku = "'" . implode("','", $enabled_sku) . "'";
            $limit = $this->cron_limit;
            if ($is_live) {
                //join incomingchannel product info and csv data table to get live $priority skus
                $query = "SELECT csv.sku,csv.price,csv.quantity,csv.channel_id,bridge.id bridge_id,bridge.price bridge_price,bridge.quantity bridge_quantity FROM bridgecsv_data csv JOIN bridge_incomingchannel_product_info bridge ON csv.sku=bridge.sku AND csv.sku IN ($enabled_sku) AND csv.priority=$priority AND csv.edi_processed=0  AND bridge.is_live=1 AND bridge.channel_id=csv.channel_id AND bridge.channel_type='incoming'  WHERE csv.channel_id IN ($incomingchannel_ids)  AND csv.primary_field_updated=1 ORDER BY csv.last_updated_time limit $limit;";
            } else {
                $query = "SELECT csv.sku,csv.price,csv.quantity,csv.channel_id,bridge.id bridge_id,bridge.price bridge_price,bridge.quantity bridge_quantity FROM bridgecsv_data csv LEFT JOIN bridge_incomingchannel_product_info bridge ON csv.sku=bridge.sku AND bridge.channel_id=csv.channel_id AND bridge.channel_type='incoming'  WHERE csv.channel_id IN ($incomingchannel_ids) AND csv.primary_field_updated=1 AND csv.sku IN ($enabled_sku) AND csv.priority=$priority AND csv.edi_processed=0  ORDER BY csv.last_updated_time limit $limit;";
            }
            try {
                $priority_skus = $readConnection->fetchAll($query);
            } catch (Exception $e) {
                echo "<pre>";
                echo $query;
                exit;
            }


            if ($priority_skus) {
                Mage::getmodel('bridge/incomingchannels_product')->saveSkuInfo(0, null, $priority_skus, 0, 0, 0, 0, null, 0);
                $this->changeEdiProcessStatus($priority_skus);
                return true;
            }
        endif;
        return false;
    }

    /**
     * change the ediprocess status
     */
    public function changeEdiProcessStatus($sku_details) {
        $updata_query = '';
        foreach ($sku_details as $sku_info) {
            $updata_query.="UPDATE bridgecsv_data SET edi_processed=1 WHERE sku='{$sku_info['sku']}' AND channel_id={$sku_info['channel_id']};";
        }
        if ($updata_query) {
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection('core_write');
            $writeConnection->raw_query($updata_query);
        }
    }

    /**
     * first edi cron process to process the delted live process
     */
    public function processDeletedSku() {
        $limit = $this->cron_limit;
        $incomingchannel_ids = $this->getActiveChannelsForEDI();
        if ($incomingchannel_ids) {
            $incomingchannel_ids_str = implode(',', $incomingchannel_ids);
            $query = "SELECT bridge.id bridge_id,csv.is_deleted,csv.channel_id,csv.sku FROM bridgecsv_data csv JOIN bridge_incomingchannel_product_info bridge ON csv.sku=bridge.sku AND bridge.channel_id=csv.channel_id AND bridge.channel_type='incoming'  WHERE csv.channel_id IN ($incomingchannel_ids_str) AND csv.edi_processed=0 AND csv.is_deleted =1 ORDER BY bridge.is_live desc,csv.last_updated_time limit $limit;";
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $priority_skus = $readConnection->fetchAll($query);

            if ($priority_skus) {
                $inventory_ids = array_column($priority_skus, 'bridge_id');
                Mage::getmodel('bridge/incomingchannels_product')->saveDeletedSkuInfo($inventory_ids);
                $this->changeEdiProcessStatus($priority_skus);
                return true;
            }
        }
        return false;
    }

    /**
     * second process the skus for which own magento store have no inventory
     */
    public function processNonStockSku() {
        $enabled_sku = $this->getEnableNonStockSkus();
        if ($enabled_sku) {
            $enabled_sku = "'" . implode("','", $enabled_sku) . "'";
            $incomingchannel_ids = $this->getActiveChannelsForEDI();
            if ($incomingchannel_ids) {
                $incomingchannel_ids_str = implode(',', $incomingchannel_ids);
                $limit = $this->cron_limit;
                //get products who have no inventory
                $select_query = "SELECT csv.sku,csv.price,csv.quantity,csv.channel_id,bridge.id bridge_id,bridge.price bridge_price,bridge.quantity bridge_quantity FROM bridgecsv_data csv LEFT JOIN bridge_incomingchannel_product_info bridge ON csv.sku=bridge.sku AND bridge.channel_id=csv.channel_id AND bridge.channel_type='incoming'  WHERE csv.channel_id IN ($incomingchannel_ids_str) AND csv.primary_field_updated=1 AND csv.sku IN ($enabled_sku) AND csv.quantity>0  ORDER BY csv.last_updated_time limit $limit;"; //echo $select_query;exit;
                $resource = Mage::getSingleton('core/resource');
                $readConnection = $resource->getConnection('core_read');
                $priority_skus = $readConnection->fetchAll($select_query);
                //echo "<pre>";
                //print_r($priority_skus);exit;


                if ($priority_skus) {
                    Mage::getmodel('bridge/incomingchannels_product')->saveSkuInfo(0, null, $priority_skus, 0, 0, 0, 0, null, 0);
                    $this->changeEdiProcessStatus($priority_skus);
                    return true;
                }
            }
        }
        return false;
    }

}