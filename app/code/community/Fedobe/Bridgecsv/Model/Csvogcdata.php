<?php
class Fedobe_Bridgecsv_Model_Csvogcdata extends Mage_Core_Model_Abstract {
     public function _construct() {
        parent::_construct();
        $this->_init('bridgecsv/csvogcdata');
    }
    
    public function saveogc($data){
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $table = Mage::getSingleton('core/resource')->getTableName('bridgecsv/csvogcdata');
        $sql = '';
        $columns =array_keys($data[0]);
        foreach ($columns as $k2 => $v2) {
            $sql_inest[$v2] = "$v2=VALUES($v2)";
        }
        $sql_inest['updated_at'] = 'updated_at= NOW()';
        $sql_inest['is_condition_changed'] = 'is_condition_changed= 0';
        $columns_str = "(".implode(",",$columns).")";
        $values_arr = array();
        foreach($data as $k1=>$v1){
            $values =array_values($v1);//print_r($values);exit; 
            $values = array_map(function($v){
                $v = addslashes($v);
                return $v;
            }, $values);
            $values_arr[] = "('".implode("','",  $values)."')";
        }
        
        $values_str = implode(',',$values_arr);
        $sql = "INSERT INTO  $table " . $columns_str . " VALUES " . $values_str . ' ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id),'
                    . implode(',', $sql_inest);
        $writeConnection->query($sql);
        echo $sql;exit;
    }
}
?>