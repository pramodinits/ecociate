<?php

class Fedobe_Bridgecsv_Model_Data extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('bridgecsv/data');
    }

    public function loadByChannelId($id) {
        if ($id) {
            $this->_getResource()->loadByField($this, $id);
            return $this;
        }
    }

    public function getPimDataToProcess() {
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $readConnection = $resource->getConnection('core_read');
        $channel_id_arr = Mage::helper('bridgecsv')->getPIMChannels();
        $all_inchannel_ids_weightage_arr = Mage::helper('bridge')->getPIMChannels();
        $all_inchannel_ids_arr=array_keys($all_inchannel_ids_weightage_arr);
        if ($channel_id_arr):
            $channel_ids = implode(',', $channel_id_arr);
        $all_inchannel_ids = implode(',', $all_inchannel_ids_arr);
        $feasible_sku = $readConnection->fetchRow("SELECT sku FROM bridgecsv_data WHERE pim_processed=0 AND primary_field_updated=1 AND pim_skipped=0 AND is_header=0 AND channel_id IN($channel_ids) AND sku IS NOT NULL AND store_view IS NOT NULL LIMIT 0,1;");
        if ($feasible_sku):
                //check if the sku is exist
            $feasible_sku = $feasible_sku['sku'];
        $product = Mage::getmodel('catalog/product')->loadByAttribute('sku', $feasible_sku);
        $existing_stores = array_column(Mage::getModel('core/store')->getCollection()->addFieldToSelect('store_id')->getData(), 'store_id');
        if ($product) {
                    //get channel priority  by store view basis
            $bridge_channel_priority_id = Mage::getResourceModel('eav/entity_attribute')
            ->getIdByCode('catalog_product', 'channel_priority');
            $entityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
            $skip_priorities_storeview = $readConnection->fetchAll("SELECT store_id FROM catalog_product_entity_text WHERE attribute_id=$bridge_channel_priority_id AND entity_type_id=$entityTypeId AND entity_id={$product->getId()} AND value is NOT NULL;");
            if ($skip_priorities_storeview) {
                $skip_priorities_storeview = array_column($skip_priorities_storeview, 'store_id');
                $existing_stores = array_diff($existing_stores, $skip_priorities_storeview);
            }
            if ($existing_stores) {
                        //get the previously updated channel info store view basis
                $bridge_channel_existing_id = Mage::getResourceModel('eav/entity_attribute')
                ->getIdByCode('catalog_product', 'bridge_channel_id');
                $skip_existing_channel_storeview = $readConnection->fetchAll("SELECT store_id,value FROM catalog_product_entity_int WHERE attribute_id=$bridge_channel_existing_id AND entity_type_id=$entityTypeId AND entity_id={$product->getId()} AND value IN ($all_inchannel_ids);");
                $skip_existing_channel_storeview = $skip_existing_channel_storeview?array_column($skip_existing_channel_storeview, 'value', 'store_id'):array();
                        //remove the channel ids for which update existing product automatically is not set
                $channel_id_arr = Mage::getmodel('bridge/incomingchannels')
                ->getCollection()
                ->addFieldToFilter('id', $channel_id_arr)
                ->addFieldToFilter('update_exist', 1)
                ->addFieldToSelect('id')
                ->setOrder('weightage','asc')
                ->getData();
            }
        } else {
                    //remove the channel ids for which create new product automatically is not set
            $channel_id_arr = Mage::getmodel('bridge/incomingchannels')
            ->getCollection()
            ->addFieldToFilter('id', $channel_id_arr)
            ->addFieldToFilter('create_new', 1)
            ->addFieldToSelect('id')
            ->setOrder('weightage','asc')
            ->getData();
        }
        $channel_ids = $channel_id_arr?implode(',', array_column($channel_id_arr, 'id')):0;
        if ($channel_ids && $existing_stores) {
            $existing_stores=implode(',',$existing_stores);
            $query = "SELECT temp_tbl.* FROM (SELECT * FROM bridgecsv_data WHERE pim_processed=0  AND pim_skipped=0 AND sku='{$feasible_sku}' AND channel_id IN($channel_ids)  AND store_view IN ($existing_stores) AND is_header=0 ORDER BY channel_weightage ASC) AS temp_tbl GROUP BY temp_tbl.store_view;";
            $result = $readConnection->fetchAll($query);
            

                    //if we have skus to sync and that are previously upated by other channel then compare the weightage between two if 
                    //previous channel have greater weightage the skip this sku
            if($result && $skip_existing_channel_storeview){
                        //echo "<pre>";
                       // print_r($result);exit;
                foreach($result as $result_key=>$product_info){
                    if(isset($skip_existing_channel_storeview[$product_info['store_view']]) && $all_inchannel_ids_weightage_arr[$skip_existing_channel_storeview[$product_info['store_view']]]<$product_info['channel_weightage'])
                        unset($result[$result_key]);
                }
            }
            
        }
        if($result){
            if(@$product)
                $result['product']=$product;
            return $result;
        }

        endif;
        endif;
        return false;
//        print_r(explode('channel_data', $result));exit;
    }
    /**
     * change the pimprocess status
     */
    public function changePimProcessStatus($sku) {
        $updata_query="UPDATE bridgecsv_data SET pim_processed=1 WHERE sku='{$sku}' AND pim_skipped=0;";
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $writeConnection->raw_query($updata_query);
    }
    /**
     * change the pimprocess ignore status
     */
    public function setSkippedStatus($id) {
        $updata_query="UPDATE bridgecsv_data SET pim_skipped=1 WHERE id='{$id}';";
        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $writeConnection->raw_query($updata_query);
    }

}
