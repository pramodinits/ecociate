<?php

class Fedobe_Bridgecsv_Model_Process extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('bridgecsv/process');
    }
    
    public function loadByChannelId($id){
        if($id){
            $this->_getResource()->loadByField($this, $id);
            return $this;
        }
    }
}
