<?php

require_once(Mage::getModuleDir('controllers', 'Fedobe_Bridge') . DS . 'Adminhtml' . DS . 'IncomingchannelsController.php');

class Fedobe_Bridgecsv_Adminhtml_IncomingbridgecsvController extends Fedobe_Bridge_Adminhtml_IncomingchannelsController {

    public function preDispatch() {
        $bypassaction = array('csv_pim_cron', 'update_csv_data', 'csv_edi_cron', 'processRemotecsv', 'update_csv_data_cron');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }

    public function _title($msg) {
        parent::_title(($msg) ? $msg : Mage::helper('bridgecsv')->__('Fedobe CSV Incoming Extension'));
    }

    public function _setActiveMenu() {
        parent::_setActiveMenu('fedobe/bridge/outgoing');
    }

    public function indexAction() {
        $this->_title(Mage::helper('bridge')->__('Manage Incoming Channels'));
        $this->loadLayout();
        $this->renderLayout();
    }

    public function checkfileExists($filename) {
        $k = 1;
        $path = Mage::helper('bridgecsv')->getUploadDirectory() . $filename;
        if (file_exists($path . ".csv")) {
            $filename = $filename . '_' . $k;
            $k++;
            return $this->checkfileExists($filename);
        } else {
            return $filename;
        }
    }

    public function saveAction() {
        $isAdmin = Mage::helper('bridge')->isAdmin();
        if ($this->getRequest()->getPost()) {
            $data = $this->getRequest()->getPost();

            try {
                Mage::getSingleton('adminhtml/session')->setPageData($data);
                $model = Mage::getModel('bridge/incomingchannels');
                $id = $data['id'];
                $new_upload = false;
                if ($id)
                    $model->load($id);
                $prev_edi_cron = $model->getEdiCron() ? $model->getEdiCron() : 'never';
                $prev_pim_cron = $model->getPimCron() ? $model->getPimCron() : 'never';
//Here to validate CSV file
                if (isset($_FILES['upload_csv_file']['name']) && file_exists($_FILES['upload_csv_file']['tmp_name'])) {
                    try {
                        $uploader = new Varien_File_Uploader('upload_csv_file');
                        $uploader->setAllowedExtensions(array('csv'));
                        $uploader->setAllowRenameFiles(false);
                        $uploader->setFilesDispersion(false);
                        $path = Mage::helper('bridgecsv')->getUploadDirectory();
                        $fileinfo = pathinfo($_FILES['upload_csv_file']['name']);
                        $filename = uniqid() . "_" . $fileinfo['filename'];
                        $filename = $this->checkfileExists($filename);
                        $uploader->save($path, "$filename.csv");
                        $data['others']['upload_csv_file'] = $filename . ".csv";
                        $new_upload = true;
                        $new_upload_name = $data['others']['upload_csv_file'];
                    } catch (Exception $e) {
                        $this->_getSession()->addError("Cannot upload the csv file,please check the file extension");
// The following line decides if it is a "save" or "save and continue"
                        if ($model->getId()) {
                            $this->_redirect('*/*/edit', array('id' => $model->getId()));
                        } else {
                            $this->_redirect('*/incomingchannels/');
                        }
                        return;
                    }
                }
                if ($isAdmin) {
                    if ($data['weightage']) {
                        if (@$data['id']) {
                            $is_exist = Mage::getModel('bridge/incomingchannels')->getCollection()->addFieldToFilter('weightage', $data['weightage'])
                                            ->addFieldToFilter('id', array('neq' => $data['id']))
                                            ->addFieldToSelect('id')->getFirstItem();
                        } else {
                            $is_exist = Mage::getModel('bridge/incomingchannels')->getCollection()->addFieldToFilter('weightage', $data['weightage'])
                                            ->addFieldToSelect('id')->getFirstItem();
                        }
                        if ($is_exist->getId()) {
                            $this->_getSession()->addError("An Incoming Channel with this wightage is exist");
                            echo Mage::app()->getResponse()->setRedirect($_SERVER['HTTP_REFERER']);
                            exit;
                        }
                    }
                    $changed_slab_order = 0;
                    $changed_weightage = 0;

                    if ($model->getSlab() != $data['slab'] || $model->getChannelOrder() != $data['channel_order'] || $model->getChannelSupplier() != $data['channel_supplier']) {
                        $changed_slab_order = 1;
                    }
                    if ($model->getWeightage() != $data['weightage']) {
                        $changed_weightage = 1;
                    }

                    unset($data['rule']);

                    $data['authorized'] = 1;
                }
                if (!empty($data['upload_csv_file']))
                    $data['others']['upload_csv_file'] = $data['upload_csv_file']['value'];

                if ($model->getData('others')) {
                    $dataothers = unserialize($model->getData('others'));
                    if (isset($dataothers['upload_csv_file']) && !$data['others']['upload_csv_file']) {
//                        $data['others'] = array_merge($data['others'],$dataothers);
                        $data['others']['upload_csv_file'] = $dataothers['upload_csv_file'];
                    }
                }

                $data['others'] = serialize($data['others']);
                $data['status'] = $data['channel_status'];
                $model->setData($data);
                try {

                    if ($model->getId() && $new_upload):
                        $csvmapping = Mage::getmodel('bridge/incomingchannels_mapping')->getCollection()
                                        ->addFieldToFilter('brand_id', $id)
                                        ->getFirstItem()->getData();
                        if ($csvmapping) {
                            $csvmapping = unserialize($csvmapping['data'])['import'];
                            if (!$this->autoProcesscsvAction($csvmapping, $new_upload_name)) {
                                $this->_getSession()->addError("Cannot upload the csv file,it does not match with the mapping headers");
                                echo Mage::app()->getResponse()->setRedirect($_SERVER['HTTP_REFERER']);
                                exit;
                            }
                        }
                    endif;
                    $model->save();
                } catch (Exception $e) {
                    echo "<pre>";
                    print_r($data);
                    print_r($e->getMessage());
                    exit;
                }

                if ($isAdmin) {
//Here to store the mapping information
                    $interface = $data['mapping'];
                    $import_mapping = array();
//                $mappingkeys = array('storeviewtype', 'storeviewmappingcolname', 'storeviewmappingcol', 'categorymaptype', 'csvcategorycolumnname', 'csvcategorycolumn', 'attributesettype', 'mapping', 'csvattrsetcol', 'allcategoryintomap', 'allstoreviewmap');
                    $formatting_keys = array('attributes', 'option_mapping', 'categories', 'attributeset', 'storeviewmappingcol', 'storeviewmappingcol', 'global', 'custommappingattr');

                    if ($data['mapping']) {
                        $import_mapping = array();
                        foreach ($data['mapping'] as $mapping_name => $mapping_value) {
                            if (in_array($mapping_name, $formatting_keys)) {
                                foreach ($mapping_value as $mapping_value_key => $mapping_value_value) {
                                    if (in_array($mapping_name, array('attributeset', 'categories', 'storeviewmappingcol'))) {
                                        if ($mapping_name == 'categories')
                                            $import_mapping['csvcategorycolumn'][$mapping_value_value['brand']] = $mapping_value_value['store'];
                                        else
                                            $import_mapping[$mapping_name][$mapping_value_value['brand']] = $mapping_value_value['store'];
                                    } else if (in_array($mapping_name, array('option_mapping', 'attributes', 'global', 'custommappingattr'))) {
                                        foreach ($mapping_value_value as $mapping_value_value_key => $mapping_value_value_value)
                                            if (!in_array($mapping_name, array('attributes', 'global', 'custommappingattr')) || ($mapping_name == 'global' && $mapping_value_key == 'option'))
                                                foreach ($mapping_value_value_value as $mvvvk => $mvvvv)
                                                    $import_mapping[$mapping_name][$mapping_value_key][$mapping_value_value_key][$mvvvv['brand']] = $mvvvv['store'];
                                            else if ($mapping_name == 'custommappingattr')
                                                $import_mapping['custom_maping'][$mapping_value_key][$mapping_value_value_value['brand']] = $mapping_value_value_value['store'];
                                            else
                                                $import_mapping[$mapping_name][$mapping_value_key][$mapping_value_value_value['brand']] = $mapping_value_value_value['store'];
                                    }
                                }
                            } else
                                $import_mapping[$mapping_name] = $mapping_value;
                        }
                    }
                    $mapping['data'] = serialize(array('interface' => $interface, 'import' => $import_mapping));

                    $mapping_model = Mage::getmodel('bridge/incomingchannels_mapping');
                    $mapping['id'] = $mapping_model->getCollection()->addFieldToFilter('brand_id', $model->getId())->addFieldToSelect('id')->getFirstItem()->getId();
                    if ($mapping['data']) {
                        $mapping['brand_id'] = $model->getId();
                        $mapping_model->setData($mapping);
                        $mapping_model->save();
                    }
//end of mapping section
                    if ($this->getRequest()->getParam('id') && $mapping_model->getId())
                        $this->setCron($prev_edi_cron, $prev_pim_cron);

//End
//add the incoming channel to attribute channel priority option
                    if (!$this->getRequest()->getParam('id')) {
                        $option_id = Mage::getModel('bridge/incomingchannels')->addChannelOption($model->getBrandName());
                        $inc_model = Mage::getmodel('bridge/incomingchannels');
                        $inc_model->setData(array('id' => $model->getId(), 'option_id' => $option_id));
                        $inc_model->save();
                    }

//end
//update edi cron table for update convert price
                    try {
                        $resource = Mage::getSingleton('core/resource');
                        $write = $resource->getConnection('core_write');

                        if (!$data['id']) {
                            $incid = $model->getId();
                            $write->raw_query("INSERT INTO bridge_edi_channelcron (channel_id,channel_type,status) values($incid,'incoming',{$data['channel_status']});");
                        } else {
                            $edicrondata = array("status" => $data['channel_status']);
                            $where = "channel_id = {$model->getId()} AND channel_type ='incoming'";
                            $write->update("bridge_edi_channelcron", $edicrondata, $where);
                        }
                    } catch (Exception $e) {
                        echo "<pre>";
                        echo "hello";
                        print_r($e->getMessage());
                        exit;
                    }
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('bridge')->__('The Channel has been saved.')
                );
                Mage::getSingleton('adminhtml/session')->setPageData(false);
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError("No data found to save");
        }
        if ($this->getRequest()->getParam('id') && $isAdmin) {
            $resource = Mage::getSingleton('core/resource');
            $writeConnection = $resource->getConnection("core_write");
//if slab or order is changed the ulpdate this in edi table if any inventory is exist for this channel
//            if ($changed_slab_order) {
            $bridge_edi_crontable = $resource->getTableName('bridge/incomingchannels_product');
            $writeConnection->raw_query("UPDATE $bridge_edi_crontable SET `slab`={$data['slab']},`order`={$data['channel_order']},channel_supplier={$data['channel_supplier']} WHERE channel_type='incoming' AND channel_id={$model->getId()};");
//            }
//if weightage is changed the update the csv data table if data exist
// if ($changed_weightage) {
            $bridgecsv_data = $resource->getTableName('bridgecsv/data');
            $writeConnection->raw_query("UPDATE $bridgecsv_data SET `channel_weightage`={$data['weightage']} WHERE  channel_id={$model->getId()};");
// }
        }
//}
// The following line decides if it is a "save" or "save and continue"
        if ($this->getRequest()->getParam('back')) {
            $this->_redirect('*/*/edit', array('id' => $model->getId()));
        } else {
            $this->_redirect('*/*/');
        }
    }

    public function generatesecuritykeyAction() {
        echo md5(uniqid());
    }

    public function checkftpconnectionAction() {
        $params = $this->getRequest()->getParams();
        $msg = array('type' => 0);
        $ftp_path = $params['ftppath'];
        $ftp_user = $params['ftpusrname'];
        $ftp_pass = $params['ftppass'];
        if ($ftp_path && $ftp_user && $ftp_pass) {
            $conn_id = ftp_connect($ftp_path);
            if ($conn_id) {
//Here let's try to login to FTP
                if (ftp_login($conn_id, $ftp_user, $ftp_pass)) {
                    $msg['msg'] = "Connected as $ftp_user@$ftp_path";
                    $msg['type'] = 1;
                } else {
                    $msg['msg'] = "Couldn't connect as $ftp_user to $ftp_path";
                }
            } else {
                $msg['msg'] = "Couldn't connect to $ftp_path";
            }
        } else {
            $msg['msg'] = "FTP details not found.";
        }
        echo json_encode($msg);
    }

    public function processcsvAction() {
        $id = $this->getRequest()->getPost('id');
        if ($id) {
            $model = Mage::getModel('bridge/incomingchannels');
            $channelsdata = $model->load($id);
            $channel_weightage = $channelsdata->getWeightage();
            $csvdata = unserialize($channelsdata->getData('others'));
            $separator = @$csvdata['csv_separator'] ? $csvdata['csv_separator'] : ",";
//Here let's start with local uploaded file
            if ($csvdata['csv_source_type'] == '3') {
//Here let's start process the csv file uploaded
                $filepath = Mage::helper('bridgecsv')->getUploadDirectory() . $csvdata['upload_csv_file'];
                if (file_exists($filepath)) {
                    $resource = Mage::getSingleton('core/resource');
                    $processtable = $resource->getTableName('bridgecsv/process');
                    $csvdatatable = $resource->getTableName('bridgecsv/data');
                    $readConnection = $resource->getConnection('core_read');
                    $writeConnection = $resource->getConnection('core_write');
                    $sql = $readConnection->select()->from($processtable, 'id')->where("channel_id=$id ")->limit(1);
                    $processdata = $readConnection->fetchAll($sql);
                    if ($processdata) {
                        if ($processdata['status']) {
//Here donot allow other CSV file to get processed till one under process
                            echo "This channel already processed!";
                        } else {
//Here donot allow other CSV file to get processed till one under process
                            echo "This channel under process,please wait till it finishes!";
                        }
                    } else {
//Here to mark this file and channel as under process.
                        $fp = file($filepath);
//Here let's check whether empty file
                        if (count($fp) > 1) {
                            $csvprocess = Mage::getModel('bridgecsv/process');
                            $processdata = array(
                                'channel_id' => $id,
                                'filename' => $csvdata['upload_csv_file'],
                                'total_records' => count($fp),
                            );
                            $csvprocess->setData($processdata);
                            $csvprocess->save();
                            $processid = $csvprocess->getId();
//Here let's prepare the data to get inserted into CSV data table
                            $header = $totaldata = $csvcntdata = $tmpdata = array();
                            $row = 1;
                            if (($handle = fopen($filepath, "r")) !== FALSE) {
                                while (($csv_data = fgetcsv($handle, 5000, $separator)) !== FALSE) {
                                    $num = count($csv_data);
                                    $totaldata[$row] = array(
                                        'channel_id' => $id,
                                        'csv_line_number' => $row,
                                    );

                                    if ($row == 1) {
                                        for ($c = 0; $c < $num; $c++) {
                                            $header[$c] = utf8_encode($csv_data[$c]);
                                        }
                                        $csvcntdata = $header;
                                        $totaldata[$row]['is_header'] = 1;
                                    } else {
                                        for ($c = 0; $c < $num; $c++) {
                                            $tmpdata[$c] = utf8_encode($csv_data[$c]);
                                        }
                                        $csvcntdata = array_combine($header, $tmpdata);
                                        $totaldata[$row]['is_header'] = 0;
                                    }
                                    $totaldata[$row]['data'] = serialize($csvcntdata);
                                    $totaldata[$row]['channel_weightage'] = $channel_weightage;
                                    if ($row % 500 == 0) {
                                        $writeConnection->insertMultiple($csvdatatable, $totaldata);
                                        $totaldata = array();
                                    }
                                    $row++;
                                }
                            }
                            if (!empty($totaldata)) {
                                try {
                                    $writeConnection->insertMultiple($csvdatatable, $totaldata);
//Here let's remove the process flag to completed
                                    $csvprocess->load($processid);
                                    $csvprocess->setData('status', 1);
                                    echo "Data processed Succesfully!";
                                    $csvprocess->save();
                                } catch (Exception $ex) {
                                    echo "Something went wrong!";
                                }
                            } else {
                                echo "Nothing saved!";
                            }
                        } else {
                            echo "Cannot process an Empty File!";
                        }
                    }
                } else {
                    echo "{$csvdata['upload_csv_file']} not found";
                }
            }
        }
    }

    /**
     * auto process csv action on upload of csv if mapping is exist for this product
     */
    public function autoProcesscsvAction($mapping_data, $file_name) {
        $id = $this->getRequest()->getParam('id');

        if ($id) {
            $model = Mage::getModel('bridge/incomingchannels');
            $channelsdata = $model->load($id);
            $channel_weightage = $channelsdata->getWeightage();
            $csvothers = unserialize($channelsdata->getData('others'));
            $separator = @$csvothers['csv_separator'] ? $csvothers['csv_separator'] : ",";
//Here let's start with local uploaded file
//Here let's start process the csv file uploaded
            $filepath = Mage::helper('bridgecsv')->getUploadDirectory() . $file_name;
            if (file_exists($filepath)) {//echo $filepath;exit;
                $resource = Mage::getSingleton('core/resource');
                $processtable = $resource->getTableName('bridgecsv/process');
                $csvdatatable = $resource->getTableName('bridgecsv/data');
                $readConnection = $resource->getConnection('core_read');
                $writeConnection = $resource->getConnection('core_write');
//                $prev_header = Mage::getModel('bridgecsv/data')->getCollection()
//                        ->addFieldToFilter('channel_id', $id)
//                        ->addFieldToFilter('is_header', 0)
//                        ->addFieldToSelect('data')
//                        ->getFirstItem()
//                        ->getData();
                $existing_sku = Mage::getModel('bridgecsv/data')->getCollection()
                        ->addFieldToFilter('channel_id', $id)
                        ->addFieldToFilter('is_header', 0)
                        ->addFieldToFilter('sku', array('notnull' => true))
                        ->addFieldToSelect(array('sku', 'id'))
                        ->getData();
                $existing_sku = array_column($existing_sku, 'id', 'sku');
//get the server current time
                $now_time = $readConnection->fetchRow("SELECT NOW() FROM $csvdatatable WHERE 1 limit 0,1");
                $now_time = $now_time['NOW()'];


//                $prev_header = array_map('utf8_decode', array_keys(unserialize($prev_header['data'])));
                $sql = $readConnection->select()->from($processtable, 'id')->where("channel_id=$id ")->limit(1);
                $processdata = $readConnection->fetchRow($sql);
//Here to mark this file and channel as under process.
                $fp = file($filepath);
//Here let's check whether empty file
                if (count($fp) > 1) {
//$processid = $csvprocess->getId();
//Here let's prepare the data to get inserted into CSV data table
                    $header = $totaldata = $csvcntdata = $tmpdata = array();
                    $row = 1;
                    $update_query = '';

                    if (($handle = fopen($filepath, "r")) !== FALSE) {
                        $existing_skus_update = array();
                        while (($csv_data = fgetcsv($handle, 5000, $separator)) !== FALSE) {
                            $num = count($csv_data);
                            $tmpdata = array();
                            $totaldata[$row] = array(
                                'channel_id' => $id,
                                'csv_line_number' => $row,
                            );

                            if ($row == 1) {
                                for ($c = 0; $c < $num; $c++) {
                                    $header[$c] = utf8_encode($csv_data[$c]);
                                }
//                                if (array_diff($prev_header, $header)) {
//                                    $mapping_conflicts = 1;
//                                    return false;
//                                }
                                $csvcntdata = serialize($header);
                                $update_query = "UPDATE $csvdatatable SET data='{$csvcntdata}',last_updated_time='{$now_time}' WHERE is_header=1 AND channel_id=$id;";
                                $row++;
                            } else {
                                for ($c = 0; $c < $num; $c++) {
                                    $tmpdata[$c] = utf8_encode($csv_data[$c]);
                                }
                                $before_mapped = array_combine($header, $tmpdata);
                                $sku = $this->getMappedSku($mapping_data, $before_mapped);
                                if ($sku) {
                                    $csvcntdata = serialize($before_mapped);
                                    if (isset($existing_sku[$sku])) {
                                        $existing_skus_update[] = $existing_sku[$sku];
                                        $update_query .="UPDATE $csvdatatable SET data='{$csvcntdata}',last_updated_time='{$now_time}',edi_processed=0,pim_processed=0,primary_field_updated=0,make_out_of_stock=0 WHERE id={$existing_sku[$sku]};";
                                    } else
                                        $update_query .="INSERT INTO  $csvdatatable (channel_id,sku,data,channel_weightage) VAlUES($id,'{$sku}','{$csvcntdata}',{$channel_weightage});";
                                    $row++;
                                } else
                                    continue;
                            }
                            if ($row % 500 == 0) {
                                $writeConnection->raw_query($update_query);
                                $update_query = '';
                            }
                        }
//echo $update_query;exit;

                        if ($update_query) {
//                            echo $update_query;exit;
                            $writeConnection->raw_query($update_query);
                            $update_query = '';
                        }
                        if ($existing_skus_update) {
                            $existing_skus_update = implode(',', $existing_skus_update);
                            $writeConnection->raw_query("UPDATE $csvdatatable SET make_out_of_stock=0 WHERE id IN ($existing_skus_update);");
                            if ($csvothers['miss_sku_action'] == 'out_of_stock') {
                                $writeConnection->raw_query("UPDATE $csvdatatable SET quantity=0,make_out_of_stock=1,edi_processed=0,priority=1 WHERE id NOT IN ($existing_skus_update) AND channel_id=$id AND is_header=0;");
                            } else if ($csvothers['miss_sku_action'] == 'deleted') {
                                $writeConnection->raw_query("UPDATE $csvdatatable SET quantity=0,make_out_of_stock=1,edi_processed=1,is_deleted=1 WHERE id NOT IN ($existing_skus_update) AND channel_id=$id AND is_header=0;");
                                $deletedSkus = $readConnection->fetchAll("SELECT sku FROM  $csvdatatable WHERE id NOT IN ($existing_skus_update) AND channel_id=$id AND is_header=0;");
                                if ($deletedSkus) {
                                    $deletedSkus = array_column($deletedSkus, 'sku');
                                    $deletedSkus = '"' . implode('","', $deletedSkus) . '"';
                                    $inventoryTobeDeleteHndl = $readConnection->query("SELECT id,product_entity_id,is_live FROM  bridge_incomingchannel_product_info WHERE sku IN ($deletedSkus) AND channel_id=$id;");
                                    while ($inventoryRes = $inventoryTobeDeleteHndl->fetch()) {
                                        $productId = $inventoryRes['product_entity_id'];
                                        $inventoryId = $inventoryRes['id'];
                                        if ($inventoryRes['is_live']) {
                                            $outOfStockQuery = "UPDATE cataloginventory_stock_item SET is_in_stock=0 WHERE product_id=$productId;";
                                            $writeConnection->query($outOfStockQuery);
                                        }
                                        //delete from edi inventory
                                        $deleteInvQUery = "DELETE FROM bridge_incomingchannel_product_info WHERE  id=$inventoryId;";
                                        $writeConnection->query($deleteInvQUery);
                                    }
                                }
                            }
                        }
//                        if (!$mapping_conflicts) {
                        $csvprocess = Mage::getModel('bridgecsv/process');
                        $csvprocess->setData(array(
                            'id' => $processdata['id'],
                            'filename' => $file_name,
                            'total_records' => --$row,
                            'status' => 1
                        ));
                        $csvprocess->save();
//                        }
                    } else {
                        echo "Cannot process an Empty File!";
                    }
                }
            } else {
                echo "{$file_name} not found";
            }
        }
        return true;
    }

//end
    /**
     * bridgecsv cron setup
     */
    public function setCron($prev_edi_cron, $prev_pim_cron) {
        $incoming_model = Mage::getmodel('bridge/incomingchannels')->load($this->getRequest()->getParam('id'));
        if ($incoming_model->getProductInfo() && ($incoming_model->getCreateNew() || $incoming_model->getUpdateExist()))
            $pim_url = Mage::getBaseUrl() . "admin/incomingbridgecsv/csv_pim_cron";
        if ($incoming_model->getProductPricenquantity())
            $edi_url = Mage::getBaseUrl() . "admin/incomingbridgecsv/csv_edi_cron";
        if (!file_exists($_SERVER["DOCUMENT_ROOT"] . '/bridgecron')) {
            mkdir($_SERVER["DOCUMENT_ROOT"] . '/bridgecron');
            chmod($_SERVER["DOCUMENT_ROOT"] . '/bridgecron', 0777);
        }
        $other = unserialize($incoming_model->getOthers());
        $crons = shell_exec('crontab -l');
        $cronurl = 'wget -o /dev/null -q -O -';
        $endcronurl = '> /dev/null';

//first remove the remote incoming csv file download cron
        $ftp_prev_download_cron_timing = $this->getFtpDownloadTiming($prev_edi_cron, $prev_pim_cron);
//set ftp file download cron 
        $ftp_download_url = Mage::getBaseUrl() . "admin/incomingbridgecsv/processRemotecsv/id/" . $incoming_model->getId();
        if ($ftp_prev_download_cron_timing) {
            $ftp_prev_download_cron = "$ftp_prev_download_cron_timing " . $cronurl . ' ' . $ftp_download_url . ' ' . $endcronurl;
            $crons = str_replace($ftp_prev_download_cron, "", $crons);
        }

        if (($pim_url || $edi_url) && $incoming_model->getStatus()) {
//set primary field update cron 
            $update_csv_data_url = Mage::getBaseUrl() . "admin/incomingbridgecsv/update_csv_data_cron";
            $update_csv_data_cron = '0 0 * * * ' . $cronurl . ' ' . $update_csv_data_url . ' ' . $endcronurl;
            $crons = str_replace($update_csv_data_cron, "", $crons);
            $crons.=$update_csv_data_cron . PHP_EOL;
            if (($incoming_model->getPimCron() != 'never' || $incoming_model->getEdiCron() != 'never') && $other['csv_source_type'] == 1) {
                $ftp_download_cron_timing = $this->getFtpDownloadTiming($incoming_model->getEdiCron(), $incoming_model->getPimCron());
                $ftp_download_cron = "$ftp_download_cron_timing " . $cronurl . ' ' . $ftp_download_url . ' ' . $endcronurl;
                $crons.=$ftp_download_cron . PHP_EOL;
            }

//set pim cron 
            $pim_cron = '* * * * * ' . $cronurl . ' ' . $pim_url . ' ' . $endcronurl;
//set edi cron 
            $edi_cron = '* * * * * ' . $cronurl . ' ' . $edi_url . ' ' . $endcronurl;
//$cron.=$cron_ready_str.PHP_EOL;       
//echo $pg_count;exit;
            if (@$pim_url) {
                $crons = str_replace($pim_cron, "", $crons, $pim_count);
                $crons.=$pim_cron . PHP_EOL;
            }
            if (@$edi_url) {
                $crons = str_replace($edi_cron, "", $crons, $edi_count);
                $crons.=$edi_cron . PHP_EOL;
            }
        }
        $crons = trim($crons);
        file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/bridgecron/crontab.txt', $crons);
        exec("crontab {$_SERVER['DOCUMENT_ROOT']}/bridgecron/crontab.txt");
    }

    /*
     * return the ftp download timing cron if csv upload type is remote
     * it always return the lowest timing if both pim and edi cron are set
     */

    public function getFtpDownloadTiming($edi_cron, $pim_cron) {
        if ($edi_cron == 'never' && $pim_cron == 'never')
            return '';
        else if ($edi_cron == 'never' && $pim_cron != 'never')
            return $pim_cron;
        else if ($pim_cron == 'never' && $edi_cron != 'never')
            return $edi_cron;
        else {
            $pim_cron_value = $this->getCronValue($pim_cron);
            $edi_cron_value = $this->getCronValue($edi_cron);
            $cron = $pim_cron_value < $edi_cron_value ? $pim_cron : $edi_cron;
            return $cron;
        }
    }

    /*
     * set a constant value for cron string
     */

    public function getCronValue($cron) {
        switch ($cron) {
            case "0 * * * *"://hourly
                return 1;
            case "0 0 * * *"://daily
                return 2;
            case "0 0 * * 0"://weekly
                return 3;
            case "0 0 1 * *"://monthly
                return 4;
            default :
                return 0;
        }
    }

    /**
     * update the primary fields of bridge csv data table through cron
     * across channel
     */
    public function update_csv_data_cronAction() {
        $resource = Mage::getSingleton('core/resource');
        $incomingtable = $resource->getTableName('bridge/incomingchannels');
        $csvdatatable = $resource->getTableName('bridgecsv/data');
        $readConnection = $resource->getConnection('core_read');
        $query = "SELECT incoming.id FROM $incomingtable as incoming JOIN $csvdatatable as csv ON incoming.id=csv.channel_id WHERE incoming.status AND ((incoming.product_info && (incoming.create_new || incoming.update_exist)) OR (incoming.product_pricenquantity)) && csv.primary_field_updated=0 AND csv.is_header=0 AND csv.SKU IS NOT NULL LIMIT 0,1;";
        $channel_id = $readConnection->fetchRow($query);

        if ($channel_id)
            $this->update_csv_dataAction($channel_id['id']);
    }

    /**
     * import the csv product into magento store based on condition and malpping
     */
    public function csv_pim_cronAction() {
//get the data to process
        $product_stviews = Mage::getmodel('bridgecsv/data')->getPimDataToProcess();
        $entityTypeId = Mage::getModel('eav/entity')
                ->setType('catalog_product')
                ->getTypeId();
        $edi_update = true;
        $image_update = true;
        if (isset($product_stviews['product'])) {
            $product = $product_stviews['product'];
            unset($product_stviews['product']);
            $edi_update = false;
            $image_update = false;
        }
        if (!$product_stviews) {
            echo "No skus available for process";
            exit;
        }
        $channel_ids = array_unique(array_column($product_stviews, 'channel_id'));
        sort($channel_ids);

//get the channell infos
        $channel_infos = Mage::getmodel('bridge/incomingchannels')->getCollection()
                ->addFieldToFilter('id', $channel_ids)
                ->setOrder('id', 'asc')
                ->getData();

        $channel_infos = array_combine(array_values($channel_ids), $channel_infos);

//get the mapping infos
        $channel_mapping_infos = Mage::getmodel('bridge/incomingchannels_mapping')->getCollection()
                ->addFieldToFilter('brand_id', $channel_ids)
                ->getData();
        $channel_mapping_ids = array_column($channel_mapping_infos, 'brand_id');
        $channel_mapping_infos = array_combine($channel_mapping_ids, $channel_mapping_infos);
        $bridge_attributes = array_flip(Mage::helper('bridge')->getBridgeAttributes());

//get own store options
        $raw_ret_options = Mage::helper('bridgecsv')->getAttributeOptions(0);
        $ret_options = array();
        foreach ($raw_ret_options as $optk => $optv) {
            $ret_options[$optv['attribute_id']][$optv['option_id']] = htmlspecialchars(strtolower($optv['value']), ENT_QUOTES);
        }
//end
//Here to set the website ids
        $websites = Mage::app()->getWebsites();
        $websiteids = array();
        foreach ($websites as $wk => $wv) {
            $websiteids[$wv->getId()] = $wv->getId();
        }
        foreach ($product_stviews as $product_key => $product_data) {
            $mapped_option = array();
            $store_view_map = $product_data['store_view'];
//get the mapping info
            $mapping_data = $channel_mapping_infos[$product_data['channel_id']];
//get the formatted key for csv product
            $product_info = unserialize($product_data['data']);
//            echo "<pre>";
//            print_r($product_info);exit;
            $product_info = Mage::helper('bridgecsv')->getFormattedKey($product_info);
            $product_info['sku'] = $product_data['sku'];


            $mapping_info = unserialize($mapping_data['data'])['import'];
            $channel_infos[$product_data['channel_id']]['others'] = unserialize($channel_infos[$product_data['channel_id']]['others']);
            $mapped_product = Mage::helper('incomingbridgecsv')->getMapped($product_info, $mapping_info, $channel_infos[$product_data['channel_id']], $ret_options, boolval($product));

            if (!$mapped_product) {
                unset($product_stviews[$product_key]);
                Mage::getmodel('bridgecsv/data')->setSkippedStatus($product_data['id']);
                continue;
            }

            $event = $product ? 'content-updated' : 'created';
            if ($event == 'created') {
                $mapped_product['sku'] = trim($product_data['sku']);
                $mapped_product['entity_type_id'] = $entityTypeId;
                $mapped_product['website_ids'] = $websiteids;
                $quantity = $product_data['quantity'] ? $product_data['quantity'] : (isset($mapped_product['fedobe_availability']) && !$mapped_product['fedobe_availability'] ? 0 : (isset($mapped_product['fedobe_quantity']) ? $mapped_product['fedobe_quantity'] : 0));
                $is_in_stock = $quantity > 0 ? 1 : 0;
                $mapped_product['stock_data'] = array('qty' => $quantity, 'is_in_stock' => $is_in_stock);
                $product = Mage::getModel('catalog/product');
                $status = $mapped_product['status'];
                $mapped_product['status'] = 2;
            } else {
                unset($mapped_product['sku']);
            }

            $mapped_product['store_id'] = $store_view_map;
            $mapped_product['bridge_channel_id'] = $product_data['channel_id'];
            echo "<pre>";
            print_r($mapped_product); //exit;

            $product->addData($mapped_product);

            try {
//save the product
                $product->save();
                Mage::helper('incomingbridgecsv')->saveCsvInLog($product->getSku(), $product_data['channel_id'], $event == 'created' ? 'New Product Created' : 'Product Updated');


//keep this product updation track in history table
                $history_data = array('brand_id' => $product_data['channel_id'], 'sku' => $product->getSku(), 'event' => $event, 'store_view' => $product_data['store_view']);
                $history_model = Mage::getmodel('bridge/incomingchannels_history');
                $history_model->setData($history_data);
                $history_model->save();
//save the data in edi
//keep the synced sku price and quantity in edi table
                if ($edi_update) {
                    $price = $product_data['price'] ? $product_data['price'] : ($mapped_product['price'] > 0 ? $mapped_product['price'] : 100000);
                    Mage::getmodel('bridge/incomingchannels_product')->saveSkuInfo($product_data['channel_id'], $channel_infos[$product_data['channel_id']]['currency'], $product->getSku(), $price, $quantity, $channel_infos[$product_data['channel_id']]['slab'], $channel_infos[$product_data['channel_id']]['channel_order']);
//save the product price and quantity if product isnot prvioulsy exist
                    if (is_numeric($channel_infos[$product_data['channel_id']]['currency'])) {
                        $converted_price = $price * $channel_infos[$product_data['channel_id']]['currency'];
                    } else if ($channel_infos[$product_data['channel_id']]['currency']) {
                        $converted_price = Mage::getmodel('bridge/incomingchannels_product')->currency_conv($price, $channel_infos[$product_data['channel_id']]['currency']);
                    } else {
                        $converted_price = $price;
                    }
                    $converted_price = Mage::helper('bridge')->addPriceRule($channel_infos[$product_data['channel_id']], $product->getId(), $converted_price);

                    $product = Mage::getmodel('catalog/product')->load($product->getId());
                    $product->setPrice($converted_price);

//save the default store bridge channel id if store map is not default and product was not exist
                    if ($store_view_map != Mage_Core_Model_App::ADMIN_STORE_ID) {
                        $product->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
                        $product->setBridgeChannelId($product_data['channel_id']);
                    }
                    $product->save();
                    $edi_update = false;
                    Mage::getmodel('bridge/inventory')->changeLiveStatusByProductId(array(0 => array('channel_id' => $product_data['channel_id'], 'product_entity_id' => $product->getId(), 'converted_price' => $converted_price)));
//                        }
//keep this product price updation track in history table
                    $history_data = array('brand_id' => $product_data['channel_id'], 'sku' => $product->getSku(), 'event' => 'price-updated', 'current_price' => $converted_price, 'current_quantity' => $quantity);
                    $history_model = Mage::getmodel('bridge/incomingchannels_history');
                    $history_model->setData($history_data);
                    $history_model->save();
                }

//update the image for this product if its new
                if ($image_update && @$mapped_product['image']) {
                    $image_urls = array_filter(explode(',', $mapped_product['image']));

                    if ($image_urls) {
                        if (Mage::helper('bridgecsv')->addProductGalleryImages($image_urls, $product->getId(), unserialize($channel_infos[$product_data['channel_id']]['others']))) {
                            $image_update = false;
                            if ($edi_update === false) {
                                $product->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
                                $product->setStatus($status);
                                $product->save();
                            }
                        }
                    }
                }
                if ($image_update === true) {
                    Mage::helper('incomingbridgecsv')->saveCsvInLog($product->getSku(), $product_data['channel_id'], "Product Image Not Found");
                }
            } catch (Exception $ex) {
                echo "<pre>";
                print_r($ex->getMessage());
                exit;
            }
        }
//        exit;
//check the condition for content source
        foreach ($product_stviews as $product_data) {
            $data = array();
            $product = Mage::getmodel('catalog/product')->setStoreId($product_data['store_view'])->load($product->getId());
            $contentruledata = Mage::helper('bridge')->getConvertedString(array('id' => $product_data['channel_id']), $product->getId(), 'inc', $product_data['store_view'], 1);
            $action_apply_on = unserialize($contentruledata['apply_on']);
            if (in_array('Own store', $action_apply_on)) {
                if (strcasecmp($channel_infos[$product_data['channel_id']]['content_source'], 'rule_base_original') === 0) {
                    if ($contentruledata['description'])
                        $data['description'] = $contentruledata['description'];
                    if ($contentruledata['short_description'])
                        $data['short_description'] = $contentruledata['short_description'];
                    if ($contentruledata['name'])
                        $data['name'] = $contentruledata['name'];
                } else if (strcasecmp($channel_infos[$product_data['channel_id']]['content_source'], 'inc_rule_base_original') === 0) {

                    $data['name'] = $contentruledata['name'] ? $contentruledata['name'] : $product->getIncName();
                    $data['description'] = $contentruledata['description'] ? $contentruledata['description'] : $product->getIncDescription();
                    $data['short_description'] = $contentruledata['short_description'] ? $contentruledata['short_description'] : $product->getIncShortDescription();
                }
            }
            if (in_array('Outgoing Channel', $action_apply_on)) {
                if ($contentruledata['description'])
                    $data['ogc_description'] = $contentruledata['description'];
                if ($contentruledata['short_description'])
                    $data['ogc_short_description'] = $contentruledata['short_description'];
                if ($contentruledata['name'])
                    $data['ogc_name'] = $contentruledata['name'];
            }
            if ($data) {
                $product->addData($data);
                $product->save();
            }
        }

//mark the record as processed
        Mage::getmodel('bridgecsv/data')->changePimProcessStatus($product->getSku());
//        echo "<pre>";
        print_r($product_stviews);
        exit;
    }

    public function getMappedSku($mapped_data, $product_data) {
//get unmappedcsv product data
        $product_data = Mage::helper('bridgecsv')->getFormattedKey($product_data);
        $attret_val = Mage::helper('bridgecsv')->clean($product_data[$mapped_data['csvattrsetcol']]);

        if ($mapped_data['attributesettype'] == 1) {
            $default_attribute_set = $mapped_data['storefieldset'];
            $mapped_Attribute = $mapped_data['attributes']['all'];
        } else if (isset($mapped_data['attributeset'][$attret_val])) {
            $mapped_Attribute = $mapped_data['attributes'][$attret_val];
        } else {
            $mapped_Attribute = array();
        }
        $mapped_Attribute = array_flip($mapped_Attribute);
        $global_Attribute = array_flip($mapped_data['global']['attribute']);
        return $product_data[$mapped_Attribute['sku']] ? $product_data[$mapped_Attribute['sku']] : ( $product_data[$global_Attribute['sku']] ? $product_data[$global_Attribute['sku']] : false);
    }

    /**
     * update bridgecsv data store the sku,price,qty and store view
     * @param type $channel_id
     * @return type
     */
    function update_csv_dataAction($channel_id = 0) {
//get channel id
        $channel_id = $channel_id ? $channel_id : $this->getRequest()->getParam('id');
        $channel_info = Mage::getmodel('bridge/incomingchannels')
                ->getCollection()
                ->addFieldToFilter('id', $channel_id)
                ->addFieldToFilter(array(
                    'product_info', //attribute_1 with key 0
                    'product_pricenquantity', //attribute_2 with key 1
                        ), array(
                    array('eq' => 1), //condition for attribute_1 with key 0
                    array('eq' => 1), //condition for attribute_2
                ))
                ->getFirstItem()
                ->getData();
        if (!$channel_info) {
            echo "Sorry this channel have pim and edi status off";
            exit;
        }
//parameter to check only those which are not processed or all records for this channel
        $process = $this->getRequest()->getParam('process');

        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        $bridge_csv_table = $resource->getTableName('bridgecsv/data');
        $sql = 'SELECT id,data,price,quantity,store_view,primary_field_updated FROM ' . $bridge_csv_table . ' WHERE channel_id=' . $channel_id . ' AND is_header=0 AND make_out_of_stock=0';
        if ($process != 'all') {
            $sql .= ' AND  primary_field_updated=0';
        }
//        $sql.=" LIMIT 1;";
        $sqlQuery = $readConnection->query($sql);
//get the mapping info
        $mapping_data = Mage::getmodel('bridge/incomingchannels_mapping')->loadAllByBrandId($channel_id)->getData();
        $mapping_info = unserialize($mapping_data[0]['data'])['import'];
        $default_attribute_set = 0;
        $default_store_view_map = false;

//storeview mapping
        if ($mapping_info['storeviewtype'] == 1) {
            $default_store_view_map = $mapping_info['allstoreviewmap'];
        }
//attribute set mapping


        if ($mapping_info['attributesettype'] == 1) {
            $default_attribute_set = $mapping_info['storefieldset'];
            $mapped_Attribute = $mapping_info['attributes']['all'];
            $custom_maping = $mapping_info['custom_maping']['all'];
            $ignore_attributes = $mapping_info['ignored']['attribute']['all'];
        }

        $count = 0;
        $update_query = '';

        while ($row = $sqlQuery->fetch()) {
            $change_status = 0;
            $update_qry = array();
            $log_status = true;
//get formatted csv data
            $product_data = Mage::helper('bridgecsv')->getFormattedKey(unserialize($row['data']));

//map the product_data by mapping data
//attribute set mapping
            if (!$default_attribute_set) {
                $attrset_val = Mage::helper('bridgecsv')->clean($product_data[$mapping_info['csvattrsetcol']]);
                if (isset($mapping_info['attributeset'][$attrset_val])) {
                    $attrset_id = $mapping_info['attributeset'][$attrset_val];
                    $mapped_Attribute = $mapping_info['attributes'][$attrset_val] ? $mapping_info['attributes'][$attrset_val] : array();
                    $custom_maping = $mapping_info['custom_maping'][$attrset_val] ? $mapping_info['custom_maping'][$attrset_val] : array();
                    $ignore_attributes = $mapping_info['ignored']['attribute'][$attrset_val] ? $mapping_info['ignored']['attribute'][$attrset_val] : array();
                    $attrset_map = true;
                } else {
                    $mapped_Attribute = array();
                    $custom_maping = array();
                    $ignore_attributes = array();
                }
            }

//attribute map 
            $mapped_product = Mage::helper('incomingbridgecsv')->map_attribute($product_data, $mapped_Attribute, $mapping_info['global']['attribute'], $ignore_attributes);
//end
//custom mapping     
            $custom_maping = array_diff_key($custom_maping, array_flip($mapped_Attribute));
            if ($custom_maping)
                $mapped_product = array_merge($mapped_product, $custom_maping);

//if sku is not for this record then skip this
            if (!$mapped_product['sku']) {
                $log_status = false;
                continue;
            }
//end
//store view mapping
            if ($default_store_view_map === false) {
                $storeview_val = Mage::helper('bridgecsv')->clean($product_data[$mapping_info['storeviewmappingcolname']]);
                if (isset($mapping_info['storeview_mapping'][$storeview_val]))
                    $store_view_map = $mapping_info['storeview_mapping'][$storeview_val];
                else {
                    $store_view_map = false;
                    $log_status = false;
                    Mage::helper('incomingbridgecsv')->saveCsvInLog($mapped_product['sku'], $channel_id, 'Store View Not Mapped');
                }
            }


//get sku,price,qunatiy,storeview
            $store_view = $default_store_view_map !== false ? $default_store_view_map : $store_view_map;
            $sku = trim($mapped_product['sku']);
            $price = $mapped_product['price'];
            $quantity = isset($mapped_product['fedobe_availability']) && !$mapped_product['fedobe_availability'] ? 0 : ($mapped_product['fedobe_quantity'] ? $mapped_product['fedobe_quantity'] : 0);
//keep price n quantity track
            if (round($row['price'], 2) != round($price, 2) || intval($row['quantity']) != intval($quantity) || $row['primary_field_updated'] == 0) {

                $update_qry['sku'] = "'" . addslashes($sku) . "'";
                $update_qry['quantity'] = $quantity > 0 ? $quantity : 0;
                $update_qry['price'] = $price > 0 ? $price : 100000;
                if ($quantity == 0 && $row['quantity'] > 0)
                    $update_priorty = 1;
                else if ($quantity > 0 && $row['quantity'] <= 0)
                    $update_priorty = 3;
                else if ($price != $row['price'])
                    $update_priorty = 2;
                else
                    $update_priorty = 4;
                $update_qry['priority'] = $update_priorty;
                $update_qry['edi_processed'] = 0;
            }
//keep store view track
            if (!isset($update_qry['sku']))
                $update_qry['sku'] = "'" . addslashes($sku) . "'";
            $update_qry['pim_processed'] = 0;
            $update_qry['store_view'] = $store_view;

            if ($update_qry) {
                $update_qry['primary_field_updated'] = 1;
                $update_qry['last_updated_time'] = 'NOW()';
                $output = implode(', ', array_map(
                                function ($v, $k) {
                            return $k . '=' . $v;
                        }, $update_qry, array_keys($update_qry)
                ));
                $update_query .="UPDATE $bridge_csv_table SET $output WHERE id={$row['id']};";
                if ($log_status)
                    Mage::helper('incomingbridgecsv')->saveCsvInLog($mapped_product['sku'], $channel_id, 'Product Ready To Update');
                $count++;
            }
            if ($count % 500 == 0) {
                $writeConnection->raw_query($update_query);
                $update_query = '';
            }
        }

        if ($update_query) {
            $writeConnection->raw_query($update_query);
        }
        if (isset($_SERVER['argv']) && !empty($_SERVER['argv'])) {
            echo $update_query;
            return;
        } else {
            Mage::getSingleton('adminhtml/session')->addSuccess($count . " records updated");
            echo Mage::app()->getResponse()->setRedirect($_SERVER['HTTP_REFERER']);
            exit;
        }
    }

    function csv_edi_cronAction() {
        if (Mage::getmodel('bridgecsv/ediimport')->processPrioritiesSku(1, 1)) {//next check if any edi inventory changed from non zero to zero with live products
            $message = "Processed non zero to zero with live skus";
// exit;
        } else if (Mage::getmodel('bridgecsv/ediimport')->processPrioritiesSku(2, 1)) {//next check if any price of any inventory is changed with live products
            $message = "Processed price changed  with live skus";
//exit;
        } else if (Mage::getmodel('bridgecsv/ediimport')->processPrioritiesSku(3, 1)) {//next check if any edi inventory changed from  zero to non zero with live products
            $message = "Processed zero to non zero with live skus";
// exit;
        } else if (Mage::getmodel('bridgecsv/ediimport')->processPrioritiesSku(3)) {//next check if any edi inventory changed from  zero to non zero which are not live products
            $message = "zero to non zero with  not live skus";
//exit;
        } else if (Mage::getmodel('bridgecsv/ediimport')->processPrioritiesSku(2)) {//next check if any price of any inventory is changed which are not live
            $message = "Processed price changed  with not live skus";
//exit;
        } else if (Mage::getmodel('bridgecsv/ediimport')->processPrioritiesSku(4)) {//next check all
            $message = "Processed price changed  with non live";
//exit;
        } else if (Mage::getmodel('bridgecsv/ediimport')->processPrioritiesSku(1)) {//next check if any edi inventory changed from non zero to zero with non  live products
            $message = "Processed non zero to zero with non live skus";
//exit;
        } else if (Mage::getmodel('bridgecsv/ediimport')->processNonStockSku()) {//next check if any own store product having empty inventory
            $message = "Processed nonstock skus";
// exit;
        } else {
            $message = "You have no inventory to process";
        }
        echo $message;
        exit;
    }

    /**
     * test ftp connection
     */
    public function testftpAction() {
        $ftp_server = $this->getRequest()->getParam('ftp_url');
        $ftp_user = $this->getRequest()->getParam('ftp_username');
        $ftp_pass = $this->getRequest()->getParam('ftp_password');

// set up a connection or die
        try {
            $conn_id = ftp_connect($ftp_server);

// try to login
            if ($conn_id && @ftp_login($conn_id, $ftp_user, $ftp_pass)) {

// close the connection
                ftp_close($conn_id);
                echo "true";
            } else {
                echo "false";
            }
        } catch (Exception $ex) {
            echo "false";
        }
        exit;
    }

    public function processRemotecsvAction() {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getmodel('bridge/incomingchannels')
                ->getCollection()
                ->addFieldToFilter('id', $id)
                ->addFieldToSelect(array('others', 'id'))
                ->getFirstItem();


        if ($model):
//get the mapping info
            $mapping_data = Mage::getmodel('bridge/incomingchannels_mapping')->loadAllByBrandId($id)->getData();
            $mapping_info = unserialize($mapping_data[0]['data'])['import'];
            if ($mapping_info):
                $credentials = unserialize($model->getOthers());
//download the file through ftp connction
                $file = $this->ftpFileDownload($credentials);
                if ($file)
                    if ($this->autoProcesscsvAction($mapping_info, $file)) {
                        $credentials['upload_csv_file'] = $file;
                        $model->setOthers(serialize($credentials));
                        exit;
                    }
            endif;
        endif;
    }

    public function ftpFileDownload($credentials) {
// define some variables
        $path_folder = explode('/', $credentials['path_to_download_csv_file']);
        $filename = uniqid() . "_" . end($path_folder);
        $filename = $this->checkfileExists($filename);
        $local_file = Mage::helper('bridgecsv')->getUploadDirectory() . $filename;
        $server_file = $credentials['path_to_download_csv_file'];

//-- Connection Settings
        $ftp_server = $credentials['ftp_url']; // Address of FTP server.
        $ftp_user_name = $credentials['ftp_username']; // Username
        $ftp_user_pass = $credentials['ftp_password']; // Password
        try {
// set up basic connection
            $conn_id = ftp_connect($ftp_server);

// login with username and password
            $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
            ftp_pasv($conn_id, TRUE);


// try to download $server_file and save to $local_file
            if (ftp_get($conn_id, $local_file, $server_file, FTP_BINARY)) {
                if ($credentials['include_img_url'] == 2 && $credentials['path_to_download_images_ftp']) {
                    $image_folder = $credentials['path_to_download_images_ftp'];
                    $image_list = ftp_nlist($conn_id, $image_folder);

                    if ($image_list) {
                        $local_image_folder = Mage::helper('bridgecsv')->getImageDirectory() . DS . trim($filename, '.csv');

                        if (!file_exists($local_image_folder)) {
                            mkdir($local_image_folder);
                            chmod($local_image_folder, 0777);
                        }
                        foreach ($image_list as $image) {
                            $imgle_name = explode('/', $image);
                            ftp_get($conn_id, $local_image_folder . DS . end($imgle_name), $image, FTP_BINARY);
                        }
                    }
                }
                echo "Successfully written to $local_file\n";
            } else {
                echo "There was a problem\n";
            }
// close the connection
            ftp_close($conn_id);
        } catch (Exception $ex) {
            return false;
        }
        return $filename;
    }

    public function gridnotificationAction() {
        $this->loadLayout();
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('bridgecsv/adminhtml_incomingchannels_edit_tab_notification_grid')->toHtml()
        );
    }

    /**
     * Export order grid to CSV format
     */
    public function exportLogCsvAction() {
        $fileName = 'log.csv';
        $grid = $this->getLayout()->createBlock('bridgecsv/adminhtml_incomingchannels_edit_tab_notification_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportLogExcelAction() {
        $fileName = 'log.xml';
        $grid = $this->getLayout()->createBlock('bridgecsv/adminhtml_incomingchannels_edit_tab_notification_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

}