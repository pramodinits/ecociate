<?php

require_once Mage::getBaseDir('lib') . "/vendor/google/apiclient/src/Google/autoload.php";

class Fedobe_Bridgecsv_Adminhtml_BridgecsvController extends Mage_Adminhtml_Controller_Action {

    public function _title($msg) {
        parent::_title(($msg) ? $msg : Mage::helper('amazon')->__('Fedobe CSV Outgoing Extension'));
    }

    public function _setActiveMenu() {
        parent::_setActiveMenu('fedobe/bridge/outgoing');
    }

    public function indexAction() {
        $this->_title(Mage::helper('bridge')->__('Manage Outgoing Channels'));
        $this->loadLayout();
        $this->renderLayout();
    }

    public function preDispatch() {
        $bypassaction = array('csvcron', 'file_cron', 'cronroute', 'googledrive_upload', 'store_refreshtoken', 'email_send');
        $current_action = Mage::app()->getRequest()->getActionName();
        if (in_array($current_action, $bypassaction)) {
            return $this;
        } else {
            return parent::preDispatch();
        }
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction() {
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $otgchannel = Mage::getModel('bridge/outgoingchannels')->load($id);
            if ($otgchannel->getProductStatus()) {
                $otgchannel->setBridge(array('product_status' => $otgchannel->getProductStatus()));
            }
        }
        $data = Mage::getSingleton('adminhtml/session')->getChannelData(true);
        if (!empty($data)) {
            $model->setData($data);
            $otgchannel->setData($data);
        }
        Mage::register('channel_condition', $otgchannel);
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Coupon codes grid
     */
    public function conditionsGridAction() {
        echo $this->getLayout()->createBlock('fedobe_bridge_block_adminhtml_outgoingchannels_edit_tab_conditions_grid')->toHtml();
    }

    public function saveAction() { //echo "<pre>";
        $id = $this->getRequest()->getParam('id');
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        if (!$this->_validateFormKey()) {
            if ($id) {
                $this->_redirect('*/*/edit', array('id' => $id));
            } else {
                $this->_redirect('*/*/new', array('id' => $id));
            }
            return;
        }

        if ($this->getRequest()->getPost()) {
            //save outgoing channel mapping data
            $session_security_salt = $id ? "{$id}_outgoing_security_salt" : "outgoing_security_salt";
            $data = $this->getRequest()->getPost();
            $otgchannel = Mage::getModel('bridge/outgoingchannels');
            $otgchannel->load($id);
//            echo "<pre>";
//            print_r($data);exit;
            $data['is_ftp_drive_enabled'] = 0;
            if ($data['others']['export_automatically']) {
                $data['is_ftp_drive_enabled'] = 1;
            }
            $defaul_settings = Mage::helper('bridge')->getDefaultSettings();
            $data['relative_product'] = @array_sum($data['relative_product']);
            $data['others']['store'] = implode(',', $data['others']['store']);
            $pim_cron = $data['others']['pim_cron_job'];
            $data['others'] = isset($data['others']) ? serialize($data['others']) : '';
            $product_ids = '';
            if ($selectedids = $this->getRequest()->getPost('product_id')) {
                $product_ids = Mage::helper('adminhtml/js')->decodeGridSerializedInput($selectedids);
                $product_ids = implode(',', array_keys($product_ids));
            }

            $data['sku_list'] = $product_ids;
            $condition_rules = @$data['rule'];
            $arr = array();
            foreach ($condition_rules['conditions'] as $id => $cdata) {
                $path = explode('--', $id);
                $node = & $arr;
                for ($i = 0, $l = sizeof($path); $i < $l; $i++) {
                    if (!isset($node['conditions'][$path[$i]])) {
                        $node['conditions'][$path[$i]] = array();
                    }
                    $node = & $node['conditions'][$path[$i]];
                }
                foreach ($cdata as $k => $v) {
                    //check for gender attribute array issue
                    if (is_array($v)) {
                        $v = implode(',', $v);
                    }
                    $node[$k] = $v;
                }
            }
            $data['conditions_serialized'] = serialize($arr['conditions'][1]);
            $data['product_status'] = @$data['bridge']['product_status'];
            unset($data['rule']);
            try {
                $data['name'] = $data['channel_name'];
                $product_info = $otgchannel->getProductInfo();
                $content_source = $otgchannel->getContentSource();
                $include_img_url = unserialize($otgchannel->getOthers())['include_img_url']; //print_r($include_img_url);exit;
                $filter_by = $otgchannel->getFilterBy();
                $multiprice_rule = $otgchannel->getMultipriceRule();
                $prev_condition = $otgchannel->getConditionsSerialized();
                $price_source = $otgchannel->getPriceSource();
                $saved_others = unserialize($otgchannel->getOthers());
                $otgchannel->setData($data);
                $otgchannel->save();
                $cron_str = Mage::helper('bridgecsv')->setCronSchedule();
                //update edi cron table for update convert price
                if (!$data['id']) {
                    $edicrondata['channel_id'] = $otgchannel->getId();
                    $edicrondata['channel_type'] = "outgoing"; //$data['channel_type'];
                    $edicrondata['status'] = $data['channel_status'];
                    $cronmodel = Mage::getModel('bridge/bridgeapi_edichannelcron')->setData($edicrondata)->save();
                } else {
                    $edicrondata = array("status" => $data['channel_status']);
                    if (($data['filter_by'] == 'condition' && unserialize($prev_condition) !== $arr['conditions'][1]) || ($otgchannel->getFilterBy() != $data['filter_by']) || ($data['filter_by'] == 'sku' && array_filter(explode(',', $data['sku_list'])) != array_filter(explode(',', $otgchannel->getSkuList())))) {
                        $edicrondata['condition_changed'] = 1;
                    }
                    $resource = Mage::getSingleton('core/resource');
                    $write = $resource->getConnection('core_write');
                    $where = "channel_id = {$otgchannel->getId()} AND channel_type ='outgoing'";
                    $write->update("bridge_edi_channelcron", $edicrondata, $where);
                }
                //end
                if ($data['id']) {
                    //update the edi cron table as condition changes
                    $bridge_edi_crontable = Mage::getSingleton('core/resource')->getTableName('bridge/bridgeapi_edichannelcron');
                    $edi_channel = $readConnection->fetchAll("SELECT COUNT(*) FROM " . $bridge_edi_crontable . " WHERE channel_id=" . $data['id']);
                    if (!$edi_channel[0]['COUNT(*)']) {
                        $insert_edi = 'INSERT INTO ' . $bridge_edi_crontable . '(channel_id,channel_type,status,is_processed,condition_changed) VALUES (' . $data['id'] . ',"outgoing",' . $data['channel_status'] . ',' . '0,1)';
                    } else {
                        $insert_edi = 'UPDATE ' . $bridge_edi_crontable . ' SET status= ' . $data['channel_status'] . ' WHERE channel_id= ' . $data['id'];
                    }
                    //echo $insert_edi;exit;
                    $writeConnection->query($insert_edi);
                   
                    $ids = array();
                    //start comment
                    /*if ($data['filter_by'] == 'sku' && !empty($data['sku_list'])) {
                        $id_status = $ids = explode(',', $data['sku_list']);
                    } else if ($data['filter_by'] == 'condition') {
                        $conditions_serialized = $data['conditions_serialized']; //print_r($conditions_serialized);exit;
                        $id_status = $ids = Mage::getmodel('bridge/rule')->getMatchingProductIds($conditions_serialized); //print_r($id_status);exit;
                        $product_status = $data['product_status'];
                        if ($product_status) {
                            $id_status = Mage::getmodel('bridge/outgoingchannels')->getFilteredSkus($ids, $product_status);
                        }
                    }// print_r(array_unique($id_status));exit;
                    $i = 0;
                    $entity_id_arr = array();
                    $table = Mage::getSingleton('core/resource')->getTableName('bridgecsv/csvogcdata');
                    $sql_select = $readConnection->fetchAll('SELECT entity_id FROM ' . $table . ' WHERE bridge_channel_id=' . $data['id']);
                    $sql_select_id = array_column($sql_select, 'entity_id');
                    $ids_new = array_diff($id_status, $sql_select_id);
                    //print_r($ids_new);exit;
                    if (!empty($ids_new)) {
                        $entity_id_str = implode(',', $ids_new);
                        //echo 'SELECT product_entity_id FROM `bridge_incomingchannel_product_info` WHERE product_entity_id IN ('.$entity_id_str.') AND is_processed IN ("3","4","1","0") ORDER BY FIELD(is_processed, '3', '0', '-1', '2', '-2', '1', '4' ),last_updated_time DESC';exit;
                        $sort_product = $readConnection->fetchAll('SELECT product_entity_id,is_processed FROM `bridge_incomingchannel_product_info` WHERE product_entity_id IN (' . $entity_id_str . ') AND is_processed IN ("3","4","-1","-2","1","0") AND channel_id= ' . $data['id'] . ' AND channel_type="outgoing" ORDER BY FIELD(is_processed,"3","4","-1","-2","1","0"),last_updated_time DESC'); //echo "<pre>";
//echo 'SELECT product_entity_id,is_processed FROM `bridge_incomingchannel_product_info` WHERE product_entity_id IN ('.$entity_id_str.') AND is_processed IN ("3","4","-1","-2","1","0") AND channel_id= '.$data['id'].' AND channel_type="outgoing" ORDER BY FIELD(is_processed,"3","4","-1","-2","1","0"),last_updated_time DESC';exit;
                        $id_status_2 = array_column($sort_product, 'product_entity_id');
                        //print_r(array_diff($id_status,$id_status_2));
                        //print_r(count(array_diff($id_status,$id_status_2)));exit;
                        if (!empty($sort_product)) {
                            foreach ($sort_product as $k => $v) {
                                $entity_id_arr[$i]['entity_id'] = $v['product_entity_id'];
                                $entity_id_arr[$i]['bridge_channel_id'] = $data['id'];
                                //$entity_id_arr[$i]['ediorder'] = $k;
                                $i++;
                            }
                        }
                        if (!empty($entity_id_arr))
                            $write->insertMultiple($table, $entity_id_arr);
                       //$sql_select_status = $readConnection->fetchAll('SELECT entity_id FROM ' . $table . ' WHERE bridge_channel_id=' . $data['id']);
                       //   $id_status_ar = array_column($sql_select_status,'entity_id');
                       //   Mage::helper('bridgecsv')->sortids($data['id'],implode(',',$id_status_ar));
                    }
                    $ignore_ids = array_diff($sql_select_id, $id_status); //print_r($ignore_ids);exit;
                    if (!empty($ignore_ids)) {
                        //$writeConnection->query('DELETE FROM ' . $table . ' WHERE entity_id IN (' . implode(',', $ignore_ids) . ') AND bridge_channel_id=' . $data['id']);
                        $writeConnection->query('UPDATE ' . $table . ' SET is_ignored=1 WHERE entity_id IN (' . implode(',', $ignore_ids) . ') AND bridge_channel_id=' . $data['id']);
                    }
                    $common_ids = array();
                    $common_ids = array_intersect($sql_select_id, $id_status);
                     if (!empty($common_ids)) {
                        //$writeConnection->query('DELETE FROM ' . $table . ' WHERE entity_id IN (' . implode(',', $ignore_ids) . ') AND bridge_channel_id=' . $data['id']);
                        $writeConnection->query('UPDATE ' . $table . ' SET is_ignored=0 WHERE entity_id IN (' . implode(',', $common_ids) . ') AND bridge_channel_id=' . $data['id']);
                    }
                     *  end comment
                     */
                    //echo $data['include_img_url'];exit;
                    //print_r($data);exit;
                    //echo unserialize($data['others'])['include_img_url'].'--'.$include_img_url;exit;
                    $table = Mage::getSingleton('core/resource')->getTableName('bridgecsv/csvogcdata');
                    $sql_processed_count = $readConnection->fetchAll('SELECT count(*) FROM ' . $table . ' WHERE bridge_channel_id=' . $data['id']);
                    if($sql_processed_count){
                            $form_others = unserialize($data['others']);
                            $saved_others_mapping['currency'] = $saved_others['currency'];
                            $saved_others_mapping['store'] = $saved_others['store'];
                            $saved_others_mapping['slno_in_csv'] = $saved_others['slno_in_csv'];
                            $saved_others_mapping['root_cat_in_csv'] = $saved_others['root_cat_in_csv'];
                            $saved_others_mapping['csvfields'] = $saved_others['csvfields'];

                            $diff = array();
                            $diff = array_diff_assoc($saved_others_mapping, $form_others); //echo $pim_cron;exit;
                            if($pim_cron=='0 * * * *')//hourly
                                $hr = 1;
                            else if($pim_cron=='0 0 * * *')//daily
                                $hr = 24;
                            else if($pim_cron=='0 0 * * 0')//weekly
                                $hr = 168;
                            else if($pim_cron=='0 0 1 * *')//monthly
                                $hr = 720;
                            if (($price_source != $data['price_source']) || ($data['multiprice_rule'] != $multiprice_rule) || ($data['filter_by'] != $filter_by) || (unserialize($data['others'])['include_img_url'] != $include_img_url) || ($data['product_info'] != $product_info) || !empty($diff) || ($content_source != $data['content_source'])) {
                                $writeConnection->query('UPDATE ' . $table . ' SET is_condition_changed=1, updated_at = DATE_SUB(updated_at, INTERVAL '.$hr.' HOUR), is_processed=0 WHERE bridge_channel_id=' . $data['id']);//inorder to start the cron immidiately
                               // Mage::helper('bridgecsv')->addcsvcron($data['id']);
                            }
                    }
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('bridge')->__('The Channel has been saved.')
                );
                Mage::getSingleton('adminhtml/session')->setChannelData(false);
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->setChannelData($data);
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/');
                return;
            }
        }
// The following line decides if it is a "save" or "save and continue"
        if ($this->getRequest()->getParam('back')) {
            $this->_redirect('*/*/edit', array('id' => $otgchannel->getId()));
        } else {
            $this->_redirect('*/*/');
        }
    }

    public function generateAction() {
        if (!$this->getRequest()->isAjax()) {
            $this->_forward('noRoute');
            return;
        }
        if ($this->getRequest()->getPost()) {
            $data = $this->getRequest()->getPost();
//Here let add the rules data
            $condition_rules = $data['rule'];
            foreach ($condition_rules['conditions'] as $id => $cdata) {
                $path = explode('--', $id);
                $node = & $arr;
                for ($i = 0, $l = sizeof($path); $i < $l; $i++) {
                    if (!isset($node['conditions'][$path[$i]])) {
                        $node['conditions'][$path[$i]] = array();
                    }
                    $node = & $node['conditions'][$path[$i]];
                }
                foreach ($cdata as $k => $v) {
//check for gender attribute array issue
                    if (is_array($v)) {
                        $v = implode(',', $v);
                    }
                    $node[$k] = $v;
                }
            }
            $conditions_serialized = serialize($arr['conditions'][1]);
        }
        if ($this->getRequest()->getParam('id'))
            $skus = Mage::getmodel('bridge/rule')->getMatchingProductIds($conditions_serialized);
        else {
            $otgchannel = Mage::getModel('bridge/outgoingchannels');
            $otgchannel->setConditionsSerialized($conditions_serialized);
            Mage::register('channel_condition', $otgchannel);
            $skus = Mage::getmodel('bridge/rule')->getMatchingProductIds();
        }
        $product_status = @$data['bridge']['product_status'];
        if ($product_status) {
            $skus = Mage::getmodel('bridge/outgoingchannels')->getFilteredSkus($skus, $product_status);
        }
        $this->_getSession()->setData('channel_condition_skus', $skus);
    }

    public function saveSkuAction() {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $channel_id = Mage::getModel('bridge/outgoingchannels')->loadByConsumerId($id)->getId();
                $model = Mage::getModel('bridge/outgoingchannels');
                $model->setId($channel_id);
                $model->setSkuList(implode(',', $this->getRequest()->getPost('ids')));
                $model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('bridge')->__('The sku has been saved.'));
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                        Mage::helper('bridge')->__('An error occurred while saving the sku. Please review the log and try again.'));
                Mage::logException($e);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('bridge')->__('Sorry you have not added the channel'));
        return;
    }

    /**
     * Get form data
     *
     * @return array
     */
    protected function _getFormData() {
        return $this->_getSession()->getData('consumer_data', true);
    }

    /**
     * Set form data
     *
     * @param $data
     * @return Mage_Oauth_Adminhtml_Oauth_ConsumerController
     */
    protected function _setFormData($data) {
        $this->_getSession()->setData('consumer_data', $data);
        return $this;
    }

    /**
     * return user information by ajax call
     */
    public function userinfoAction() {
        $user_id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('admin/user')->load($user_id)->getData();
        print_r(json_encode($model));
        exit;
    }

    /**
     * get the list of users
     */
    public function urlunique_validationAction() {
        $exist = Mage::getmodel('bridge/outgoingchannels')->checkUniqueUrl($this->getRequest()->getParam('id'), $this->getRequest()->getParam('url'));
        echo $exist;
        exit;
    }

    public function checkaddonAction() {
        $channel_type = $this->getRequest()->getParam('channel_type');
        if ($channel_type != 'magento') {
            $module = 'Fedobe_' . ucfirst($channel_type);
            $addon_enabled = Mage::helper('core')->isModuleEnabled($module);
            if ($addon_enabled) {
                $this->_redirect('adminhtml/' . $channel_type . '/edit/channel_type/' . $channel_type);
            } else {
                //Here to check the decsription of the corresponding sales description
                Mage::register('addon', $channel_type);
                $this->loadLayout();
                $this->renderLayout();
            }
        } else {
            $this->_redirect('*/*/edit/channel_type/magento');
        }
    }

    public function generatesecuritykeyAction() {
        echo md5(uniqid());
    }

    public function exportdataAction() {
        $channel_id = $this->getRequest()->getParam('id');
        $exportformat = $this->getRequest()->getParam('exportformat');
        if ($channel_id) {
            $channelobj = Mage::getmodel('bridge/outgoingchannels')->load($channel_id);
            $productids = Mage::getmodel('bridge/rule')->getMatchingProductIds($channelobj->getData('conditions_serialized'));
            if ($exportformat == 'csv') {
                $this->exportDataAsCsv($productids, $channelobj, 0);
            } else {
                $this->exportDataAsXml($productids, $channelobj, 0);
            }
        }
    }

    public function exportDataAsXml($productids, $channelobj, $settingscheck = 1) {
        if ($productids) {
            $xmldata = $this->getFormattedXmlData($productids, $channelobj, $settingscheck);
            $filename = $channelobj->getName() . '-' . date("Y-m-d-h-i-s") . ".xml";
            header('Content-type: text/csv; charset=UTF-8');
            header('Content-Disposition: attachment; filename="' . $filename . '"');
            $xmlheader = <<<EOD
<?xml version="1.0" encoding="UTF-8"?>
<products>
EOD;
            $xmlfooter = <<<EOD
            
</products>
EOD;
            echo $xmlheader;
            echo $xmldata;
            echo $xmlfooter;
        }
    }

    public function exportDataAsCsv($productids, $channelobj, $settingscheck = 1) {
        if ($productids) {
            echo "<pre>";
            print_r($productids);
            echo "</pre>";
            $csvdata = $this->getFormattedCsvData($productids, $channelobj, $settingscheck);
            $filename = $channelobj->getName() . '-' . date("Y-m-d-h-i-s") . ".csv";
            header('Content-type: text/xml; charset=UTF-8');
            header('Content-Disposition: attachment; filename="' . $filename . '"');
            echo $csvdata;
        }
    }

    public function getFormattedCsvData($productids, $channelobj, $settingscheck) {
        $attributetofetch = $this->getAllAttributeList($productids, $channelobj);
        if ($settingscheck) {
            
        }
        echo "<pre>";
        print_r($attributetofetch);
        echo "</pre>";
    }

    public function getFormattedXmlData($productids, $channelobj, $settingscheck) {
        $othersdata = $channelobj->getData('others');
        $exportsetting = unserialize($othersdata);
        $attributetofetch = $this->getAllAttributeList($productids, $channelobj);
        if ($settingscheck) {
            
        }
        $get_related = $get_crossell = $get_upsell = 0;
        if (($channelobj->getData('relative_product') & 4))
            $get_related = 1;
        if (($channelobj->getData('relative_product') & 2))
            $get_upsell = 1;
        if (($channelobj->getData('relative_product') & 1))
            $get_crossell = 1;
        //Here to get condition matched related,crosssell and upsell products
        $strictcheck = $channelobj->getData('cond_apply_on_relative_products');

        $xmldatastring = "";
        $storename = array();
        $storeinfo = explode(',', $exportsetting['store']);
        $storeinfo = (!empty($storeinfo)) ? $storeinfo : array(Mage_Core_Model_App::ADMIN_STORE_ID);
        foreach ($storeinfo as $sk => $store_id) {
            $store = Mage::getModel('core/store')->load($store_id);
            $storename[$store_id] = $store->getName();
        }
        $sl_no = 1;
        //Here let's iterate all product ids to get the 
        foreach ($productids as $k => $product_id) {
            //Here to fetch the store specific data
            foreach ($storeinfo as $sk => $store_id) {
                $xmldatastring .= $this->getXmlProductDetails($product_id, $store_id, $attributetofetch, $channelobj, $sl_no, $parent_sku = '', $type = '', $storename);
//                if($get_related){
//                    $relatedids = $product->getRelatedProductIds();
//                    //Here to check for exact condition match settings
//                }
//                if($get_upsell){
//                    $updellids = $product->getUpSellProductIds();
//                }
//                if($get_crossell){
//                    $crossellids = $product->getCrossSellProductIds();
//                }
            }
            $sl_no++;
        }
        exit;
    }

    public function getXmlProductDetails($product_id, $store_id, $attributetofetch, $channelobj, $sl_no, $parent_sku = '', $type = '', $storename) {
        $productdata = $this->getProdcutData($product_id, $store_id, $attributetofetch, $channelobj, $sl_no, $parent_sku = '', $type = '', $storename);
        echo "<pre>";
        print_r($productdata);
        echo "</pre>";
    }

    public function getProdcutData($product_id, $store_id, $attributetofetch, $channelobj, $sl_no, $parent_sku = '', $type = '', $storename) {
        $productdata = array();
        $product = Mage::getModel('catalog/product')->setStoreId($store_id)->load($product_id);
        $othersdata = $channelobj->getData('others');
        $exportsetting = unserialize($othersdata);
        $targetcur = ($exportsetting['currency'] == 'custom') ? Mage::app()->getStore()->getBaseCurrencyCode() : $exportsetting['currency'];
        $customvalue = $exportsetting['out_currency_val'];
        foreach ($attributetofetch as $k => $attribute_code) {
            $flag = 1;
            //Here if there is Store field settings 
            if ($attribute_code == 'language') {
                $flag = 0;
                $productdata[$attribute_code] = $storename[$store_id];
            }
            //End
            //Here if there is currencycode field settings 
            if ($attribute_code == 'currencycode') {
                $flag = 0;
                $productdata[$attribute_code] = $targetcur;
            }
            //End
            //Here if there is SL No field settings 
            if ($attribute_code == "sl_no") {
                $flag = 0;
                $productdata[$attribute_code] = $sl_no;
            }
            //End
            //Here to convert the price for all price attributes
            $pattern = '/price/i';
            if (preg_match($pattern, $attribute_code, $match)) {
                $flag = 0;
                $productdata[$attribute_code] = $this->convertCurrency($product, $attribute_code, $targetcur, $customvalue);
                if ($attribute_code == 'price') {
                    $productdata[$attribute_code] = Mage::helper('bridge')->addPriceRule($channelobj->getData(), $product_id, $productdata[$attribute_code], $type = "out");
                }
            }
            //End
            //Here if root category added to data
            if ($attribute_code == 'attrset_id') {
                $flag = 0;
                $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
                $attributeSetModel->load($product->getAttributeSetId());
                $productdata[$attribute_code] = $attributeSetModel->getAttributeSetName();
            }
            //End
            //Here if Related,Crosssel and upsell added to data
            if ($attribute_code == 'parent_sku') {
                $flag = 0;
                $productdata[$attribute_code] = $parent_sku;
            }
            if ($attribute_code == 'type') {
                $flag = 0;
                $productdata[$attribute_code] = $type;
            }
            //End
            //Here if category ids added to listing
            if ($attribute_code == 'category_ids') {
                $flag = 0;
                $tempcat = array();
                $cats = $product->getCategoryIds();
                foreach ($cats as $category_id) {
                    $_cat = Mage::getModel('catalog/category')->load($category_id);
                    $tempcat[] = $_cat->getName();
                }
                $productdata[$attribute_code] = implode('|', $tempcat);
                unset($tempcat);
            }
            //End
            if ($flag) {
                $productdata[$attribute_code] = $product->getResource()->getAttribute($attribute_code)->getFrontend()->getValue($product);
            }
        }
        return $productdata;
    }

    public function convertCurrency($product, $attribute_code, $currencycode, $customvalue) {
        $currenctcurrencycode = Mage::app()->getStore()->getBaseCurrencyCode();
        $targetcurrencycode = $currencycode;
        $priceattrval = $product->getData($attribute_code);
        if ($currenctcurrencycode == $targetcurrencycode) {
            $finalval = round($priceattrval * $customvalue, 2);
        } else {
            $priceOne = Mage::helper('directory')->currencyConvert($priceattrval, $currenctcurrencycode, $targetcurrencycode);
            $finalval = round($priceOne, 2);
        }
        return $finalval;
    }

    public function getAllAttributeList($productids, $channelobj) {
        $othersdata = $channelobj->getData('others');
        $exportsetting = unserialize($othersdata);
        $attributetofetch = ($exportsetting['csvfields']) ? explode(',', $exportsetting['csvfields']) : $this->getCompulsoryAttributes();
        //Here to add store code
        array_unshift($attributetofetch, "currencycode");
        if ($exportsetting['store'])
            array_unshift($attributetofetch, "language");
        //Here to add parent sku and type whether Related,Cross sell or Upsell
        if ($channelobj->getData('relative_product'))
            array_unshift($attributetofetch, "parent_sku", "type");
        //Here to add subcategory    
        if ($exportsetting['sub_cat_in_csv']) {
            array_unshift($attributetofetch, "category_ids");
        }
        //Here to add Root category i.e Attribute set name    
        if ($exportsetting['root_cat_in_csv']) {
            array_unshift($attributetofetch, "attrset_id");
        }
        //Here to add serial number to xml
        if ($exportsetting['slno_in_csv']) {
            array_unshift($attributetofetch, "sl_no");
        }
        return $attributetofetch;
    }

    public function getCompulsoryAttributes() {
        return array('name', 'sku', 'price');
    }

    public function csvcronAction() {
        ini_set('max_execution_time', 0);
        ini_set("memory_limit", "256M");
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        if (isset($_SERVER['argv']) && !empty($_SERVER['argv'])) { //in case we need to create a specific channel's product by cron
            parse_str($_SERVER['argv'][0], $arr);
            $debug = var_export($arr, true);
            $bridge_id = $arr['id'];
        } else if (!empty($this->getRequest()->getParam('id'))) {
            $bridge_id = $this->getRequest()->getParam('id'); //$channelres[0]['id'];
        }
        if ($bridge_id) {
//        $_product_ar = Mage::getModel('catalog/product')->setStoreId(2)->load(499);
//        echo $_product_ar->getName();exit;
            $table = Mage::getSingleton('core/resource')->getTableName('bridgecsv/csvogcdata');
            $sql_select_res = $readConnection->fetchAll('SELECT * FROM ' . $table . ' WHERE bridge_channel_id = ' . $bridge_id . ' AND is_processed=0 limit 10');// AND is_ignored!=1
            $availablestores = Mage::helper('bridgecsv')->getstores(); //print_r($sql_select_res);exit;
            $availablestores_1 = array_combine(array_column($availablestores, 'store_id'), array_column($availablestores, 'code'));
            //print_r($sql_select_res);exit;
            if (!empty($sql_select_res)) {
                $i = 0;
                $insert = array();
                $cat_sql = $readConnection->fetchALl("select cat.entity_id, cv.value as `category_name` from `catalog_category_entity` as cat
                                        join `catalog_category_entity_varchar` as cv on cat.entity_id = cv.`entity_id`
                                        join `eav_attribute` as att on att.`attribute_id` = cv.`attribute_id`
                                        join `eav_entity_type` as aty on att.`entity_type_id` = aty.`entity_type_id`
                                        where aty.`entity_model` = 'catalog/category' and att.`attribute_code` = 'name' and cv.`store_id` = 0");

                $categories = array_combine(array_column($cat_sql, 'entity_id'), array_column($cat_sql, 'category_name')); //exit;
                foreach ($sql_select_res as $k => $v) {
                    $insert[$i]['entity_id'] = $v['entity_id'];
                    $insert[$i]['bridge_channel_id'] = $v['bridge_channel_id'];
                    $str_1 = array();
                    $other_csv_arr = array();
                    $other_csv_tag_arr = array();
                    $csvfields_arr = array();
                    $model = Mage::getModel('bridge/outgoingchannels');
                    $model->load($v['bridge_channel_id']);
                    $data = $model->getData();
                    $str = '';
                    $slno = array();
                    $others = unserialize($data['others']); //print_r($others);exit;
                    $store = explode(',', $others['store']);
                    $content_source = $data['content_source'];
                    $product_info = $data['product_info'];

                    //                if ($others['slno_in_csv']) {
                    //                    array_push($slno, 'serial no');
                    //                }
                    if ($others['csvfields'])
                        $other_csv_arr = explode(',', $others['csvfields']);

                    //print_r($other_csv_arr);exit;
                    $optional_csv_attr = array();
                    if ($others['root_cat_in_csv']) {
                        array_push($optional_csv_attr, 'category');
                    }
                    if ($others['currency'] != 'custom') {
                        array_push($optional_csv_attr, 'currency');
                        array_push($optional_csv_attr, 'currencysymbol');
                        $currency = $others['currency'];
                    }//echo $currency;exit;
                    if ($others['include_img_url']) {
                        $imgurl = array();
                        $product_1 = Mage::getModel('catalog/product')->load($v['entity_id']);
                        $img_u = $product_1->getImageUrl();
                        $img_u_arr = array_reverse(explode('/',$img_u));
                        $img_u_str = $img_u_arr[2].'/'.$img_u_arr[1].'/'.$img_u_arr[0];
                        $imgurl[0] = $img_u_str;//$img_u;
                        array_push($other_csv_arr, 'image_url_1');
                        array_push($other_csv_arr, 'image_url_2');
                        array_push($other_csv_arr, 'image_url_3');
                        array_push($other_csv_arr, 'image_url_4');
                        array_push($other_csv_arr, 'image_url_5');
                        $image_arr = $product_1->getMediaGalleryImages();
                        foreach ($image_arr as $image) {
                            $match_u = $image->getUrl();
                            $match_u_arr = array_reverse(explode('/', $match_u));
                            $match_img_u_str = $match_u_arr[2] . '/' . $match_u_arr[1] . '/' . $match_u_arr[0]; //echo $match_img_u_str."<br>";
                            if ($imgurl[0] == $match_img_u_str) {
                                $imgurl[0] = $match_u;
                            } else {
                                $imgurl[] = $image->getUrl();
                            }
                        }
                    }
                    $other_csv_arr = array_merge($other_csv_arr, $optional_csv_attr); //print_r($store);exit;
                   foreach ($store as $k4 => $v4) {
                        foreach ($other_csv_arr as $k5 => $v5) {
                            if(count($store)>1)
                                $other_csv_tag_arr[] =  $v5 . '_storev' . $availablestores_1[$v4];
                            else
                                $other_csv_tag_arr[] =  $v5 . '_storev';
                        }
                    }
                    $csvfields_arr = array_merge($slno, $other_csv_tag_arr); //print_r($csvfields_arr);exit;
                    foreach ($csvfields_arr as $k1 => $v1) {
                        $v1_store = explode('_storev', $v1);
                        str_replace('image_url_1', '', $v1_store[0], $count_img1);
                        str_replace('image_url_2', '', $v1_store[0], $count_img2);
                        str_replace('image_url_3', '', $v1_store[0], $count_img3);
                        str_replace('image_url_4', '', $v1_store[0], $count_img4);
                        str_replace('image_url_5', '', $v1_store[0], $count_img5);
                        $v1 = str_replace('_storev', '_', $v1);
                        $v1 = ucfirst(str_replace('_', ' ', $v1));
                        $store_id = array_flip($availablestores_1)[$v1_store[1]]; //echo $store_id."<br>";
                        $product = Mage::getModel('catalog/product')->setStoreId($store_id)->load($v['entity_id']);
                        
                        //echo $product->getName();exit;
                        //print_r($product->getData());exit;
                        if ($product->getResource()->getAttribute($v1_store[0])) {
                            //$cpBlock = $this->getLayout()->getBlockSingleton('fedobe_bridge_block_adminhtml_managebridgecontent_widget_button');
                            //echo $this->__getFrontendProductUrl($product);exit;
                            if ((!$product_info && in_array($v1_store[0], array('name', 'price', 'sku'))) || $product_info) {

                                $val = $product->getResource()->getAttribute($v1_store[0])->setStoreId($store_id)->getFrontend()->getValue($product);
                                if($val=='No'){
                                    $val = '';
                                }
                                str_replace('price', '', strtolower($v1), $price_cnt); //echo $v1_store[0].'--'.$price_cnt."<br>";
                                str_replace('url_path', '', $v1, strtolower($url_cnt)); //echo $v1_store[0].'--'.$price_cnt."<br>";
                                //echo $url_cnt;exit;
                                if ($price_cnt) {
                                    $val = 'calculate';
                                }
                                if ($url_cnt) {
                                    $val = $this->__getFrontendProductUrl($product);
                                }
                                //print_r($val);exit;
                                if (in_array($v1_store[0], array('name', 'description', 'short_description'))) {
                                    $contentruledata = Mage::helper('bridge')->getConvertedString(array('id' => $bridge_id), $v['entity_id'], 'ogc', $v['store_id']);
                                    if ($v1_store[0] == 'name' && $contentruledata['name']) {
                                        if ($content_source == 'contentrule')
                                            $val = trim($contentruledata['name']);
                                        else if ($content_source == 'copy')
                                            $val = trim($product->getOgcName());
                                        else if ($content_source == 'original')
                                            $val = trim($product->getName());
                                        else if ($content_source == 'contentrule_or_copy')
                                            $val = ($contentruledata['name']) ? $contentruledata['name'] : $product->getOgcName();
                                        else if ($content_source == 'copy_or_contentrule')
                                            $val = ($product->getOgcName()) ? $product->getOgcName() : $contentruledata['name'];
                                    }if ($v1_store[0] == 'description' && $contentruledata['description']) {
                                        if ($content_source == 'contentrule')
                                            $val = ($contentruledata['description']) ? $contentruledata['description'] : $product->getDescription();
                                        else if ($content_source == 'copy')
                                            $val = $product->getOgcDescription();
                                        else if ($content_source == 'original')
                                            $val = $product->getDescription();
                                        else if ($content_source == 'contentrule_or_copy')
                                            $val = ($contentruledata['description']) ? $contentruledata['description'] : $product->getOgcDescription();
                                        else if ($content_source == 'copy_or_contentrule')
                                            $val = ($product->getOgcDescription()) ? $product->getOgcDescription() : $contentruledata['description'];
                                    }if ($v1_store[0] == 'short_description' && $contentruledata['short_description']) {
                                        if ($content_source == 'contentrule')
                                            $val = $contentruledata['short_description'];
                                        else if ($content_source == 'copy')
                                            $val = $product->getOgcShortDescription();
                                        else if ($content_source == 'original')
                                            $val = $product->getShortDescription();
                                        else if ($content_source == 'contentrule_or_copy')
                                            $val = ($contentruledata['short_description']) ? $contentruledata['short_description'] : $product->getOgcShortDescription();
                                        else if ($content_source == 'copy_or_contentrule')
                                            $val = ($product->getOgcShortDescription()) ? $product->getOgcShortDescription() : $contentruledata['short_description'];
                                    }
                                }
                                $str_1[$v1] = addslashes(str_replace("\r\n", "", strip_tags($val)));
                            }
                        } else if ($v1_store[0] == 'category') {
                            $cat_path_str = array();
                            foreach ($product->getCategoryCollection() as $category) {
                                $cat_path_arr_nm = array();
                                $cat_path_arr = explode('/', $category->getData()['path']);
                                foreach ($cat_path_arr as $k6 => $v6) {
                                    if (isset($categories[$v6]))
                                        $cat_path_arr_nm[] = $categories[$v6];
                                }
                                $cat_path_str[] = implode('/', $cat_path_arr_nm);
                            }
                            if (!empty($cat_path_str)) {
                                $str_1[$v1] = implode("|", $cat_path_str);
                            } else {
                                $str_1[$v1] = '';
                            } //print_r($str_1[$v1]);//exit;
                        } else if ($v1_store[0] == 'currency') {
                            $str_1[$v1] = $currency;
                        } else if ($v1_store[0] == 'currencysymbol') {
                            $str_1[$v1] = Mage::app()->getLocale()->currency($currency)->getSymbol(); //echo $str_1[$v1];exit;
                        } else if ($count_img1) {
                            $str_1[$v1] = (isset($imgurl[0])) ? $imgurl[0] : "";
                        }else if ($count_img2) {
                            $str_1[$v1] = (isset($imgurl[1])) ? $imgurl[1] : "";
                        }else if ($count_img3) {
                            $str_1[$v1] = (isset($imgurl[2])) ? $imgurl[2] : "";
                        }else if ($count_img4) {
                            $str_1[$v1] = (isset($imgurl[3])) ? $imgurl[3] : "";
                        }else if ($count_img5) {
                            $str_1[$v1] = (isset($imgurl[4])) ? $imgurl[4] : "";
                        } else {
                            $str_1[$v1] = '';
                        }
                    }//exit;
                    //echo "<pre>";
                    //print_r($str_1);exit;
                    $product_attr_set = $product->getAttributeSetId();
                    $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
                    $attributeSetModel->load($product_attr_set);
                    $attributeSetName = $attributeSetModel->getAttributeSetName();
                    $str_1['attribute_set'] = $attributeSetName;
                    $insert[$i]['mapping_data'] = serialize($str_1);
                    $insert[$i]['is_processed'] = 1;
                    $i++;
                }
                Mage::getmodel('bridgecsv/csvogcdata')->saveogc($insert);
                exit;
            } else {
                //delete cron.
                $writeConnection->query('UPDATE ' . $table . ' SET is_processed=0 ,is_condition_changed=0,is_cron_run=0 WHERE bridge_channel_id=' . $bridge_id);
                //exit;
                Mage::helper('bridgecsv')->removecron($bridge_id);
                echo 'All product processed for this channel';
            }
        }
    }

    public function file_cronAction($bridge_id = NULL, $type = NULL, $is_drive = NULL) { //echo $bridge_id;exit;
        //echo "<pre>";
        ini_set('max_execution_time', 0);
        ini_set("memory_limit", "256M");
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        if (!empty($this->getRequest()->getParam('salt'))) {
            $salt = sha1($this->getRequest()->getParam('salt'));
            $type = $this->getRequest()->getParam('type');
            $format = $this->getRequest()->getParam('format');
            $ogctable = Mage::getSingleton('core/resource')->getTableName('bridge/outgoingchannels');
            $ogcsql = 'SELECT id FROM ' . $ogctable . ' WHERE sha1(salt)="' . $salt . '"';
            $channelres = $readConnection->fetchAll($ogcsql); //print_r($channelres);exit;
            $bridge_id = $channelres[0]['id'];
        }
        if ($bridge_id) {
            $drive_data = array();
            $model = Mage::getModel('bridge/outgoingchannels');
            $model->load($bridge_id);
            $data = $model->getData();
             $ip_str = ($data['url'])?$data['url']:'';
            $ip_arr = array();
            $ip_arr = ($ip_str)?explode(',',$ip_str):array();
            //print_r($_SERVER['REMOTE_ADDR']);exit;
            if($data['channel_mode'] && !empty($ip_arr) && !in_array($_SERVER['REMOTE_ADDR'],$ip_arr)){
                echo 'Not Authorized';
                exit;
            }
            $servernm = str_replace('www.', '', $_SERVER['SERVER_NAME']);
            $channel_name = $servernm . '_' . $data['name'];
            $price_source = $data['price_source'];
            $others = unserialize($data['others']); //echo "<pre>";print_r($others);exit;
            $include_deleted_prod = $others['include_deleted_products'];
            if ($is_drive)
                $format = $others['ftp_format'];

            $other_edi_qty_arr = array();
            $source_folder = $_SERVER['DOCUMENT_ROOT'] . "/ogccsv";
            if ($type == 'ftp') { //echo "<pre>";print_r($others);exit;
                if ($others['export_automatically']) {
                    $ftp_server = $others['ftp_url_ogc']; //'67.227.186.73';
                    $ftp_conn = ftp_connect($ftp_server) or die("Couldn't connect to $ftp_server");
                    $ftp_username = $others['ftp_username_ogc']; //'fedobeo' ;
                    $ftp_userpass = $others['ftp_password_ogc']; //'contingencyp455';       
                    $ftp_filepath = $others['ftp_file_path_ogc']; //'contingencyp455';       
                    $login = ftp_login($ftp_conn, $ftp_username, $ftp_userpass);
                    if (!file_exists($source_folder)) {
                        mkdir($source_folder);
                        chmod($source_folder, 0777);
                    }
                } else {
                    echo 'no ftp options';
                    exit;
                }
            }
            if ($others['store_fields'])
                $other_edi_qty_arr = explode(',', $others['store_fields']); //print_r($other_edi_qty_arr);exit;



                
//$allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies(); //print_r($allowedCurrencies);exit;
            $db_price['currency'] = $others['currency'];
            if ($data['channel_type']) {
                $table = Mage::getSingleton('core/resource')->getTableName('bridgecsv/csvogcdata');
                $editable = Mage::getSingleton('core/resource')->getTableName('bridge/incomingchannels_product');
                $qu = 'SELECT ogc.id,ogc.bridge_channel_id,ogc.entity_id,ogc.mapping_data,ogc.is_condition_changed,ogc.is_processed,ogc.is_cron_run,ogc.is_ignored,ogc.created_at,ogc.updated_at,ogc.last_ftp_upload_time FROM ' . $table . ' as ogc JOIN `bridge_incomingchannel_product_info` pfo ON ogc.entity_id = pfo.product_entity_id  WHERE ogc.bridge_channel_id = ' . $bridge_id . ' AND pfo.channel_id = ' . $bridge_id . '  AND pfo.channel_type="outgoing" ';// AND pfo.is_processed!=2
                if($include_deleted_prod)
                 $qu.= 'ORDER BY FIELD(pfo.is_processed,"3","4","-1","-2","1","0","2"),pfo.last_updated_time DESC';
                else
                 $qu.= 'AND pfo.is_processed NOT IN("3","4") AND pfo.deleted!=1 ORDER BY FIELD(pfo.is_processed,"-1","-2","1","0","2"),pfo.last_updated_time DESC';   
                //echo $qu;exit;
                $sql_select_res = $readConnection->fetchAll($qu);
                //$sql_select_res = $readConnection->fetchAll('SELECT * FROM ' . $table . ' as ogc JOIN `bridge_incomingchannel_product_info` as cat ON ogc.entity_id = cat.product_entity_id  WHERE bridge_channel_id = ' . $bridge_id.' ORDER BY FIELD(is_processed,"3","4","1","0")');
                //$sql_select_res = $readConnection->fetchAll('SELECT * FROM ' . $table . ' WHERE bridge_channel_id = ' . $bridge_id.' ORDER BY FIELD(is_processed,"3","4","1","0")');
                if (!empty($sql_select_res)) { //echo "<pre>";print_r($sql_select_res);exit;
                    $count = count($sql_select_res);
                    $is_conditionchanged_arr = array_column($sql_select_res, 'is_condition_changed');
                    $is_processed_arr = array_column($sql_select_res, 'is_processed');
                    $entity_id_arr = array_column($sql_select_res, 'entity_id');
                    $entity_id_str = implode(',', $entity_id_arr);
                    $count_val = array_count_values($is_conditionchanged_arr); //if channel condition is not changed.
                    $count_val_p = array_count_values($is_processed_arr); //if channel is processing or not.

                    $mappingdata_ar = array_column($sql_select_res, 'mapping_data');
                    $count_val_mappingdata = array_count_values($mappingdata_ar); //print_r($count_val);exit;
                    $total_mapping_data_count = array_sum($count_val_mappingdata); //total number of product's data collected so far.
                    $status_code=300;
                    $response_code = 'generating product feed';//echo "<pre>";print_r($sql_select_res);exit; echo $total_mapping_data_count .'=='. count($sql_select_res);exit;
                    if (!isset($count_val_p[1]) && !isset($count_val[1]) && !empty($count_val_mappingdata) && $total_mapping_data_count == count($sql_select_res)) {
                        $status_code=200;
                        $response_code = 'ok';
                        //}
                        $attributeset = $sl_no = $header = array();
                        if ($others['slno_in_csv']) {
                            $sl_no = array('serial_no');
                        }; //exit;
                        if ($others['attribute_set_in_csv']) {
                            $attributeset = array('Attributeset');
                        }; //exit;
                        if ($others['slno_in_csv']) {
                            $sl_no = array('serial_no');
                        }; //exit;
                        if ($type == 'view') {
                            $sep = "\t";
                        } else {
                            $sep = ",";
                        }
                        $eol = "\n";
                        $j = $i = 0;
                        $str = array();
                        $edi_status_value = array(-2 => 'new out of stock', -1 => 'new', 0 => 'changed', 1 => 'synced', 2 => 'processing', 3 => 'no access', 4 => 'trashed');
                        // if ($price_source == 'price') {
                        $sql_select_price = $readConnection->fetchAll('SELECT * FROM ' . $editable . ' WHERE product_entity_id IN (' . $entity_id_str . ') AND channel_id=' . $bridge_id . ' AND channel_type="outgoing"');
                        // }
                        /* else if ($price_source == 'ogc_price') {
                          $sql_select_price = $readConnection->fetchAll('SELECT value as price,entity_id as product_entity_id  FROM  `catalog_product_entity_decimal` WHERE entity_id IN (' . $entity_id_str . ') AND attribute_id=341'); //print_r($sql_select_price);exit;
                          } */
                        $edi_entityid_price = array_combine(array_column($sql_select_price, 'product_entity_id'), array_column($sql_select_price, 'converted_price'));// echo "<pre>";print_r($edi_entityid_price);exit;
                        $edi_entityid_processed = array_combine(array_column($sql_select_price, 'product_entity_id'), array_column($sql_select_price, 'is_processed'));
                        $edi_entityid_qty = array_combine(array_column($sql_select_price, 'product_entity_id'), array_column($sql_select_price, 'quantity'));
                        $edi_entityid_time = array_combine(array_column($sql_select_price, 'product_entity_id'), array_column($sql_select_price, 'last_updated_time'));
                        $edi_entityid_deleted = array_combine(array_column($sql_select_price, 'product_entity_id'), array_column($sql_select_price, 'deleted'));

                        $nin_edi_price = array_diff($entity_id_arr, array_keys($edi_entityid_price)); //print_r($edi_entityid_price);exit;
                        foreach ($sql_select_res as $k => $v) {
                            
                            if ($edi_status_value[$edi_entityid_processed[$v['entity_id']]]) {
                                $edi_status = $edi_status_value[$edi_entityid_processed[$v['entity_id']]];
                            } else {
                                $edi_status = 'unknown';
                            }
                            //if(!in_array($edi_status,array('processing','unknown'))){
                            $map_data = unserialize($v['mapping_data']); //print_r($v['mapping_data']);
                            
                            $attribute_set = $map_data['attribute_set'];
                            unset($map_data['attribute_set']);
                            if ($i == 0) {
                                $header = array_keys($map_data);
                                $header = array_merge($sl_no, $header);
                                $header = array_merge($attributeset, $header);
                                if (!empty($other_edi_qty_arr) && in_array('edi_status', $other_edi_qty_arr))
                                    array_push($header, 'Edi status');
                                    $i++;
                                }
                                if ($sl_no) {
                                    $sl_no_val = array($j);
                                    $j++;
                                    $map_data = array_merge(array('serial_no' => $j), $map_data);
                                }
                                if ($attributeset) {
                                    $map_data = array_merge(array('Attributeset' => $attribute_set), $map_data);
                                }
                                $db_price['price'] = (isset($edi_entityid_price[$v['entity_id']])) ? $edi_entityid_price[$v['entity_id']] : ''; //print_r($db_price);exit;
                                $db_price['last_updated_time'] = (isset($edi_entityid_time[$v['entity_id']])) ? $edi_entityid_time[$v['entity_id']] : ''; //print_r($db_price);exit;
                                $db_price['quantity'] = (isset($edi_entityid_qty[$v['entity_id']])) ? $edi_entityid_qty[$v['entity_id']] : ''; //print_r($db_price);exit;
                                $db_price['edi_status'] = (isset($edi_entityid_processed[$v['entity_id']])) ? $edi_entityid_processed[$v['entity_id']] : ''; //print_r($db_price);exit;
                                $db_price['deleted'] = (isset($edi_entityid_deleted[$v['entity_id']])) ? $edi_entityid_deleted[$v['entity_id']] : ''; //print_r($db_price);exit;
                                //$currency = 'USD';
                                //echo "<pre>";print_r($map_data);exit;
                                array_walk($map_data, function(&$v, $k, $data) {
                                    //keys have first letter capital inorder to match them lowercase them first
                                    $k = str_replace(" ", '_', $k);
                                    $k = strtolower($k);
                                    $v = stripslashes(str_replace(array("\r", "\n", ",", ";"), '', $v)); //print_r($v."<br>");
                                    if ($v == 'calculate') { //echo 123;exit;
                                        $v = ($data['price']) ? $data['price'] : $v;// print_r($v);exit;
                                        if ($v != 'calculate')
                                            $v = $this->currency_convAction($v, $data['currency']); //print_r($v);exit;
                                    }
                                    str_replace('category', '', $k, $cat_cnt);
                                    if ($cat_cnt) {
                                        $v = str_replace('Root Catalog/Default Category/', '', $v);
                                    }
                                    str_replace('last_updated_time', '', $k, $time_cnt);
                                    if($time_cnt)
                                        $v = $data['last_updated_time'];
                                    str_replace('quantity', '', $k, $qty_cnt);
                                    if($qty_cnt){
                                        if(in_array($data['edi_status'],array(2,4,3)) || $data['deleted'])
                                            $v = 0;
                                        else 
                                            $v = $data['quantity'];
                                    }
                                }, $db_price);  //print_r($map_data);exit;
                           // }
                            if (!empty($other_edi_qty_arr) && in_array('edi_status', $other_edi_qty_arr))
                                $map_data = array_merge($map_data, array('Edi status' => $edi_status));
                            $str[] = implode($sep, $map_data);
                            $xml[] = $map_data;
                            $xls[$attribute_set][] = $map_data;
                            //}
                        }//exit;
//                        echo "<pre>";
//                        print_r($xls);
//                        exit;
                        $status_id = array_keys($edi_entityid_processed);
                        $status_id_sql_1 = $writeConnection->query('UPDATE ' . $editable . ' SET is_processed = 4 WHERE is_processed=3 AND product_entity_id IN (' . implode(",", $status_id) . ') AND channel_type="outgoing" AND channel_id = ' . $bridge_id);
                        $status_id_sql_2 = $writeConnection->query('UPDATE ' . $editable . ' SET is_processed = 1 WHERE is_processed IN ("-1","-2","0") AND product_entity_id IN (' . implode(",", $status_id) . ') AND channel_type="outgoing" AND channel_id = ' . $bridge_id);
                        $str = array_filter($str);
                        $csv_output.= implode($sep, $header) . $eol . implode($eol, $str);
                        $channel_name = str_replace('.com', '', $channel_name);
                        $file = $channel_name . '_' . date('Y-m-d-H:i:s', time()); //echo $format;exit;
                        if ($format == 'csv') {
                            $filename = $file . '.csv';
                            if ($type == 'view') {
                                echo $csv_output;
                            } else if ($type == 'ftp') {
                                if ($others['upload_file_to_ftp']) {
                                    $sourcefile_path = $source_folder . '/' . $filename;
                                    file_put_contents($sourcefile_path, $csv_output); //exit;
                                    $dest_file = $ftp_filepath . '/' . $filename;
                                    if (ftp_put($ftp_conn, $dest_file, $sourcefile_path, FTP_BINARY)) {
                                        echo "Successfully uploaded $filename";
                                        unlink($sourcefile_path);
                                    } else {
                                        echo "Error uploading $filename";
                                    }
                                    ftp_close($ftp_conn);
                                } else {
                                    echo 'no ftp options';
                                }
                                exit;
                            } else if ($type == 'drive') { // echo $csv_output;exit;
                                $drive_data['data'] = $csv_output;
                                $drive_data['file_name'] = $filename;
                                return $drive_data;
                                exit;
                            } else if ($type == 'email') { // echo $csv_output;exit;
                                $email_data['data'] = $csv_output;
                                $email_data['file_name'] = $filename;
                                $email_data['extenssion'] = 'csv';
                                $email_data['channel_name'] = $channel_name;
                                return $email_data;
                                exit;
                            } else {
                                $encoded_csv = mb_convert_encoding($csv_output, 'UTF-16LE', 'UTF-8');
                                header('Content-Description: File Transfer');
                                header('Content-Type: application/vnd.ms-excel');
                                header('Content-Disposition: attachment; filename=' . $filename);
                                header('Content-Transfer-Encoding: binary');
                                header('Expires: 0');
                                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                                header('Pragma: public');
                                header('Content-Length: ' . strlen($encoded_csv));
                                print chr(255) . chr(254) . $encoded_csv;
                            }
                        } else if ($format == 'xml') {
                            $filename = $file . '.xml';
                            $doc = new DOMDocument('1.0');
                            $doc->formatOutput = true;
                            $subElt_root = $doc->createElement('Productfeed');
                            $subElt_root_1 = $doc->appendChild($subElt_root);

                            $subElt_data = $doc->createElement('data');
                            $subElt_data_1 = $subElt_root_1->appendChild($subElt_data);

                            $subElt_meta = $doc->createElement('meta');
                            $subElt_meta_1 = $subElt_data_1->appendChild($subElt_meta);

                            $status = $doc->createElement('status');
                            $status_1 = $subElt_meta_1->appendChild($status);
                            $status_text = $doc->createTextNode($status_code);
                            $status_text_1 = $status_1->appendChild($status_text);

                            $response = $doc->createElement('response');
                            $response_1 = $subElt_meta_1->appendChild($response);
                            $response_text = $doc->createTextNode($response_code);
                            $response_text_1 = $response_1->appendChild($response_text);

                            $total_count = $doc->createElement('totalcount');
                            $total_count_1 = $subElt_meta_1->appendChild($total_count);
                            $totalcount_text = $doc->createTextNode($count);
                            $totalcount_text_1 = $total_count_1->appendChild($totalcount_text);

                            $rootElt = $doc->createElement('itemlist');
                            $root = $subElt_root_1->appendChild($rootElt);

                            foreach ($xml as $k => $v) {
                                $subElt = $doc->createElement('item');
                                foreach ($v as $k1 => $v1) {
                                     $v1 = str_replace('','',$v1,$spcnt);
                                    $v1 = (strlen($v1)|| $v1===0) ? $v1 : 'N/A'; //echo $v1."<br>";
                                    $k1 = trim(str_replace(' ', '_', $k1),'_');
                                    $subNode = $root->appendChild($subElt);
                                    $title = $doc->createElement($k1);
                                    $title = $subNode->appendChild($title);
                                    $text = $doc->createTextNode($v1);
                                    $text = $title->appendChild($text);
                                }
                            }
                            if ($type == 'view') {
                                header('Content-type: text/xml');
                                echo $doc->saveXML();
                            } else if ($type == 'ftp') {
                                $sourcefile_path = $source_folder . '/' . $filename;
                                $doc->save($sourcefile_path);
                                $dest_file = $ftp_filepath . '/' . $filename;
                                //$dest_file= '/subdomains/dev/ogccsv/test2.xml';
                                //ftp_chdir($ftp_conn, $_SERVER['DOCUMENT_ROOT'].'/ogccsv/');
                                if (ftp_put($ftp_conn, $dest_file, $sourcefile_path, FTP_BINARY)) {
                                    echo "Successfully uploaded $filename";
                                    unlink($sourcefile_path);
                                } else {
                                    echo "Error uploading $filename";
                                }
                                // close connection
                                ftp_close($ftp_conn);
                                exit;
                            } else if ($type == 'drive') {
                                $sourcefile_path = $source_folder . '/' . $filename; //echo $sourcefile_path;exit;
                                $doc->save($sourcefile_path);
                                $xml_data = file_get_contents($sourcefile_path); //echo $xml_data;exit;
                                $drive_data['data'] = $xml_data;
                                $drive_data['file_name'] = $filename;
                                unlink($sourcefile_path);
                                return $drive_data;
                                exit;
                            } else if ($type == 'email') { // echo $csv_output;exit;
                                $sourcefile_path = $source_folder . '/' . $filename; //echo $sourcefile_path;exit;
                                $doc->save($sourcefile_path);
                                $xml_data = file_get_contents($sourcefile_path);
                                $email_data['data'] = $xml_data;
                                $email_data['file_name'] = $filename;
                                $email_data['extenssion'] = 'xml';
                                $email_data['channel_name'] = $channel_name;
                                unlink($sourcefile_path);
                                return $email_data;
                                exit;
                            } else {
                                header('Content-type: text/xml');
                                header('Content-Disposition: attachment; filename="' . $filename . '"');
                                echo $doc->saveXML();
                            }
                        }else if($format == 'xls'){
                            require_once Mage::getBaseDir('lib') . "/PHPExcel/Classes/PHPExcel.php";
                            $objPHPExcel = new PHPExcel();//echo $fileName;exit;
                            //ob_start();
                            $filename = $file . '.xlsx';
                             $activesheet = 0;
                             $xls_header = array_filter(array_keys($xls));// print_r(array_filter($xls_header));exit;
                             foreach($xls_header as $k7=>$v7){
                            //  if ($activesheet == 0) {
//                                    $objPHPExcel->setActiveSheetIndex($activesheet);
//                                    $objPHPExcel->getActiveSheet()->setTitle($v7);
//                                    $sheetindexforattribute[$v7] = $activesheet;
                            //    }
                                if ($activesheet > 0) {
                                    $objPHPExcel->createSheet();
                                }
                                 $sheet = $objPHPExcel->setActiveSheetIndex($activesheet);
                                 $sheet->setTitle("$v7");
                                 $sheetindexforattribute[$v7] = $activesheet;
                                $attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection') 
                                        ->addFieldToFilter('attribute_set_name',$v7)
                                        ->getData(); //print_r($attributeSetCollection[0]['attribute_set_id']);exit;
                                $attrs = Mage::getModel('catalog/product_attribute_api')->items($attributeSetCollection[0]['attribute_set_id']);;
                                $attribute_cl[$v7]=array_column($attrs,'code');
                                
                                $ct = 0;
                                foreach ($header as $k3 => $v3) {
                                    $lv = str_replace(' ','_',trim(strtolower($v3)));
                                    if(in_array($lv,$attribute_cl[$v7]) || in_array($lv, array('last_updated_time','currency','currencysymbol','image_url_1','image_url_2','image_url_3','image_url_4','image_url_5','quantity','edi_status','Attributeset','category'))){
                                        $v3 = (!is_array($v3) ? ucfirst(str_replace('_',' ', $v3)) : '');
                                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($ct, 1, $v3);
                                        $ct++;
                                    }else{
                                        $nin[$v7][$k3]=trim($v3);
                                    }
                                } 
                                $activesheet++;
                            }
                           //print_r($nin);exit;
                             //echo $fileName;exit;
                            $row = 2;
                            foreach($xls as $k8=>$v8){
                                if($k8!=''){ 
                                 $row = 2;
                                foreach($v8 as $k5=>$v5){
                                    $col = 0;
                                    foreach($v5 as $k6=>$v6){
                                        $nk = trim($k6);// echo "$nk<br>";
                                        if(!in_array($nk,$nin[$k8])){
                                            $objPHPExcel->setActiveSheetIndex($sheetindexforattribute[$k8]);
                                            $value_1 = trim(strip_tags($v6));
                                            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $value_1);
                                            $col++;
                                        }else{
                                           $nin_1[$k8][$k6]=trim($v6);
                                        }
                                    }
                                    $row++;
                                  }
                            }
                             }
                            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                            if ($type == 'view') { //echo 345;exit;
                               echo implode($sep, $header) . $eol . implode($eol, $str);
                               exit;
                            } else if ($type == 'ftp') {
                                $sourcefile_path = $source_folder . '/' . $filename;
                                $objWriter->save($sourcefile_path);
                                $dest_file = $ftp_filepath . '/' . $filename;
                                if (ftp_put($ftp_conn, $dest_file, $sourcefile_path, FTP_BINARY)) {
                                    echo "Successfully uploaded $filename";
                                    unlink($sourcefile_path);
                                } else {
                                    echo "Error uploading $filename";
                                }
                                // close connection
                                ftp_close($ftp_conn);
                                exit;
                            } else if ($type == 'drive') {
                                $sourcefile_path = $source_folder . '/' . $filename; //echo $sourcefile_path;exit;
                                $objWriter->save($sourcefile_path);
                                $xls_data = file_get_contents($sourcefile_path); //echo $xml_data;exit;
                                $drive_data['data'] = $xls_data;
                                $drive_data['file_name'] = $filename;
                                unlink($sourcefile_path);
                                return $drive_data;
                                exit;
                            } else if ($type == 'email') { // echo $csv_output;exit;
                                $sourcefile_path = $source_folder . '/' . $filename; //echo $sourcefile_path;exit;
                                $objWriter->save($sourcefile_path);
                                $xls_data = file_get_contents($sourcefile_path);
                                $email_data['data'] = $xls_data;
                                $email_data['file_name'] = $filename;
                                $email_data['extenssion'] = 'xls';
                                $email_data['channel_name'] = $channel_name;
                                unlink($sourcefile_path);
                                return $email_data;
                                exit;
                            } else {
                                ob_end_clean();
                                header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                                header('Content-Disposition: attachment; filename="' . $filename . '"');
                                header("Cache-Control: max-age=0");
                                //header('Content-Disposition: attachment; filename="' . $filename . '"');
                                $objWriter->save('php://output');
                            }
                        }
                        exit;
                    } else {
                        echo "This channel is not ready. Please wait sometime.";
                        exit;
                    }
                }
            } else {
                echo "channel inactive";
                exit;
            }
        }
        exit;
    }

    public function cronrouteAction() {
        if (isset($_SERVER['argv']) && !empty($_SERVER['argv'])) {
            parse_str($_SERVER['argv'][0], $arr);
            $debug = var_export($arr, true);
            $name = $arr['name'];
            $channel_id = $arr['channel_id'];
            /* if ($_SERVER['SERVER_NAME'] == 'kanarygifts.com') {
              $file_path = $_SERVER['DOCUMENT_ROOT'] . DS . 'amazon' . DS . 'import' . DS . 'test1.php';
              $file = fopen($file_path, "a+");
              fwrite($file, "$debug\n");
              fclose($file); //exit;
              } */
        } else {
            $name = $this->getRequest()->getParam('name');
            $channel_id = $this->getRequest()->getParam('channel_id');
        }
        if ($name == 'cronstart') {
            echo Mage::helper('bridgecsv')->cronready();
        }
        exit;
    }

    public function currency_convAction($price, $currentCurrencyCode) {
        // Base Currency
        $baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
        if ($currentCurrencyCode != $baseCurrencyCode) {
            // Allowed currencies
            $allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
            $rates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, array_values($allowedCurrencies));
            // the price converted
            $price = $price / $rates[$currentCurrencyCode];
        }
        return $price;
    }

    public function __getFrontendProductUrl($product) {
        $websites = Mage::app()->getWebsites(); //print_r($websites);exit;
        $stores = array();
        foreach ($websites as $wk => $wv) {
            $website_id = $wv->getId();
            break;
        }
        $website_id = ($this->getRequest()->getParam('website')) ? $this->getRequest()->getParam('website') : $website_id;
        $websites = Mage::getModel("core/website")->load($website_id);
        foreach ($websites->getStoreIds() as $sk => $sv) {
            $store_id = $sv;
            break;
        }
        $storeid = ($this->getRequest()->getParam('store')) ? $this->getRequest()->getParam('store') : $store_id;
        $use_store_code_in_url = Mage::getStoreConfig('web/url/use_store', $storeid);
        if ($use_store_code_in_url) {
            $store_code = Mage::getModel('core/store')->load($storeid)->getCode();
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $store_code . '/' . $product->getUrlPath();
        } else {
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . $product->getUrlPath();
        }
    }

    public function googledrive_uploadAction() {
        if (!empty($this->getRequest()->getParam('channel_id'))) {
            $channel_id = (isset($_SESSION['channel_id'])) ? $_SESSION['channel_id'] : $this->getRequest()->getParam('channel_id');
            if (!isset($_SESSION['channel_id']))
                $_SESSION['channel_id'] = $channel_id;
            $model = Mage::getModel('bridge/outgoingchannels');
            $model->load($channel_id);
            $data = $model->getData();
            $others = unserialize($data['others']); // print_r($others);exit;
            if ($others['refreshtoken']) {// print_r($others);exit;
                $client_id = '1016428387562-q3p0hjgqae2e3idha17n6miqgfp1bd8k.apps.googleusercontent.com'; //'641471234732-urimipams8pfmtfeds3n9tv3355e1jmj.apps.googleusercontent.com'; //<YOUR_CLIENT_ID>';
                $client_secret = 'qtaD_tBcW2N_-sYO9H0hPNEu'; //'LgwlTqqxTZiFA_gtMkw80ftG'; //<YOUR_CLIENT_SECRET>';
               // $redirect_uri = Mage::getBaseUrl() .'http://localhost/bridge/index.php/admin/bridgecsv/googledrive_upload'; //urn:ietf:wg:oauth:2.0:oob';//4/USfXx9c9VduM-H-hJlwVYn7cXSrVSU0M2xwERxxa4SU
                $redirect_uri = Mage::getBaseUrl() .'admin/bridgecsv/googledrive_upload'; //urn:ietf:wg:oauth:2.0:oob';//4/USfXx9c9VduM-H-hJlwVYn7cXSrVSU0M2xwERxxa4SU

                $client = new Google_Client();
                $client->setClientId($client_id);
                $client->setClientSecret($client_secret);
                $client->setRedirectUri($redirect_uri);
//            $client->setAccessType('offline');
                //$client->setApprovalPrompt('force') ;
                $client->refreshToken($others['refreshtoken']);
                $access_token = $client->getAccessToken(); //echo $access_token;exit;
                //$_SESSION['upload_token'] = $access_token;
                $client->addScope("https://www.googleapis.com/auth/drive");
                $service = new Google_Service_Drive($client);
                if (isset($_GET['code'])) {
                    $client->authenticate($_GET['code']);
                    //$refreshtoken = '1/dbdwErTo74qwKYPRXJMyJJsxxTv3X6OXIgD3NVemGjo';//1/RSmanwG1RblZ7-OHFc8gRhIHBmjS4B_KKYKjVEkVzZo;//1/z39Hi0M8xm8HUTNmFPovqrSNT3qiARcvABhmeo6DX1A
                    $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
                    header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
                }

                /*                 * **********************************************
                  If we're signed in then lets try to upload our
                  file. For larger files, see fileupload.php.
                 * ********************************************** */
                if ($access_token) { //echo $channel_id;exit;
                    $drivedata = $this->file_cronAction($channel_id, 'drive', 1);
                    $data = $drivedata['data'];
                    $filename = $drivedata['file_name'];
                    // Now lets try and send the metadata as well using multipart!
                    $file = new Google_Service_Drive_DriveFile();
                    $file->setName($filename);
                    $result2 = $service->files->create(
                            $file, array(
                        'data' => $data, //file_get_contents(TESTFILE),
                        'mimeType' => 'application/octet-stream',
                        'uploadType' => 'multipart'
                            )
                    );
                    echo $filename . ' file uploaded';
                } else {
                    echo 'no accesstoken';
                }
            } else {
                echo 'no refreshtoken';
            }
            exit;
        }
        exit;
    }

    public function store_refreshtokenAction() {
        ini_set('max_execution_time', 0);
        ini_set("memory_limit", "256M");
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $writeConnection = $resource->getConnection("core_write");
        if (!empty($this->getRequest()->getParam('salt')) || $_GET['code'] || $_GET['error']) {
            session_start();
            if (!isset($_SESSION['refferer'])) {
                $_SESSION['refferer'] = $_SERVER['HTTP_REFERER'];
            }
            $salt = (isset($_SESSION['salt'])) ? $_SESSION['salt'] : $this->getRequest()->getParam('salt');
            $_SESSION['salt'] = $salt;
            $salt = sha1($salt);
            $ogctable = Mage::getSingleton('core/resource')->getTableName('bridge/outgoingchannels');
            $ogcsql = 'SELECT id,others FROM ' . $ogctable . ' WHERE sha1(salt)="' . $salt . '"';
            $channelres = $readConnection->fetchAll($ogcsql); //print_r($channelres);exit;
            $bridge_id = $channelres[0]['id'];
            $others = unserialize($channelres[0]['others']); //print_r($others);exit;
            if ($bridge_id) {
                $client_id = '1016428387562-q3p0hjgqae2e3idha17n6miqgfp1bd8k.apps.googleusercontent.com'; //'641471234732-urimipams8pfmtfeds3n9tv3355e1jmj.apps.googleusercontent.com'; //<YOUR_CLIENT_ID>';
                $client_secret = 'qtaD_tBcW2N_-sYO9H0hPNEu'; //'LgwlTqqxTZiFA_gtMkw80ftG'; //<YOUR_CLIENT_SECRET>';
                //$redirect_uri = 'http://localhost/bridge/index.php/admin/bridgecsv/store_refreshtoken'; //urn:ietf:wg:oauth:2.0:oob';//4/USfXx9c9VduM-H-hJlwVYn7cXSrVSU0M2xwERxxa4SU
                $redirect_uri = Mage::getBaseUrl() .'admin/bridgecsv/store_refreshtoken'; //urn:ietf:wg:oauth:2.0:oob';//4/USfXx9c9VduM-H-hJlwVYn7cXSrVSU0M2xwERxxa4SU

                $client = new Google_Client();
                $client->setClientId($client_id);
                $client->setClientSecret($client_secret);
                $client->setRedirectUri($redirect_uri);
                $client->setAccessType('offline');
                $client->setApprovalPrompt('force') ;
                $client->addScope("https://www.googleapis.com/auth/drive");
                //$service = new Google_Service_Drive($client);
                // print_r($_GET['code']);exit;
                if (isset($_GET['code'])) {
                    $client->authenticate($_GET['code']);
                    // echo $client->getRefreshToken();exit;
                    $refreshtoken = $client->getRefreshToken(); //1/RSmanwG1RblZ7-OHFc8gRhIHBmjS4B_KKYKjVEkVzZo
                    $others['refreshtoken'] = $refreshtoken; //print_r($others);exit;
                    $refreshtoken_others = serialize($others);
                    $writeConnection->query("UPDATE " . $ogctable . " SET others='" . $refreshtoken_others . "' WHERE id=" . $bridge_id);
                    //echo "success";
                    //echo $_SESSION['refferer'];//$this->getUrl('adminhtml/bridgecsv/edit/id/'.$bridge_id);exit;
                    //print_r($_SERVER);exit;
                    $refer = $_SESSION['refferer'];
                    unset($_SESSION['refferer']);
                    $session = Mage::getSingleton('adminhtml/session');
                    $session->addSuccess(Mage::helper('catalog')->__("Verified"));
                    header('Location: ' . $refer);
                    exit;
                } else if ($_GET['error']) {
                    $others['refreshtoken'] = ''; //print_r($others);exit;
                    $refreshtoken_others = serialize($others);
                    $writeConnection->query("UPDATE " . $ogctable . " SET others='" . $refreshtoken_others . "' WHERE id=" . $bridge_id);
                    echo "access denied";
                    exit;
                } else {
                    $authUrl = $client->createAuthUrl();
                }
                if (isset($authUrl)) {
                    header('Location: ' . $authUrl);
                }
            }
        }
        exit;
    }

    function email_sendAction() {
        ob_clean();
        $channel_id = $this->getRequest()->getParam('channel_id'); //echo $channel_id;exit;
        if ($channel_id) {
             $model = Mage::getModel('bridge/outgoingchannels');
            $model->load($channel_id);
            $data = $model->getData();
            $others = unserialize($data['others']); 
            $fromEmail = $others['from_email'];
            $fromName = $others['from_email'];
            $toName = $others['to_email'];
            $toEmail = $others['to_email'];
            $body = $others['body_email'];
            $bcc_list = $others['bcc_email'];
            $content = $this->file_cronAction($channel_id, 'email', 1);
            // body text
            if (!empty($content)) {
                $subject = $content['channel_name'] . ' Feed';
                try {
                    $mail = new Zend_Mail();
                    $mail->setFrom($fromEmail, $fromName);
                    if(!empty($bcc_list)){
                        $mail->addBcc($bcc_list);
                    }
                    $mail->addTo($toEmail, $toName);
                    $mail->setSubject($subject);
                    $mail->setBodyHtml($body);
                    $attachment = $content['data'];
                    $mail->createAttachment(
                        $attachment,
                        Zend_Mime::TYPE_OCTETSTREAM,
                        Zend_Mime::DISPOSITION_ATTACHMENT,
                        Zend_Mime::ENCODING_BASE64,
                       $content['file_name']
                    );
                    $mail->send();
                    echo 'file uploaded';
                } catch (Exception $e) {
                    //echo $e->getMassage();
                    echo "error";
                }
            }
        }
        exit;
    }


}
