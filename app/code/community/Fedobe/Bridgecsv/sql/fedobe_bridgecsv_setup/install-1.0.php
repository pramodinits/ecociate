<?php
$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
$installer->startSetup();
$installer->run("
CREATE TABLE IF NOT EXISTS `bridgecsv_process` (
  `id` int(11),
  `channel_id` int(11) DEFAULT NULL,
  `status` tinyint DEFAULT 0,
  `filename` varchar(255) DEFAULT NULL,
  `total_records` int(11) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);
ALTER TABLE `bridgecsv_process` ADD PRIMARY KEY (`id`);
ALTER TABLE `bridgecsv_process` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
CREATE TABLE IF NOT EXISTS `bridgecsv_data` (
  `id` int(11),
  `channel_id` int(11) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `status` tinyint DEFAULT 0,
  `csv_line_number` int(11) DEFAULT 0,
  `data` text DEFAULT NULL,
  `is_header` tinyint DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);
ALTER TABLE `bridgecsv_data` ADD PRIMARY KEY (`id`);
ALTER TABLE `bridgecsv_data` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `bridgecsv_data` ADD `price` DECIMAL(10,2) NULL AFTER `data`, ADD `quantity` INT NULL AFTER `price`, ADD `store_view` VARCHAR(255) NULL AFTER `quantity`, ADD `primary_field_updated` BOOLEAN NOT NULL DEFAULT FALSE AFTER `store_view`, ADD `priority` INT NULL AFTER `primary_field_updated`;
ALTER TABLE `bridgecsv_data` CHANGE `store_view` `store_view` INT NULL DEFAULT NULL;
ALTER TABLE `bridgecsv_data` ADD `last_updated_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `is_header`;
ALTER TABLE `bridgecsv_data` CHANGE `status` `pim_processed` TINYINT(4) NOT NULL DEFAULT '0';
ALTER TABLE `bridgecsv_data` ADD `edi_processed` BOOLEAN NOT NULL DEFAULT FALSE AFTER `pim_processed`;
ALTER TABLE `bridgecsv_data` ADD `is_deleted` BOOLEAN NOT NULL DEFAULT FALSE AFTER `is_header`;
ALTER TABLE `bridgecsv_data` ADD `channel_weightage` INT NULL AFTER `is_deleted`;
ALTER TABLE `bridgecsv_data` ADD `make_out_of_stock` BOOLEAN NOT NULL DEFAULT FALSE AFTER `channel_weightage`;
ALTER TABLE `bridgecsv_data` ADD `pim_skipped` BOOLEAN NOT NULL DEFAULT FALSE AFTER `edi_processed`;
ALTER TABLE `bridgecsv_data` CHANGE `data` `data` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
CREATE TABLE `bridgecsv_ogcdata` (
  `id` int(50) NOT NULL,
  `entity_id` int(50) DEFAULT NULL,
  `bridge_channel_id` int(50) DEFAULT NULL,
  `mapping_data` text,
  `is_processed` int(1) DEFAULT '0',
  `is_ignored` int(1) DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP
);
ALTER TABLE `bridgecsv_ogcdata`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `entity_id` (`entity_id`,`bridge_channel_id`);
ALTER TABLE `bridgecsv_ogcdata`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;
ALTER TABLE `bridgecsv_ogcdata` ADD `is_cron_run` INT(1) NULL DEFAULT '0' AFTER `is_processed`;
ALTER TABLE `bridgecsv_ogcdata` ADD `is_condition_changed` INT(1) NULL DEFAULT '0' AFTER `mapping_data`;
ALTER TABLE `bridgecsv_ogcdata` ADD `ediorder` INT(100) NULL AFTER `is_ignored`;");
$installer->endSetup();
 

