<?php

/**
 * Catalin Ciobanu
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License (MIT)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/MIT
 *
 * @package     Catalin_Seo
 * @copyright   Copyright (c) 2016 Catalin Ciobanu
 * @license     https://opensource.org/licenses/MIT  MIT License (MIT)
 */
class Catalin_SEO_Model_Catalog_Layer_Filter_Price extends Mage_Catalog_Model_Layer_Filter_Price {

    protected $_values = array();

    public function getValues() {
        return $this->_values;
    }

    /**
     * Get maximum price from layer products set
     *
     * @return float
     */
    public function getMaxPriceFloat() {
        if (!$this->hasData('max_price_float')) {
            $this->collectPriceRange();
        }

        return $this->getData('max_price_float');
    }

    /**
     * Get minimum price from layer products set
     *
     * @return float
     */
    public function getMinPriceFloat() {
        if (!$this->hasData('min_price_float')) {
            $this->collectPriceRange();
        }

        return $this->getData('min_price_float');
    }

    /**
     * Collect useful information - max and min price
     *
     * @return Catalin_SEO_Model_Catalog_Layer_Filter_Price
     */
    protected function collectPriceRange() {
        $collection = $this->getLayer()->getProductCollection();
        $select = $collection->getSelect();
        $conditions = $select->getPart(Zend_Db_Select::WHERE);

        // Remove price sql conditions
        $conditionsNoPrice = array();
        foreach ($conditions as $key => $condition) {
            if (stripos($condition, 'price_index') !== false) {
                continue;
            }
            $conditionsNoPrice[] = $condition;
        }
        $select->setPart(Zend_Db_Select::WHERE, $conditionsNoPrice);

        $this->setData('min_price_float', floor($collection->getMinPrice()));
        $this->setData('max_price_float', round($collection->getMaxPrice()));

        // Restore all sql conditions
        $select->setPart(Zend_Db_Select::WHERE, $conditions);

        return $this;
    }

    /**
     * Get data for build price filter items
     *
     * @return array
     */
    protected function _getItemsData() {
        $range = 100;
        $this->getPriceRange();
        $dbRanges = $this->getRangeItemCounts($range);
        $filter = Mage::app()->getRequest()->getParam($this->getRequestVar());

        $suffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
        $filter = str_replace($suffix, '', $filter);
        $this->_values = explode(Catalin_SEO_Helper_Data::MULTIPLE_FILTERS_DELIMITER, $filter);
        $data = array();
        if (!empty($dbRanges)) {
            $lastIndex = array_keys($dbRanges);
            $lastIndex = $lastIndex[count($lastIndex) - 1];

            foreach ($dbRanges as $index => $count) {
                $fromPrice = ($index == 1) ? '' :(($index - 1) * $range);
                $toPrice_val = $toPrice = ($index == $lastIndex) ? '' : ($index * $range);
                if($toPrice)
                $toPrice_val = $toPrice-0.01;
                $data[] = array(
                    'label' => $this->_renderRangeLabel($fromPrice, $toPrice),
                    'value' => $fromPrice . '-' . $toPrice_val,
                    'count' => $count,
                );
            }
        }
        return $data;
    }

    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock) {
        /**
         * Filter must be string: $fromPrice-$toPrice
         */
        $filter = $request->getParam($this->getRequestVar());
        if (!$filter) {
            return $this;
        }
        //validate filter
       if(strpos($filter,',')===false){
         $filterParams[0] = str_replace('.html','',$filter);
       }else{
         $filterParams = explode(',', $filter);
       }
       //print_r($filter);exit;
        $filter = $this->_validateFilter($filterParams[0]);// print_r($filter);exit;
        if (!$filter) {
            return $this;
        }

        list($from, $to) = $filter;

        $this->setInterval(array($from, $to));

        $priorFilters = array();
        for ($i = 1; $i < count($filterParams); ++$i) {
            $priorFilter = $this->_validateFilter($filterParams[$i]);
            if ($priorFilter) {
                $priorFilters[] = $priorFilter;
            } else {
                //not valid data
                $priorFilters = array();
                break;
            }
        }
        if ($priorFilters) {
            $this->setPriorIntervals($priorFilters);
        }

        $this->_applyPriceRange();

        $pricefilter = $request->getParam($this->getRequestVar());
        $suffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
        $del = Catalin_SEO_Helper_Data::MULTIPLE_FILTERS_DELIMITER;
        $pricefilter = str_replace($suffix, '', $pricefilter);
        if (!$pricefilter) {
            return $this;
        }
        $from = $to = "";
        $pricearr = explode($del, $pricefilter);
        $minfrom = explode('-', $pricearr[0]);
        $from = $minfrom[0];
        $lastindex = count($pricearr) - 1;
        $maxto = explode('-', $pricearr[$lastindex]);
        $to = $maxto[1];
        $this->getLayer()->getState()->addFilter($this->_createItem(
                        $this->_renderRangeLabel(empty($from) ? 0 : $from, $to,1), $filter
        ));

        return $this;
    }
     protected function _renderRangeLabel($fromPrice, $toPrice, $param=NULL)
    {
        $store      = Mage::app()->getStore();
        $formattedFromPrice  = $store->formatPrice($fromPrice);
        if ($toPrice === '') {
            return Mage::helper('catalog')->__('%s and above', $formattedFromPrice);
        } elseif ($fromPrice == $toPrice && Mage::app()->getStore()->getConfig(self::XML_PATH_ONE_PRICE_INTERVAL)) {
            return $formattedFromPrice;
        } else {
            if ($fromPrice != $toPrice && IS_NULL($param)) {
                $toPrice -= .01;
            }
            return Mage::helper('catalog')->__('%s - %s', $formattedFromPrice, $store->formatPrice($toPrice));
        }
    }


}